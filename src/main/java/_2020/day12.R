#Decembre 12 -----------------------------------------------------------------------------------
source("script.R")

## import input
data <- read_csv2("~/Projets/adventofcode_2020/input/input12.txt",col_names = FALSE)
data$action=str_sub(data$X1, 1, 1) 
data$valeur <- str_sub(data$X1, 2,nchar(data$X1))%>% as.numeric()


#### Part A ----

# Traitement des différentes actions sur le navire
action_F = function(x,y,direction,valeur){
  if (direction=="E"){x<-x+valeur}
  if (direction=="W"){x<-x-valeur}
  if (direction=="N"){y<-y+valeur}
  if (direction=="S"){y<-y-valeur}
  
  return(c(x,y,direction))
}


action_N = function(x,y,direction,valeur){
y<- y+valeur
return(c(x,y,direction))
}


action_S = function(x,y,direction,valeur){
  y<- y-valeur
  return(c(x,y,direction,valeur))
}


action_E = function(x,y,direction,valeur){
  x<- x+valeur
  return(c(x,y,direction))
}


action_W = function(x,y,direction,valeur){
  x<- x-valeur
  return(c(x,y,direction))
}


action_R = function(x,y,direction,valeur){
  if (direction=="E" & valeur==90){new_direction <-"S"}
  if (direction=="S" & valeur==90){new_direction <-"W"}
  if (direction=="W" & valeur==90){new_direction <-"N"}
  if (direction=="N" & valeur==90){new_direction <-"E"}
  if (direction=="E" & valeur==180){new_direction <-"W"}
  if (direction=="S" & valeur==180){new_direction <-"N"}
  if (direction=="W" & valeur==180){new_direction <-"E"}
  if (direction=="N" & valeur==180){new_direction <-"S"}
  if (direction=="E" & valeur==270){new_direction <-"N"}
  if (direction=="S" & valeur==270){new_direction <-"E"}
  if (direction=="W" & valeur==270){new_direction <-"S"}
  if (direction=="N" & valeur==270){new_direction <-"W"}
  
  return(c(x,y,new_direction))
}


action_L = function(x,y,direction,valeur){
  if (direction=="E" & valeur==90){new_direction <-"N"}
  if (direction=="S" & valeur==90){new_direction <-"E"}
  if (direction=="W" & valeur==90){new_direction <-"S"}
  if (direction=="N" & valeur==90){new_direction <-"W"}
  if (direction=="E" & valeur==180){new_direction <-"W"}
  if (direction=="S" & valeur==180){new_direction <-"N"}
  if (direction=="W" & valeur==180){new_direction <-"E"}
  if (direction=="N" & valeur==180){new_direction <-"S"}
  if (direction=="E" & valeur==270){new_direction <-"S"}
  if (direction=="S" & valeur==270){new_direction <-"W"}
  if (direction=="W" & valeur==270){new_direction <-"N"}
  if (direction=="N" & valeur==270){new_direction <-"E"}
  return(c(x,y,new_direction))
}



# calcul de l'intinéraire selon les règles
x <-0
y <-0
direction <-"E"
etape=NULL
parcours_x=0
parcours_y=0
for (i in 1: nrow(data)){
  if (data$action[i]=="F"){etape<- action_F(x,y,direction,data$valeur[i])}
  if (data$action[i]=="N"){etape<- action_N(x,y,direction,data$valeur[i])}
  if (data$action[i]=="S"){etape<- action_S(x,y,direction,data$valeur[i])}
  if (data$action[i]=="E"){etape<- action_E(x,y,direction,data$valeur[i])}
  if (data$action[i]=="W"){etape<- action_W(x,y,direction,data$valeur[i])}
  if (data$action[i]=="R"){etape<- action_R(x,y,direction,data$valeur[i])}
  if (data$action[i]=="L"){etape<- action_L(x,y,direction,data$valeur[i])}
  x<-as.numeric(etape[1])
  y<-as.numeric(etape[2])
  direction <- etape[3]
  etape=NULL
  parcours_x= c(parcours_x,x)
  parcours_y= c(parcours_y,y)
}

#Solution: distance de Manhattan à l'origine
abs(x)+abs(y)
#2057

# Parcours du navire en graphique
library(ggplot2)
df=data.frame(parcours_x,parcours_y)
ggplot(df, aes(parcours_x, parcours_y)) + 
  geom_line()


#### Part B ----

# Traitement des différentes actions sur le Navire et le Waypoint
action_F = function(x,y,valeur, waypoint_x, waypoint_y){
 x<- x+ waypoint_x*valeur
 y <-y+  waypoint_y*valeur
 return(c(x,y, waypoint_x, waypoint_y))
}


action_N = function(x,y,valeur, waypoint_x, waypoint_y){
  waypoint_y<- waypoint_y+valeur
  return(c(x,y, waypoint_x, waypoint_y))
}


action_S = function(x,y,valeur, waypoint_x, waypoint_y){
  waypoint_y<- waypoint_y-valeur
  return(c(x,y,waypoint_x, waypoint_y))
}


action_E = function(x,y,valeur, waypoint_x, waypoint_y){
  waypoint_x<- waypoint_x+valeur
  return(c(x,y, waypoint_x, waypoint_y))
}


action_W = function(x,y,valeur, waypoint_x, waypoint_y){
  waypoint_x<- waypoint_x-valeur
  return(c(x,y, waypoint_x, waypoint_y))
}


action_R = function(x,y,valeur, waypoint_x, waypoint_y){
  new_waypoint_x=0
  new_waypoint_y=0
  
if (valeur==90){
  new_waypoint_x<- waypoint_y
  new_waypoint_y<- - waypoint_x
}
 if (valeur==180){
  new_waypoint_x<- - waypoint_x
  new_waypoint_y<- - waypoint_y
}
if (valeur==270){
  new_waypoint_x<- - waypoint_y
  new_waypoint_y<-  waypoint_x
}
  
  return(c(x,y, new_waypoint_x, new_waypoint_y))
}


action_L = function(x,y,valeur, waypoint_x, waypoint_y){
  new_waypoint_x=0
  new_waypoint_y=0
  
  if (valeur==270){
    new_waypoint_x<- waypoint_y
    new_waypoint_y<- - waypoint_x
  }
  if (valeur==180){
    new_waypoint_x<- - waypoint_x
    new_waypoint_y<- - waypoint_y
  }
  if (valeur==90){
    new_waypoint_x<- - waypoint_y
    new_waypoint_y<-  waypoint_x
  }
  return(c(x,y,  new_waypoint_x, new_waypoint_y))
}



# Calcul de l'itinéraire du Navire et du Waypoint

x <-0 #coordonnees absolues du navire
y <-0 #coordonnees absolues du navire
waypoint_x <- 10 #coordonnees relatives au navire
waypoint_y <- 1  #coordonnees relatives au navire
etape=NULL
parcours_x=0
parcours_y=0

for (i in 1: nrow(data)){
  if (data$action[i]=="F"){etape<- action_F(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="N"){etape<- action_N(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="S"){etape<- action_S(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="E"){etape<- action_E(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="W"){etape<- action_W(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="R"){etape<- action_R(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  if (data$action[i]=="L"){etape<- action_L(x,y,data$valeur[i],waypoint_x, waypoint_y )}
  x<-as.numeric(etape[1])
  y<-as.numeric(etape[2])
  waypoint_x <- as.numeric(etape[3])
  waypoint_y <- as.numeric(etape[4])
  etape=NULL
  parcours_x=c(parcours_x,x)
  parcours_y=c(parcours_y,y)
}

#Solution: distance de Manhattan à l'origine
abs(x)+abs(y)
#71504

# Parcours du navire en graphique
library(ggplot2)
df=data.frame(parcours_x,parcours_y)
ggplot(df, aes(parcours_x, parcours_y)) + 
  geom_line()
