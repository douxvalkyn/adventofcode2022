################################
#-------- 24 Decembre ---------#
################################
source("script.R")
input <- read.csv("~/Projets/adventofcode_2019/input/input24.txt", header=FALSE, stringsAsFactors = F)

grille=c()
for (i in 1:dim(input)[1]){
  ligne=str_split(input[i,],'')[[1]]
  grille=rbind(grille,ligne)
}

#ajout de bordures vides autour
grille <- rbind(c('.','.','.','.','.'),grille,c('.','.','.','.','.'))
grille <- cbind (c('.','.','.','.','.','.','.'),grille,c('.','.','.','.','.','.','.'))

get_voisins=function(grille,i,j){
  voisin_N <- grille[i-1,j]
  voisin_S <- grille[i+1,j]
  voisin_E <- grille[i,j+1]
  voisin_W <- grille[i,j-1]
  return(c(voisin_N,voisin_S,voisin_E,voisin_W))
}



#regles du jeu de la vie des bugs ----

grilles<-list()
grilles[[1]]<- grille
minute=1
GO=TRUE
while (GO){
  minute=minute+1
  grilles[[minute]]<- grilles[[minute-1]] # on initialise la grille de la minute suivante
for (i in 2:6){
  for (j in 2:6){
   case=grilles[[minute-1]][i,j] 
   voisins <- get_voisins(grilles[[minute-1]],i,j)
   
   if (case=='#'){
    if (sum(voisins=='#')!=1){grilles[[minute]][i,j]<-'.'} # bug meurt si pas exactement 1 bug voisin
   }
   
   if (case=='.'){
    if (sum(voisins=='#') %in% c(1,2)){grilles[[minute]][i,j]<-'#'} #bug apparait si 1 ou 2 bugs voisin
   }
  }
}
  
 #Verifier si la nouvelle grille est déjà apparue 
  for (iter in 1:(minute-1)){
  if( all( grilles[[iter]]==grilles[[minute]]) ){ print ("STOP"); print(minute); GO=FALSE}
  
  }
}

grilles[minute]
#calcul des points de biodiversité
bio=c()
for (i in 2:6){
  for (j in 2:6){
    if (grilles[[minute]][i,j]=='#'){    bio=c(bio,2^(5*(i-2)+j-2))}
  }
}

sum(bio)
