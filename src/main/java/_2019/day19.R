################################
#-------- 19 Decembre ---------#
################################
source("script.R")

data <- unlist(read_csv("input/input19.txt",  col_names = FALSE))
programme_intcode7=function(data, inputs,indice_i,rb){
  #agrandir le data en mettant des zéros au bout
  # data <- c(data,rep(0,1000))
  i=indice_i
  GO=TRUE
  STOP="FEU VERT"
  relative_base=rb
  outputs=c()
  inputs=inputs
  while(GO){
    opcode=data[i]
    
    action=str_sub(opcode,nchar(opcode)-1,nchar(opcode))
    if (nchar(opcode)>2){mode_param1=str_sub(opcode,nchar(opcode)-2,nchar(opcode)-2) }else{mode_param1='0'}
    if (nchar(opcode)>3){mode_param2=str_sub(opcode,nchar(opcode)-3,nchar(opcode)-3) }else{mode_param2='0'}
    if (nchar(opcode)>4){mode_param3=str_sub(opcode,nchar(opcode)-4,nchar(opcode)-4) }else{mode_param3='0'}
    
    if (action %in% c('1','01')){ #addition
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1+param2 }
      pas=4
    }
    if (action %in% c('2','02')){ #multiplication
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1*param2 }
      pas=4
    }
    
    if (action %in% c('3','03')){ #input
      if (mode_param1==0){param1=data[i+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=relative_base+data[i+1]}
      if (length(inputs)==0){print("input manquant !!!")}
      data[param1+1] <- inputs[1]; #on utilise le premier input de la reserve
      pas=2
      # on supprime l'input utilisé,on conserve ceux qui restent en stock
      inputs= inputs[-1]
    }
    
    if (action %in% c('4','04')){ #output
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      # print(param1)
      outputs=c(outputs,param1)
      pas=2
      #GO=FALSE    
    }
    
    if (action %in% c('5','05')){ #jump si != 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 != 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('6','06')){ #jump si == 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 == 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('7','07')){ #test inf
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1<param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('8','08')){ #test egalité
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1==param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('9','09')){ # modif relative base
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      relative_base <-relative_base+param1
      pas=2
    }
    
    if (action==99){
      GO=FALSE
      STOP="FEU ROUGE" #met l'amplifier en arret
    }
    
    i=i+pas
    if (length(inputs)==0 & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
    if (length(inputs)>0){ if (all(is.na(inputs)) & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
      #met l'amplifier en pause si l'input est vide et qu'il va être requis
    }
  }
  
  #on sauvegarde l'output, le data, et le i pour savoir ou on est rendu; + inputs pour savoir ceux qui restent à utiliser
  #ajout d'un voyant lumineux STOP qui indique que l'amplifier cesse de fonctionner
  sortie = list(outputs,data,i,inputs, STOP,relative_base)
  return(sortie)
}


grille = matrix('_',nrow=50,ncol=50)


colonne_commencee=FALSE
t0=Sys.time()
x=10
PAS_TROUVE=TRUE

while (x <50 & PAS_TROUVE){ #on regarde colonne par colonne
y=round(x/2,0)
  GO=TRUE;
  colonne_commencee=FALSE
  while (GO) {

    res<-programme_intcode7(data,c(x,y),1,0)[[1]]
    
    if (res==0){grille[y+1,x+1]<- '.'; if (colonne_commencee){GO=FALSE} }
    if (res==1){grille[y+1,x+1]<- '#'; colonne_commencee=TRUE}
    y=y+1;
    if (x%%50==0 & y==x){print(x); t1=Sys.time(); print(t1-t0);}
    
    
    if (x>10 & y>10){
      if ( grille[y,x]=='#'){
        testx=c();testy=c()
        for (j in 1:4){
          testx[j]<-grille[y, x-j]=='#';
          testy[j]<-grille[y-j, x]=='#';
        }
        if ( all(testx) & all(testy)) {PAS_TROUVE=FALSE;print(c(x,y))}
      }
    }
  }
  x=x+1
}


