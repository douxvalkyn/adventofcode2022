####---------04 Decembre ------------------------------------------------------------------


range=seq(134792,675810)


#### Part A ----

# criteria 1: 2 digits are the same
# range2=c()
# for(i in 1:length(range)){
# if (any(duplicated(unlist(str_split(range[i],"")))) ){ range2=c(range2,range[i])}
# }

crit1=function(nombre){
  if (any(duplicated(unlist(str_split(nombre,""))))) { return(nombre)}
}

range2 <- sapply(range,crit1)
range2 <- unique(range2)
range2=range2[-1]

#criteria2: digits tjs croissants
crit2 = function(nombres){
  nombres_d <- unlist(str_split(nombres,""))
    if( (nombres_d[1]<=nombres_d[2]) & (nombres_d[2]<=nombres_d[3]) & (nombres_d[3]<=nombres_d[4]) & 
        (nombres_d[4]<=nombres_d[5]) & (nombres_d[5]<=nombres_d[6]) ) {
      return(nombres)
  }
}

range3 <- sapply(range2,crit2)
range3 <- unique(range3)
range3=range3[-1]


#### Part B ----
library(stringr)

range3[is.na(range3)] #1955 possibles

newrange=sapply(range3,critere_double) %>% unique()
newrange=newrange[-1] # 1 NULL à supprimer