package advent2021.day10;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import advent2021.outils.Outil;

public class Day10 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day10.class.getSimpleName() + "]");
		Day10 day = new Day10();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day10.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// init
		int illegal = 0;
		List<Long> scores = new ArrayList<Long>();

		for (int i = 0; i < lignes.size(); i++) { // boucle sur chaque ligne
			String ligne = lignes.get(i); // ligne en cours
			List<Character> chunksOuverts = new ArrayList<Character>();
			boolean incomplete = true;

			// System.out.println(ligne);

			// on passe en revue chaque caractere:
			for (int j = 0; j < ligne.length(); j++) {
				if (ligne.charAt(j) == '[') {
					chunksOuverts.add('[');
				}
				if (ligne.charAt(j) == '(') {
					chunksOuverts.add('(');
				}
				if (ligne.charAt(j) == '{') {
					chunksOuverts.add('{');
				}
				if (ligne.charAt(j) == '<') {
					chunksOuverts.add('<');
				}
				if (ligne.charAt(j) == ']') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '[') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ ']');
						illegal = illegal + 57;
						incomplete = false;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '[') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == ')') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '(') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ ')');
						illegal = illegal + 3;
						incomplete = false;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '(') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == '}') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '{') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ '}');
						illegal = illegal + 1197;
						incomplete = false;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '{') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == '>') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '<') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ '>');
						illegal = illegal + 25137;
						incomplete = false;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '<') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
			}

			if (incomplete) {
				// calcul du score de cette ligne
				long score = 0;
				for (int k = chunksOuverts.size() - 1; k >= 0; k--) {
					score = score * 5;
					if (chunksOuverts.get(k) == '(') {
						score = score + 1;
					}
					if (chunksOuverts.get(k) == '[') {
						score = score + 2;
					}
					if (chunksOuverts.get(k) == '{') {
						score = score + 3;
					}
					if (chunksOuverts.get(k) == '<') {
						score = score + 4;
					}
				}
				// System.out.println("score: " + score);
				scores.add(score);
			}
		} // fin boucle principale
		Collections.sort(scores);
		System.out.println("-----------------");
		System.out.println("scores: " + scores);
		System.out.println("score median: " + scores.get((scores.size() / 2)));
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day10.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// init
		List<Character> chunksOuverts = new ArrayList<Character>();
		int illegal = 0;

		for (int i = 0; i < lignes.size(); i++) { // boucle sur chque ligne
			String ligne = lignes.get(i); // ligne en cours
			// System.out.println(ligne);

			// on passe en revue chaque caractere:
			for (int j = 0; j < ligne.length(); j++) {
				if (ligne.charAt(j) == '[') {
					chunksOuverts.add('[');
				}
				if (ligne.charAt(j) == '(') {
					chunksOuverts.add('(');
				}
				if (ligne.charAt(j) == '{') {
					chunksOuverts.add('{');
				}
				if (ligne.charAt(j) == '<') {
					chunksOuverts.add('<');
				}
				if (ligne.charAt(j) == ']') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '[') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ ']');
						illegal = illegal + 57;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '[') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == ')') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '(') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ ')');
						illegal = illegal + 3;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '(') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == '}') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '{') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ '}');
						illegal = illegal + 1197;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '{') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
				if (ligne.charAt(j) == '>') {
					if (chunksOuverts.get(chunksOuverts.size() - 1) != '<') {
						// System.out.println("error " + j+ " expected closure of " +chunksOuverts.get(chunksOuverts.size()-1)+" but found "+ '>');
						illegal = illegal + 25137;
						break;
					}
					if (chunksOuverts.get(chunksOuverts.size() - 1) == '<') {
						chunksOuverts.remove(chunksOuverts.size() - 1);
					}
				}
			}

		} // fin boucle principale
		System.out.println("illegal: " + illegal);
	}

}
