package advent2021.day24;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day24 {

	private static int cptInput = 0;
	private static long w = 0;
	private static long x = 0;
	private static long y = 0;
	private static long z = 0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day24.class.getSimpleName() + "]");
		Day24 day = new Day24();
		LocalDateTime start = LocalDateTime.now();
		day.run();
		// day.run1bis();
		// day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// tester toutes les possiblités en réduisant les choix selon les contraintes des regles ----
	public void run() {
		String filename = "src/main/resources/advent2021/" + Day24.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		long nombreInitial = 9999;
		// import des input X, Y, Z
		List<Integer> listeX = new ArrayList<>();
		List<Integer> listeY = new ArrayList<>();
		List<Integer> listeZ = new ArrayList<>();
		List<Integer> listeW = convertirNombreInitialDansInput(nombreInitial);
		for (int pas = 0; pas < 14; pas++) {
			String xxx = StringUtils.split(lignes.get(5 + 18 * pas), " ")[2];
			String yyy = StringUtils.split(lignes.get(15 + 18 * pas), " ")[2];
			String zzz = StringUtils.split(lignes.get(4 + 18 * pas), " ")[2];
			listeX.add(Integer.parseInt(xxx));
			listeY.add(Integer.parseInt(yyy));
			listeZ.add(Integer.parseInt(zzz));
		}

		// map de sauvegarde
		Map<Long, Long> map = new HashMap<>();

		// Les iterations 1 à 4 sont des multiplications
		while (nombreInitial >= 1111) {
			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 4;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
			nombreInitial--;
			boolean boucle = true;
			while (boucle) {
				boolean checkConditionsNombre = verifierConditionsNombre(nombreInitial);
				if (checkConditionsNombre) {
					boucle = false;
				} else {
					nombreInitial--;
				}
			}
		}

		// iteration 5=division
		// contrainte 1: iteration 5: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-12=w est possible
		// ie: z mod 26 est compris entre 13 et 21
		// 9puissance4=6561 possibilités pour les 4 premiers digits
		Map<Long, Long> mapTemp = new HashMap<>();

		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			if (resZ % 26 >= 13 & resZ % 26 <= 21) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// avec la contrainte de l'iteration 5, on reduit à 5102 possibilites
		// Cela va imposer le digit suivant pour chaque possibilité
		// ( par exemple pour z=287605, si on veut apres l'iteration 5 revenir à 11061, il faut que w=7

		// calcul du digit5 pour chaque possibilité
		List<Long> digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(4);
			Long cinqPremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(cinqPremiersDigits);
		}

		// On continue avec l'iteration 6 qui est une multiplication
		// pour chaque cas, on ajoute 1 digit allant de 1 à 9
		// map de sauvegarde
		map = new HashMap<>();

		// calcul des z à l'iteration 6
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);
			for (int j = 1; j <= 9; j++) {
				nombreInitial = Long.parseLong(digits.get(i) + "" + j);

				// init variables x, y, z, w
				long x = 0;
				long y = 0;
				long z = 0;
				long w = 0;
				listeW = convertirNombreInitialDansInput(nombreInitial);

				int nbIter = 6;
				z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
				map.put(nombreInitial, z);
			}
		}

		// Reste: 45926 cas
		// Iteration 7 est une division
		// contrainte 2: iteration 7: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-2=w est possible
		// ie: z mod 26 est compris entre 3 et 11

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			if (resZ % 26 >= 3 & resZ % 26 <= 11) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste 15308 cas
		// calcul du digit7 pour chaque cas
		map = new HashMap<>();
		digits = new ArrayList<>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(6);
			Long septPremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(septPremiersDigits);
		}

		// calcul des z à l'iteration 7
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 7;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
		}

		// Iteration 8 est une division
		// contrainte 3: iteration 8: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-11=w est possible
		// ie: z mod 26 est compris entre 12 et 20

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			long test = resZ % 26;
			if (resZ % 26 >= 12 & resZ % 26 <= 20) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste 1701 cas
		// calcul du digit8 pour chaque possibilité
		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(7);
			Long huitPremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(huitPremiersDigits);
		}

		// Iterations 9 et 10 sont des multiplications
		// pour chaque cas, on ajoute 1 digit allant de 1 à 9

		// calcul des z à l'iteration 9
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);
			for (int j = 1; j <= 9; j++) {
				nombreInitial = Long.parseLong(digits.get(i) + "" + j);

				// init variables x, y, z, w
				long x = 0;
				long y = 0;
				long z = 0;
				long w = 0;
				listeW = convertirNombreInitialDansInput(nombreInitial);

				int nbIter = 9;
				z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
				map.put(nombreInitial, z);
			}
		}

		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : map.entrySet()) {
			digits.add(set.getKey());
		}

		// calcul des z à l'iteration 10
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);
			for (int j = 1; j <= 9; j++) {
				nombreInitial = Long.parseLong(digits.get(i) + "" + j);

				// init variables x, y, z, w
				long x = 0;
				long y = 0;
				long z = 0;
				long w = 0;
				listeW = convertirNombreInitialDansInput(nombreInitial);

				int nbIter = 10;
				z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
				map.put(nombreInitial, z);
			}
		}

		// map contient 153089 cas
		// Les iterations 11, 12, 13, 14 sont des divisions ------

		// Iteration 11 est une division
		// contrainte 4: iteration 11: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-0=w est possible
		// ie: z mod 26 est compris entre 1 et 9

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			long test = resZ % 26;
			if (resZ % 26 >= 1 & resZ % 26 <= 9) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste 91853 cas
		// calcul du digit11 pour chaque possibilité
		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(10);
			Long onzePremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(onzePremiersDigits);
		}

		// calcul des z à l'iteration 11
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 11;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
		}

		// Iteration 12 est une division
		// contrainte 4: iteration 11: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-12=w est possible
		// ie: z mod 26 est compris entre 13 et 21

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			long test = resZ % 26;
			if (resZ % 26 >= 13 & resZ % 26 <= 21) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste 61235 cas
		// calcul du digit12 pour chaque possibilité
		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(11);
			Long onzePremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(onzePremiersDigits);
		}

		// calcul des z à l'iteration 12
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 12;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
		}

		// Iteration 13 est une division
		// contrainte 6: iteration 13: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-13=w est possible
		// ie: z mod 26 est compris entre 14 et 22

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			long test = resZ % 26;
			if (resZ % 26 >= 14 & resZ % 26 <= 22) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste XXXX cas
		// calcul du digit13 pour chaque possibilité
		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(12);
			Long onzePremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(onzePremiersDigits);
		}

		// calcul des z à l'iteration 13
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 13;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
		}

		// Iteration 14 est une division
		// contrainte 7: iteration 14: on veut que z soit divisé:
		// il faut verifier parmi toutes les possibilités, lesquelles respectent la contrainte zmod26-6=w est possible
		// ie: z mod 26 est compris entre 7 et 15

		mapTemp = new HashMap<>();
		for (Entry<Long, Long> set : map.entrySet()) {
			long resZ = set.getValue();
			long test = resZ % 26;
			if (resZ % 26 >= 7 & resZ % 26 <= 15) {
				mapTemp.put(set.getKey(), resZ);
			}
		}

		// reste 21167 cas
		// calcul du digit13 pour chaque possibilité
		digits = new ArrayList<Long>();
		for (Entry<Long, Long> set : mapTemp.entrySet()) {
			long digit = set.getValue() % 26 + listeX.get(13);
			Long onzePremiersDigits = Long.parseLong("" + set.getKey() + digit);
			digits.add(onzePremiersDigits);
		}

		// calcul des z à l'iteration 14
		map = new HashMap<>();
		for (int i = 0; i < digits.size(); i++) {
			nombreInitial = digits.get(i);

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;
			listeW = convertirNombreInitialDansInput(nombreInitial);

			int nbIter = 14;
			z = monadSimple(listeX, listeY, listeZ, listeW, z, nbIter);
			map.put(nombreInitial, z);
		}

		// Il y a 21176 cas qui font z=0 !!!

		// recherche du max
		long max = 0;
		for (Entry<Long, Long> set : map.entrySet()) {
			if (set.getKey() > max) {
				max = set.getKey();
				System.out.println("max: " + max);
			}
		}

		// recherche du min
		long min = Long.MAX_VALUE;
		for (Entry<Long, Long> set : map.entrySet()) {
			if (set.getKey() < min) {
				min = set.getKey();
				System.out.println("min: " + min);
			}
		}
	}

	// Algorithme simplifié de Monad, jusqu'à l'iteration nbIter
	public long monadSimple(List<Integer> listeX, List<Integer> listeY, List<Integer> listeZ, List<Integer> listeW, long z, int nbIter) {

		long x;
		long w;
		for (int iter = 1; iter < (nbIter + 1); iter++) {
			w = listeW.get(iter - 1);
			x = z % 26 + listeX.get(iter - 1);
			if (x == w) {
				z = (int) z / (listeZ.get(iter - 1));
			} else {
				z = z * 26 + w + listeY.get(iter - 1);
			}
		}
		return z;
	}

	// run1 bis ----Essai de toutes les possibilités: 9^14 > trop long !
	public void run1bis() {
		String filename = "src/main/resources/advent2021/" + Day24.class.getSimpleName() + "d.txt";
		List<String> lignes = Outil.importationString(filename);

		boolean stop = false;
		long nombreInitial = 99999999999999L;
		while (!stop) {

			// import des input X, Y, Z
			List<Integer> listeX = new ArrayList<>();
			List<Integer> listeY = new ArrayList<>();
			List<Integer> listeZ = new ArrayList<>();
			List<Integer> listeW = convertirNombreInitialDansInput(nombreInitial);
			for (int pas = 0; pas < 14; pas++) {
				String xxx = StringUtils.split(lignes.get(5 + 18 * pas), " ")[2];
				String yyy = StringUtils.split(lignes.get(15 + 18 * pas), " ")[2];
				String zzz = StringUtils.split(lignes.get(4 + 18 * pas), " ")[2];
				listeX.add(Integer.parseInt(xxx));
				listeY.add(Integer.parseInt(yyy));
				listeZ.add(Integer.parseInt(zzz));
			}

			// init variables x, y, z, w
			long x = 0;
			long y = 0;
			long z = 0;
			long w = 0;

			// boucle principale
			outerloop: for (int iter = 1; iter < 15; iter++) {
				w = listeW.get(iter - 1);
				x = z % 26 + listeX.get(iter - 1);
				if (x == w) {
					z = (int) z / (listeZ.get(iter - 1));
				} else {
					x = 1;
					z = z * 26 + w + listeY.get(iter - 1);
				}

				// quitter la boucle si z dépasse une valeur possible pour arriver à z=0 à la fin
				long prod = 1;
				for (int j = 14; j > iter; j--) {
					prod = prod * listeZ.get(j - 1);
				}
				if (z >= prod) {
					break outerloop;
				}
			}

			if (z == 0) {
				stop = true;
				System.out.println(nombreInitial);
			}

			nombreInitial--;
			boolean boucle = true;
			while (boucle) {
				boolean checkConditionsNombre = verifierConditionsNombre(nombreInitial);
				if (checkConditionsNombre) {
					boucle = false;
				} else {
					nombreInitial--;
				}
			}
		}
	}

	// run1 bis ----Essai de toutes les possibilités: 9^14 > trop long !
	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day24.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		boolean stop = false;
		long nombreInitial = 99999999999999L;
		while (!stop) {

			List<Integer> input = convertirNombreInitialDansInput(nombreInitial);
			monad(lignes, input);
			if (z == 0) {
				System.out.println("Numero obtenu: " + z);
				stop = true;
			}
			z = 0;
			y = 0;
			x = 0;
			w = 0;
			cptInput = 0;
			nombreInitial--;
			boolean boucle = true;
			while (boucle) {
				boolean checkConditionsNombre = verifierConditionsNombre(nombreInitial);
				if (checkConditionsNombre) {
					boucle = false;
				} else {
					nombreInitial--;
				}
			}
		}
	}

	// verifie si le nombre ne contient aucun 0
	private boolean verifierConditionsNombre(long nombreInitial) {
		boolean checkConditionsNombre = true;
		String nombreInitialString = "" + nombreInitial;
		for (int i = 0; i < nombreInitialString.length(); i++) {
			String carac = "" + nombreInitialString.charAt(i);
			if (Integer.parseInt(carac) == 0) {
				checkConditionsNombre = false;
			}
		}
		return checkConditionsNombre;
	}

	private List<Integer> convertirNombreInitialDansInput(long nombreInitial) {
		List<Integer> input = new ArrayList<>();
		String nombreInitialString = "" + nombreInitial;
		for (int i = 0; i < nombreInitialString.length(); i++) {
			input.add(Integer.parseInt("" + (nombreInitialString).charAt(i)));
		}
		return input;
	}

	// premier algo monad, au plus pres de l'enoncé
	public void monad(List<String> lignes, List<Integer> input) {
		// ------ Remplir l'input ici: --------

		// System.out.println("input: "+input);
		// ------------------------------------

		for (String instruction : lignes) {
			long varX = x;
			long varY = y;
			long varZ = z;
			long varW = w;

			String operation = StringUtils.substring(instruction, 0, 3);
			String calcul = StringUtils.substring(instruction, 4);

			switch (operation) {
				case "inp":
					inp(calcul, input.get(cptInput));
					break;
				case "add":
					add(StringUtils.split(calcul, " ")[0], StringUtils.split(calcul, " ")[1]);
					break;
				case "mul":
					mul(StringUtils.split(calcul, " ")[0], StringUtils.split(calcul, " ")[1]);
					break;
				case "div":
					div(StringUtils.split(calcul, " ")[0], StringUtils.split(calcul, " ")[1]);
					break;
				case "mod":
					mod(StringUtils.split(calcul, " ")[0], StringUtils.split(calcul, " ")[1]);
					break;
				case "eql":
					eql(StringUtils.split(calcul, " ")[0], StringUtils.split(calcul, " ")[1]);
					break;
				default:
					System.out.println("Erreur");
			}
		}
	}

	// definition des 6 fonctions de monad
	private void mod(String var1, String var2) {
		if (var1.equals("w") & var2.equals("w")) {
			if (w >= 0 & w > 0) {
				w = w % w;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("w") & var2.equals("x")) {
			if (w >= 0 & x > 0) {
				w = w % x;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("w") & var2.equals("y")) {
			if (w >= 0 & y > 0) {
				w = w % y;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("w") & var2.equals("z")) {
			if (w >= 0 & z > 0) {
				w = w % z;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("w") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (w >= 0 & Integer.parseInt(var2) > 0) {
				w = w % Integer.parseInt(var2);
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("x") & var2.equals("w")) {
			if (x >= 0 & w > 0) {
				x = x % w;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("x") & var2.equals("x")) {
			if (x >= 0 & x > 0) {
				x = x % x;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("x") & var2.equals("y")) {
			if (x >= 0 & y > 0) {
				x = x % y;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("x") & var2.equals("z")) {
			if (x >= 0 & z > 0) {
				x = x % z;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("x") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (x >= 0 & Integer.parseInt(var2) > 0) {
				x = x % Integer.parseInt(var2);
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("y") & var2.equals("w")) {
			if (y >= 0 & w > 0) {
				y = y % w;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("y") & var2.equals("x")) {
			if (y >= 0 & x > 0) {
				y = y % x;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("y") & var2.equals("y")) {
			if (y >= 0 & y > 0) {
				y = y % y;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("y") & var2.equals("z")) {
			if (y >= 0 & z > 0) {
				y = y % z;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("y") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (y >= 0 & Integer.parseInt(var2) > 0) {
				y = y % Integer.parseInt(var2);
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("z") & var2.equals("w")) {
			if (z >= 0 & w > 0) {
				z = w % w;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("z") & var2.equals("x")) {
			if (z >= 0 & x > 0) {
				z = x % x;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("z") & var2.equals("y")) {
			if (z >= 0 & y > 0) {
				z = y % y;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("z") & var2.equals("z")) {
			if (z >= 0 & z > 0) {
				z = z % z;
			} else {
				System.out.println("Erreur modulo");
			}
		}
		if (var1.equals("z") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (z >= 0 & Integer.parseInt(var2) > 0) {
				z = z % Integer.parseInt(var2);
			} else {
				System.out.println("Erreur modulo");
			}
		}
	}

	// div a b - Divide the value of a by the value of b,
	// truncate the result to an integer, then store the result in variable a.
	// (Here, "truncate" means to round the value toward zero.)
	private void div(String var1, String var2) {
		if (var1.equals("w") & var2.equals("w")) {
			if (w != 0) {
				w = (int) w / w;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("w") & var2.equals("x")) {
			if (x != 0) {
				w = (int) w / x;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("w") & var2.equals("y")) {
			if (y != 0) {
				w = (int) w / y;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("w") & var2.equals("z")) {
			if (z != 0) {
				w = (int) w / z;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("w") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (Integer.parseInt(var2) != 0) {
				w = (int) w / Integer.parseInt(var2);
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("x") & var2.equals("w")) {
			if (w != 0) {
				x = (int) x / w;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("x") & var2.equals("x")) {
			if (x != 0) {
				x = (int) x / x;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("x") & var2.equals("y")) {
			if (y != 0) {
				x = (int) x / y;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("x") & var2.equals("z")) {
			if (z != 0) {
				x = (int) x / z;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("x") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (Integer.parseInt(var2) != 0) {
				x = (int) x / Integer.parseInt(var2);
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("y") & var2.equals("w")) {
			if (w != 0) {
				y = (int) y / w;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("y") & var2.equals("x")) {
			if (x != 0) {
				y = (int) y / x;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("y") & var2.equals("y")) {
			if (y != 0) {
				y = (int) y / y;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("y") & var2.equals("z")) {
			if (z != 0) {
				y = (int) y / z;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("y") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (Integer.parseInt(var2) != 0) {
				y = (int) y / Integer.parseInt(var2);
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("z") & var2.equals("w")) {
			if (w != 0) {
				z = (int) w / w;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("z") & var2.equals("x")) {
			if (x != 0) {
				z = (int) x / x;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("z") & var2.equals("y")) {
			if (y != 0) {
				z = (int) y / y;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("z") & var2.equals("z")) {
			if (z != 0) {
				z = (int) z / z;
			} else {
				System.out.println("Erreur division par zero");
			}
		}
		if (var1.equals("z") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (Integer.parseInt(var2) != 0) {
				z = (int) z / Integer.parseInt(var2);
			} else {
				System.out.println("Erreur division par zero");
			}
		}
	}

	private void eql(String var1, String var2) {
		if (var1.equals("w") & var2.equals("w")) {
			if (w == w) {
				w = 1;
			} else {
				w = 0;
			}
		}
		if (var1.equals("w") & var2.equals("x")) {
			if (w == x) {
				w = 1;
			} else {
				w = 0;
			}
		}
		if (var1.equals("w") & var2.equals("y")) {
			if (w == y) {
				w = 1;
			} else {
				w = 0;
			}
		}
		if (var1.equals("w") & var2.equals("z")) {
			if (w == z) {
				w = 1;
			} else {
				w = 0;
			}
		}
		if (var1.equals("w") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (w == Integer.parseInt(var2)) {
				w = 1;
			} else {
				w = 0;
			}
		}
		if (var1.equals("x") & var2.equals("w")) {
			if (x == w) {
				x = 1;
			} else {
				x = 0;
			}
		}
		if (var1.equals("x") & var2.equals("x")) {
			if (x == x) {
				x = 1;
			} else {
				x = 0;
			}
		}
		if (var1.equals("x") & var2.equals("y")) {
			if (x == y) {
				x = 1;
			} else {
				x = 0;
			}
		}
		if (var1.equals("x") & var2.equals("z")) {
			if (x == z) {
				x = 1;
			} else {
				x = 0;
			}
		}
		if (var1.equals("x") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (x == Integer.parseInt(var2)) {
				x = 1;
			} else {
				x = 0;
			}
		}
		if (var1.equals("y") & var2.equals("w")) {
			if (y == w) {
				y = 1;
			} else {
				y = 0;
			}
		}
		if (var1.equals("y") & var2.equals("x")) {
			if (y == x) {
				y = 1;
			} else {
				y = 0;
			}
		}
		if (var1.equals("y") & var2.equals("y")) {
			if (y == y) {
				y = 1;
			} else {
				y = 0;
			}
		}
		if (var1.equals("y") & var2.equals("z")) {
			if (y == z) {
				y = 1;
			} else {
				y = 0;
			}
		}
		if (var1.equals("y") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (y == Integer.parseInt(var2)) {
				y = 1;
			} else {
				y = 0;
			}
		}
		if (var1.equals("z") & var2.equals("w")) {
			if (z == w) {
				z = 1;
			} else {
				z = 0;
			}
		}
		if (var1.equals("z") & var2.equals("x")) {
			if (z == x) {
				z = 1;
			} else {
				z = 0;
			}
		}
		if (var1.equals("z") & var2.equals("y")) {
			if (z == y) {
				z = 1;
			} else {
				z = 0;
			}
		}
		if (var1.equals("z") & var2.equals("z")) {
			if (z == z) {
				z = 1;
			} else {
				z = 0;
			}
		}
		if (var1.equals("z") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			if (z == Integer.parseInt(var2)) {
				z = 1;
			} else {
				z = 0;
			}
		}
	}

	private void add(String var1, String var2) {
		if (var1.equals("w") & var2.equals("w")) {
			w = w + w;
		}
		if (var1.equals("w") & var2.equals("x")) {
			w = w + x;
		}
		if (var1.equals("w") & var2.equals("y")) {
			w = w + y;
		}
		if (var1.equals("w") & var2.equals("z")) {
			w = w + z;
		}
		if (var1.equals("w") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			w = w + Integer.parseInt(var2);
		}
		if (var1.equals("x") & var2.equals("w")) {
			x = x + w;
		}
		if (var1.equals("x") & var2.equals("x")) {
			x = x + x;
		}
		if (var1.equals("x") & var2.equals("y")) {
			x = x + y;
		}
		if (var1.equals("x") & var2.equals("z")) {
			x = x + z;
		}
		if (var1.equals("x") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			x = x + Integer.parseInt(var2);
		}
		if (var1.equals("y") & var2.equals("w")) {
			y = y + w;
		}
		if (var1.equals("y") & var2.equals("x")) {
			y = y + x;
		}
		if (var1.equals("y") & var2.equals("y")) {
			y = y + y;
		}
		if (var1.equals("y") & var2.equals("z")) {
			y = y + z;
		}
		if (var1.equals("y") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			y = y + Integer.parseInt(var2);
		}
		if (var1.equals("z") & var2.equals("w")) {
			z = z + w;
		}
		if (var1.equals("z") & var2.equals("x")) {
			z = z + x;
		}
		if (var1.equals("z") & var2.equals("y")) {
			z = z + y;
		}
		if (var1.equals("z") & var2.equals("z")) {
			z = z + z;
		}
		if (var1.equals("z") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			z = z + Integer.parseInt(var2);
		}
	}

	private void mul(String var1, String var2) {
		if (var1.equals("w") & var2.equals("w")) {
			w = w * w;
		}
		if (var1.equals("w") & var2.equals("x")) {
			w = w * x;
		}
		if (var1.equals("w") & var2.equals("y")) {
			w = w * y;
		}
		if (var1.equals("w") & var2.equals("z")) {
			w = w * z;
		}
		if (var1.equals("w") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			w = w * Integer.parseInt(var2);
		}
		if (var1.equals("x") & var2.equals("w")) {
			x = x * w;
		}
		if (var1.equals("x") & var2.equals("x")) {
			x = x * x;
		}
		if (var1.equals("x") & var2.equals("y")) {
			x = x * y;
		}
		if (var1.equals("x") & var2.equals("z")) {
			x = x * z;
		}
		if (var1.equals("x") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			x = x * Integer.parseInt(var2);
		}
		if (var1.equals("y") & var2.equals("w")) {
			y = y * w;
		}
		if (var1.equals("y") & var2.equals("x")) {
			y = y * x;
		}
		if (var1.equals("y") & var2.equals("y")) {
			y = y * y;
		}
		if (var1.equals("y") & var2.equals("z")) {
			y = y * z;
		}
		if (var1.equals("y") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			y = y * Integer.parseInt(var2);
		}
		if (var1.equals("z") & var2.equals("w")) {
			z = z * w;
		}
		if (var1.equals("z") & var2.equals("x")) {
			z = z * x;
		}
		if (var1.equals("z") & var2.equals("y")) {
			z = z * y;
		}
		if (var1.equals("z") & var2.equals("z")) {
			z = z * z;
		}
		if (var1.equals("z") & !var2.equals("w") & !var2.equals("x") & !var2.equals("y") & !var2.equals("z")) {
			z = z * Integer.parseInt(var2);
		}
	}

	private void inp(String variable, Integer nb) {
		if (variable.equals("w")) {
			w = nb;
		}
		if (variable.equals("x")) {
			x = nb;
		}
		if (variable.equals("y")) {
			y = nb;
		}
		if (variable.equals("z")) {
			z = nb;
		}
		cptInput++;
	}

}
