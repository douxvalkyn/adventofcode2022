package advent2021.day07;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2021.outils.Outil;

public class Day07 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day07.class.getSimpleName() + "]");
		Day07 day = new Day07();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2021/" + Day07.class.getSimpleName() + ".txt";
		List<Integer> positions = Outil.importationIntegerSeparateurVirgule(filename);

		// calcul de la mediane qui sera la position initiale lors de la recherche
		int median = (positions.get(positions.size() / 2) + positions.get(positions.size() / 2 - 1)) / 2;
		System.out.println("median: " + median);

		// recherche autour de la mediane
		long minimum = Integer.MAX_VALUE;
		for (int i = 0; i < 100000; i++) {
			long candidat;
			if (i % 2 == 0) {
				candidat = median + i;
			} else {
				candidat = median - i;
			}

			// calcul de la somme des distances au nombre candidat
			Long test = positions.stream().map(l -> ((Math.abs(l - candidat)) * (Math.abs(l - candidat) + 1)) / 2).reduce((long) 0, (a, b) -> a + b);

			// si la valeur est plus petite au min, on sauvegarde ce nouveau min
			if (test < minimum) {
				minimum = test;
				System.out.println("minimum: " + test + " ....best position: " + candidat);
			}
		}
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2021/" + Day07.class.getSimpleName() + ".txt";
		List<Integer> positions = Outil.importationIntegerSeparateurVirgule(filename);

		// calcul de la mediane qui sera la position initiale lors de la recherche
		int median = (positions.get(positions.size() / 2) + positions.get(positions.size() / 2 - 1)) / 2;
		System.out.println("median: " + median);

		// recherche autour de la mediane, on teste les candidats un par un
		int minimum = Integer.MAX_VALUE;
		for (int i = 0; i < 100000; i++) {
			int candidat;
			if (i % 2 == 0) {
				candidat = median + i;
			} else {
				candidat = median - i;
			}

			// calcul de la somme des distances au nombre candidat
			Integer test = positions.stream().map(l -> Math.abs(l - candidat)).reduce(0, (a, b) -> a + b);

			// si la valeur est plus petite au min, on sauvegarde ce nouveau min
			if (test < minimum) {
				minimum = test;
				System.out.println("minimum trouvé!: " + test);
			}
		}
	}
}
