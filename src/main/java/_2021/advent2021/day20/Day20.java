package advent2021.day20;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Case;
import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day20 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day20.class.getSimpleName() + "]");
		Day20 day = new Day20();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day20.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		// String
		// algorithm="..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#";
		String algorithm = "##..##.#.....#...###.#.##.##..#.####..###.##..#.#.#.#...###..###.##..#.#.#..#..######..#..#.##.##..###.......#..##.##.#.######..#.####.##.#.####.#.###...###.##.###.#.##.#.###.#..###.###.##.#.#..####.##....#.#..####.###.###..##..######.#.#..###..#...####..#.#..###.###....#.#.#...####.#...#....#.##.#.##.#.###.#..#.##...#.##.#..##.#..##..#..#..##.#..##..#....##.##..##.#....##..#..#...#.##.#....####.##.#.#.#...##.#.######.#..##.##.#.#......#.####.#.#.#.##..##.##.##..##..###..####..##.#..##.##.###.####..#..##...";

		// INIT-----
		int xMax = 220;
		int yMax = 220;
		Grille input = new Grille(xMax, yMax);
		creationGrille(lignes, input, 60);

		// au depart mettre tous les pixels autour de l'input en black .
		for (Case cell : input.getCases()) {
			if (cell.getEtat() == "_") {
				cell.setEtat(".");
			}
		}

		// System.out.println("-------------------input-------------");
		// input.affichageEtat();
		// System.out.println("-------------------input-------------");

		int cptDiese = 0; // pour compter les pixels à la fin des etapes
		int nbStep = 50; // nb d'etapes a realiser
		int nbStepPlussUn = nbStep + 1;

		// Boucle sur le nb d'iterations-----
		for (int step = 1; step < nbStepPlussUn; step++) {
			cptDiese = 0;

			// creation grille output
			Grille output = new Grille(xMax, yMax);
			creationGrille(lignes, output, 60);
			// mettre toutes les cases de l'output en black pour le moment
			for (Case cell2 : output.getCases()) {
				cell2.setEtat(".");
			}

			// regarder les cases de l'input
			// puis regarder ses voisins et decoder l'algorithme
			for (Case cell : input.getCases()) {
				if (cell.getX() > 0 & cell.getY() > 0 & cell.getX() < (xMax - 1) & cell.getY() < (yMax - 1)) {
					List<Case> voisins = cell.getCasesAdjacentesQueen(input);

					// combiner les 8voisins et la cellule elle meme
					String combinaison = voisins.get(4).getEtat() + voisins.get(0).getEtat() + voisins.get(5).getEtat() + voisins.get(3).getEtat() + cell.getEtat() + voisins.get(2).getEtat()
						+ voisins.get(6).getEtat() + voisins.get(1).getEtat() + voisins.get(7).getEtat();

					// convertir la combinaison en binaire
					combinaison = StringUtils.replace(combinaison, ".", "0");
					combinaison = StringUtils.replace(combinaison, "#", "1");
					int combinaisonDecimal = Integer.parseInt(combinaison, 2);

					// lecture dans l'algorithme du caractere concerné
					String caractere = "" + algorithm.charAt(combinaisonDecimal);

					// inserer ce caractere a la bonne place dans la grille output
					int x = cell.getX();
					int y = cell.getY();
					output.getCase(x, y).setEtat(caractere);

					// correction pour le reel, pour les bords: step impaire c'est # step paire c'est .
					for (Case cell2 : output.getCases()) {
						if (step % 2 == 0) {
							if (cell2.getX() == 0 | cell2.getX() == (xMax - 1) | cell2.getY() == 0 | cell2.getY() == (yMax - 1))
								cell2.setEtat(".");
						}
						if (step % 2 == 1) {
							if (cell2.getX() == 0 | cell2.getX() == (xMax - 1) | cell2.getY() == 0 | cell2.getY() == (yMax - 1))
								cell2.setEtat("#");
						}
					}
				}
			}

			// modifier input pour preparer prochaine etape
			input = output;

			System.out.println("-------output-------------");
			System.out.println("-------step: " + step + "-------");
			if (step == nbStep) {
				output.affichageEtat();
			}

			// comptages des pixels
			for (Case cell : output.getCases()) { // filtrer en enlevant les bords qui sont faux.
				if (cell.getX() > 1 & cell.getX() < (xMax - 2) & cell.getY() > 1 & cell.getY() < (yMax - 2)) {
					if (cell.getEtat().equals("#")) {
						cptDiese++;
					}
				}
			}
		} // fin boucle sur les step

		// compter le nombre de cases #
		System.out.println("nb pixels: " + cptDiese);

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day20.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		// String
		// algorithm="..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#";
		String algorithm = "##..##.#.....#...###.#.##.##..#.####..###.##..#.#.#.#...###..###.##..#.#.#..#..######..#..#.##.##..###.......#..##.##.#.######..#.####.##.#.####.#.###...###.##.###.#.##.#.###.#..###.###.##.#.#..####.##....#.#..####.###.###..##..######.#.#..###..#...####..#.#..###.###....#.#.#...####.#...#....#.##.#.##.#.###.#..#.##...#.##.#..##.#..##..#..#..##.#..##..#....##.##..##.#....##..#..#...#.##.#....####.##.#.#.#...##.#.######.#..##.##.#.#......#.####.#.#.#.##..##.##.##..##..###..####..##.#..##.##.###.####..#..##...";

		int xMax = 150;
		int yMax = 150;
		Grille input = new Grille(xMax, yMax);
		creationGrille(lignes, input, 10);

		// mettre tous les pixels autour de l'input en black .
		for (Case cell : input.getCases()) {
			if (cell.getEtat() == "_") {
				cell.setEtat(".");
			}
		}

		System.out.println("-------------------input-------------");
		input.affichageEtat();
		System.out.println("-------------------input-------------");

		int cptDiese = 0;

		// Boucle sur le nb d'iterations-----
		for (int step = 1; step < 4; step++) {
			cptDiese = 0;

			// creation grille output
			Grille output = new Grille(xMax, yMax);
			creationGrille(lignes, output, 10);
			// mettre toutes les cases de l'output en black pour le moment
			for (Case cell2 : output.getCases()) {
				cell2.setEtat(".");
			}

			// regarder les cases de l'input
			// puis regarder ses voisins et decoder l'algorithme
			for (Case cell : input.getCases()) {
				if (cell.getX() > 0 & cell.getY() > 0 & cell.getX() < (xMax - 1) & cell.getY() < (yMax - 1)) {
					List<Case> voisins = cell.getCasesAdjacentesQueen(input);

					// combiner les 8voisins et la cellule elle meme
					String combinaison = voisins.get(4).getEtat() + voisins.get(0).getEtat() + voisins.get(5).getEtat() + voisins.get(3).getEtat() + cell.getEtat() + voisins.get(2).getEtat()
						+ voisins.get(6).getEtat() + voisins.get(1).getEtat() + voisins.get(7).getEtat();

					// convertir la combinaison en binaire
					combinaison = StringUtils.replace(combinaison, ".", "0");
					combinaison = StringUtils.replace(combinaison, "#", "1");
					int combinaisonDecimal = Integer.parseInt(combinaison, 2);

					// lecture dans l'algorithme du caractere concerné
					String caractere = "" + algorithm.charAt(combinaisonDecimal);

					// inserer ce caractere a la bonne place dans la grille output
					int x = cell.getX();
					int y = cell.getY();
					output.getCase(x, y).setEtat(caractere);

					// correction pour les bords (valable que pour le reel, pas pour le sample): step impaire c'est # , step paire c'est .
					// for (Case cell2: output.getCases()) {
					// if (step%2==0) {
					// if (cell2.getX()==0 | cell2.getX()==119 | cell2.getY()==0 | cell2.getY()==119 )
					// cell2.setEtat(".");
					// }
					// if (step%2==1) {
					// if (cell2.getX()==0 | cell2.getX()==119 | cell2.getY()==0 | cell2.getY()==119 )
					// cell2.setEtat("#");
					// }
					// }

				}
			}

			// modifier input pour preparer prochaine etape
			input = output;

			System.out.println("-------output-------------");
			System.out.println("-------step: " + step + "-------");
			if (step == 2) {
				output.affichageEtat();
			}
			System.out.println("-------output-------------");

			for (Case cell : output.getCases()) { // filtrer en enlevant les bords qui sont faux.
				if (cell.getX() > 1 & cell.getX() < (xMax - 2) & cell.getY() > 1 & cell.getY() < (yMax - 2)) {
					if (cell.getEtat().equals("#")) {
						cptDiese++;
					}
				}
			}
		} // fin boucle sur les step

		// compter le nombre de cases #
		System.out.println("nb pixels: " + cptDiese);

	}

	private void creationGrille(List<String> lignes, Grille grille, int decalage) {
		// decalage de 10 en x et y
		int x = decalage;
		int y = decalage;
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i + x, j + y).setEtat("" + lignes.get(j).charAt(i));
			}
		}
		// grille.affichageEtat();
	}
}
