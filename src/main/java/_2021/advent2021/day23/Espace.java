package advent2021.day23;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Espace {

	private String name;
	private String type;
	private Amphipode amphipode;
	private List<String> deplRestants = new ArrayList<String>(Arrays.asList("A1", "A2", "A3", "A4", "B1", "B2", "B3", "B4", "C1", "C2", "C3", "C4", "D1", "D2", "D3", "D4"));

	public List<String> getDeplRestants() {
		return deplRestants;
	}

	public void setDeplRestants(List<String> deplRestants) {
		this.deplRestants = deplRestants;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Amphipode getAmphipode() {
		return amphipode;
	}

	public void setAmphipode(Amphipode amphipode) {
		this.amphipode = amphipode;
	}

	@Override
	public String toString() {
		return "Espace [name=" + name + ", {" + amphipode + "}]";
	}

	public Espace(String name) {
		super();
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Espace other = (Espace) obj;
		return Objects.equals(name, other.name);
	}

}
