package advent2021.day23;

import java.util.Date;
import java.util.Objects;

public class Amphipode {

	private String race;
	private String nom;
	private int nbDeplacements;
	private Date dateCreation;

	/* constructeur copie > pour créer un clone d'un amphipode */
	public Amphipode(Amphipode amphipode) {
		this.race = amphipode.race;
		this.nom = amphipode.nom;
		this.nbDeplacements = amphipode.nbDeplacements;
		this.dateCreation = new Date(amphipode.dateCreation.getTime()); // si la classe contient un attribut de type non primitif (par ex de type Date)
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Amphipode(String race, String nom) {
		super();
		this.race = race;
		this.nom = nom;
		this.nbDeplacements = 2;
	}

	public int getNbDeplacements() {
		return nbDeplacements;
	}

	public void setNbDeplacements(int nbDeplacements) {
		this.nbDeplacements = nbDeplacements;
	}

	@Override
	public String toString() {
		return "[" + nom + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(nom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Amphipode other = (Amphipode) obj;
		return Objects.equals(nom, other.nom);
	}

}
