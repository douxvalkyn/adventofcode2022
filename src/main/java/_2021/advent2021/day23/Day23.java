package advent2021.day23;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import advent2021.outils.Outil;

public class Day23 {

	private static List<Integer> couts = new ArrayList<Integer>();
	private static List<String> deplacements = new ArrayList<String>();

	// main -----
	public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		System.out.println("[" + Day23.class.getSimpleName() + "]");
		Day23 day = new Day23();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day23.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		List<Espace> espacesInit = initialisationEspaces();
		ajoutAmphipodes(lignes, espacesInit);

		// optimisation initiale: si un amphipode est d�j� bien plac�, il n'a pas besoin de bouger: ses d�placements sont � z�ro. (inutile pour l'input reel...)
		// optimiserNbDeplacementsInitiauxDesAmphipodes(espacesInit);

		// recursion
		effectueUnMoveSiPossible(null, null, espacesInit, 0, 0, "");
		System.out.println("min cout: " + Outil.min(couts));
		// System.out.println("taille couts: "+couts.size());
		// trouver les deplacements dont cout = min cout
		// int min=Outil.min(couts);
		// for (int i=0; i<couts.size(); i++) {
		// if( couts.get(i)==min) {
		// System.out.println(deplacements.get(i));
		// }
		// }

	}

	public void effectueUnMoveSiPossible(Amphipode amphipode, Espace dest, List<Espace> espaces, int cout, int nbDeplacements, String deplacement) {

		if (amphipode != null & dest != null) {
			int coutDuDeplacement = appliquerDeplacement(amphipode, dest, espaces);
			cout = cout + coutDuDeplacement;
			deplacement = deplacement + "[" + amphipode.getNom() + ">" + dest.getName() + "] ;";
			nbDeplacements++;
		}

		Map<Amphipode, List<Espace>> map = repererAmphipodesDeplacables(espaces);
		if (map.isEmpty() | cout > 45000) { // parametrer le cout max tolerable et le nbDeplacements max tolerable: | nbDeplacements==32 || cout>50000
			// System.out.println("cout: "+ cout);
			// System.out.println("nb deplacements: "+ nbDeplacements);
			boolean finValide = checkFin(espaces);
			finValide = checkFin(espaces);
			if (finValide) {
				couts.add(cout);
				deplacements.add(deplacement);
			}
		} else {
			for (Amphipode key : map.keySet()) {
				for (Espace destination : map.get(key)) {
					// System.out.println("amphipode "+key + ":" +destination);
					// recopie espaces dans espacesCopy pour initier un nouvel espace des possibles
					List<Espace> espacesCopy = copie(espaces);
					effectueUnMoveSiPossible(key, destination, espacesCopy, cout, nbDeplacements, deplacement); // recursion
				}

			}
		}
	}

	private boolean checkFin(List<Espace> espaces) {
		boolean checkA = false;
		boolean checkB = false;
		boolean checkC = false;
		boolean checkD = false;
		boolean check = false;

		if ((espaces.get(7).getAmphipode() != null) & (espaces.get(8).getAmphipode() != null) & (espaces.get(9).getAmphipode() != null) & (espaces.get(10).getAmphipode() != null)
			& (espaces.get(11).getAmphipode() != null) & (espaces.get(12).getAmphipode() != null) & (espaces.get(13).getAmphipode() != null) & (espaces.get(14).getAmphipode() != null)
			& (espaces.get(15).getAmphipode() != null) & (espaces.get(16).getAmphipode() != null) & (espaces.get(17).getAmphipode() != null) & (espaces.get(18).getAmphipode() != null)
			& (espaces.get(19).getAmphipode() != null) & (espaces.get(20).getAmphipode() != null) & (espaces.get(21).getAmphipode() != null) & (espaces.get(22).getAmphipode() != null)) {
			checkA = (espaces.get(7)).getAmphipode().getRace().equals("A") & (espaces.get(8)).getAmphipode().getRace().equals("A") & (espaces.get(9)).getAmphipode().getRace().equals("A")
				& (espaces.get(10)).getAmphipode().getRace().equals("A");
			checkB = (espaces.get(11)).getAmphipode().getRace().equals("B") & (espaces.get(12)).getAmphipode().getRace().equals("B") & (espaces.get(13)).getAmphipode().getRace().equals("B")
				& (espaces.get(14)).getAmphipode().getRace().equals("B");
			checkC = (espaces.get(15)).getAmphipode().getRace().equals("C") & (espaces.get(16)).getAmphipode().getRace().equals("C") & (espaces.get(17)).getAmphipode().getRace().equals("C")
				& (espaces.get(18)).getAmphipode().getRace().equals("C");
			checkD = (espaces.get(19)).getAmphipode().getRace().equals("D") & (espaces.get(20)).getAmphipode().getRace().equals("D") & (espaces.get(21)).getAmphipode().getRace().equals("D")
				& (espaces.get(22)).getAmphipode().getRace().equals("D");
			check = (checkA & checkB & checkC & checkD);
		}
		return check;
	}

	private void optimiserNbDeplacementsInitiauxDesAmphipodes(List<Espace> espacesInit) {

		Espace roomA4 = espacesInit.get(10);
		if (roomA4.getAmphipode().getRace().equals("A")) {
			roomA4.getAmphipode().setNbDeplacements(0);
		}

		Espace roomB4 = espacesInit.get(14);
		if (roomB4.getAmphipode().getRace().equals("B")) {
			roomB4.getAmphipode().setNbDeplacements(0);
		}

		Espace roomC4 = espacesInit.get(18);
		if (roomC4.getAmphipode().getRace().equals("C")) {
			roomC4.getAmphipode().setNbDeplacements(0);
		}

		Espace roomD4 = espacesInit.get(22);
		if (roomA4.getAmphipode().getRace().equals("D")) {
			roomD4.getAmphipode().setNbDeplacements(0);
		}

	}

	private List<Espace> copie(List<Espace> espaces) {
		// cree un nouvel objet espacesCopy
		List<Espace> espacesCopy = new ArrayList<Espace>();
		for (int i = 0; i < espaces.size(); i++) {

			Espace espace = espaces.get(i);
			String nom = espace.getName();
			String type = espace.getType();
			List<String> deplRestants = espace.getDeplRestants();
			List<String> deplRestantsCopy = new ArrayList<String>();
			for (String depl : deplRestants) {
				String deplCopy = depl;
				deplRestantsCopy.add(deplCopy);
			}

			Espace sp = new Espace(nom); // nouvel objet

			if (espace.getAmphipode() != null) {
				String amphiNom = espace.getAmphipode().getNom();
				String amphiRace = espace.getAmphipode().getRace();
				int amphiNbDepl = espace.getAmphipode().getNbDeplacements();
				Amphipode amphipode = new Amphipode(amphiRace, amphiNom);
				amphipode.setNbDeplacements(amphiNbDepl);
				sp.setAmphipode(amphipode);
			}

			sp.setDeplRestants(deplRestantsCopy);
			sp.setType(type);

			espacesCopy.add(sp); // copier le nouvel objet dans espacesCopy
		}

		return espacesCopy;
	}

	private int appliquerDeplacement(Amphipode key, Espace destination, List<Espace> espaces) {

		// trouver dans quel endroit se trouve l'amphipode
		Espace origine = null;
		for (Espace espace : espaces) {
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNom().equals(key.getNom())) {
					origine = espace;
					origine.getAmphipode().setNbDeplacements(origine.getAmphipode().getNbDeplacements() - 1); // Nb deplacements -1
				}
			}
		}

		// trouver dans quel endroit se trouve la destination
		Espace dest = null;
		for (Espace espace : espaces) {
			if (espace.getName().equals(destination.getName())) {
				dest = espace;
			}
		}

		dest.setAmphipode(origine.getAmphipode());
		origine.setAmphipode(null);

		// calculer le nombre de pas effectue
		int pas = 0;
		if (origine.getName().equals("RA1") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RA1")) {
			pas = 3;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RA2")) {
			pas = 4;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RA3")) {
			pas = 5;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RA4")) {
			pas = 6;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RB1")) {
			pas = 5;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RB2")) {
			pas = 6;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RB3")) {
			pas = 7;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RB4")) {
			pas = 8;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RC1")) {
			pas = 7;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RC2")) {
			pas = 8;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RC3")) {
			pas = 9;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RC4")) {
			pas = 10;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RD1")) {
			pas = 9;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RD2")) {
			pas = 10;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RD3")) {
			pas = 11;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H1") | origine.getName().equals("H1") & dest.getName().equals("RD4")) {
			pas = 12;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RA1")) {
			pas = 2;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RA2")) {
			pas = 3;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RA3")) {
			pas = 4;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RA4")) {
			pas = 5;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RB1")) {
			pas = 4;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RB2")) {
			pas = 5;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RB3")) {
			pas = 6;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RB4")) {
			pas = 7;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RC1")) {
			pas = 6;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RC2")) {
			pas = 7;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RC3")) {
			pas = 8;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RC4")) {
			pas = 9;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RD1")) {
			pas = 8;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RD2")) {
			pas = 90;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RD3")) {
			pas = 10;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H2") | origine.getName().equals("H2") & dest.getName().equals("RD4")) {
			pas = 11;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RA1")) {
			pas = 2;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RA2")) {
			pas = 3;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RA3")) {
			pas = 4;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RA4")) {
			pas = 5;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RB1")) {
			pas = 2;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RB2")) {
			pas = 3;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RB3")) {
			pas = 4;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RB4")) {
			pas = 5;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RC1")) {
			pas = 4;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RC2")) {
			pas = 5;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RC3")) {
			pas = 6;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RC4")) {
			pas = 7;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RD1")) {
			pas = 6;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RD2")) {
			pas = 7;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RD3")) {
			pas = 8;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H4") | origine.getName().equals("H4") & dest.getName().equals("RD4")) {
			pas = 9;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RA1")) {
			pas = 4;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RA2")) {
			pas = 5;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RA3")) {
			pas = 6;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RA4")) {
			pas = 7;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RB1")) {
			pas = 2;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RB2")) {
			pas = 3;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RB3")) {
			pas = 4;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RB4")) {
			pas = 5;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RC1")) {
			pas = 2;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RC2")) {
			pas = 3;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RC3")) {
			pas = 4;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RC4")) {
			pas = 5;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RD1")) {
			pas = 4;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RD2")) {
			pas = 5;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RD3")) {
			pas = 6;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H6") | origine.getName().equals("H6") & dest.getName().equals("RD4")) {
			pas = 7;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RA1")) {
			pas = 6;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RA2")) {
			pas = 7;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RA3")) {
			pas = 8;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RA4")) {
			pas = 9;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RB1")) {
			pas = 4;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RB2")) {
			pas = 5;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RB3")) {
			pas = 6;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RB4")) {
			pas = 7;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RC1")) {
			pas = 2;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RC2")) {
			pas = 3;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RC3")) {
			pas = 4;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RC4")) {
			pas = 5;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RD1")) {
			pas = 2;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RD2")) {
			pas = 3;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RD3")) {
			pas = 4;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H8") | origine.getName().equals("H8") & dest.getName().equals("RD4")) {
			pas = 5;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RA1")) {
			pas = 8;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RA2")) {
			pas = 9;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RA3")) {
			pas = 10;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RA4")) {
			pas = 11;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RB1")) {
			pas = 6;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RB2")) {
			pas = 7;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RB3")) {
			pas = 8;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RB4")) {
			pas = 9;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RC1")) {
			pas = 4;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RC2")) {
			pas = 5;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RC3")) {
			pas = 6;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RC4")) {
			pas = 7;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RD1")) {
			pas = 2;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RD2")) {
			pas = 3;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RD3")) {
			pas = 4;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H10") | origine.getName().equals("H10") & dest.getName().equals("RD4")) {
			pas = 5;
		}

		if (origine.getName().equals("RA1") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RA1")) {
			pas = 9;
		}
		if (origine.getName().equals("RA2") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RA2")) {
			pas = 10;
		}
		if (origine.getName().equals("RA3") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RA3")) {
			pas = 11;
		}
		if (origine.getName().equals("RA4") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RA4")) {
			pas = 12;
		}
		if (origine.getName().equals("RB1") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RB1")) {
			pas = 7;
		}
		if (origine.getName().equals("RB2") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RB2")) {
			pas = 8;
		}
		if (origine.getName().equals("RB3") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RB3")) {
			pas = 9;
		}
		if (origine.getName().equals("RB4") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RB4")) {
			pas = 10;
		}
		if (origine.getName().equals("RC1") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RC1")) {
			pas = 5;
		}
		if (origine.getName().equals("RC2") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RC2")) {
			pas = 6;
		}
		if (origine.getName().equals("RC3") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RC3")) {
			pas = 7;
		}
		if (origine.getName().equals("RC4") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RC4")) {
			pas = 8;
		}
		if (origine.getName().equals("RD1") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RD1")) {
			pas = 3;
		}
		if (origine.getName().equals("RD2") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RD2")) {
			pas = 4;
		}
		if (origine.getName().equals("RD3") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RD3")) {
			pas = 5;
		}
		if (origine.getName().equals("RD4") & dest.getName().equals("H11") | origine.getName().equals("H11") & dest.getName().equals("RD4")) {
			pas = 6;
		}

		// calculer le cout en fonction de l'amphipode qui se deplace et du nb de pas
		int coutDuDeplacement = 0;
		if (dest.getAmphipode().getRace().equals("A")) {
			coutDuDeplacement = 1 * pas;
		}
		if (dest.getAmphipode().getRace().equals("B")) {
			coutDuDeplacement = 10 * pas;
		}
		if (dest.getAmphipode().getRace().equals("C")) {
			coutDuDeplacement = 100 * pas;
		}
		if (dest.getAmphipode().getRace().equals("D")) {
			coutDuDeplacement = 1000 * pas;
		}

		return (coutDuDeplacement);
	}

	public void ajoutAmphipodes(List<String> lignes, List<Espace> espaces) {
		int cptA = 1;
		int cptB = 1;
		int cptC = 1;
		int cptD = 1;
		int cpt = 1;

		if (lignes.get(2).charAt(3) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(2).charAt(3) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(2).charAt(3) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(2).charAt(3) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi1 = new Amphipode("" + lignes.get(2).charAt(3), "" + lignes.get(2).charAt(3) + cpt);
		espaces.get(7).setAmphipode(amphi1);

		if (lignes.get(3).charAt(3) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(3).charAt(3) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(3).charAt(3) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(3).charAt(3) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi2 = new Amphipode("" + lignes.get(3).charAt(3), "" + lignes.get(3).charAt(3) + cpt);
		espaces.get(8).setAmphipode(amphi2);

		if (lignes.get(4).charAt(3) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(4).charAt(3) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(4).charAt(3) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(4).charAt(3) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi3 = new Amphipode("" + lignes.get(4).charAt(3), "" + lignes.get(4).charAt(3) + cpt);
		espaces.get(9).setAmphipode(amphi3);

		if (lignes.get(5).charAt(3) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(5).charAt(3) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(5).charAt(3) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(5).charAt(3) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi4 = new Amphipode("" + lignes.get(5).charAt(3), "" + lignes.get(5).charAt(3) + cpt);
		espaces.get(10).setAmphipode(amphi4);

		if (lignes.get(2).charAt(5) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(2).charAt(5) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(2).charAt(5) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(2).charAt(5) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi5 = new Amphipode("" + lignes.get(2).charAt(5), "" + lignes.get(2).charAt(5) + cpt);
		espaces.get(11).setAmphipode(amphi5);

		if (lignes.get(3).charAt(5) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(3).charAt(5) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(3).charAt(5) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(3).charAt(5) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi6 = new Amphipode("" + lignes.get(3).charAt(5), "" + lignes.get(3).charAt(5) + cpt);
		espaces.get(12).setAmphipode(amphi6);

		if (lignes.get(4).charAt(5) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(4).charAt(5) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(4).charAt(5) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(4).charAt(5) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi7 = new Amphipode("" + lignes.get(4).charAt(5), "" + lignes.get(4).charAt(5) + cpt);
		espaces.get(13).setAmphipode(amphi7);

		if (lignes.get(5).charAt(5) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(5).charAt(5) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(5).charAt(5) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(5).charAt(5) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi8 = new Amphipode("" + lignes.get(5).charAt(5), "" + lignes.get(5).charAt(5) + cpt);
		espaces.get(14).setAmphipode(amphi8);

		if (lignes.get(2).charAt(7) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(2).charAt(7) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(2).charAt(7) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(2).charAt(7) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi9 = new Amphipode("" + lignes.get(2).charAt(7), "" + lignes.get(2).charAt(7) + cpt);
		espaces.get(15).setAmphipode(amphi9);

		if (lignes.get(3).charAt(7) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(3).charAt(7) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(3).charAt(7) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(3).charAt(7) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi10 = new Amphipode("" + lignes.get(3).charAt(7), "" + lignes.get(3).charAt(7) + cpt);
		espaces.get(16).setAmphipode(amphi10);

		if (lignes.get(4).charAt(7) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(4).charAt(7) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(4).charAt(7) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(4).charAt(7) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi11 = new Amphipode("" + lignes.get(4).charAt(7), "" + lignes.get(4).charAt(7) + cpt);
		espaces.get(17).setAmphipode(amphi11);

		if (lignes.get(5).charAt(7) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(5).charAt(7) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(5).charAt(7) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(5).charAt(7) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi12 = new Amphipode("" + lignes.get(5).charAt(7), "" + lignes.get(5).charAt(7) + cpt);
		espaces.get(18).setAmphipode(amphi12);

		if (lignes.get(2).charAt(9) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(2).charAt(9) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(2).charAt(9) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(2).charAt(9) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi13 = new Amphipode("" + lignes.get(2).charAt(9), "" + lignes.get(2).charAt(9) + cpt);
		espaces.get(19).setAmphipode(amphi13);

		if (lignes.get(3).charAt(9) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(3).charAt(9) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(3).charAt(9) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(3).charAt(9) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi14 = new Amphipode("" + lignes.get(3).charAt(9), "" + lignes.get(3).charAt(9) + cpt);
		espaces.get(20).setAmphipode(amphi14);

		if (lignes.get(4).charAt(9) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(4).charAt(9) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(4).charAt(9) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(4).charAt(9) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi15 = new Amphipode("" + lignes.get(4).charAt(9), "" + lignes.get(4).charAt(9) + cpt);
		espaces.get(21).setAmphipode(amphi15);

		if (lignes.get(5).charAt(9) == 'A') {
			cpt = cptA;
			cptA++;
		}
		if (lignes.get(5).charAt(9) == 'B') {
			cpt = cptB;
			cptB++;
		}
		if (lignes.get(5).charAt(9) == 'C') {
			cpt = cptC;
			cptC++;
		}
		if (lignes.get(5).charAt(9) == 'D') {
			cpt = cptD;
			cptD++;
		}
		Amphipode amphi16 = new Amphipode("" + lignes.get(5).charAt(9), "" + lignes.get(5).charAt(9) + cpt);
		espaces.get(22).setAmphipode(amphi16);
	}

	private List<Espace> initialisationEspaces() {

		List<Espace> espaces = new ArrayList<Espace>();

		Espace espaceH1 = new Espace("H1");
		espaceH1.setType("H");
		espaces.add(espaceH1);
		Espace espaceH2 = new Espace("H2");
		espaceH2.setType("H");
		espaces.add(espaceH2);
		Espace espaceH4 = new Espace("H4");
		espaceH4.setType("H");
		espaces.add(espaceH4);
		Espace espaceH6 = new Espace("H6");
		espaceH6.setType("H");
		espaces.add(espaceH6);
		Espace espaceH8 = new Espace("H8");
		espaceH8.setType("H");
		espaces.add(espaceH8);
		Espace espaceH10 = new Espace("H10");
		espaceH10.setType("H");
		espaces.add(espaceH10);
		Espace espaceH11 = new Espace("H11");
		espaceH11.setType("H");
		espaces.add(espaceH11);

		Espace espaceRA1 = new Espace("RA1");
		espaceRA1.setType("R");
		espaces.add(espaceRA1);
		Espace espaceRA2 = new Espace("RA2");
		espaceRA2.setType("R");
		espaces.add(espaceRA2);
		Espace espaceRA3 = new Espace("RA3");
		espaceRA3.setType("R");
		espaces.add(espaceRA3);
		Espace espaceRA4 = new Espace("RA4");
		espaceRA4.setType("R");
		espaces.add(espaceRA4);

		Espace espaceRB1 = new Espace("RB1");
		espaceRB1.setType("R");
		espaces.add(espaceRB1);
		Espace espaceRB2 = new Espace("RB2");
		espaceRB2.setType("R");
		espaces.add(espaceRB2);
		Espace espaceRB3 = new Espace("RB3");
		espaceRB3.setType("R");
		espaces.add(espaceRB3);
		Espace espaceRB4 = new Espace("RB4");
		espaceRB4.setType("R");
		espaces.add(espaceRB4);

		Espace espaceRC1 = new Espace("RC1");
		espaceRC1.setType("R");
		espaces.add(espaceRC1);
		Espace espaceRC2 = new Espace("RC2");
		espaceRC2.setType("R");
		espaces.add(espaceRC2);
		Espace espaceRC3 = new Espace("RC3");
		espaceRC3.setType("R");
		espaces.add(espaceRC3);
		Espace espaceRC4 = new Espace("RC4");
		espaceRC4.setType("R");
		espaces.add(espaceRC4);

		Espace espaceRD1 = new Espace("RD1");
		espaceRD1.setType("R");
		espaces.add(espaceRD1);
		Espace espaceRD2 = new Espace("RD2");
		espaceRD2.setType("R");
		espaces.add(espaceRD2);
		Espace espaceRD3 = new Espace("RD3");
		espaceRD3.setType("R");
		espaces.add(espaceRD3);
		Espace espaceRD4 = new Espace("RD4");
		espaceRD4.setType("R");
		espaces.add(espaceRD4);

		return espaces;
	}

	private Map<Amphipode, List<Espace>> repererAmphipodesDeplacables(List<Espace> espaces) {
		Map<Amphipode, List<Espace>> map = new HashMap<>();

		// premier test preliminaire: un amphipode ne se deplace que s'il lui reste des d�placements (2 au max au d�part, un aller, un retour)

		boolean retourneDansSaRoom = false; // mouvement prioritaire

		// un amphipode du couloir, s'il a encore des deplacements, ne peut aller que dans sa room, si le chemin est libre et s'il n'y a que sa race dans la room
		// il ne va se deplacer que dans sa room la plus profonde
		// H1
		Espace espace = espaces.get(0);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H1 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(1).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null
						& espaces.get(10).getAmphipode() != null) { // H2 et RA1 vides, RA2, RA3, RA4 remplies: RA1 atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null
							& espaces.get(10).getAmphipode() != null) { // H2, RA1, RA2 vides, RA3, RA4 remplies: RA2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null
							& espaces.get(10).getAmphipode() != null) { // H2, RA1, RA2, RA3 vides, RA4 rempli: RA3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null
							& espaces.get(10).getAmphipode() == null) { // H2, RA1, RA2, RA3, RA4 vides: RA4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null
							& espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H2, H4 , RB1 vide, RB2, RB3, RB4 remplis: RB1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H2 , H4, RB1, RB2 vide, RB3 et RB4 remplis: RB2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() != null) { // H2, H4 , RB1, RB2 , RB3 vides, RB4 rempli: RB3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() == null) { // H2 vide, H4 vide, RB1 vide, RB2 vide, RB3 vide, RB4 vide: RB4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null
							& espaces.get(16).getAmphipode() != null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H2 , H4 , H6 , RC1 vides, RC2, RC3, RC4
																																							// remplis : RC1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null
							& espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H2 , H4 , H6 , RC1 , RC2 vide , RC3 et RC4
																																							// remplis: RC2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null
							& espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() != null) { // H2 , H4 , H6 , RC1 , RC2 , RC3 vides, RC4
																																							// rempli : RC3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null
							& espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() == null) { // H2 , H4 , H6 , RC1 , RC2 , RC3 , RC4 vides
																																							// : RC4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
							& espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H2
																																																	// ,
																																																	// H4
																																																	// ,
																																																	// H6
																																																	// ,
																																																	// H8
																																																	// ,
																																																	// RD1
																																																	// vides,
																																																	// RD2,
																																																	// RD3,
																																																	// RD4
																																																	// remplis:
																																																	// RD1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
							& espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H2
																																																	// ,
																																																	// H4
																																																	// ,
																																																	// H6
																																																	// ,
																																																	// H8
																																																	// ,
																																																	// RD1,
																																																	// RD2
																																																	// vides,
																																																	// RD3,
																																																	// RD4
																																																	// remplis:
																																																	// RD2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
							& espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() != null) { // H2
																																																	// ,
																																																	// H4
																																																	// ,
																																																	// H6
																																																	// ,
																																																	// H8
																																																	// ,
																																																	// RD1
																																																	// ,
																																																	// RD2
																																																	// ,
																																																	// RD3
																																																	// vides,
																																																	// RD4
																																																	// rempli:
																																																	// RD3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H1 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
							& espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() == null) { // H2
																																																	// vide,
																																																	// H4
																																																	// vide,
																																																	// H6
																																																	// vide,
																																																	// H8
																																																	// vide,
																																																	// RD1
																																																	// vide,
																																																	// RD2
																																																	// vide,
																																																	// RD3
																																																	// vide,
																																																	// RD4
																																																	// vide:
																																																	// RD4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H2
		espace = espaces.get(1);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H2 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // RA1 vide,
																																															// RA2, RA3,
																																															// RA4
																																															// remplis:
																																															// RA1
																																															// atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // RA1 ,
																																																// RA2
																																																// vides,
																																																// RA3
																																																// et
																																																// RA4
																																																// remplis:
																																																// RA2
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() != null) { // RA1 ,
																																																// RA2 ,
																																																// RA3
																																																// vide
																																																// et
																																																// RA4
																																																// rempli:
																																																// RA3
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() == null) { // RA1
																																																// vide,
																																																// RA2
																																																// vide,
																																																// RA3
																																																// vide,
																																																// RA4
																																																// vide:
																																																// RA4
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null & espaces.get(13).getAmphipode() != null
							& espaces.get(14).getAmphipode() != null) { // H4 , RB1 vides, RB2, RB3, RB4 remplis: RB1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() != null
							& espaces.get(14).getAmphipode() != null) { // H4 , RB1 , RB2 vides, RB3 et RB4 remplis: RB2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null
							& espaces.get(14).getAmphipode() != null) { // H4 , RB1 , RB2 , RB3 vides, RB4 rempli: RB3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null
							& espaces.get(14).getAmphipode() == null) { // H4 vide, RB1 vide, RB2 vide, RB3 vide, RB4 vide: RB4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null
							& espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H2 , H4 , H6 , RC1 vides, RC2, RC3, RC4 remplis : RC1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H4 , H6 , RC1 , RC2 vides, RC3 et RC4 remplis : RC2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() != null) { // H4 , H6 , RC1 , RC2 , RC3 vides, RC4 rempli : RC3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() == null) { // H4 vide, H6 vide, RC1 vide, RC2 vide, RC3 vide, RC4 vide : RC4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null
							& espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H4 , H6 , H8 , RD1 vides, RD2, RD3, RD4
																																							// remplis: RD1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null
							& espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H4 , H6 , H8 , RD1 , RD2 vides, RD3 et RD4
																																							// remplis: RD2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null
							& espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() != null) { // H4 , H6 , H8 , RD1 , RD2 , RD3 vides, RD4
																																							// rempli: RD3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H2 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null
							& espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() == null) { // H4 vide, H6 vide, H8 vide, RD1 vide, RD2
																																							// vide, RD3 vide, RD4 vide: RD4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H4
		espace = espaces.get(2);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H4 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // RA1 vide,
																																															// RA2, RA3,
																																															// RA4
																																															// remplis:
																																															// RA1
																																															// atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // RA1 ,
																																																// RA2
																																																// vides,
																																																// RA3
																																																// et
																																																// RA4
																																																// remplis:
																																																// RA2
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() != null) { // RA1 ,
																																																// RA2 ,
																																																// RA3
																																																// vides,
																																																// RA4
																																																// rempli:
																																																// RA3
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() == null) { // RA1
																																																// vide,
																																																// RA2
																																																// vide,
																																																// RA3
																																																// vide,
																																																// RA4
																																																// vide:
																																																// RA4
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// vide,
																																																	// RB2,
																																																	// RB3,
																																																	// RB4
																																																	// remplis:
																																																	// RB1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// ,
																																																	// RB2
																																																	// vides,
																																																	// RB3
																																																	// et
																																																	// RB4
																																																	// remplis:
																																																	// RB2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// ,
																																																	// RB2
																																																	// ,
																																																	// RB3
																																																	// vides,
																																																	// RB4
																																																	// rempli:
																																																	// RB3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
							} else {
								List<Espace> list = new ArrayList<>();
								retourneDansSaRoom = true;
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() == null) { // RB1
																																																	// vide,
																																																	// RB2
																																																	// vide,
																																																	// RB3
																																																	// vide,
																																																	// RB4
																																																	// vide:
																																																	// RB4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null & espaces.get(17).getAmphipode() != null
							& espaces.get(18).getAmphipode() != null) { // H6 , RC1 vides, RC2, RC3, RC4 remplis : RC1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() != null
							& espaces.get(18).getAmphipode() != null) { // H6 , RC1 , RC2 vides, RC3 et RC4 remplis : RC2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null
							& espaces.get(18).getAmphipode() != null) { // H6 , RC1 , RC2 , RC3 vides, RC4 rempli : RC3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null
							& espaces.get(18).getAmphipode() == null) { // H6 vide, RC1 vide, RC2 vide, RC3 vide, RC4 vide : RC4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null
							& espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H6 , H8 , RD1 vides, RD2, RD3, RD4 remplis: RD1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null
							& espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // H6 , H8 , RD1 , RD2 vides, RD3 et RD4 remplis: RD2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null
							& espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() != null) { // H6 , H8 , RD1 , RD2 , RD3 vides, RD4 rempli: RD3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H4 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null
							& espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() == null) { // H6 vide, H8 vide, RD1 vide, RD2 vide, RD3 vide, RD4 vide: RD4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H6
		espace = espaces.get(3);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H6 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(2).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null
						& espaces.get(10).getAmphipode() != null) { // H4 , RA1 vide, RA2, RA3, RA4 remplis: RA1 atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null
							& espaces.get(10).getAmphipode() != null) { // H4 , RA1 , RA2 vides, RA3 et RA4 remplis: RA2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null
							& espaces.get(10).getAmphipode() != null) { // H4 , RA1 , RA2 , RA3 vides, RA4 rempli: RA3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null
							& espaces.get(10).getAmphipode() == null) { // H4 vide, RA1 vide, RA2 vide, RA3 vide, RA4 vide: RA4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// vide,
																																																	// RB2,
																																																	// RB3,
																																																	// RB4
																																																	// remplis:
																																																	// RB1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// ,
																																																	// RB2
																																																	// vides,
																																																	// RB3
																																																	// et
																																																	// RB4
																																																	// remplis:
																																																	// RB2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() != null) { // RB1
																																																	// ,
																																																	// RB2
																																																	// ,
																																																	// RB3
																																																	// vides,
																																																	// RB4
																																																	// remplis:
																																																	// RB3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() == null) { // RB1
																																																	// vide,
																																																	// RB2
																																																	// vide,
																																																	// RB3
																																																	// vide,
																																																	// RB4
																																																	// vide:
																																																	// RB4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// vide,
																																																	// RC2,
																																																	// RC3,
																																																	// RC4
																																																	// remplis
																																																	// :
																																																	// RC1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// ,
																																																	// RC2
																																																	// vides,
																																																	// RC3
																																																	// et
																																																	// RC4
																																																	// remplis
																																																	// :
																																																	// RC2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// ,
																																																	// RC2
																																																	// ,
																																																	// RC3
																																																	// vides,
																																																	// RC4
																																																	// remplis
																																																	// :
																																																	// RC3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() == null) { // RC1
																																																	// vide,
																																																	// RC2
																																																	// vide,
																																																	// RC3
																																																	// vide,
																																																	// RC4
																																																	// vide
																																																	// :
																																																	// RC4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null
							& espaces.get(22).getAmphipode() != null) { // H8 , RD1 vides, RD2, RD3, RD4 remplis: RD1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null
							& espaces.get(22).getAmphipode() != null) { // H8 , RD1 , RD2 vides, RD3 et RD4 vides: RD2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null
							& espaces.get(22).getAmphipode() != null) { // H8 , RD1 , RD2 , RD3 vides, RD4 rempli: RD3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H6 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null
							& espaces.get(22).getAmphipode() == null) { // H8 vide, RD1 vide, RD2 vide, RD3 vide, RD4 vide: RD4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H8
		espace = espaces.get(4);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H8 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null
						& espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , RA1 vides, RA2, RA3, RA4 remplis: RA1 atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null
							& espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , RA1 , RA2 vides, RA3 et RA4 remplis: RA2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null
							& espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , RA1 , RA2 , RA3 vides, RA4 rempli: RA3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null
							& espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() == null) { // H4 vide, H6 vide, RA1 vide, RA2 vide, RA3 vide, RA4 vide: RA4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null & espaces.get(13).getAmphipode() != null
							& espaces.get(14).getAmphipode() != null) { // H6 , RB1 vides, RB2, RB3, RB4 remplis: RB1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() != null
							& espaces.get(14).getAmphipode() != null) { // H6 , RB1 , RB2 vides, RB3 et RB4 remplis: RB2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null
							& espaces.get(14).getAmphipode() != null) { // H6 , RB1 , RB2 , RB3 vides, RB4 rempli: RB3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null
							& espaces.get(14).getAmphipode() == null) { // H6 vide, RB1 vide, RB2 vide, RB3 vide, RB4 vide: RB4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// vide,
																																																	// RC2,
																																																	// RC3,
																																																	// RC4
																																																	// remplis
																																																	// :
																																																	// RC1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// ,
																																																	// RC2
																																																	// vide,
																																																	// RC3
																																																	// et
																																																	// RC4
																																																	// rempli
																																																	// :
																																																	// RC2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() != null) { // RC1
																																																	// ,
																																																	// RC2
																																																	// ,
																																																	// RC3
																																																	// vides
																																																	// et
																																																	// RC4
																																																	// rempli
																																																	// :
																																																	// RC3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() == null) { // RC1
																																																	// vide,
																																																	// RC2
																																																	// vide,
																																																	// RC3
																																																	// vide,
																																																	// RC4
																																																	// vide
																																																	// :
																																																	// RC4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// vide,
																																																	// RD2,
																																																	// RD3,
																																																	// RD4
																																																	// remplis:
																																																	// RD1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// ,
																																																	// RD2
																																																	// vides,
																																																	// RD3
																																																	// et
																																																	// RD4
																																																	// remplis:
																																																	// RD2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// ,
																																																	// RD2
																																																	// ,
																																																	// RD3
																																																	// vides,
																																																	// RD4
																																																	// rempli:
																																																	// RD3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H8 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() == null) { // RD1
																																																	// vide,
																																																	// RD2
																																																	// vide,
																																																	// RD3
																																																	// vide,
																																																	// RD4
																																																	// vide:
																																																	// RD4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H10
		espace = espaces.get(5);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H10 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(7).getAmphipode() == null
						& espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , H8 , RA1 vides, RA2, RA3, RA4 remplis:
																																					// RA1 atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(7).getAmphipode() == null
							& espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , H8 , RA1 , RA2 vides, RA3 et RA4
																																						// vides: RA2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(7).getAmphipode() == null
							& espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() != null) { // H4 , H6 , H8 , RA1 , RA2 , RA3 vides, RA4
																																						// rempli: RA3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(7).getAmphipode() == null
							& espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() == null) { // H4 vide, H6 vide, H8 vide, RA1 vide, RA2
																																						// vide, RA3 vide, RA4 vide: RA4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() != null
							& espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H8 , H6 , RB1 vides, RB2, RB3, RB4 remplis: RB1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H8 , H6 , RB1 , RB2 vides, RB3 et RB4 remplis: RB2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() != null) { // H8 , H6 , RB1 , RB2 , RB3 vides, RB4 rempli: RB3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(12).getAmphipode() == null
							& espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() == null) { // H8 vide, H6 vide, RB1 vide, RB2 vide, RB3 vide, RB4 vide: RB4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null & espaces.get(17).getAmphipode() != null
							& espaces.get(18).getAmphipode() != null) { // H8 , RC1 vide, RC2, RC3, RC4 remplis : RC1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() != null
							& espaces.get(18).getAmphipode() != null) { // H8 , RC1 , RC2 vides, RC3 et RC4 remplis : RC2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null
							& espaces.get(18).getAmphipode() != null) { // H8 , RC1 , RC2 , RC3 vides, RC4 rempli : RC3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(17).getAmphipode() == null
							& espaces.get(18).getAmphipode() == null) { // H8 vide, RC1 vide, RC2 vide, RC3 vide, RC4 vide : RC4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// vide,
																																																	// RD2,
																																																	// RD3,
																																																	// RD4
																																																	// remplis:
																																																	// RD1
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// ,
																																																	// RD2
																																																	// vides,
																																																	// RD3
																																																	// et
																																																	// RD4
																																																	// remplis:
																																																	// RD2
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() != null) { // RD1
																																																	// ,
																																																	// RD2
																																																	// ,
																																																	// RD3
																																																	// vides,
																																																	// RD4
																																																	// rempli:
																																																	// RD3
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H10 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null & espaces.get(22).getAmphipode() == null) { // RD1
																																																	// vide,
																																																	// RD2
																																																	// vide,
																																																	// RD3
																																																	// vide,
																																																	// RD4
																																																	// vide:
																																																	// RD4
																																																	// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// H11
		espace = espaces.get(6);
		if (espace.getAmphipode() != null) {
			if (espace.getAmphipode().getNbDeplacements() > 0) {

				// H11 > RA1
				if (espace.getAmphipode().getRace().equals("A")) {
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
						& espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() != null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 , H6 ,
																																															// H8 , H10
																																															// , RA1
																																															// vides,
																																															// RA2, RA3,
																																															// RA4
																																															// vides:
																																															// RA1
																																															// atteignable
						// RA2 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
						if ((espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
							& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
							& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(7));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(7));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RA2
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA3 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
							& espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() != null & espaces.get(10).getAmphipode() != null) { // H4 ,
																																																// H6 ,
																																																// H8 ,
																																																// H10 ,
																																																// RA1 ,
																																																// RA2
																																																// vides,
																																																// RA3
																																																// et
																																																// RA4
																																																// remplis:
																																																// RA2
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(8));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(8));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RA3
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA2 contient rien ou un A, RA1 contient rien ou un A, RA4 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(10).getAmphipode() == null || espaces.get(10).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
							& espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() != null) { // H4 ,
																																																// H6 ,
																																																// H8 ,
																																																// H10 ,
																																																// RA1 ,
																																																// RA2 ,
																																																// RA3
																																																// vides,
																																																// RA4
																																																// rempli:
																																																// RA3
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(9));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(9));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RA4
				if (espace.getAmphipode().getRace().equals("A")) {
					// RA1 contient rien ou un A, RA2 contient rien ou un A, RA3 contient rien ou un A
					if ((espaces.get(7).getAmphipode() == null || espaces.get(7).getAmphipode().getRace().equals("A"))
						& (espaces.get(8).getAmphipode() == null || espaces.get(8).getAmphipode().getRace().equals("A"))
						& (espaces.get(9).getAmphipode() == null || espaces.get(9).getAmphipode().getRace().equals("A"))) {
						if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
							& espaces.get(7).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(9).getAmphipode() == null & espaces.get(10).getAmphipode() == null) { // H4 ,
																																																// H6 ,
																																																// H8 ,
																																																// H10 ,
																																																// RA1 ,
																																																// RA2 ,
																																																// RA3 ,
																																																// RA4
																																																// vides:
																																																// RA4
																																																// atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(10));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(10));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RB1
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB2 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null
							& espaces.get(12).getAmphipode() != null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H10 , H8 , H6 , RB1 vides, RB2, RB3, RB4
																																							// remplis: RB1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(11));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(11));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RB2
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB3 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null
							& espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() != null & espaces.get(14).getAmphipode() != null) { // H10 , H8 , H6 , RB1 , RB2 vides, RB3 et RB4
																																							// remplis: RB2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(12));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(12));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RB3
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB4 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(14).getAmphipode() == null || espaces.get(14).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null
							& espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() != null) { // H10 , H8 , H6 , RB1 , RB2 , RB3 vides, RB4
																																							// rempli: RB3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(13));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(13));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RB4
				if (espace.getAmphipode().getRace().equals("B")) {
					// RB1 contient rien ou un B, RB2 contient rien ou un B, RB3 contient rien ou un B
					if ((espaces.get(11).getAmphipode() == null || espaces.get(11).getAmphipode().getRace().equals("B"))
						& (espaces.get(12).getAmphipode() == null || espaces.get(12).getAmphipode().getRace().equals("B"))
						& (espaces.get(13).getAmphipode() == null || espaces.get(13).getAmphipode().getRace().equals("B"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(11).getAmphipode() == null
							& espaces.get(12).getAmphipode() == null & espaces.get(13).getAmphipode() == null & espaces.get(14).getAmphipode() == null) { // H10 vide, H8 vide, H6 vide, RB1 vide, RB2
																																							// vide, RB3 vide, RB4 vide: RB4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(14));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(14));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RC1
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC2 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() != null
							& espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H10 , H8 , RC1 vides, RC2 , RC3, RC4 remplis : RC1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(15));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(15));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RC2
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC3 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() != null & espaces.get(18).getAmphipode() != null) { // H10 , H8 , RC1 , RC2 vides, RC3 et RC4 rempls : RC2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(16));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(16));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RC3
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC4 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(18).getAmphipode() == null || espaces.get(18).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() != null) { // H10 , H8 , RC1 , RC2 , RC3 vide, RC4 rempli : RC3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(17));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(17));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RC4
				if (espace.getAmphipode().getRace().equals("C")) {
					// RC1 contient rien ou un C, RC2 contient rien ou un C, RC3 contient rien ou un C
					if ((espaces.get(15).getAmphipode() == null || espaces.get(15).getAmphipode().getRace().equals("C"))
						& (espaces.get(16).getAmphipode() == null || espaces.get(16).getAmphipode().getRace().equals("C"))
						& (espaces.get(17).getAmphipode() == null || espaces.get(17).getAmphipode().getRace().equals("C"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(16).getAmphipode() == null
							& espaces.get(17).getAmphipode() == null & espaces.get(18).getAmphipode() == null) { // H10 vide, H8 vide, RC1 vide, RC2 vide, RC3 vide, RC4 vide : RC4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(18));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(18));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RD1
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD2 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() != null & espaces.get(21).getAmphipode() != null
							& espaces.get(22).getAmphipode() != null) { // H10 , RD1 vides, RD2, RD3, RD4 remplis: RD1 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(19));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(19));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RD2
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD3 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() != null
							& espaces.get(22).getAmphipode() != null) { // H10 , RD1 , RD2 vides, RD3 et RD4 remplis: RD2 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(20));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(20));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RD3
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD4 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(22).getAmphipode() == null || espaces.get(22).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null
							& espaces.get(22).getAmphipode() != null) { // H10 , RD1 , RD2 , RD3 vides, RD4 rempli: RD3 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(21));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(21));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}

				// H11 > RD4
				if (espace.getAmphipode().getRace().equals("D")) {
					// RD1 contient rien ou un D, RD2 contient rien ou un D, RD3 contient rien ou un D
					if ((espaces.get(19).getAmphipode() == null || espaces.get(19).getAmphipode().getRace().equals("D"))
						& (espaces.get(20).getAmphipode() == null || espaces.get(20).getAmphipode().getRace().equals("D"))
						& (espaces.get(21).getAmphipode() == null || espaces.get(21).getAmphipode().getRace().equals("D"))) {
						if (espaces.get(5).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(21).getAmphipode() == null
							& espaces.get(22).getAmphipode() == null) { // H10 vide, RD1 vide, RD2 vide, RD3 vide, RD4 vide: RD4 atteignable
							if (map.containsKey(espace.getAmphipode())) {
								map.get(espace.getAmphipode()).add(espaces.get(22));
								retourneDansSaRoom = true;
							} else {
								List<Espace> list = new ArrayList<>();
								list.add(espaces.get(22));
								map.put(espace.getAmphipode(), list);
								retourneDansSaRoom = true;
							}
						}
					}
				}
			}
		}

		// si aucun amphipode ne peut retourner dans sa room, on regarde si des deplacements sont possible vers le couloir
		// un amphipode d'une room ne peut aller que dans certains espaces du couloir, si le chemin est libre
		if (!retourneDansSaRoom) {

			// ROOM A1
			espace = espaces.get(7);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H1 et H2 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(1).getAmphipode() == null) { // H2 vide: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H4 et H6 et
																																															// H8 et H10
																																															// vides:
																																															// H10
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
						& espaces.get(6).getAmphipode() == null) { // H4 et H6 et H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM A2
			espace = espaces.get(8);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(7).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H1 et H2 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H2 vide: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et
																																															// H8 vides:
																																															// H8
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null) { // H4 et H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H4 et H6 et H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM A3
			espace = espaces.get(9);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H1 et H2
																																															// vides: H1
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H2 vide: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6
																																															// vides: H6
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H4 et H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H4 et H6 et H8 et H10 et H11 vides: H11
																																					// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM A4
			espace = espaces.get(10);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(0).getAmphipode() == null
						& espaces.get(1).getAmphipode() == null) { // H1 et H2 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(1).getAmphipode() == null) { // H2 vide: H2
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide: H4
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H4 et H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(9).getAmphipode() == null & espaces.get(8).getAmphipode() == null & espaces.get(7).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H4 et H6
																																															// et H8 et
																																															// H10 et
																																															// H11
																																															// vides:
																																															// H11
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM B1
			espace = espaces.get(11);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H1 et H2 et H4 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H2 et H4 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H6 et H8 et
																																															// H10 et
																																															// H11
																																															// vides:
																																															// H11
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM B2
			espace = espaces.get(12);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(11).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H1 et H2 et
																																															// H4 vides:
																																															// H1
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H2 et H4 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H6 et H8 et
																																															// H10
																																															// vides:
																																															// H10
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
						& espaces.get(6).getAmphipode() == null) { // H6 et H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM B3
			espace = espaces.get(13);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null) { // H1 et H2 et H4 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H2 et H4
																																															// vides: H2
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8
																																															// vides: H8
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null) { // H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H6 et H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM B4
			espace = espaces.get(14);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(0).getAmphipode() == null
						& espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H1 et H2 et H4 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null) { // H2 et H4 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(2).getAmphipode() == null) { // H4 vide:
																																															// H4
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide:
																																															// H6
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H6 et H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H6 et H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(13).getAmphipode() == null & espaces.get(12).getAmphipode() == null & espaces.get(11).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H6 et H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM C1
			espace = espaces.get(15);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H1 et H2 et
																																															// H4 et H6
																																															// vides: H1
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H2 et H4 et H6 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM C2
			espace = espaces.get(16);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(15).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null) { // H1 et H2 et H4 et H6 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H2 et H4 et
																																															// H6 vides:
																																															// H2
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H8 et H10
																																															// et H11
																																															// vides:
																																															// H11
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM C3
			espace = espaces.get(17);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H1 et H2 et H4 et H6 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null) { // H2 et H4 et H6 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H4 et H6
																																															// vides: H4
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H8 et H10
																																															// vides:
																																															// H10
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null & espaces.get(5).getAmphipode() == null
						& espaces.get(6).getAmphipode() == null) { // H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM C4
			espace = espaces.get(18);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(0).getAmphipode() == null
						& espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H1 et H2 et H4 et H6 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H2 et H4 et H6 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null) { // H4 et H6 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(3).getAmphipode() == null) { // H6 vide:
																																															// H6
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides:
																																															// H8
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null) { // H8 et H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(17).getAmphipode() == null & espaces.get(16).getAmphipode() == null & espaces.get(15).getAmphipode() == null & espaces.get(4).getAmphipode() == null
						& espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H8 et H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM D1
			espace = espaces.get(19);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H1 et H2 et H4 et H6 et H8 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H2 et H4 et
																																															// H6 et H8
																																															// vides: H2
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(5).getAmphipode() == null) { // H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM D2
			espace = espaces.get(20);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(19).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H1 et H2 et H4 et H6 et H8 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H2 et H4 et H6 et H8 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et
																																															// H8 vides:
																																															// H4
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H10 et H11 vides: H11 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM D3
			espace = espaces.get(21);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(0).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H1 et H2 et H4 et H6 et H8 vides: H1 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H2 et H4 et H6 et H8 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H6 et H8
																																															// vide: H6
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides: H8 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H10 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null & espaces.get(6).getAmphipode() == null) { // H10 et H11
																																															// vides:
																																															// H11
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

			// ROOM D4
			espace = espaces.get(22);
			if (espace.getAmphipode() != null) {
				if (espace.getAmphipode().getNbDeplacements() > 0) {

					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(0).getAmphipode() == null
						& espaces.get(1).getAmphipode() == null & espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H1 et H2
																																															// et H4 et
																																															// H6 et H8
																																															// vides: H1
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(0));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(0));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(1).getAmphipode() == null
						& espaces.get(2).getAmphipode() == null & espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H2 et H4 et H6 et H8 vides: H2 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(1));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(1));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(2).getAmphipode() == null
						& espaces.get(3).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H4 et H6 et H8 vides: H4 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(2));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(2));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(3).getAmphipode() == null
						& espaces.get(4).getAmphipode() == null) { // H6 et H8 vide: H6 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(3));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(3));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(4).getAmphipode() == null) { // H8 vides:
																																															// H8
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(4));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(4));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null) { // H10
																																															// vides:
																																															// H10
																																															// atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(5));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(5));
							map.put(espace.getAmphipode(), list);
						}
					}
					if (espaces.get(21).getAmphipode() == null & espaces.get(20).getAmphipode() == null & espaces.get(19).getAmphipode() == null & espaces.get(5).getAmphipode() == null
						& espaces.get(6).getAmphipode() == null) { // H10 et H11 vides: H10 atteignable
						if (map.containsKey(espace.getAmphipode())) {
							map.get(espace.getAmphipode()).add(espaces.get(6));
						} else {
							List<Espace> list = new ArrayList<>();
							list.add(espaces.get(6));
							map.put(espace.getAmphipode(), list);
						}
					}
				}
			}

		}

		return map;
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day23.class.getSimpleName() + "a.txt";
		List<String> lignes = Outil.importationString(filename);
		System.out.println(lignes);

		// TODO fait a la main !

	}

}
