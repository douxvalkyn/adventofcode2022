package advent2021.day08;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day08 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day08.class.getSimpleName() + "]");
		Day08 day = new Day08();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		// lecture input
		String filename = "src/main/resources/advent2021/" + Day08.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		List<String> wires = new ArrayList<String>();
		List<String> outputs = new ArrayList<String>();
		for (String ligne : lignes) {
			String[] res = StringUtils.split(ligne, "|");
			wires.add(res[0]);
			outputs.add(res[1]);
		}

		int total = 0; // compteur de la somme totale des nombres output

		for (int i = 0; i < wires.size(); i++) { // pour chaque unique signal pattern

			// 1. initialisations
			String zero = null, un = null, deux = null, trois = null, quatre = null, cinq = null, six = null, sept = null, huit = null, neuf = null; // nombres digitaux
			char aa = 0, bb, cc, dd, ee = 0, ff, gg = 0; // positions qui forment les nomres digitaux
			String wire = wires.get(i); // signal (= 10 chiffres)
			String output = outputs.get(i); // output (= 4 chiffres)

			// 2. Determiner un, quatre, sept, huit (digits avec nb segment unique)
			String[] digits = StringUtils.split(wire, " ");

			// Determiner un
			for (String str : digits) {
				if (str.length() == 2) {
					un = str;
					break;
				}
			}

			// Determiner quatre
			for (String str : digits) {
				if (str.length() == 4) {
					quatre = str;
					break;
				}
			}

			// Determiner sept
			for (String str : digits) {
				if (str.length() == 3) {
					sept = str;
					break;
				}
			}

			// Determiner huit
			for (String str : digits) {
				if (str.length() == 7) {
					huit = str;
					break;
				}
			}

			// 3. Determiner position aa (=lettre dans sept non presente dans un)
			String res = un + sept;
			for (char j : res.toCharArray()) {
				if (res.indexOf(j) == res.lastIndexOf(j)) {
					aa = j;
					break;
				}
			}

			// 4. Determiner le zero (=segment de longueur 6 et qui n'a pas les 2 lettres qui distinguent un et quatre)
			char[] lettresQuiDistinguentUnEtQuatre = new char[2];
			res = un + quatre;
			int k = 0;
			for (char j : res.toCharArray()) {
				if (res.indexOf(j) == res.lastIndexOf(j)) {
					lettresQuiDistinguentUnEtQuatre[k] = j;
					k++;
				}
			}
			for (String str : digits) {
				if (str.length() == 6 & !((StringUtils.contains(str, lettresQuiDistinguentUnEtQuatre[0]) & StringUtils.contains(str, lettresQuiDistinguentUnEtQuatre[1])))) {
					zero = str;
				}
			}

			// 5. Determiner position bb et dd
			// zero possede une des 2 lettres dans lettresQuiDistinguentUnEtQuatre > c'est la position bb
			if (StringUtils.contains(zero, lettresQuiDistinguentUnEtQuatre[0])) {
				bb = lettresQuiDistinguentUnEtQuatre[0];
				dd = lettresQuiDistinguentUnEtQuatre[1];

			} else {
				bb = lettresQuiDistinguentUnEtQuatre[1];
				dd = lettresQuiDistinguentUnEtQuatre[0];
			}

			// 6. Determiner le six (= segment de longueur 6 et qui n'a pas les 2 lettres du un)
			char[] lettresDuUn = new char[2];
			lettresDuUn[0] = un.charAt(0);
			lettresDuUn[1] = un.charAt(1);
			for (String str : digits) {
				if (str.length() == 6 & !(StringUtils.contains(str, lettresDuUn[0]) & StringUtils.contains(str, lettresDuUn[1]))) {
					six = str;
				}
			}

			// 7. Determiner position ff (= la lettre du un qui est dans le six) et cc
			if (StringUtils.contains(six, lettresDuUn[0])) {
				ff = lettresDuUn[0];
				cc = lettresDuUn[1];
			} else {
				ff = lettresDuUn[1];
				cc = lettresDuUn[0];
			}

			// 8. Determiner le neuf (= dernier segments de longueur 6)
			for (String str : digits) {
				if (str.length() == 6 & !str.equals(six) & !str.equals(zero)) {
					neuf = str;
				}
			}

			// 9. Determiner position gg et ee
			for (int l = 0; l < neuf.length(); l++) {
				if (neuf.charAt(l) != aa & neuf.charAt(l) != bb & neuf.charAt(l) != cc & neuf.charAt(l) != dd & neuf.charAt(l) != ff) {
					gg = neuf.charAt(l);
				}
			}

			String res3 = "" + aa + bb + cc + dd + ff + gg + "abcdefg";
			for (char j : res3.toCharArray()) {
				if (res3.indexOf(j) == res3.lastIndexOf(j)) {
					ee = j;
					break;
				}
			}

			// 10. Determiner deux, trois, cinq, huit
			for (String str : digits) {
				if (str.length() == 5 & StringUtils.contains(str, aa) & StringUtils.contains(str, cc) & StringUtils.contains(str, dd) & StringUtils.contains(str, ee) & StringUtils.contains(str, gg)) {
					deux = str;
				}
				if (str.length() == 5 & StringUtils.contains(str, aa) & StringUtils.contains(str, cc) & StringUtils.contains(str, dd) & StringUtils.contains(str, ff) & StringUtils.contains(str, gg)) {
					trois = str;
				}
				if (str.length() == 5 & StringUtils.contains(str, aa) & StringUtils.contains(str, bb) & StringUtils.contains(str, dd) & StringUtils.contains(str, ff) & StringUtils.contains(str, gg)) {
					cinq = str;
				}
			}

			// 11. Decrypter l'output
			String[] nombresADecoder = StringUtils.split(output);

			// 4 nombres � decoder
			String nombreDecode = "";
			for (int index = 0; index < 4; index++) {
				if (sameChars(nombresADecoder[index], zero)) {
					nombreDecode = nombreDecode + "0";
				}
				if (sameChars(nombresADecoder[index], un)) {
					nombreDecode = nombreDecode + "1";
				}
				if (sameChars(nombresADecoder[index], deux)) {
					nombreDecode = nombreDecode + "2";
				}
				if (sameChars(nombresADecoder[index], trois)) {
					nombreDecode = nombreDecode + "3";
				}
				if (sameChars(nombresADecoder[index], quatre)) {
					nombreDecode = nombreDecode + "4";
				}
				if (sameChars(nombresADecoder[index], cinq)) {
					nombreDecode = nombreDecode + "5";
				}
				if (sameChars(nombresADecoder[index], six)) {
					nombreDecode = nombreDecode + "6";
				}
				if (sameChars(nombresADecoder[index], sept)) {
					nombreDecode = nombreDecode + "7";
				}
				if (sameChars(nombresADecoder[index], huit)) {
					nombreDecode = nombreDecode + "8";
				}
				if (sameChars(nombresADecoder[index], neuf)) {
					nombreDecode = nombreDecode + "9";
				}
			}
			Integer.parseInt(nombreDecode);
			total = total + Integer.parseInt(nombreDecode);
		}
		System.out.println("Total: " + total);
	}

	private boolean sameChars(String firstStr, String secondStr) {
		char[] first = firstStr.toCharArray();
		char[] second = secondStr.toCharArray();
		Arrays.sort(first);
		Arrays.sort(second);
		return Arrays.equals(first, second);
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day08.class.getSimpleName() + ".txt";

		// Lecture input
		List<String> lignes = Outil.importationString(filename);
		List<String> wires = new ArrayList<String>();
		List<String> output = new ArrayList<String>();
		for (String ligne : lignes) {
			String[] res = StringUtils.split(ligne, "|");
			wires.add(res[0]);
			output.add(res[1]);
		}

		// recherche des output = 1,4,7,8 (= constitues de 2,4,3,7 segments)
		int cpt = 0;
		for (String out : output) {
			String[] digits = StringUtils.split(out, " ");
			for (int i = 0; i < digits.length; i++) {
				if (digits[i].length() == 2 | digits[i].length() == 4 | digits[i].length() == 3 | digits[i].length() == 7) {
					cpt++;
				}
			}
		}
		System.out.println("Nombre de un, quatre, sept, huit: " + cpt);
	}
}
