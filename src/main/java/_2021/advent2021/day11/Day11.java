package advent2021.day11;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2021.outils.Case;
import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day11 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day11.class.getSimpleName() + "]");
		Day11 day = new Day11();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		// lecture input
		String filename = "src/main/resources/advent2021/" + Day11.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}

		// init
		boolean GO = true;
		int step = 0;

		// boucle principale sur les steps
		while (GO) {
			step++;

			// +1 pour chaque case
			for (Case cell : grille.getCases()) {
				cell.setValeur(cell.getValeur() + 1);
			}

			// reperer toutes les cases qui d�passent 9
			List<Case> highlighted = new ArrayList<Case>();
			for (Case cell : grille.getCases()) {
				if (cell.getValeur() > 9) {
					highlighted.add(cell);
				}
			}

			// faire augmenter les adjacentes de chaque highlighted de 1 et rajouter celles qui flashent � la liste
			List<Case> highlightedNew = new ArrayList<Case>();
			while (highlighted.size() > 0) {
				highlightedNew.clear();
				for (Case cellH : highlighted) {
					if (!cellH.getEtat().equals("F")) {
						cellH.setEtat("F");
					}

					List<Case> adj = cellH.getCasesAdjacentesQueen(grille);
					for (Case cell : adj) {
						if (!cell.getEtat().equals("F")) {
							cell.setValeur(cell.getValeur() + 1);
						}
						if (cell.getValeur() > 9 & !cell.getEtat().equals("F") & !highlighted.contains(cell) & !highlightedNew.contains(cell)) {
							highlightedNew.add(cell);
						}
					}
					cellH.setValeur(0);
					if (!cellH.getEtat().equals("F")) {
						cellH.setEtat("F");
					}
				}

				highlighted.clear();
				for (Case c : highlightedNew) {
					highlighted.add(c);
				}
				highlightedNew.clear();
			}

			// reset l'etat F de chaque cellule !
			for (Case cell : grille.getCases()) {
				cell.setEtat("_");
			}

			// verifier si toutes les cellules sont synchro
			boolean check = true;
			for (Case cell : grille.getCases()) {
				if (cell.getValeur() != 0) {
					check = false;
					break;
				}
			}
			if (check) {
				System.out.println("SYNCHRO !!! Step: " + step);
				GO = false;
			}
		} // fin boucle sur chaque step
	}

	// run1 ----
	public void run1() {

		// lecture input
		String filename = "src/main/resources/advent2021/" + Day11.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}
		grille.affichageValeur();

		int nbFlash = 1;
		// eclairage sur 100 steps
		for (int step = 1; step < 101; step++) {
			nbFlash--;

			// +1 pour chaque case
			for (Case cell : grille.getCases()) {
				cell.setValeur(cell.getValeur() + 1);
			}

			// reperer toutes les cases qui d�passent 9
			List<Case> highlighted = new ArrayList<Case>();
			for (Case cell : grille.getCases()) {
				if (cell.getValeur() > 9) {
					highlighted.add(cell);
				}
			}

			// faire augmenter les adjacentes de chaque highlighted de 1 et rajouter celles qui flashent � la liste
			List<Case> highlightedNew = new ArrayList<Case>();
			while (highlighted.size() > 0) {
				highlightedNew.clear();
				for (Case cellH : highlighted) {
					if (!cellH.getEtat().equals("F")) {
						cellH.setEtat("F");
						nbFlash++;
					}

					List<Case> adj = cellH.getCasesAdjacentesQueen(grille);
					for (Case cell : adj) {
						if (!cell.getEtat().equals("F")) {
							cell.setValeur(cell.getValeur() + 1);
						}
						if (cell.getValeur() > 9 & !cell.getEtat().equals("F") & !highlighted.contains(cell) & !highlightedNew.contains(cell)) {
							highlightedNew.add(cell);
						}
					}
					cellH.setValeur(0);
					if (!cellH.getEtat().equals("F")) {
						cellH.setEtat("F");
						nbFlash++;
					}

				}

				highlighted.clear();
				for (Case c : highlightedNew) {
					highlighted.add(c);
				}
				highlightedNew.clear();

			}
			// reset l'etat F de chaque cellule !
			for (Case cell : grille.getCases()) {
				cell.setEtat("_");
			}
			// System.out.println("------STEP " + step+" -------");
			// grille.affichageValeur();

			// System.out.println("nbFlash: "+nbFlash++);
		}
		System.out.println("nbFlash: " + nbFlash++);
	}

}
