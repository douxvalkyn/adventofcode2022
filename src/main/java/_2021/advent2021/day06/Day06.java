package advent2021.day06;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day06 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day06.class.getSimpleName() + "]");
		Day06 day = new Day06();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2021/" + Day06.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		String[] nombres = StringUtils.split(lignes.get(0), ",");
		List<Integer> lanternfish = new LinkedList<Integer>();
		for (int index = 0; index < nombres.length; index++)
			lanternfish.add(Integer.parseInt(nombres[index]));
		// System.out.println(lanternfish);

		// boucle principale
		int max = 18;
		for (int i = 0; i < max; i++) {

			// diminution de 1 de chaque nombre
			lanternfish = lanternfish.stream().map(l -> l - 1).collect(Collectors.toList());

			// si un -1 apparait, le remplacer par un 6 et faire apparaitre un 8 en fin de liste
			for (int j = 0; j < lanternfish.size(); j++) {
				if (lanternfish.get(j) == -1) {
					lanternfish.set(j, 6);
					lanternfish.add(8);
				}
			}
			// System.out.println(lanternfish);
		}
		// System.out.println(lanternfish);
		System.out.println("taille: " + lanternfish.size());
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day06.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		String[] nombres = StringUtils.split(lignes.get(0), ",");
		List<Integer> lanternfish = new ArrayList<Integer>();

		// boucle principale
		int max = 256;
		int[] compteurs = new int[9];
		List<List<Integer>> tablesPassages = new ArrayList<>();

		// tablePassage
		for (int m = 0; m < 9; m++) {
			lanternfish.removeAll(lanternfish);
			lanternfish.add(m);
			List<Integer> tablePassage = new ArrayList<>();
			tablesPassages.add(tablePassage);
			for (int i = 0; i < max / 2; i++) {
				// diminution de 1 de chaque nombre
				lanternfish = lanternfish.stream().map(l -> l - 1).collect(Collectors.toList());

				// si un -1 apparait, le remplacer par un 6 et faire apparaitre un 8 en fin de liste
				for (int j = 0; j < lanternfish.size(); j++) {
					if (lanternfish.get(j) == -1) {
						lanternfish.set(j, 6);
						lanternfish.add(8);
					}
				}
			}
			for (int k = 0; k < 9; k++) {
				tablesPassages.get(m).add(Collections.frequency(lanternfish, k));
			}
		}

		// ajout de l'input
		lanternfish.removeAll(lanternfish);
		for (int index = 0; index < nombres.length; index++) {
			lanternfish.add(Integer.parseInt(nombres[index]));
		}

		// Compteurs
		for (int i = 0; i < (max / 2); i++) {

			// diminution de 1 de chaque nombre
			lanternfish = lanternfish.stream().map(l -> l - 1).collect(Collectors.toList());

			// si un -1 apparait, le remplacer par un 6 et faire apparaitre un 8 en fin de la liste
			for (int j = 0; j < lanternfish.size(); j++) {
				if (lanternfish.get(j) == -1) {
					lanternfish.set(j, 6);
					lanternfish.add(8);
				}
			}
		}

		for (int k = 0; k < 9; k++) {
			compteurs[k] = Collections.frequency(lanternfish, k);
		}

		// tablePassageRemplissage
		long cpt = 0;
		for (int m = 0; m < 9; m++) {
			int nb = compteurs[m];
			for (int j = 0; j < 9; j++) {
				long res = (long) nb * (long) tablesPassages.get(m).get(j);
				cpt = cpt + res;
			}
		}
		System.out.println("cpt: " + cpt);
	}
}
