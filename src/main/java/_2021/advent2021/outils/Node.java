package advent2021.outils;

public class Node {

	int value;
	Node nextNode;

	public Node(int value) {
		this.value = value;
	}
}
