package advent2021.outils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.day01.Day01;
import advent2021.day02.Day02;
import advent2021.day03.Day03;
import advent2021.day04.Day04;
import advent2021.day05.Day05;
import advent2021.day06.Day06;
import advent2021.day07.Day07;
import advent2021.day08.Day08;
import advent2021.day09.Day09;
import advent2021.day10.Day10;
import advent2021.day11.Day11;
import advent2021.day12.Day12;
import advent2021.day13.Day13;
import advent2021.day14.Day14;
import advent2021.day15.Day15;
import advent2021.day16.Day16;
import advent2021.day17.Day17;
import advent2021.day18.Day18;
import advent2021.day19.Day19;
import advent2021.day20.Day20;
import advent2021.day21.Day21;
import advent2021.day22.Day22;
import advent2021.day23.Day23;
import advent2021.day24.Day24;
import advent2021.day25.Day25;

public class Timings {

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";
	private static final String ANSI_BRIGHT_GREEN = "\u001B[92m";
	private static final String ANSI_YELLOW = "\u001B[33m";
	private static final String ANSI_PURPLE = "\u001B[35m";
	private static final String ANSI_BLUE = "\u001B[34m";
	private static final String ANSI_BRIGHT_MAGENTA = "\u001B[95m";

	public static List<Long> dureesRun1;
	public static List<Long> dureesRun2;
	
	public static void main(String[] args) throws IOException {
		List<Long> dureesRun1 = new ArrayList<Long>();
		List<Long> dureesRun2 = new ArrayList<Long>();

		// Day01
		System.out.println("----- DAY 01 -----");
		long start = System.currentTimeMillis();
		Day01 day01 = new Day01();
		day01.run1();
		long duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day01.run2();
		long duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day02
		System.out.println("----- DAY 02 -----");
		start = System.currentTimeMillis();
		Day02 day02 = new Day02();
		day02.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day02.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day03
		System.out.println("----- DAY 03 -----");
		start = System.currentTimeMillis();
		Day03 day03 = new Day03();
		// day03.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		// day03.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day04
		System.out.println("----- DAY 04 -----");
		start = System.currentTimeMillis();
		Day04 day04 = new Day04();
		day02.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day04.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day05
		System.out.println("----- DAY 05 -----");
		start = System.currentTimeMillis();
		Day05 day05 = new Day05();
		day05.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day05.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day06
		System.out.println("----- DAY 06 -----");
		start = System.currentTimeMillis();
		Day06 day06 = new Day06();
		day06.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day06.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day07
		System.out.println("----- DAY 07 -----");
		start = System.currentTimeMillis();
		Day07 day07 = new Day07();
		day07.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day07.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day08
		System.out.println("----- DAY 08 -----");
		start = System.currentTimeMillis();
		Day08 day08 = new Day08();
		day08.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day08.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day09
		System.out.println("----- DAY 09 -----");
		start = System.currentTimeMillis();
		Day09 day09 = new Day09();
		day09.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day09.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day10
		System.out.println("----- DAY 10 -----");
		start = System.currentTimeMillis();
		Day10 day10 = new Day10();
		day10.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day10.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day11
		System.out.println("----- DAY 11 -----");
		start = System.currentTimeMillis();
		Day11 day11 = new Day11();
		day11.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day11.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day12
		System.out.println("----- DAY 12 -----");
		start = System.currentTimeMillis();
		Day12 day12 = new Day12();
		day12.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day12.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day13
		System.out.println("----- DAY 13 -----");
		start = System.currentTimeMillis();
		Day13 day13 = new Day13();
		day13.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day13.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day14
		System.out.println("----- DAY 14 -----");
		start = System.currentTimeMillis();
		Day14 day14 = new Day14();
		day14.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day14.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day15
		System.out.println("----- DAY 15 -----");
		start = System.currentTimeMillis();
		Day15 day15 = new Day15();
		day15.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day15.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day16
		System.out.println("----- DAY 16 -----");
		start = System.currentTimeMillis();
		Day16 day16 = new Day16();
		day16.run();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day16.run();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day17
		System.out.println("----- DAY 17 -----");
		start = System.currentTimeMillis();
		Day17 day17 = new Day17();
		day17.run();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day17.run();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day18
		System.out.println("----- DAY 18 -----");
		start = System.currentTimeMillis();
		Day18 day18 = new Day18();
		day18.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day18.run1();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day19
		System.out.println("----- DAY 19 -----");
		start = System.currentTimeMillis();
		Day19 day19 = new Day19();
		// day19.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		// day19.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day20
		System.out.println("----- DAY 20 -----");
		start = System.currentTimeMillis();
		Day20 day20 = new Day20();
		day20.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day20.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day21
		System.out.println("----- DAY 21 -----");
		start = System.currentTimeMillis();
		Day21 day21 = new Day21();
		day21.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day21.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day22
		System.out.println("----- DAY 22 -----");
		start = System.currentTimeMillis();
		Day22 day22 = new Day22();
		day22.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();

		duree2 = 1000000000;
		dureesRun2.add(duree2);

		// Day23
		System.out.println("----- DAY 23 -----");
		start = System.currentTimeMillis();
		Day23 day23 = new Day23();
		day23.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day23.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day24
		System.out.println("----- DAY 24 -----");
		start = System.currentTimeMillis();
		Day24 day24 = new Day24();
		day24.run();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day24.run();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		// Day25
		System.out.println("----- DAY 25 -----");
		start = System.currentTimeMillis();
		Day25 day25 = new Day25();
		day25.run1();
		duree = System.currentTimeMillis() - start;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day25.run1();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);

		Timings.dureesRun1=dureesRun1;
		Timings.dureesRun2=dureesRun2;
		
		System.out.println("-------------------------------------------------------");
		System.out.println("------------------" + ANSI_BLUE + "Advent of Code 2021" + ANSI_RESET + "------------------");
		System.out.println("------------------------" + ANSI_BLUE + "Timings" + ANSI_RESET + "------------------------");
		System.out.println("-------------------------------------------------------");
		for (int i = 0; i < dureesRun1.size(); i++) {
			long calcul1 = dureesRun1.get(i);
			String calculString1 = "" + calcul1;
			long calcul2 = dureesRun2.get(i);
			String calculString2 = "" + calcul2;
			if (i < 9) {
				System.out
					.println("[ Day" + (i + 1) + " -  Run1: " + StringUtils.leftPad(colored(calculString1), 18, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 18, ".") + " ms]]");
			}
			if (i >= 9) {
				System.out
					.println("[ Day" + (i + 1) + " - Run1: " + StringUtils.leftPad(colored(calculString1), 18, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 18, ".") + " ms]]");
			}
		}
		long sum = 0;
		for (long dur : dureesRun1) {
			sum = sum + dur;
		}
		for (long dur : dureesRun2) {
			sum = sum + dur;
		}
		long sumSecondes = sum / 1000;
		System.out.println("[ Total:      " + sum + " ms soit " + sumSecondes + " s]");
	}

	private static String colored(String calculString) {
		String res = "";
		if (Integer.parseInt(calculString) < 100) {
			res = ANSI_BRIGHT_GREEN + calculString + ANSI_RESET;
		}
		if (Integer.parseInt(calculString) >= 1000 & Integer.parseInt(calculString) < 10000) {
			res = ANSI_GREEN + calculString + ANSI_RESET;
		}
		if (Integer.parseInt(calculString) >= 10000 & Integer.parseInt(calculString) < 100000) {
			res = ANSI_YELLOW + calculString + ANSI_RESET;
		}
		if (Integer.parseInt(calculString) >= 100000 & Integer.parseInt(calculString) < 1000000) {
			res = ANSI_RED + calculString + ANSI_RESET;
		}
		if (Integer.parseInt(calculString) >= 1000000 & Integer.parseInt(calculString) < 10000000) {
			res = ANSI_PURPLE + calculString + ANSI_RESET;
		}
		if (Integer.parseInt(calculString) > 1000000) {
			res = ANSI_BRIGHT_MAGENTA + calculString + ANSI_RESET;
		}
		return res;
	}

}
