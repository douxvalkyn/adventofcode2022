package advent2021.day19;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day19 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day19.class.getSimpleName() + "]");
		Day19 day = new Day19();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// TODO LIRE pour comprendre les 24 orientations: http://www.euclideanspace.com/maths/algebra/matrix/transforms/examples/index.htm

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day19.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// Lecture Input------
		// scanners contient les differents scanners
		List<List<String>> scannersTemp = splitConventional(lignes);
		List<List<Point3D>> scanners = lectureScanners(scannersTemp);

		// Initialisation-----
		// Liste des coordonnees des SCANNERS dans le referentiel du scanner 0
		List<Point3D> COORD_SCANNERS = new ArrayList<Point3D>();

		// Liste des coordonnees des BALISES dans le referentiel du scanner 0
		List<Point3D> COORD_BALISES = new ArrayList<Point3D>();

		// Traitements------
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!On modifie les balises du second scanner !!!!!!!!!!!!!!!!!!!!!!!!!
		// modifieBalisesDansRef0(scanners, indice_premier_scanner, indice_second_scanner, numeroConfig);
		// !! le premier scanner doit etre en config 0 !!
		modifieBalisesDansRef0(scanners, 0, 17, 18);
		modifieBalisesDansRef0(scanners, 17, 32, 5);
		modifieBalisesDansRef0(scanners, 32, 25, 8);
		modifieBalisesDansRef0(scanners, 17, 9, 16); // l'inverse de la 22 c'est la 16
		modifieBalisesDansRef0(scanners, 9, 26, 23);
		modifieBalisesDansRef0(scanners, 25, 2, 1); // l'inverse de la 3 c'est la 1
		modifieBalisesDansRef0(scanners, 25, 4, 13); // l'inverse de la 19 c'est la 13
		modifieBalisesDansRef0(scanners, 4, 21, 22);
		modifieBalisesDansRef0(scanners, 21, 14, 15); // l'inverse de la 21 c'est la 15
		modifieBalisesDansRef0(scanners, 14, 27, 14);
		modifieBalisesDansRef0(scanners, 27, 11, 8); // l'inverse de la 8 c'est la 8
		modifieBalisesDansRef0(scanners, 11, 7, 6); // l'inverse de la 6 c'est la 6
		modifieBalisesDansRef0(scanners, 11, 6, 14); // l'inverse de la 14 c'est la 14
		modifieBalisesDansRef0(scanners, 6, 13, 2);
		modifieBalisesDansRef0(scanners, 13, 12, 23); // l'inverse de la 7 c'est la 23
		modifieBalisesDansRef0(scanners, 12, 1, 12); // l'inverse de la 4 c'est la 12
		modifieBalisesDansRef0(scanners, 1, 7, 0);
		modifieBalisesDansRef0(scanners, 7, 10, 11);
		modifieBalisesDansRef0(scanners, 10, 34, 13);
		modifieBalisesDansRef0(scanners, 1, 23, 21);
		modifieBalisesDansRef0(scanners, 23, 3, 5); // l'inverse de la 2 c'est la 5
		modifieBalisesDansRef0(scanners, 23, 18, 7); // l'inverse de la 23 c'est la 7
		modifieBalisesDansRef0(scanners, 18, 29, 5);
		modifieBalisesDansRef0(scanners, 29, 33, 14); // l'inverse de la 14 c'est la 14
		modifieBalisesDansRef0(scanners, 29, 8, 4); // l'inverse de la 4 c'est la 12
		modifieBalisesDansRef0(scanners, 8, 19, 3);
		modifieBalisesDansRef0(scanners, 19, 31, 17);
		modifieBalisesDansRef0(scanners, 19, 28, 18);
		modifieBalisesDansRef0(scanners, 28, 22, 9);// l'inverse de la 9 c'est la 9
		modifieBalisesDansRef0(scanners, 22, 16, 17);
		modifieBalisesDansRef0(scanners, 16, 30, 23);
		modifieBalisesDansRef0(scanners, 16, 35, 17);
		modifieBalisesDansRef0(scanners, 30, 20, 10); // l'inverse de la 10 c'est la 10
		modifieBalisesDansRef0(scanners, 20, 34, 0);
		modifieBalisesDansRef0(scanners, 30, 24, 15); // l'inverse de la 21 c'est la 15
		modifieBalisesDansRef0(scanners, 24, 15, 20);
		modifieBalisesDansRef0(scanners, 1, 36, 12);
		modifieBalisesDansRef0(scanners, 36, 5, 19);

		// Partie 1 -- Detecter les liens entre scanners
		// Prendre le scanner 0 qui sera la reference
		for (int indice_premier_scanner = 0; indice_premier_scanner <= 36; indice_premier_scanner++) {
			for (int indice_second_scanner = indice_premier_scanner; indice_second_scanner <= 36; indice_second_scanner++) {

				List<Point3D> scanner0 = scanners.get(indice_premier_scanner);

				// Prendre chacun des autres scanners, cr�er les 24 orientations et comparer ces 24 listes au scanner0
				// pour comparer, calculer la distance entre les x, puis entre les y et enfin les z
				/// si on trouve le m�me nombre, il y a correspondance

				List<List<Point3D>> listeDifferences = new ArrayList<List<Point3D>>();
				List<List<Point3D>> SecondScanner = cree24Orientations(scanners.get(indice_second_scanner));

				for (List<Point3D> scan1Configurations : SecondScanner) { // scan contient une liste de balises/points
					List<Point3D> differences = new ArrayList<Point3D>();
					for (Point3D pointScan1 : scan1Configurations) {

						for (Point3D pointScan0 : scanner0) {
							int diffX = (pointScan1.getX() - pointScan0.getX());
							int diffY = (pointScan1.getY() - pointScan0.getY());
							int diffZ = (pointScan1.getZ() - pointScan0.getZ());
							Point3D coord = new Point3D(diffX, diffY, diffZ);
							differences.add(coord);
						}
					}
					listeDifferences.add(differences);
				}

				// liste coordonnees contient, pour chaque configuration, les differences entre tous les points
				// il faut verifier pour chaque config s'il y a 12 doublons
				for (int numeroConfig = 0; numeroConfig < 24; numeroConfig++) {

					// on recherche les 12 doublons
					Map<Point3D, Long> map = countByForEachLoopWithGetOrDefault(listeDifferences.get(numeroConfig));

					if (map.containsValue(12L)) {
						System.out.println("---------------------------------------------------------------------------------------------------");
						System.out.println("Scanner " + indice_premier_scanner + " overlaps with scanner  " + indice_second_scanner + " " + getKey(map, 12L) + " - config: " + +numeroConfig);

					}
				}
			}
		}

		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-");

		// Ajout des coordonn�es des scanners (par deduction manuelle ...)
		// dans l'ordre du scanner 0 � n
		Point3D S0 = new Point3D(0, 0, 0); //
		COORD_SCANNERS.add(S0);
		Point3D S1 = new Point3D(2441, 2327, 32);//
		COORD_SCANNERS.add(S1);
		Point3D S2 = new Point3D(1229, -1194, -2365);//
		COORD_SCANNERS.add(S2);
		Point3D S3 = new Point3D(1164, 2447, -1148);//
		COORD_SCANNERS.add(S3);
		Point3D S4 = new Point3D(1091, 1162, -2279);//
		COORD_SCANNERS.add(S4);
		Point3D S5 = new Point3D(1177, 1304, 62);//
		COORD_SCANNERS.add(S5);
		Point3D S6 = new Point3D(3520, 1164, 1251);//
		COORD_SCANNERS.add(S6);
		Point3D S7 = new Point3D(3497, 2466, 125);///
		COORD_SCANNERS.add(S7);
		Point3D S8 = new Point3D(2353, 3695, -3617);//
		COORD_SCANNERS.add(S8);
		Point3D S9 = new Point3D(1085, 113, -1077);//
		COORD_SCANNERS.add(S9);
		Point3D S10 = new Point3D(3644, 2461, -1102);//
		COORD_SCANNERS.add(S10);
		Point3D S11 = new Point3D(3658, 1148, 99);//
		COORD_SCANNERS.add(S11);
		Point3D S12 = new Point3D(2282, 2449, 1295);//
		COORD_SCANNERS.add(S12);
		Point3D S13 = new Point3D(2332, 1135, 1209);//
		COORD_SCANNERS.add(S13);
		Point3D S14 = new Point3D(2434, 1185, -1060);//
		COORD_SCANNERS.add(S14);
		Point3D S15 = new Point3D(3594, 7195, -2276);//
		COORD_SCANNERS.add(S15);
		Point3D S16 = new Point3D(4760, 4885, -2420);//
		COORD_SCANNERS.add(S16);
		Point3D S17 = new Point3D(17, -57, -1068);//
		COORD_SCANNERS.add(S17);
		Point3D S18 = new Point3D(2320, 3668, -1086);//
		COORD_SCANNERS.add(S18);
		Point3D S19 = new Point3D(3646, 3677, -3540);//
		COORD_SCANNERS.add(S19);
		Point3D S20 = new Point3D(3595, 4882, -1227);//
		COORD_SCANNERS.add(S20);
		Point3D S21 = new Point3D(2304, 1222, -2257);//
		COORD_SCANNERS.add(S21);
		Point3D S22 = new Point3D(4806, 3551, -2346);//
		COORD_SCANNERS.add(S22);
		Point3D S23 = new Point3D(2344, 2410, -1069);//
		COORD_SCANNERS.add(S23);
		Point3D S24 = new Point3D(3565, 5957, -2421);
		COORD_SCANNERS.add(S24);
		Point3D S25 = new Point3D(1271, 34, -2375);//
		COORD_SCANNERS.add(S25);
		Point3D S26 = new Point3D(2371, -78, -1176);//
		COORD_SCANNERS.add(S26);
		Point3D S27 = new Point3D(3575, 1313, -1041);//
		COORD_SCANNERS.add(S27);
		Point3D S28 = new Point3D(4741, 3709, -3496);//
		COORD_SCANNERS.add(S28);
		Point3D S29 = new Point3D(2295, 3547, -2400);//
		COORD_SCANNERS.add(S29);
		Point3D S30 = new Point3D(3587, 4726, -2331);//
		COORD_SCANNERS.add(S30);
		Point3D S31 = new Point3D(3594, 3558, -4695);//
		COORD_SCANNERS.add(S31);
		Point3D S32 = new Point3D(54, 109, -2401);//
		COORD_SCANNERS.add(S32);
		Point3D S33 = new Point3D(3510, 3624, -2322);//
		COORD_SCANNERS.add(S33);
		Point3D S34 = new Point3D(3508, 3701, -1106);//
		COORD_SCANNERS.add(S34);
		Point3D S35 = new Point3D(4762, 4906, -3542);//
		COORD_SCANNERS.add(S35);
		Point3D S36 = new Point3D(2469, 1165, -22);//
		COORD_SCANNERS.add(S36);

		// Modifier les balises en les mettant dans le referentiel S0 par transformation
		for (int i = 0; i < scanners.size(); i++) {
			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				balise.setX(COORD_SCANNERS.get(i).getX() + balise.getX());
				balise.setY(COORD_SCANNERS.get(i).getY() + balise.getY());
				balise.setZ(COORD_SCANNERS.get(i).getZ() + balise.getZ());
			}
		}

		// ajout des balises sans doublons
		for (int i = 0; i < scanners.size(); i++) {

			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				if (!COORD_BALISES.contains(balise)) {
					COORD_BALISES.add(balise);
				}
			}
		}

		System.out.println(COORD_BALISES.size());

		// Calcul des distances de manhattan
		int maxDist = 0;
		for (int i = 0; i < COORD_SCANNERS.size() - 1; i++) {
			Point3D scanner0 = COORD_SCANNERS.get(i);

			for (int j = (i + 1); j < COORD_SCANNERS.size(); j++) {
				Point3D scanner1 = COORD_SCANNERS.get(j);

				int distX = Math.abs(scanner1.getX() - scanner0.getX());
				int distY = Math.abs(scanner1.getY() - scanner0.getY());
				int distZ = Math.abs(scanner1.getZ() - scanner0.getZ());
				int dist = distX + distY + distZ;
				if (dist > maxDist) {
					maxDist = dist;
				}
			}
		}
		System.out.println("maxDist: " + maxDist);

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day19.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// Lecture Input------
		// scanners contient les differents scanners
		List<List<String>> scannersTemp = splitConventional(lignes);
		List<List<Point3D>> scanners = lectureScanners(scannersTemp);

		// Initialisation-----
		// Liste des coordonnees des SCANNERS dans le referentiel du scanner 0
		List<Point3D> COORD_SCANNERS = new ArrayList<Point3D>();

		// Liste des coordonnees des BALISES dans le referentiel du scanner 0
		List<Point3D> COORD_BALISES = new ArrayList<Point3D>();

		// Traitements------
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!On modifie les balises du second scanner !!!!!!!!!!!!!!!!!!!!!!!!!
		// modifieBalisesDansRef0(scanners, indice_premier_scanner, indice_second_scanner, numeroConfig);
		// !! le premier scanner doit etre en config 0 !!
		modifieBalisesDansRef0(scanners, 0, 17, 18);
		modifieBalisesDansRef0(scanners, 17, 32, 5);
		modifieBalisesDansRef0(scanners, 32, 25, 8);
		modifieBalisesDansRef0(scanners, 17, 9, 16); // l'inverse de la 22 c'est la 16
		modifieBalisesDansRef0(scanners, 9, 26, 23);
		modifieBalisesDansRef0(scanners, 25, 2, 1); // l'inverse de la 3 c'est la 1
		modifieBalisesDansRef0(scanners, 25, 4, 13); // l'inverse de la 19 c'est la 13
		modifieBalisesDansRef0(scanners, 4, 21, 22);
		modifieBalisesDansRef0(scanners, 21, 14, 15); // l'inverse de la 21 c'est la 15
		modifieBalisesDansRef0(scanners, 14, 27, 14);
		modifieBalisesDansRef0(scanners, 27, 11, 8); // l'inverse de la 8 c'est la 8
		modifieBalisesDansRef0(scanners, 11, 7, 6); // l'inverse de la 6 c'est la 6
		modifieBalisesDansRef0(scanners, 11, 6, 14); // l'inverse de la 14 c'est la 14
		modifieBalisesDansRef0(scanners, 6, 13, 2);
		modifieBalisesDansRef0(scanners, 13, 12, 23); // l'inverse de la 7 c'est la 23
		modifieBalisesDansRef0(scanners, 12, 1, 12); // l'inverse de la 4 c'est la 12
		modifieBalisesDansRef0(scanners, 1, 7, 0);
		modifieBalisesDansRef0(scanners, 7, 10, 11);
		modifieBalisesDansRef0(scanners, 10, 34, 13);
		modifieBalisesDansRef0(scanners, 1, 23, 21);
		modifieBalisesDansRef0(scanners, 23, 3, 5); // l'inverse de la 2 c'est la 5
		modifieBalisesDansRef0(scanners, 23, 18, 7); // l'inverse de la 23 c'est la 7
		modifieBalisesDansRef0(scanners, 18, 29, 5);
		modifieBalisesDansRef0(scanners, 29, 33, 14); // l'inverse de la 14 c'est la 14
		modifieBalisesDansRef0(scanners, 29, 8, 4); // l'inverse de la 4 c'est la 12
		modifieBalisesDansRef0(scanners, 8, 19, 3);
		modifieBalisesDansRef0(scanners, 19, 31, 17);
		modifieBalisesDansRef0(scanners, 19, 28, 18);
		modifieBalisesDansRef0(scanners, 28, 22, 9);// l'inverse de la 9 c'est la 9
		modifieBalisesDansRef0(scanners, 22, 16, 17);
		modifieBalisesDansRef0(scanners, 16, 30, 23);
		modifieBalisesDansRef0(scanners, 16, 35, 17);
		modifieBalisesDansRef0(scanners, 30, 20, 10); // l'inverse de la 10 c'est la 10
		modifieBalisesDansRef0(scanners, 20, 34, 0);
		modifieBalisesDansRef0(scanners, 30, 24, 15); // l'inverse de la 21 c'est la 15
		modifieBalisesDansRef0(scanners, 24, 15, 20);
		modifieBalisesDansRef0(scanners, 1, 36, 12);
		modifieBalisesDansRef0(scanners, 36, 5, 19);

		// Partie 1 -- Detecter les liens entre scanners
		// Prendre le scanner 0 qui sera la reference
		for (int indice_premier_scanner = 0; indice_premier_scanner <= 36; indice_premier_scanner++) {
			for (int indice_second_scanner = indice_premier_scanner; indice_second_scanner <= 36; indice_second_scanner++) {

				List<Point3D> scanner0 = scanners.get(indice_premier_scanner);

				// Prendre chacun des autres scanners, cr�er les 24 orientations et comparer ces 24 listes au scanner0
				// pour comparer, calculer la distance entre les x, puis entre les y et enfin les z
				/// si on trouve le m�me nombre, il y a correspondance

				List<List<Point3D>> listeDifferences = new ArrayList<List<Point3D>>();
				List<List<Point3D>> SecondScanner = cree24Orientations(scanners.get(indice_second_scanner));

				for (List<Point3D> scan1Configurations : SecondScanner) { // scan contient une liste de balises/points
					List<Point3D> differences = new ArrayList<Point3D>();
					for (Point3D pointScan1 : scan1Configurations) {

						for (Point3D pointScan0 : scanner0) {
							int diffX = (pointScan1.getX() - pointScan0.getX());
							int diffY = (pointScan1.getY() - pointScan0.getY());
							int diffZ = (pointScan1.getZ() - pointScan0.getZ());
							Point3D coord = new Point3D(diffX, diffY, diffZ);
							differences.add(coord);
						}
					}
					listeDifferences.add(differences);
				}

				// liste coordonnees contient, pour chaque configuration, les differences entre tous les points
				// il faut verifier pour chaque config s'il y a 12 doublons
				for (int numeroConfig = 0; numeroConfig < 24; numeroConfig++) {

					// on recherche les 12 doublons
					Map<Point3D, Long> map = countByForEachLoopWithGetOrDefault(listeDifferences.get(numeroConfig));

					if (map.containsValue(12L)) {
						System.out.println("---------------------------------------------------------------------------------------------------");
						System.out.println("Scanner " + indice_premier_scanner + " overlaps with scanner  " + indice_second_scanner + " " + getKey(map, 12L) + " - config: " + +numeroConfig);

					}
				}
			}
		}

		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-");

		// Ajout des coordonn�es des scanners (par deduction manuelle ...)
		// dans l'ordre du scanner 0 � n
		Point3D S0 = new Point3D(0, 0, 0); //
		COORD_SCANNERS.add(S0);
		Point3D S1 = new Point3D(2441, 2327, 32);//
		COORD_SCANNERS.add(S1);
		Point3D S2 = new Point3D(1229, -1194, -2365);//
		COORD_SCANNERS.add(S2);
		Point3D S3 = new Point3D(1164, 2447, -1148);//
		COORD_SCANNERS.add(S3);
		Point3D S4 = new Point3D(1091, 1162, -2279);//
		COORD_SCANNERS.add(S4);
		Point3D S5 = new Point3D(1177, 1304, 62);//
		COORD_SCANNERS.add(S5);
		Point3D S6 = new Point3D(3520, 1164, 1251);//
		COORD_SCANNERS.add(S6);
		Point3D S7 = new Point3D(3497, 2466, 125);///
		COORD_SCANNERS.add(S7);
		Point3D S8 = new Point3D(2353, 3695, -3617);//
		COORD_SCANNERS.add(S8);
		Point3D S9 = new Point3D(1085, 113, -1077);//
		COORD_SCANNERS.add(S9);
		Point3D S10 = new Point3D(3644, 2461, -1102);//
		COORD_SCANNERS.add(S10);
		Point3D S11 = new Point3D(3658, 1148, 99);//
		COORD_SCANNERS.add(S11);
		Point3D S12 = new Point3D(2282, 2449, 1295);//
		COORD_SCANNERS.add(S12);
		Point3D S13 = new Point3D(2332, 1135, 1209);//
		COORD_SCANNERS.add(S13);
		Point3D S14 = new Point3D(2434, 1185, -1060);//
		COORD_SCANNERS.add(S14);
		Point3D S15 = new Point3D(3594, 7195, -2276);//
		COORD_SCANNERS.add(S15);
		Point3D S16 = new Point3D(4760, 4885, -2420);//
		COORD_SCANNERS.add(S16);
		Point3D S17 = new Point3D(17, -57, -1068);//
		COORD_SCANNERS.add(S17);
		Point3D S18 = new Point3D(2320, 3668, -1086);//
		COORD_SCANNERS.add(S18);
		Point3D S19 = new Point3D(3646, 3677, -3540);//
		COORD_SCANNERS.add(S19);
		Point3D S20 = new Point3D(3595, 4882, -1227);//
		COORD_SCANNERS.add(S20);
		Point3D S21 = new Point3D(2304, 1222, -2257);//
		COORD_SCANNERS.add(S21);
		Point3D S22 = new Point3D(4806, 3551, -2346);//
		COORD_SCANNERS.add(S22);
		Point3D S23 = new Point3D(2344, 2410, -1069);//
		COORD_SCANNERS.add(S23);
		Point3D S24 = new Point3D(3565, 5957, -2421);
		COORD_SCANNERS.add(S24);
		Point3D S25 = new Point3D(1271, 34, -2375);//
		COORD_SCANNERS.add(S25);
		Point3D S26 = new Point3D(2371, -78, -1176);//
		COORD_SCANNERS.add(S26);
		Point3D S27 = new Point3D(3575, 1313, -1041);//
		COORD_SCANNERS.add(S27);
		Point3D S28 = new Point3D(4741, 3709, -3496);//
		COORD_SCANNERS.add(S28);
		Point3D S29 = new Point3D(2295, 3547, -2400);//
		COORD_SCANNERS.add(S29);
		Point3D S30 = new Point3D(3587, 4726, -2331);//
		COORD_SCANNERS.add(S30);
		Point3D S31 = new Point3D(3594, 3558, -4695);//
		COORD_SCANNERS.add(S31);
		Point3D S32 = new Point3D(54, 109, -2401);//
		COORD_SCANNERS.add(S32);
		Point3D S33 = new Point3D(3510, 3624, -2322);//
		COORD_SCANNERS.add(S33);
		Point3D S34 = new Point3D(3508, 3701, -1106);//
		COORD_SCANNERS.add(S34);
		Point3D S35 = new Point3D(4762, 4906, -3542);//
		COORD_SCANNERS.add(S35);
		Point3D S36 = new Point3D(2469, 1165, -22);//
		COORD_SCANNERS.add(S36);

		// Modifier les balises en les mettant dans le referentiel S0 par transformation
		for (int i = 0; i < scanners.size(); i++) {
			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				balise.setX(COORD_SCANNERS.get(i).getX() + balise.getX());
				balise.setY(COORD_SCANNERS.get(i).getY() + balise.getY());
				balise.setZ(COORD_SCANNERS.get(i).getZ() + balise.getZ());
			}
		}

		// ajout des balises sans doublons
		for (int i = 0; i < scanners.size(); i++) {

			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				if (!COORD_BALISES.contains(balise)) {
					COORD_BALISES.add(balise);
				}
			}
		}

		System.out.println(COORD_BALISES.size());

	}

	// run1 ----
	public void run1_sample() {
		String filename = "src/main/resources/advent2021/" + Day19.class.getSimpleName() + "b.txt";
		List<String> lignes = Outil.importationString(filename);

		// Lecture Input------
		// scanners contient les differents scanners
		List<List<String>> scannersTemp = splitConventional(lignes);
		List<List<Point3D>> scanners = lectureScanners(scannersTemp);

		// Initialisation-----
		// Liste des coordonnees des SCANNERS dans le referentiel du scanner 0
		List<Point3D> COORD_SCANNERS = new ArrayList<Point3D>();

		// Liste des coordonnees des BALISES dans le referentiel du scanner 0
		List<Point3D> COORD_BALISES = new ArrayList<Point3D>();

		// Traitements------
		// modifieBalisesDansRef0(scanners, indice_premier_scanner, indice_second_scanner, numeroConfig);
		modifieBalisesDansRef0(scanners, 0, 1, 10);
		modifieBalisesDansRef0(scanners, 1, 3, 10);
		modifieBalisesDansRef0(scanners, 1, 4, 13);
		modifieBalisesDansRef0(scanners, 4, 2, 11);

		// Partie 1 -- Detecter les liens entre scanners
		// Prendre le scanner 0 qui sera la reference
		for (int indice_premier_scanner = 0; indice_premier_scanner < 5; indice_premier_scanner++) {
			for (int indice_second_scanner = indice_premier_scanner; indice_second_scanner < 5; indice_second_scanner++) {

				List<Point3D> scanner0 = scanners.get(indice_premier_scanner);

				// Prendre chacun des autres scanners, cr�er les 24 orientations et comparer ces 24 listes au scanner0
				// pour comparer, calculer la distance entre les x, puis entre les y et enfin les z
				/// si on trouve le m�me nombre, il y a correspondance

				List<List<Point3D>> listeDifferences = new ArrayList<List<Point3D>>();
				List<List<Point3D>> SecondScanner = cree24Orientations(scanners.get(indice_second_scanner));

				for (List<Point3D> scan1Configurations : SecondScanner) { // scan contient une liste de balises/points
					List<Point3D> differences = new ArrayList<Point3D>();
					for (Point3D pointScan1 : scan1Configurations) {

						for (Point3D pointScan0 : scanner0) {
							int diffX = (pointScan1.getX() - pointScan0.getX());
							int diffY = (pointScan1.getY() - pointScan0.getY());
							int diffZ = (pointScan1.getZ() - pointScan0.getZ());
							Point3D coord = new Point3D(diffX, diffY, diffZ);
							differences.add(coord);
						}
					}
					listeDifferences.add(differences);
				}

				// liste coordonnees contient, pour chaque configuration, les differences entre tous les points
				// il faut verifier pour chaque config s'il y a 12 doublons
				for (int numeroConfig = 0; numeroConfig < 24; numeroConfig++) {

					// on recherche les 12 doublons
					Map<Point3D, Long> map = countByForEachLoopWithGetOrDefault(listeDifferences.get(numeroConfig));

					if (map.containsValue(12L)) {
						System.out.println("---------------------------------------------------------------------------------------------------");
						System.out.println("Scanner " + indice_premier_scanner + " overlaps with scanner  " + indice_second_scanner + " " + getKey(map, 12L) + " - config: " + +numeroConfig);

					}
				}
			}
		}

		System.out.println("---------------------------------------------------------------------------------------------------");
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-");
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-");

		// Ajout des coordonn�es des scanners (par deduction manuelle ...)
		// dans l'ordre du scanner 0 � n
		Point3D S0 = new Point3D(0, 0, 0);
		COORD_SCANNERS.add(S0);
		Point3D S1 = new Point3D(68, -1246, -43);
		COORD_SCANNERS.add(S1);
		Point3D S2 = new Point3D(1105, -1205, 1229);
		COORD_SCANNERS.add(S2);
		Point3D S3 = new Point3D(-92, -2380, -20);
		COORD_SCANNERS.add(S3);
		Point3D S4 = new Point3D(-20, -1133, 1061);
		COORD_SCANNERS.add(S4);

		// Modifier les balises en les mettant dans le referentiel S0 par transformation
		for (int i = 0; i < scanners.size(); i++) {
			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				balise.setX(COORD_SCANNERS.get(i).getX() + balise.getX());
				balise.setY(COORD_SCANNERS.get(i).getY() + balise.getY());
				balise.setZ(COORD_SCANNERS.get(i).getZ() + balise.getZ());
			}
		}

		// ajout des balises sans doublons
		for (int i = 0; i < 5; i++) {
			List<Point3D> scanner = scanners.get(i);
			for (Point3D balise : scanner) {
				if (!COORD_BALISES.contains(balise)) {
					COORD_BALISES.add(balise);
				}
			}
		}
		System.out.println(COORD_BALISES.size());

	}

	public void modifieBalisesDansRef0(List<List<Point3D>> scanners, int indice_premier_scanner, int indice_second_scanner, int numeroConfig) {
		List<Point3D> scanner0 = scanners.get(indice_premier_scanner);
		List<List<Point3D>> secondScanner = cree24Orientations(scanners.get(indice_second_scanner));

		// on prend secondScanner en config numeroConfig, les balises seront forcement dans la ref du scanner0, on les remplace donc dans SCANNERS
		List<Point3D> balisesSecondScanner = secondScanner.get(numeroConfig);
		scanners.set(indice_second_scanner, balisesSecondScanner);
	}

	public <K, V> K getKey(Map<K, V> map, V value) {
		for (Entry<K, V> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public <T> Map<T, Long> countByForEachLoopWithGetOrDefault(List<T> inputList) {
		Map<T, Long> resultMap = new HashMap<>();
		inputList.forEach(e -> resultMap.put(e, resultMap.getOrDefault(e, 0L) + 1L));
		return resultMap;
	}

	public List<List<Point3D>> cree24Orientations(List<Point3D> scanner) {

		List<List<Point3D>> scannersOriented = new ArrayList<List<Point3D>>();

		// Orientation 0 (x,y,z) > (x,y,z)
		List<Point3D> scannerOriented1 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented1.add(pt);
		}
		for (Point3D point : scannerOriented1) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(xTemp);
			point.setY(yTemp);
			point.setZ(zTemp);
		}
		scannersOriented.add(scannerOriented1);

		// Orientation 1 (x,y,z) > (x,z,-y)
		List<Point3D> scannerOriented2 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented2.add(pt);
		}
		for (Point3D point : scannerOriented2) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(xTemp);
			point.setY(zTemp);
			point.setZ(-yTemp);
		}
		scannersOriented.add(scannerOriented2);

		// Orientation 2 (x,y,z) > (x,-y,-z)
		List<Point3D> scannerOriented3 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented3.add(pt);
		}
		for (Point3D point : scannerOriented3) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(xTemp);
			point.setY(-yTemp);
			point.setZ(-zTemp);
		}
		scannersOriented.add(scannerOriented3);

		// Orientation 3 (x,y,z) > (x,-z,y)
		List<Point3D> scannerOriented4 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented4.add(pt);
		}
		for (Point3D point : scannerOriented4) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(xTemp);
			point.setY(-zTemp);
			point.setZ(yTemp);
		}
		scannersOriented.add(scannerOriented4);

		// Orientation 4 (x,y,z) > (y,-x,z)
		List<Point3D> scannerOriented5 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented5.add(pt);
		}
		for (Point3D point : scannerOriented5) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(yTemp);
			point.setY(-xTemp);
			point.setZ(zTemp);
		}
		scannersOriented.add(scannerOriented5);

		// Orientation 5 (x,y,z) > (y,z,x)
		List<Point3D> scannerOriented6 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented6.add(pt);
		}
		for (Point3D point : scannerOriented6) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(yTemp);
			point.setY(zTemp);
			point.setZ(xTemp);
		}
		scannersOriented.add(scannerOriented6);

		// Orientation 6 (x,y,z) > (y,x,-z)
		List<Point3D> scannerOriented7 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented7.add(pt);
		}
		for (Point3D point : scannerOriented7) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(yTemp);
			point.setY(xTemp);
			point.setZ(-zTemp);
		}
		scannersOriented.add(scannerOriented7);

		// Orientation 7 (x,y,z) > (y,-z,-x)
		List<Point3D> scannerOriented8 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented8.add(pt);
		}
		for (Point3D point : scannerOriented8) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(yTemp);
			point.setY(-zTemp);
			point.setZ(-xTemp);
		}
		scannersOriented.add(scannerOriented8);

		// Orientation 8 (x,y,z) > (-x,-y,z)
		List<Point3D> scannerOriented9 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented9.add(pt);
		}
		for (Point3D point : scannerOriented9) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-xTemp);
			point.setY(-yTemp);
			point.setZ(zTemp);
		}
		scannersOriented.add(scannerOriented9);

		// Orientation 9 (x,y,z) > (-x,-z,-y)
		List<Point3D> scannerOriented10 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented10.add(pt);
		}
		for (Point3D point : scannerOriented10) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-xTemp);
			point.setY(-zTemp);
			point.setZ(-yTemp);
		}
		scannersOriented.add(scannerOriented10);

		// Orientation 10 (x,y,z) > (-x,y,-z)
		List<Point3D> scannerOriented11 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented11.add(pt);
		}
		for (Point3D point : scannerOriented11) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-xTemp);
			point.setY(yTemp);
			point.setZ(-zTemp);
		}
		scannersOriented.add(scannerOriented11);

		// Orientation 11 (x,y,z) > (-x,z,y)
		List<Point3D> scannerOriented12 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented12.add(pt);
		}
		for (Point3D point : scannerOriented12) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-xTemp);
			point.setY(zTemp);
			point.setZ(yTemp);
		}
		scannersOriented.add(scannerOriented12);

		// Orientation 12 (x,y,z) > (-y,x,z)
		List<Point3D> scannerOriented13 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented13.add(pt);
		}
		for (Point3D point : scannerOriented13) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-yTemp);
			point.setY(xTemp);
			point.setZ(zTemp);
		}
		scannersOriented.add(scannerOriented13);

		// Orientation 13 (x,y,z) > (-y,-z,x)
		List<Point3D> scannerOriented14 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented14.add(pt);
		}
		for (Point3D point : scannerOriented14) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-yTemp);
			point.setY(-zTemp);
			point.setZ(xTemp);
		}
		scannersOriented.add(scannerOriented14);

		// Orientation 14 (x,y,z) > (-y,-x,-z)
		List<Point3D> scannerOriented15 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented15.add(pt);
		}
		for (Point3D point : scannerOriented15) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-yTemp);
			point.setY(-xTemp);
			point.setZ(-zTemp);
		}
		scannersOriented.add(scannerOriented15);

		// Orientation 15 (x,y,z) > (-y,z,-x)
		List<Point3D> scannerOriented16 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented16.add(pt);
		}
		for (Point3D point : scannerOriented16) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-yTemp);
			point.setY(zTemp);
			point.setZ(-xTemp);
		}
		scannersOriented.add(scannerOriented16);

		// Orientation 16 (x,y,z) > (z,y,-x)
		List<Point3D> scannerOriented17 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented17.add(pt);
		}
		for (Point3D point : scannerOriented17) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(zTemp);
			point.setY(yTemp);
			point.setZ(-xTemp);
		}
		scannersOriented.add(scannerOriented17);

		// Orientation 17 (x,y,z) > (z,x,y)
		List<Point3D> scannerOriented18 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented18.add(pt);
		}
		for (Point3D point : scannerOriented18) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(zTemp);
			point.setY(xTemp);
			point.setZ(yTemp);
		}
		scannersOriented.add(scannerOriented18);

		// Orientation 18 (x,y,z) > (z,-y,x)
		List<Point3D> scannerOriented19 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented19.add(pt);
		}
		for (Point3D point : scannerOriented19) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(zTemp);
			point.setY(-yTemp);
			point.setZ(xTemp);
		}
		scannersOriented.add(scannerOriented19);

		// Orientation 19 (x,y,z) > (z,-x,-y)
		List<Point3D> scannerOriented20 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented20.add(pt);
		}
		for (Point3D point : scannerOriented20) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(zTemp);
			point.setY(-xTemp);
			point.setZ(-yTemp);
		}
		scannersOriented.add(scannerOriented20);

		// Orientation 20 (x,y,z) > (-z,-y,-x)
		List<Point3D> scannerOriented21 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented21.add(pt);
		}
		for (Point3D point : scannerOriented21) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-zTemp);
			point.setY(-yTemp);
			point.setZ(-xTemp);
		}
		scannersOriented.add(scannerOriented21);

		// Orientation 21 (x,y,z) > (-z,-x,y)
		List<Point3D> scannerOriented22 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented22.add(pt);
		}
		for (Point3D point : scannerOriented22) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-zTemp);
			point.setY(-xTemp);
			point.setZ(yTemp);
		}
		scannersOriented.add(scannerOriented22);

		// Orientation 22 (x,y,z) > (-z,y,x)
		List<Point3D> scannerOriented23 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented23.add(pt);
		}
		for (Point3D point : scannerOriented23) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-zTemp);
			point.setY(yTemp);
			point.setZ(xTemp);
		}
		scannersOriented.add(scannerOriented23);

		// Orientation 23 (x,y,z) > (-z,x,-y)
		List<Point3D> scannerOriented24 = new ArrayList<Point3D>();
		for (int i = 0; i < scanner.size(); i++) {
			Point3D pt = new Point3D(scanner.get(i).getX(), scanner.get(i).getY(), scanner.get(i).getZ());
			scannerOriented24.add(pt);
		}
		for (Point3D point : scannerOriented24) {
			int xTemp = point.getX();
			int yTemp = point.getY();
			int zTemp = point.getZ();
			point.setX(-zTemp);
			point.setY(xTemp);
			point.setZ(-yTemp);
		}
		scannersOriented.add(scannerOriented24);

		return scannersOriented;
	}

	public List<List<Point3D>> lectureScanners(List<List<String>> scannersTemp) {
		List<List<Point3D>> scanners = new ArrayList<List<Point3D>>();
		for (List<String> scannerTemp : scannersTemp) {
			List<Point3D> scanner = new ArrayList<Point3D>();
			for (int i = 1; i < scannerTemp.size(); i++) {
				String[] nombres = StringUtils.split(scannerTemp.get(i), ",");
				int nb1 = Integer.parseInt(nombres[0]);
				int nb2 = Integer.parseInt(nombres[1]);
				int nb3 = Integer.parseInt(nombres[2]);
				Point3D point = new Point3D(nb1, nb2, nb3);
				scanner.add(point);
			}
			scanners.add(scanner);
		}
		return scanners;
	}

	public void lectureScanner(List<String> lignes) {
		for (String ligne : lignes) {
			if (ligne.charAt(0) == '-') {

			}

		}
	}

	public List<List<String>> splitConventional(List<String> input) {
		List<List<String>> result = new ArrayList<>();
		int prev = 0;

		for (int cur = 0; cur < input.size(); cur++) {
			if (input.get(cur).length() == 0) {
				result.add(input.subList(prev, cur));
				prev = cur + 1;
			}
		}
		result.add(input.subList(prev, input.size()));

		return result;
	}

}
