package advent2021.day02;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2021.outils.Outil;

public class Day02 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day02.class.getSimpleName() + "]");
		Day02 day = new Day02();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/day02.txt";
		List<String> lignes = Outil.importationString(filename);
		int horizontal = 0;
		int depth = 0;
		for (String ligne : lignes) {
			String[] instruction = ligne.split(" ");
			String deplacement = instruction[0];
			Integer valeur = Integer.valueOf(instruction[1]);

			if (deplacement.equals("forward")) {
				horizontal = horizontal + valeur;
			}
			if (deplacement.equals("up")) {
				depth = depth - valeur;
			}
			if (deplacement.equals("down")) {
				depth = depth + valeur;
			}
		}
		// System.out.println(horizontal);
		// System.out.println(depth);
		System.out.println("res: " + horizontal * depth);
	}

	// run2 ----
	public void run2() {
		String FILENAME = "src/main/resources/advent2021/day02.txt";
		List<String> lignes = Outil.importationString(FILENAME);
		int horizontal = 0;
		int depth = 0;
		int aim = 0;

		for (String ligne : lignes) {
			String[] instruction = ligne.split(" ");
			String deplacement = instruction[0];
			Integer valeur = Integer.valueOf(instruction[1]);

			if (deplacement.equals("down")) {
				aim = aim + valeur;
			}
			if (deplacement.equals("up")) {
				aim = aim - valeur;
			}
			if (deplacement.equals("forward")) {
				horizontal = horizontal + valeur;
				depth = depth + valeur * aim;
			}
		}
		// System.out.println("horiz: "+horizontal);
		// System.out.println("depth: "+depth);
		// System.out.println("aim: "+aim);
		System.out.println("res: " + horizontal * depth);
	}
}
