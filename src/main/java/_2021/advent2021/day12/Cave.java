package advent2021.day12;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Cave {

	private String lettre;
	private boolean estGrand;
	private List<Cave> cavesAdjacentes;

	public Cave(String lettre, boolean grand) {
		super();
		this.lettre = lettre;
		this.estGrand = grand;
		List<Cave> cavesAdjacentes = new LinkedList<Cave>();
		this.cavesAdjacentes = cavesAdjacentes;
	}

	public String getLettre() {
		return lettre;
	}

	public void setLettre(String lettre) {
		this.lettre = lettre;
	}

	public boolean isGrand() {
		return estGrand;
	}

	public void setGrand(boolean grand) {
		this.estGrand = grand;
	}

	public List<Cave> getCavesAdjacentes() {
		return cavesAdjacentes;
	}

	public void setCavesAdjacentes(List<Cave> cavesAdjacentes) {
		this.cavesAdjacentes = cavesAdjacentes;
	}

	public void addCavesAdjacentes(Cave cave) {
		cavesAdjacentes.add(cave);
	}

	@Override
	public int hashCode() {
		return Objects.hash(estGrand, lettre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cave other = (Cave) obj;
		return estGrand == other.estGrand && Objects.equals(lettre, other.lettre);
	}

	@Override
	public String toString() {
		return "Cave [" + lettre + "]";
	}

}
