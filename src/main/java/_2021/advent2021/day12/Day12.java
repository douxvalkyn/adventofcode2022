package advent2021.day12;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day12 {

	private int compteur = 0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day12.class.getSimpleName() + "]");
		Day12 day = new Day12();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day12.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// creation des caves
		List<Cave> caves = new LinkedList<Cave>();
		creationCaves(lignes, caves);
		ajoutCavesAdjacentes(lignes, caves);

		// recherche de la case START
		Cave start = null;
		for (Cave cave : caves) {
			if (cave.getLettre().equals("start")) {
				start = cave;
			}
		}

		// chemins
		List<Cave> chemin = new LinkedList<Cave>();
		chemin.add(start);
		visiteRun2(start, chemin);
		System.out.println(compteur);
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day12.class.getSimpleName() + "c.txt";
		List<String> lignes = Outil.importationString(filename);

		// creation des caves
		List<Cave> caves = new LinkedList<Cave>();
		creationCaves(lignes, caves);
		ajoutCavesAdjacentes(lignes, caves);

		// recherche de la case START
		Cave start = null;
		for (Cave cave : caves) {
			if (cave.getLettre().equals("start")) {
				start = cave;
			}
		}

		// chemins
		List<Cave> chemin = new LinkedList<Cave>();
		chemin.add(start);
		visite(start, chemin);
		System.out.println(compteur);
	}

	private void visiteRun2(Cave cave, List<Cave> chemin) {
		List<Cave> cavesAdj = cave.getCavesAdjacentes();
		for (Cave caveAdj : cavesAdj) {
			if (caveAdj.getLettre().equals("end")) {
				// System.out.println(chemin);
				compteur++;
			}

			if (!caveAdj.getLettre().equals("end")) {
				if (chemin.contains(caveAdj) & caveAdj.isGrand() & !caveAdj.getLettre().equals("start")) {

					// copie de chemin dans chemin2
					List<Cave> chemin2 = new LinkedList<Cave>();
					for (Cave c : chemin) {
						chemin2.add(c);
					}
					chemin2.add(caveAdj);
					visiteRun2(caveAdj, chemin2);
				}
				// verifier si chemin contient d�j� 1 seul exemplaire de caveAdj et pas d�j� 1 petit doublon
				int cpt = 0;
				for (Cave caveChemin : chemin) {
					if (caveChemin.equals(caveAdj)) {
						cpt++;
					}
				}
				Set<Cave> eventuelsDoublons = findDuplicates(chemin);
				List<Cave> eventuelsDoublons2 = new LinkedList<>(eventuelsDoublons);
				boolean dejaDautresPetitsDoublons = false;
				for (Cave caveDoublon : eventuelsDoublons2) {
					if (!caveDoublon.isGrand()) {
						dejaDautresPetitsDoublons = true;
					}
				}

				if (chemin.contains(caveAdj) & !caveAdj.isGrand() & cpt == 1 & !dejaDautresPetitsDoublons & !caveAdj.getLettre().equals("start")) {
					// copie de chemin dans chemin2
					List<Cave> chemin4 = new LinkedList<Cave>();
					for (Cave c : chemin) {
						chemin4.add(c);
					}
					chemin4.add(caveAdj);
					visiteRun2(caveAdj, chemin4);
				}
				if (chemin.contains(caveAdj) & !caveAdj.isGrand()) {
					// fin de parcours
				}
				if (!chemin.contains(caveAdj) & !caveAdj.getLettre().equals("start")) {

					// copie de chemin dans chemin3
					List<Cave> chemin3 = new LinkedList<Cave>();
					for (Cave c : chemin) {
						chemin3.add(c);
					}
					chemin3.add(caveAdj);
					visiteRun2(caveAdj, chemin3);
				}
			}
		}
	}

	private <T> Set<T> findDuplicates(Collection<T> collection) {
		Set<T> duplicates = new LinkedHashSet<>();
		Set<T> uniques = new HashSet<>();
		for (T t : collection) {
			if (!uniques.add(t)) {
				duplicates.add(t);
			}
		}
		return duplicates;
	}

	private void visite(Cave cave, List<Cave> chemin) {
		List<Cave> cavesAdj = cave.getCavesAdjacentes();
		for (Cave caveAdj : cavesAdj) {
			if (caveAdj.getLettre().equals("end")) {
				// System.out.println(chemin);
				compteur++;
			}

			if (!caveAdj.getLettre().equals("end")) {
				if (chemin.contains(caveAdj) & caveAdj.isGrand()) {

					// copie de chemin dans chemin2
					List<Cave> chemin2 = new LinkedList<Cave>();
					for (Cave c : chemin) {
						chemin2.add(c);
					}
					chemin2.add(caveAdj);
					visite(caveAdj, chemin2);
				}
				if (chemin.contains(caveAdj) & !caveAdj.isGrand()) {
					// fin de parcours
				}
				if (!chemin.contains(caveAdj)) {

					// copie de chemin dans chemin3
					List<Cave> chemin3 = new LinkedList<Cave>();
					for (Cave c : chemin) {
						chemin3.add(c);
					}
					chemin3.add(caveAdj);
					visite(caveAdj, chemin3);
				}
			}
		}
	}

	private void ajoutCavesAdjacentes(List<String> lignes, List<Cave> caves) {
		for (String str : lignes) {
			String[] splitted = StringUtils.split(str, "-");
			String debut = splitted[0];
			String fin = splitted[1];

			// recherche caves debut et fin
			Cave caveDebut = null;
			Cave caveFin = null;

			for (Cave cave : caves) {
				if (cave.getLettre().equals(debut)) {
					caveDebut = cave;
				}
				if (cave.getLettre().equals(fin)) {
					caveFin = cave;
				}
			}

			// ajout de cave fin comme adj � cave debut

			caveDebut.addCavesAdjacentes(caveFin);
			if (!caveDebut.getLettre().equals("start") & !caveFin.getLettre().equals("end")) {
				caveFin.addCavesAdjacentes(caveDebut);
			}
		}
	}

	private void creationCaves(List<String> lignes, List<Cave> caves) {
		for (String str : lignes) {
			String[] splitted = StringUtils.split(str, "-");
			String debut = splitted[0];
			String fin = splitted[1];
			boolean estGrand = true;
			if (StringUtils.isAllLowerCase(debut)) {
				estGrand = false;
			}
			Cave cave = new Cave(debut, estGrand);
			if (!caves.contains(cave)) {
				caves.add(cave);
			}
			estGrand = true;
			if (StringUtils.isAllLowerCase(fin)) {
				estGrand = false;
			}
			Cave cave2 = new Cave(fin, estGrand);
			if (!caves.contains(cave2)) {
				caves.add(cave2);
			}
		}
	}
}
