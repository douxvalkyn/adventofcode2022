package advent2021.day01;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2021.outils.Outil;

public class Day01 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day01.class.getSimpleName() + "]");
		Day01 day = new Day01();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2021/day01.txt";
		List<Integer> nombres = Outil.importationInteger(filename);
		System.out.println(nombres);
		int cpt = 0;
		for (int i = 1; i < nombres.size(); i++) {
			if (nombres.get(i) > nombres.get(i - 1)) {
				cpt++;
			}
		}
		System.out.println(cpt);

	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2021/day01.txt";
		List<Integer> nombres = Outil.importationInteger(filename);
		System.out.println("nombres" + nombres);
		int cpt = 0;
		List<Integer> nombresParTrois = new ArrayList<Integer>();
		for (int i = 0; i < nombres.size() - 2; i++) {
			nombresParTrois.add(nombres.get(i) + nombres.get(i + 1) + nombres.get(i + 2));
		}
		System.out.println("nombres par 3:" + nombresParTrois);
		for (int i = 1; i < nombresParTrois.size(); i++) {
			if (nombresParTrois.get(i) > nombresParTrois.get(i - 1)) {
				cpt++;
			}
		}
		System.out.println(cpt);
	}
}
