package advent2021.day14;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Trie.TrieBuilder;
import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day14 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day14.class.getSimpleName() + "]");
		Day14 day = new Day14();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run4() ----methode en comptant les paires sans calculer le string global -
	public void run2() {

		String filename = "src/main/resources/advent2021/" + Day14.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		String polymer = lignes.get(0); // polymer de départ
		List<String> myrules = lignes.subList(2, lignes.size()); // input avec juste les regles

		// init (on met des Long car les nombres dépassent les Integer)
		Map<String, Character> rules = new HashMap<>();
		Map<String, Long> paires = new HashMap<>();
		Map<String, Long> pairesTemp = new HashMap<>();
		Map<Character, Long> compteurs = new HashMap<>();

		// initialisations des Map qui sauvegarderont les états
		for (int i = 0; i < myrules.size(); i++) {
			String[] temp = myrules.get(i).split(" -> ");
			rules.put(temp[0], temp[1].charAt(0));
			paires.put(temp[0], 0L);
			pairesTemp.put(temp[0], 0L);
		}

		// comptage des paires initiales
		for (int i = 0; i < polymer.length() - 1; i++) {
			String paireEncours = polymer.substring(i, i + 2);
			paires.put(paireEncours, (paires.get(paireEncours) + 1));
		}

		int step = 40; // nb de STEP
		for (int i = 0; i < step; i++) {
			// on passe en revue chaque paire existante dans les regles
			for (String s : paires.keySet()) {

				String endroit = "" + s.charAt(0) + rules.get(s);
				String envers = "" + rules.get(s) + s.charAt(1);

				if (paires.get(s) > 0) { // si la paire existe, on crée les nouvelles paires dans pairesTemp
					pairesTemp.put(endroit, (pairesTemp.get(endroit) + paires.get(s)));
					pairesTemp.put(envers, (pairesTemp.get(envers) + paires.get(s)));
				}
			}
			// reset de pairesTemp et recopie de pairesTemp à paires
			for (String str : pairesTemp.keySet()) {
				paires.put(str, pairesTemp.get(str));
				pairesTemp.put(str, 0L);
			}
		} // fin boucle principale

		compteurs.put(polymer.charAt(polymer.length() - 1), 1L);

		// comptage du max et du min
		for (String s : paires.keySet()) {
			compteurs.putIfAbsent(s.charAt(0), 0L);
			compteurs.put(s.charAt(0), (compteurs.get(s.charAt(0)) + paires.get(s)));
		}

		System.out.println("max: " + Collections.max(compteurs.values()));
		System.out.println("min: " + Collections.min(compteurs.values()));
		System.out.println("Resultat:");
		System.out.println(Collections.max(compteurs.values()) - Collections.min(compteurs.values()));
	}

	// run3 -----Methode BAC-----
	public void run3() {
		String filename = "src/main/resources/advent2021/" + Day14.class.getSimpleName() + "a.txt";
		List<String> lignes = Outil.importationString(filename);
		String polymerTemplate = lignes.get(0);
		List<String> rules = lignes.subList(2, lignes.size());
		System.out.println(polymerTemplate);
		System.out.println("rules: " + rules);
		List<String> pattern = new ArrayList<String>();
		for (int j = 0; j < rules.size(); j++) {
			String rule = rules.get(j);
			pattern.add(rule.substring(0, 2));
		}

		String[] keys = new String[rules.size()];
		String[] values = new String[rules.size()];
		String[] numeros = new String[rules.size()];
		String[] valuesUnCaractere = new String[rules.size()];
		Map<String, String> definitions1 = new HashMap<>();
		Map<String, String> definitions2 = new HashMap<>();
		int i = 0;
		for (String rule : rules) {

			String motif = rule.substring(0, 2);
			String remplacement = motif.charAt(0) + String.valueOf(10 + i) + motif.charAt(1);
			keys[i] = motif;
			values[i] = remplacement;
			valuesUnCaractere[i] = "" + rule.charAt(rule.length() - 1);
			numeros[i] = String.valueOf(10 + i);

			definitions1.put(motif, remplacement);
			definitions2.put(numeros[i], valuesUnCaractere[i]);

			i++;

		}

		for (int step = 1; step < 2; step++) {
			String res = testBorAhoCorasick(polymerTemplate, definitions1);
			polymerTemplate = testBorAhoCorasick(res, definitions2);
			System.out.println("step: " + step);
			// System.out.println(polymerTemplate.length());
		}

		// compter le nb d'elements par caractere
		Map<Character, Integer> map = new TreeMap<>();
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().mapToObj(l -> (char) l).forEach(c -> map.put(c, 0));

		polymerTemplate.chars().mapToObj(l -> (char) l).forEach(c -> map.computeIfPresent(c, (k, v) -> v + 1));
		System.out.println(map);

	}

	// run2 ----
	public void run4() {
		String filename = "src/main/resources/advent2021/" + Day14.class.getSimpleName() + "a.txt";
		List<String> lignes = Outil.importationString(filename);
		String polymerTemplate = lignes.get(0);
		List<String> rules = lignes.subList(2, lignes.size());
		System.out.println(polymerTemplate);
		System.out.println("rules: " + rules);
		List<String> pattern = new ArrayList<String>();
		for (int j = 0; j < rules.size(); j++) {
			String rule = rules.get(j);
			pattern.add(rule.substring(0, 2));
		}

		String[] keys = new String[rules.size()];
		String[] values = new String[rules.size()];
		String[] numeros = new String[rules.size()];
		String[] valuesUnCaractere = new String[rules.size()];
		int i = 0;
		for (String rule : rules) {

			String motif = rule.substring(0, 2);
			String remplacement = motif.charAt(0) + String.valueOf(10 + i) + motif.charAt(1);
			keys[i] = motif;
			values[i] = remplacement;
			valuesUnCaractere[i] = "" + rule.charAt(rule.length() - 1);
			numeros[i] = String.valueOf(10 + i);
			i++;
		}

		for (int step = 1; step < 1; step++) {

			polymerTemplate = StringUtils.replaceEach(polymerTemplate, keys, values);
			String res = StringUtils.replaceEachRepeatedly(polymerTemplate, keys, values);
			polymerTemplate = StringUtils.replaceEach(res, numeros, valuesUnCaractere);
			System.out.println("step: " + step);
			// System.out.println(polymerTemplate.length());

		}
		// compter le nb d'elements par caractere
		Map<Character, Integer> map = new TreeMap<>();
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().mapToObj(l -> (char) l).forEach(c -> map.put(c, 0));

		polymerTemplate.chars().mapToObj(l -> (char) l).forEach(c -> map.computeIfPresent(c, (k, v) -> v + 1));

		System.out.println(map);
	}

	private String testBorAhoCorasick(final String text, final Map<String, String> definitions) {
		// Create a buffer sufficiently large that re-allocations are minimized.
		final StringBuilder sb = new StringBuilder(text.length() << 1);

		final TrieBuilder builder = Trie.builder();
		// builder.onlyWholeWords();
		builder.ignoreOverlaps();

		int i = 0;
		final String[] keys = new String[definitions.size()];
		for (String key : definitions.keySet()) {
			keys[i] = key;
			i++;
		}

		for (final String key : keys) {
			builder.addKeyword(key);
		}

		final Trie trie = builder.build();
		final Collection<Emit> emits = trie.parseText(text);

		int prevIndex = 0;

		for (final Emit emit : emits) {
			final int matchIndex = emit.getStart();

			sb.append(text.substring(prevIndex, matchIndex));
			sb.append(definitions.get(emit.getKeyword()));
			prevIndex = emit.getEnd() + 1;
		}

		// Add the remainder of the string (contains no more matches).
		sb.append(text.substring(prevIndex));

		return sb.toString();
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day14.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		String polymerTemplate = lignes.get(0);
		List<String> rules = lignes.subList(2, lignes.size());
		System.out.println(polymerTemplate);
		System.out.println("rules: " + rules);
		List<String> pattern = new ArrayList<String>();
		for (int j = 0; j < rules.size(); j++) {
			String rule = rules.get(j);
			pattern.add(rule.substring(0, 2));
		}
		System.out.println("pattern: " + pattern);

		for (int step = 1; step < 11; step++) {

			for (int i = 0; i < polymerTemplate.length() - 1; i++) {
				// analyse des positions i et i+1
				char c1 = polymerTemplate.charAt(i);
				char c2 = polymerTemplate.charAt(i + 1);
				String str = "" + c1 + c2;

				// recherche si str match une rule
				if (pattern.contains(str)) {
					// inserer
					for (int k = 0; k < pattern.size(); k++) {
						if (pattern.get(k).charAt(0) == c1 & pattern.get(k).charAt(1) == c2) {
							polymerTemplate = polymerTemplate.substring(0, i + 1) + rules.get(k).charAt(rules.get(k).length() - 1) + polymerTemplate.substring(i + 1);
						}
					} // fin insertion
						// on deplace le curseur de 1 de plus puisque insertion
					i++;
				}
			}
			System.out.println("step " + step + ": " + "(" + polymerTemplate.length() + ")");
			// System.out.println("step " + step +": "+ "("+polymerTemplate.length() +") "+ polymerTemplate);

		}

		// compter le nb d'elements par caractere
		String input = polymerTemplate;
		Map<Character, Integer> map = new TreeMap<>();
		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".chars().mapToObj(i -> (char) i).forEach(c -> map.put(c, 0));

		input.chars().mapToObj(i -> (char) i).forEach(c -> map.computeIfPresent(c, (k, v) -> v + 1));

		System.out.println(map);
	}

}
