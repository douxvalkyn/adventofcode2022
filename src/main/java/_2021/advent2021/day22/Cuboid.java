package advent2021.day22;

import java.util.concurrent.atomic.AtomicInteger;

public class Cuboid {

	private int xMin;
	private int yMin;
	private int zMin;
	private int xMax;
	private int yMax;
	private int zMax;
	private String onOff;
	private final int id;
	private static final AtomicInteger count = new AtomicInteger(0);

	public Cuboid(int xMin, int yMin, int zMin, int xMax, int yMax, int zMax, String onOff) {
		super();
		this.xMin = xMin;
		this.yMin = yMin;
		this.zMin = zMin;
		this.xMax = xMax;
		this.yMax = yMax;
		this.zMax = zMax;
		this.onOff = onOff;
		id = count.incrementAndGet();
	}

	public int getId() {
		return id;
	}

	public static AtomicInteger getCount() {
		return count;
	}

	public int getxMin() {
		return xMin;
	}

	public void setxMin(int xMin) {
		this.xMin = xMin;
	}

	public int getxMax() {
		return xMax;
	}

	public void setxMax(int xMax) {
		this.xMax = xMax;
	}

	public int getyMin() {
		return yMin;
	}

	public void setyMin(int yMin) {
		this.yMin = yMin;
	}

	public int getyMax() {
		return yMax;
	}

	public void setyMax(int yMax) {
		this.yMax = yMax;
	}

	public int getzMin() {
		return zMin;
	}

	public void setzMin(int zMin) {
		this.zMin = zMin;
	}

	public int getzMax() {
		return zMax;
	}

	public void setzMax(int zMax) {
		this.zMax = zMax;
	}

	public String getOnOff() {
		return onOff;
	}

	public void setOnOff(String onOff) {
		this.onOff = onOff;
	}

	@Override
	public String toString() {
		return "Cuboid [id=" + id + "]";
	}

}
