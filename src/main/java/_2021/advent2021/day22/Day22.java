package advent2021.day22;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day22 {

	static Map<List<Cuboid>, Long> mapVolumes = new HashMap<List<Cuboid>, Long>();
	static Map<List<Cuboid>, Cuboid> mapResultatsIntersection = new HashMap<List<Cuboid>, Cuboid>();

	// main -----

	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day22.class.getSimpleName() + "]");
		Day22 day = new Day22();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run3();
		// day.run2bis();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// https://fr.wikipedia.org/wiki/Principe_d%27inclusion-exclusion
	// https://stackoverflow.com/questions/5556170/finding-shared-volume-of-two-overlapping-cuboids
	// https://stackoverflow.com/questions/5009526/overlapping-cubes
	// A prive de B= A - (A^B)
	// AuB = A+B-A^B

	// le programme s'eternise apres 416 étapes sur 420. Pour trouver le resultat final, j'ai ajouté, au volume apres 416 étapes, le nb de light allumées par les 4 etapes manquantes
	// lorsque le programme tourne sur 20étapes seulement: et ça marche car ces 4 étapes sont tres proches les unes des autres !

	// run3 ----
	public void run3() {

		String filename = "src/main/resources/advent2021/" + Day22.class.getSimpleName() + "part2.txt";
		List<String> lignes = Outil.importationString(filename);
		List<Cuboid> nCubes = importationsCubes(lignes);
		long volume = 0; // volume total cherche
		List<Cuboid> cubesExistants = new ArrayList<Cuboid>(); // liste des cubes précédents, déjà existants dans l'ordre inverse

		List<Cuboid> mesCubes = new ArrayList<Cuboid>();
		mesCubes.add(nCubes.get(0));
		mesCubes.add(nCubes.get(1));
		mesCubes.add(nCubes.get(2));
		Cuboid res = resultatIntersectionNCubes(mesCubes);
		calculeVolumeCube(res);

		// on regarde les instructions une par une, dans l'ordre inverse
		for (int numInstruction = lignes.size() - 1; numInstruction >= 0; numInstruction--) {

			LocalDateTime timeStart = LocalDateTime.now();
			System.out.println("numInstruction: " + numInstruction + " :" + nCubes.get(numInstruction).getOnOff());

			// si c'est une case OFF, on ne fait rien car ça ne rajoute pas de light
			if (nCubes.get(numInstruction).getOnOff().equals("off")) {

				// ne rien faire pour le calcul des volumes! (il faudra quand meme le compter
				// apres car il peut cacher des light, donc je l'ajoute aux cubes existants)
				cubesExistants.add(nCubes.get(numInstruction));
			}

			// si c'est une case ON
			if (nCubes.get(numInstruction).getOnOff().equals("on")) {

				// on ajoute le volume du cube
				volume = volume + calculeVolumeCube(nCubes.get(numInstruction));

				// et on retire les intersections sans double compter les intersections d'intersections
				if (cubesExistants.size() != 0) {
					// intersection= une liste de cubes
					Set<Cuboid> intersection = new HashSet<Cuboid>();
					intersection.add(nCubes.get(numInstruction));
					// tant que volume different de 0
					recursiveCalculVolume(cubesExistants, intersection);
				}

				cubesExistants.add(nCubes.get(numInstruction));

				// prise en compte des volumes d'intersections à partir de map et remise à zero
				// de la map

				for (Entry<List<Cuboid>, Long> entry : mapVolumes.entrySet()) {
					if (entry.getKey().size() % 2 == 0) {
						volume = volume - entry.getValue();
					}
					if (entry.getKey().size() % 2 == 1) {
						volume = volume + entry.getValue();
					}
				}

				mapVolumes.clear();
			}

			LocalDateTime timeEnd = LocalDateTime.now();
			Duration duree = Duration.between(timeStart, timeEnd);
			System.out.println("vol=" + volume);
			System.out.println("duree de l'etape: " + duree.getSeconds() + " s");
			System.out.println("--------");
		}
	}

	public void recursiveCalculVolume(List<Cuboid> cubesExistants, Set<Cuboid> intersection) {
		// System.out.println(mapVolumes.size());

		// generer les nouvelles intersections à partir de l'intersection precedente (au
		// debut il y a seulement le cube en cours)

		Set<Set<Cuboid>> listeIntersections = creationIntersectionsNiveauSuivant(intersection, cubesExistants);

		// Calculer le volume de chaque intersection de cette liste et ajout de ce calcul à la map

		if (listeIntersections != null) {

			for (Set<Cuboid> inter : listeIntersections) {
				List<Cuboid> inter_ = new ArrayList<>(inter);

				Cuboid res = resultatIntersectionNCubes(inter_);
				mapResultatsIntersection.put(inter_, res);
				long vol = calculeVolumeCube(res);
				mapVolumes.put(inter_, vol);

				if (vol != 0) {

					// on doit continuer à calculer les intersections
					recursiveCalculVolume(cubesExistants, inter);

				}
			}
		}
	}

	private Set<Set<Cuboid>> creationIntersectionsNiveauSuivant(Set<Cuboid> intersection, List<Cuboid> cubesExistants) {

		// on crée une nouvelle liste d'intersections qui contiendra l'intersection
		// précédente augmentée de chaque cube de cubesExistants

		// regle: si l'intersection est de taille maximale, on ne renvoie rien
		if (intersection.size() == (cubesExistants.size() + 1)) {
			return null;
		}

		Set<Set<Cuboid>> listeIntersections = new HashSet<Set<Cuboid>>();

		for (int j = 0; j < cubesExistants.size(); j++) {

			Set<Cuboid> initIntersections = new HashSet<Cuboid>();
			for (Cuboid cube : intersection) {
				initIntersections.add(cube);
			}

			initIntersections.add(cubesExistants.get(j)); // n'ajoutera pas s'il y a un doublon :)

			// regle: si l'intersection engendree existe déjà dans map, on ne l'ajoute pas
			// ici; attention, il faut vérifier l'égalité malgré l'ordre
			List<Cuboid> initIntersections_ = new ArrayList<>(initIntersections);
			boolean check = true;
			for (Entry<List<Cuboid>, Long> entry : mapVolumes.entrySet()) {
				List<Cuboid> listeAComparer = entry.getKey();
				check = check & !listEqualsIgnoreOrder(initIntersections_, listeAComparer);
			}
			if (check) {
				listeIntersections.add(initIntersections); // on ajoute !
			}

		}
		return listeIntersections;
	}

	public static <T> boolean areAllUnique(List<T> list) {
		Set<T> set = new HashSet<>();
		for (T t : list) {
			if (!set.add(t))
				return false;
		}
		return true;
	}

	private List<List<Integer>> copie2(List<List<Integer>> listeCubesIntersection) {

		List<List<Integer>> listeCubesIntersection2 = new ArrayList<List<Integer>>();

		for (int i = 0; i < listeCubesIntersection.size(); i++) {

			List<Integer> intersection = listeCubesIntersection.get(i);

			List<Integer> intersection2 = new ArrayList<Integer>();

			for (int k = 0; k < intersection.size(); k++) {

				int coord = intersection.get(k);

				intersection2.add(coord);

			}

			listeCubesIntersection2.add(intersection2);

		}

		return listeCubesIntersection2;

	}

	// supprime les intersections qui ont 2 cubes identiques (car correspond Ã un
	// croisement parent, d'un niveau d'avant)

	private void retirerSiDeuxCubesIdentiques(List<List<List<Integer>>> listeIntersections) {

		List<Integer> indicesToRemove = new ArrayList<>();

		for (int i = 0; i < listeIntersections.size(); i++) {

			for (List<Integer> cube : listeIntersections.get(i)) {

				for (List<Integer> cube2 : listeIntersections.get(i)) {

					if (cube != cube2) {

						if (cube.get(0) == cube2.get(0) & cube.get(1) == cube2.get(1) & cube.get(2) == cube2.get(2)

							& cube.get(3) == cube2.get(3) & cube.get(4) == cube2.get(4) & cube.get(5) == cube2.get(5)) {

							indicesToRemove.add(i);

						}

					}

				}

			}

		}

		// remove if indicesToRemove est non vide

		if (indicesToRemove.size() > 0) {

			for (int i = 0; i < indicesToRemove.size(); i++) {

				listeIntersections.get(indicesToRemove.get(i)).clear();

			}

		}

		listeIntersections.removeIf(p -> p.isEmpty());

	}

	// supprime les intersections en doublon (=avec les mÃªmes cubes, mÃªmes si
	// placÃ©s dans un autre ordre!)

	private void checkDoublons(List<List<List<Integer>>> listeIntersections) {

		List<Integer> indicesToRemove = new ArrayList<>();

		for (int i = 0; i < listeIntersections.size(); i++) {

			for (int j = i; j < listeIntersections.size(); j++) {

				if (i != j) {

					boolean check = listEqualsIgnoreOrder(listeIntersections.get(i), listeIntersections.get(j));

					if (check) {

						indicesToRemove.add(i);

					}

				}

			}

		}

		// remove if indicesToRemove est non vide

		if (indicesToRemove.size() > 0) {

			for (int i = 0; i < indicesToRemove.size(); i++) {

				listeIntersections.get(indicesToRemove.get(i)).clear();

			}

		}

		listeIntersections.removeIf(p -> p.isEmpty());

	}

	// compare 2 listes et renvoie true si elles sont Ã©gales (peu importe si les
	// elements sont rangÃ©s dans des ordres diffÃ©rents)
	public <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
		return new HashSet<>(list1).equals(new HashSet<>(list2));
	}

	private List<List<List<Integer>>> copieOpti(List<List<List<Integer>>> listeIntersections, List<List<Integer>> cubesExistants) {

		List<List<List<Integer>>> listeIntersections2 = new ArrayList<List<List<Integer>>>();

		for (List<Integer> cubeExistant : cubesExistants) {

			for (int i = 0; i < listeIntersections.size(); i++) {

				List<List<Integer>> intersections = listeIntersections.get(i);

				List<List<Integer>> intersections2 = new ArrayList<List<Integer>>();

				for (int j = 0; j < intersections.size(); j++) {

					List<Integer> cube = intersections.get(j);

					List<Integer> cube2 = new ArrayList<>();

					for (int k = 0; k < cube.size(); k++) {

						int coord = cube.get(k);

						cube2.add(coord);

					}

					intersections2.add(cube2);

				}

				// ajout du cube existant

				List<Integer> cube3 = new ArrayList<>();

				for (int k = 0; k < cubeExistant.size(); k++) {

					int coord2 = cubeExistant.get(k);

					cube3.add(coord2);

				}

				intersections2.add(cube3);

				listeIntersections2.add(intersections2);

			}

		}

		return listeIntersections2;

	}

	private List<List<List<Integer>>> copie(List<List<List<Integer>>> listeIntersections) {

		List<List<List<Integer>>> listeIntersections2 = new ArrayList<List<List<Integer>>>();

		for (int i = 0; i < listeIntersections.size(); i++) {

			List<List<Integer>> intersections = listeIntersections.get(i);

			List<List<Integer>> intersections2 = new ArrayList<List<Integer>>();

			for (int j = 0; j < intersections.size(); j++) {

				List<Integer> intersection = intersections.get(j);

				List<Integer> intersection2 = new ArrayList<>();

				for (int k = 0; k < intersection.size(); k++) {

					int coord = intersection.get(k);

					intersection2.add(coord);

				}

				intersections2.add(intersection2);

			}

			listeIntersections2.add(intersections2);

		}

		return listeIntersections2;

	}

	// public long unionMultiple(List<List<Integer>> nCubes) {
	//
	// long vol = 0;
	//
	// List<List<List<List<Integer>>>> listeTousCubesDecomposes = decompositionDenombrement(nCubes);
	//
	// for (int i = 0; i < listeTousCubesDecomposes.size(); i++) {
	//
	// List<List<List<Integer>>> listeCubesDecomposes = listeTousCubesDecomposes.get(i);
	//
	// for (int j = 0; j < listeCubesDecomposes.size(); j++) {
	//
	// List<List<Integer>> cubesDecomposes = listeCubesDecomposes.get(j);
	//
	// List<Integer> cube = resultatIntersectionNCubes(cubesDecomposes);
	//
	// // System.out.println(v);
	//
	// if (i % 2 == 0) {
	//
	// vol = vol + calculeVolumeCube(cube);
	//
	// }
	//
	// if (i % 2 == 1) {
	//
	// vol = vol - calculeVolumeCube(cube);
	//
	// }
	//
	// }
	//
	// }
	//
	// return vol;
	//
	// }

	public List<List<List<List<Integer>>>> decompositionDenombrement(List<List<Integer>> cubes) {

		int n = cubes.size();

		List<List<List<List<Integer>>>> listeTousCubesDecomposes = new ArrayList<List<List<List<Integer>>>>();

		// System.out.println("cubes "+cubes);

		for (int p = 1; p <= n; p++) {

			List<int[]> combinations = generate(n, p);

			List<List<List<Integer>>> listeCubesDecomposes = new ArrayList<List<List<Integer>>>();

			for (int[] combination : combinations) {

				List<List<Integer>> cubesDecomposes = new ArrayList<List<Integer>>();

				for (int j = 0; j < combination.length; j++) {

					List<Integer> cube = cubes.get(combination[j]);

					cubesDecomposes.add(cube);

				}

				listeCubesDecomposes.add(cubesDecomposes);

			}

			// System.out.println("------------ "+"p = "+ p + ": -----------");

			// System.out.println(listeCubesDecomposes);

			listeTousCubesDecomposes.add(listeCubesDecomposes);

		}

		return listeTousCubesDecomposes;

	}

	public List<int[]> generate(int n, int r) {

		List<int[]> combinations = new LinkedList<>();

		helper(combinations, new int[r], 0, n - 1, 0);

		return combinations;

	}

	public List<List<Integer>> generate2(int n, int r) {

		List<List<Integer>> combinations = new ArrayList<>();

		List<Integer> rrr = new ArrayList<Integer>();

		rrr.add(r);

		helper2(combinations, rrr, 0, n - 1, 0);

		return combinations;

	}

	private void helper2(List<List<Integer>> combinations, List<Integer> data, int start, int end, int index) {

		if (index == data.size()) {

			List<Integer> combination = new ArrayList<Integer>();

			for (int i = 0; i < data.size(); i++) {

				combination.add(data.get(i));

			}

			combinations.add(combination);

		} else if (start <= end) {

			data.set(index, start);

			helper2(combinations, data, start + 1, end, index + 1);

			helper2(combinations, data, start + 1, end, index);

		}

	}

	private void helper(List<int[]> combinations, int data[], int start, int end, int index) {

		if (index == data.length) {

			int[] combination = data.clone();

			combinations.add(combination);

		} else if (start <= end) {

			data[index] = start;

			helper(combinations, data, start + 1, end, index + 1);

			helper(combinations, data, start + 1, end, index);

		}

	}

	public Cuboid resultatIntersectionNCubes(List<Cuboid> cubes) {

		if (cubes.size() == 1) {
			return cubes.get(0);
		}

		if (cubes.size() == 2) {
			return resultatIntersectionDeuxCubes(cubes.get(0), cubes.get(1));
		} else {

			List<Cuboid> cubesMoinsLeDernier = new ArrayList<Cuboid>();
			for (int i = 0; i < cubes.size() - 1; i++) {
				cubesMoinsLeDernier.add(cubes.get(i));
			}

			Cuboid res = null;
			if (mapResultatsIntersection.containsKey(cubesMoinsLeDernier)) {
				Cuboid resCubesMoinsLeDernier = mapResultatsIntersection.get(cubesMoinsLeDernier);
				res = resultatIntersectionDeuxCubes(cubes.get(cubes.size() - 1), resCubesMoinsLeDernier);
				mapResultatsIntersection.put(cubes, res);
				return res;
			} else {
				res = resultatIntersectionDeuxCubes(cubes.get(cubes.size() - 1), resultatIntersectionNCubes(cubesMoinsLeDernier));
				mapResultatsIntersection.put(cubes, res);
				return res;
			}
		}
	}

	public long calculeVolumeCube(Cuboid cube) {
		if (cube == null) {
			return 0;
		}
		long a = (1 + cube.getxMax() - cube.getxMin());
		long b = (1 + cube.getyMax() - cube.getyMin());
		long c = (1 + cube.getzMax() - cube.getzMin());
		long produit = a * b * c;

		return produit;
	}

	public Cuboid resultatIntersectionDeuxCubes(Cuboid cubeA, Cuboid cubeB) {

		if (intersecte(cubeA, cubeB)) {

			// coin en bas a gauche
			int xMin = Math.max(cubeA.getxMin(), cubeB.getxMin());
			int yMin = Math.max(cubeA.getyMin(), cubeB.getyMin());
			int zMin = Math.max(cubeA.getzMin(), cubeB.getzMin());

			// coin en haut a droite
			int xMax = Math.min(cubeA.getxMax(), cubeB.getxMax());
			int yMax = Math.min(cubeA.getyMax(), cubeB.getyMax());
			int zMax = Math.min(cubeA.getzMax(), cubeB.getzMax());

			Cuboid cube = creationCube(xMin, xMax, yMin, yMax, zMin, zMax);
			return cube;
		} else {
			return null;
		}
	}

	public List<Cuboid> importationsCubes(List<String> lignes) {

		// liste des cubes
		List<Cuboid> cubes = new ArrayList<>();

		for (int index = 0; index < lignes.size(); index++) {

			String ligne = lignes.get(index);
			String[] split = StringUtils.split(ligne, " ");
			String onOff = split[0];

			String[] coordonnees = StringUtils.split(split[1], ",");
			String coordonneesX = coordonnees[0];
			String coordonneesY = coordonnees[1];
			String coordonneesZ = coordonnees[2];
			String coordonneesXSansEgal = StringUtils.split(coordonneesX, "=")[1];
			String coordonneesYSansEgal = StringUtils.split(coordonneesY, "=")[1];
			String coordonneesZSansEgal = StringUtils.split(coordonneesZ, "=")[1];
			int xDeb = Integer.parseInt(StringUtils.split(coordonneesXSansEgal, "..")[0]);
			int xFin = Integer.parseInt(StringUtils.split(coordonneesXSansEgal, "..")[1]);
			int yDeb = Integer.parseInt(StringUtils.split(coordonneesYSansEgal, "..")[0]);
			int yFin = Integer.parseInt(StringUtils.split(coordonneesYSansEgal, "..")[1]);
			int zDeb = Integer.parseInt(StringUtils.split(coordonneesZSansEgal, "..")[0]);
			int zFin = Integer.parseInt(StringUtils.split(coordonneesZSansEgal, "..")[1]);

			Cuboid cube = creationCube(xDeb, xFin, yDeb, yFin, zDeb, zFin);
			cube.setOnOff(onOff);
			cubes.add(cube);
		}
		return cubes;
	}

	public boolean intersecte(Cuboid cubeA, Cuboid cubeB) {
		boolean res = false;
		if (cubeA == null || cubeB == null) {
			res = false;
		}
		if (cubeA != null && cubeB != null)
			if ((cubeA.getxMax() >= cubeB.getxMin()) & (cubeA.getxMin() <= cubeB.getxMax()) & (cubeA.getyMax() >= cubeB.getyMin()) & (cubeA.getyMin() <= cubeB.getyMax())
				& (cubeA.getzMax() >= cubeB.getzMin()) & (cubeA.getzMin() <= cubeB.getzMax())) {
				res = true;
			}
		return res;
	}

	public Cuboid creationCube(int xDeb, int xFin, int yDeb, int yFin, int zDeb, int zFin) {

		// determiner min et max sur l'axe X

		int minX = 0;
		int maxX = 0;

		if (xDeb < xFin) {
			minX = xDeb;
			maxX = xFin;
		} else {
			minX = xFin;
			maxX = xDeb;
		}

		// determiner min et max sur l'axe Y

		int minY = 0;
		int maxY = 0;

		if (yDeb < yFin) {
			minY = yDeb;
			maxY = yFin;
		} else {
			minY = yFin;
			maxY = yDeb;
		}

		// determiner min et max sur l'axe Z

		int minZ = 0;
		int maxZ = 0;

		if (zDeb < zFin) {
			minZ = zDeb;
			maxZ = zFin;
		} else {
			minZ = zFin;
			maxZ = zDeb;
		}

		// un cube sera determinÃ© par une liste de 6 parametres: minX, minY, minZ,
		// maxX, maxY, maxZ

		Cuboid cuboid = new Cuboid(minX, minY, minZ, maxX, maxY, maxZ, null);

		return cuboid;

	}

	// run1 ----
	public void run1() {

		String filename = "src/main/resources/advent2021/" + Day22.class.getSimpleName() + "part1.txt";

		List<String> lignes = Outil.importationString(filename);

		Map<List<Integer>, String> coords = new HashMap<List<Integer>, String>();

		for (int index = 0; index < lignes.size(); index++) {

			String ligne = lignes.get(index);

			String[] split = StringUtils.split(ligne, " ");

			String onOff = split[0];

			String[] coordonnees = StringUtils.split(split[1], ",");

			String coordonneesX = coordonnees[0];

			String coordonneesY = coordonnees[1];

			String coordonneesZ = coordonnees[2];

			String coordonneesXSansEgal = StringUtils.split(coordonneesX, "=")[1];

			String coordonneesYSansEgal = StringUtils.split(coordonneesY, "=")[1];

			String coordonneesZSansEgal = StringUtils.split(coordonneesZ, "=")[1];

			int coordonneesXDeb = Integer.parseInt(StringUtils.split(coordonneesXSansEgal, "..")[0]);

			int coordonneesXFin = Integer.parseInt(StringUtils.split(coordonneesXSansEgal, "..")[1]);

			int coordonneesYDeb = Integer.parseInt(StringUtils.split(coordonneesYSansEgal, "..")[0]);

			int coordonneesYFin = Integer.parseInt(StringUtils.split(coordonneesYSansEgal, "..")[1]);

			int coordonneesZDeb = Integer.parseInt(StringUtils.split(coordonneesZSansEgal, "..")[0]);

			int coordonneesZFin = Integer.parseInt(StringUtils.split(coordonneesZSansEgal, "..")[1]);

			for (int x = coordonneesXDeb; x <= coordonneesXFin; x++) {

				for (int y = coordonneesYDeb; y <= coordonneesYFin; y++) {

					for (int z = coordonneesZDeb; z <= coordonneesZFin; z++) {

						List<Integer> listeCoords = new ArrayList<Integer>();

						listeCoords.add(x);

						listeCoords.add(y);

						listeCoords.add(z);

						// si c'est on et que Ã§a existe pas deja, je l'ajoute

						// si c'est off et que Ã§a existe je l'enleve

						if (onOff.equals("on")) {

							if (!coords.containsKey(listeCoords)) {

								coords.put(listeCoords, onOff);

							}

						}

						if (onOff.equals("off")) {

							if (coords.containsKey(listeCoords)) {

								coords.remove(listeCoords, "on");

							}

						}

					}

				}

			}

		}

		System.out.println(coords.size());

	}

}
