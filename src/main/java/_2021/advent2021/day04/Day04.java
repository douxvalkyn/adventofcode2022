package advent2021.day04;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day04 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day04.class.getSimpleName() + "]");
		Day04 day = new Day04();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day04.class.getSimpleName() + ".txt";
		List<String> grilles = Outil.importationString(filename);
		System.out.println(grilles);
		List<Integer[][]> grillesFormatees = creationGrille(grilles);
		// String numeros = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1";
		String numeros = "26,55,7,40,56,34,58,90,60,83,37,36,9,27,42,19,46,18,49,52,75,17,70,41,12,78,15,64,50,54,2,77,76,10,43,79,22,32,47,0,72,30,21,82,6,95,13,59,16,89,1,85,57,62,81,38,29,80,8,67,20,53,69,25,23,61,86,71,68,98,35,31,4,33,91,74,14,28,65,24,97,88,3,39,11,93,66,44,45,96,92,51,63,84,73,99,94,87,5,48";
		String[] TabNumeros = StringUtils.split(numeros, ',');
		for (String numero : TabNumeros) {
			lireNumero(Integer.parseInt(numero), grillesFormatees);
			boolean check = verifieVictoire(grillesFormatees, Integer.parseInt(numero));
			if (check) {
				break;
			}
		}
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day04.class.getSimpleName() + ".txt";
		List<String> grilles = Outil.importationString(filename);
		System.out.println(grilles);
		List<Integer[][]> grillesFormatees = creationGrille(grilles);
		// String numeros = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1";
		String numeros = "26,55,7,40,56,34,58,90,60,83,37,36,9,27,42,19,46,18,49,52,75,17,70,41,12,78,15,64,50,54,2,77,76,10,43,79,22,32,47,0,72,30,21,82,6,95,13,59,16,89,1,85,57,62,81,38,29,80,8,67,20,53,69,25,23,61,86,71,68,98,35,31,4,33,91,74,14,28,65,24,97,88,3,39,11,93,66,44,45,96,92,51,63,84,73,99,94,87,5,48";
		String[] TabNumeros = StringUtils.split(numeros, ',');
		for (String numero : TabNumeros) {
			lireNumero(Integer.parseInt(numero), grillesFormatees);
			boolean check = verifieVictoire(grillesFormatees, Integer.parseInt(numero));
			if (check) {
				supprimeGrilleGagnante(grillesFormatees);
			}
		}
	}

	private void supprimeGrilleGagnante(List<Integer[][]> grillesFormatees) {
		int indice = -1;
		List<Integer> indicesWin = new ArrayList<>();
		for (Integer[][] grille : grillesFormatees) {
			indice++;
			boolean ligne1 = ((grille[0][0] == null) & (grille[0][1] == null) & (grille[0][2] == null) & (grille[0][3] == null) & (grille[0][4] == null));
			boolean ligne2 = ((grille[1][0] == null) & (grille[1][1] == null) & (grille[1][2] == null) & (grille[1][3] == null) & (grille[1][4] == null));
			boolean ligne3 = ((grille[2][0] == null) & (grille[2][1] == null) & (grille[2][2] == null) & (grille[2][3] == null) & (grille[2][4] == null));
			boolean ligne4 = ((grille[3][0] == null) & (grille[3][1] == null) & (grille[3][2] == null) & (grille[3][3] == null) & (grille[3][4] == null));
			boolean ligne5 = ((grille[4][0] == null) & (grille[4][1] == null) & (grille[4][2] == null) & (grille[4][3] == null) & (grille[4][4] == null));
			boolean colonne1 = ((grille[0][0] == null) & (grille[1][0] == null) & (grille[2][0] == null) & (grille[3][0] == null) & (grille[4][0] == null));
			boolean colonne2 = ((grille[0][1] == null) & (grille[1][1] == null) & (grille[2][1] == null) & (grille[3][1] == null) & (grille[4][1] == null));
			boolean colonne3 = ((grille[0][2] == null) & (grille[1][2] == null) & (grille[2][2] == null) & (grille[3][2] == null) & (grille[4][2] == null));
			boolean colonne4 = ((grille[0][3] == null) & (grille[1][3] == null) & (grille[2][3] == null) & (grille[3][3] == null) & (grille[4][3] == null));
			boolean colonne5 = ((grille[0][4] == null) & (grille[1][4] == null) & (grille[2][4] == null) & (grille[3][4] == null) & (grille[4][4] == null));

			if (ligne1 | ligne2 | ligne3 | ligne4 | ligne5 | colonne1 | colonne2 | colonne3 | colonne4 | colonne5) {
				indicesWin.add(indice);
			}
		}
		for (int i = indicesWin.size(); i > 0; i--) {
			int numeroToDelete = indicesWin.get(i - 1);
			grillesFormatees.remove(numeroToDelete);
		}

	}

	// verifier si une ligne ou une colonne d'une grille est remplie
	private boolean verifieVictoire(List<Integer[][]> grillesFormatees, int numero) {
		for (Integer[][] grille : grillesFormatees) {
			boolean ligne1 = ((grille[0][0] == null) & (grille[0][1] == null) & (grille[0][2] == null) & (grille[0][3] == null) & (grille[0][4] == null));
			boolean ligne2 = ((grille[1][0] == null) & (grille[1][1] == null) & (grille[1][2] == null) & (grille[1][3] == null) & (grille[1][4] == null));
			boolean ligne3 = ((grille[2][0] == null) & (grille[2][1] == null) & (grille[2][2] == null) & (grille[2][3] == null) & (grille[2][4] == null));
			boolean ligne4 = ((grille[3][0] == null) & (grille[3][1] == null) & (grille[3][2] == null) & (grille[3][3] == null) & (grille[3][4] == null));
			boolean ligne5 = ((grille[4][0] == null) & (grille[4][1] == null) & (grille[4][2] == null) & (grille[4][3] == null) & (grille[4][4] == null));
			boolean colonne1 = ((grille[0][0] == null) & (grille[1][0] == null) & (grille[2][0] == null) & (grille[3][0] == null) & (grille[4][0] == null));
			boolean colonne2 = ((grille[0][1] == null) & (grille[1][1] == null) & (grille[2][1] == null) & (grille[3][1] == null) & (grille[4][1] == null));
			boolean colonne3 = ((grille[0][2] == null) & (grille[1][2] == null) & (grille[2][2] == null) & (grille[3][2] == null) & (grille[4][2] == null));
			boolean colonne4 = ((grille[0][3] == null) & (grille[1][3] == null) & (grille[2][3] == null) & (grille[3][3] == null) & (grille[4][3] == null));
			boolean colonne5 = ((grille[0][4] == null) & (grille[1][4] == null) & (grille[2][4] == null) & (grille[3][4] == null) & (grille[4][4] == null));
			if (ligne1 | ligne2 | ligne3 | ligne4 | ligne5 | colonne1 | colonne2 | colonne3 | colonne4 | colonne5) {
				// System.out.println("victoire");
				// System.out.println("numero: "+numero);

				// calcul du score
				List<Integer> nombresNonMarques = new ArrayList<>();
				for (int x = 0; x < 5; x++) {
					for (int y = 0; y < 5; y++) {
						if (grille[y][x] != null) {
							nombresNonMarques.add(grille[y][x]);
						}
					}
				}
				int sum = nombresNonMarques.stream().mapToInt(a -> a).sum();
				// System.out.println("res: "+sum*numero);
				return true;

			} // fin test win
		} // fin boucle sur grille
		return false;

	}

	private List<Integer[][]> creationGrille(List<String> grilles) {
		List<Integer[][]> grillesFormatees = new ArrayList<>();
		for (int i = 0; i < grilles.size(); i = i + 6) {
			String ligne1 = grilles.get(i);
			String ligne2 = grilles.get(i + 1);
			String ligne3 = grilles.get(i + 2);
			String ligne4 = grilles.get(i + 3);
			String ligne5 = grilles.get(i + 4);
			String[] NumerosLigne1 = StringUtils.split(ligne1, ' ');
			String[] NumerosLigne2 = StringUtils.split(ligne2, ' ');
			String[] NumerosLigne3 = StringUtils.split(ligne3, ' ');
			String[] NumerosLigne4 = StringUtils.split(ligne4, ' ');
			String[] NumerosLigne5 = StringUtils.split(ligne5, ' ');

			Integer[][] grilleFormatee = new Integer[5][5];
			grilleFormatee[0][0] = Integer.parseInt(NumerosLigne1[0]);
			grilleFormatee[0][1] = Integer.parseInt(NumerosLigne1[1]);
			grilleFormatee[0][2] = Integer.parseInt(NumerosLigne1[2]);
			grilleFormatee[0][3] = Integer.parseInt(NumerosLigne1[3]);
			grilleFormatee[0][4] = Integer.parseInt(NumerosLigne1[4]);
			grilleFormatee[1][0] = Integer.parseInt(NumerosLigne2[0]);
			grilleFormatee[1][1] = Integer.parseInt(NumerosLigne2[1]);
			grilleFormatee[1][2] = Integer.parseInt(NumerosLigne2[2]);
			grilleFormatee[1][3] = Integer.parseInt(NumerosLigne2[3]);
			grilleFormatee[1][4] = Integer.parseInt(NumerosLigne2[4]);
			grilleFormatee[2][0] = Integer.parseInt(NumerosLigne3[0]);
			grilleFormatee[2][1] = Integer.parseInt(NumerosLigne3[1]);
			grilleFormatee[2][2] = Integer.parseInt(NumerosLigne3[2]);
			grilleFormatee[2][3] = Integer.parseInt(NumerosLigne3[3]);
			grilleFormatee[2][4] = Integer.parseInt(NumerosLigne3[4]);
			grilleFormatee[3][0] = Integer.parseInt(NumerosLigne4[0]);
			grilleFormatee[3][1] = Integer.parseInt(NumerosLigne4[1]);
			grilleFormatee[3][2] = Integer.parseInt(NumerosLigne4[2]);
			grilleFormatee[3][3] = Integer.parseInt(NumerosLigne4[3]);
			grilleFormatee[3][4] = Integer.parseInt(NumerosLigne4[4]);
			grilleFormatee[4][0] = Integer.parseInt(NumerosLigne5[0]);
			grilleFormatee[4][1] = Integer.parseInt(NumerosLigne5[1]);
			grilleFormatee[4][2] = Integer.parseInt(NumerosLigne5[2]);
			grilleFormatee[4][3] = Integer.parseInt(NumerosLigne5[3]);
			grilleFormatee[4][4] = Integer.parseInt(NumerosLigne5[4]);
			grillesFormatees.add(grilleFormatee);
		}
		return grillesFormatees;
	}

	private void lireNumero(int numero, List<Integer[][]> grillesFormatees) {
		for (Integer[][] grille : grillesFormatees) {
			for (Integer x = 0; x < 5; x++) {
				for (Integer y = 0; y < 5; y++) {
					if (grille[y][x] != null) {
						if (grille[y][x] == numero) {
							grille[y][x] = null;
						}
					}
				}
			}
		}
	}
}
