package advent2021.day17;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2021.outils.Outil;

public class Day17 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day17.class.getSimpleName() + "]");
		Day17 day = new Day17();
		LocalDateTime start = LocalDateTime.now();
		day.run();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run() {
		// target area sample:
		// int target_x_min=20;
		// int target_x_max=30;
		// int target_y_min=-5;
		// int target_y_max=-10;

		// target area reel:
		int target_x_min = 230;
		int target_x_max = 283;
		int target_y_min = -57;
		int target_y_max = -107;

		// init
		int x = 0;
		int y = 0;
		List<Integer> maximums = new ArrayList<>();
		int cpt = 0;

		// recherche vecteur initial
		for (int vx_init = 1; vx_init < 2 * target_x_max; vx_init++) {
			for (int vy_init = target_y_max; vy_init < 2 * target_x_max; vy_init++) {

				// STEPS
				int vx = vx_init;
				int vy = vy_init;
				int yMax = 0;
				x = 0;
				y = 0;

				boolean go = true;
				while (go) {
					x = x + vx;
					y = y + vy;
					if (y > yMax) {
						yMax = y;
					} // noter le y max
					vy--;
					if (vx > 0) {
						vx--;
					} else {
						if (vx < 0) {
							vx++;
						}
					}

					// verif target
					if (x >= target_x_min & x <= target_x_max & y >= target_y_max & y <= target_y_min) {
						// System.out.println("vx: "+vx_init+", vy: "+vy_init + ", yMax: "+ yMax);
						maximums.add(yMax);
						go = false;
						cpt++;
						x = 0;
						y = 0;
					}

					// condition d'arret si d�passement
					if (x > target_x_max | y < target_y_max) {
						// System.out.println("rat�!");
						go = false;
						x = 0;
						y = 0;
					}
				}
			}
		}
		System.out.println("ymax " + Outil.max(maximums) + " - nb solutions: " + cpt);
	}
}
