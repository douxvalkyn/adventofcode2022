package advent2021.day05;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day05 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day05.class.getSimpleName() + "]");
		Day05 day = new Day05();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		//day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
		double calcul1 = 1.0 * duree.getSeconds();
		String calculString1 = "" + calcul1;
		System.out.println(calculString1);
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/day05.txt";
		List<String> lignes = Outil.importationString(filename);
		List<Integer> listeX1 = new ArrayList<>();
		List<Integer> listeX2 = new ArrayList<>();
		List<Integer> listeY1 = new ArrayList<>();
		List<Integer> listeY2 = new ArrayList<>();
		selectionLignesHorizontalesOuVerticalesOuDiagonales(lignes, listeX1, listeX2, listeY1, listeY2);
		Grille grille = new Grille(1000, 1000);
		remplissageGrille2(grille, listeX1, listeX2, listeY1, listeY2);
		// grille.affichageValeur();
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day05.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		List<Integer> listeX1 = new ArrayList<>();
		List<Integer> listeX2 = new ArrayList<>();
		List<Integer> listeY1 = new ArrayList<>();
		List<Integer> listeY2 = new ArrayList<>();
		selectionLignesHorizontalesOuVerticales(lignes, listeX1, listeX2, listeY1, listeY2);
		Grille grille = new Grille(1000, 1000);
		remplissageGrille(grille, listeX1, listeX2, listeY1, listeY2);
		// grille.affichageValeur();
	}

	public void run1_test() {
		String filename = "src/main/resources/advent2021/" + Day05.class.getSimpleName() + "a.txt";
		List<String> lignes = Outil.importationString(filename);
		List<Integer> listeX1 = new ArrayList<>();
		List<Integer> listeX2 = new ArrayList<>();
		List<Integer> listeY1 = new ArrayList<>();
		List<Integer> listeY2 = new ArrayList<>();
		selectionLignesHorizontalesOuVerticales(lignes, listeX1, listeX2, listeY1, listeY2);

		int cpt = 0;
		for (int i = 0; i < listeX1.size(); i++) {
			for (int j = 0; j < listeX1.size(); j++) {
				boolean testCroisement = intersection(listeX1.get(i), listeY1.get(i), listeX2.get(i), listeY2.get(i), listeX1.get(j), listeY1.get(j), listeX2.get(j), listeY2.get(j));
				if (testCroisement) {
					cpt++;
				}
			}
		}

		// boolean res = intersection(1,1,8,5,1,4,7,2);
		System.out.println(cpt);

	}

	// renvoie true si deux segments se croisent
	// premier segment (x1,y1) (x2,y2)
	// second segment (x3,y3) (x4,y4)
	public boolean intersection(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
		boolean res = false;

		// Equations sont de la forme: y = a*x + b
		double a1 = 1.0 * (y2 - y1) / (x2 - x1);
		double b1 = 1.0 * (y1 - a1 * x1);
		double a2 = 1.0 * (y4 - y3) / (x4 - x3);
		double b2 = 1.0 * (y3 - a2 * x3);

		// solution �ventuelle si intersection
		double x0 = -(b1 - b2) / (a1 - a2);
		double y0 = a1 * x0 + b1;

		// check si la solution appartient aux 2 segments
		if (Math.min(x1, x2) < x0 & x0 < Math.max(x1, x2) & Math.min(x3, x4) < x0 & x0 < Math.max(x3, x4)) {
			res = true;
		}
		return res;

	}

	public void remplissageGrille2(Grille grille, List<Integer> listeX1, List<Integer> listeX2, List<Integer> listeY1, List<Integer> listeY2) {
		int cpt = 0;
		for (int i = 0; i < listeX1.size(); i++) {
			// System.out.println(i);
			if (listeX1.get(i).equals(listeX2.get(i))) { // ligne verticale
				// determiner si y1 ou y2 est le plus petit
				if (listeY1.get(i) < listeY2.get(i)) {
					for (int y = listeY1.get(i); y <= listeY2.get(i); y++) {
						grille.getCase(listeX1.get(i), y).setEtatPlusUn();
					}
				}
				if (listeY2.get(i) < listeY1.get(i)) {
					for (int y = listeY2.get(i); y <= listeY1.get(i); y++) {
						grille.getCase(listeX1.get(i), y).setEtatPlusUn();
					}
				}
			}
			if (listeY1.get(i).equals(listeY2.get(i))) { // ligne horizontale
				// determiner si x1 ou x2 est le plus petit
				if (listeX1.get(i) < listeX2.get(i)) {
					for (int x = listeX1.get(i); x <= listeX2.get(i); x++) {
						grille.getCase(x, listeY1.get(i)).setEtatPlusUn();
					}
				}
				if (listeX2.get(i) < listeX1.get(i)) {
					for (int x = listeX2.get(i); x <= listeX1.get(i); x++) {
						grille.getCase(x, listeY1.get(i)).setEtatPlusUn();
					}
				}
			}
			if (listeX1.get(i) > listeX2.get(i) & listeY1.get(i) < listeY2.get(i)) { // ligne diagonale decroissante vers la gauche
				int x = listeX1.get(i);
				int y = listeY1.get(i);
				int nbPts = listeX1.get(i) - listeX2.get(i) + 1;
				for (int j = 1; j <= nbPts; j++) {
					grille.getCase(x, y).setEtatPlusUn();
					x--;
					y++;
				}
			}
			if (listeX1.get(i) < listeX2.get(i) & listeY1.get(i) > listeY2.get(i)) { // ligne diagonale croissante vers la droite
				int x = listeX1.get(i);
				int y = listeY1.get(i);
				int nbPts = listeX2.get(i) - listeX1.get(i) + 1;
				for (int j = 1; j <= nbPts; j++) {
					grille.getCase(x, y).setEtatPlusUn();
					x++;
					y--;
				}
			}
			if (listeX1.get(i) < listeX2.get(i) & listeY1.get(i) < listeY2.get(i)) { // ligne diagonale decroissante vers la droite
				int x = listeX1.get(i);
				int y = listeY1.get(i);
				int nbPts = listeX2.get(i) - listeX1.get(i) + 1;
				for (int j = 1; j <= nbPts; j++) {
					grille.getCase(x, y).setEtatPlusUn();
					x++;
					y++;
				}
			}
			if (listeX1.get(i) > listeX2.get(i) & listeY1.get(i) > listeY2.get(i)) { // ligne diagonale croissante vers la gauche
				int x = listeX1.get(i);
				int y = listeY1.get(i);
				int nbPts = listeX1.get(i) - listeX2.get(i) + 1;
				for (int j = 1; j <= nbPts; j++) {
					grille.getCase(x, y).setEtatPlusUn();
					x--;
					y--;
				}
			}
		}
	}

	private void remplissageGrille(Grille grille, List<Integer> listeX1, List<Integer> listeX2, List<Integer> listeY1, List<Integer> listeY2) {
		int cpt = 0;
		for (int i = 0; i < listeX1.size(); i++) {
			// System.out.println(i);
			if (listeX1.get(i).equals(listeX2.get(i))) { // ligne verticale
				// determiner si y1 ou y2 est le plus petit
				if (listeY1.get(i) < listeY2.get(i)) {
					for (int y = listeY1.get(i); y <= listeY2.get(i); y++) {
						grille.getCase(listeX1.get(i), y).setEtatPlusUn();
						if (grille.getCase(listeX1.get(i), y).getValeur() > 1) {
							cpt++;
						}
					}
				} else {
					for (int y = listeY2.get(i); y <= listeY1.get(i); y++) {
						grille.getCase(listeX1.get(i), y).setEtatPlusUn();
						if (grille.getCase(listeX1.get(i), y).getValeur() > 1) {
							cpt++;
						}
					}
				}
			} else { // ligne horizontale
						// determiner si x1 ou x2 est le plus petit
				if (listeX1.get(i) < listeX2.get(i)) {
					for (int x = listeX1.get(i); x <= listeX2.get(i); x++) {
						grille.getCase(x, listeY1.get(i)).setEtatPlusUn();
						if (grille.getCase(x, listeY1.get(i)).getValeur() > 1) {
							cpt++;
						}
					}
				} else {
					for (int x = listeX2.get(i); x <= listeX1.get(i); x++) {
						grille.getCase(x, listeY1.get(i)).setEtatPlusUn();
						if (grille.getCase(x, listeY1.get(i)).getValeur() > 1) {
							cpt++;
						}
					}
				}
			}
		}
		System.out.println("cpt: " + cpt);
	}

	private void selectionLignesHorizontalesOuVerticalesOuDiagonales(List<String> lignes, List<Integer> listeX1, List<Integer> listeX2, List<Integer> listeY1, List<Integer> listeY2) {
		for (String ligne : lignes) {
			String[] ligneSplit = StringUtils.split(ligne, "-> ");
			String depart = ligneSplit[0];
			String arrivee = ligneSplit[1];
			String x1 = StringUtils.split(depart, ",")[0];
			String y1 = StringUtils.split(depart, ",")[1];
			String x2 = StringUtils.split(arrivee, ",")[0];
			String y2 = StringUtils.split(arrivee, ",")[1];
			listeX1.add(Integer.parseInt(x1));
			listeX2.add(Integer.parseInt(x2));
			listeY1.add(Integer.parseInt(y1));
			listeY2.add(Integer.parseInt(y2));
		}
	}

	private void selectionLignesHorizontalesOuVerticales(List<String> lignes, List<Integer> listeX1, List<Integer> listeX2, List<Integer> listeY1, List<Integer> listeY2) {
		for (String ligne : lignes) {
			String[] ligneSplit = StringUtils.split(ligne, "-> ");
			String depart = ligneSplit[0];
			String arrivee = ligneSplit[1];
			String x1 = StringUtils.split(depart, ",")[0];
			String y1 = StringUtils.split(depart, ",")[1];
			String x2 = StringUtils.split(arrivee, ",")[0];
			String y2 = StringUtils.split(arrivee, ",")[1];
			if ((x1.equals(x2)) | (y1.equals(y2))) {
				listeX1.add(Integer.parseInt(x1));
				listeX2.add(Integer.parseInt(x2));
				listeY1.add(Integer.parseInt(y1));
				listeY2.add(Integer.parseInt(y2));
			}
		}
	}
}
