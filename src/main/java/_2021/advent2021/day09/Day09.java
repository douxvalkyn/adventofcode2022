package advent2021.day09;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import advent2021.outils.Case;
import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day09 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day09.class.getSimpleName() + "]");
		Day09 day = new Day09();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day09.class.getSimpleName() + ".txt";

		// lecture input
		List<String> lignes = Outil.importationString(filename);
		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}

		// recherche des low points
		List<Case> lowPoints = new ArrayList<Case>();
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				List<Case> casesAdj = grille.getCase(i, j).getCasesAdjacentes(grille);
				int valeur = grille.getCase(i, j).getValeur();
				boolean lowPoint = true;
				for (Case c : casesAdj) {
					if (c.getValeur() <= valeur) {
						lowPoint = false;
					}
				}
				if (lowPoint) {
					lowPoints.add(grille.getCase(i, j));
					// grille.getCase(i,j).setEtat("BASIN");
				}
			}
		}

		List<Integer> basinsSize = new ArrayList<>();
		for (int indiceLowPoint = 0; indiceLowPoint < lowPoints.size(); indiceLowPoint++) { // boucle sur chaque bassin

			// recherche basin
			List<Case> basin = new ArrayList<Case>();
			basin.add(lowPoints.get(indiceLowPoint));

			// liste des cases a tester
			List<Case> casesATester = new ArrayList<Case>();
			casesATester.add(lowPoints.get(indiceLowPoint));

			while (!casesATester.isEmpty()) {
				List<Case> casesAdjSansDoublons = new ArrayList<>();
				List<Case> casesAdj = new ArrayList<>();
				for (int k = 0; k < casesATester.size(); k++) {
					casesAdj.addAll(casesATester.get(k).getCasesAdjacentes(grille));
				}
				// remove doublons de la liste
				casesAdjSansDoublons = new ArrayList<>(new HashSet<>(casesAdj));

				// ajouter les cases adjacentes au basin si possible
				for (int j = 0; j < casesAdjSansDoublons.size(); j++) {
					if (casesAdjSansDoublons.get(j).getValeur() != 9 & casesAdjSansDoublons.get(j).getEtat() == "_") {
						casesAdjSansDoublons.get(j).setEtat("basin");
						basin.add(casesAdjSansDoublons.get(j));
					}
				}
				casesATester.clear();

				// ajouter les voisines dans les cases a tester ensuite
				for (int i = 0; i < basin.size(); i++) {
					if (basin.get(i).getEtat().equals("basin")) {
						casesATester.add(basin.get(i));
						basin.get(i).setEtat("BASIN");
					}
				}
			}
			// System.out.println("basin size: "+basin.size());
			basinsSize.add(basin.size());
		}

		// resultat
		Collections.sort(basinsSize, Collections.reverseOrder());
		System.out.println("resultats: " + basinsSize.get(0) * basinsSize.get(1) * basinsSize.get(2));

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day09.class.getSimpleName() + ".txt";

		// lecture input
		List<String> lignes = Outil.importationString(filename);
		// System.out.println(lignes);
		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}
		// grille.affichageValeur();

		// recherche des low points
		int risk = 0;
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				List<Case> casesAdj = grille.getCase(i, j).getCasesAdjacentes(grille);
				int valeur = grille.getCase(i, j).getValeur();
				boolean lowPoint = true;

				for (Case c : casesAdj) {
					if (c.getValeur() <= valeur) {
						lowPoint = false;
					}
				}
				if (lowPoint) {
					// System.out.println("lowPoint: "+ grille.getCase(i,j));
					risk = risk + grille.getCase(i, j).getValeur() + 1;
				}
			}
		}
		System.out.println("risk: " + risk);
	}

}
