package advent2021.day25;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2021.outils.Case;
import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day25 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day25.class.getSimpleName() + "]");
		Day25 day = new Day25();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		// day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day25.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		creationGrille(lignes, grille);
		// grille.affichageEtat();

		boolean stable = false;
		int step = 0;
		// Boucle
		// for (int step=1;step<59;step++) {
		while (!stable) {

			// grille save1 avant deplacement EST
			Grille grilleSave1 = new Grille(lignes.get(0).length(), lignes.size());
			List<Case> casesSave1 = grilleSave1.getCases();
			List<Case> cases = grille.getCases();
			for (int i = 0; i < casesSave1.size(); i++) {
				casesSave1.get(i).setEtat(cases.get(i).getEtat());
			}

			// deplacements vers EST si possible
			for (Case cell : cases) {
				if (cell.getEtat().equals(">") & cell.getValeur() == 0) {
					Case voisinSave1 = null;
					Case voisin = null;
					// pour les cases non au bord, on regarde le voisin SAVE1 de droite
					if (cell.getX() != grille.getNbCol() - 1) {
						voisinSave1 = grilleSave1.getCase(cell.getX() + 1, cell.getY());
						voisin = grille.getCase(cell.getX() + 1, cell.getY());
					}
					// pour les cases au bord, on regarde la premiere colonne
					if (cell.getX() == grille.getNbCol() - 1) {
						voisinSave1 = grilleSave1.getCase(0, cell.getY());
						voisin = grille.getCase(0, cell.getY());
					}

					// deplacement si possible
					if (voisinSave1.getEtat().equals(".")) { // deplacement OK
						cell.setEtat(".");
						voisin.setEtat(">");
						voisin.setValeur(1);
					}
				}
			} // fin deplacements vers est

			// grille save2 avant deplacement SUD
			Grille grilleSave2 = new Grille(lignes.get(0).length(), lignes.size());
			List<Case> casesSave2 = grilleSave2.getCases();
			cases = grille.getCases();
			for (int i = 0; i < casesSave2.size(); i++) {
				casesSave2.get(i).setEtat(cases.get(i).getEtat());
			}

			// deplacements vers sud si possible
			for (Case cell : cases) {
				if (cell.getEtat().equals("v") & cell.getValeur() == 0) {
					Case voisinSave2 = null;
					Case voisin = null;
					// pour les cases non au bord, on regarde le voisin SAVE2 du sud
					if (cell.getY() != grille.getNbLignes() - 1) {
						voisinSave2 = grilleSave2.getCase(cell.getX(), cell.getY() + 1);
						voisin = grille.getCase(cell.getX(), cell.getY() + 1);
					}
					// pour les cases au bord, on regarde la premiere colonne
					if (cell.getY() == grille.getNbLignes() - 1) {
						voisinSave2 = grilleSave2.getCase(cell.getX(), 0);
						voisin = grille.getCase(cell.getX(), 0);
					}

					// deplacement si possible
					if (voisinSave2.getEtat().equals(".")) { // deplacement OK
						cell.setEtat(".");
						voisin.setEtat("v");
						voisin.setValeur(1);
					}
				}
			} // fin deplacements vers est

			step++;
			// System.out.println("------- STEP "+step +"--------" );
			// grille.affichageEtat();
			System.out.println(step);

			// remettre � zero le compteur de deplacement (valeur)
			for (Case cell : cases) {
				cell.setValeur(0);
			}

			// verifier si on atteint un etat stable
			stable = true;
			outerloop: // revenir ici en quittant la boucle si etat non stable
			for (int i = 0; i < grille.getNbCol(); i++) {
				for (int j = 0; j < grille.getNbLignes(); j++) {
					grille.getCase(i, j);
					grilleSave1.getCase(i, j);
					if (!grille.getCase(i, j).getEtat().equals(grilleSave1.getCase(i, j).getEtat())) {
						stable = stable & false;
						if (!stable) {
							break outerloop;
						}
					}
				}
			}

			// System.out.println("stable: " + stable);

		} // fin boucle

	}

	private void creationGrille(List<String> lignes, Grille grille) {
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setEtat("" + lignes.get(j).charAt(i));
				grille.getCase(i, j).setValeur(0);
			}
		}
	}

}
