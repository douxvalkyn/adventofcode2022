package advent2021.day03;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2021.outils.Outil;

public class Day03 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day03.class.getSimpleName() + "]");
		Day03 day = new Day03();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/day03.txt";
		List<String> lignes = Outil.importationString(filename);
		int largeur = lignes.get(0).length();

		String[] gamma = new String[largeur];
		String[] epsilon = new String[largeur];
		String[] lettres = null;
		List<List<Integer>> superListe = new ArrayList<>();
		for (int i = 0; i < largeur; i++) {
			List<Integer> liste = new ArrayList<>();
			superListe.add(liste);
		}

		for (String ligne : lignes) {

			lettres = ligne.split("");
			for (int j = 0; j < largeur; j++) {

				superListe.get(j).add(Integer.valueOf(lettres[j]));
			}
		}

		for (int j = 0; j < largeur; j++) {
			Integer sum = superListe.get(j).stream().reduce(0, (a, b) -> a + b);
			if (sum > lignes.size() / 2) {
				gamma[j] = "1";
				epsilon[j] = "0";
			} else {
				gamma[j] = "0";
				epsilon[j] = "1";
			}
		}

		String gammaString = "";
		for (int i = 0; i < gamma.length; i++) {
			gammaString = gammaString + gamma[i];
		}
		String epsilonString = "";
		for (int i = 0; i < epsilon.length; i++) {
			epsilonString = epsilonString + epsilon[i];
		}
		int gammaDecimal = Integer.parseInt(gammaString, 2);
		int epsilonDecimal = Integer.parseInt(epsilonString, 2);
		System.out.println("res: " + gammaDecimal * epsilonDecimal);

	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/day03.txt";
		List<String> lignes = Outil.importationString(filename);
		int largeur = lignes.get(0).length();

		List<String> res1 = selectionLigne(lignes, largeur, '1', '0'); // O2
		List<String> res2 = selectionLigne(lignes, largeur, '0', '1'); // CO2
		int a = Integer.parseInt(res1.get(0), 2);
		int b = Integer.parseInt(res2.get(0), 2);
		System.out.println("res: " + a * b);
	}

	private List<String> selectionLigne(List<String> lignes, int largeur, char param1, char param2) {
		String[] lettres;
		for (int j = 0; j < largeur; j++) {

			// creation des listes
			List<List<Integer>> superListe = new ArrayList<>();
			for (int i = 0; i < largeur; i++) {
				List<Integer> liste = new ArrayList<>();
				superListe.add(liste);
			}

			// remplissage des listes
			for (String ligne : lignes) {
				lettres = ligne.split("");
				for (int k = 0; k < largeur; k++) {
					superListe.get(k).add(Integer.valueOf(lettres[k]));
				}
			}

			// application des regles
			List<String> lignes2 = new ArrayList<>();

			Integer sum = superListe.get(j).stream().reduce(0, (a, b) -> a + b);
			if (sum >= ((1.0 * lignes.size()) / 2)) {
				// majorite de 1:
				for (String list : lignes) {
					if (list.charAt(j) == param1) {
						lignes2.add(list);
					}
				}
			}
			if (sum < ((1.0 * lignes.size()) / 2)) {
				// majorite de 0:
				for (String list : lignes) {
					if (list.charAt(j) == param2) {
						lignes2.add(list);
					}
				}
			}
			// on prend en compte la selection sauf si �a genere une liste nulle
			if (lignes2.size() != 0) {
				lignes = lignes2;
			}
		}
		return lignes;
	}
}
