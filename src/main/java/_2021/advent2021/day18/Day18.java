package advent2021.day18;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Outil;

public class Day18 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day18.class.getSimpleName() + "]");
		Day18 day = new Day18();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day18.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// calcul des additions 2 � 2 et de la magitude associ�e
		// x+y != y+x
		int max = 0;
		for (int i = 0; i < lignes.size(); i++) {
			for (int j = 0; j < lignes.size(); j++) {
				if (i != j) {
					String str = addition(lignes.get(i), lignes.get(j));
					str = reduction(str);
					int magnitude = calculMagnitude(str);
					if (magnitude > max) {
						max = magnitude;
					}
				}
			}
		}

		System.out.println(max);

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day18.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		// reductions apres additions successives
		String str = addition(lignes.get(0), lignes.get(1));
		str = reduction(str);
		for (int i = 2; i < lignes.size(); i++) {
			str = addition(str, lignes.get(i));
			str = reduction(str);
		}

		// calcul magnitude
		int magnitude = calculMagnitude(str);
		System.out.println("magntude: " + magnitude);
	}

	public int calculMagnitude(String str) {

		boolean go = true;
		String str2 = "";
		int i = 0;
		while (go) {

			if (str.charAt(i) == ']') {
				// paire trouv�e, on r�cupere les nombres la constituant:
				// nb1; vers la gauche
				int k = i - 1;
				String NB = "";
				while (str.charAt(k) != '[') {
					NB = NB + str.charAt(k);
					k--;
				}
				// reverse NB1
				char ch;
				String nstr = "";
				for (int l = 0; l < NB.length(); l++) {
					ch = NB.charAt(l);
					nstr = ch + nstr;
				}
				NB = nstr;
				String[] nombres = StringUtils.split(NB, ",");

				int nb1 = Integer.parseInt(nombres[0]);
				int nb2 = Integer.parseInt(nombres[1]);
				int magnitude = 3 * nb1 + 2 * nb2;

				// on remplace la paire par sa magnitude
				str2 = StringUtils.substring(str, 0, i - NB.length() - 1) + magnitude + StringUtils.substring(str, i + 1);
				str = str2;
				i = -1;
				// check si c'est fini (plus de crochets)
				if (str.charAt(str.length() - 1) != ']') {
					go = false;
				}
			}
			i++;
		}
		return Integer.parseInt("" + str2);
	}

	public String reduction(String str) {
		String strE = explodes(str);
		// System.out.println(strE);
		boolean reduit = true;
		while (reduit) {
			while (!str.equals(strE)) {
				str = strE;
				strE = explodes(str);
				// System.out.println(strE);
			}

			strE = split(str);
			// System.out.println(strE);
			if (str.equals(strE)) {
				// le split n'a rien fait, reduction termin�e
				reduit = false;
				System.out.println("reduction termin�e: " + strE);
			}
		}
		return strE;
	}

	public String split(String str) {
		String str2 = str;
		boolean go = true;
		// recherche si split
		for (int i = 0; i < str.length() - 1; i++) {
			if (go & str.charAt(i) != '[' & str.charAt(i) != ']' & str.charAt(i) != ',' & str.charAt(i + 1) != '[' & str.charAt(i + 1) != ']' & str.charAt(i + 1) != ',') {
				// nombre >10 trouv�
				go = false;
				int nb = Integer.parseInt("" + str.charAt(i) + str.charAt(i + 1));

				// split
				str2 = str.substring(0, i) + "[" + Math.round(Math.floor(1.0 * nb / 2)) + "," + Math.round(Math.ceil(1.0 * nb / 2)) + "]" + str.substring(i + 2);
			}
		}

		return str2;
	}

	public String explodes(String str) {
		// recherche si explosion
		int cpt_ouvrante = 0;
		boolean explosion = false;
		List<String> exploded = new ArrayList<String>();
		List<Integer> explodedIndices = new ArrayList<Integer>();

		for (int i = 0; i < str.length(); i++) {

			char carac = str.charAt(i);
			String caracString = "" + str.charAt(i);
			if (carac == '[') {
				cpt_ouvrante++;
			}
			if (carac == ']') {
				cpt_ouvrante--;
			}
			if (cpt_ouvrante == 5) {

				exploded.add(caracString);
				explodedIndices.add(i);
				explosion = true;
			}
		}

		if (explosion) {
			// recuperer les bonnes valeurs
			int nb1 = 0;
			int nb2 = 0;
			int indiceVirgule = 0;
			// si la virgule est en position 2, nb1= le chiffre en position1
			if (exploded.get(2).equals(",")) {
				nb1 = Integer.parseInt("" + exploded.get(1));
				indiceVirgule = 2;
			}
			// si la virgule est en position 3, nb1= les 2 chiffres en position1 et 2
			if (exploded.get(3).equals(",")) {
				nb1 = Integer.parseInt("" + exploded.get(1) + exploded.get(2));
				indiceVirgule = 3;
			}
			// pour le nb2 on recupere tous les chiffres tant qu'on n'a pas de ]
			String nbString = "";
			// j'ajoute [ � la fin de exploded pour que la condition d'arrete ci-dessous fonctionne toujours, m�me s'il n'y a pas d'autre explosion
			exploded.add("[");
			while (!exploded.get(indiceVirgule).equals("[")) {
				if (!exploded.get(indiceVirgule).equals(",")) {
					nbString = nbString + exploded.get(indiceVirgule);
				}
				indiceVirgule++;
			}
			nb2 = Integer.parseInt(nbString);

			String debut = "";
			String fin = "";
			String intermediaire1 = "";
			String intermediaire2 = "";

			// ajout du exploded de gauche au premier nombre � sa gauche (if any)
			boolean goG = true;
			String strModified = str;
			int createdNumber = -1;

			for (int j = explodedIndices.get(1) - 1; j > 0; j--) {
				// calcul de l'intermediaire1
				if ((str.charAt(j) == '[' | str.charAt(j) == ']' | str.charAt(j) == ',') & goG) {
					intermediaire1 = intermediaire1 + str.charAt(j);
				}
				if (str.charAt(j) != '[' & str.charAt(j) != ']' & str.charAt(j) != ',' & goG) {
					// check si nb d'arriv�e est un nombre de 2 chiffres
					if (strModified.charAt(j - 1) != '[' & strModified.charAt(j - 1) != ']' & strModified.charAt(j - 1) != ',') {
						strModified = str.substring(0, j - 1) + (Integer.parseInt("" + str.charAt(j - 1) + str.charAt(j)) + nb1) + str.substring(j + 1);
						goG = false;
						createdNumber = Integer.parseInt("" + str.charAt(j - 1) + str.charAt(j)) + nb1;
						debut = str.substring(0, j - 1) + (Integer.parseInt("" + str.charAt(j - 1) + str.charAt(j)) + nb1);
					} else { // si nb d'arriv�e est un nombre de 1 chiffre
						strModified = str.substring(0, j) + (Integer.parseInt("" + str.charAt(j)) + nb1) + str.substring(j + 1);
						goG = false;
						createdNumber = Integer.parseInt("" + str.charAt(j)) + nb1;
						debut = str.substring(0, j) + (Integer.parseInt("" + str.charAt(j)) + nb1);
					}
				}
			}
			// System.out.println(debut);

			// ajout du exploded de droite au premier nombre � sa droite (if any)
			boolean goD = true;
			String strModified2 = strModified;
			int lengthNb1 = (int) (Math.log10(nb1) + 1);
			int lengthNb2 = (int) (Math.log10(nb2) + 1);
			if (nb1 == 0) {
				lengthNb1 = 1;
			} // correctif car log0 n'existe pas
			if (nb2 == 0) {
				lengthNb2 = 1;
			} // correctif car log0 n'existe pas

			int lengthCreatedNumber = 0;
			int lengthnbInitialGauche = 0;
			int nbInitialGauche;
			if (createdNumber == -1) {
				lengthCreatedNumber = 0;
				lengthnbInitialGauche = 0;
				nbInitialGauche = -999;
			} else {
				lengthCreatedNumber = (int) (Math.log10(createdNumber) + 1);
				if (createdNumber == 0) {
					lengthCreatedNumber = 1;
				}
				nbInitialGauche = createdNumber - nb1;
				lengthnbInitialGauche = (int) (Math.log10(nbInitialGauche) + 1);
				if (nbInitialGauche == 0) {
					lengthnbInitialGauche = 1;
				} // correctif car log0 n'existe pas
			}

			for (int j = explodedIndices.get(3) + lengthNb1 + lengthNb2 + lengthCreatedNumber - lengthnbInitialGauche - 1 - 1 + 1; j < strModified.length() - 1; j++) {
				// calcul de l'intermediaire2 (il faudra enlever 1 crochet)
				if ((strModified.charAt(j) == '[' | strModified.charAt(j) == ']' | strModified.charAt(j) == ',') & goD) {
					intermediaire2 = intermediaire2 + strModified.charAt(j);
				}
				// calcul de la partie droite
				if (strModified.charAt(j) != '[' & strModified.charAt(j) != ']' & strModified.charAt(j) != ',' & goD) {
					// check si nb d'arriv�e est un nombre de 2 chiffres
					if (strModified.charAt(j + 1) != '[' & strModified.charAt(j + 1) != ']' & strModified.charAt(j + 1) != ',') {
						strModified2 = strModified.substring(0, j) + (Integer.parseInt("" + strModified.charAt(j) + strModified.charAt(j + 1)) + nb2) + strModified.substring(j + 2);
						goD = false;
						fin = (Integer.parseInt("" + strModified.charAt(j) + strModified.charAt(j + 1)) + nb2) + strModified.substring(j + 2);
					} else { // si nb d'arriv�e est un nombre de 1 chiffre
						strModified2 = strModified.substring(0, j) + (Integer.parseInt("" + strModified.charAt(j)) + nb2) + strModified.substring(j + 1);
						goD = false;
						fin = (Integer.parseInt("" + strModified.charAt(j)) + nb2) + strModified.substring(j + 1);
					}
				}
			}
			if (debut.isEmpty()) {
				debut = "[" + debut;
			}
			if (fin.isEmpty()) {
				fin = fin + "]";
			}
			intermediaire2 = StringUtils.substring(intermediaire2, 1); // on enleve un crochet au debut
			// inverser intermediaire1 et enlever un crochet
			char ch;
			String nstr = "";
			for (int i = 0; i < intermediaire1.length(); i++) {

				ch = intermediaire1.charAt(i); // extracts each character
				nstr = ch + nstr; // adds each character in front of the existing string
			}
			intermediaire1 = nstr;
			intermediaire1 = StringUtils.substring(intermediaire1, 0, intermediaire1.length() - 1); // on enleve un crochet � la fin

			// System.out.println(intermediaire);
			// System.out.println(fin);

			// suppresion du exploded
			String strModified3 = debut + intermediaire1 + "0" + intermediaire2 + fin;
			str = strModified3;
		}

		// retourne le string eventuellement modifi�
		return str;
	}

	public String addition(String str1, String str2) {
		return "[" + str1 + "," + str2 + "]";

	}

}
