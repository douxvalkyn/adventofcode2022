package advent2021.day15;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2021.outils.*;

public class Day15 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day15.class.getSimpleName() + "]");
		Day15 day = new Day15();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day15.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);

		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}

		// creation de la grille complete
		Grille grilleComplete = new Grille(5 * lignes.get(0).length(), 5 * lignes.size());

		List<Grille> grilles = new ArrayList<Grille>();
		grilles.add(grille); // ajout grille initiale (grille1)
		for (int i = 1; i < 25; i++) {
			Grille grilleSupp = new Grille(lignes.get(0).length(), lignes.size());
			// remplissage de grilleSupp avec les valeurs de grille1 +x sauf pour 9 ou on retourne 0
			int x = 0;
			if (i < 5) {
				x = i;
			}
			if (i >= 5 & i < 10) {
				x = i - 4;
			}
			if (i >= 10 & i < 15) {
				x = i - 8;
			}
			if (i >= 15 & i < 20) {
				x = i - 12;
			}
			if (i >= 20 & i < 25) {
				x = i - 16;
			}
			for (int j = 0; j < grille.getNbCol() * grille.getNbLignes(); j++) {
				int val = grilles.get(0).getCases().get(j).getValeur();
				if ((val + x) <= 9) {
					grilleSupp.getCases().get(j).setValeur(val + x);
				}
				if ((val + x) > 9) {
					grilleSupp.getCases().get(j).setValeur(val + x - 9);
				}
			}
			grilles.add(grilleSupp);
		}

		// concatener les grilles
		int z = grilles.get(0).getNbLignes();
		// grille1
		for (int y = 0; y < z; y++) {
			for (int x = 0; x < z; x++) {
				int val = grilles.get(0).getCase(x, y).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille2
		for (int y = 0; y < z; y++) {
			for (int x = z; x < (2 * z); x++) {
				int val = grilles.get(1).getCase(x - z, y).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille3
		for (int y = 0; y < z; y++) {
			for (int x = (2 * z); x < (3 * z); x++) {
				int val = grilles.get(2).getCase(x - (2 * z), y).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille4
		for (int y = 0; y < z; y++) {
			for (int x = (3 * z); x < (4 * z); x++) {
				int val = grilles.get(3).getCase(x - (3 * z), y).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille5
		for (int y = 0; y < z; y++) {
			for (int x = (4 * z); x < (5 * z); x++) {
				int val = grilles.get(4).getCase(x - (4 * z), y).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}

		// grille6
		for (int y = z; y < (2 * z); y++) {
			for (int x = 0; x < z; x++) {
				int val = grilles.get(5).getCase(x, y - z).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille7
		for (int y = z; y < (2 * z); y++) {
			for (int x = z; x < (2 * z); x++) {
				int val = grilles.get(6).getCase(x - z, y - z).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille8
		for (int y = z; y < (2 * z); y++) {
			for (int x = (2 * z); x < (3 * z); x++) {
				int val = grilles.get(7).getCase(x - (2 * z), y - z).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille9
		for (int y = z; y < (2 * z); y++) {
			for (int x = (3 * z); x < (4 * z); x++) {
				int val = grilles.get(8).getCase(x - (3 * z), y - z).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille10
		for (int y = z; y < (2 * z); y++) {
			for (int x = (4 * z); x < (5 * z); x++) {
				int val = grilles.get(9).getCase(x - (4 * z), y - z).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}

		// grille11
		for (int y = (2 * z); y < (3 * z); y++) {
			for (int x = 0; x < z; x++) {
				int val = grilles.get(10).getCase(x, y - (2 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille12
		for (int y = (2 * z); y < (3 * z); y++) {
			for (int x = z; x < (2 * z); x++) {
				int val = grilles.get(11).getCase(x - z, y - (2 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille13
		for (int y = (2 * z); y < (3 * z); y++) {
			for (int x = (2 * z); x < (3 * z); x++) {
				int val = grilles.get(12).getCase(x - (2 * z), y - (2 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille14
		for (int y = (2 * z); y < (3 * z); y++) {
			for (int x = (3 * z); x < (4 * z); x++) {
				int val = grilles.get(13).getCase(x - (3 * z), y - (2 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille15
		for (int y = (2 * z); y < (3 * z); y++) {
			for (int x = (4 * z); x < (5 * z); x++) {
				int val = grilles.get(14).getCase(x - (4 * z), y - (2 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}

		// grille16
		for (int y = (3 * z); y < (4 * z); y++) {
			for (int x = 0; x < z; x++) {
				int val = grilles.get(15).getCase(x, y - (3 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille17
		for (int y = (3 * z); y < (4 * z); y++) {
			for (int x = z; x < (2 * z); x++) {
				int val = grilles.get(16).getCase(x - z, y - (3 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille18
		for (int y = (3 * z); y < (4 * z); y++) {
			for (int x = (2 * z); x < (3 * z); x++) {
				int val = grilles.get(17).getCase(x - (2 * z), y - (3 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille19
		for (int y = (3 * z); y < (4 * z); y++) {
			for (int x = (3 * z); x < (4 * z); x++) {
				int val = grilles.get(18).getCase(x - (3 * z), y - (3 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille20
		for (int y = (3 * z); y < (4 * z); y++) {
			for (int x = (4 * z); x < (5 * z); x++) {
				int val = grilles.get(19).getCase(x - (4 * z), y - (3 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}

		// grille21
		for (int y = (4 * z); y < (5 * z); y++) {
			for (int x = 0; x < z; x++) {
				int val = grilles.get(20).getCase(x, y - (4 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille22
		for (int y = (4 * z); y < (5 * z); y++) {
			for (int x = z; x < (2 * z); x++) {
				int val = grilles.get(21).getCase(x - z, y - (4 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille23
		for (int y = (4 * z); y < (5 * z); y++) {
			for (int x = (2 * z); x < (3 * z); x++) {
				int val = grilles.get(22).getCase(x - (2 * z), y - (4 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille24
		for (int y = (4 * z); y < (5 * z); y++) {
			for (int x = (3 * z); x < (4 * z); x++) {
				int val = grilles.get(23).getCase(x - (3 * z), y - (4 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}
		// grille25
		for (int y = (4 * z); y < (5 * z); y++) {
			for (int x = (4 * z); x < (5 * z); x++) {
				int val = grilles.get(24).getCase(x - (4 * z), y - (4 * z)).getValeur();
				grilleComplete.getCase(x, y).setValeur(val);
			}
		}

		// grilleComplete.affichageValeur();

		Graph graph = creationGraphe2(grilleComplete);
		// Graph graph = creationGraphe3(grilleComplete,10);
		int cost = graph.uniformSearch(0, 249999); // sample: 2499 reel:249999
		System.out.println("Cost : " + cost);

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day15.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		// System.out.println(lignes);

		Grille grille = new Grille(lignes.get(0).length(), lignes.size());
		for (int j = 0; j < lignes.size(); j++) {
			for (int i = 0; i < lignes.get(0).length(); i++) {
				grille.getCase(i, j).setValeur(Character.getNumericValue(lignes.get(j).charAt(i)));
			}
		}
		// grille.affichageValeur();

		// List<NodeWeighted> noeuds= new ArrayList<NodeWeighted>();
		// GraphWeighted graphWeighted = creationGraphe(grille, noeuds );
		Graph graph = creationGraphe2(grille);
		int cost = graph.uniformSearch(0, 9999);
		System.out.println("Cost : " + cost);

		// System.out.println(graphWeighted);
		// System.out.println(lignes.size());
		// System.out.println(lignes.get(0).length());
		// NodeWeighted source = findNodeWithName(noeuds, 0, 0);
		// NodeWeighted destination = findNodeWithName(noeuds, 99, 99); // modifier la dest
		// String[] res = graphWeighted.DijkstraShortestPath(source, destination);
		// System.out.println(res[0]);
		// System.out.println(res[1]);

	}

	// restreindre, exclure les points trop eloign�s de la diagonale
	private Graph creationGraphe3(Grille grille, int ecart) {

		Graph graph = new Graph();
		// creation des nodes
		List<Case> cases = grille.getCases();
		int cpt = 0;
		for (Case cell : cases) {
			// ne s'occuper que des cases proches de la diagonale
			int x = cell.getX();
			int y = cell.getY();
			if (Math.abs(x - y) < ecart) {
				graph.addNode(cell.getRang());
				cpt++;
			}
		}
		// passer en revue chaque case, regarder ses voisins, ajouter les liens avec chaque voisin

		for (Case cell : cases) {
			List<Case> casesAdj = cell.getCasesAdjacentes(grille);
			for (Case voisin : casesAdj) {
				// System.out.println(nodeVoisin);
				graph.addEdge(cell.getRang(), voisin.getRang(), voisin.getValeur());

			}
		}
		System.out.println("nb de cases etudiees: " + cpt);
		return graph;
	}

	private Graph creationGraphe2(Grille grille) {

		Graph graph = new Graph();
		// creation des nodes
		List<Case> cases = grille.getCases();
		for (Case cell : cases) {
			graph.addNode(cell.getRang());
		}
		// passer en revue chaque case, regarder ses voisins, ajouter les liens avec chaque voisin
		for (Case cell : cases) {
			List<Case> casesAdj = cell.getCasesAdjacentes(grille);
			for (Case voisin : casesAdj) {
				// System.out.println(nodeVoisin);
				graph.addEdge(cell.getRang(), voisin.getRang(), voisin.getValeur());
			}
		}
		return graph;
	}

	private GraphWeighted creationGraphe(Grille grille, List<NodeWeighted> noeuds) {
		GraphWeighted graphWeighted = new GraphWeighted(true);

		// creation des nodes
		List<Case> cases = grille.getCases();
		for (Case cell : cases) {
			NodeWeighted noeud = new NodeWeighted(cell.getRang(), cell.getX() + ";" + cell.getY());
			noeuds.add(noeud);
		}

		// passer en revue chaque case, regarder ses voisins, ajouter les liens avec chaque voisin
		for (Case cell : cases) {
			NodeWeighted node = findNodeWithName(noeuds, cell.getX(), cell.getY());
			List<Case> casesAdj = cell.getCasesAdjacentes(grille);
			for (Case voisin : casesAdj) {
				NodeWeighted nodeVoisin = findNodeWithName(noeuds, voisin.getX(), voisin.getY());
				// System.out.println(nodeVoisin);
				graphWeighted.addEdge(node, nodeVoisin, voisin.getValeur()); // ->
			}
		}

		return graphWeighted;
	}

	public NodeWeighted findNodeWithName(List<NodeWeighted> noeuds, int x, int y) {
		NodeWeighted res = new NodeWeighted(0, null);
		for (NodeWeighted noeud : noeuds) {
			if (noeud.getName().equals(x + ";" + y)) {
				res = noeud;
			}
		}
		return res;
	}

}
