package advent2021.day13;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import advent2021.outils.Grille;
import advent2021.outils.Outil;

public class Day13 {

	int compteurRun1 = 0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day13.class.getSimpleName() + "]");
		Day13 day = new Day13();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {
		String filename = "src/main/resources/advent2021/" + Day13.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		List<String> coordonnees = new ArrayList<String>();
		List<String> instructions = new ArrayList<String>();
		creation(lignes, coordonnees, instructions);
		Grille grille = new Grille(40, 40);

		for (String instruction : instructions) {
			char instructionType = StringUtils.split(instruction, "=")[0].charAt(StringUtils.split(instruction, "=")[0].length() - 1);

			if (instructionType == 'y') {
				coordonnees = foldHorizontal(coordonnees, Integer.parseInt(StringUtils.split(instruction, "=")[1]));
			}
			if (instructionType == 'x') {
				coordonnees = foldVertical(coordonnees, Integer.parseInt(StringUtils.split(instruction, "=")[1]));
			}
			Set<String> test = findDuplicates(coordonnees);

		}
		creationGrille(coordonnees, grille);
		grille.affichageEtatRestreint(40, 40);

	}

	// run1 ----
	public void run1() {
		String filename = "src/main/resources/advent2021/" + Day13.class.getSimpleName() + ".txt";
		List<String> lignes = Outil.importationString(filename);
		List<String> coordonnees = new ArrayList<String>();
		List<String> instructions = new ArrayList<String>();
		creation(lignes, coordonnees, instructions);
		Grille grille = new Grille(1400, 1400);

		// for (String instruction: instructions) {

		String instruction = instructions.get(0); // pour le run1
		char instructionType = StringUtils.split(instruction, "=")[0].charAt(StringUtils.split(instruction, "=")[0].length() - 1);
		List<String> coordonneesOutput = new ArrayList<String>();
		if (instructionType == 'y') {
			coordonneesOutput = foldHorizontal(coordonnees, Integer.parseInt(StringUtils.split(instruction, "=")[1]));
		}
		if (instructionType == 'x') {
			coordonneesOutput = foldVertical(coordonnees, Integer.parseInt(StringUtils.split(instruction, "=")[1]));
		}

		Set<String> test = findDuplicates(coordonneesOutput);

		System.out.println(coordonneesOutput.size() - test.size());
		// }

	}

	private List<String> foldHorizontal(List<String> coordonnees, int valeurAxe) {
		List<String> coordonneesOutput = new ArrayList<>();
		for (String coord : coordonnees) {

			int coordX = Integer.parseInt(StringUtils.split(coord, ",")[0]);
			int coordY = Integer.parseInt(StringUtils.split(coord, ",")[1]);

			if (coordY > valeurAxe) {
				coordY = valeurAxe - (coordY - valeurAxe);
			}
			coord = coordX + "," + coordY;
			coordonneesOutput.add(coord);
		}
		return coordonneesOutput;
	}

	private List<String> foldVertical(List<String> coordonnees, int valeurAxe) {
		List<String> coordonneesOutput = new ArrayList<>();
		for (String coord : coordonnees) {

			int coordX = Integer.parseInt(StringUtils.split(coord, ",")[0]);
			int coordY = Integer.parseInt(StringUtils.split(coord, ",")[1]);

			if (coordX > valeurAxe) {
				coordX = valeurAxe - (coordX - valeurAxe);
			}
			coord = coordX + "," + coordY;
			coordonneesOutput.add(coord);
		}
		return coordonneesOutput;
	}

	private <T> Set<T> findDuplicates(Collection<T> collection) {
		Set<T> uniques = new HashSet<>();
		return collection.stream().filter(e -> !uniques.add(e)).collect(Collectors.toSet());
	}

	private void creation(List<String> lignes, List<String> coordonnees, List<String> instructions) {

		for (String str : lignes) {
			if (!str.equals("")) {
				if (str.charAt(0) != 'f') {
					coordonnees.add(str);
				} else {
					instructions.add(str);
				}
			}
		}

	}

	private void creationGrille(List<String> coordonnees, Grille grille) {

		for (int i = 0; i < coordonnees.size(); i++) {
			int coordX = Integer.parseInt(StringUtils.split(coordonnees.get(i), ",")[0]);
			int coordY = Integer.parseInt(StringUtils.split(coordonnees.get(i), ",")[1]);
			grille.getCase(coordX, coordY).setEtat("#");
		}
		// grille.affichageEtat();
	}

}
