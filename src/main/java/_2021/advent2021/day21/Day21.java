package advent2021.day21;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

public class Day21 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day21.class.getSimpleName() + "]");
		Day21 day = new Day21();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() {

		// Init on va de 0 a 9 (plus simple pour faire modul0 10...)
		// Position va de 0 a 9, score va de 1 a 10
		int p1Position = 9; // faire -1 par rapport � l'input sur le score!
		int p2Position = 3; // faire -1 par rapport � l'input sur le score!
		int p1Score = 0;
		int p2Score = 0;
		int joueur = 1;

		// simulation des univers
		long nbParties = simuleTousLancers(p1Score, p1Position, p2Score, p2Position, joueur);
		System.out.println("nbParties: " + nbParties);

	}

	// Il y a 27 possibilites en 3 lancers pour la somme sur un d� 1/2/3: 3
	private long simuleTousLancers(int p1Score, int p1Position, int p2Score, int p2Position, int joueur) {
		long win = 0; // compte les win du Joueur1 qui gagne plus souvent

		win = win + 1 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 3)); // 1 cas pour somme=3

		win = win + 3 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 4)); // 3 cas pour somme=4

		win = win + 6 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 5)); // 6 cas pour somme=5

		win = win + 7 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 6)); // 7 cas pour somme=6

		win = win + 6 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 7)); // 6 cas pour somme=7

		win = win + 3 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 8)); // 3 cas pour somme=8

		win = win + 1 * (joue(p1Score, p1Position, p2Score, p2Position, joueur, 9)); // 1 cas pour somme=9

		return win;
	}

	public long joue(int p1Score, int p1Position, int p2Score, int p2Position, int joueur, int resultatTroisDes) {
		boolean fin = false;
		int win1 = 0;

		if (joueur == 1) {
			// P1 Turn
			int total = resultatTroisDes;
			p1Position = (p1Position + total) % 10;
			p1Score = p1Score + p1Position + 1;
			joueur = 2;
			if (p1Score >= 21) {
				fin = true;
				win1 = 1; // on compte la win du joueur 1
			}
		} else {
			if (!fin) { // P2 Turn
				int total = resultatTroisDes;
				p2Position = (p2Position + total) % 10;
				p2Score = p2Score + p2Position + 1;
				joueur = 1;
				if (p2Score >= 21) {
					fin = true;
				}
			}
		}

		if (fin) {
			return win1;
		}
		if (!fin) {
			return simuleTousLancers(p1Score, p1Position, p2Score, p2Position, joueur);
		}
		return 0;

	}

	// run1 ----
	public void run1() {

		// Init on va de 0 � 9 (plus simple pour faire module10...)
		int p1Position = 9;
		int p2Position = 3;
		int dice = 0;
		int cptRoll = 0;
		int p1Score = 0;
		int p2Score = 0;
		boolean fin = false;

		// Deterministic Game
		while (!fin) {
			// P1 Turn
			int total = 3 * (dice + 1 + dice + 3) / 2;
			dice = dice + 3;
			p1Position = (p1Position + total) % 10;
			p1Score = p1Score + p1Position + 1;
			cptRoll = cptRoll + 3;
			if (p1Score >= 1000) {
				fin = true;
			}

			if (!fin) { // P2 Turn
				total = 3 * (dice + 1 + dice + 3) / 2;
				dice = dice + 3;
				p2Position = (p2Position + total) % 10;
				p2Score = p2Score + p2Position + 1;
				cptRoll = cptRoll + 3;
				if (p2Score >= 1000) {
					fin = true;
				}
			}
		}

		System.out.println("p1Score: " + p1Score);
		System.out.println("p2Score: " + p2Score);
		System.out.println("nb lancers " + cptRoll);
		System.out.println("resultat: " + cptRoll * p2Score);
	}

}
