package advent2021.day16;

import java.util.ArrayList;
import java.util.List;

public class Paquet {

	private int version;
	private int type;
	private String litteralOuOperator;
	private int mode; // si operator (=length type ID)
	private int lengthTypeInfo; // si operator
	private long valeur; // si litteral value
	private List<Paquet> paquets = null;
	private int longueur;

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public List<Paquet> getPaquets() {
		return paquets;
	}

	public void setPaquets(List<Paquet> paquets) {
		this.paquets = paquets;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getLitteralOuOperator() {
		return litteralOuOperator;
	}

	public void setLitteralOuOperator(String litteralOuOperator) {
		this.litteralOuOperator = litteralOuOperator;
	}

	public int getLengthTypeInfo() {
		return lengthTypeInfo;
	}

	public void setLengthTypeInfo(int lengthTypeInfo) {
		this.lengthTypeInfo = lengthTypeInfo;
	}

	public long getValeur() {
		return valeur;
	}

	public void setValeur(long valeur) {
		this.valeur = valeur;
	}

	// -----

	public Paquet(int version, int type) {
		super();
		List<Paquet> paquets = new ArrayList<Paquet>();
		this.paquets = paquets;
		this.version = version;
		this.type = type;
	}

	public Paquet() {
		super();
		List<Paquet> paquets = new ArrayList<Paquet>();
		this.paquets = paquets;
	}

	// to string
	@Override
	public String toString() {
		return "Paquet [v=" + version + ", type=" + type + ", ?=" + litteralOuOperator + ", valeur=" + valeur + ", paquets=" + paquets + "]";
	}

}
