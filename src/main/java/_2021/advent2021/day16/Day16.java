package advent2021.day16;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Day16 {

	private static int sommeVersion = 0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day16.class.getSimpleName() + "]");
		Day16 day = new Day16();
		LocalDateTime start = LocalDateTime.now();
		day.run();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run() {

		// input reel
		String hexadecimal0 = "A20D6CE8F00033925A95338B6549C0149E3398DE75817200992531E25F005A18C8C8C0001849FDD43629C293004B001059363936796973BF3699CFF4C6C0068C9D72A1231C339802519F001029C2B9C29700B2573962930298B6B524893ABCCEC2BCD681CC010D005E104EFC7246F5EE7328C22C8400424C2538039239F720E3339940263A98029600A80021B1FE34C69100760B41C86D290A8E180256009C9639896A66533E459148200D5AC0149D4E9AACEF0F66B42696194031F000BCE7002D80A8D60277DC00B20227C807E8001CE0C00A7002DC00F300208044E000E69C00B000974C00C1003DC0089B90C1006F5E009CFC87E7E43F3FBADE77BE14C8032C9350D005662754F9BDFA32D881004B12B1964D7000B689B03254564414C016B004A6D3A6BD0DC61E2C95C6E798EA8A4600B5006EC0008542D8690B80010D89F1461B4F535296B6B305A7A4264029580021D1122146900043A0EC7884200085C598CF064C0129CFD8868024592FEE9D7692FEE9D735009E6BBECE0826842730CD250EEA49AA00C4F4B9C9D36D925195A52C4C362EB8043359AE221733DB4B14D9DCE6636ECE48132E040182D802F30AF22F131087EDD9A20804D27BEFF3FD16C8F53A5B599F4866A78D7898C0139418D00424EBB459915200C0BC01098B527C99F4EB54CF0450014A95863BDD3508038600F44C8B90A0801098F91463D1803D07634433200AB68015299EBF4CF5F27F05C600DCEBCCE3A48BC1008B1801AA0803F0CA1AC6200043A2C4558A710E364CC2D14920041E7C9A7040402E987492DE5327CF66A6A93F8CFB4BE60096006E20008543A8330780010E8931C20DCF4BFF13000A424711C4FB32999EE33351500A66E8492F185AB32091F1841C91BE2FDC53C4E80120C8C67EA7734D2448891804B2819245334372CBB0F080480E00D4C0010E82F102360803B1FA2146D963C300BA696A694A501E589A6C80";
		String pat = "A20D5080210CE4BB9BAFB001BD14A4574C014C004AE46A9B2E27297EECF0C013F00564776D7E3A825CAB8CD47B6C537DB99CD746674C1000D29BBC5AC80442966FB004C401F8771B61D8803D0B22E4682010EE7E59ACE5BC086003E3270AE4024E15C8010073B2FAD98E004333F9957BCB602E7024C01197AD452C01295CE2DC9934928B005DD258A6637F534CB3D89A944230043801A596B234B7E58509E88798029600BCF5B3BA114F5B3BA10C9E77BAF20FA4016FCDD13340118B929DD4FD54E60327C00BEB7002080AA850031400D002369400B10034400F30021400F20157D804AD400FE00034E000A6D001EB2004E5C00B9AE3AC3C300470029091ACADBFA048D656DFD126792187008635CD736B3231A51BA5EBDF42D4D299804F26B33C872E213C840022EC9C21FFB34EDE7C559C8964B43F8AD77570200FC66697AFEB6C757AC0179AB641E6AD9022006065CEA714A4D24C0179F8E795D3078026200FC118EB1B40010A8D11EA27100990200C45A83F12C401A8611D60A0803B1723542889537EFB24D6E0844004248B1980292D608D00423F49F9908049798B4452C0131006230C14868200FC668B50650043196A7F95569CF6B663341535DCFE919C464400A96DCE1C6B96D5EEFE60096006A400087C1E8610A4401887D1863AC99F9802DC00D34B5BCD72D6F36CB6E7D95EBC600013A88010A8271B6281803B12E124633006A2AC3A8AC600BCD07C9851008712DEAE83A802929DC51EE5EF5AE61BCD0648028596129C3B98129E5A9A329ADD62CCE0164DDF2F9343135CCE2137094A620E53FACF37299F0007392A0B2A7F0BA5F61B3349F3DFAEDE8C01797BD3F8BC48740140004322246A8A2200CC678651AA46F09AEB80191940029A9A9546E79764F7C9D608EA0174B63F815922999A84CE7F95C954D7FD9E0890047D2DC13B0042488259F4C0159922B0046565833828A00ACCD63D189D4983E800AFC955F211C700";

		// exemples
		String hexadecimal1 = "D2FE28";
		String hexadecimal2 = "38006F45291200";
		String hexadecimal3 = "EE00D40C823060";
		String hexadecimal4 = "8A004A801A8002F478";
		String hexadecimal5 = "620080001611562C8802118E34";
		String hexadecimal6 = "C0015000016115A2E0802F182340";
		String hexadecimal7 = "A0016C880162017C3686B18A3D4780";
		String hexadecimal8 = "C200B40A82";
		String hexadecimal9 = "04005AC33890";
		String hexadecimal10 = "880086C3E88112";
		String hexadecimal11 = "CE00C43D881120";
		String hexadecimal12 = "D8005AC2A8F0";
		String hexadecimal13 = "F600BC2D8F";
		String hexadecimal14 = "9C005AC2F8F0";
		String hexadecimal15 = "9C0141080250320F1802104A08";

		// convertir l'hexadecimal en bit 01
		String paquetString = convertirHexToBin(hexadecimal0);

		// decode
		Paquet paquet = decode(paquetString);
		System.out.println("valeur: " + paquet.getValeur());
		System.out.println("somme version: " + sommeVersion);
	}

	private Paquet decode(String paquetString) {

		// -----Header -----
		// decoder la version (3 premiers) et le type (3 suivants)
		int version = Integer.parseInt(StringUtils.substring(paquetString, 0, 3), 2);
		sommeVersion = sommeVersion + version;
		int type = Integer.parseInt(StringUtils.substring(paquetString, 3, 6), 2);
		Paquet paquet = new Paquet(version, type);

		int i = 1;
		if (type == 4) { // litteral value
			paquet.setLitteralOuOperator("litteral");
			boolean go = true;
			String bitGlobal = "";
			paquet.setLongueur(6);
			while (go) {
				paquet.setLongueur(paquet.getLongueur() + 5);
				String bit = StringUtils.substring(paquetString, i + 5, i + 10);
				char prefixe = bit.charAt(0);
				if (prefixe == '0') {
					go = false;
				} // dernier packet
				// lecture du packet sans le prefixe
				bit = StringUtils.substring(bit, 1, 5);
				bitGlobal = bitGlobal + bit;
				i = i + 5;
			}
			// convertir le bit global en decimal
			long bitGlobalDecimal = Long.parseLong(bitGlobal, 2);
			paquet.setValeur(bitGlobalDecimal);
			System.out.println(bitGlobalDecimal);

		} // fin litteral

		if (type != 4) { // operator value
			paquet.setLitteralOuOperator("operator");
			int mode = Integer.parseInt(StringUtils.substring(paquetString, 6, 7));
			paquet.setMode(mode);

			if (mode == 0) { // the next 15 bits are a number that represents the total length in bits of the sub-packets
				paquet.setLengthTypeInfo(Integer.parseInt(StringUtils.substring(paquetString, 7, 22), 2));
				Paquet subPaquet = new Paquet();
				int sommeLongueurSubPaquets = 0;
				while (sommeLongueurSubPaquets < paquet.getLengthTypeInfo()) {
					subPaquet = decode(StringUtils.substring(paquetString, 22 + sommeLongueurSubPaquets));
					paquet.getPaquets().add(subPaquet);
					sommeLongueurSubPaquets = sommeLongueurSubPaquets + subPaquet.getLongueur();
				}

			}
			if (mode == 1) { // the next 11 bits are a number that represents the number of sub-packets immediately contained
				paquet.setLengthTypeInfo(Integer.parseInt(StringUtils.substring(paquetString, 7, 18), 2));
				Paquet subPaquet = new Paquet();
				int nbSubPaquets = paquet.getLengthTypeInfo();
				int sommeLongueursSubPaquets = 0;
				while (nbSubPaquets > 0) {
					subPaquet = decode(StringUtils.substring(paquetString, 18 + sommeLongueursSubPaquets));
					paquet.getPaquets().add(subPaquet);
					nbSubPaquets--;
					sommeLongueursSubPaquets = sommeLongueursSubPaquets + subPaquet.getLongueur();
				}
			}

			// calcul longueur operator
			List<Paquet> packets = paquet.getPaquets();
			int L = 0; // longueur initiale du header d'un operator=18 ou 22 (selon le mode 1/0) !
			if (paquet.getMode() == 0) {
				L = 22;
			}
			if (paquet.getMode() == 1) {
				L = 18;
			}
			for (Paquet packet : packets) {
				L = L + packet.getLongueur();
			}
			paquet.setLongueur(L);

			// calcul valeur operator en fonction de ses sous paquets (PART2)
			long valeur = 0;
			switch (paquet.getType()) {

				case 0: // sum packet
					long somme = 0;
					for (Paquet packet : packets) {
						somme = somme + packet.getValeur();
					}
					valeur = somme;
					break;

				case 1: // product packet
					long produit = 1;
					for (Paquet packet : packets) {
						produit = produit * packet.getValeur();
					}
					valeur = produit;
					break;

				case 2: // minimum packet
					long min = Long.MAX_VALUE;
					for (Paquet packet : packets) {
						if (packet.getValeur() < min) {
							min = packet.getValeur();
						}
					}
					valeur = min;
					break;

				case 3: // maximum packet
					long max = Long.MIN_VALUE;
					for (Paquet packet : packets) {
						if (packet.getValeur() > max) {
							max = packet.getValeur();
						}
					}
					valeur = max;
					break;

				case 5: // greater than packet
					long gt = Long.MAX_VALUE;
					if (packets.get(0).getValeur() > packets.get(1).getValeur()) {
						valeur = 1;
					} else {
						valeur = 0;
					}
					break;

				case 6: // less than packet
					long lt = Long.MAX_VALUE;
					if (packets.get(0).getValeur() < packets.get(1).getValeur()) {
						valeur = 1;
					} else {
						valeur = 0;
					}
					break;

				case 7: // equal than packet
					long eq = Long.MAX_VALUE;
					if (packets.get(0).getValeur() == packets.get(1).getValeur()) {
						valeur = 1;
					} else {
						valeur = 0;
					}
					break;

				default:
					// default statements
			}
			// appliquer la valeur
			paquet.setValeur(valeur);
			System.out.println(valeur);

		} // fin operator

		return paquet;

	}

	public String convertirHexToBin(String hex) {
		hex = hex.toUpperCase();

		// initializing the HashMap class
		HashMap<Character, String> hashMap = new HashMap<Character, String>();

		// storing the key value pairs
		hashMap.put('0', "0000");
		hashMap.put('1', "0001");
		hashMap.put('2', "0010");
		hashMap.put('3', "0011");
		hashMap.put('4', "0100");
		hashMap.put('5', "0101");
		hashMap.put('6', "0110");
		hashMap.put('7', "0111");
		hashMap.put('8', "1000");
		hashMap.put('9', "1001");
		hashMap.put('A', "1010");
		hashMap.put('B', "1011");
		hashMap.put('C', "1100");
		hashMap.put('D', "1101");
		hashMap.put('E', "1110");
		hashMap.put('F', "1111");

		int i;
		char ch;
		String binary = "";

		for (i = 0; i < hex.length(); i++) {
			ch = hex.charAt(i);
			if (hashMap.containsKey(ch))
				binary += hashMap.get(ch);
			else {
				binary = "Invalid Hexadecimal String";
				return binary;
			}
		}
		return binary;
	}

}
