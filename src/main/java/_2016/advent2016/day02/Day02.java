package advent2016.day02;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day02 {
	
	 private static Logger logger = LoggerFactory.getLogger(Day02.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day02.class.getSimpleName()+"]");
		Day02 Day = new Day02();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		//on definit les boutons
		Bouton b1= new Bouton("1");		Bouton b2= new Bouton("2");		Bouton b3= new Bouton("3");
		Bouton b4= new Bouton("4");		Bouton b5= new Bouton("5");		Bouton b6= new Bouton("6");
		Bouton b7= new Bouton("7");		Bouton b8= new Bouton("8");		Bouton b9= new Bouton("9");
		Bouton ba= new Bouton("A");		Bouton bb= new Bouton("B");		Bouton bc= new Bouton("C");
		Bouton bd= new Bouton("D");
		b1.setDown(b3); b1.setLeft(b1); b1.setUp(b1); b1.setRight(b1);
		b2.setDown(b6); b2.setLeft(b2); b2.setUp(b2); b2.setRight(b3);
		b3.setDown(b7); b3.setLeft(b2); b3.setUp(b1); b3.setRight(b4);
		b4.setDown(b8); b4.setLeft(b3); b4.setUp(b4); b4.setRight(b4);
		b5.setDown(b5); b5.setLeft(b5); b5.setUp(b5); b5.setRight(b6);
		b6.setDown(ba); b6.setLeft(b5); b6.setUp(b2); b6.setRight(b7);
		b7.setDown(bb); b7.setLeft(b6); b7.setUp(b3); b7.setRight(b8);
		b8.setDown(bc); b8.setLeft(b7); b8.setUp(b4); b8.setRight(b9);
		b9.setDown(b9); b9.setLeft(b8); b9.setUp(b9); b9.setRight(b9);
		ba.setDown(ba); ba.setLeft(ba); ba.setUp(b6); ba.setRight(bb);
		bb.setDown(bd); bb.setLeft(ba); bb.setUp(b7); bb.setRight(bc);
		bc.setDown(bc); bc.setLeft(bb); bc.setUp(b8); bc.setRight(bc);
		bd.setDown(bd); bd.setLeft(bd); bd.setUp(bb); bd.setRight(bd);
		
		//lecture fichier
		String filename = "src/main/resources/advent2016/Day02.txt";
		List<String> fichier = Outil.importationString(filename);
		
		//parcours
		Bouton boutonEnCours= b5;
		for (String ligne:fichier) {
			for (int i=0;i<ligne.length();i++) {
				String lettre = StringUtils.substring(ligne, i,i+1);
				switch (lettre) {
				case "U": boutonEnCours= boutonEnCours.getUp();
					break;
				case "D": boutonEnCours= boutonEnCours.getDown();
				break;
				case "R": boutonEnCours= boutonEnCours.getRight();
				break;
				case "L": boutonEnCours= boutonEnCours.getLeft();
				break;
				default:
					break;
				}
			}
			System.out.print(boutonEnCours.getNom());
		}
		System.out.println();
	}

	public void run1() throws IOException {
		//start à 5, si up: -3, si down: +3, etc
		String filename = "src/main/resources/advent2016/Day02.txt";
		List<String> fichier = Outil.importationString(filename);
		int chiffreEnCours=5;
		
		for (String ligne:fichier) {
			for (int i=0;i<ligne.length();i++) {
				String lettre = StringUtils.substring(ligne, i,i+1);
				switch (lettre) {
				case "U": if (chiffreEnCours-3>0) {
					chiffreEnCours=chiffreEnCours-3;
				}
					break;
				case "D": if (chiffreEnCours+3<10) {
					chiffreEnCours=chiffreEnCours+3;
				}
					break;
				case "R": if (chiffreEnCours+1!=4 && chiffreEnCours+1!=7 && chiffreEnCours+1!=10) {
					chiffreEnCours=chiffreEnCours+1;
				}
					break;
				case "L": if (chiffreEnCours-1!=0 && chiffreEnCours-1!=3 &&chiffreEnCours-1!=6) {
					chiffreEnCours=chiffreEnCours-1;
				}
					break;
				default:
					break;
				}
				
			}
			System.out.print(chiffreEnCours);
		}
		System.out.println();
	}
	
	
}

	
