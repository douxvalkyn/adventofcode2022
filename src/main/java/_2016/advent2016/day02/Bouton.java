package advent2016.day02;

public class Bouton {

	
	private String nom;
	private Bouton up;
	private Bouton down;
	private Bouton left;
	private Bouton right;
	
	public Bouton(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Bouton getUp() {
		return up;
	}

	public void setUp(Bouton up) {
		this.up = up;
	}

	public Bouton getDown() {
		return down;
	}

	public void setDown(Bouton down) {
		this.down = down;
	}

	public Bouton getLeft() {
		return left;
	}

	public void setLeft(Bouton left) {
		this.left = left;
	}

	public Bouton getRight() {
		return right;
	}

	public void setRight(Bouton right) {
		this.right = right;
	}

	@Override
	public String toString() {
		return "Bouton [nom=" + nom + "]";
	}

	
	
	

	
}

