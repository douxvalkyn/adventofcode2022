package advent2016.day24;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Case;
import advent2016.outils.Graph;
import advent2016.outils.Grille;
import advent2016.outils.Outil;

public class Day24 {

	private static Logger logger = LoggerFactory.getLogger(Day24.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day24.class.getSimpleName() + "]");
		Day24 Day = new Day24();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() {
		List<String> input = lectureInput();
		Grille grille = new Grille(input.get(0).length(),input.size());
		initGrille(grille, input);
		Graph graphe = new Graph();
		initGraphe(graphe, grille);
		//grille.affichageEtat();

		int minDist=10000;
		List<Integer> minElements=new ArrayList<Integer>();
		List<List<Integer>> elementsHisto=new ArrayList<List<Integer>>();
		for (int j=0;j<6000;j++) {

			//liste aleatoire des numeros à relier (tjs commencer par 0)
			List<Integer> elements=new ArrayList<Integer>();
			elements.add(1);		elements.add(2);		elements.add(3);		elements.add(4);elements.add(5);elements.add(6);elements.add(7);
			Collections.shuffle(elements);
			elements.add(0, 0);
			if (!elementsHisto.contains(elements)) {
				elementsHisto.add(elements);

			//calculer la somme des distances du chemin aleatoire
			int dist=0;
			for (int i=0;i<elements.size()-1;i++) {
				dist = dist+distanceEntreDeuxNumeros(graphe, grille,elements.get(i),elements.get(i+1));
			}
			if (dist<minDist) {
				minDist=dist;
				minElements=elements;
			}
		}
		}
		System.out.println("distance minimale: "+minDist +", chemin: "+minElements);


	}

	public int distanceEntreDeuxNumeros(Graph graphe, Grille grille, int i, int j) {
		List<Case> mesCases = grille.getCases();
		int num1 = 0;
		int num2 = 0;
		int dist = 0;
		for (Case maCase:mesCases) {
			if (maCase.getEtat().equals(""+i)) {
				num1=Integer.valueOf( maCase.getRang() );
			}
		}
		for (Case maCase2:mesCases) {
			if (maCase2.getEtat().equals(""+j)) {
				num2=Integer.valueOf( maCase2.getRang() );
				dist=graphe.uniformSearch(num1, num2);
			}
		}
		return dist;
	}

	private void initGraphe(Graph graphe, Grille grille) {
		List<Case> mesCases = grille.getCases();

		//creation noeuds
		for (Case maCase: mesCases) {
			graphe.addNode(maCase.getRang());
		}

		//creation liens
		for (Case maCase: mesCases) {
			if (!maCase.getEtat().equals("#")){
				List<Case> voisins = maCase.getCasesAdjacentes2(grille);
				for (Case voisin: voisins) {
					if (!voisin.getEtat().equals("#")) {
						graphe.addEdge(maCase.getRang(), voisin.getRang(), 1);
					}	
				}
			}
		}
	}

	private void initGrille(Grille grille, List<String> input) {
		int y=-1;
		for(String ligne: input) {
			y++;
			for(int x=0;x<ligne.length();x++) {
				String carac = ""+ligne.charAt(x);
				grille.getCase(x, y).setEtat(carac);
			}
		}
	}



	public void run2() {
		List<String> input = lectureInput();
		Grille grille = new Grille(input.get(0).length(),input.size());
		initGrille(grille, input);
		Graph graphe = new Graph();
		initGraphe(graphe, grille);
		//grille.affichageEtat();

		int minDist=10000;
		List<Integer> minElements=new ArrayList<Integer>();
		List<List<Integer>> elementsHisto=new ArrayList<List<Integer>>();
		for (int j=0;j<6000;j++) {

			//liste aleatoire des numeros à relier (tjs commencer par 0)
			List<Integer> elements=new ArrayList<Integer>();
			elements.add(1);		elements.add(2);		elements.add(3);		elements.add(4);elements.add(5);elements.add(6);elements.add(7);
			Collections.shuffle(elements);
			elements.add(0, 0); // 0 au debut
			elements.add(0); // 0 à la fin
			if (!elementsHisto.contains(elements)) {
				elementsHisto.add(elements);

			//calculer la somme des distances du chemin aleatoire
			int dist=0;
			for (int i=0;i<elements.size()-1;i++) {
				dist = dist+distanceEntreDeuxNumeros(graphe, grille,elements.get(i),elements.get(i+1));
			}
			if (dist<minDist) {
				minDist=dist;
				minElements=elements;
			}
		}
		}
		System.out.println("distance minimale: "+minDist +", chemin: "+minElements);


	}


	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day24.txt";
		return Outil.importationString(filename);
	}

}