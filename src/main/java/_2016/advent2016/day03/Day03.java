package advent2016.day03;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day03 {
	
	 private static Logger logger = LoggerFactory.getLogger(Day03.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day03.class.getSimpleName()+"]");
		Day03 Day = new Day03();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}



	public void run1() throws IOException {
		String filename = "src/main/resources/advent2016/Day03.txt";
		List<String> fichier = Outil.importationString(filename);
		int possibles=0;
		for (String triangle: fichier) {
			int x = Integer.parseInt(StringUtils.split(triangle, " ")[0]);
			int y = Integer.parseInt(StringUtils.split(triangle, " ")[1]);
			int z = Integer.parseInt(StringUtils.split(triangle, " ")[2]);
			if (x<(y+z) && y<(x+z) && z<(x+y)) {
				possibles++;
			}
		}
		System.out.println("nb triangles possibles run1: "+possibles);
	
}
	public void run2() {
		String filename = "src/main/resources/advent2016/Day03.txt";
		List<String> fichier = Outil.importationString(filename);
		int possibles=0;
		
		List<Integer> liste1 = new ArrayList<>();
		List<Integer> liste2 = new ArrayList<>();
		List<Integer> liste3 = new ArrayList<>();
		List<List<Integer>> listes = new ArrayList<>();
		for (String triangle: fichier) {
			int x = Integer.parseInt(StringUtils.split(triangle, " ")[0]);
			int y = Integer.parseInt(StringUtils.split(triangle, " ")[1]);
			int z = Integer.parseInt(StringUtils.split(triangle, " ")[2]);
			liste1.add(x);
			liste2.add(y);
			liste3.add(z);
		}
		listes.add(liste1);
		listes.add(liste2);
		listes.add(liste3);
		
		//triangles des 3 listes (on prend les dimensions 3 par 3 (x,y,z) )
		for (int j=0;j<listes.size();j++) {
			List<Integer> listeEnCours = listes.get(j);
			for (int i=0;i<listeEnCours.size();i=i+3) {
				Integer x = listeEnCours.get(i);
				Integer y = listeEnCours.get(i+1);
				Integer z = listeEnCours.get(i+2);
				if (x<(y+z) && y<(x+z) && z<(x+y)) {
					possibles++;
				}
			}
		}
		System.out.println("nb triangles possibles run2: "+possibles);
	}
	
}

	
