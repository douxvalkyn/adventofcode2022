package advent2016.day11;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

//Utilisation d'un algo BFS (breadth-first-search)
/**
 * BFS analyse un graphe en largeur (alors qe DFS analyse un graphe en
 * profondeur) Algo BFS pour rechercher un noeud particulier: - on visite un
 * premier noeud et on l'ajoute à la liste des noeuds visités et à la queue -
 * tant qu'il reste des noeuds dans la queue: - on prend le premier noeud de la
 * file (on le supprime de la queue) et on regarde si c'est celui qu'on cherche
 * - si ce n'est pas celui qu'on cherche, on ajoute à la queue les noeuds
 * adjacents (ou suivants) s'ils n'ont pas déjà été visités (important pour
 * éviter de tourner en rond !) - etc...
 * 
 * L'algorithme BFS est tres efficace pour analyser un graphe et pour trouver le
 * shortest path entre deux noeuds (pour un graphe non pondéré, BFS = dijkstra).
 * 
 * @author karl4
 *
 */

public class Day11 {

	private static Logger logger = LoggerFactory.getLogger(Day11.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day11.class.getSimpleName() + "]");
		Day11 Day = new Day11();
		LocalDateTime start = LocalDateTime.now();
 		Day.run1();
 		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	public void run2() {
		List<Floor> initialFloors = lectureInput();
		int resultat = bfs(initialFloors);
		System.err.println(resultat+24); 
 		//Any complete pairs on floor 1 add 12 steps to the solution
 		//Ainsi, le run2 demande d'ajouter 2 paires au floor0, donc il suffit d'ajouter 24 au resultat du run 1 ! (sinon c'est bcp trop long avec la methode du run1,
 		//il faudrait optimiser, soit en changeant la classe Floor par une liste d'int; soit avec des optimisations de parcours.
	}




	public void run2old() {
		List<Floor> initialFloors = lectureInput();

		// Ajout de 4 elements à l'étage 0
		MicrochipOrGenerator m1 = new MicrochipOrGenerator();
		m1.setElementChimique("Elerium");
		m1.setType("M");
		MicrochipOrGenerator g1 = new MicrochipOrGenerator();
		g1.setElementChimique("Elerium");
		g1.setType("G");
		MicrochipOrGenerator m2 = new MicrochipOrGenerator();
		m2.setElementChimique("Dilithium");
		m2.setType("M");
		MicrochipOrGenerator g2 = new MicrochipOrGenerator();
		g2.setElementChimique("Dilithium");
		g2.setType("G");

		initialFloors.get(0).getMicroChipOrGenerator().add(m1);
		initialFloors.get(0).getMicroChipOrGenerator().add(g1);
		initialFloors.get(0).getMicroChipOrGenerator().add(m2);
		initialFloors.get(0).getMicroChipOrGenerator().add(g2);

		// lancement de l'algorithme de recherche
		int resultat = bfs(initialFloors);
		System.err.println(resultat);
	}

	public void run1() {
		List<Floor> initialFloors = lectureInput();
		int resultat = bfs(initialFloors);
		System.err.println(resultat);
	}

	
	public int bfs(List<Floor> initialFloors) {
		
		Floors initialState = new Floors(initialFloors, 0, 0); // 0 depl, ascenseur au niveau 0
		Queue<Floors> queue = new LinkedList<>(); // creation d'une queue des étages
		queue.add(initialState);

		Set<Floors> visitedStates = new HashSet<>(); // historique des etages visites
		visitedStates.add(initialState);

		int nbElementsTotaux = calculNbElements(initialState);

		int cpt = 0;
		
		while (!queue.isEmpty()) {
			cpt++;
			if (cpt % 1000 == 0) {
				System.out.println(cpt);
			}

			Floors currentFloors = queue.poll();
			List<Floor> currentFloorFloors = currentFloors.floors;
			int currentSteps = currentFloors.stepsTaken;
			int currentFloor = currentFloors.currentFloor;

			// done
			if (currentFloorFloors.get(3).getMicroChipOrGenerator().size() == nbElementsTotaux) {
				System.out.println(cpt);
				return currentSteps;
			}

			// nextFloorStates = copie de currentFloorFloors
			List<Floor> nextFloorStates = copie(currentFloorFloors);


			// vers le HAUT
			if (currentFloor != 3) {

				// lister les elements à l'étage de départ
				List<MicrochipOrGenerator> objetsEtageDepart = nextFloorStates.get(currentFloor)
						.getMicroChipOrGenerator();
				List<MicrochipOrGenerator> listeObjetsEtageDepart = new ArrayList<MicrochipOrGenerator>();
				// en faire des copies
				for (MicrochipOrGenerator objet : objetsEtageDepart) {
					MicrochipOrGenerator objetCopie = new MicrochipOrGenerator(objet.getElementChimique(),
							objet.getType());
					listeObjetsEtageDepart.add(objetCopie);
				}

				// on les prend un par un et on les mets à l'étage supérieur
				int nbObjets = listeObjetsEtageDepart.size();
				for (int l = nbObjets - 1; l >= 0; l--) {
					nextFloorStates.get(currentFloor + 1).getMicroChipOrGenerator().add(listeObjetsEtageDepart.get(l));
					MicrochipOrGenerator toRemove = listeObjetsEtageDepart.get(l);
					for (int i = nbObjets - 1; i >= 0; i--) {
						if (objetsEtageDepart.get(i).getElementChimique().equals(toRemove.getElementChimique())
								&& (objetsEtageDepart.get(i).getType() == toRemove.getType())) {
							nextFloorStates.get(currentFloor).getMicroChipOrGenerator().remove(objetsEtageDepart.get(i));
						}
					}

					if (checkEtagesPossibles(nextFloorStates)) {
						Floors nextState = new Floors(nextFloorStates, currentSteps + 1, currentFloor + 1);
						if (!checkVisited(visitedStates, nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}

					}
					nextFloorStates = copie(currentFloorFloors);
					objetsEtageDepart = nextFloorStates.get(currentFloor).getMicroChipOrGenerator();

				}

				// on en prend 2 parmi ceux qui sont compatibles //
				if (nbObjets >= 2) {
					for (int l = nbObjets - 1; l >= 0; l--) {
						for (int m = l - 1; m >= 0; m--) {
							List<MicrochipOrGenerator> toMove = new ArrayList<MicrochipOrGenerator>();
							toMove.add(objetsEtageDepart.get(l));
							toMove.add(objetsEtageDepart.get(m));

							nextFloorStates.get(currentFloor + 1).getMicroChipOrGenerator().addAll(toMove);
							nextFloorStates.get(currentFloor).getMicroChipOrGenerator().removeAll(toMove);

							if (checkEtagesPossibles(nextFloorStates)) {
								Floors nextState = new Floors(nextFloorStates, currentSteps + 1, currentFloor + 1);
								if (!checkVisited(visitedStates, nextState)) {
									queue.add(nextState);
									visitedStates.add(nextState);
								}
							}
							nextFloorStates = copie(currentFloorFloors);
							objetsEtageDepart = nextFloorStates.get(currentFloor).getMicroChipOrGenerator();
						}
					}
				}
			}

			// vers le BAS
			if (currentFloor != 0) {

				// lister les elements à l'étage de départ
				List<MicrochipOrGenerator> objetsEtageDepart = nextFloorStates.get(currentFloor)
						.getMicroChipOrGenerator();
				List<MicrochipOrGenerator> listeObjetsEtageDepart = new ArrayList<MicrochipOrGenerator>();
				// en faire des copies
				for (MicrochipOrGenerator objet : objetsEtageDepart) {
					MicrochipOrGenerator objetCopie = new MicrochipOrGenerator(objet.getElementChimique(),
							objet.getType());
					listeObjetsEtageDepart.add(objetCopie);
				}

				// on les prend un par un et on les mets à l'étage supérieur
				int nbObjets = listeObjetsEtageDepart.size();
				for (int l = nbObjets - 1; l >= 0; l--) {
					nextFloorStates.get(currentFloor - 1).getMicroChipOrGenerator().add(listeObjetsEtageDepart.get(l));
					MicrochipOrGenerator toRemove = listeObjetsEtageDepart.get(l);
					for (int i = nbObjets - 1; i >= 0; i--) {
						if (objetsEtageDepart.get(i).getElementChimique().equals(toRemove.getElementChimique())
								&& (objetsEtageDepart.get(i).getType() == toRemove.getType())) {
							nextFloorStates.get(currentFloor).getMicroChipOrGenerator()
									.remove(objetsEtageDepart.get(i));
						}
					}

					if (checkEtagesPossibles(nextFloorStates)) {
						Floors nextState = new Floors(nextFloorStates, currentSteps + 1, currentFloor - 1);
						if (!checkVisited(visitedStates, nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}

					}
					nextFloorStates = copie(currentFloorFloors);
					objetsEtageDepart = nextFloorStates.get(currentFloor).getMicroChipOrGenerator();
				}

				// on en prend 2 parmi ceux qui sont compatibles //
				if (nbObjets >= 2) {
					for (int l = nbObjets - 1; l >= 0; l--) {
						for (int m = l - 1; m >= 0; m--) {
							List<MicrochipOrGenerator> toMove = new ArrayList<MicrochipOrGenerator>();
							toMove.add(objetsEtageDepart.get(l));
							toMove.add(objetsEtageDepart.get(m));

							nextFloorStates.get(currentFloor - 1).getMicroChipOrGenerator().addAll(toMove);
							nextFloorStates.get(currentFloor).getMicroChipOrGenerator().removeAll(toMove);

							if (checkEtagesPossibles(nextFloorStates)) {
								Floors nextState = new Floors(nextFloorStates, currentSteps + 1, currentFloor - 1);
								if (!checkVisited(visitedStates, nextState)) {
									queue.add(nextState);
									visitedStates.add(nextState);
								}
							}
							nextFloorStates = copie(currentFloorFloors);
							objetsEtageDepart = nextFloorStates.get(currentFloor).getMicroChipOrGenerator();
						}
					}
				}
			}

		}
		return -1;
	}

	private int calculNbElements(Floors initialState) {
		List<Floor> floors = initialState.floors;
		int cpt = 0;
		for (Floor floor : floors) {
			cpt = cpt + floor.getMicroChipOrGenerator().size();
		}

		return cpt;
	}

	public boolean compareDeuxFloors(Floors fl1, Floors fl2) {
		boolean checkFloors = true;
		if (fl1.currentFloor != fl2.currentFloor) {
			return false;
		}

		for (int i = 0; i < 4; i++) {
			List<MicrochipOrGenerator> liste1Etage = fl1.floors.get(i).getMicroChipOrGenerator();
			List<MicrochipOrGenerator> liste2Etage = fl2.floors.get(i).getMicroChipOrGenerator();
			liste1Etage = liste1Etage.stream().sorted(Comparator.comparing(MicrochipOrGenerator::getElementChimique))
					.sorted(Comparator.comparing(MicrochipOrGenerator::getType)).collect(Collectors.toList());
			liste2Etage = liste2Etage.stream().sorted(Comparator.comparing(MicrochipOrGenerator::getElementChimique))
					.sorted(Comparator.comparing(MicrochipOrGenerator::getType)).collect(Collectors.toList());

			if (liste1Etage.size() == liste2Etage.size()) {
				for (int j = 0; j < liste1Etage.size(); j++) {
					boolean testElement = liste1Etage.get(j).getElementChimique()
							.equals(liste2Etage.get(j).getElementChimique());
					boolean testType = liste1Etage.get(j).getType().equals(liste2Etage.get(j).getType());
					if (testElement && testType) {
						checkFloors = checkFloors && true;
					} else {
						checkFloors = false;
					}
				}
			} else {
				return false;
			}
		}
		return checkFloors;
	}

	public boolean checkVisited(Set<Floors> visitedStates, Floors nextFloors) {
		if (visitedStates == null) {
			return false;
		}
		for (Floors floors : visitedStates) {
			if (compareDeuxFloors(floors, nextFloors)) {
				return true;
			}
		}
		return false;
	}

	private static class Floors {
		public List<Floor> floors;
		public int stepsTaken;
		public int currentFloor;

		public Floors(List<Floor> floors, int stepsTaken, int currentFloor) {
			this.floors = floors;
			this.stepsTaken = stepsTaken;
			this.currentFloor = currentFloor;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || !(o instanceof Floors)) {
				return false;
			}

			Floors tmp = (Floors) o;

			boolean checkFloors = false;

			for (int i = 0; i < 4; i++) {
				List<MicrochipOrGenerator> liste1Etage = this.floors.get(i).getMicroChipOrGenerator();
				List<MicrochipOrGenerator> liste2Etage = tmp.floors.get(i).getMicroChipOrGenerator();
				if (liste1Etage.size() == liste2Etage.size()) {
					for (int j = 0; j < liste1Etage.size(); j++) {
						boolean testElement = liste1Etage.get(j).getElementChimique()
								.equals(liste2Etage.get(j).getElementChimique());
						boolean testType = liste1Etage.get(j).getType().equals(liste2Etage.get(j).getType());
						if (testElement && testType) {
							checkFloors = checkFloors && true;
						} else {
							checkFloors = false;
						}
					}
				} else {
					checkFloors = false;
				}
			}

			return this.currentFloor == tmp.currentFloor && checkFloors;
		}

		@Override
		public int hashCode() {
			return Objects.hash(currentFloor, floors);
		}
	}

	public boolean checkEtagesPossibles(List<Floor> etages) {
		// verifier que tous les etages sont ok (certains ne le seront pas, donc il faut
		// supprimer cette possibilité)
		// un etage est invalide si: un microchip est à un étage avec un autre
		// generateur que son complémentaire

		List<Floor> etagesAVerifier = copie(etages);
		for (Floor etage : etagesAVerifier) {
			List<MicrochipOrGenerator> objetsEtage = etage.getMicroChipOrGenerator();
			for (MicrochipOrGenerator obj : objetsEtage) {
				boolean protegee = false;
				if (obj.getType().equals("M")) { // attention, un microchip, s'il y a un autre generateur que son
													// complementaire et que son complementaire n'est pas là, boom
					for (MicrochipOrGenerator obj2 : objetsEtage) {
						if (obj2.getType().equals("G") && obj2.getElementChimique().equals(obj.getElementChimique())) {
							// microchip protegée par son generateur complementaire
							protegee = true;
						}
					}
					for (MicrochipOrGenerator obj2 : objetsEtage) {
						if (obj2.getType().equals("G") && !obj2.getElementChimique().equals(obj.getElementChimique())) {
							// presence d'un generateur d'un autre element
							if (!protegee) {
								return false;
							}
						}
					}
				}

			} // fin etage
		}

		return true;
	}

	public List<List<Floor>> checkListeFloorsPossibles(List<List<Floor>> listeFloorsPossibles) {
		// verifier que tous les etages sont ok (certains ne le seront pas, donc il faut
		// supprimer cette possibilité)
		// un etage est invalide si: un microchip est à un étage avec un autre
		// generateur que son complémentaire
		for (int i = listeFloorsPossibles.size() - 1; i >= 0; i--) {
			List<Floor> etages = listeFloorsPossibles.get(i);
			List<Floor> etagesAVerifier = copie(etages);
			outerloop: for (Floor etage : etagesAVerifier) {
				List<MicrochipOrGenerator> objetsEtage = etage.getMicroChipOrGenerator();
				for (MicrochipOrGenerator obj : objetsEtage) {
					boolean etageInvalide = false;
					boolean protegee = false;
					if (obj.getType().equals("M")) { // attention, un microchip, s'il y a un autre generateur que son
														// complementaire et que son complementaire n'est pas là, boom
						for (MicrochipOrGenerator obj2 : objetsEtage) {
							if (obj2.getType().equals("G")
									&& obj2.getElementChimique().equals(obj.getElementChimique())) {
								// microchip protegée par son generateur complementaire
								protegee = true;
							}
						}
						for (MicrochipOrGenerator obj2 : objetsEtage) {
							if (obj2.getType().equals("G")
									&& !obj2.getElementChimique().equals(obj.getElementChimique())) {
								// presence d'un generateur d'un autre element
								if (!protegee) {
									etageInvalide = true;
								}
							}
						}
					}

					// action si etage invalide
					if (etageInvalide) {
						listeFloorsPossibles.remove(etages);
						break outerloop;
					}
				} // fin etage
			}
		}
		return listeFloorsPossibles;
	}

	public static List<int[]> generate(int n, int r) {
		List<int[]> liste = new ArrayList<int[]>();
		Iterator<int[]> iterator = CombinatoricsUtils.combinationsIterator(n, r);
		int[] combination = null;
		while (iterator.hasNext()) {
			combination = iterator.next();
			liste.add(combination);
		}
		return liste;
	}

	private List<Floor> copie(List<Floor> floors) {
		List<Floor> floors2 = new ArrayList<Floor>();
		for (Floor floor : floors) {
			Floor floor2 = new Floor(floor);
			floors2.add(floor2);
		}
		return floors2;
	}

	private List<Floor> lectureInput() {
		String filename = "src/main/resources/advent2016/day11c.txt";
		List<String> input = Outil.importationString(filename);

		List<Floor> floors = new ArrayList<Floor>();
		for (int i = 1; i < input.size() + 1; i++) {
			Floor floor = new Floor();
			floor.setNum(i);
			floors.add(floor);
			String ligne = input.get(i - 1);
			String[] mots = StringUtils.split(ligne, ' ');
			for (int j = 0; j < mots.length; j++) {
				String mot = mots[j];
				MicrochipOrGenerator mg = new MicrochipOrGenerator();
				if (mot.equals("generator.") | mot.equals("generator") | mot.equals("generator,")) {
					mg.setElementChimique(mots[j - 1]);
					mg.setType("G");
					floors.get(i - 1).getMicroChipOrGenerator().add(mg);
				}
				if (mot.equals("microchip.") | mot.equals("microchip") | mot.equals("microchip,")) {
					mg.setElementChimique(StringUtils.split(mots[j - 1], '-')[0]);
					mg.setType("M");
					floors.get(i - 1).getMicroChipOrGenerator().add(mg);
				}

			}

		}
		return floors;

	}

}
