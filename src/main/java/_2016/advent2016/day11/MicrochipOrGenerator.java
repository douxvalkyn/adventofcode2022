package advent2016.day11;

public class MicrochipOrGenerator {

	String elementChimique;
	String type;

	
	/* constructeurs */
	
	public MicrochipOrGenerator() {
		super();
	}

	public MicrochipOrGenerator(String elementChimique, String type) {
		super();
		this.elementChimique = elementChimique;
		this.type = type;
	}

	public MicrochipOrGenerator(MicrochipOrGenerator microchipOrGenerator) {
		this.elementChimique=microchipOrGenerator.getElementChimique();
		this.type=microchipOrGenerator.getType();
	}
	

	/*getters and setters*/
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	//getters and setters
	public String getElementChimique() {
		return elementChimique;
	}

	public void setElementChimique(String elementChimique) {
		this.elementChimique = elementChimique;
	}

	
	/*methodes*/
	@Override
	public String toString() {
		return "["+ elementChimique + " "+type +"]";
	}

	
}
