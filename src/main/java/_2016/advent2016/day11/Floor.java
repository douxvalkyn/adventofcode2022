package advent2016.day11;

import java.util.ArrayList;
import java.util.List;

public class Floor {
	
	int num;
	List<MicrochipOrGenerator> microChipOrGenerator;
	int nbDeplacements;
	
	public Floor() {
		super();
		this.microChipOrGenerator = new ArrayList<MicrochipOrGenerator>();
	}
	
	public Floor(Floor floor) {
		this.num=floor.num;
		this.nbDeplacements=floor.nbDeplacements;
		this.microChipOrGenerator = new ArrayList<>();
		List<MicrochipOrGenerator> mcog = floor.getMicroChipOrGenerator();
		for (int i=0;i<mcog.size();i++) {
			MicrochipOrGenerator mg = new MicrochipOrGenerator();
			mg.setElementChimique(mcog.get(i).getElementChimique());
			mg.setType(mcog.get(i).getType());
			microChipOrGenerator.add(mg);
		}

	}
	
	
	
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public List<MicrochipOrGenerator> getMicroChipOrGenerator() {
		return microChipOrGenerator;
	}
	public void setMicroChipOrGenerator(List<MicrochipOrGenerator> microChipOrGenerator) {
		this.microChipOrGenerator = microChipOrGenerator;
	}
	
	public int getNbDeplacements() {
		return nbDeplacements;
	}
	public void setNbDeplacements(int nbDeplacements) {
		this.nbDeplacements = nbDeplacements;
	}
	@Override
	public String toString() {
		return String.format("[n°%s, elts=%s]", num, 
				microChipOrGenerator);
	}

	
}
