package advent2016.day05;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class Day05 {

	private static Logger logger = LoggerFactory.getLogger(Day05.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day05.class.getSimpleName()+"]");
		Day05 Day = new Day05();
		LocalDateTime start = LocalDateTime.now();
		//Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}



	public void run1() throws IOException {
	
		// String input = "abc";
		 String input = "uqwqemis";
		 String password = "";
		 int i=0;
		 
		 while (password.length()<8) {
			String input2 = input+i;
		    String md5Hex = DigestUtils.md5Hex(input2);
		   
		    if (StringUtils.startsWith(md5Hex, "00000")){
		    	 password=password+ StringUtils.substring(md5Hex,5,6);
		    }
		
		    i++;
		 }
		 System.out.println(password);
	}



	public void run2() throws IOException {
		
		// String input = "abc";
		 String input = "uqwqemis";
		 String password = "        ";
		 int i=0;
		 int nbPositionsTrouvees=0;
		 
		 while (nbPositionsTrouvees<8) {
			String input2 = input+i;
		    String md5Hex = DigestUtils.md5Hex(input2);
		   
		    if (StringUtils.startsWith(md5Hex, "00000")){
		    	 String position = StringUtils.substring(md5Hex,5,6);
		    	 String character = StringUtils.substring(md5Hex,6,7);
		    	 if (position.equals("0") | position.equals("1") | position.equals("2") | position.equals("3") | position.equals("4") | position.equals("5") | position.equals("6") | position.equals("7")) {
		    		String test = password.substring(Integer.valueOf(position),Integer.valueOf(position)+1);
		    		 if ((password.substring(Integer.valueOf(position),Integer.valueOf(position)+1)).equals(" ") ) {
		    		 password = password.substring(0, Integer.valueOf(position)) + character + password.substring(Integer.valueOf(position)+1);
		    		 nbPositionsTrouvees++;
		    		 }
		    	 }
		    }
		    i++;
		 }
		 System.out.println(password);
	}
}


