package advent2016.day08;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Case;
import advent2016.outils.Grille;
import advent2016.outils.Outil;



public class Day08 {

	private static Logger logger = LoggerFactory.getLogger(Day08.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day08.class.getSimpleName()+"]");
		Day08 Day = new Day08();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		run1();
		
	}

	public void run1() {
		//Grille grille = initialiserGrille(7,3);
		Grille grille = initialiserGrille(50,6);
		List<String> fichier = lectureInput();
		grille=applicationRegles(grille, fichier);
		int nb=compterPixelsOn(grille);
		System.out.println(nb);
		grille.affichageEtat();
	}

	
	private int compterPixelsOn(Grille grille) {
		int cpt=0;
		for ( Case pixel: grille.getCases()) {
			if (pixel.getEtat().equals("#")) {
				cpt++;
			}
		}
		return cpt;
	}

	private Grille applicationRegles(Grille grille, List<String> fichier) {
		for (String regle: fichier) {
			 Grille grille2 = new Grille(grille);
			
			switch (StringUtils.split(regle," ")[0]) {
			case "rect":
				grille=rec(grille, grille2, regle);
				break;
			case "rotate":
				grille=rotate(grille, grille2, regle);
				break;
			default:
				break;
			}
		}
		return grille;
		
	}

	private Grille rotate(Grille grille, Grille grille2, String regle) {
		String columnOrRow=StringUtils.split(regle," ")[1];
		int xy=Integer.valueOf( StringUtils.split( StringUtils.split(regle," ")[2], "=")[1]);
		int by=Integer.valueOf( StringUtils.split(regle," ")[4]);
		
		if (columnOrRow.equals("row")) {
			//decaler les cases de la ligne xy de by vers la droite
			for (int i=0;i<grille2.getNbCol();i++) {
				if (grille.getCase(i, xy).getEtat().equals("#")) {
					if (grille2.getCase(i, xy).getValeur()!=1) { //valeur=1 > la case a été mise ON pendant cette regle, donc on ne l'efface pas apres !
					grille2.getCase(i, xy).setEtat("_");
					}
					int position=i+by;
					if (position>=grille2.getNbCol()) {
						position=(i+by)%grille2.getNbCol();
					}
						
					grille2.getCase(position, xy).setEtat("#");
					grille2.getCase(position, xy).setValeur(1);
				}
			}
			//remettre toutes les valeurs des cases à 0
			for (Case c:grille2.getCases()) {
				c.setValeur(0);
			}
		}
		
		if (columnOrRow.equals("column")) {
			//decaler les cases de la ligne xy de by vers la droite
			for (int i=0;i<grille2.getNbLignes();i++) {
				if (grille.getCase(xy, i).getEtat().equals("#")) {
					if (grille2.getCase(xy, i).getValeur()!=1) { //valeur=1 > la case a été mise ON pendant cette regle, donc on ne l'efface pas apres !
					grille2.getCase(xy, i).setEtat("_");
					}
					int position=i+by;
					if (position>=grille2.getNbLignes()) {
						position=(i+by)%grille2.getNbLignes();
					}
						
					grille2.getCase(xy, position).setEtat("#");
					grille2.getCase(xy, position).setValeur(1);
				}
			}
			//remettre toutes les valeurs des cases à 0
			for (Case c:grille2.getCases()) {
				c.setValeur(0);
			}
		}

//		System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
//		grille2.affichageEtat();
		return grille2;
	}

	private Grille rec(Grille grille, Grille grille2,  String regle) {
		int a = Integer.valueOf( StringUtils.split(StringUtils.split(regle," ")[1], "x")[0] );
		int b = Integer.valueOf( StringUtils.split(StringUtils.split(regle," ")[1], "x")[1] );
		for (int i=0;i<a;i++) {
			for (int j=0;j<b;j++) {
				grille2.getCase(i, j).setEtat("#");
			}
		}
//		System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------");
//		grille2.affichageEtat();
		return grille2;
	}

	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day08.txt";
		return Outil.importationString(filename);
	}

	private Grille initialiserGrille(int x,int y) {
		Grille grille =new Grille(x,y);
		return grille;
	}





}


