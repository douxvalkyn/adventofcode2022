package advent2016.day21;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day21 {

	private static Logger logger = LoggerFactory.getLogger(Day21.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day21.class.getSimpleName() + "]");
		Day21 Day = new Day21();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	public void run1() {
		List<String> input = lectureInput();
		String password="abcdefgh";
		
		
		for (String ligne: input) {
			if (StringUtils.contains(ligne, "swap position")) {
				 password = swapPosition(ligne, password);
			}
			if (StringUtils.contains(ligne, "swap letter")) {
				password = swapLetter(ligne, password);
			}
			if (StringUtils.contains(ligne, "reverse positions")) {
				password = reversePositions(ligne, password);
			}
			if (StringUtils.contains(ligne, "rotate left")) {
				password = rotateLeft(ligne, password);
			}
			if (StringUtils.contains(ligne, "rotate right")) {
				password = rotateRight(ligne, password);
			}
			if (StringUtils.contains(ligne, "move position")) {
				password = movePosition(ligne, password);
			}
			if (StringUtils.contains(ligne, "rotate based")) {
				password = rotateBased(ligne, password);
			}
		}
		System.out.println("run1: "+password);
	}

	private String rotateBased(String ligne, String password) {
		String x = StringUtils.split(ligne, ' ')[6];
		int posX=StringUtils.indexOf(password, x);
		int steps=0;
		if (posX<4) {
			steps=1+posX;
		}else {
			steps=1+posX+1;
		}
		steps=steps%password.length();
		
		String deb = StringUtils.right(password, steps);
		String fin = StringUtils.left(password, password.length()-steps);
		
		return deb+fin;
	}




	private String movePosition(String ligne, String password) {
		int x = Integer.valueOf( StringUtils.split(ligne, ' ')[2]);
		int y = Integer.valueOf( StringUtils.split(ligne, ' ')[5]);
		char charX = password.charAt(x);
		String password2 = password.substring(0,x)+  password.substring(x+1);
		String password3 = password2.substring(0,y)+ charX + password2.substring(y);
		
		
		return password3;
	}




	private String rotateRight(String ligne, String password) {
		int steps = Integer.valueOf( StringUtils.split(ligne, ' ')[2]);
		String deb = StringUtils.right(password, steps);
		String fin = StringUtils.left(password, password.length()-steps);
		return deb+fin;
	}



	private String rotateLeft(String ligne, String password) {
		int steps = Integer.valueOf( StringUtils.split(ligne, ' ')[2]);
		String fin = StringUtils.left(password, steps);
		String deb = StringUtils.right(password, password.length()-steps);
		return deb+fin;
	}




	private String reversePositions(String ligne, String password) {
		int x = Integer.valueOf( StringUtils.split(ligne, ' ')[2]);
		int y = Integer.valueOf( StringUtils.split(ligne, ' ')[4]);
		char charX = password.charAt(x);
		char charY = password.charAt(y);
		String reversed= password.substring(x, y+1);
		reversed=StringUtils.reverse(reversed);
		String password2 = password.substring(0,x)+  reversed   +password.substring(y+1);
		return password2;
	}




	private String swapLetter(String ligne, String password) {
		String x =  StringUtils.split(ligne, ' ')[2];
		String y =  StringUtils.split(ligne, ' ')[5];
		int posX = StringUtils.indexOf(password, x);
		int posY = StringUtils.indexOf(password, y);
		String password2 = password.substring(0,posX)+y+password.substring(posX+1);
		String password3 = password2.substring(0,posY)+x+password2.substring(posY+1);
		return password3;
	}
	
	private String swapPosition(String ligne, String password) {
		int x = Integer.valueOf( StringUtils.split(ligne, ' ')[2]);
		int y = Integer.valueOf( StringUtils.split(ligne, ' ')[5]);
		char charX = password.charAt(x);
		char charY = password.charAt(y);
		String password2 = password.substring(0,x)+charY+password.substring(x+1);
		String password3 = password2.substring(0,y)+charX+password2.substring(y+1);
		return password3;
	}
	
	



	public void run2() {
		List<String>historique= new ArrayList<String>();
		boolean go=true;
		while (go) {
		String password=generateRandomPassword2();
		if (!historique.contains(password)) {
			historique.add(password);
			List<String> input = lectureInput();
			String passwordSave = password;
			
			for (String ligne: input) {
				if (StringUtils.contains(ligne, "swap position")) {
					 password = swapPosition(ligne, password);
				}
				if (StringUtils.contains(ligne, "swap letter")) {
					password = swapLetter(ligne, password);
				}
				if (StringUtils.contains(ligne, "reverse positions")) {
					password = reversePositions(ligne, password);
				}
				if (StringUtils.contains(ligne, "rotate left")) {
					password = rotateLeft(ligne, password);
				}
				if (StringUtils.contains(ligne, "rotate right")) {
					password = rotateRight(ligne, password);
				}
				if (StringUtils.contains(ligne, "move position")) {
					password = movePosition(ligne, password);
				}
				if (StringUtils.contains(ligne, "rotate based")) {
					password = rotateBased(ligne, password);
				}
			}
			if (password.equals("fbgdceah")) {
				System.err.println("run2: "+passwordSave);
				go=false;
			}
		}

		
	}
	}

	
	public static boolean checkNoDoublons(CharSequence g) {
	    for (int i = 0; i < g.length(); i++) {
	        for (int j = i + 1; j < g.length(); j++) {
	            if (g.charAt(i) == g.charAt(j)) {
	                return false;
	            }
	        }
	    }
	    return true;
	}
	
	
	private String generateRandomPassword() {
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 104; // letter 'h'
	    int targetStringLength = 8;
	    Random random = new Random();

	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

	    return generatedString;
	}

	
	private String generateRandomPassword2() {
		List<Character> sequence = new ArrayList<>();
		for (char c = 'a' ; c <= 'h' ; c++) {
			sequence.add(c);
		}
		Collections.shuffle(sequence);
		String resultat="";
		for (Character c : sequence) {
			resultat=resultat+c;
		}
		return resultat;
	}


	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day20.txt";
		return Outil.importationString(filename);
	}
	
}