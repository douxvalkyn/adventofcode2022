package advent2016.day22;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Grille;
import advent2016.outils.Outil;

public class Day22 {

	private static Logger logger = LoggerFactory.getLogger(Day22.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day22.class.getSimpleName() + "]");
		Day22 Day = new Day22();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		List<Noeud> noeuds = lectureInput();
		int xMax = recherchexMax(noeuds);
		int yMax = rechercheyMax(noeuds);
		Grille grille = new Grille(xMax+1, yMax+1);
		creationGrille(grille, noeuds);
		ajoutCaseG(grille,noeuds);
		//grille.affichageEtat();
		System.out.println("on affiche la grille et on compte les deplacements: 81+1+36+5=261");
		
	}

	
	private void ajoutCaseG(Grille grille, List<Noeud> noeuds) {
		int xMax=0;
		for (Noeud noeud:noeuds) {
			if (noeud.getY()==0) {
				if (noeud.getX()>xMax) {
					xMax=noeud.getX();
				}
			}
		}
		grille.getCase(xMax, 0).setEtat("G");
	}
	
	private int recherchexMax(List<Noeud> noeuds) {
		int xMax=0;
		for (Noeud noeud:noeuds) {
			if (noeud.getY()==0) {
				if (noeud.getX()>xMax) {
					xMax=noeud.getX();
				}
			}
		}
		return xMax;
	}
	
	private int rechercheyMax(List<Noeud> noeuds) {
		int yMax=0;
		for (Noeud noeud:noeuds) {
			if (noeud.getX()==0) {
				if (noeud.getY()>yMax) {
					yMax=noeud.getY();
				}
			}
		}
		return yMax;
	}

	
	private void creationGrille(Grille grille, List<Noeud> noeuds) {
		for (Noeud noeudA: noeuds) {
			if (noeudA.getUsed()!=0) {
				for (Noeud noeudB: noeuds) {
					if (noeudA!=noeudB) {
						if (grille.getCase(noeudA.getX(), noeudA.getY()).getEtat()=="_") {
						
						if(  ( noeudA.getX()==noeudB.getX()	&	Math.abs(noeudA.getY()-noeudB.getY())==1 ) |
								( noeudA.getY()==noeudB.getY()	&	Math.abs(noeudA.getX()-noeudB.getX())==1 ) ) {

							if (noeudA.getUsed()<noeudB.getSize()) {
								grille.getCase(noeudA.getX(), noeudA.getY()).setEtat(".");

							}
							
							if (noeudA.getUsed()>100) {
								grille.getCase(noeudA.getX(), noeudA.getY()).setEtat("#");

							}
						}
					}
				}
				}
			}else {
				grille.getCase(noeudA.getX(), noeudA.getY()).setEtat("-");
			}
		}
	}

	public void run1() {
		List<Noeud> noeuds = lectureInput();
		noeuds=rechercheNoeudsViables(noeuds);
		System.out.println(noeuds.size());
		
	}

	
	private List<Noeud> rechercheNoeudsViables(List<Noeud> noeuds) {
		List<Noeud>noeuds2 = new ArrayList<Noeud>();
		for (Noeud noeudA: noeuds) {
			if (noeudA.getUsed()!=0) {
				for (Noeud noeudB: noeuds) {
					if (noeudA!=noeudB) {
//					if(  ( noeudA.getX()==noeudB.getX()	&	Math.abs(noeudA.getY()-noeudB.getY())==1 ) |
//							( noeudA.getY()==noeudB.getY()	&	Math.abs(noeudA.getX()-noeudB.getX())==1 ) ) {

							if (noeudA.getUsed()<noeudB.getAvail()) {
								noeuds2.add(noeudA);

							}
						}
					}
//				}
			}
		}
		
		
		
		return noeuds2;
	}

	private List<Noeud> lectureInput() {
		String filename = "src/main/resources/advent2016/Day22.txt";

		List<String> lignes = Outil.importationString(filename);
		List<Noeud> noeuds = new ArrayList<Noeud>();
		for (String ligne: lignes) {
			if (ligne.charAt(0)=='/') {
				String a = StringUtils.split(ligne)[0];
				String size = StringUtils.split(ligne)[1];
				String used = StringUtils.split(ligne)[2];
				String avail = StringUtils.split(ligne)[3];
				String usedPct = StringUtils.split(ligne)[4];
				Integer size_ = Integer.valueOf(size.substring(0, size.length() - 1));
				Integer used_ = Integer.valueOf( used.substring(0, used.length() - 1));
				Integer avail_ = Integer.valueOf( avail.substring(0, avail.length() - 1)  );
				Integer usedPct_ = Integer.valueOf( usedPct.substring(0, usedPct.length() - 1));
				int x = Integer.valueOf( StringUtils.substring(StringUtils.split(a,'-')[1],1,StringUtils.split(a,'-')[1].length() ));
				int y = Integer.valueOf( StringUtils.substring(StringUtils.split(a,'-')[2],1,StringUtils.split(a,'-')[2].length() ));
				Noeud noeud = new Noeud(x, y, size_, used_, avail_, usedPct_);
				noeuds.add(noeud);
			}
		}
		
		return noeuds;
	}
	
}