package advent2016.day22;

public class Noeud {

	private int x;
	private int y;
	private int size;
	private int used;
	private int avail;
	private int usedPct;
	
	
	public Noeud(int x, int y, int size, int used, int avail, int usedPct) {
		super();
		this.x = x;
		this.y = y;
		this.size = size;
		this.used = used;
		this.avail = avail;
		this.usedPct = usedPct;
	}


	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public int getUsed() {
		return used;
	}


	public void setUsed(int used) {
		this.used = used;
	}


	public int getAvail() {
		return avail;
	}


	public void setAvail(int avail) {
		this.avail = avail;
	}


	public int getUsedPct() {
		return usedPct;
	}


	public void setUsedPct(int usedPct) {
		this.usedPct = usedPct;
	}


	@Override
	public String toString() {
		return "Noeud [x=" + x + ", y=" + y + ", size=" + size + ", used=" + used + ", avail=" + avail + ", usedPct="
				+ usedPct + "]";
	}
	
	
	
}
