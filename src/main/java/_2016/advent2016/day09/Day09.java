package advent2016.day09;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;



public class Day09 {

	private static Logger logger = LoggerFactory.getLogger(Day09.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day09.class.getSimpleName()+"]");
		Day09 day = new Day09();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}
	

	public void run1() {
		
		List<String> fichier = lectureInput();
		//String resultat = traitement("X(8x2)(3x3)ABCY");
		String resultat = fichier.get(0);
		
		resultat = traitement(resultat);
		System.out.println("etape 1" + ", longueur: "+resultat.length());
	}

	
	
	
	public void run2() {
		String resultat = lectureInput().get(0);
		//String resultat = "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN";
		
		//initialiser la liste de toutes les lettres du fichier (non marker)
		List<Lettre> lettres= new ArrayList<>();
		for ( int i=0;i<resultat.length();i++) {
			if (StringUtils.isAllUpperCase(""+resultat.charAt(i))) { //lettre
				Lettre lettre = new Lettre(resultat.charAt(i),i);
				lettres.add(lettre);
			}
		}
		//System.out.println(lettres);
		
		//relire le fichier en associant les multiplicateurs des markers à chaque lettre
		for ( int i=0;i<resultat.length();i++) {
			if (resultat.charAt(i)=='(') { //debut de marker
				int j=i+1;
				String marker="";
				while (resultat.charAt(j)!=')') {
					marker = marker+ resultat.charAt(j);
					j++;
				}
				Integer param1 = Integer.valueOf( StringUtils.split(marker, "x")[0] );
				Integer param2 = Integer.valueOf( StringUtils.split(marker, "x")[1] );
				
				//retrouver les lettres concernées par ce marker: on avance de param1 caracteres et on cherche les lettres
				for (int k=i+marker.length()+2;k<i+marker.length()+2+param1;k++) {
					if (StringUtils.isAllUpperCase(""+resultat.charAt(k))) { 
						Lettre lettre = trouveLettreSelonPosition(lettres,k);
						lettre.addMultiplicateurs(param2);
					}
				}
			}
		}
		
		//calculer le nb total de caracteres à ecrire à l'aide des multiplicateurs de chaque lettre
		long score=0;
		for ( Lettre lettre: lettres) {
			int produit=1;
			for (Integer multiplicateur : lettre.getMultiplicateurs()) {
				produit=produit*multiplicateur;
			}
			score=score+produit;
		}
		System.out.println("nb de caracteres à décompresser: "+score);
	}

	
	private Lettre trouveLettreSelonPosition(List<Lettre> lettres, int position) {
		for (int i=0;i<lettres.size();i++) {
			if (lettres.get(i).getPosition() == position)
				return lettres.get(i);
		}
		return null;
	}
	
	
	private void compteNbMarkers(String resultat) {
		int cpt=0;
		for (int i=0;i<resultat.length();i++) {
			if (resultat.charAt(i)=='(') {
				cpt++;
			}
		}
		System.err.println(cpt);
		
	}

	private String traitement(String string) {
		String resultat="";
		for(int i=0;i<string.length();i++) {
			if (StringUtils.isAllUpperCase(""+string.charAt(i))) { // not a marker
				resultat = resultat + string.charAt(i);
			}else { //marker
				int j=i+1;
				String marker="";
				while (string.charAt(j)!=')') {
					marker = marker+ string.charAt(j);
					j++;
				}
				Integer param1 = Integer.valueOf( StringUtils.split(marker, "x")[0] );
				Integer param2 = Integer.valueOf( StringUtils.split(marker, "x")[1] );
				
				//recuperer les param1 elements et les reproduire param2 fois
				String pattern =  StringUtils.substring(string, i+marker.length()+2, i+marker.length()+2+param1);
				for (int k=0;k<param2;k++) {
					resultat = resultat + pattern;
				}
				i=i+marker.length()+1+param1;
			}
		}
		return resultat;
	}

	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/day09.txt";
		return Outil.importationString(filename);
	}



}


