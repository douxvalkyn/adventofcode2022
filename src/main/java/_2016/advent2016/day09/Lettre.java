package advent2016.day09;

import java.util.ArrayList;
import java.util.List;

public class Lettre {

	char nom;
	int position;
	List<Integer> multiplicateurs;
	
	
	public Lettre(char nom, int position) {
		super();
		this.position = position;
		this.nom = nom;
		multiplicateurs = new ArrayList<>();
		multiplicateurs.add(1);
	}


	public int getPosition() {
		return position;
	}

	

	public char getNom() {
		return nom;
	}


	public void setNom(char nom) {
		this.nom = nom;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public List<Integer> getMultiplicateurs() {
		return multiplicateurs;
	}


	public void setMultiplicateurs(List<Integer> multiplicateurs) {
		this.multiplicateurs = multiplicateurs;
	}
	
	public void addMultiplicateurs( int multiplicateur) {
		this.multiplicateurs.add(multiplicateur);
	}


	@Override
	public String toString() {
		return ""+nom;
	}
	
	
	
	
	
}
