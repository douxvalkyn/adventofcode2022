package advent2016.day01;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Case;
import advent2016.outils.Outil;

public class Day01 {
	
	 private static Logger logger = LoggerFactory.getLogger(Day01.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day01.class.getSimpleName()+"]");
		Day01 Day = new Day01();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2016/Day01.txt";
		List<String> fichier = Outil.importationString(filename);
		String[] deplacements = StringUtils.split(fichier.get(0),",");
		int orientation=0; //0 degrés
		int x=0;
		int y=0;
		
		for(String deplacement:deplacements) {
			deplacement=StringUtils.trim(deplacement);
			String changementOrientation = StringUtils.substring(deplacement, 0, 1);
			switch(changementOrientation){
		       case "R": 
		    	   orientation=orientation+90;
		           break;
		       case "L":
		    	   orientation=orientation-90;
		           break;
		       default:
		           System.out.println("Choix incorrect");
		           break;
		   }
			if (orientation>=360) {
				orientation=orientation-360;
			}
			if (orientation<0) {
				orientation=orientation+360;
			}
		
		int nbDeplacements = Integer.parseInt(StringUtils.substring(deplacement, 1));

		switch(orientation){
	       case 0: 
	    	   y=y+nbDeplacements;
	           break;
	       case 90:
	    	   x=x+nbDeplacements;
	           break;
	       case 180: 
	    	   y=y-nbDeplacements;
	           break;
	       case 270: 
	    	   x=x-nbDeplacements;
	           break;
	       default:
	           System.out.println("Choix incorrect2");
	           break;
	   }
		
		}
		
		//System.out.println("x: "+x);
		//-System.out.println("y: "+y);
		int dist=Math.abs(x)+Math.abs(y);
		System.out.println("distance1: "+ dist);

	}



	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2016/Day01.txt";
		List<String> fichier = Outil.importationString(filename);
		String[] deplacements = StringUtils.split(fichier.get(0),",");
		int orientation=0; //0 degrés
		List<Case> cases = new ArrayList<Case>();
		cases.add(new Case(0,0));
		int x=0;
		int y=0;
		
		outerloop:
		for(String deplacement:deplacements) {
			deplacement=StringUtils.trim(deplacement);
			String changementOrientation = StringUtils.substring(deplacement, 0, 1);
			switch(changementOrientation){
		       case "R": 
		    	   orientation=orientation+90;
		           break;
		       case "L":
		    	   orientation=orientation-90;
		           break;
		       default:
		           System.out.println("Choix incorrect");
		           break;
		   }
			if (orientation>=360) {
				orientation=orientation-360;
			}
			if (orientation<0) {
				orientation=orientation+360;
			}
		
		int nbDeplacements = Integer.parseInt(StringUtils.substring(deplacement, 1));
		int stop=0;
		switch(orientation){
	       case 0: 
	    	   stop=majCases(cases,x,x,y,y+nbDeplacements);
	    	   y=y+nbDeplacements;
	           break;
	       case 90:
	    	   stop=majCases(cases,x,x+nbDeplacements,y,y);
	    	   x=x+nbDeplacements;
	           break;
	       case 180: 
	    	   stop=majCases(cases,x,x,y,y-nbDeplacements);
	    	   y=y-nbDeplacements;
	           break;
	       case 270: 
	    	   stop=majCases(cases,x,x-nbDeplacements,y,y);
	    	   x=x-nbDeplacements;
	           break;
	       default:
	           System.out.println("Choix incorrect2");
	           break;
	   }
		
		if (stop!=0) {
			break outerloop;
		}
		
		}
	}



	private int majCases(List<Case> cases, int x1, int x2, int y1, int y2) {
		Case caseDejaVisitee=null;
		int nbMaj=0;
		int distance=0;
		
		if (x2 > x1) {
			nbMaj=Math.abs(x2-x1);
			for (int i=1;i<=nbMaj;i++) {
				Case cell= new Case (x1+i,y1);
				if (casesContientCell(cases, cell)) {
					caseDejaVisitee=cell;
				}
				cases.add(cell);
			}
		}
		if (x1 > x2) {
			nbMaj=Math.abs(x2-x1);
			for (int i=1;i<=nbMaj;i++) {
				Case cell= new Case (x1-i,y1);
				if (casesContientCell(cases, cell)) {
					caseDejaVisitee=cell;
				}
				cases.add(cell);
			}
		}
		if (y2 > y1) {
			nbMaj=Math.abs(y2-y1);
			for (int i=1;i<=nbMaj;i++) {
				Case cell= new Case (x1,y1+i);
				if (casesContientCell(cases, cell)) {
					caseDejaVisitee=cell;
				}
				cases.add(cell);
			}
		}
		if (y1 > y2) {
			nbMaj=Math.abs(y2-y1);
			for (int i=1;i<=nbMaj;i++) {
				Case cell= new Case (x1,y1-i);
				if (casesContientCell(cases, cell)) {
					caseDejaVisitee=cell;
				}
				cases.add(cell);
			}
			
		}
		
		if (caseDejaVisitee != null) {
			distance = Math.abs(caseDejaVisitee.getX()) + Math.abs(caseDejaVisitee.getY());
			System.out.println("distance2: " + distance);
		}
		
		return distance;
	}



	private boolean casesContientCell(List<Case> cases, Case cell) {
		for (Case c:cases) {
			if (c.getX()==cell.getX() && c.getY()==cell.getY()) {
				return true;
			}
		}
		return false;
	}


	
}
