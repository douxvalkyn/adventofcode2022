package advent2016.day14;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day14 {

	private static Logger logger = LoggerFactory.getLogger(Day14.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day14.class.getSimpleName() + "]");
		Day14 Day = new Day14();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		//String input="abc";
		String input="zpqevtbw";
		int i = 0;
		Map<Integer, String>map=new HashMap<Integer, String>();
		Map<String, Integer>map2=new HashMap<String, Integer>();
		String md5Hex="";
		while (map.size()<64) {
			String code = input + i;
			md5Hex= DigestUtils.md5Hex(code);
			//stretched hash
			for (int k=0;k<2016;k++) {
				md5Hex= DigestUtils.md5Hex(md5Hex);
			}
			//rechercher un motif triple
			String motif = contientTriple(md5Hex);
			if (motif != null) {
				//System.out.println(motif + ", index:" + i);
				String res="";
				//rechercher le motif 5 fois dans les 1000 prochains
				if (map2.containsKey(motif) && (map2.get(motif)-i)<1000 && (map2.get(motif)-i)>0) {
					//System.err.println("("+map.size()+") "+"i:"+i+", "+motif +", deja ok indice: "+map2.get(motif));
					map.put(i, md5Hex);
				}else {
				
				for (int j=i+1;j<i+1001;j++) {
					code = input+j;
					String md5Hex2 = DigestUtils.md5Hex(code);
					//stretched hash
					for (int k=0;k<2016;k++) {
						md5Hex2= DigestUtils.md5Hex(md5Hex2);
					}
					res=contientCinqFoisMotif(md5Hex2, motif);
					if (res!=null) {
						//System.err.println("("+map.size()+") "+"i:"+i+", "+res +", indice: "+ j);
						map.put(i, md5Hex);
						map2.put(motif,  j);
					}
				}
				}
			}
			i++;
		}
		System.out.println((i-1)+" "+md5Hex ); //last
	}

	public void run1() {
		//String input="abc";
		String input="zpqevtbw";
		int i = 0;
		Map<Integer, String>map=new HashMap<Integer, String>();
		String md5Hex="";
		while (map.size()<64) {
			String code = input + i;
			md5Hex= DigestUtils.md5Hex(code);
			//rechercher un motif triple
			String motif = contientTriple(md5Hex);
			if (motif != null) {
				//System.out.println(motif + ", index:" + i);
				String res="";
				//rechercher le motif 5 fois dans les 1000 prochains
				for (int j=i+1;j<i+1001;j++) {
					code = input+j;
					String md5Hex2 = DigestUtils.md5Hex(code);
					res=contientCinqFoisMotif(md5Hex2, motif);
					if (res!=null) {
						//System.err.println(res +", indice: "+ j);
						map.put(i, md5Hex);
					}
				}
			}
			i++;
		}
		System.out.println((i-1)+" "+md5Hex ); //last
	}

	
	private String contientTriple(String md5Hex) {
		for (int i = 0; i < md5Hex.length() - 2; i++) {
			if (md5Hex.charAt(i) == md5Hex.charAt(i + 1) && md5Hex.charAt(i + 1) == md5Hex.charAt(i + 2)) {
				return "" + md5Hex.charAt(i);
			}
		}

		return null;
	}
	
	private String contientCinqFoisMotif(String md5Hex, String motif) {
		for (int i = 0; i < md5Hex.length() - 4; i++) {
			if ((""+md5Hex.charAt(i)).equals(motif) && (""+md5Hex.charAt(i+1)).equals(motif) && (""+md5Hex.charAt(i+2)).equals(motif)
					&& (""+md5Hex.charAt(i+3)).equals(motif) && (""+md5Hex.charAt(i+4)).equals(motif)) {
				return motif;
			}
		}

		return null;
	}

}
