package advent2016.day16;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day16 {

	private static Logger logger = LoggerFactory.getLogger(Day16.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day16.class.getSimpleName() + "]");
		Day16 Day = new Day16();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}


	
	public void run1() {
		
		String a="11100010111110100";
		int longueur = 272;
		
		
		while (a.length() < longueur ) {
		a = fillDisk(a);
		}
		
		if (a.length()>longueur) {
			a=StringUtils.substring(a,0, longueur);
		}

		//System.out.println(a.length() + ":"+a);
		a  = generateChecksum(a);
		System.out.println("checksum run1: "+a);
		
	}
	
	public void run2() {

		String a="11100010111110100";
		int longueur = 35651584;


		while (a.length() < longueur ) {
			//System.out.println(a.length());
			a = fillDisk3(a);
		}

		if (a.length()>longueur) {
			a=StringUtils.substring(a,0, longueur);
		}

		//System.out.println(">"+a.length() + ":"+a);
		a  = driveChecksum(a);
		System.out.println("checksum run2: "+a);

	}


	private String generateChecksum(String string) {
		
		
		StringBuilder sb = new StringBuilder();
		
		String checksum="";
			for (int i=0;i<string.length()-1;i=i+2) {

				if (string.charAt(i)==string.charAt(i+1)) {
					checksum=checksum+"1";
				}else {
					checksum=checksum+"0";
				}
			}
			sb.append(checksum);

			checksum = sb.toString();
			if(checksum.length() % 2 == 0) {
				checksum = generateChecksum(checksum);
			}
			
		return sb.toString();
	}


	public String fillDisk(String a) {
		String b=a;
		b=StringUtils.reverse(b);
		b=StringUtils.replaceEach(b, new String[]{"0", "1"}, new String[]{"1", "0"});
//		b=StringUtils.replace(b, "1", "y");
//		b=StringUtils.replace(b, "x", "1");
//		b=StringUtils.replace(b, "y", "0");
		String res=a+"0"+b;

		return res;
	}
	
	public String fillDisk2(String a) {
		String b=a;
		b=StringUtils.reverse(b);
		replaceAll(b, "0", "x");
		replaceAll(b, "1", "y");
		replaceAll(b, "x", "1");
		replaceAll(b, "y", "0");
		String res=a+"0"+b;

		return res;
	}
	
	public String fillDisk3(String a) {
		String b = new StringBuffer(a).reverse().toString();
		 
	    b = b.replace('0', 'x');
	    b = b.replace('1', '0');
	    b = b.replace('x', '1');
	    

		return a+"0"+b;
	}
	

    
	
	public static void replaceAll(String from,String to, String matcher) {
		Pattern p = Pattern.compile(from);
		Matcher m = p.matcher(matcher);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, to);
		}
		m.appendTail(sb);
	//	System.out.println(sb.toString());
	}

	
	private static String driveChecksum(String input) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < input.length(); i+=2) {
			sb.append(input.charAt(i) == input.charAt(i + 1) ? '1' : '0');
		}
		
		String checksum = sb.toString();
		
		if(checksum.length() % 2 == 0) {
			checksum = driveChecksum(checksum);
		}
		
		return checksum;

	}
	
}
