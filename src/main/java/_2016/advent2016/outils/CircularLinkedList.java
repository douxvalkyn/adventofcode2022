package advent2016.outils;

public class CircularLinkedList {
    private Node head = null;
    private Node tail = null;

    
    public int getNode(int indice) {
    	Node currentNode = head;
    	if (indice==0) {
    		return head.value;
    	}else {
    		for (int i=0;i<indice;i++) {
    			currentNode = currentNode.nextNode;
    		}
    		return currentNode.value;
    	}
    }
    
    
    
    public void addNode(int value) {
        Node newNode = new Node(value);

        if (head == null) {
            head = newNode;
        } else {
            tail.nextNode = newNode;
        }

        tail = newNode;
        tail.nextNode = head;
    }
    
    public boolean containsNode(int searchValue) {
        Node currentNode = head;

        if (head == null) {
            return false;
        } else {
            do {
                if (currentNode.value == searchValue) {
                    return true;
                }
                currentNode = currentNode.nextNode;
            } while (currentNode != head);
            return false;
        }
    }
    
    public void deleteNode(int valueToDelete) {
        Node currentNode = head;
        if (head == null) { // the list is empty
            return;
        }
        do {
            Node nextNode = currentNode.nextNode;
            if (nextNode.value == valueToDelete) {
                if (tail == head) { // the list has only one single element
                    head = null;
                    tail = null;
                } else {
                    currentNode.nextNode = nextNode.nextNode;
                    if (head == nextNode) { //we're deleting the head
                        head = head.nextNode;
                    }
                    if (tail == nextNode) { //we're deleting the tail
                        tail = currentNode;
                    }
                }
                break;
            }
            currentNode = nextNode;
        } while (currentNode != head);
    }
   
    
    
    public Node getRealNode(int index) {
    	Node currentNode = head;
        if (head != null) {
            do {
                currentNode = currentNode.nextNode;
                if (currentNode.value==index) {
                	return currentNode;
                }
            } while (currentNode != head);
        }
		return null;
    }

    public int size() {
        Node currentNode = head;
        int cpt=0;

        if (head != null) {
            do {
            	 cpt++;
                currentNode = currentNode.nextNode;
            } while (currentNode != head);
        }
        return cpt;
    }
    
    
   public Node getNodeEnFace(Node node) {
	  int taille= this.size();
	  if (taille%2==0) {
		  for (int i=0;i<(taille/2);i++) {
			  node=node.getNextNode();
		  }
	  }
	  if (taille%2!=0) {
		  for (int i=0;i<(taille/2);i++) {
			  node=node.getNextNode();
		  }
	  }
	   
	   
	   
	return node;
   }

	public Node getHead() {
		return head;
	}



	public void setHead(Node head) {
		this.head = head;
	}



	public Node getTail() {
		return tail;
	}



	public void setTail(Node tail) {
		this.tail = tail;
	}
}