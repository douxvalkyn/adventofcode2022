package advent2016.outils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class Outil {
	
	
	// pour cloner (deepclone) un objet:
	// > creer un constructeur de copie
	// exemple: voir la classe Amphipode dans le Day23
	
	
	/** importer des Integer */
	public static  List<Integer> importationInteger(String FILENAME) throws IOException {
		return Files.lines(Paths.get(FILENAME)).map(i -> Integer.valueOf(i))
				.collect(Collectors.toList());
	}

	/** importer des String */
	public static  List<String> importationString(String FILENAME) {
		try {
			return Files.lines(Paths.get(FILENAME)).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** importer une liste d'entiers séparés par des virgules */
	public static  List<Integer> importationIntegerSeparateurVirgule(String FILENAME) throws IOException {
		List<String> maListeString = null;
		try {
			maListeString = Files.lines(Paths.get(FILENAME)).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] nombres = StringUtils.split(maListeString.get(0),",");
		List<Integer> maListeInt = new ArrayList<Integer>();
		for(int index = 0 ; index<nombres.length ; index++) {
			maListeInt.add(Integer.parseInt(nombres[index]));
		}
		return maListeInt;
	}

	
	
	
	
	/** calculer le maximum d'une liste d'entiers */
	public static Integer max(List<Integer> listOfIntegers) {
		int max = 0;
		try {
			max = listOfIntegers.stream().mapToInt(v -> v).max().orElseThrow(Exception::new);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return max;
	}
	
	/** calculer le minimum d'une liste d'entiers */
	public static Integer min(List<Integer> listOfIntegers) {
		int min = 0;
		try {
			min = listOfIntegers.stream().mapToInt(v -> v).min().orElseThrow(Exception::new);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return min;
	}
	
	/** calculer la moyenne d'une liste d'entiers */
	public static Double moy(List<Integer> listOfIntegers) {
		double moy = 0;
		try {
			   moy =  listOfIntegers.stream()
					.mapToDouble(d -> d)
					.average()
					.orElse(0.0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return moy;
	}

	/** calculer la somme d'une liste d'entiers */
	public static Integer sum(List<Integer> listOfIntegers) {
		int somme = 0;
		try {
			somme = listOfIntegers.stream().mapToInt(v -> v).sum();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return somme;
	}
	
	
	/** renvoyer la liste des caracteres d'une chaine de caracteres ainsi que le nb d'occurrences 
	 * @return */
	public static HashMap<String, Integer> characterCount(String inputString)  {
        // Creating a HashMap containing char
        // as a key and occurrences as  a value
        HashMap<String, Integer> charCountMap = new HashMap<String, Integer>();
 
        // Converting given string to char array
        char[] strArray = inputString.toCharArray();
 
        // checking each char of strArray
        for (char c : strArray) {
        	
            if (charCountMap.containsKey(""+c)) {
 
                // If char is present in charCountMap,
                // incrementing it's count by 1
                charCountMap.put(""+c, charCountMap.get(""+c) + 1);
            }
            else {
                // If char is not present in charCountMap,
                // putting this char to charCountMap with 1 as it's value
                charCountMap.put(""+c, 1);
            }
        }
        return charCountMap;
    }

	/** calculer le minimum d'une liste d'entiers */
	public static Long minLong(List<Long> listOfLong) {
		Long min = 0L;
		try {
			min = listOfLong.stream().mapToLong(v -> v).min().orElseThrow(Exception::new);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return min;
	}
 
	

	
}
