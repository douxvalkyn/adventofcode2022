package advent2016.outils;

import java.util.LinkedList;

public class NodeWeighted {
    // The int n and String name are just arbitrary attributes
    // we've chosen for our nodes these attributes can of course
    // be whatever you need
    int n;
    String name;
    private boolean visited;
    LinkedList<EdgeWeighted> edges;
    int x;
    int y;


    
    public NodeWeighted(int n, String name) {
        this.n = n;
        this.name = name;
        visited = false;
        edges = new LinkedList<EdgeWeighted>();   
    }



	@Override
	public String toString() {
		return "Node"+name;
	}

	boolean isVisited() {
        return visited;
    }

    void visit() {
        visited = true;
    }

    void unvisit() {
        visited = false;
    }



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
    
    

    
}