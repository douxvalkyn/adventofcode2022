package advent2016.day10;



import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;



public class Day10 {

	private static Logger logger = LoggerFactory.getLogger(Day10.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day10.class.getSimpleName()+"]");
		Day10 Day = new Day10();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}
	
	
	public void run2() {
		List<String> fichier = lectureInput();
		List<Bot> bots = initBots(fichier);
		bots = initOutput(fichier, bots);
		initValues(fichier,bots);
		initRules(fichier, bots);
		Bot bot = trouveBotAvecDeuxValues(bots);
		while (bot != null) {
		bots=exec(bots, bot);
		bot = trouveBotAvecDeuxValues(bots);
		}
		System.out.println("run2: "+bots);
		int score = calculeScore(bots);
		System.out.println("score: "+score);
	}
	
	private int calculeScore(List<Bot> bots) {
		int score=1;
		for (Bot bot: bots) {
			if (bot.isOutput() & (bot.getNumero()==0 |bot.getNumero()==1 | bot.getNumero()==2) ) {
				score=score*bot.getValues().get(0);
			}
		}
		return score;
	}


	public void run1() {
		List<String> fichier = lectureInput();
		List<Bot> bots = initBots(fichier);
		bots = initOutput(fichier, bots);
		initValues(fichier,bots);
		initRules(fichier, bots);
		System.out.println(bots);
		Bot bot = trouveBotAvecDeuxValues(bots);
		while ((bot.getValues().get(0) != 61 |  bot.getValues().get(1) != 17)) {
		bots=exec(bots, bot);
		bot = trouveBotAvecDeuxValues(bots);
		}
		System.out.println(bots);
		System.out.println(bot);
	}

	
	
	private List<Bot> exec(List<Bot> bots, Bot bot) {
		Integer value1 = bot.getValues().get(0);
		Integer value2 = bot.getValues().get(1);
		Bot botLow = bot.getLowGoesTo();
		Bot botHigh = bot.getHighGoesTo();
		if (value1>value2) {
			botHigh.getValues().add(value1);
			botLow.getValues().add(value2);
		}else {
			botHigh.getValues().add(value2);
			botLow.getValues().add(value1);
		}
		bot.getValues().clear();
		return bots;
	}
	
	
	private void initRules(List<String> fichier, List<Bot> bots) {
		for (int i=0;i<fichier.size();i++) {
			String ligne = fichier.get(i);
			if (StringUtils.split(ligne," ")[0].equals("bot")) {
				Integer numBot = Integer.valueOf( StringUtils.split(ligne," ")[1] );
				Integer lowNum = Integer.valueOf( StringUtils.split(ligne," ")[6] );
				String lowBotOuOutput = StringUtils.split(ligne," ")[5] ;
				Integer highNum = Integer.valueOf( StringUtils.split(ligne," ")[11] );
				String highBotOuOutput = StringUtils.split(ligne," ")[10];
				for (Bot b:bots) {
					if (b.getNumero()==numBot && !b.isOutput() ) {
						Bot botLow = trouveBotSelonNumero(bots, lowNum, lowBotOuOutput);
						b.setLowGoesTo(botLow);
						Bot botHigh = trouveBotSelonNumero(bots, highNum, highBotOuOutput);
						b.setHighGoesTo(botHigh);
					}
				}
			}
		}	
	}



	private Bot trouveBotAvecDeuxValues(List<Bot> bots) {
		for (Bot bot:bots) {
			if (!bot.isOutput() && bot.getValues().size()==2) {
				return bot;
			}
		}
		return null;
	}
	
	
	private Bot trouveBotSelonNumero(List<Bot> bots, int numero, String lowBotOuOutput) {
		if (lowBotOuOutput.equals("bot")) {
			for (Bot bot:bots) {
				if (!bot.isOutput() && bot.getNumero()==numero) {
					return bot;
				}
			}
		}
		if (lowBotOuOutput.equals("output")) {
			for (Bot bot:bots) {
				if (bot.isOutput() && bot.getNumero()==numero) {
					return bot;
				}
			}
		}
		return null;
	}


	private void initValues(List<String> fichier, List<Bot> bots) {
		for (int i=0;i<fichier.size();i++) {
			String ligne = fichier.get(i);
			if (StringUtils.split(ligne," ")[0].equals("value")) {
				Integer value = Integer.valueOf( StringUtils.split(ligne," ")[1] );
				Integer numBot = Integer.valueOf( StringUtils.split(ligne," ")[5] );
				for (Bot b:bots) {
					if (b.getNumero()==numBot && !b.isOutput()) {
						b.getValues().add(value);
					}
				}
			}
		}
	}


	private List<Bot> initOutput(List<String> fichier, List<Bot> bots) {
		for (int i=0;i<fichier.size();i++) {
			String ligne = fichier.get(i);
			if (StringUtils.split(ligne," ")[0].equals("bot")) {
				if (StringUtils.split(ligne," ")[10].equals("output") ) {
					int numeroOutput =Integer.valueOf( StringUtils.split(ligne," ")[11] );
					boolean original=true;
					for (Bot b:bots) {
						if (b.getNumero()==numeroOutput && b.isOutput()) {
							original=false;
						}
					}
					if (original) {
						Bot output = new Bot(numeroOutput);
						output.setOutput(true);
						bots.add(output);
					}
				}
				if (StringUtils.split(ligne," ")[5].equals("output") ) {
					int numeroOutput =Integer.valueOf( StringUtils.split(ligne," ")[6] );
					boolean original=true;
					for (Bot b:bots) {
						if (b.getNumero()==numeroOutput && b.isOutput()) {
							original=false;
						}
					}
					if (original) {
						Bot output = new Bot(numeroOutput);
						output.setOutput(true);
						bots.add(output);
					}
				}
			}
		}
		return bots;
	}


	private List<Bot> initBots(List<String> fichier) {
		List<Bot> bots = new ArrayList<>();
		for (int i=0;i<fichier.size();i++) {
			String ligne = fichier.get(i);
			if (StringUtils.split(ligne," ")[0].equals("bot")) {
				int numeroBot =Integer.valueOf( StringUtils.split(ligne," ")[1] );
				boolean original=true;
				for (Bot b:bots) {
					if (b.getNumero()==numeroBot && !b.isOutput()) {
						original=false;
					}
				}
				if (original) {
					Bot bot = new Bot(numeroBot);
					bot.setOutput(false);
					bots.add(bot);
				}
				
			}
		}
		return bots;
		
	}


	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/day10.txt";
		return Outil.importationString(filename);
	}


}


