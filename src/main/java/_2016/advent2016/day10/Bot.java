package advent2016.day10;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Bot {

	
	int numero;
	List<Integer> values;
	Bot lowGoesTo;
	Bot highGoesTo;
	boolean output;
	
	public Bot(int numero) {
		super();
		this.numero = numero;
		ArrayList<Integer>values=new ArrayList<>();
		this.values=values;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}



	public List<Integer> getValues() {
		return values;
	}

	public void setValues(List<Integer> values) {
		this.values = values;
	}

	public Bot getLowGoesTo() {
		return lowGoesTo;
	}

	public void setLowGoesTo(Bot lowGoesTo) {
		this.lowGoesTo = lowGoesTo;
	}

	public Bot getHighGoesTo() {
		return highGoesTo;
	}

	public void setHighGoesTo(Bot highGoesTo) {
		this.highGoesTo = highGoesTo;
	}

	public boolean isOutput() {
		return output;
	}

	public void setOutput(boolean output) {
		this.output = output;
	}

	@Override
	public String toString() {
		String BotOuOutput="Bot";
		if (this.output) {
			BotOuOutput="Output";
		}
		return BotOuOutput + numero +  " values=" + values ;

	}
	
	
	
}
