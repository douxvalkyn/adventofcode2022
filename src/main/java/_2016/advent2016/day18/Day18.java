package advent2016.day18;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day18 {

	private static Logger logger = LoggerFactory.getLogger(Day18.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day18.class.getSimpleName() + "]");
		Day18 Day = new Day18();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}


	public void run2() {
		String input="......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^..";
		int nbSafe = StringUtils.countMatches(input, '.');
		int largeur= input.length();
		String old=input;
		
		for (int j=0;j<399999;j++) {
			String next="";

			for (int i=0;i<largeur;i++) {
				char left;
				try {left = old.charAt(i-1);}catch (Exception e) {
					left='.';}
				char center;
				try {center = old.charAt(i);}catch (Exception e) {
					center='.';}
				char right;
				try {right = old.charAt(i+1);}catch (Exception e) {
					right='.';}

				char newChar='.';
				if (left=='^' && center=='^' && right!='^') {
					newChar='^';
				}
				if (left!='^' && center=='^' && right=='^') {
					newChar='^';
				}
				if (left=='^' && center!='^' && right!='^') {
					newChar='^';
				}
				if (left!='^' && center!='^' && right=='^') {
					newChar='^';
				}
				next=next+newChar;
				if (newChar=='.') {
					nbSafe++;
				}

			}
			old=next;
			//System.out.println(next);

		}
		System.out.println("run2: "+nbSafe);
	}
	
	
	public void run1() {
		String input="......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^..";
		int nbSafe = StringUtils.countMatches(input, '.');
		int largeur= input.length();
		String old=input;
		
		for (int j=0;j<39;j++) {
			String next="";

			for (int i=0;i<largeur;i++) {
				char left;
				try {left = old.charAt(i-1);}catch (Exception e) {
					left='.';}
				char center;
				try {center = old.charAt(i);}catch (Exception e) {
					center='.';}
				char right;
				try {right = old.charAt(i+1);}catch (Exception e) {
					right='.';}

				char newChar='.';
				if (left=='^' && center=='^' && right!='^') {
					newChar='^';
				}
				if (left!='^' && center=='^' && right=='^') {
					newChar='^';
				}
				if (left=='^' && center!='^' && right!='^') {
					newChar='^';
				}
				if (left!='^' && center!='^' && right=='^') {
					newChar='^';
				}
				next=next+newChar;
				if (newChar=='.') {
					nbSafe++;
				}

			}
			old=next;
			//System.out.println(next);

		}
		System.out.println("run1: "+nbSafe);
	}


	
}
