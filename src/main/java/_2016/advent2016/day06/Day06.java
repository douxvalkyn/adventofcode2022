package advent2016.day06;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;



public class Day06 {

	private static Logger logger = LoggerFactory.getLogger(Day06.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day06.class.getSimpleName()+"]");
		Day06 Day = new Day06();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}



	public void run1() throws IOException {
		
		//importation du fichier
		String filename = "src/main/resources/advent2016/Day06.txt";
		List<String> fichier = Outil.importationString(filename);
		System.out.print("run1: ");
		
		//init des colonnes
		List<List<String>> colonnes = new ArrayList<>();
		for (int c=0;c<fichier.get(0).length();c++){
			List<String> colonne = new ArrayList<>();
			colonnes.add(colonne);
		}
		
		//lecture de chaque ligne
		for (String ligne: fichier) {
			//lecture de colonne c
			for (int c=0;c<ligne.length();c++){
				colonnes.get(c).add(""+ligne.charAt(c));
			}
		}

		//determiner la lettre la plus presente dans chaque colonne
		for (int c=0;c<fichier.get(0).length();c++){
		 String colonneString= colonnes.get(c).toString();
		 colonneString=StringUtils.remove(colonneString, "[");
		 colonneString=StringUtils.remove(colonneString, "]");
		 colonneString=StringUtils.remove(colonneString, ",");
		 colonneString=StringUtils.remove(colonneString, " ");
		 HashMap<String, Integer> occurrences = Outil.characterCount(colonneString);
		 int maxValue = Collections.max(occurrences.values());
		 List<String> lettresImportantes = new ArrayList<>();
			for (Entry<String, Integer> entry : occurrences.entrySet()) {  
				if (entry.getValue()==maxValue) {
					lettresImportantes.add(entry.getKey());    
				}
			}
			System.out.print(lettresImportantes.get(0));
			
		 
		}
		System.out.println();
	}



public void run2() throws IOException {
		
		//importation du fichier
		String filename = "src/main/resources/advent2016/Day06.txt";
		List<String> fichier = Outil.importationString(filename);
		System.out.print("run2: ");
		
		//init des colonnes
		List<List<String>> colonnes = new ArrayList<>();
		for (int c=0;c<fichier.get(0).length();c++){
			List<String> colonne = new ArrayList<>();
			colonnes.add(colonne);
		}
		
		//lecture de chaque ligne
		for (String ligne: fichier) {
			//lecture de colonne c
			for (int c=0;c<ligne.length();c++){
				colonnes.get(c).add(""+ligne.charAt(c));
			}
		}

		//determiner la lettre la plus presente dans chaque colonne
		for (int c=0;c<fichier.get(0).length();c++){
		 String colonneString= colonnes.get(c).toString();
		 colonneString=StringUtils.remove(colonneString, "[");
		 colonneString=StringUtils.remove(colonneString, "]");
		 colonneString=StringUtils.remove(colonneString, ",");
		 colonneString=StringUtils.remove(colonneString, " ");
		 HashMap<String, Integer> occurrences = Outil.characterCount(colonneString);
		 int maxValue = Collections.min(occurrences.values());
		 List<String> lettresImportantes = new ArrayList<>();
			for (Entry<String, Integer> entry : occurrences.entrySet()) {  
				if (entry.getValue()==maxValue) {
					lettresImportantes.add(entry.getKey());    
				}
			}
			System.out.print(lettresImportantes.get(0));
			
		 
		}
		System.out.println();
	}
		

}


