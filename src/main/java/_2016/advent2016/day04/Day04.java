package advent2016.day04;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day04 {

	private static Logger logger = LoggerFactory.getLogger(Day04.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day04.class.getSimpleName()+"]");
		Day04 Day = new Day04();
		LocalDateTime start = LocalDateTime.now();
		//Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}



	public List<String> run1() throws IOException {
		String filename = "src/main/resources/advent2016/Day04.txt";
		List<String> fichier = Outil.importationString(filename);
		int sommeIdValides = 0;
		List<String> lignesValides = new ArrayList<>();

		for (String ligne: fichier) {
			String id = StringUtils.substring(StringUtils.split(ligne, "[")[0],-3);
			String name = StringUtils.left(StringUtils.split(ligne, "[")[0],StringUtils.split(ligne, "[")[0].length()-4);
			String checksum = StringUtils.substring(StringUtils.split(ligne, "[")[1],0,5);

			//retirer les tirets
			name=StringUtils.remove(name, "-");

			//compter les occurrences de chaque lettre
			HashMap<String, Integer> characterCount = Outil.characterCount(name);

			//prendre les 5 lettres les plus utilisees
			List<String> cinqLettresLesPlusUtilisees =  new ArrayList<>();
			for (int i=0;i<5;i++) {
				int maxValue = Collections.max(characterCount.values());
				List<String> lettresImportantes = new ArrayList<>();
				for (Entry<String, Integer> entry : characterCount.entrySet()) {  
					if (entry.getValue()==maxValue) {
						lettresImportantes.add(entry.getKey());    
					}
				}

				//trier les lettres avec le plus d'occurrences selon l'ordre alpha en cas d'égalité
				Collections.sort(lettresImportantes);
				cinqLettresLesPlusUtilisees.add(lettresImportantes.get(0));
				characterCount.replace(lettresImportantes.get(0), 0);
			}

			//mettre les lettres de la liste dans un string
			String aVerifier= cinqLettresLesPlusUtilisees.toString();
			aVerifier=StringUtils.remove(aVerifier, "[");
			aVerifier=StringUtils.remove(aVerifier, "]");
			aVerifier=StringUtils.remove(aVerifier, ",");
			aVerifier=StringUtils.remove(aVerifier, " ");

			if (aVerifier.equals(checksum)) {
				//System.out.println(checksum + " validée");
				sommeIdValides=sommeIdValides+ Integer.valueOf(id);
				lignesValides.add(ligne);
			}else {
				//System.out.println(checksum + " non validée");
			}
		}
		System.out.println("somme Id valides: "+sommeIdValides);
		return lignesValides;
	}



	public void run2() throws IOException {
		List<String> lignesValides = run1();

		for (String ligne: lignesValides) {
			String id = StringUtils.substring(StringUtils.split(ligne, "[")[0],-3);
			String name = StringUtils.left(StringUtils.split(ligne, "[")[0],StringUtils.split(ligne, "[")[0].length()-4);
			String checksum = StringUtils.substring(StringUtils.split(ligne, "[")[1],0,5);
			String ligneDecodee="";
			
			//decodage de chaque lettre (sauf tiret)
			for (int i=0;i<name.length();i++) {
				char lettre = name.charAt(i);
				if (lettre=='-') {
					//System.out.print(" ");
					ligneDecodee=ligneDecodee+" ";
				}
				if (lettre!='-') {
					int numLettre = ((int)lettre - 'a'+1);
					numLettre=numLettre+Integer.valueOf(id);
					numLettre=numLettre%26;
					String lettreString = String.valueOf((char)(numLettre + 64));
					//System.out.print(lettreString);
					ligneDecodee=ligneDecodee+lettreString;
				}

			}
			if (ligneDecodee.equals("NORTHPOLE OBJECT STORAGE")) {
				System.out.println("id cherchée: "+id);
			}
			//	System.out.print(" [id: "+id+"]");
			//	System.out.println(" ");
		}
	}
}


