package advent2016.day07;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;



public class Day07 {

	private static Logger logger = LoggerFactory.getLogger(Day07.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day07.class.getSimpleName()+"]");
		Day07 Day = new Day07();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}



	public void run1() throws IOException {
		String filename = "src/main/resources/advent2016/Day07.txt";
		List<String> fichier = Outil.importationString(filename);
		int cpt=0;

		for (String ligne:fichier) {
			boolean abbaOutsideBracket=false;
			boolean abbaInsideBracket=false;

			for (int i=0;i<ligne.length()-3;i++) {
				String carac1 = ""+ligne.charAt(i);
				String carac2 = ""+ligne.charAt(i+1);
				String carac3 = ""+ligne.charAt(i+2);
				String carac4 = ""+ligne.charAt(i+3);

				if ( carac1.equals(carac4) && carac2.equals(carac3) && !carac1.equals("[") && !carac2.equals("[") && !carac1.equals("]") && !carac2.equals("]") 
						&& !carac1.equals(carac2)) {

					if (isInBracket(ligne,i)) {
						abbaInsideBracket=true;
					}else {
						abbaOutsideBracket=true;
					}
				}
			}

			if (abbaOutsideBracket && !abbaInsideBracket) {
				//System.out.println(ligne);
				cpt++;
			}

		}
		System.out.println(cpt);
	}



	private boolean isInBracket(String ligne, int i) {
		//on verifie s'il y a un [ vers gauche et pas un ]
		boolean gauche=false;
		outerloop:
			for (int j=i;j>=0;j--) {
				char carac = ligne.charAt(j);
				if (carac==']') {
					break outerloop;
				}
				if (carac=='[') {
					gauche=true;
					break outerloop;
				}
			}

		//on verifie s'il y a un ] vers droite et pas un [
		boolean droite=false;
		outerloop:
			for (int j=i;j<ligne.length();j++) {
				char carac = ligne.charAt(j);
				if (carac=='[') {
					break outerloop;
				}
				if (carac==']') {
					droite=true;
					break outerloop;
				}
			}

		return (gauche && droite);
	}



	public void run2() throws IOException {
		String filename = "src/main/resources/advent2016/Day07.txt";
		List<String> fichier = Outil.importationString(filename);
		int cpt=0;

		
		for (String ligne:fichier) {
		//	System.out.println("-----");
			List<String> abaOutside = new ArrayList<>();
			List<String> babInside = new ArrayList<>();
			
			//recherche des ABA/BAB de cette ligne
			for (int i=0;i<ligne.length()-2;i++) {
				String carac1 = ""+ligne.charAt(i);
				String carac2 = ""+ligne.charAt(i+1);
				String carac3 = ""+ligne.charAt(i+2);

				if ( carac1.equals(carac3) && !carac2.equals(carac1) && !carac1.equals("[") && !carac2.equals("[") && !carac1.equals("]") && !carac2.equals("]") ) {

					//les ABA sont à l'exterieur des crochets
					if (!isInBracket(ligne, i)) {
						abaOutside.add(carac1+carac2+carac3);
					}
					//les ABA sont à l'interieur des crochets
					if (isInBracket(ligne, i)) {
						babInside.add(carac1+carac2+carac3);
					}
				}
			}
			
		//	System.out.println("out: "+abaOutside);
		//	System.out.println("in: "+babInside);
			
			//verifier la correspondance ABA outside / BAB inside
			boolean correspondance=false;
			for (String troisLettresOutside: abaOutside) {
				char a = troisLettresOutside.charAt(0);
				char b = troisLettresOutside.charAt(1);
				for (String troisLettresInside: babInside) {
					if (troisLettresInside.charAt(0)==b && troisLettresInside.charAt(1)==a) {
						//System.out.println(ligne+" supporte SSL");
						correspondance=true;
					}
				}
			}
			if (correspondance) {
				cpt++;
			}
		}
	//	System.out.println("----------");
		System.out.println(cpt);
		
	}


}


