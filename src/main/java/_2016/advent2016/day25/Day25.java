package advent2016.day25;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day25 {

	private static Logger logger = LoggerFactory.getLogger(Day25.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day25 Day = new Day25();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		//Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() {
		List<String> input = lectureInput();
		Map<String, Integer> registers = new HashMap<String, Integer>();
		boolean go=true;
		int a=0;

		while (go) {
			registers=initialisationRegisters2(registers);
			registers.put("a", a);
			List<Integer> output = calculs(input, registers);

			boolean check=true;
			int i=0;
			while (check) {
				if (i%2==0 & output.get(i)!=0) {
					check=false;
				}
				if (i%2==1 & output.get(i)!=1) {
					check=false;
				}
				i++;
				if (i==10) {
					System.err.println(a);
					check=false;
					go=false;
				}
			}


			if (check) {
				go=false;
				System.out.println(a);
			}

			a++;
		}
	}

	public void run2() {
	}


	private Map<String, Integer> initialisationRegisters2(Map<String, Integer> registers) {
		registers.put("a", 0);
		registers.put("b", 0);
		registers.put("c", 0);
		registers.put("d", 0);
		return registers;
	}

	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day25.txt";
		return Outil.importationString(filename);
	}

	public List<Integer> calculs(List<String> input, Map<String, Integer> registers) {
		List<Integer> output = new ArrayList<Integer>();
		int i=0;
		while (i<input.size()) {

			String ligne = input.get(i);
			String commande = StringUtils.split(ligne, " ")[0];

			switch (commande) {
			case "out": {
				int x2=0;
				String x = StringUtils.split(ligne, " ")[1];
				try{
					x2 = Integer.parseInt(x);
				}catch(Exception e){
					x2 = registers.get(x);
				}
				output.add(x2);
				if (output.size()==10) {i=100;}else {
					i++;
				}
				break;
			}
			case "cpy": {
				String x = StringUtils.split(ligne, " ")[1];
				String reg = StringUtils.split(ligne, " ")[2];
				int x2=0;
				try{
					x2 = Integer.parseInt(x);
					registers.put(reg, x2);
				}catch(Exception e){
					x2 = registers.get(x);
					registers.put(reg, x2);
				}
				i++;
				break;
			}
			case "inc": {
				String reg = StringUtils.split(ligne, " ")[1];
				registers.put(reg, registers.get(reg)+1);
				i++;
				break;
			}
			case "dec": {
				String reg = StringUtils.split(ligne, " ")[1];
				registers.put(reg, registers.get(reg)-1);
				i++;
				break;
			}
			case "jnz": {
				String reg = StringUtils.split(ligne, " ")[1];
				int x2=0;
				int y2=0;
				try{
					x2 = Integer.parseInt(reg);
					//registers.put(reg, x2);
				}catch(Exception e){
					x2 = registers.get(reg);
					//registers.put(reg, x2);
				}
				try {

					y2 = Integer.parseInt(StringUtils.split(ligne, " ")[2]);
				}catch(Exception e) {
					y2 = registers.get(StringUtils.split(ligne, " ")[2]);
				}

				if (x2!=0) {
					i=i+y2;
				}else {
					i++;
				}

				break;
			}

			case "tgl": {
				String y = StringUtils.split(ligne, " ")[1];
				int y2=0;
				try{
					y2 = Integer.parseInt(y);
				}catch(Exception e){
					y2 = registers.get(y);
				}
				if (i+y2<input.size()) {
					String commandeAModifier = StringUtils.split(input.get(i+y2), " ")[0];

					//gestion du toggle: modifie la commande de l'input
					if (commandeAModifier.equals("inc")) {
						input.set(i+y2, "dec " + StringUtils.split(input.get(i+y2), " ")[1]);
					}
					if (commandeAModifier.equals("dec")) {
						input.set(i+y2, "inc " + StringUtils.split(input.get(i+y2), " ")[1]);
					}
					if (commandeAModifier.equals("tgl")) {
						input.set(i+y2, "inc " + StringUtils.split(input.get(i+y2), " ")[1]);
					}
					if (commandeAModifier.equals("jnz")) {
						input.set(i+y2, "cpy " + StringUtils.split(input.get(i+y2), " ")[1]+" "+ StringUtils.split(input.get(i+y2), " ")[2]);
					}
					if (commandeAModifier.equals("cpy")) {
						input.set(i+y2, "jnz " + StringUtils.split(input.get(i+y2), " ")[1]+" "+ StringUtils.split(input.get(i+y2), " ")[2]);
					}
				}
				i++;
				break;
			}

			default:
				throw new IllegalArgumentException("Unexpected value: ");
			}
		}

		return output;
	}
}