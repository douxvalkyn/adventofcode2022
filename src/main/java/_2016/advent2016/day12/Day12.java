package advent2016.day12;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;



public class Day12 {

	private static Logger logger = LoggerFactory.getLogger(Day12.class);

	// main -----
	public static void main(String[] args) throws IOException  {
		logger.info("["+Day12.class.getSimpleName()+"]");
		Day12 Day = new Day12();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}
	
	
	

	public void run1() {
		List<String> input = lectureInput();
		Map<String, Integer> registers = new HashMap<String, Integer>();
		registers=initialisationRegisters(registers);
		calculs(input, registers);
		System.out.println("[RUN1] register a: "+registers.get("a"));

	}




	public void calculs(List<String> input, Map<String, Integer> registers) {
		int i=0;
		while (i<input.size()) {
			String ligne = input.get(i);
			switch (StringUtils.split(ligne, " ")[0]) {
			case "cpy": {
				String x = StringUtils.split(ligne, " ")[1];
				String reg = StringUtils.split(ligne, " ")[2];
				int x2=0;
				try{
					x2 = Integer.parseInt(x);
					registers.put(reg, x2);
				}catch(Exception e){
					x2 = registers.get(x);
					registers.put(reg, x2);
				}
				i++;
				break;
			}
			case "inc": {
				String reg = StringUtils.split(ligne, " ")[1];
				registers.put(reg, registers.get(reg)+1);
				i++;
				break;
			}
			case "dec": {
				String reg = StringUtils.split(ligne, " ")[1];
				registers.put(reg, registers.get(reg)-1);
				i++;
				break;
			}
			case "jnz": {
				String reg = StringUtils.split(ligne, " ")[1];
				int x2=0;
				try{
					x2 = Integer.parseInt(reg);
					registers.put(reg, x2);
				}catch(Exception e){
					x2 = registers.get(reg);
					registers.put(reg, x2);
				}
				int y = Integer.parseInt(StringUtils.split(ligne, " ")[2]);
				if (x2!=0) {
					i=i+y;
				}else {
					i++;
				}
				break;
			}

			default:
				throw new IllegalArgumentException("Unexpected value: ");
			}
		}
	}
	
	
	
	private Map<String, Integer> initialisationRegisters(Map<String, Integer> registers) {
		registers.put("a", 0);
		registers.put("b", 0);
		registers.put("c", 0);
		registers.put("d", 0);
		registers.put("e", 0);
		return registers;
	}
	
	private Map<String, Integer> initialisationRegistersRun2(Map<String, Integer> registers) {
		registers.put("a", 0);
		registers.put("b", 0);
		registers.put("c", 1);
		registers.put("d", 0);
		registers.put("e", 0);
		return registers;
	}




	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day12.txt";
		return Outil.importationString(filename);
	}




	public void run2() {
		List<String> input = lectureInput();
		Map<String, Integer> registers = new HashMap<String, Integer>();
		registers=initialisationRegistersRun2(registers);
		calculs(input, registers);
		System.out.println("[RUN2] register a: "+registers.get("a"));

	}
	
}

