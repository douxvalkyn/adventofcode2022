package advent2016.day20;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Outil;

public class Day20 {

	private static Logger logger = LoggerFactory.getLogger(Day20.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day20.class.getSimpleName() + "]");
		Day20 Day = new Day20();
		LocalDateTime start = LocalDateTime.now();
		//Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() {
		List<String> input = lectureInput()	;	
		long min = rechercheMin(input);
		System.out.println(min);
	}

	private long rechercheMin(List<String> input) {
		long min = 0;
		boolean go=true;

		while (go) {
			outerloop:
				for (String string:input) {
					Long a = Long.valueOf(StringUtils.split(string, '-')[0]);
					Long b = Long.valueOf(StringUtils.split(string, '-')[1]);
					if (min>=a && min<=b) {
						min=b+1;
						go=true;
						break outerloop;
					}else {
						go=false;
					}
				}
		}
		return min;
	}

	public void run2() {
		List<String> input = lectureInput()	;	
		List<Long> liste = rechercheAllIP(input);
		System.out.println(liste.size() +  " ... il faut retrancher 1 car la derniere valeur n'est pas autorisée.");
		
	}
	private List<Long> rechercheAllIP(List<String> input) {
		List<Long> liste = new ArrayList<Long>();
		long min = rechercheMin(input);
		liste.add(min);
		
		boolean go=true;
		while (go) {
		Long nextMin = rechercheNextMin(input, min);
		if (nextMin==-1L) {
			go = false;
		}else {
		min = rechercheMin2(input,nextMin );
		liste.add(min);
		}
		}
		
		return liste;
	}
	
	private long rechercheMin2(List<String> input, Long min) {
		boolean go=true;

		while (go) {
			outerloop:
				for (String string:input) {
					Long a = Long.valueOf(StringUtils.split(string, '-')[0]);
					Long b = Long.valueOf(StringUtils.split(string, '-')[1]);
					if (min>=a && min<=b) {
						min=b+1;
						go=true;
						break outerloop;
					}else {
						go=false;
					}
				}
		}
		return min;
	}

	private Long rechercheNextMin(List<String> input, long min) {
		List<Long>candidats= new ArrayList<Long>();
		for (String string: input) {
			Long a = Long.valueOf(StringUtils.split(string, '-')[0]);
			Long b = Long.valueOf(StringUtils.split(string, '-')[1]);
			if (a>min) {
				candidats.add(a);
			}
		}
		if (candidats.size()>0) {
		Long newMin = Outil.minLong(candidats);
		return newMin;
		}else {
			return -1L;
		}
		
		
	}

	private List<String> lectureInput() {
		String filename = "src/main/resources/advent2016/Day19.txt";
		return Outil.importationString(filename);
	}


}
