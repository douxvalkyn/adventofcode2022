package advent2016.day15;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day15 {

	private static Logger logger = LoggerFactory.getLogger(Day15.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day15.class.getSimpleName() + "]");
		Day15 Day = new Day15();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}


	
	public void run2() {
		long t=0;
		boolean ok=false;
		while (t<4_000_000 & !ok) {
		
			if ( (t+2)%13==0 && (t+12)%19==0 && (t+5)%3==0 && (t+5)%7==0 && (t+8)%5==0 && (t+11)%17==0 && (t+7)%11==0 ) {
				System.err.println(t);
				ok=true;
			}
			t++;
		}
	}
	
	public void run1() {
		long t=0;
		boolean ok=false;
		while (t<400_000 & !ok) {
		
			if ( (t+2)%13==0 && (t+12)%19==0 && (t+5)%3==0 && (t+5)%7==0 && (t+8)%5==0 && (t+11)%17==0)  {
				System.err.println(t);
				ok=true;
			}
			t++;
		}
	}
	
	public void run1Example() {
		long t=0;
		boolean ok=false;
		while (t<1000000 & !ok) {
		
			if ( (t+5)%5==0 && (t+3)%2==0) {
				System.err.println(t);
				ok=true;
			}
			t++;
		}
	}


}
