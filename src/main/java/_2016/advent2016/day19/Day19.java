package advent2016.day19;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.CircularLinkedList;
import advent2016.outils.Node;

public class Day19 {

	private static Logger logger = LoggerFactory.getLogger(Day19.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day19.class.getSimpleName() + "]");
		Day19 Day = new Day19();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		//Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	private void run1() {
		CircularLinkedList liste = new CircularLinkedList();
		int nbElfes=3005290;
		for (int i=1;i<=nbElfes;i++) {
			liste.addNode(i);
		}
		LocalDateTime start = LocalDateTime.now();
		Node n0 = liste.getRealNode(1);
		while (liste.getHead()!=liste.getTail()) {
			Node n1 = n0.getNextNode();
			liste.deleteNode(n1.getValue());
			n0=n0.getNextNode();
			int taille=liste.size();
			if (taille%1000==0) {
				LocalDateTime pause = LocalDateTime.now();
				System.err.println(taille +" "+ Duration.between(start,pause).getSeconds());
				start = LocalDateTime.now();
			}
		}
		System.out.println(liste.getHead().getValue());

		
	}

	private void run2() {
		CircularLinkedList liste = new CircularLinkedList();
		int nbElfes=3005290;
		for (int i=1;i<=nbElfes;i++) {
			liste.addNode(i);
		}
		Node n0 = liste.getRealNode(1);
		LocalDateTime start = LocalDateTime.now();
		while (liste.getHead()!=liste.getTail()) {
			Node n1 = liste.getNodeEnFace(n0);
			liste.deleteNode(n1.getValue());
			n0=n0.getNextNode();
			int taille=liste.size();
			if (taille%1000==0) {
				LocalDateTime pause = LocalDateTime.now();
				System.err.println(taille +" "+ Duration.between(start,pause).getSeconds());
				start = LocalDateTime.now();
			}
		}
		System.out.println(liste.getHead().getValue());
	}


}
