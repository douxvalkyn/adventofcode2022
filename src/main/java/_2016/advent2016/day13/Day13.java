package advent2016.day13;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.outils.Case;
import advent2016.outils.Graph;
import advent2016.outils.Grille;

public class Day13 {

	private static Logger logger = LoggerFactory.getLogger(Day13.class);

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day13.class.getSimpleName() + "]");
		Day13 Day = new Day13();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		Grille grille = creationGrille(1358);
		Graph graphe = creationGraphe(grille);
		// grille.affichageEtat();
		int cpt = 0;
		for (int x = 0; x < 50; x++) {
			for (int y = 0; y < 50; y++) {
				if (grille.getCase(x, y).getEtat().equals(".")) {
					int cost = graphe.uniformSearch(101, 100 * x + y);
					if (cost <= 50 && cost > 0) {
						cpt++;
					}
				}
			}
		}
		System.out.println("nb locations <= 50 steps : " + (cpt + 1)); // on ajout 1 pour compter la localisation de
																		// départ (qui a un cout de 0 avec l'algo)
	}

	public void run1() {
		// Grille grille = creationGrille(10);
		Grille grille = creationGrille(1358);
		Graph graphe = creationGraphe(grille);
		// int cost = graphe.uniformSearch(101,704);
		// grille.affichageEtat();
		
		int cost = graphe.uniformSearch(101, 3139);
		System.out.println("Cost : " + cost);

	}

	private Graph creationGraphe(Grille grille) {
		Graph graph = new Graph();

		// Ajout des noeuds
		for (int x = 0; x < 50; x++) {
			for (int y = 0; y < 50; y++) {
				graph.addNode(100 * x + y);
			}
		}

		// Ajout des liens
		for (int x = 0; x < 50; x++) {
			for (int y = 0; y < 50; y++) {
				if (grille.getCase(x, y).getEtat().equals(".")) {

					List<Case> casesAdj = grille.getCase(x, y).getCasesAdjacentes2(grille);

					if (casesAdj.get(0) != null) { // N
						graph.addEdge(100 * x + y, 100 * x + (y - 1), 1);
					}
					if (casesAdj.get(1) != null) { // S
						graph.addEdge(100 * x + y, 100 * x + (y + 1), 1);
					}
					if (casesAdj.get(2) != null) { // E
						graph.addEdge(100 * x + y, 100 * (x + 1) + (y), 1);
					}
					if (casesAdj.get(3) != null) { // W
						graph.addEdge(100 * x + y, 100 * (x - 1) + (y), 1);
					}

				}
			}
		}

		return graph;
	}

	private Grille creationGrille(int input) {
		Grille grille = new Grille(50, 50);
		for (int x = 0; x < 50; x++) {
			for (int y = 0; y < 50; y++) {
				int score = x * x + 3 * x + 2 * x * y + y + y * y + input;
				String scoreBin = Integer.toBinaryString(score);
				int nbOne = 0;
				for (int i = 0; i < scoreBin.length(); i++) {
					if (scoreBin.charAt(i) == '1') {
						nbOne++;
					}
				}
				if (nbOne % 2 == 0) {// open space
					grille.getCase(x, y).setEtat(".");
				} else {
					grille.getCase(x, y).setEtat("#");
				}
			}
		}
		return grille;
	}

}
