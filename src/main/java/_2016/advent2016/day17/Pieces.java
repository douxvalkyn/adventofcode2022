package advent2016.day17;

import java.util.List;
import java.util.Objects;



public class Pieces {
	public List<Integer> pieces;
	public int nbDeplacements;
	public int pieceActuelle;
	public String passwordActuel;
	public List<Integer> getPieces() {
		return pieces;
	}
	public void setPieces(List<Integer> pieces) {
		this.pieces = pieces;
	}
	public int getNbDeplacements() {
		return nbDeplacements;
	}
	public void setNbDeplacements(int nbDeplacements) {
		this.nbDeplacements = nbDeplacements;
	}
	public int getPieceActuelle() {
		return pieceActuelle;
	}
	public void setPieceActuelle(int pieceActuelle) {
		this.pieceActuelle = pieceActuelle;
	}
	public String getPasswordActuel() {
		return passwordActuel;
	}
	public void setPasswordActuel(String passwordActuel) {
		this.passwordActuel = passwordActuel;
	}
	public Pieces(List<Integer> pieces, int nbDeplacements, int pieceActuelle, String passwordActuel) {
		super();
		this.pieces = pieces;
		this.nbDeplacements = nbDeplacements;
		this.pieceActuelle = pieceActuelle;
		this.passwordActuel = passwordActuel;
	}
	@Override
	public int hashCode() {
		return Objects.hash(nbDeplacements, passwordActuel, pieceActuelle, pieces);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pieces other = (Pieces) obj;
		return nbDeplacements == other.nbDeplacements && Objects.equals(passwordActuel, other.passwordActuel)
				&& pieceActuelle == other.pieceActuelle && Objects.equals(pieces, other.pieces);
	}


	
}
