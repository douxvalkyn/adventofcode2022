package advent2016.day17;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day17 {

	private static Logger logger = LoggerFactory.getLogger(Day17.class);
	
	
	//Exemple d'algo BFS

	// main -----
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day17.class.getSimpleName() + "]");
		Day17 Day = new Day17();
		LocalDateTime start = LocalDateTime.now();
		Day.run1();
		Day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	public void run2() {
		String password="hhhxzeay";
		String res=bfs2(password);
		System.err.println("run2: "+res);
	}

	
	public String bfs(String password) {

		List<Integer>pieces = new ArrayList<Integer>();
		for (int i=0;i<16;i++) {
		pieces.add(i);
		}
		Pieces initialPieces = new Pieces(pieces, 0, 0, password); 
		Queue<Pieces> queue = new LinkedList<>(); 
		queue.add(initialPieces);

		Set<Pieces> visitedPieces = new HashSet<>(); // historique des etages visites
		visitedPieces.add(initialPieces);


		while (!queue.isEmpty()) {
			
			Pieces piecesActuelles = queue.poll();
			int pieceActuelle = piecesActuelles.getPieceActuelle();
			int nbDeplActuel = piecesActuelles.getNbDeplacements();
			String passwordActuel = piecesActuelles.getPasswordActuel();
			
			//System.out.println(pieceActuelle);

			// fin
			if (pieceActuelle==15) {
				return nbDeplActuel+" "+passwordActuel;
			}
			
			//traitement
			
			//liste des sorties possibles: NSOE
			int[] directionsPossibles = new int[4];
			
			
			//reduction des sorties possibles selon l'emplacement de la piece actuelle
			if (pieceActuelle==0) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=0;directionsPossibles[3]=1;	}//
			if (pieceActuelle==1 | pieceActuelle==2) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=1;	}//
			if (pieceActuelle==3) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=0;	}//
			
			if (pieceActuelle==4 | pieceActuelle==8) {directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=0;directionsPossibles[3]=1;	}//
			
			if (pieceActuelle==5 | pieceActuelle==6 | pieceActuelle==9 | pieceActuelle==10) {
				directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=1;	}//
			
			if (pieceActuelle==7 | pieceActuelle==11) {directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=0; }//
			if (pieceActuelle==13 | pieceActuelle==14) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=1;directionsPossibles[3]=1; }//
			
			if (pieceActuelle==12) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=0;directionsPossibles[3]=1; }//
			if (pieceActuelle==15) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=1;directionsPossibles[3]=0; }//
			
			//reduction des sorties possibles selon le password décodé
			String md5Hex = DigestUtils.md5Hex(passwordActuel);
			String udlr = StringUtils.substring(md5Hex, 0,4);
			char nord = udlr.charAt(0);
			char sud = udlr.charAt(1);
			char ouest = udlr.charAt(2);
			char est = udlr.charAt(3);
			if (nord!= 'b' && nord!='c' && nord!= 'd' && nord!= 'e' && nord!= 'f') {
				//close
				directionsPossibles[0]=0;
			}
			if (sud!= 'b' && sud!='c' && sud!= 'd' && sud!= 'e' && sud!= 'f') {
				//close
				directionsPossibles[1]=0;
			}
			if (ouest!= 'b' && ouest!='c' && ouest!= 'd' && ouest!= 'e' && ouest!= 'f') {
				//close
				directionsPossibles[2]=0;
			}
			if (est!= 'b' && est!='c' && est!= 'd' && est!= 'e' && est!= 'f') {
				//close
				directionsPossibles[3]=0;
			}
			
			
			//ajout d'une visite pour chaque sortie possible
			if (directionsPossibles[0]==1) { //N
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle-4, passwordActuel+"U" );
				
				//si pas déjà visité, on ajoute à la queue
				if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
					visitedPieces.add(nextPieces);
				}
			}
			if (directionsPossibles[1]==1) { //S
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle+4, passwordActuel+"D" );
				//si pas déjà visité, on ajoute à la queue
				if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
					visitedPieces.add(nextPieces);
				}
			}
			if (directionsPossibles[2]==1) { //O
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle-1, passwordActuel+"L" );
				//si pas déjà visité, on ajoute à la queue
				if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
					visitedPieces.add(nextPieces);
				}
			}
			if (directionsPossibles[3]==1) { //E
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle+1, passwordActuel+"R" );
				//si pas déjà visité, on ajoute à la queue
				if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
					visitedPieces.add(nextPieces);
				}
			}
		}
		return "-1";
	}
	
	
	public String bfs2(String password) {

		List<Integer>pieces = new ArrayList<Integer>();
		for (int i=0;i<16;i++) {
		pieces.add(i);
		}
		Pieces initialPieces = new Pieces(pieces, 0, 0, password); 
		Queue<Pieces> queue = new LinkedList<>(); 
		queue.add(initialPieces);

		//Set<Pieces> visitedPieces = new HashSet<>(); // historique des etages visites
		//visitedPieces.add(initialPieces);
		
		int maxDeplacements=0;


		while (!queue.isEmpty()) {
			
			Pieces piecesActuelles = queue.poll();
			int pieceActuelle = piecesActuelles.getPieceActuelle();
			int nbDeplActuel = piecesActuelles.getNbDeplacements();
			String passwordActuel = piecesActuelles.getPasswordActuel();
			
			// fin
			if (pieceActuelle==15) {
				maxDeplacements= nbDeplActuel;
				
			}else {
			
			//traitement
			
			//liste des sorties possibles: NSOE
			int[] directionsPossibles = new int[4];
			
			
			//reduction des sorties possibles selon l'emplacement de la piece actuelle
			if (pieceActuelle==0) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=0;directionsPossibles[3]=1;	}//
			if (pieceActuelle==1 | pieceActuelle==2) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=1;	}//
			if (pieceActuelle==3) {directionsPossibles[0]=0;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=0;	}//
			
			if (pieceActuelle==4 | pieceActuelle==8) {directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=0;directionsPossibles[3]=1;	}//
			
			if (pieceActuelle==5 | pieceActuelle==6 | pieceActuelle==9 | pieceActuelle==10) {
				directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=1;	}//
			
			if (pieceActuelle==7 | pieceActuelle==11) {directionsPossibles[0]=1;directionsPossibles[1]=1;directionsPossibles[2]=1;directionsPossibles[3]=0; }//
			if (pieceActuelle==13 | pieceActuelle==14) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=1;directionsPossibles[3]=1; }//
			
			if (pieceActuelle==12) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=0;directionsPossibles[3]=1; }//
			if (pieceActuelle==15) {directionsPossibles[0]=1;directionsPossibles[1]=0;directionsPossibles[2]=1;directionsPossibles[3]=0; }//
			
			//reduction des sorties possibles selon le password décodé
			String md5Hex = DigestUtils.md5Hex(passwordActuel);
			String udlr = StringUtils.substring(md5Hex, 0,4);
			char nord = udlr.charAt(0);
			char sud = udlr.charAt(1);
			char ouest = udlr.charAt(2);
			char est = udlr.charAt(3);
			if (nord!= 'b' && nord!='c' && nord!= 'd' && nord!= 'e' && nord!= 'f') {
				//close
				directionsPossibles[0]=0;
			}
			if (sud!= 'b' && sud!='c' && sud!= 'd' && sud!= 'e' && sud!= 'f') {
				//close
				directionsPossibles[1]=0;
			}
			if (ouest!= 'b' && ouest!='c' && ouest!= 'd' && ouest!= 'e' && ouest!= 'f') {
				//close
				directionsPossibles[2]=0;
			}
			if (est!= 'b' && est!='c' && est!= 'd' && est!= 'e' && est!= 'f') {
				//close
				directionsPossibles[3]=0;
			}
			
			
			//ajout d'une visite pour chaque sortie possible
			if (directionsPossibles[0]==1) { //N
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle-4, passwordActuel+"U" );
				
				//si pas déjà visité, on ajoute à la queue
				//if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
					//visitedPieces.add(nextPieces);
				//}
			}
			if (directionsPossibles[1]==1) { //S
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle+4, passwordActuel+"D" );
				//si pas déjà visité, on ajoute à la queue
				//if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
				//	visitedPieces.add(nextPieces);
				//}
			}
			if (directionsPossibles[2]==1) { //O
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle-1, passwordActuel+"L" );
				//si pas déjà visité, on ajoute à la queue
				//if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
				//	visitedPieces.add(nextPieces);
				//}
			}
			if (directionsPossibles[3]==1) { //E
				Pieces nextPieces = new Pieces(pieces, nbDeplActuel + 1, pieceActuelle+1, passwordActuel+"R" );
				//si pas déjà visité, on ajoute à la queue
				//if (!visitedPieces.contains(nextPieces)) {
					queue.add(nextPieces);
				//	visitedPieces.add(nextPieces);
				//}
			}
		}
		}
		return ""+maxDeplacements;
	}
	
	
	
	public void run1() {
		String password="hhhxzeay";
		String res=bfs(password);
		System.err.println("run1: "+res);	
	}



	
}
