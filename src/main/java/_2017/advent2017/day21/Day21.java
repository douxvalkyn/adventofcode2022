package advent2017.day21;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Case;
import advent2017.outils.Grille;
import advent2017.outils.Outil;

public class Day21 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day21.class.getSimpleName() + "]");
		Day21 day = new Day21();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() throws IOException {
		System.out.println("RUN1:");
		run(5);
	}

	public void run2() throws IOException {
		System.out.println("RUN2:");
		run(18);
	}

	// run ----
	public void run(int stepMax) throws IOException {

		// lecture input--
		String filename = "src/main/resources/advent2017/Day21.txt";
		List<String> input = Outil.importationString(filename);
		// System.out.println(input);
		Map<String, String> map = new HashMap<String, String>();
		for (String ligne : input) {
			String avant = StringUtils.split(ligne, " => ")[0];
			String apres = StringUtils.split(ligne, " => ")[1];
			map.put(avant, apres);
		}
		Grille grille = new Grille(3, 3);
		grille.getCase(0, 0).setEtat(".");
		grille.getCase(1, 0).setEtat("#");
		grille.getCase(2, 0).setEtat(".");
		grille.getCase(0, 1).setEtat(".");
		grille.getCase(1, 1).setEtat(".");
		grille.getCase(2, 1).setEtat("#");
		grille.getCase(0, 2).setEtat("#");
		grille.getCase(1, 2).setEtat("#");
		grille.getCase(2, 2).setEtat("#");
		// System.out.println("Step 0 (taille 3):");
		// grille.affichageEtat();
		String grilleAPlat = miseAPlat(grille);

		// grilleAPlat = "......../########/......../......../......../########/......../......../";
		// grilleAPlat = "#..#/..../..../#..#";
		// grilleAPlat = ".#..../.#..../.#..../....#./....#./....#./";

		for (int step = 0; step < stepMax; step++) {
			// compter la taille de la grille
			int taille = (int) Math.sqrt(grilleAPlat.length());

			if (taille % 2 == 0) {
				// on divise la grille en carrés de 2x2 et on applique les regles 2x2

				// division
				List<String> carres = divisionEnCarres2X2(grilleAPlat);

				// application des regles sur chaque carré 2x2
				List<String> carresAugmentes3x3 = applicationRegles(carres, map);

				// recomposition de la grille totale
				grilleAPlat = recompositionCarres3X3(carresAugmentes3x3);

			}
			if (taille % 3 == 0 & taille % 2 != 0) {
				// on divise la grille en carrés de 3x3 et on applique les regles 3x3

				// division
				List<String> carres = divisionEnCarres3X3(grilleAPlat);

				// application des regles sur chaque carré 3x3
				grilleAPlat = miseAPlat(grille);
				List<String> carresAugmentes4x4 = applicationRegles(carres, map);

				// recomposition de la grille totale
				grilleAPlat = recompositionCarres4X4(carresAugmentes4x4);

			}
			taille = (int) Math.sqrt(grilleAPlat.length());
			// System.out.println("Step "+(step+1)+ " (taille "+ taille+")"+":");
			// miseEnGrille(grilleAPlat).affichageEtat();

		}

		// compter le nombre de pixels de la grille finale
		int cpt = 0;
		for (int i = 0; i < grilleAPlat.length(); i++) {
			if (grilleAPlat.charAt(i) == '#') {
				cpt++;
			}
		}
		System.out.println(cpt);
	}

	private String recompositionCarres4X4(List<String> carres) {
		int nbCarres = carres.size();
		int nbCarresParLigne = (int) Math.sqrt(nbCarres); // = nbCarres par colonne

		String res = "";
		for (int i = 0; i < nbCarres; i = i + nbCarresParLigne) {
			String res1 = "";
			String res2 = "";
			String res3 = "";
			String res4 = "";
			for (int j = 0; j < nbCarresParLigne; j++) {
				res1 = res1 + StringUtils.split(carres.get(j + i), "/")[0];
				res2 = res2 + StringUtils.split(carres.get(j + i), "/")[1];
				res3 = res3 + StringUtils.split(carres.get(j + i), "/")[2];
				res4 = res4 + StringUtils.split(carres.get(j + i), "/")[3];
			}
			res1 = res1 + "/";
			res2 = res2 + "/";
			res3 = res3 + "/";
			res4 = res4 + "/";
			res = res + res1 + res2 + res3 + res4;
		}

		// retirer le dernier slash
		res = res.substring(0, res.length() - 1);

		return res;
	}

	private List<String> divisionEnCarres3X3(String grilleAPlat) {

		String[] lignes = StringUtils.split(grilleAPlat, "/");
		List<String> carres = new ArrayList<String>();
		int nbLignes = lignes.length;
		for (int i = 0; i < nbLignes; i = i + 3) {

			String ligneHaut = lignes[i];
			String ligneMilieu = lignes[i + 1];
			String ligneBas = lignes[i + 2];
			for (int j = 0; j < ligneHaut.length(); j = j + 3) {
				carres.add("" + ligneHaut.charAt(j) + ligneHaut.charAt(j + 1) + ligneHaut.charAt(j + 2) + "/" + ligneMilieu.charAt(j) + ligneMilieu.charAt(j + 1) + ligneMilieu.charAt(j + 2) + "/"
					+ ligneBas.charAt(j) + ligneBas.charAt(j + 1) + ligneBas.charAt(j + 2));
			}
		}
		return carres;
	}

	private List<String> applicationRegles(List<String> carres, Map<String, String> map) {

		List<String> res = new ArrayList<String>();

		for (int i = 0; i < carres.size(); i++) {
			String carre = carres.get(i);
			List<String> carreAllFlipRotation = carreAllFlipRotation(carre);
			boolean ok = false;
			for (String c : carreAllFlipRotation) {
				if (map.containsKey(c) & !ok) {
					res.add(map.get(c));
					ok = true;
				}
			}
		}
		return res;
	}

	/**
	 * @param carre
	 * @return la liste de toutes les carrés possibles via flip et rotation à partir du carré en entrée
	 */
	private List<String> carreAllFlipRotation(String carre) {
		List<String> res = new ArrayList<String>();
		List<Grille> res0 = new ArrayList<Grille>();
		Grille grille = miseEnGrille(carre);

		// if I is identity, R the rotation, and H a horizontal flip: 8 combinations are: I,R,RR,RRR, H,RH,RRH, RRRH

		// I
		res0.add(grille);

		// R
		grille = grille.rotate();
		res0.add(grille);

		// RR
		grille = grille.rotate();
		res0.add(grille);

		// RRR
		grille = grille.rotate();
		res0.add(grille);
		// retour position initiale
		grille = grille.rotate();

		// H
		grille = grille.flipHorizontal();
		res0.add(grille);
		//// retour position initiale
		grille = grille.flipHorizontal();

		// RH
		grille = grille.rotate();
		grille = grille.flipHorizontal();
		res0.add(grille);
		// retour position precedente
		grille = grille.flipHorizontal();

		// RRH
		grille = grille.rotate();
		grille = grille.flipHorizontal();
		res0.add(grille);
		// retour position precedente
		grille = grille.flipHorizontal();

		// RRRH
		grille = grille.rotate();
		grille = grille.flipHorizontal();
		res0.add(grille);

		// mise en ligne des grilles
		for (int i = 0; i < res0.size(); i++) {
			Grille g = res0.get(i);
			String g1 = miseAPlat(g);
			res.add(g1);
		}

		return res;
	}

	public String recompositionCarres3X3(List<String> carres) {
		int nbCarres = carres.size();
		int nbCarresParLigne = (int) Math.sqrt(nbCarres); // = nbCarres par colonne

		String res = "";
		for (int i = 0; i < nbCarres; i = i + nbCarresParLigne) {
			String res1 = "";
			String res2 = "";
			String res3 = "";
			for (int j = 0; j < nbCarresParLigne; j++) {
				res1 = res1 + StringUtils.split(carres.get(j + i), "/")[0];
				res2 = res2 + StringUtils.split(carres.get(j + i), "/")[1];
				res3 = res3 + StringUtils.split(carres.get(j + i), "/")[2];
			}
			res1 = res1 + "/";
			res2 = res2 + "/";
			res3 = res3 + "/";
			res = res + res1 + res2 + res3;
		}

		// retirer le dernier slash
		res = res.substring(0, res.length() - 1);

		return res;
	}

	public List<String> divisionEnCarres2X2(String grilleAPlat) {

		String[] lignes = StringUtils.split(grilleAPlat, "/");
		List<String> carres = new ArrayList<String>();
		int nbLignes = lignes.length;
		for (int i = 0; i < nbLignes; i = i + 2) {

			String ligneHaut = lignes[i];
			String ligneBas = lignes[i + 1];
			for (int j = 0; j < ligneHaut.length(); j = j + 2) {
				carres.add("" + ligneHaut.charAt(j) + ligneHaut.charAt(j + 1) + "/" + ligneBas.charAt(j) + ligneBas.charAt(j + 1));
			}
		}

		return carres;
	}

	public String miseAPlat(Grille grille) {
		String grilleAPlat = "";
		int taille = grille.getNbCol();
		List<Case> cases = grille.getCases();
		int i = 0;
		for (Case cell : cases) {
			i++;
			grilleAPlat = grilleAPlat + cell.getEtat();
			if (i == taille) {
				grilleAPlat = grilleAPlat + "/";
				i = 0;
			}
		}
		// retirer le dernier slash
		grilleAPlat = grilleAPlat.substring(0, grilleAPlat.length() - 1);
		return grilleAPlat;
	}

	public Grille miseEnGrille(String grilleAPlat) {
		String[] grilleAPlatSansSlash = StringUtils.split(grilleAPlat, "/");
		Grille grille = new Grille(grilleAPlatSansSlash.length, grilleAPlatSansSlash.length);
		grille.getCases().clear();
		for (int i = 0; i < grilleAPlatSansSlash.length; i++) {
			String ligne = grilleAPlatSansSlash[i];
			for (int j = 0; j < ligne.length(); j++) {
				char carac = ligne.charAt(j);
				Case cell = new Case(j, i);
				cell.setEtat("" + carac);
				grille.getCases().add(cell);
			}
		}
		return grille;
	}

}
