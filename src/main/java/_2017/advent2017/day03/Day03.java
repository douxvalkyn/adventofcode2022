package advent2017.day03;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2017.outils.Case;
import advent2017.outils.Grille;

public class Day03 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day03.class.getSimpleName() + "]");
		Day03 day = new Day03();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		day.run1();
		// day.run1bis();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {

		// fait à la main par calcul car chaque carré se termine par 1, 9, 25, soit les impairs², il suffit ensuite de compter les cases

	}

	// run2 ----
	// amusons nous à créér la grille spiralaire
	public void run2() throws IOException {
		Grille grille = new Grille(20, 20);
		// on place le premier point au centre
		grille.getCase(10, 10).setValeur(1);

		Case cell = grille.getCase(10, 10);

		int nombreMax = 65;

		// 1er déplacement: En partant de cette premiere case, on se déplace d'un cran vers la droite
		cell = grille.getCase(cell.getX() + 1, cell.getY());
		cell.setValeur(1);
		String deplacement = "E";
		int valeur = 2;

		// ensuite pour chaque déplacement, on continue en cherchant à changer de direction dès que possible:
		// si on se déplace vers E, on cherche à aller vers N
		// si on se déplace vers N, on cherche à aller vers O
		// si on se déplace vers O, on cherche à aller vers S
		// si on se déplace vers S, on cherche à aller vers E

		for (int numCase = 0; numCase < nombreMax - 2; numCase++) {

			boolean go = true;
			if (deplacement == "E" & go) {
				go = false;
				if (grille.getCase(cell.getX(), cell.getY() - 1).getValeur() == 0) {
					cell = grille.getCase(cell.getX(), cell.getY() - 1);
					calculValeurSelonVoisines(grille, cell);
					deplacement = "N";
				} else {
					cell = grille.getCase(cell.getX() + 1, cell.getY());
					calculValeurSelonVoisines(grille, cell);
				}
			}
			if (deplacement == "N" & go) {
				go = false;
				if (grille.getCase(cell.getX() - 1, cell.getY()).getValeur() == 0) {
					cell = grille.getCase(cell.getX() - 1, cell.getY());
					calculValeurSelonVoisines(grille, cell);
					deplacement = "O";
				} else {
					cell = grille.getCase(cell.getX(), cell.getY() - 1);
					calculValeurSelonVoisines(grille, cell);
				}
			}
			if (deplacement == "O" & go) {
				go = false;
				if (grille.getCase(cell.getX(), cell.getY() + 1).getValeur() == 0) {
					cell = grille.getCase(cell.getX(), cell.getY() + 1);
					calculValeurSelonVoisines(grille, cell);
					deplacement = "S";
				} else {
					cell = grille.getCase(cell.getX() - 1, cell.getY());
					calculValeurSelonVoisines(grille, cell);
				}
			}
			if (deplacement == "S" & go) {
				go = false;
				if (grille.getCase(cell.getX() + 1, cell.getY()).getValeur() == 0) {
					cell = grille.getCase(cell.getX() + 1, cell.getY());
					calculValeurSelonVoisines(grille, cell);
					deplacement = "E";
				} else {
					cell = grille.getCase(cell.getX(), cell.getY() + 1);
					calculValeurSelonVoisines(grille, cell);
				}
			}
		}

		// calcul de la distance entre la case cible et l'origine;
		int dist = Math.abs(cell.getX() - 10) + Math.abs(cell.getY() - 10);

		// affichage reponse
		grille.affichageValeur();
		System.out.println(dist);
	}

	public void calculValeurSelonVoisines(Grille grille, Case cell) {
		int valeur;
		List<Case> voisines = cell.getCasesAdjacentesQueen(grille);
		valeur = 0;
		for (Case voisine : voisines) {
			valeur = valeur + voisine.getValeur();
		}
		cell.setValeur(valeur);
	}

	// run1bis ----
	// amusons nous à créér la grille spiralaire
	public void run1bis() throws IOException {
		Grille grille = new Grille(650, 650);
		// on place le premier point au centre
		grille.getCase(325, 325).setValeur(1);

		Case cell = grille.getCase(325, 325);

		int nombreMax = 368078;

		// 1er déplacement: En partant de cette premiere case, on se déplace d'un cran vers la droite
		cell = grille.getCase(cell.getX() + 1, cell.getY());
		cell.setValeur(2);
		String deplacement = "E";
		int valeur = 2;

		// ensuite pour chaque déplacement, on continue en cherchant à changer de direction dès que possible:
		// si on se déplace vers E, on cherche à aller vers N
		// si on se déplace vers N, on cherche à aller vers O
		// si on se déplace vers O, on cherche à aller vers S
		// si on se déplace vers S, on cherche à aller vers E

		for (int numCase = 0; numCase < nombreMax - 2; numCase++) {
			if (numCase % 10000 == 0) {
				System.out.println(numCase);
			}
			boolean go = true;
			if (deplacement == "E" & go) {
				go = false;
				if (grille.getCase(cell.getX(), cell.getY() - 1).getValeur() == 0) {
					cell = grille.getCase(cell.getX(), cell.getY() - 1);
					cell.setValeur(valeur + 1);
					deplacement = "N";
					valeur++;
				} else {
					cell = grille.getCase(cell.getX() + 1, cell.getY());
					cell.setValeur(valeur + 1);
					valeur++;
				}
			}
			if (deplacement == "N" & go) {
				go = false;
				if (grille.getCase(cell.getX() - 1, cell.getY()).getValeur() == 0) {
					cell = grille.getCase(cell.getX() - 1, cell.getY());
					cell.setValeur(valeur + 1);
					deplacement = "O";
					valeur++;
				} else {
					cell = grille.getCase(cell.getX(), cell.getY() - 1);
					cell.setValeur(valeur + 1);
					valeur++;
				}
			}
			if (deplacement == "O" & go) {
				go = false;
				if (grille.getCase(cell.getX(), cell.getY() + 1).getValeur() == 0) {
					cell = grille.getCase(cell.getX(), cell.getY() + 1);
					cell.setValeur(valeur + 1);
					deplacement = "S";
					valeur++;
				} else {
					cell = grille.getCase(cell.getX() - 1, cell.getY());
					cell.setValeur(valeur + 1);
					valeur++;
				}
			}
			if (deplacement == "S" & go) {
				go = false;
				if (grille.getCase(cell.getX() + 1, cell.getY()).getValeur() == 0) {
					cell = grille.getCase(cell.getX() + 1, cell.getY());
					cell.setValeur(valeur + 1);
					deplacement = "E";
					valeur++;
				} else {
					cell = grille.getCase(cell.getX(), cell.getY() + 1);
					cell.setValeur(valeur + 1);
					valeur++;
				}
			}
		}

		// calcul de la distance entre la case cible et l'origine;
		int dist = Math.abs(cell.getX() - 325) + Math.abs(cell.getY() - 325);
		// grille.affichageValeur();
		System.out.println(dist);
	}
}
