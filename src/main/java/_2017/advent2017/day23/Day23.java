package advent2017.day23;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day23 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day23.class.getSimpleName() + "]");
		Day23 day = new Day23();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run3 algo encore plus direct
	public void run3() {
		int res = hValue();
		System.out.println(res);

	}

	public int hValue() {
		int count = 0;
		for (int i = 109900; i <= 126900; i += 17) {
			if (!isPrimeNumber(i))
				count++;
		}
		return count;
	}

	private static boolean isPrimeNumber(int num) {
		for (int i = 2; i < num; i++) {
			if (num % i == 0)
				return false;
		}
		return true;
	}

	// run2 -- algo direct je trouve 936 au lieu de 913, erreur dans l'input ? un autre algo fonctionnel donne aussi 936 ...
	public void run2() throws IOException {
		int a = 1;
		int b = 109900;
		int c = 126900;
		int d = 0, e = 0, f = 0, g = 0, h = 0;

		while (b != c) {
			f = 1;
			d = 2;

			while (d != b) {
				e = 2;

				while (e != b) {
					if (d * e == b) {
						f = 0;
					}
					e++;
				}
				d++;
			}
			if (f == 0) {
				h++;
			}
			b = b + 17;
			System.out.println("b-c: " + (b - c) + ", h: " + h);
		}

		// g=0, c'est la fin de l'algo
		System.out.println("---------");
		System.out.println("h: " + h);
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "resources/day23.txt";
		List<String> input = Outil.importationString(filename);
		System.out.println(input);

		// creation des registers
		Map<String, Long> registers0 = new HashMap<String, Long>();
		registers0.put("a", 1L);
		registers0.put("b", 0L);
		registers0.put("c", 0L);
		registers0.put("d", 0L);
		registers0.put("e", 0L);
		registers0.put("f", 0L);
		registers0.put("g", 0L);
		registers0.put("h", 0L);

		algo(input, registers0);
	}

	public int algo(List<String> input, Map<String, Long> registers0) {
		int i = 0;
		int cpt = 0;
		boolean go = true;

		while (go) {
			String[] instructions = StringUtils.split(input.get(i), " ");
			String operation = instructions[0];
			String param1 = instructions[1];
			if (!registers0.containsKey(param1) & !isNumeric(param1)) {
				registers0.put(param1, 0L);
			}
			String param2 = instructions[2];

			if (operation.equals("set")) {
				if (isNumeric(param2)) {
					registers0.put(param1, Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param2));
				}
			}

			if (operation.equals("sub")) {
				if (isNumeric(param2)) {
					registers0.put(param1, registers0.get(param1) - Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param1) - registers0.get(param2));
				}
			}

			if (operation.equals("mul")) {
				if (isNumeric(param2)) {
					registers0.put(param1, registers0.get(param1) * Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param1) * registers0.get(param2));
				}
				cpt++;
			}

			if (operation.equals("jnz")) {
				long p1 = 0;
				long p2 = 0;
				if (isNumeric(param1)) {
					p1 = Long.parseLong(param1);
				} else {
					p1 = registers0.get(param1);
				}
				if (isNumeric(param2)) {
					p2 = Long.parseLong(param2);
				} else {
					p2 = registers0.get(param2);
				}
				if (p1 != 0) {
					i = (int) (i + p2 - 1);
				}
			}
			i++;
			// System.out.println("i: "+i+", cpt: "+ cpt);
			System.out.println(registers0);

		}

		// System.out.println(cpt);
		return (cpt);
	}

	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
