package advent2017.day01;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2017.outils.Outil;

public class Day01 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day01.class.getSimpleName() + "]");
		Day01 day = new Day01();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day01.txt";
		List<String> nombres = Outil.importationString(filename);
		System.out.println(nombres);
		String nombres_ = nombres.get(0);

		// gestion de la liste sauf le dernier
		List<Integer> selectionNombres = new ArrayList();
		for (int i = 0; i < nombres_.length() - 1; i++) {
			if (nombres_.charAt(i) == nombres_.charAt(i + 1)) {
				selectionNombres.add(Integer.parseInt("" + nombres_.charAt(i)));
			}
		}

		// gestion du dernier (liste circulaire)
		if (nombres_.charAt(nombres_.length() - 1) == nombres_.charAt(0)) {
			selectionNombres.add(Integer.parseInt("" + nombres_.charAt(nombres_.length() - 1)));
		}

		// calcul de la somme
		System.out.println(Outil.sum(selectionNombres));
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day01.txt";
		List<String> nombres = Outil.importationString(filename);
		System.out.println(nombres);
		String nombres_ = nombres.get(0);
		int step = nombres_.length() / 2;

		// ajout des step premiers à la fin de la liste à cause de la circularité
		for (int i = 0; i <= step; i++) {
			nombres_ = nombres_ + nombres_.charAt(i);
		}

		// gestion de la liste
		List<Integer> selectionNombres = new ArrayList<Integer>();

		for (int i = 0; i < nombres_.length() - step; i++) {
			if (nombres_.charAt(i) == nombres_.charAt(i + step)) {
				selectionNombres.add(Integer.parseInt("" + nombres_.charAt(i)));
			}
		}

		// calcul de la somme
		System.out.println(Outil.sum(selectionNombres));
	}

}
