package advent2017.day25;

public class State {

	private String name;
	private int if1Write;
	private int if0Write;
	private String if1Move;
	private String if0Move;
	private String if1NextState;
	private String if0NextState;
	


	public State(String name, int if1Write, int if0Write, String if1Move, String if0Move, String if1NextState,
			String if0NextState) {
		super();
		this.name = name;
		this.if1Write = if1Write;
		this.if0Write = if0Write;
		this.if1Move = if1Move;
		this.if0Move = if0Move;
		this.if1NextState = if1NextState;
		this.if0NextState = if0NextState;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIf1Write() {
		return if1Write;
	}

	public void setIf1Write(int if1Write) {
		this.if1Write = if1Write;
	}

	public int getIf0Write() {
		return if0Write;
	}

	public void setIf0Write(int if0Write) {
		this.if0Write = if0Write;
	}

	public String getIf1Move() {
		return if1Move;
	}

	public void setIf1Move(String if1Move) {
		this.if1Move = if1Move;
	}

	public String getIf0Move() {
		return if0Move;
	}

	public void setIf0Move(String if0Move) {
		this.if0Move = if0Move;
	}

	public String getIf1NextState() {
		return if1NextState;
	}

	public void setIf1NextState(String if1NextState) {
		this.if1NextState = if1NextState;
	}

	public String getIf0NextState() {
		return if0NextState;
	}

	public void setIf0NextState(String if0NextState) {
		this.if0NextState = if0NextState;
	}


	
	
	
}
