package advent2017.day25;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Day25 {

	// main -----
	public static void main(String[] args) throws IOException  {
		System.out.println("["+Day25.class.getSimpleName()+"]");
		Day25 day = new Day25();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		//day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}


	
	
	// run1 ----
		public void run1() throws IOException {
//	State stateA =new State("A", 0, 1, "L", "R", "B", "B");
//	State stateB =new State("B", 1, 1, "R", "L", "A", "A");
	State stateA =new State("A", 0, 1, "L", "R", "F", "B");
	State stateB =new State("B", 0, 0, "R", "R", "D", "C");
	State stateC =new State("C", 1, 1, "R", "L", "E", "D");
	State stateD =new State("D", 0, 0, "L", "L", "D", "E");
	State stateE =new State("E", 1, 0, "R", "R", "C", "A");
	State stateF =new State("F", 1, 1, "R", "L", "A", "A");
	List<State>states =new ArrayList<State>();
	states.add(stateA);
	states.add(stateB);
	states.add(stateC);
	states.add(stateD);
	states.add(stateE);
	states.add(stateF);
	
			int[] tape= new int[10000000];
			int cursor=5000000;
			State state = stateA;
			
			int cpt1=0;
			for (int step=0;step<12_994_925 ; step++) {
				int value = tape[cursor];
			if (value==0) {
				tape[cursor]=state.getIf0Write();
				if (state.getIf0Move().equals("R")) {
					cursor++;
				}
				if (state.getIf0Move().equals("L")) {
					cursor--;
				}
				String nextStateName=state.getIf0NextState();
				for (State s:states) {
					if (s.getName().equals(nextStateName)) {
						state=s;
					}
				}
			}
			if (value==1) {
				tape[cursor]=state.getIf1Write();
				if (state.getIf1Move().equals("R")) {
					cursor++;
				}
				if (state.getIf1Move().equals("L")) {
					cursor--;
				}
				String nextStateName=state.getIf1NextState();
				for (State s:states) {
					if (s.getName().equals(nextStateName)) {
						state=s;
					}
				}
			}
			//System.out.print("step"+(step+1)+"   " );
//			for (int i=0;i<tape.length;i++) {System.out.print(tape[i]);}
//			System.out.println("");
			}
			
			for (int j=0;j<tape.length;j++) {
				if (tape[j]==1) {
					cpt1++;
				}
			}
			System.out.println(cpt1);
			
		}
	


}
