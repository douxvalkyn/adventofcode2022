package advent2017.day18;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day18 {

	boolean quit0 = false;
	boolean quit1 = false;
	int cpt1 = 0;
	int cpt0 = 0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day18.class.getSimpleName() + "]");
		Day18 day = new Day18();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day18.txt";
		List<String> input = Outil.importationString(filename);
		System.out.println(input);

		// creation des registers
		Map<String, Long> registers0 = new HashMap<String, Long>();
		Map<String, Long> registers1 = new HashMap<String, Long>();
		registers0.put("a", 0L);
		registers0.put("b", 0L);
		registers0.put("f", 0L);
		registers0.put("i", 0L);
		registers1.put("a", 0L);
		registers1.put("b", 0L);
		registers1.put("f", 0L);
		registers1.put("i", 0L);

		registers0.put("p", 0L);
		registers1.put("p", 1L);

		Integer indiceDeb0 = 0;
		Integer indiceDeb1 = 0;
		List<Integer> queue0 = new ArrayList<Integer>();
		List<Integer> queue1 = new ArrayList<Integer>();
		List<List<Integer>> save = new ArrayList<List<Integer>>();

		int step = 0;
		while (step < 100) {
			// param: instructions, registres du prog, num du prog, indice où debuter la lecture
			step++;
			save = algo(input, registers0, 0, indiceDeb0, queue0, queue1);
			indiceDeb0 = save.get(0).get(0);
			queue0 = save.get(1);
			queue1 = save.get(2);
			save = algo(input, registers1, 1, indiceDeb1, queue0, queue1);
			indiceDeb1 = save.get(0).get(0);
			queue0 = save.get(1);
			queue1 = save.get(2);
		}

		System.out.println(("prog 1 sent: " + cpt1));
		System.out.println(("prog 0 sent: " + cpt0));
		System.out.println((registers0));
		System.out.println((registers1));
	}

	public List<List<Integer>> algo(List<String> input, Map<String, Long> registers0, int numProg, Integer indiceDeb0, List<Integer> queue0, List<Integer> queue1) {
		int i = indiceDeb0.intValue();
		boolean go = true;

		while (go) {
			String[] instructions = StringUtils.split(input.get(i), " ");
			String operation = instructions[0];
			String param1 = instructions[1];
			if (!registers0.containsKey(param1) & !isNumeric(param1)) {
				registers0.put(param1, 0L);
			}
			String param2 = "";
			if (!operation.equals("snd") & !operation.equals("rcv")) {
				param2 = instructions[2];
			}

			if (operation.equals("snd")) {
				int p = 0;
				if (isNumeric(param1)) {
					p = Integer.parseInt(param1);
				} else {
					p = registers0.get(param1).intValue();
				}
				if (numProg == 0) {
					queue1.add(p);
					cpt0++;
				}
				if (numProg == 1) {
					queue0.add(p);
					cpt1++;
				}
			}

			if (operation.equals("set")) {
				if (isNumeric(param2)) {
					registers0.put(param1, Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param2));
				}
			}

			if (operation.equals("add")) {
				if (isNumeric(param2)) {
					registers0.put(param1, registers0.get(param1) + Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param1) + registers0.get(param2));
				}
			}

			if (operation.equals("mul")) {
				if (isNumeric(param2)) {
					registers0.put(param1, registers0.get(param1) * Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param1) * registers0.get(param2));
				}
			}

			if (operation.equals("mod")) {
				if (isNumeric(param2)) {
					registers0.put(param1, registers0.get(param1) % Long.parseLong(param2));
				} else {
					registers0.put(param1, registers0.get(param1) % registers0.get(param2));
				}
			}

			if (operation.equals("rcv")) {
				long tmp = 0;
				if (numProg == 1) {
					if (queue1.size() == 0) {
						go = false;
						quit0 = true;
					}
					if (queue1.size() != 0) {
						tmp = queue1.get(0);
						queue1.remove(0);
						registers0.put(param1, tmp);
					}
				}
				if (numProg == 0) {
					if (queue0.size() == 0) {
						go = false;
						quit1 = true;
					}
					if (queue0.size() != 0) {
						tmp = queue0.get(0);
						queue0.remove(0);
						registers0.put(param1, tmp);
					}
				}

			}
			if (operation.equals("jgz")) {
				long p1 = 0;
				long p2 = 0;
				if (isNumeric(param1)) {
					p1 = Long.parseLong(param1);
				} else {
					p1 = registers0.get(param1);
				}
				if (isNumeric(param2)) {
					p2 = Long.parseLong(param2);
				} else {
					p2 = registers0.get(param2);
				}
				if (p1 > 0) {
					i = (int) (i + p2 - 1);
				}
			}
			i++;
		}

		List<List<Integer>> save = new ArrayList<List<Integer>>();
		List<Integer> listeI = new ArrayList<Integer>();
		listeI.add((i - 1));
		save.add(listeI);
		save.add(queue0);
		save.add(queue1);

		return (save);
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day18.txt";
		List<String> input = Outil.importationString(filename);
		System.out.println(input);

		// creation des registers
		Map<String, Integer> registers = new HashMap<String, Integer>();
		Integer sound = null;

		// algo(input, registers, sound);

	}

	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}
