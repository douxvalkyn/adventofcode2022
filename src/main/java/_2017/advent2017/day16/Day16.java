package advent2017.day16;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day16 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day16.class.getSimpleName() + "]");
		Day16 day = new Day16();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {

		// L'idée est de trouver combien de steps sont necessaires pour retomber sur l'input initial
		// Il y a une répétition tous les 30 steps
		// Donc on peut zapper bcp de steps avant d'arriver à proximité du milliard
		// 33 333 333*30=999 999 990; Il restera donc 10 steps pour atteindre le milliard !

		String programmes = "abcdefghijklmnop";
		String programmes_initial = "abcdefghijklmnop";
		// String programmes = "abcde";
		System.out.println(programmes);

		// String input = "s1,x3/4,pe/b";
		String filename = "src/main/resources/advent2017/Day16.txt";
		List<String> input_ = Outil.importationString(filename);
		String input = "";
		for (String str : input_) {
			input = input + str;
		}

		for (int step = 0; step < 10; step++) {
			String[] danse = StringUtils.split(input, ",");
			for (String mvt : danse) {
				if (mvt.charAt(0) == 's') { // spin
					int x = Integer.parseInt(StringUtils.substring(mvt, 1, mvt.length()));
					String part1 = StringUtils.substring(programmes, -1 * x);
					String part2 = StringUtils.substring(programmes, 0, programmes.length() - x);
					programmes = part1 + part2;
				}
				if (mvt.charAt(0) == 'x') { // exchange positions
					int posA = Integer.parseInt(StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[0]);
					int posB = Integer.parseInt(StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[1]);
					char[] prog = programmes.toCharArray();
					char temp = prog[posA];
					prog[posA] = prog[posB];
					prog[posB] = temp;
					programmes = new String(prog);
				}
				if (mvt.charAt(0) == 'p') { // partner
					String A = StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[0];
					String B = StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[1];
					int posA = programmes.indexOf(A);
					int posB = programmes.indexOf(B);
					char[] prog = programmes.toCharArray();
					char temp = prog[posA];
					prog[posA] = prog[posB];
					prog[posB] = temp;
					programmes = new String(prog);
				}
			}
			System.out.println("step: " + (step + 1) + " > " + programmes);
			if (programmes.equals(programmes_initial)) {
				System.out.println("step: " + (step + 1) + " > " + programmes);
			}
		}
		System.out.println(programmes);
	}

	// run1 ----
	public void run1() throws IOException {

		String programmes = "abcdefghijklmnop";
		// String programmes = "abcde";
		System.out.println(programmes);

		// String input = "s1,x3/4,pe/b";
		String filename = "src/main/resources/advent2017/Day16.txt";
		List<String> input_ = Outil.importationString(filename);
		String input = "";
		for (String str : input_) {
			input = input + str;

		}

		String[] danse = StringUtils.split(input, ",");
		for (String mvt : danse) {
			if (mvt.charAt(0) == 's') { // spin
				int x = Integer.parseInt(StringUtils.substring(mvt, 1, mvt.length()));
				String part1 = StringUtils.substring(programmes, -1 * x);
				String part2 = StringUtils.substring(programmes, 0, programmes.length() - x);
				programmes = part1 + part2;
			}
			if (mvt.charAt(0) == 'x') { // exchange positions
				int posA = Integer.parseInt(StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[0]);
				int posB = Integer.parseInt(StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[1]);
				char[] prog = programmes.toCharArray();
				char temp = prog[posA];
				prog[posA] = prog[posB];
				prog[posB] = temp;
				programmes = new String(prog);
			}
			if (mvt.charAt(0) == 'p') { // partner
				String A = StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[0];
				String B = StringUtils.split(StringUtils.substring(mvt, 1, mvt.length()), "/")[1];
				int posA = programmes.indexOf(A);
				int posB = programmes.indexOf(B);
				char[] prog = programmes.toCharArray();
				char temp = prog[posA];
				prog[posA] = prog[posB];
				prog[posB] = temp;
				programmes = new String(prog);
			}
		}
		System.out.println(programmes);

	}

}
