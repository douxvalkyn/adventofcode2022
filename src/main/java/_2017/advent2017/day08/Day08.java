package advent2017.day08;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day08 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day08.class.getSimpleName() + "]");
		Day08 day = new Day08();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		// day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day08.txt";
		List<String> lignes = Outil.importationString(filename);
		System.out.println(lignes);

		// premiere lecture du fichier pour reperer et creer tous les registres
		Map<String, Integer> registers = new HashMap<>();
		creationRegistres(lignes, registers);
		System.out.println(registers);

		// deuxieme lecture du fichier pour effectuer les instructions dans l'ordre
		effectueInstructions(lignes, registers);

		// recherche du max
		Integer max = maxUsingStreamAndLambda(registers);
		System.out.println("max: " + max);
	}

	public <K, V extends Comparable<V>> V maxUsingStreamAndLambda(Map<K, V> map) {
		Optional<Entry<K, V>> maxEntry = map.entrySet().stream().max((Entry<K, V> e1, Entry<K, V> e2) -> e1.getValue().compareTo(e2.getValue()));

		return maxEntry.get().getValue();
	}

	private void effectueInstructions(List<String> lignes, Map<String, Integer> registers) {
		int nouvelleValeurMax = 0;

		for (String ligne : lignes) {
			String name = StringUtils.split(ligne, " ")[0];
			String operation = StringUtils.split(ligne, " ")[1];
			int valeurAjouteeOuSoustraite = Integer.parseInt(StringUtils.split(ligne, " ")[2]);
			String nomDuRegistreA = StringUtils.split(ligne, " ")[4];
			String operateurComparaison = StringUtils.split(ligne, " ")[5];
			int valB = Integer.parseInt(StringUtils.split(ligne, " ")[6]);

			if (operation.equals("dec")) {
				valeurAjouteeOuSoustraite = -1 * valeurAjouteeOuSoustraite;
			}

			// Test
			Integer valRegA = registers.get(nomDuRegistreA);

			boolean test = false;
			switch (operateurComparaison) {
				case ">":
					if (valRegA > valB) {
						test = true;
					} else {
						test = false;
					}
					break;
				case "<":
					if (valRegA < valB) {
						test = true;
					} else {
						test = false;
					}
					break;
				case "==":
					if (valRegA == valB) {
						test = true;
					} else {
						test = false;
					}
					break;
				case "!=":
					if (valRegA != valB) {
						test = true;
					} else {
						test = false;
					}
					break;

				case ">=":
					if (valRegA >= valB) {
						test = true;
					} else {
						test = false;
					}
					break;
				case "<=":
					if (valRegA <= valB) {
						test = true;
					} else {
						test = false;
					}
					break;
				default:
					System.out.println("erreur dans le switch case");
			} // fin switch

			// appliquer le résultat du test
			if (test) {
				int nouvelleValeur = registers.get(name) + valeurAjouteeOuSoustraite;
				if (nouvelleValeur > nouvelleValeurMax) {
					nouvelleValeurMax = nouvelleValeur;
				}
				registers.put(name, nouvelleValeur);
			}

		}
		System.out.println(registers);
		System.out.println("max atteint in any register: " + nouvelleValeurMax);

	}

	private void creationRegistres(List<String> lignes, Map<String, Integer> registers) {
		for (String ligne : lignes) {
			String name = StringUtils.split(ligne, " ")[0];
			if (!registers.containsKey(name)) {
				registers.put(name, 0);
			}

		}

	}

}
