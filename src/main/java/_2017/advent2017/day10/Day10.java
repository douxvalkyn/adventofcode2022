package advent2017.day10;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.CircularLinkedList;

public class Day10 {

	// int currentPosition=0;
	// int skipSize=0;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day10.class.getSimpleName() + "]");
		Day10 day = new Day10();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		String res = day.fastHash("1,2,4");
		System.out.println(res);
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		CircularLinkedList cll = new CircularLinkedList();
		for (int i = 0; i < 256; i++) {
			cll.addNode(i);
		}

		String inputLengthString = "130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224";
		inputLengthString = "1,2,4";
		// inputLengthString="AoC 2017";
		// inputLengthString="";

		String res = knotHash(cll, inputLengthString);
		System.out.println(res);

		// 826e0bf21098d3cf2581bd0591eb2bcc
		// 3960835bcdc130f0b66d7ff4f6a5a8e
		// 3960835bcdc130f0b66d7ff4f6a5a8e
	}

	public String knotHash(CircularLinkedList cll, String inputLengthString) {
		List<Integer> inputLength = new ArrayList();
		for (int i = 0; i < inputLengthString.length(); i++) {
			char character = inputLengthString.charAt(i);
			int ascii = (int) character;
			inputLength.add(ascii);
		}

		// ajout de la sequence: 17, 31, 73, 47, 23
		inputLength.add(17);
		inputLength.add(31);
		inputLength.add(73);
		inputLength.add(47);
		inputLength.add(23);

		int[] retour = new int[2];
		retour[0] = 0;
		retour[1] = 0;
		for (int round = 0; round < 64; round++) { // faire 64 iterations
			// System.out.println("etape: "+ round +"/64");
			retour = knotHashSingleRound2(cll, inputLength, retour[0], retour[1]);

		}

		String resHex = null;
		String resHex32 = "";
		// calcul 16 par 16 des blocks:
		for (int j = 0; j < 16; j++) {
			List<Integer> maListe = new ArrayList();
			for (int i = j * 16; i < (j + 1) * 16; i++) {
				int res = 0;
				resHex = null;
				int valeur = cll.getNode(i);
				maListe.add(valeur);
			}
			// calcul xor
			int resXOR = maListe.get(0) ^ maListe.get(1) ^ maListe.get(2) ^ maListe.get(3) ^ maListe.get(4) ^ maListe.get(5) ^ maListe.get(6) ^ maListe.get(7) ^ maListe.get(8) ^ maListe.get(9)
				^ maListe.get(10) ^ maListe.get(11) ^ maListe.get(12) ^ maListe.get(13) ^ maListe.get(14) ^ maListe.get(15);
			resHex = Integer.toHexString(resXOR);
			if (resHex.length() == 2) {
				// System.out.print(resHex);
				resHex32 = resHex32 + resHex;
			}
			if (resHex.length() == 1) {
				// System.out.print("0"+resHex);
				resHex32 = resHex32 + "0" + resHex;
			}
		}
		return resHex32;
	}

	public int[] knotHashSingleRound2(CircularLinkedList cll, List<Integer> inputLength, int currentPosition, int skipSize) {
		int[] retour = new int[2];
		for (int index = 0; index < inputLength.size(); index++) {

			// selection de la longueur
			LinkedList<Integer> selection = new LinkedList<>();
			for (int i = currentPosition; i < (currentPosition + inputLength.get(index)); i++) {
				int res = cll.getNode(i);
				selection.add(cll.getNode(i));
			}

			Collections.reverse(selection); // on inverse la selection
			// System.out.println("selection inv:"+selection);

			// on replace la selection inversee
			for (int i = currentPosition; i < (currentPosition + inputLength.get(index)); i++) {
				cll.setNode(i, selection.get(i - currentPosition));
			}

			// move currentPosition
			currentPosition = currentPosition + inputLength.get(index) + skipSize;

			// increase skipSize
			skipSize++;

		}
		retour[0] = currentPosition;
		retour[1] = skipSize;

		// cll.traverseList();
		// System.out.println("----------");
		return retour;
	}

	// run1 ----
	public void run1() throws IOException {
		CircularLinkedList cll = new CircularLinkedList();
		// cll.addNode(0); cll.addNode(1); cll.addNode(2); cll.addNode(3); cll.addNode(4);
		// String inputLengthString="3, 4, 1, 5";

		for (int i = 0; i < 256; i++) {
			cll.addNode(i);
		}
		// cll.traverseList();
		String inputLengthString = "130,126,1,11,140,2,255,207,18,254,246,164,29,104,0,224";
		String[] inputLengthStringArray = StringUtils.split(inputLengthString, ", ");
		List<String> inputLengthStringList = Arrays.asList(inputLengthStringArray);
		List<Integer> inputLength = inputLengthStringList.stream().map(Integer::parseInt).collect(Collectors.toList());
		// cll.traverseList();

		int currentPosition = 0;
		int skipSize = 0;

		knotHashSingleRound(cll, inputLength, currentPosition, skipSize);
	}

	public void knotHashSingleRound(CircularLinkedList cll, List<Integer> inputLength, int currentPosition, int skipSize) {
		for (int index = 0; index < inputLength.size(); index++) {
			// selection de la longueur
			LinkedList<Integer> selection = new LinkedList<>();
			for (int i = currentPosition; i < (currentPosition + inputLength.get(index)); i++) {
				int res = cll.getNode(i);
				selection.add(cll.getNode(i));
			}
			Collections.reverse(selection); // on inverse la selection
			// System.out.println("selection inv:"+selection);

			// on replace la selection inversee
			for (int i = currentPosition; i < (currentPosition + inputLength.get(index)); i++) {
				cll.setNode(i, selection.get(i - currentPosition));
			}

			// move currentPosition
			currentPosition = currentPosition + inputLength.get(index) + skipSize;

			// increase skipSize
			skipSize++;

		}
		// cll.traverseList();
		// System.out.println("----------");
	}

	public String fastHash(String string) {
		List<Integer> sparseHash = IntStream.range(0, 256).boxed().collect(Collectors.toList());
		// String inputString = "1,2,4";
		byte[] bytes = string.getBytes();

		List<Integer> lengths = IntStream.concat(IntStream.range(0, bytes.length).map(i -> bytes[i]), Arrays.asList(17, 31, 73, 47, 23).stream().mapToInt(Integer::intValue)).boxed()
			.collect(Collectors.toList());

		int current = 0, skip = 0;
		for (int round = 0; round < 64; round++) {
			for (int length : lengths) {
				List<Integer> segment = new ArrayList<>();
				for (int i = current; i < current + length; i++)
					segment.add(sparseHash.get(i % sparseHash.size()));
				Collections.reverse(segment);
				for (int i = 0; i < segment.size(); i++)
					sparseHash.set((i + current) % sparseHash.size(), segment.get(i));
				current += length + skip++;
			}
		}

		final int size = sparseHash.size();
		List<String> denseHash = IntStream.range(0, (size + 15) >> 4).mapToObj(i -> sparseHash.subList(i << 4, Math.min((i + 1) << 4, size)).stream().reduce((a, b) -> a ^ b).orElse(0))
			.map(i -> String.format("%02X", i).toLowerCase()).collect(Collectors.toList());

		String res = "";
		for (String l : denseHash)
			res = res + l;

		return res;
	}

}
