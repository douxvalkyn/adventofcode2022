package advent2017.day04;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day04 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day04.class.getSimpleName() + "]");
		Day04 day = new Day04();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day04.txt";
		List<String> lignes = Outil.importationString(filename);
		int cptValide = 0;
		for (String ligne : lignes) {
			String[] mots = StringUtils.split(ligne);
			boolean duplicates = false;
			for (int j = 0; j < mots.length; j++)
				for (int k = j + 1; k < mots.length; k++)
					if (isAnagramSort(mots[k], mots[j])) {
						duplicates = true;
					}
			if (!duplicates) {
				cptValide++;
			}

		}
		System.out.println("nb lignes correctes: " + cptValide);
	}

	boolean isAnagramSort(String string1, String string2) {
		if (string1.length() != string2.length()) {
			return false;
		}
		char[] a1 = string1.toCharArray();
		char[] a2 = string2.toCharArray();
		Arrays.sort(a1);
		Arrays.sort(a2);
		return Arrays.equals(a1, a2);
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day04.txt";
		List<String> lignes = Outil.importationString(filename);
		int cptValide = 0;
		for (String ligne : lignes) {
			String[] mots = StringUtils.split(ligne);
			boolean duplicates = false;
			for (int j = 0; j < mots.length; j++)
				for (int k = j + 1; k < mots.length; k++)
					if (k != j && mots[k].equals(mots[j])) {
						duplicates = true;
					}
			if (!duplicates) {
				cptValide++;
			}

		}
		System.out.println("nb lignes correctes: " + cptValide);
	}

}
