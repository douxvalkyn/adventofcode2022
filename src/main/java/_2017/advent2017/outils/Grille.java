package advent2017.outils;

import java.util.ArrayList;
import java.util.List;

/**
 * Grille: Grille (4,2): [ a | b | c | d ] ----> X [ e | f | g | h ] | | V y
 */

public class Grille {

	private int nbCol;
	private int nbLignes;
	private List<Case> cases;

	public Grille(int nbCol, int nbLignes) {
		super();
		this.nbCol = nbCol;
		this.nbLignes = nbLignes;
		// ajout des cases vierges
		List<Case> cases = new ArrayList<>();
		int index = 0;
		for (int j = 0; j < nbLignes; j++) {
			for (int i = 0; i < nbCol; i++) {
				Case c = new Case(i, j);
				c.setRang(index);
				index++;
				c.setX(i);
				c.setY(j);
				cases.add(c);
			}
		}
		this.cases = cases;
	}

	public int getNbCol() {
		return nbCol;
	}

	public void setNbCol(int nbCol) {
		this.nbCol = nbCol;
	}

	public int getNbLignes() {
		return nbLignes;
	}

	public void setNbLignes(int nbLignes) {
		this.nbLignes = nbLignes;
	}

	public List<Case> getCases() {
		return cases;
	}

	public void setCases(List<Case> cases) {
		this.cases = cases;
	}

	@Override
	public String toString() {
		return "Grille [" + cases + "]";
	}

	public void affichageEtat() {
		String[][] matrice = new String[this.nbLignes][this.nbCol];
		int index = 0;
		for (int y = 0; y < this.nbLignes; y++) {
			for (int x = 0; x < this.nbCol; x++) {
				matrice[y][x] = this.cases.get(index).getEtat() + "";
				index++;
				System.out.print(matrice[y][x] + "");
			}
			System.out.println();
		}
	}

	public void affichageEtatRestreint(int a, int b) {
		String[][] matrice = new String[a][b];
		int index = 0;
		for (int y = 0; y < a; y++) {
			for (int x = 0; x < b; x++) {
				matrice[y][x] = this.cases.get(index).getEtat() + "";
				index++;
				System.out.print(matrice[y][x] + "  ");
			}
			System.out.println();
		}
	}

	public void affichageValeur() {
		String[][] matrice = new String[this.nbLignes][this.nbCol];
		int index = 0;
		for (int y = 0; y < this.nbLignes; y++) {
			for (int x = 0; x < this.nbCol; x++) {
				matrice[y][x] = this.cases.get(index).getValeur() + "";
				index++;
				System.out.print(matrice[y][x] + "  ");
			}
			System.out.println();
		}
	}

	/**
	 * @param coordonnee x
	 * @param coordonnee y
	 * @return la Case
	 */
	public Case getCase(int x, int y) {
		Case CaseResultat = null;
		for (Case c : this.getCases()) {
			if ((c.getX() == x) & (c.getY() == y)) {
				CaseResultat = c;
			}
		}
		return CaseResultat;
	}

	public Grille flipVertical() {
		Grille grilleNew = new Grille(this.getNbCol(), this.getNbLignes());
		for (int i = 0; i < this.getNbLignes(); i++) {
			for (int j = this.getNbCol() - 1; j >= 0; j--) {
				Case cell = this.getCase(j, i);
				grilleNew.getCase(this.getNbLignes() - 1 - j, i).setEtat(cell.getEtat());
			}
		}
		return grilleNew;
	}

	public Grille flipHorizontal() {
		Grille grilleNew = new Grille(this.getNbCol(), this.getNbLignes());
		for (int i = this.getNbLignes() - 1; i >= 0; i--) {
			for (int j = 0; j < this.getNbCol(); j++) {
				Case cell = this.getCase(j, i);
				grilleNew.getCase(j, this.getNbLignes() - 1 - i).setEtat(cell.getEtat());
			}
		}
		return grilleNew;
	}

	public Grille rotate() {
		Grille grilleNew = new Grille(this.getNbCol(), this.getNbLignes());
		for (int i = 0; i < this.getNbCol(); i++) {
			for (int j = this.getNbCol() - 1; j >= 0; j--) {
				Case cell = this.getCase(i, j);
				grilleNew.getCase(this.getNbCol() - 1 - j, i).setEtat(cell.getEtat());
			}
		}
		return grilleNew;
	}
}
