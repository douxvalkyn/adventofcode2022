package advent2017.outils;

public class Node {

	int value;
	Node nextNode;

	public Node(int value) {
		this.value = value;
	}
}
