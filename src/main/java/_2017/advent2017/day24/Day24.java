package advent2017.day24;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day24 {

	List<List<Domino>> liste = new ArrayList<List<Domino>>();

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day24.class.getSimpleName() + "]");
		Day24 day = new Day24();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {

		// lecture input & creation dominos --
		String filename = "src/main/resources/advent2017/day24.txt";
		List<String> input = Outil.importationString(filename);
		List<Domino> dominos = new ArrayList<Domino>();
		creationDominos(input, dominos);
		// System.out.println(dominos);

		// creation toutes les combinaisons.

		for (Domino domino : dominos) {
			if (domino.getNumero1() == 0 | domino.getNumero2() == 0) {
				List<Domino> suiteDeDominos = new ArrayList<Domino>();
				if (domino.getNumero2() == 0) {
					domino.tourne();
				}
				suiteDeDominos.add(domino);
				// creer une nvlle liste des dominos restants, sans celui qu'on vient d'ajouter
				int index = dominos.indexOf(domino);
				List<Domino> dominosRestants = new ArrayList<Domino>();
				for (int i = 0; i < dominos.size(); i++) {
					if (i != index) {
						int n1 = dominos.get(i).getNumero1();
						int n2 = dominos.get(i).getNumero2();
						Domino dom = new Domino(n1, n2);
						dominosRestants.add(dom);
					}
				}
				// System.out.println(suiteDeDominos);
				liste.add(suiteDeDominos);
				ajouteDominos(dominosRestants, suiteDeDominos);

			}
		}

		System.out.println("nb de listes: " + liste.size());

		// selection des plus longs chemins
		int longueurMax = 0;
		List<List<Domino>> listeLongueurMax = new ArrayList<List<Domino>>();
		for (List<Domino> l : liste) {
			if (l.size() > longueurMax) {
				longueurMax = l.size();
			}
		}
		for (List<Domino> l : liste) {
			if (l.size() == longueurMax) {
				listeLongueurMax.add(l);
			}
		}
		System.out.println("longueurMax " + longueurMax);
		// calcul de la force
		int max = 0;
		List<Domino> listeMax = new ArrayList<Domino>();
		int cpt = 0;
		for (List<Domino> l : listeLongueurMax) {
			cpt++;
			int strength = 0;
			for (Domino d : l) {
				strength = strength + d.getNumero1() + d.getNumero2();
			}
			if (strength > max) {
				max = strength;
				listeMax = l;
			}
		}

		System.out.println(listeMax + " : " + max);

	}

	// run1 ----
	public void run1() throws IOException {

		// lecture input & creation dominos --
		String filename = "src/main/resources/advent2017/day24.txt";
		List<String> input = Outil.importationString(filename);
		List<Domino> dominos = new ArrayList<Domino>();
		creationDominos(input, dominos);
		// System.out.println(dominos);

		// creation toutes les combinaisons.

		for (Domino domino : dominos) {
			if (domino.getNumero1() == 0 | domino.getNumero2() == 0) {
				List<Domino> suiteDeDominos = new ArrayList<Domino>();
				if (domino.getNumero2() == 0) {
					domino.tourne();
				}
				suiteDeDominos.add(domino);
				// creer une nvlle liste des dominos restants, sans celui qu'on vient d'ajouter
				int index = dominos.indexOf(domino);
				List<Domino> dominosRestants = new ArrayList<Domino>();
				for (int i = 0; i < dominos.size(); i++) {
					if (i != index) {
						int n1 = dominos.get(i).getNumero1();
						int n2 = dominos.get(i).getNumero2();
						Domino dom = new Domino(n1, n2);
						dominosRestants.add(dom);
					}
				}
				System.out.println(suiteDeDominos);
				liste.add(suiteDeDominos);
				ajouteDominos(dominosRestants, suiteDeDominos);

			}
		}

		System.out.println(liste.size());
		// calcul de la force
		int max = 0;
		List<Domino> listeMax = new ArrayList<Domino>();
		int cpt = 0;
		for (List<Domino> l : liste) {
			cpt++;
			int strength = 0;
			for (Domino d : l) {
				strength = strength + d.getNumero1() + d.getNumero2();
			}
			if (strength > max) {
				max = strength;
				listeMax = l;
			}
		}
		System.out.println("MAX: " + listeMax + " : " + max);

	}

	private void ajouteDominos(List<Domino> dominos, List<Domino> suiteDeDominos) {

		for (Domino dom : dominos) {
			Domino lastDomino = suiteDeDominos.get(suiteDeDominos.size() - 1);
			int num2 = lastDomino.getNumero2(); // numero a completer
			if (dom.getNumero1() == num2 | dom.getNumero2() == num2) {
				if (dom.getNumero2() == num2) {
					dom.tourne();
				}

				// creer une nvlle suite des dominos
				List<Domino> suiteDeDominos2 = new ArrayList<Domino>();
				for (int i = 0; i < suiteDeDominos.size(); i++) {
					int n1 = suiteDeDominos.get(i).getNumero1();
					int n2 = suiteDeDominos.get(i).getNumero2();
					Domino domi = new Domino(n1, n2);
					suiteDeDominos2.add(domi);
				}
				suiteDeDominos2.add(dom);
				// creer une nvlle liste des dominos restants, sans celui qu'on vient d'ajouter
				int index = dominos.indexOf(dom);
				List<Domino> dominosRestants = new ArrayList<Domino>();
				for (int i = 0; i < dominos.size(); i++) {
					if (i != index) {
						int n1 = dominos.get(i).getNumero1();
						int n2 = dominos.get(i).getNumero2();
						Domino domi = new Domino(n1, n2);
						dominosRestants.add(domi);
					}
				}

				// System.out.println(suiteDeDominos2);
				liste.add(suiteDeDominos2);
				ajouteDominos(dominosRestants, suiteDeDominos2);

			}

		}

	}

	private void creationDominos(List<String> input, List<Domino> dominos) {
		for (String dominoString : input) {
			int a = Integer.parseInt(StringUtils.split(dominoString, "/")[0]);
			int b = Integer.parseInt(StringUtils.split(dominoString, "/")[1]);
			Domino domino = new Domino(a, b);
			dominos.add(domino);
		}

	}

}
