package advent2017.day24;

public class Domino {
	
	
private int numero1;
private int numero2;
public int getNumero1() {
	return numero1;
}
public void setNumero1(int numero1) {
	this.numero1 = numero1;
}
public int getNumero2() {
	return numero2;
}
public void setNumero2(int numero2) {
	this.numero2 = numero2;
}
public Domino(int numero1, int numero2) {
	super();
	this.numero1 = numero1;
	this.numero2 = numero2;
}
@Override
public String toString() {
	return "Domino ["+numero1 +"/"+ numero2 + "]";
}

public void tourne() {
int old_num1 = this.getNumero1();
int old_num2 = this.getNumero2();
this.setNumero1(old_num2);
this.setNumero2(old_num1);
}


}
