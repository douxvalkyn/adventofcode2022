package advent2017.day22;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2017.outils.Grille;
import advent2017.outils.Outil;

public class Day22 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day22.class.getSimpleName() + "]");
		Day22 day = new Day22();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----

	// Clean (avec virus present): . !
	// Weakened (avec virus present): w W
	// Infected (avec virus present): # *
	// Flagged (avec virus present): f F

	public void run2() throws IOException {

		// lecture input & creation grille--
		String filename = "src/main/resources/advent2017/Day22.txt";
		List<String> input = Outil.importationString(filename);
		int taille = 500;
		Grille grille = new Grille(taille, taille);
		int xInit = (taille / 2) - (input.size() / 2) - 1;
		int yInit = (taille / 2) - (input.size() / 2);
		creationGrille(input, grille, xInit, yInit);

		// position currentNode
		int x = xInit + input.size() / 2;
		int y = yInit + input.size() / 2;
		if (grille.getCase(x, y).getEtat().equals(".")) {
			grille.getCase(x, y).setEtat("!");
		}
		if (grille.getCase(x, y).getEtat().equals("#")) {
			grille.getCase(x, y).setEtat("*");
		}
		String orientation = "N";
		// grille.affichageEtat();

		int cpt = 0;
		for (int step = 0; step < 10_000_000; step++) {
			if (step % 100_000 == 0) {
				System.out.println(step);
			}
			// System.out.println("step "+(step+1));
			if (grille.getCase(x, y).getEtat().equals("!")) { // si currentNode est CLEAN
				switch (orientation) {
					case "N":
						orientation = "O";
						break;
					case "O":
						orientation = "S";
						break;
					case "S":
						orientation = "E";
						break;
					case "E":
						orientation = "N";
						break;
					default:
						break;
				}
				grille.getCase(x, y).setEtat("w"); // currentNode devient weakened
			}

			if (grille.getCase(x, y).getEtat().equals("W")) { // si currentNode est WEAKENED

				grille.getCase(x, y).setEtat("#"); // currentNode devient infecté.
				cpt++;
			}

			if (grille.getCase(x, y).getEtat().equals("*")) { // si currentNode est INFECTED
				switch (orientation) {
					case "N":
						orientation = "E";
						break;
					case "O":
						orientation = "N";
						break;
					case "S":
						orientation = "O";
						break;
					case "E":
						orientation = "S";
						break;
					default:
						break;
				}
				grille.getCase(x, y).setEtat("f"); // currentNode devient Flagged

			}

			if (grille.getCase(x, y).getEtat().equals("F")) { // si currentNode est Flagged
				switch (orientation) {
					case "N":
						orientation = "S";
						break;
					case "O":
						orientation = "E";
						break;
					case "S":
						orientation = "N";
						break;
					case "E":
						orientation = "O";
						break;
					default:
						break;
				}
				grille.getCase(x, y).setEtat("."); // currentNode devient clean

			}

			// deplacement du virus
			if (orientation.equals("N")) {
				y = y - 1;
			}
			if (orientation.equals("O")) {
				x = x - 1;
			}
			if (orientation.equals("S")) {
				y = y + 1;
			}
			if (orientation.equals("E")) {
				x = x + 1;
			}
			if (grille.getCase(x, y).getEtat().equals(".")) {
				grille.getCase(x, y).setEtat("!");
			}
			if (grille.getCase(x, y).getEtat().equals("#")) {
				grille.getCase(x, y).setEtat("*");
			}
			if (grille.getCase(x, y).getEtat().equals("w")) {
				grille.getCase(x, y).setEtat("W");
			}
			if (grille.getCase(x, y).getEtat().equals("f")) {
				grille.getCase(x, y).setEtat("F");
			}

			// affichage
			// grille.affichageEtat();
		}
		System.out.println("nb cases infectees: " + cpt);
	}

	// run1 ----
	public void run1() throws IOException {

		// lecture input & creation grille--
		String filename = "src/main/resources/advent2017/Day22.txt";
		List<String> input = Outil.importationString(filename);
		int taille = 100;
		Grille grille = new Grille(taille, taille);
		int xInit = (taille / 2) - (input.size() / 2) - 1;
		int yInit = (taille / 2) - (input.size() / 2);
		creationGrille(input, grille, xInit, yInit);

		// position currentNode
		int x = xInit + input.size() / 2;
		int y = yInit + input.size() / 2;
		if (grille.getCase(x, y).getEtat().equals(".")) {
			grille.getCase(x, y).setEtat("!");
		}
		if (grille.getCase(x, y).getEtat().equals("#")) {
			grille.getCase(x, y).setEtat("*");
		}
		String orientation = "N";
		// grille.affichageEtat();

		int cpt = 0;
		for (int step = 0; step < 10000; step++) {
			System.out.println("step " + (step + 1));
			if (grille.getCase(x, y).getEtat().equals("*")) { // si currentNode est infectée
				switch (orientation) {
					case "N":
						orientation = "E";
						break;
					case "O":
						orientation = "N";
						break;
					case "S":
						orientation = "O";
						break;
					case "E":
						orientation = "S";
						break;
					default:
						break;
				}
				grille.getCase(x, y).setEtat("."); // currentNode n'est plus infecté.

			}
			if (grille.getCase(x, y).getEtat().equals("!")) { // si currentNode n'est pas infectée
				switch (orientation) {
					case "N":
						orientation = "O";
						break;
					case "O":
						orientation = "S";
						break;
					case "S":
						orientation = "E";
						break;
					case "E":
						orientation = "N";
						break;
					default:
						break;
				}
				grille.getCase(x, y).setEtat("#"); // currentNode devient infecté.
				cpt++;
			}

			// deplacement du virus
			if (orientation.equals("N")) {
				y = y - 1;
			}
			if (orientation.equals("O")) {
				x = x - 1;
			}
			if (orientation.equals("S")) {
				y = y + 1;
			}
			if (orientation.equals("E")) {
				x = x + 1;
			}
			if (grille.getCase(x, y).getEtat().equals(".")) {
				grille.getCase(x, y).setEtat("!");
			}
			if (grille.getCase(x, y).getEtat().equals("#")) {
				grille.getCase(x, y).setEtat("*");
			}

			// affichage
			// grille.affichageEtat();
		}
		System.out.println("nb cases infectees: " + cpt);
	}

	private void creationGrille(List<String> input, Grille grille, int xInit, int yInit) {
		// mettre des . partout
		for (int j = 0; j < grille.getNbCol(); j++) {
			for (int i = 0; i < grille.getNbLignes(); i++) {
				grille.getCase(i, j).setEtat(".");
			}
		}

		// creer la grille
		for (int j = 0; j < input.size(); j++) {
			String ligne = input.get(j);
			for (int i = 0; i < ligne.length(); i++) {
				char carac = ligne.charAt(i);
				grille.getCase(i + xInit, j + yInit).setEtat("" + carac);
			}
		}

	}

}
