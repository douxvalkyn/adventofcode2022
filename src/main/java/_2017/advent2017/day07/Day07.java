package advent2017.day07;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day07 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day07.class.getSimpleName() + "]");
		Day07 day = new Day07();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day07.txt";
		List<String> lignes = Outil.importationString(filename);

		List<Prog> progs = new ArrayList<Prog>();
		for (String ligne : lignes) {
			String[] ligneSepareeEnDeux = StringUtils.split(ligne, "->");
			String contenantName = StringUtils.split(ligneSepareeEnDeux[0], " ")[0];
			String temp = StringUtils.split(ligneSepareeEnDeux[0], " ")[1];
			temp = StringUtils.substring(temp, 1);
			temp = StringUtils.substring(temp, 0, temp.length() - 1);
			int contenantValue = Integer.parseInt(temp);

			String[] contenu = null;
			if (ligneSepareeEnDeux.length > 1) {
				contenu = StringUtils.split(ligneSepareeEnDeux[1], ",");
				contenu = Arrays.stream(contenu).map(String::trim).toArray(String[]::new);
			}

			Prog prog = new Prog(contenantName, contenantValue, contenu);
			progs.add(prog);
		}

		// une fois que tous les programmes sont dans la liste, ajouter les subProgs
		for (Prog prog : progs) {
			if (prog.getSubProgsName() != null) {
				for (String nom : prog.getSubProgsName()) {
					// recherche du prog ayant ce nom
					for (Prog p : progs) {
						if (p.getName().equals(nom)) {
							prog.getSubProgs().add(p);
							// prog.setValue(prog.getValue()+p.getValue()); //ajout des valeurs des sous programmes
						}
					}
				}
			}
		}

		// recherche du programme de base (= celui qui a le plus de sous programmes imbriqués)
		int max = 0;
		Prog progBase = null;
		for (Prog prog : progs) {
			int maxDepth = prog.maxDepth();
			if (maxDepth > max) {
				max = maxDepth;
				progBase = prog;
			}

		}

		// ajout de la valeur totale à chaque programme
		for (Prog prog : progs) {
			int res = prog.calculeValeurTotale();
			// System.out.println(prog.getName() +" : "+ res);
		}

		// recherche des programmes non equilibrés
		for (Prog prog : progs) {
			if (prog.getSubProgsName() != null) {

				List<Prog> subs = prog.getSubProgs();
				boolean balanced = true;
				int valeurPremierSub = subs.get(0).getValeurTotale();
				for (Prog s : subs) {
					if (s.getValeurTotale() != valeurPremierSub & s.getValeurTotale() != 0 & valeurPremierSub != 0) {
						balanced = false;
					}
				}

				if (!balanced) {

					System.out.println(prog.getName() + ", valeur unitaire: " + prog.getValue() + ", de niveau: " + prog.maxDepth() + " is non balanced");
					for (Prog s : subs) {
						System.out.println(s.getName() + " (valeur unitaire: " + s.getValue() + "), total: " + s.getValeurTotale());
					}
				}
			}
		}

	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day07.txt";
		List<String> lignes = Outil.importationString(filename);

		List<Prog> progs = new ArrayList<Prog>();
		for (String ligne : lignes) {
			String[] ligneSepareeEnDeux = StringUtils.split(ligne, "->");
			String contenantName = StringUtils.split(ligneSepareeEnDeux[0], " ")[0];
			String temp = StringUtils.split(ligneSepareeEnDeux[0], " ")[1];
			temp = StringUtils.substring(temp, 1);
			temp = StringUtils.substring(temp, 0, temp.length() - 1);
			int contenantValue = Integer.parseInt(temp);

			String[] contenu = null;
			if (ligneSepareeEnDeux.length > 1) {
				contenu = StringUtils.split(ligneSepareeEnDeux[1], ",");
				contenu = Arrays.stream(contenu).map(String::trim).toArray(String[]::new);
			}

			Prog prog = new Prog(contenantName, contenantValue, contenu);
			progs.add(prog);
		}

		// une fois que tous les programmes sont dans la liste, ajouter les subProgs
		for (Prog prog : progs) {
			if (prog.getSubProgsName() != null) {
				for (String nom : prog.getSubProgsName()) {
					// recherche du prog ayant ce nom
					for (Prog p : progs) {
						if (p.getName().equals(nom)) {
							prog.getSubProgs().add(p);
						}
					}
				}
			}
		}

		// recherche du programme de base (= celui qui a le plus de sous programmes imbriqués)
		int max = 0;
		Prog progBase = null;
		for (Prog prog : progs) {
			int maxDepth = prog.maxDepth();
			if (maxDepth > max) {
				max = maxDepth;
				progBase = prog;
			}
		}

		System.out.println(max);
		System.out.println(progBase);

	}

}
