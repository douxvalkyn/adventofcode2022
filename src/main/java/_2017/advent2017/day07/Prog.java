package advent2017.day07;

import java.util.ArrayList;
import java.util.List;

public class Prog {

	private String name;
	private int value;
	private List<Prog> subProgs;
	private String[] subProgsName;
	private int valeurTotale;
	
	
	public Prog(String name, int value) {
		super();
		this.name = name;
		this.value = value;
	}


	public Prog(String name, int value, String[] subProgsName) {
		super();
		this.name = name;
		this.value = value;
		this.subProgsName = subProgsName;
		List<Prog> subProgs = new ArrayList();
		this.subProgs=subProgs;
	}


	public int calculeValeurTotale() {
		 if (subProgs.isEmpty()) {
	            return this.value;
	        }
		int valeurTotale=this.value;
		 for (Prog p: subProgs) {
			 valeurTotale = valeurTotale+ p.calculeValeurTotale();
	        }
		this.valeurTotale=valeurTotale;
		return valeurTotale;
		
	}
	
    public int maxDepth() {
        if (subProgs.isEmpty()) {
            return 0;
        }
        int maxChildrenDepth = 0;
        for (Prog p: subProgs) {
            maxChildrenDepth = Math.max(maxChildrenDepth, p.maxDepth());
        }
        return 1+maxChildrenDepth;
    }
	
	
	
	public int getValeurTotale() {
		return valeurTotale;
	}


	public void setValeurTotale(int valeurTotale) {
		this.valeurTotale = valeurTotale;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}


	public List<Prog> getSubProgs() {
		return subProgs;
	}


	public void setSubProgs(List<Prog> subProgs) {
		this.subProgs = subProgs;
	}


	public String[] getSubProgsName() {
		return subProgsName;
	}


	public void setSubProgsName(String[] subProgsName) {
		this.subProgsName = subProgsName;
	}


	@Override
	public String toString() {
		return "Prog [name=" + name + ", value=" + value + "]";
	}
	
	

	
	
	
}
