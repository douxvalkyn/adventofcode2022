package advent2017.day06;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2017.outils.Outil;

public class Day06 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day06.class.getSimpleName() + "]");
		Day06 day = new Day06();
		LocalDateTime start = LocalDateTime.now();
		// day.run2();
		day.run();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run() throws IOException {

		List<List<Integer>> banksSave = new ArrayList<List<Integer>>();

		// initialisation
		List<Integer> banks = new ArrayList<Integer>();
		banks.add(2);
		banks.add(8);
		banks.add(8);
		banks.add(5);
		banks.add(4);
		banks.add(2);
		banks.add(3);
		banks.add(1);
		banks.add(5);
		banks.add(5);
		banks.add(1);
		banks.add(2);
		banks.add(15);
		banks.add(13);
		banks.add(5);
		banks.add(14);

		banksSave.add(banks);

		// reallocation
		int step = 1;
		boolean go = true;
		while (go) {

			// creer une copie des banks
			banks = copy(banks);

			// recherche de la bank à dépiler
			int max = Outil.max(banks);
			int indiceMax = banks.indexOf(max);

			// mettre cette bank à 0
			banks.set(indiceMax, 0);

			// ajouter les blocks 1 par 1 de maniere circulaire
			for (int i = 1; i <= max; i++) {
				int j = indiceMax + i;
				while (j >= banks.size()) {
					j = j - banks.size();
				}
				banks.set(j, banks.get(j) + 1);
			}

			// check
			if (banksSave.contains(banks)) {
				go = false;
				System.out.println("Part1: " + step);
				System.out.println("Part2: " + (banksSave.size() - banksSave.indexOf(banks)));

			} else {
				banksSave.add(banks);
				step++;
			}
		}
	}

	private List<Integer> copy(List<Integer> banks) {
		List<Integer> banksNew = new ArrayList<>();
		for (Integer bank : banks) {
			int res = bank;
			banksNew.add(res);
		}
		return banksNew;
	}


}
