package advent2017.day02;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day02 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day02.class.getSimpleName() + "]");
		Day02 day = new Day02();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day02.txt";
		List<String> lignes = Outil.importationString(filename);

		int somme = 0;
		// calculer le max et min pour chaque ligne
		for (int i = 0; i < lignes.size(); i++) {
			String[] ligneTab = StringUtils.split(lignes.get(i), "\t");
			List<Integer> maListe = new ArrayList<Integer>();
			for (String nb : ligneTab) {
				maListe.add(Integer.parseInt(nb));
			}
			int ecart = Outil.max(maListe) - Outil.min(maListe);
			somme = somme + ecart;
		}
		System.out.println(somme);
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day02.txt";
		List<String> lignes = Outil.importationString(filename);
		int somme = 0;
		// calculer le max et min pour chaque ligne
		for (int i = 0; i < lignes.size(); i++) {
			String[] ligneTab = StringUtils.split(lignes.get(i), "\t");
			List<Integer> maListe = new ArrayList<Integer>();
			for (String nb : ligneTab) {
				maListe.add(Integer.parseInt(nb));
			}

			int[] diviseurDividende = rechercheDiviseurDividende(maListe);
			int a = diviseurDividende[0];
			int b = diviseurDividende[1];
			if (a > b) {
				somme = somme + a / b;
			}
			if (a <= b) {
				somme = somme + b / a;
			}
		}
		System.out.println(somme);

	}

	private int[] rechercheDiviseurDividende(List<Integer> maListe) {
		int[] res = new int[2];
		for (Integer nb1 : maListe) {
			for (Integer nb2 : maListe) {
				if (nb1 % nb2 == 0 & nb1 / nb2 != 1) {
					res[0] = nb1;
					res[1] = nb2;
				}
			}
		}

		return res;
	}

}
