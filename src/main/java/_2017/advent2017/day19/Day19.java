package advent2017.day19;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2017.outils.Case;
import advent2017.outils.Grille;
import advent2017.outils.Outil;

public class Day19 {
	String orientation = "";
	boolean go = true;
	int pas = 1;

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day19.class.getSimpleName() + "]");
		Day19 day = new Day19();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		// lecture input
		String filename = "src/main/resources/advent2017/Day19.txt";
		List<String> input = Outil.importationString(filename);
		Grille grille = new Grille(input.get(0).length(), input.size());
		creationGrille(input, grille);

		// repérage de la position de depart
		int x = reperageXStart(input, grille);
		orientation = "S";
		int y = 0;
		Case caseActuelle = grille.getCase(x, y);

		// Parcours en suivant les lignes
		List<String> lettres = new ArrayList<String>();

		while (go) {
			caseActuelle = seDeplace2(caseActuelle, grille, lettres);
		}

		System.out.println(lettres);
		System.out.println(pas);

	}

	// run1 ----
	public void run1() throws IOException {
		// lecture input
		String filename = "src/main/resources/advent2017/Day19.txt";
		List<String> input = Outil.importationString(filename);
		Grille grille = new Grille(input.get(0).length(), input.size());
		creationGrille(input, grille);

		// repérage de la position de depart
		int x = reperageXStart(input, grille);
		orientation = "S";
		int y = 0;
		Case caseActuelle = grille.getCase(x, y);

		// Parcours en suivant les lignes
		List<String> lettres = new ArrayList<String>();

		while (go) {
			caseActuelle = seDeplace(caseActuelle, grille, lettres);
		}

		System.out.println(lettres);

	}

	private Case seDeplace2(Case caseActuelle, Grille grille, List<String> lettres) {
		int x = caseActuelle.getX();
		int y = caseActuelle.getY();

		if (orientation.equals("S")) {
			Case sud = caseActuelle.getCaseVoisineSud(grille);
			if (sud != null) {
				if (!sud.getEtat().equals(" ")) {
					caseActuelle = sud;
					pas++;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller au SUD, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
					Case east = caseActuelle.getCaseVoisineEast(grille);
					Case west = caseActuelle.getCaseVoisineWest(grille);
					if (east != null) {
						if (!east.getEtat().equals(" ")) {
							orientation = "E";
							caseActuelle = east;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (west != null) {
						if (!west.getEtat().equals(" ")) {
							orientation = "W";
							caseActuelle = west;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller au SUD, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
				Case east = caseActuelle.getCaseVoisineEast(grille);
				Case west = caseActuelle.getCaseVoisineWest(grille);
				if (east != null) {
					if (!east.getEtat().equals(" ")) {
						orientation = "E";
						caseActuelle = east;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (west != null) {
					if (!west.getEtat().equals(" ")) {
						orientation = "W";
						caseActuelle = west;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("E")) {
			Case east = caseActuelle.getCaseVoisineEast(grille);
			if (east != null) {
				if (!east.getEtat().equals(" ")) {
					caseActuelle = east;
					pas++;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers EST, on recherche un deplacement vers SUD ou NORD (on suppose qu'il n'y a pas de choix entre les deux)
					Case sud = caseActuelle.getCaseVoisineSud(grille);
					Case nord = caseActuelle.getCaseVoisineNord(grille);
					if (sud != null) {
						if (!sud.getEtat().equals(" ")) {
							orientation = "S";
							caseActuelle = sud;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (nord != null) {
						if (!nord.getEtat().equals(" ")) {
							orientation = "N";
							caseActuelle = nord;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers EST, on recherche un deplacement vers SUD ou NORD (on suppose qu'il n'y a pas de choix entre les deux)
				Case sud = caseActuelle.getCaseVoisineSud(grille);
				Case nord = caseActuelle.getCaseVoisineNord(grille);
				if (sud != null) {
					if (!sud.getEtat().equals(" ")) {
						orientation = "S";
						caseActuelle = sud;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (nord != null) {
					if (!nord.getEtat().equals(" ")) {
						orientation = "N";
						caseActuelle = nord;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("N")) {
			Case nord = caseActuelle.getCaseVoisineNord(grille);
			if (nord != null) {
				if (!nord.getEtat().equals(" ")) {
					caseActuelle = nord;
					pas++;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers nord, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
					Case east = caseActuelle.getCaseVoisineEast(grille);
					Case west = caseActuelle.getCaseVoisineWest(grille);
					if (east != null) {
						if (!east.getEtat().equals(" ")) {
							orientation = "E";
							caseActuelle = east;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (west != null) {
						if (!west.getEtat().equals(" ")) {
							orientation = "W";
							caseActuelle = west;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers nord, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
				Case east = caseActuelle.getCaseVoisineEast(grille);
				Case west = caseActuelle.getCaseVoisineWest(grille);
				if (east != null) {
					if (!east.getEtat().equals(" ")) {
						orientation = "E";
						caseActuelle = east;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (west != null) {
					if (!west.getEtat().equals(" ")) {
						orientation = "W";
						caseActuelle = west;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("W")) {
			Case west = caseActuelle.getCaseVoisineWest(grille);
			if (west != null) {
				if (!west.getEtat().equals(" ")) {
					caseActuelle = west;
					pas++;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers west, on recherche un deplacement vers Nord ou Sud (on suppose qu'il n'y a pas de choix entre les deux)
					Case nord = caseActuelle.getCaseVoisineNord(grille);
					Case sud = caseActuelle.getCaseVoisineSud(grille);
					if (nord != null) {
						if (!nord.getEtat().equals(" ")) {
							orientation = "N";
							caseActuelle = nord;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (sud != null) {
						if (!sud.getEtat().equals(" ")) {
							orientation = "S";
							caseActuelle = sud;
							pas++;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers west, on recherche un deplacement vers Nord ou Sud (on suppose qu'il n'y a pas de choix entre les deux)
				Case nord = caseActuelle.getCaseVoisineNord(grille);
				Case sud = caseActuelle.getCaseVoisineSud(grille);
				if (nord != null) {
					if (!nord.getEtat().equals(" ")) {
						orientation = "N";
						caseActuelle = nord;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (sud != null) {
					if (!sud.getEtat().equals(" ")) {
						orientation = "S";
						caseActuelle = sud;
						pas++;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		// condition d'arret, si on ne bouge pas
		if (x == caseActuelle.getX() & y == caseActuelle.getY()) {
			go = false;
		}

		return caseActuelle;
	}

	private Case seDeplace(Case caseActuelle, Grille grille, List<String> lettres) {
		int x = caseActuelle.getX();
		int y = caseActuelle.getY();

		if (orientation.equals("S")) {
			Case sud = caseActuelle.getCaseVoisineSud(grille);
			if (sud != null) {
				if (!sud.getEtat().equals(" ")) {
					caseActuelle = sud;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller au SUD, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
					Case east = caseActuelle.getCaseVoisineEast(grille);
					Case west = caseActuelle.getCaseVoisineWest(grille);
					if (east != null) {
						if (!east.getEtat().equals(" ")) {
							orientation = "E";
							caseActuelle = east;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (west != null) {
						if (!west.getEtat().equals(" ")) {
							orientation = "W";
							caseActuelle = west;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller au SUD, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
				Case east = caseActuelle.getCaseVoisineEast(grille);
				Case west = caseActuelle.getCaseVoisineWest(grille);
				if (east != null) {
					if (!east.getEtat().equals(" ")) {
						orientation = "E";
						caseActuelle = east;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (west != null) {
					if (!west.getEtat().equals(" ")) {
						orientation = "W";
						caseActuelle = west;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("E")) {
			Case east = caseActuelle.getCaseVoisineEast(grille);
			if (east != null) {
				if (!east.getEtat().equals(" ")) {
					caseActuelle = east;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers EST, on recherche un deplacement vers SUD ou NORD (on suppose qu'il n'y a pas de choix entre les deux)
					Case sud = caseActuelle.getCaseVoisineSud(grille);
					Case nord = caseActuelle.getCaseVoisineNord(grille);
					if (sud != null) {
						if (!sud.getEtat().equals(" ")) {
							orientation = "S";
							caseActuelle = sud;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (nord != null) {
						if (!nord.getEtat().equals(" ")) {
							orientation = "N";
							caseActuelle = nord;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers EST, on recherche un deplacement vers SUD ou NORD (on suppose qu'il n'y a pas de choix entre les deux)
				Case sud = caseActuelle.getCaseVoisineSud(grille);
				Case nord = caseActuelle.getCaseVoisineNord(grille);
				if (sud != null) {
					if (!sud.getEtat().equals(" ")) {
						orientation = "S";
						caseActuelle = sud;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (nord != null) {
					if (!nord.getEtat().equals(" ")) {
						orientation = "N";
						caseActuelle = nord;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("N")) {
			Case nord = caseActuelle.getCaseVoisineNord(grille);
			if (nord != null) {
				if (!nord.getEtat().equals(" ")) {
					caseActuelle = nord;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers nord, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
					Case east = caseActuelle.getCaseVoisineEast(grille);
					Case west = caseActuelle.getCaseVoisineWest(grille);
					if (east != null) {
						if (!east.getEtat().equals(" ")) {
							orientation = "E";
							caseActuelle = east;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (west != null) {
						if (!west.getEtat().equals(" ")) {
							orientation = "W";
							caseActuelle = west;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers nord, on recherche un deplacement vers EST ou OUEST (on suppose qu'il n'y a pas de choix entre les deux)
				Case east = caseActuelle.getCaseVoisineEast(grille);
				Case west = caseActuelle.getCaseVoisineWest(grille);
				if (east != null) {
					if (!east.getEtat().equals(" ")) {
						orientation = "E";
						caseActuelle = east;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (west != null) {
					if (!west.getEtat().equals(" ")) {
						orientation = "W";
						caseActuelle = west;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		if (orientation.equals("W")) {
			Case west = caseActuelle.getCaseVoisineWest(grille);
			if (west != null) {
				if (!west.getEtat().equals(" ")) {
					caseActuelle = west;
					if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
						lettres.add(caseActuelle.getEtat());
					}
				} else {
					// impossible d'aller vers west, on recherche un deplacement vers Nord ou Sud (on suppose qu'il n'y a pas de choix entre les deux)
					Case nord = caseActuelle.getCaseVoisineNord(grille);
					Case sud = caseActuelle.getCaseVoisineSud(grille);
					if (nord != null) {
						if (!nord.getEtat().equals(" ")) {
							orientation = "N";
							caseActuelle = nord;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
					if (sud != null) {
						if (!sud.getEtat().equals(" ")) {
							orientation = "S";
							caseActuelle = sud;
							if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
								lettres.add(caseActuelle.getEtat());
							}
						}
					}
				}
			} else {
				// impossible d'aller vers west, on recherche un deplacement vers Nord ou Sud (on suppose qu'il n'y a pas de choix entre les deux)
				Case nord = caseActuelle.getCaseVoisineNord(grille);
				Case sud = caseActuelle.getCaseVoisineSud(grille);
				if (nord != null) {
					if (!nord.getEtat().equals(" ")) {
						orientation = "N";
						caseActuelle = nord;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
				if (sud != null) {
					if (!sud.getEtat().equals(" ")) {
						orientation = "S";
						caseActuelle = sud;
						if (!caseActuelle.getEtat().equals("|") & !caseActuelle.getEtat().equals("-") & !caseActuelle.getEtat().equals("+")) {
							lettres.add(caseActuelle.getEtat());
						}
					}
				}
			}
		}

		// condition d'arret, si on ne bouge pas
		if (x == caseActuelle.getX() & y == caseActuelle.getY()) {
			go = false;
		}

		return caseActuelle;
	}

	public int reperageXStart(List<String> input, Grille grille) {
		int xStart = 0;
		for (int i = 0; i < input.get(0).length(); i++) {
			if (grille.getCase(i, 0).getEtat().equals("|")) {
				xStart = i;
			}
		}
		return xStart;
	}

	public void creationGrille(List<String> input, Grille grille) {
		for (int x = 0; x < input.get(0).length(); x++) {
			for (int y = 0; y < input.size(); y++) {
				grille.getCase(x, y).setEtat("" + input.get(y).charAt(x));
			}
		}
		grille.affichageEtat();
	}

}
