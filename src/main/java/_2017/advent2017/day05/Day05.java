package advent2017.day05;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2017.outils.Outil;

public class Day05 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day05.class.getSimpleName() + "]");
		Day05 day = new Day05();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day05.txt";
		List<Integer> lignes = Outil.importationInteger(filename);

		int step = 0;
		int cursor = 0;
		while (cursor < lignes.size() && cursor >= 0) {
			Integer jump = lignes.get(cursor);
			if (jump >= 3) {
				lignes.set(cursor, lignes.get(cursor) - 1);
			} else {
				lignes.set(cursor, lignes.get(cursor) + 1);
			}
			cursor = cursor + jump;
			step++;
		}

		System.out.println("steps: " + step);
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day05.txt";
		List<Integer> lignes = Outil.importationInteger(filename);

		int step = 0;
		int cursor = 0;
		while (cursor < lignes.size() && cursor >= 0) {
			Integer jump = lignes.get(cursor);
			lignes.set(cursor, lignes.get(cursor) + 1);
			cursor = cursor + jump;
			step++;
		}

		System.out.println("steps: " + step);
	}

}
