package advent2017.day17;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Day17 {

	// main -----
	public static void main(String[] args) throws IOException  {
		System.out.println("["+Day17.class.getSimpleName()+"]");
		Day17 day = new Day17();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		//day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	//pattern: on veut savoir quand la currentPosition est à 1
//	exemple: input=3
//	STEP currentPosition
//	0	0
//	1	0+input=3 > 3-step=2-step=1-step=0 > 0+1=1
//	2	1+input=4 > 4-step=2-step=0 > 0+1=1
//	3	1+input=4 > 4-step=1 > 1+1=2
//	4	2+input=5 > 5-step=1 > 1+1=2
//	5	2+input=5 > 5-step=0 > 0+1 = 1

	// run2 ----
	public void run2() throws IOException {

		int input=314;
		int currentPosition = 0;
		for (int step=1; step<=50000000; step++) {
			currentPosition=currentPosition+input;
			while (currentPosition>=step) {
				currentPosition=currentPosition-step;
			}
				currentPosition=currentPosition+1;
				if (currentPosition==1) {
					System.out.println(step);
				}
				//System.out.println("step: " +step + ", currentPosition:" + currentPosition);
		}
	}
	
	

	// run1 ----
	public void run1() throws IOException {

		int input=314;
		List<Integer> liste = new ArrayList<Integer>();
		liste.add(0);
		int currentPosition = 0;
		for (int i=1; i<=3000000; i++) {
			if (i%100000==0) {System.out.println(i);}
			//on avance de input;
			currentPosition=currentPosition+input;
			while (currentPosition>=liste.size()) {
				currentPosition=currentPosition-liste.size();
			}

			//inserer une nouvelle valeur apres cette position
			if ((currentPosition+1)>liste.size()) {
				liste.add(i);}
			else {
				liste.add(currentPosition+1, i);
				currentPosition=currentPosition+1;
				}
		}
		//System.out.println(liste);
		int indice2017 = liste.indexOf(2017); //part1
		System.out.println(liste.get(indice2017+1)); //part1
	}




}
