package advent2017.day09;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import advent2017.outils.Outil;

public class Day09 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day09.class.getSimpleName() + "]");
		Day09 day = new Day09();
		LocalDateTime start = LocalDateTime.now();
		day.run();
		// day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run() throws IOException {
		String input2 = "{<!!!>>}";
		String filename = "src/main/resources/advent2017/Day09.txt";
		List<String> input = Outil.importationString(filename);
		input2 = input.get(0);
		System.out.println(input2);

		String clean = clean(input2);
		System.out.println(clean);

		int somme = sommeProfondeurs(clean);
		System.out.println("somme: " + somme);

	}

	private int sommeProfondeurs(String clean) {
		int somme = 0;
		int niveau = 1;
		for (int i = 0; i < clean.length(); i++) {
			char caractere = clean.charAt(i);
			if (caractere == '{') {
				niveau++;
			}
			if (caractere == '}') {
				niveau--;
				somme = somme + niveau;
			}

		}
		return somme;
	}

	public String clean(String input) {
		// clean garbage
		String clean = "";
		boolean garbage = false;
		boolean ignore = false;
		int cptGarbage = 0;
		for (int i = 0; i < input.length(); i++) {
			char caractere = input.charAt(i);
			if (garbage) {
				cptGarbage++;
			}

			if (garbage & caractere == '!' & !ignore) {
				i = i + 1;
				cptGarbage = cptGarbage - 1;
			}
			if (caractere != '<' & caractere != '>' & !garbage & !ignore) {
				clean = clean + caractere;
			}
			if (caractere == '<' & !garbage & !ignore) { // debut garbage
				garbage = true;
				cptGarbage--;
			}
			if (caractere == '>' & garbage & !ignore) { // fin garbage
				garbage = false;
			}

		}
		System.out.println("cptGarbage: " + cptGarbage);
		return clean;

	}

}
