package advent2017.day20;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day20 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day20.class.getSimpleName() + "]");
		Day20 day = new Day20();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {

		// lecture input--
		String filename = "src/main/resources/advent2017/Day20.txt";
		List<String> input = Outil.importationString(filename);

		// creation particles --
		List<Particle> particles = new ArrayList<Particle>();
		creationParticles(input, particles);

		// application des regles
		int stepMax = 40;
		List<Integer> toRemove = new ArrayList<Integer>();
		for (int step = 0; step < stepMax; step++) {

			for (Particle particle : particles) {
				particle.setVelX(particle.getVelX() + particle.getAccX());
				particle.setVelY(particle.getVelY() + particle.getAccY());
				particle.setVelZ(particle.getVelZ() + particle.getAccZ());
				particle.setPosX(particle.getPosX() + particle.getVelX());
				particle.setPosY(particle.getPosY() + particle.getVelY());
				particle.setPosZ(particle.getPosZ() + particle.getVelZ());
			}
			// suppression des particles au même endroit

			for (Particle particle1 : particles) {
				for (Particle particle2 : particles) {
					if (particle1 != particle2) {
						if (particle1.getPosX() == particle2.getPosX() & particle1.getPosY() == particle2.getPosY() & particle1.getPosZ() == particle2.getPosZ()) {
							toRemove.add(particle2.getId());
						}
					}
				}
			}

			Collections.sort(toRemove);
			List<Integer> toRemoveWithoutDuplicate = toRemove.stream().distinct().collect(Collectors.toList());
			toRemove = toRemoveWithoutDuplicate;

		}
		System.out.println(particles.size() - toRemove.size());

	}

	// run1 ----
	public void run1() throws IOException {

		// lecture input--
		String filename = "src/main/resources/advent2017/Day20.txt";
		List<String> input = Outil.importationString(filename);

		// creation particles --
		List<Particle> particles = new ArrayList<Particle>();
		creationParticles(input, particles);

		// application des regles

		Particle particleProcheZero = null;
		for (int step = 0; step < 1_000; step++) {
			long distMin = Integer.MAX_VALUE;
			for (Particle particle : particles) {
				particle.setVelX(particle.getVelX() + particle.getAccX());
				particle.setVelY(particle.getVelY() + particle.getAccY());
				particle.setVelZ(particle.getVelZ() + particle.getAccZ());
				particle.setPosX(particle.getPosX() + particle.getVelX());
				particle.setPosY(particle.getPosY() + particle.getVelY());
				particle.setPosZ(particle.getPosZ() + particle.getVelZ());

				// calcul de la distance à l'origine
				long dist = Math.abs(particle.getPosX()) + Math.abs(particle.getPosY()) + Math.abs(particle.getPosZ());
				if (dist < distMin) {
					distMin = dist;
					particleProcheZero = particle;
				}
			}
		}
		System.out.println("particleProcheZero: " + particleProcheZero);

	}

	public void creationParticles(List<String> input, List<Particle> particles) {
		int num = 0;
		for (String ligne : input) {
			// System.out.println(ligne);
			String p = StringUtils.split(ligne, ">")[0];
			String v = StringUtils.split(ligne, ">")[1];
			String a = StringUtils.split(ligne, ">")[2];
			p = StringUtils.substring(p, 3);
			v = StringUtils.substring(v, 5);
			a = StringUtils.substring(a, 5);
			long px = Long.parseLong(StringUtils.split(p, ",")[0].trim());
			long py = Long.parseLong(StringUtils.split(p, ",")[1].trim());
			long pz = Long.parseLong(StringUtils.split(p, ",")[2].trim());
			long vx = Long.parseLong(StringUtils.split(v, ",")[0].trim());
			long vy = Long.parseLong(StringUtils.split(v, ",")[1].trim());
			long vz = Long.parseLong(StringUtils.split(v, ",")[2].trim());
			long ax = Long.parseLong(StringUtils.split(a, ",")[0].trim());
			long ay = Long.parseLong(StringUtils.split(a, ",")[1].trim());
			long az = Long.parseLong(StringUtils.split(a, ",")[2].trim());

			Particle particle = new Particle(px, py, pz, vx, vy, vz, ax, ay, az, num);
			particles.add(particle);
			num++;
		}
	}

}
