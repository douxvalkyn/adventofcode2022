package advent2017.day20;

public class Particle {
	
private long posX;
private long posY;
private long posZ;

private long velX;
private long velY;
private long velZ;

private long accX;
private long accY;
private long accZ;

private int id;

public Particle(long posX, long posY, long posZ, long velX, long velY, long velZ, long accX, long accY, long accZ, int id) {
	super();
	this.posX = posX;
	this.posY = posY;
	this.posZ = posZ;
	this.velX = velX;
	this.velY = velY;
	this.velZ = velZ;
	this.accX = accX;
	this.accY = accY;
	this.accZ = accZ;
	this.id = id;
}

public long getPosX() {
	return posX;
}

public void setPosX(long posX) {
	this.posX = posX;
}

public long getPosY() {
	return posY;
}

public void setPosY(long posY) {
	this.posY = posY;
}

public long getPosZ() {
	return posZ;
}

public void setPosZ(long posZ) {
	this.posZ = posZ;
}

public long getVelX() {
	return velX;
}

public void setVelX(long velX) {
	this.velX = velX;
}

public long getVelY() {
	return velY;
}

public void setVelY(long velY) {
	this.velY = velY;
}

public long getVelZ() {
	return velZ;
}

public void setVelZ(long velZ) {
	this.velZ = velZ;
}

public long getAccX() {
	return accX;
}

public void setAccX(long accX) {
	this.accX = accX;
}

public long getAccY() {
	return accY;
}

public void setAccY(long accY) {
	this.accY = accY;
}

public long getAccZ() {
	return accZ;
}

public void setAccZ(long accZ) {
	this.accZ = accZ;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

@Override
public String toString() {
	return "Particle [posX=" + posX + ", posY=" + posY + ", posZ=" + posZ + ", velX=" + velX + ", velY=" + velY
			+ ", velZ=" + velZ + ", accX=" + accX + ", accY=" + accY + ", accZ=" + accZ + ", id=" + id + "]";
}






}
