package advent2017.day12;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Graph;
import advent2017.outils.Outil;

public class Day12 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day12.class.getSimpleName() + "]");
		Day12 day = new Day12();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day12.txt";
		List<String> input = Outil.importationString(filename);
		// System.out.println(input);

		Graph graph = new Graph();

		// creation des noeuds
		for (int i = 0; i < input.size(); i++) {
			graph.addNode(i);
		}

		// creation des liens
		for (int i = 0; i < input.size(); i++) {
			String ligne = input.get(i);
			int noeudSource = Integer.parseInt(StringUtils.trim(StringUtils.split(ligne, "<->")[0]));
			String[] noeudsConnectes = StringUtils.split(StringUtils.split(ligne, "<->")[1], ", ");
			for (int j = 0; j < noeudsConnectes.length; j++) {
				graph.addEdge(noeudSource, Integer.parseInt(noeudsConnectes[j]), 1);
			}
		}

		// tester
		Map<Integer, Integer> map = new HashMap<>();
		int source = 0;
		map.put(0, 0); // 0 appartient au groupe 0
		int cpt = teste(input, graph, map, 1, source);

		while (map.size() != input.size()) {
			// recherche du premier numero non contenu dans la map
			boolean go = true;
			for (int i = 0; i < 2000; i++) {
				if (!map.containsKey(i) & go) {
					source = i;
					go = false;
				}
			}
			map.put(source, source);
			teste(input, graph, map, 1, source);
		}

		// compter le nb de groupes
		System.out.println(map);
		long count = map.values().stream().distinct().count();
		System.out.println(count);

	}

	public int teste(List<String> input, Graph graph, Map<Integer, Integer> map, int cpt, int source) {
		for (int i = 0; i < input.size(); i++) {
			int cost = graph.uniformSearch(source, i);
			if (cost > 0) {
				cpt++;
				map.put(i, 0); // appartient au groupe de source
			}
		}
		return cpt;
	}

	public <K, V extends Comparable<V>> V maxUsingStreamAndLambda(Map<K, V> map) {
		Optional<Entry<K, V>> maxEntry = map.entrySet().stream().max((Entry<K, V> e1, Entry<K, V> e2) -> e1.getValue().compareTo(e2.getValue()));

		return maxEntry.get().getValue();
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day12.txt";
		List<String> input = Outil.importationString(filename);
		// System.out.println(input);

		Graph graph = new Graph();

		// creation des noeuds
		for (int i = 0; i < input.size(); i++) {
			graph.addNode(i);
		}

		// creation des liens
		for (int i = 0; i < input.size(); i++) {
			String ligne = input.get(i);
			int noeudSource = Integer.parseInt(StringUtils.trim(StringUtils.split(ligne, "<->")[0]));
			String[] noeudsConnectes = StringUtils.split(StringUtils.split(ligne, "<->")[1], ", ");
			for (int j = 0; j < noeudsConnectes.length; j++) {
				graph.addEdge(noeudSource, Integer.parseInt(noeudsConnectes[j]), 1);
			}
		}

		// tester
		int cpt = 1;
		for (int i = 0; i < input.size(); i++) {
			int cost = graph.uniformSearch(0, i);
			if (cost > 0) {
				cpt++;
			}
		}

		System.out.println(cpt);

	}

}
