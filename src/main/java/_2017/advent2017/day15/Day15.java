package advent2017.day15;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Day15 {

	// main -----
	public static void main(String[] args) throws IOException  {
		System.out.println("["+Day15.class.getSimpleName()+"]");
		Day15 day = new Day15();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}


	// run2 ----
	public void run2() throws IOException {
		List<Long> generatorA = new ArrayList<>();
		List<Long> generatorB = new ArrayList<>();
		List<Long> generatorA2 = new ArrayList<>();
		List<Long> generatorB2 = new ArrayList<>();
		//		generatorA.add(65L);
		//		generatorB.add(8921L);
		generatorA.add(116L);
		generatorB.add(299L);

		//remplir les generator
		int nbIter=0;
		while  (generatorA2.size()<=5000000 && generatorB2.size()<=5000000) {
			nbIter++;
			for (int i=0;i<40_000_000;i++) {
				Long valA = generatorA.get(i);
				Long valB = generatorB.get(i);
				long remainderA = (valA*16807)%2147483647;
				long remainderB = (valB*48271)%2147483647;
				generatorA.add(remainderA);
				generatorB.add(remainderB);
				if (remainderA%4==0) {generatorA2.add(remainderA);}
				if (remainderB%8==0) {generatorB2.add(remainderB);}
			}
			System.out.println("nb iterations de 40M: "+ (nbIter+1));
		}

		//transformation en binaire et comptages
		int cpt=0;
		for (int j=0;j<5000000;j++) {
			Long a = generatorA2.get(j);
			Long b = generatorB2.get(j);
			String binaryA = Long.toBinaryString(a);
			String binaryB = Long.toBinaryString(b);
			String endA = StringUtils.substring(binaryA, -16);
			String endB = StringUtils.substring(binaryB, -16);
			if (endA.equals(endB)) {
				cpt++;
			}
		}
		System.out.println("nb de paires identiques sur 5M de tests: "+cpt);
	}



	// run1 ----
	public void run1() throws IOException {
		List<Long> generatorA = new ArrayList<>();
		List<Long> generatorB = new ArrayList<>();
		//			generatorA.add(65L);
		//			generatorB.add(8921L);
		generatorA.add(116L);
		generatorB.add(299L);

		int cpt=0;
		for (int i=0;i<40_000_000;i++) {
			Long valA = generatorA.get(i);
			Long valB = generatorB.get(i);
			long remainderA = (valA*16807)%2147483647;
			long remainderB = (valB*48271)%2147483647;
			generatorA.add(remainderA);
			generatorB.add(remainderB);

			//transformation en binaire
			String remainderA_Binary = Long.toBinaryString(remainderA);
			String remainderB_Binary = Long.toBinaryString(remainderB);
			String endA = StringUtils.substring(remainderA_Binary, -16);
			String endB = StringUtils.substring(remainderB_Binary, -16);
			if (endA.equals(endB)) {
				cpt++;
			}
		}
		System.out.println(cpt);
	}


}
