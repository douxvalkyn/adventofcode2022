package advent2017.day14;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import advent2017.day10.Day10;
import advent2017.outils.Case;
import advent2017.outils.Grille;

public class Day14 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day14.class.getSimpleName() + "]");
		Day14 day = new Day14();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run1 ----
	public void run2() throws IOException {

		String key_ = "jxqlasbh";
		// String key_="flqrgnkx";
		List<String> disk = new ArrayList<>();
		Day10 d10 = new Day10();

		// remplir le disk selon les indications des hash
		for (int index = 0; index < 128; index++) {
			String key = key_ + "-" + index;
			String hash = d10.fastHash(key);
			String bin = hexToBin(hash);
			disk.add(bin);
		}

		// compter les 1
		int cpt = compterUn(disk);
		// System.out.println("cpt: " + cpt);

		// transformer le disk en grille
		Grille grille = new Grille(128, 128);
		creationGrille(grille, disk);
		grille.affichageEtat();

		// numéroter les regions distinctes
		numeroterRegions(grille);
		// grille.affichageValeur();

	}

	private void numeroterRegions(Grille grille) {
		int numReg = 1;
		List<Case> cases = grille.getCases();
		for (Case cell : cases) {
			if (cell.getEtat().equals("#") & cell.getValeur() == 0) {
				cell.setValeur(numReg);
				numeroterVoisins(cell, grille);
				numReg++;
			}
		} // fin for
		System.out.println("nb de regions: " + (numReg - 1));

	}

	private void numeroterVoisins(Case cell, Grille grille) {
		List<Case> voisins = cell.getCasesAdjacentesSiEtatEgalParam(grille, "#");
		for (Case voisin : voisins) {
			if (voisin.getValeur() == 0) {
				voisin.setValeur(cell.getValeur());
				numeroterVoisins(voisin, grille);
			}
		}

	}

	private void creationGrille(Grille grille, List<String> disk) {
		for (int j = 0; j < disk.size(); j++) {
			String row = disk.get(j);
			for (int i = 0; i < row.length(); i++) {
				char caractere = row.charAt(i);
				if (caractere == '1') {
					grille.getCase(i, j).setEtat("#");
				}
				if (caractere == '0') {
					grille.getCase(i, j).setEtat(".");
				}
			}
		}

	}

	public int compterUn(List<String> disk) {
		int cpt = 0;
		for (int i = 0; i < 128; i++) {
			String row = disk.get(i);
			for (int j = 0; j < row.length(); j++) {
				if (row.charAt(j) == '1') {
					cpt++;
				}
			}
		}
		return cpt;
	}

	// run1 ----
	public void run1() throws IOException {

		String key_ = "jxqlasbh";
		List<String> disk = new ArrayList<>();
		Day10 d10 = new Day10();

		for (int index = 0; index < 128; index++) {
			String key = key_ + "-" + index;
			// String hash = d10.knotHash(cll, key);
			String hash = d10.fastHash(key);
			// System.out.println(key+ ": " +hash);
			String bin = hexToBin(hash);
			// System.out.println("bin: "+bin);
			disk.add(bin);
		}

		int cpt = compterUn(disk);
		System.out.println("cpt: " + cpt);
	}

	private String hexToBin(String hex) {
		hex = hex.replaceAll("0", "0000");
		hex = hex.replaceAll("1", "0001");
		hex = hex.replaceAll("2", "0010");
		hex = hex.replaceAll("3", "0011");
		hex = hex.replaceAll("4", "0100");
		hex = hex.replaceAll("5", "0101");
		hex = hex.replaceAll("6", "0110");
		hex = hex.replaceAll("7", "0111");
		hex = hex.replaceAll("8", "1000");
		hex = hex.replaceAll("9", "1001");
		hex = hex.replaceAll("a", "1010");
		hex = hex.replaceAll("b", "1011");
		hex = hex.replaceAll("c", "1100");
		hex = hex.replaceAll("d", "1101");
		hex = hex.replaceAll("e", "1110");
		hex = hex.replaceAll("f", "1111");
		return hex;
	}

}
