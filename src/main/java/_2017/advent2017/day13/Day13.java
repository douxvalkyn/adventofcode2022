package advent2017.day13;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import advent2017.outils.Outil;

public class Day13 {

	// main -----
	public static void main(String[] args) throws IOException {
		System.out.println("[" + Day13.class.getSimpleName() + "]");
		Day13 day = new Day13();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	// run2 ----
	public void run2() throws IOException {
		String filename = "src/main/resources/advent2017/Day13.txt";
		List<String> input = Outil.importationString(filename);

		for (int i = 0; i < 4_000_000; i++) {
			exec(input, i);
		}
	}

	public void exec(List<String> input, int delay) {
		int severity = 0;
		for (String ligne : input) {
			int depth = delay + Integer.parseInt(StringUtils.split(ligne, ": ")[0]);
			int range = Integer.parseInt(StringUtils.split(ligne, ": ")[1]);
			int frequence = (range - 1) * 2;
			if (depth % frequence == 0) {
				severity = severity + depth * range;
			}
		}
		if (severity == 0) {
			System.out.println("PASS ! delay: " + delay);
		}
	}

	// run1 ----
	public void run1() throws IOException {
		String filename = "src/main/resources/advent2017/Day13.txt";
		List<String> input = Outil.importationString(filename);
		System.out.println(input);

		int severity = 0;
		for (String ligne : input) {
			int depth = Integer.parseInt(StringUtils.split(ligne, ": ")[0]);
			int range = Integer.parseInt(StringUtils.split(ligne, ": ")[1]);
			int frequence = (range - 1) * 2;
			if (depth % frequence == 0) {
				System.out.println(depth);
				System.out.println(range);
				System.out.println("-----");
				severity = severity + depth * range;
			}
		}
		System.out.println("severity: " + severity);
	}

}
