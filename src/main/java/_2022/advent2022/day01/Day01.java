package advent2022.day01;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day01 {

	private static Logger logger = LoggerFactory.getLogger(Day01.class);

	public static void main(String[] args) throws IOException {
		if (System.console() != null && System.getenv().get("TERM") != null) {
			System.out.println("\u001B[36m" + "Menu option" + "\u001B[0m");
		} else {
			System.out.println("Menu option");
		}

		logger.info("[" + Day01.class.getSimpleName() + "]");
		Day01 day = new Day01();
		LocalDateTime start = LocalDateTime.now();
		day.run();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run() {
		String file = "src/main/resources/advent2022/day01.txt";
		List<String> res = Outil.importationString(file);
		// System.out.println(res);
		List<Integer> food = new ArrayList<Integer>();
		int cpt = 0;
		for (int i = 0; i < res.size(); i++) {
			if (!res.get(i).isEmpty()) {
				cpt = cpt + Integer.valueOf(res.get(i));

			} else {
				// System.out.println(cpt);
				food.add(cpt);
				cpt = 0;
			}

		}
		int top1 = Outil.max(food);
		System.out.println("(run1) max: " + top1);
		food.remove(Outil.max(food));
		int top2 = Outil.max(food);
		food.remove(Outil.max(food));
		int top3 = Outil.max(food);
		System.out.println("(run2) total: " + (top1 + top2 + top3));
	}

}
