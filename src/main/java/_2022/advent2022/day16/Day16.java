package advent2022.day16;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import communs.GraphWeighted;
import communs.NodeWeighted;
import communs.Outil;

public class Day16 {

	private static Logger logger = LoggerFactory.getLogger(Day16.class);
	private int max = 0;

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day16.class.getSimpleName() + "]");
		Day16 day = new Day16();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}


	public void run2() { //algo BFS 2 worker 2181 too low
		String file = "src/main/resources/advent2022/day16.txt";
		List<String> res = Outil.importationString(file);
		List<Valve> valves = creationValves(res);
		List<Valve> valvesFluxPositif = trouverValvesDebitPositif(valves);
		// déterminer le plus court chemin entre ces valves à flux positif
		Map<String, Integer> distances = new HashMap<>();
		distances = calculeDistancesEntreValves(valves, distances);
		Valve valveDepart =  getValveByName(valves, "AA");
		int maxTime=26;
		int maxPressure=0;
		int max = bfs2( valveDepart, maxTime, distances, maxPressure, valvesFluxPositif);
		System.out.println("run2: "+max);

	}

	private int bfs2(Valve valveDepart, int maxTime, Map<String, Integer> distances, int maxPressure, List<Valve> valvesFluxPositif) {

		State stateInitial = new State();
		stateInitial.setTime(0);
		stateInitial.setTimeElephant(0);
		stateInitial.setValveActuelle(valveDepart);
		stateInitial.setValveActuelleElephant(valveDepart);

		Queue<State> queue = new LinkedList<>();
		queue.add(stateInitial);

		Set<State> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial);

		while (!queue.isEmpty()) {
			State stateActuel = queue.poll();
		//	System.err.println(queue.size());
			if ((stateActuel.getTime() >= maxTime && stateActuel.getTimeElephant() >= maxTime )|| stateActuel.getValvesOuvertes().size()==valvesFluxPositif.size()) {
				if (stateActuel.getPressure() > maxPressure) {
					maxPressure = stateActuel.getPressure();
					//System.out.println(stateActuel.getPressure() + " "+stateActuel.getValvesOuvertes()); //pour connaitre le chemin
				}
			} else {
				
				//optimisations
				if (stateActuel.getTime()>maxTime/4 && stateActuel.getPressure()<700 || stateActuel.getTime()>maxTime/2 && stateActuel.getPressure()<1400) {
					//hypothese: ce chemin est moisi: on ne l'emprunte pas
				}else {

				//s'il reste des valves à ouvrir non visitées, on s'y rend
				if (stateActuel.getValvesOuvertes().size()< valvesFluxPositif.size()) {
					List<Valve> nextValves = choisirProchaineValve(stateActuel.getValvesOuvertes(),valvesFluxPositif );

					for (Valve nextValve:nextValves) {

						State nextState = new State();

						//Humain et Elephant
						if (stateActuel.getTime()<maxTime && stateActuel.getTimeElephant()<maxTime) {
							Set<Valve> valvesOuvertesActuellement = stateActuel.getValvesOuvertes();
							for (Valve v:valvesOuvertesActuellement) {
								String nom = v.getNom();
								Valve valve = getValveByName(valvesFluxPositif, nom);
								nextState.getValvesOuvertes().add(valve);
							}
							nextState.getValvesOuvertes().add(nextValve);
							String trajet = stateActuel.getValveActuelle().getNom() + ":" + nextValve.getNom();
							Integer distanceDuTrajet = distances.get(trajet);
							nextState.setTime(stateActuel.getTime()+distanceDuTrajet+1);
							nextState.setValveActuelle(nextValve);
							nextState.setTimeElephant(stateActuel.getTimeElephant());
							nextState.setValveActuelleElephant(stateActuel.getValveActuelleElephant());
							int dureeFonctionnement = (maxTime-stateActuel.getTime()) - distanceDuTrajet - 1;
							if (dureeFonctionnement>0) {
								nextState.setPressure(stateActuel.getPressure() + dureeFonctionnement * nextValve.getFlowRate());
							}else {
								nextState.setPressure(stateActuel.getPressure());
							}

							for (Valve nextValve2:nextValves) {
								if (nextValve!=nextValve2) {
									State nextState2 = new State();
									nextState2.setPressure(nextState.getPressure());
									nextState2.setTime(nextState.getTime());
									nextState2.setValveActuelle(nextState.getValveActuelle());
									valvesOuvertesActuellement = nextState.getValvesOuvertes();
									for (Valve v:valvesOuvertesActuellement) {
										String nom = v.getNom();
										Valve valve = getValveByName(valvesFluxPositif, nom);
										nextState2.getValvesOuvertes().add(valve);
									}
									nextState2.getValvesOuvertes().add(nextValve2);
									trajet = stateActuel.getValveActuelleElephant().getNom() + ":" + nextValve2.getNom();
									distanceDuTrajet = distances.get(trajet);
									nextState2.setTimeElephant(stateActuel.getTimeElephant()+distanceDuTrajet+1);
									nextState2.setValveActuelleElephant(nextValve2);
									dureeFonctionnement = (maxTime-stateActuel.getTimeElephant()) - distanceDuTrajet - 1;
									if (dureeFonctionnement>0) {
										nextState2.setPressure(nextState.getPressure() + dureeFonctionnement * nextValve2.getFlowRate());
									}else {
										nextState2.setPressure(nextState.getPressure());
									}
									if (!visitedStates.contains(nextState2) ) {
										queue.add(nextState2);
										visitedStates.add(nextState2);
									}
								}
							}
						}

						//Humain seulement (elephant terminé)
						if (stateActuel.getTime()<maxTime && stateActuel.getTimeElephant()>=maxTime) {
							Set<Valve> valvesOuvertesActuellement = stateActuel.getValvesOuvertes();
							for (Valve v:valvesOuvertesActuellement) {
								String nom = v.getNom();
								Valve valve = getValveByName(valvesFluxPositif, nom);
								nextState.getValvesOuvertes().add(valve);
							}
							nextState.getValvesOuvertes().add(nextValve);
							String trajet = stateActuel.getValveActuelle().getNom() + ":" + nextValve.getNom();
							Integer distanceDuTrajet = distances.get(trajet);
							nextState.setTime(stateActuel.getTime()+distanceDuTrajet+1);
							nextState.setValveActuelle(nextValve);
							nextState.setTimeElephant(stateActuel.getTimeElephant());
							nextState.setValveActuelleElephant(stateActuel.getValveActuelleElephant());
							int dureeFonctionnement = (maxTime-stateActuel.getTime()) - distanceDuTrajet - 1;
							if (dureeFonctionnement>0) {
								nextState.setPressure(stateActuel.getPressure() + dureeFonctionnement * nextValve.getFlowRate());
							}else {
								nextState.setPressure(stateActuel.getPressure());
							}
							if (!visitedStates.contains(nextState) ) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}

						//Elephant seulement (elephant terminé)
						if (stateActuel.getTime()>=maxTime && stateActuel.getTimeElephant()<maxTime) {
							Set<Valve> valvesOuvertesActuellement = stateActuel.getValvesOuvertes();
							for (Valve v:valvesOuvertesActuellement) {
								String nom = v.getNom();
								Valve valve = getValveByName(valvesFluxPositif, nom);
								nextState.getValvesOuvertes().add(valve);
							}
							nextState.getValvesOuvertes().add(nextValve);
							String trajet = stateActuel.getValveActuelleElephant().getNom() + ":" + nextValve.getNom();
							Integer distanceDuTrajet = distances.get(trajet);
							nextState.setTime(stateActuel.getTime());
							nextState.setValveActuelle(stateActuel.getValveActuelle());
							nextState.setTimeElephant(stateActuel.getTimeElephant()+distanceDuTrajet+1);
							nextState.setValveActuelleElephant(nextValve);
							int dureeFonctionnement = (maxTime-stateActuel.getTimeElephant()) - distanceDuTrajet - 1;
							if (dureeFonctionnement>0) {
								nextState.setPressure(stateActuel.getPressure() + dureeFonctionnement * nextValve.getFlowRate());
							}else {
								nextState.setPressure(stateActuel.getPressure());
							}
							if (!visitedStates.contains(nextState) ) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}	
						}

						

					}
				}
				}
			}//fin else
		}//fin while


		return maxPressure;
	}


	public void run1() { //algo BFS 1 worker
		String file = "src/main/resources/advent2022/day16.txt";
		List<String> res = Outil.importationString(file);
		List<Valve> valves = creationValves(res);
		List<Valve> valvesFluxPositif = trouverValvesDebitPositif(valves);
		// déterminer le plus court chemin entre ces valves à flux positif
		Map<String, Integer> distances = new HashMap<>();
		distances = calculeDistancesEntreValves(valves, distances);
		Valve valveDepart =  getValveByName(valves, "AA");
		int maxTime=30;
		int maxPressure=0;
		int max = bfs( valveDepart, maxTime, distances, maxPressure, valvesFluxPositif);
		System.out.println("run1: "+max);
	}






	private int bfs(Valve valveDepart, int maxTime, Map<String, Integer> distances, int maxPressure, List<Valve> valvesFluxPositif) {

		State stateInitial = new State();
		stateInitial.setTime(0);
		stateInitial.setValveActuelle(valveDepart);

		Queue<State> queue = new LinkedList<>();
		queue.add(stateInitial);

		Set<State> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial);

		while (!queue.isEmpty()) {
			State stateActuel = queue.poll();
			if (stateActuel.getTime() >= maxTime || stateActuel.getValvesOuvertes().size()==valvesFluxPositif.size()) {
				if (stateActuel.getPressure() > maxPressure) {
					maxPressure = stateActuel.getPressure();
					System.err.println(stateActuel.getValvesOuvertes()); //pour connaitre le chemin
				}
			} else {

				//s'il reste des valves à ouvrir non visitées, on s'y rend
				if (stateActuel.getValvesOuvertes().size()< valvesFluxPositif.size()) {
					List<Valve> nextValves = choisirProchaineValve(stateActuel.getValvesOuvertes(),valvesFluxPositif );

					for (Valve nextValve:nextValves) {
						State nextState = new State();
						Set<Valve> valvesOuvertesActuellement = stateActuel.getValvesOuvertes();
						for (Valve v:valvesOuvertesActuellement) {
							String nom = v.getNom();
							Valve valve = getValveByName(valvesFluxPositif, nom);
							nextState.getValvesOuvertes().add(valve);
						}
						nextState.getValvesOuvertes().add(nextValve);
						String trajet = stateActuel.getValveActuelle().getNom() + ":" + nextValve.getNom();
						Integer distanceDuTrajet = distances.get(trajet);
						nextState.setTime(stateActuel.getTime()+distanceDuTrajet+1);
						nextState.setValveActuelle(nextValve);
						int dureeFonctionnement = (maxTime-stateActuel.getTime()) - distanceDuTrajet - 1;
						if (dureeFonctionnement>0) {
							nextState.setPressure(stateActuel.getPressure() + dureeFonctionnement * nextValve.getFlowRate());
						}else {
							nextState.setPressure(stateActuel.getPressure());
						}

						if (!visitedStates.contains(nextState) ) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}
				}

			}//fin else
		}//fin while


		return maxPressure;
	}


	private List<Valve> choisirProchaineValve(Set<Valve> valvesOuvertes, List<Valve> valvesFluxPositif) {
		List<Valve> nextValves = new ArrayList<Valve>();
		for (Valve valve:valvesFluxPositif) {
			if (getValveByName(valvesOuvertes, valve.getNom()) == null) {
				nextValves.add(valve);
			}
		}
		return nextValves;
	}


	private void run1b() { //algo non BFS, plus lent mais fonctionne

		String file = "src/main/resources/advent2022/day16.txt";
		List<String> res = Outil.importationString(file);

		List<Valve> valves = creationValves(res);

		List<Valve> valvesFluxPositif = trouverValvesDebitPositif(valves);

		// déterminer le plus court chemin entre ces valves à flux positif
		Map<String, Integer> distances = new HashMap<>();
		distances = calculeDistancesEntreValves(valves, distances);
		//List<Entry<String, Integer>> trois = distances.entrySet().stream().filter(l->l.getValue()<2).collect(Collectors.toList());

		// preparer tous les chemins possibles à analyser ensuite
		List<String> chemins = calculeTousCheminsPossibles(valvesFluxPositif, distances, valves);

		System.err.println("run1: " + max);

	}


	private Map<String, Integer> calculeDistancesEntreValves(List<Valve> valves, Map<String, Integer> distances) {
		GraphWeighted graphe = new GraphWeighted(true);

		// creation des nodes
		List<NodeWeighted> nodes = new ArrayList<>();
		int identifiant = 0;
		for (Valve valve : valves) {
			identifiant++;
			NodeWeighted node = new NodeWeighted(identifiant, valve.getNom());
			nodes.add(node);
		}

		// creation des edges
		for (Valve valve : valves) {
			List<Valve> voisins = valve.getVoisins();
			for (Valve voisin : voisins) {
				NodeWeighted node1 = getNodeByName(valve.getNom(), nodes);
				NodeWeighted node2 = getNodeByName(voisin.getNom(), nodes);
				graphe.addEdge(node1, node2, 1);
			}
		}

		// calcul plus court chemin entre valves à flux positif ou AA
		for (Valve valve1 : valves) {
			for (Valve valve2 : valves)
				if (valve1 != valve2 && ((valve1.getFlowRate() > 0 && valve2.getFlowRate() > 0) || (valve1.getNom().equals("AA") | valve2.getNom().equals("AA")))) {
					graphe.resetNodesVisited();
					NodeWeighted node1 = getNodeByName(valve1.getNom(), nodes);
					NodeWeighted node2 = getNodeByName(valve2.getNom(), nodes);
					String[] res = graphe.DijkstraShortestPath(node1, node2);

					distances.put(valve1.getNom() + ":" + valve2.getNom(), Integer.valueOf(StringUtils.split(res[1], '.')[0]));

				}
		}

		return distances;
	}

	private NodeWeighted getNodeByName(String nom, List<NodeWeighted> nodes) {
		for (NodeWeighted node : nodes) {
			if (node.getName().equals(nom)) {
				return node;
			}

		}
		return null;
	}

	private List<String> calculeTousCheminsPossibles(List<Valve> valvesFluxPositif, Map<String, Integer> distances, List<Valve> valves) {
		List<String> names = new ArrayList<String>();
		for (Valve valve : valvesFluxPositif) {
			names.add(valve.getNom());
		}
		int n = names.size();
		List<String> a = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			a.add(names.get(i));
		}
		Set<String> b = new HashSet<>(a);
		Set<Set<String>> combinaisons = Sets.combinations(b, 7);

		System.out.println("nb combinaisons: " + combinaisons.size());
		int cpt = 0;
		for (Set<String> combi : combinaisons) {
			cpt++;
			if (cpt % 100 == 0) {
				System.out.println(cpt);
			}
			Collection<List<String>> permutations = Collections2.permutations(combi);
			for (List<String> permutation : permutations) {
				String perm = "AA";
				for (String p : permutation) {
					perm = perm + p;
				}
				if (perm.equals("AAKMICGBOEKTAKNT")) {
					System.out.println();
				}
				int pressure = calculerPression(distances, perm, valves);
				if (pressure > max) {
					max = pressure;
					System.err.println("max actuel: " + max);
					System.err.println(perm);
				}
			}
		}

		return null;

	}

	private int calculerPression(Map<String, Integer> distances, String chemin, List<Valve> valves) {
		int pressure = 0;
		int dureeFonctionnement = 30;
		for (int i = 0; i < chemin.length() - 2; i = i + 2) {
			String depart = "" + chemin.charAt(i) + chemin.charAt(i + 1);
			String arrivee = "" + chemin.charAt(i + 2) + chemin.charAt(i + 3);
			String trajet = depart + ":" + arrivee;
			Integer distanceDuTrajet = distances.get(trajet);
			dureeFonctionnement = dureeFonctionnement - distanceDuTrajet - 1; // 1 est la durée d'ouverture de la valve
			Valve valve = getValveByName(valves, arrivee); // on considere que la premiere valve est TOUJOURS à 0;
			if (dureeFonctionnement <=0) { // deplacement impossible, donc on s'arrete à cette valve
				return pressure;
			}

			pressure = pressure + dureeFonctionnement * valve.getFlowRate();
		}

		return pressure;

	}

	private List<Valve> trouverValvesDebitPositif(List<Valve> valves) {
		return valves.stream().filter(v -> v.getFlowRate() > 0).collect(Collectors.toList());
	}


	private List<Valve> creationValves(List<String> res) {

		List<Valve> valves = new ArrayList<Valve>();

		// creation des Valves sans info
		for (String ligne : res) {
			String debutLigne = StringUtils.split(ligne, ";")[0];
			String nomValve = StringUtils.split(debutLigne, ' ')[1];
			int flowValve = Integer.valueOf(StringUtils.split(StringUtils.split(StringUtils.split(debutLigne, ' ')[4], '=')[1], '=')[0]);

			Valve valve = new Valve();
			valve.setFlowRate(flowValve);
			valve.setNom(nomValve);
			valves.add(valve);
		}

		// Ajout des infos des voisins dans les valves
		for (int j = 0; j < res.size(); j++) {
			String ligne = res.get(j);
			String finLigne = StringUtils.split(ligne, ";")[1];

			String[] nomVoisins = StringUtils.split(finLigne, ' ');
			String nomsVoisins = "";
			for (int i = 4; i < nomVoisins.length; i++) {
				nomsVoisins = nomsVoisins + nomVoisins[i];
			}
			String[] voisins = StringUtils.split(nomsVoisins, ',');

			Valve valve = valves.get(j);

			for (int k = 0; k < voisins.length; k++) {
				Valve valveVoisine = getValveByName(valves, voisins[k]);
				valve.getVoisins().add(valveVoisine);

			}

		}

		return valves;
	}

	private Valve getValveByName(List<Valve> valves, String string) {
		for (Valve valve : valves) {
			if (valve.getNom().equals(string)) {
				return valve;
			}
		}
		//System.out.println("valve non trouvée");
		return null;
	}
	private Valve getValveByName(Set<Valve> valves, String string) {
		for (Valve valve : valves) {
			if (valve.getNom().equals(string)) {
				return valve;
			}
		}
		//System.out.println("valve non trouvée");
		return null;
	}

}