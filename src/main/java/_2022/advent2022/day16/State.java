package advent2022.day16;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class State {

private Set<Valve>	 valvesOuvertes;
private int pressure;

private int time;
private Valve valveActuelle;

private int timeElephant;
private Valve valveActuelleElephant;






public State() {
	super();
	this.valvesOuvertes=new HashSet<Valve>();
}



public int getTimeElephant() {
	return timeElephant;
}



public void setTimeElephant(int timeElephant) {
	this.timeElephant = timeElephant;
}



public Valve getValveActuelleElephant() {
	return valveActuelleElephant;
}


public void setValveActuelleElephant(Valve valveActuelleElephant) {
	this.valveActuelleElephant = valveActuelleElephant;
}


public Valve getValveActuelle() {
	return valveActuelle;
}


public void setValveActuelle(Valve valveActuelle) {
	this.valveActuelle = valveActuelle;
}


public int getPressure() {
	return pressure;
}


public void setPressure(int pressure) {
	this.pressure = pressure;
}


public Set<Valve> getValvesOuvertes() {
	return valvesOuvertes;
}
public void setValvesOuvertes(Set<Valve> valvesOuvertes) {
	this.valvesOuvertes = valvesOuvertes;
}
public int getTime() {
	return time;
}
public void setTime(int time) {
	this.time = time;
}
@Override
public int hashCode() {
	return Objects.hash(pressure, valvesOuvertes);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	State other = (State) obj;
	return pressure == other.pressure && Objects.equals(valvesOuvertes, other.valvesOuvertes);
}
@Override
public String toString() {
	return "State [valvesOuvertes=" + valvesOuvertes + ", pressure=" + pressure + ", time=" + time + ", valveActuelle="
			+ valveActuelle + ", timeElephant=" + timeElephant + ", valveActuelleElephant=" + valveActuelleElephant
			+ "]";
}
	
	
	
}
