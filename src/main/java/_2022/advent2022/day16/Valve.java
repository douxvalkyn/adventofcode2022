package advent2022.day16;

import java.util.ArrayList;
import java.util.List;

public class Valve {

	private int flowRate;
	private List<Valve> voisins;
	private String nom;



	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Valve() {
		super();
		this.flowRate = 0;
		this.voisins = new ArrayList<Valve>();
	}

	public int getFlowRate() {
		return flowRate;
	}

	public void setFlowRate(int flowRate) {
		this.flowRate = flowRate;
	}

	public List<Valve> getVoisins() {
		return voisins;
	}

	public void setVoisins(List<Valve> voisins) {
		this.voisins = voisins;
	}

	@Override
	public String toString() {
		return "Valve [flowRate=" + flowRate + ", nom=" + nom + "]";
	}



}
