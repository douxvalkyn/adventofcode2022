package advent2022.day08;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day08 {

	private static Logger logger = LoggerFactory.getLogger(Day08.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day08 day = new Day08();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day08.txt";
		List<String> res = Outil.importationString(file);
		// System.out.println(res);
		Grille grille = creationGrille(res);
		analyseVisibilite2(grille);
		// grille.affichageEtat();
		rechercheMaxSchenicScore(grille);

	}

	private void rechercheMaxSchenicScore(Grille grille) {
		List<Case> cases = grille.getCases();
		int max = 0;
		Case cellMax = null;
		for (Case cell : cases) {
			if (!cell.getEtat().equals("_")) {
				if (Integer.valueOf(cell.getEtat()) > max) {
					max = Integer.valueOf(cell.getEtat());
					cellMax = cell;
				}
			}
		}
		System.out.println(cellMax);
		System.out.println(max);
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day08.txt";
		List<String> res = Outil.importationString(file);
		// System.out.println(res);
		Grille grille = creationGrille(res);
		analyseVisibilite(grille);
		// grille.affichageEtat();
		calculNbVisibles(grille);

	}

	private void analyseVisibilite2(Grille grille) {

		List<Case> cases = grille.getCases();
		for (Case cell : cases) {
			if (cell.getX() >= 1 && cell.getY() >= 1 && cell.getX() < (grille.getNbCol() - 1) && cell.getY() < (grille.getNbLignes() - 1)) {
				List<Case> casesAdjEst = cell.getCasesAdjacentesEstTouteLaLigneJusquAuBord(grille);
				boolean est = true;
				int nbEst = 0;
				for (Case caseAdj : casesAdjEst) {
					if (caseAdj.getValeur() >= cell.getValeur() && est) {
						nbEst++;
						est = false;
					}
					if (caseAdj.getValeur() < cell.getValeur() && est) {
						nbEst++;
					} else {
						est = false;
					}
				}
				List<Case> casesAdjOuest = cell.getCasesAdjacentesOuestTouteLaLigneJusquAuBord(grille);
				boolean ouest = true;
				int nbOuest = 0;
				for (Case caseAdj : casesAdjOuest) {
					if (caseAdj.getValeur() >= cell.getValeur() && ouest) {
						nbOuest++;
						ouest = false;
					}
					if (caseAdj.getValeur() < cell.getValeur() && ouest) {
						nbOuest++;
					} else {
						ouest = false;
					}
				}
				List<Case> casesAdjNord = cell.getCasesAdjacentesNordTouteLaLigneJusquAuBord(grille);
				boolean nord = true;
				int nbNord = 0;
				for (Case caseAdj : casesAdjNord) {
					if (caseAdj.getValeur() >= cell.getValeur() && nord) {
						nbNord++;
						nord = false;
					}
					if (caseAdj.getValeur() < cell.getValeur() && nord) {
						nbNord++;
					} else {
						nord = false;
					}
				}
				List<Case> casesAdjSud = cell.getCasesAdjacentesSudTouteLaLigneJusquAuBord(grille);
				boolean sud = true;
				int nbSud = 0;
				for (Case caseAdj : casesAdjSud) {
					if (caseAdj.getValeur() >= cell.getValeur() && sud) {
						nbSud++;
						sud = false;
					}
					if (caseAdj.getValeur() < cell.getValeur() && sud) {
						nbSud++;
					} else {
						sud = false;
					}
				}

				int scenicScore = nbOuest * nbEst * nbNord * nbSud;
				cell.setEtat("" + scenicScore);

			}

		}

	}

	private void calculNbVisibles(Grille grille) {
		List<Case> cases = grille.getCases();
		int sum = 0;
		for (Case cell : cases) {
			if (cell.getEtat().equals(".") | cell.getEtat().equals("_")) {
				sum++;
			}

		}
		System.out.println(sum);
	}

	private void analyseVisibilite(Grille grille) {

		List<Case> cases = grille.getCases();
		for (Case cell : cases) {
			if (cell.getX() >= 1 && cell.getY() >= 1 && cell.getX() < (grille.getNbCol() - 1) && cell.getY() < (grille.getNbLignes() - 1)) {
				List<Case> casesAdjEst = cell.getCasesAdjacentesEstTouteLaLigneJusquAuBord(grille);
				boolean est = true;
				for (Case caseAdj : casesAdjEst) {
					if (caseAdj.getValeur() >= cell.getValeur()) {
						est = false;
					}
				}
				List<Case> casesAdjOuest = cell.getCasesAdjacentesOuestTouteLaLigneJusquAuBord(grille);
				boolean ouest = true;
				for (Case caseAdj : casesAdjOuest) {
					if (caseAdj.getValeur() >= cell.getValeur()) {
						ouest = false;
					}
				}
				List<Case> casesAdjNord = cell.getCasesAdjacentesNordTouteLaLigneJusquAuBord(grille);
				boolean nord = true;
				for (Case caseAdj : casesAdjNord) {
					if (caseAdj.getValeur() >= cell.getValeur()) {
						nord = false;
					}
				}
				List<Case> casesAdjSud = cell.getCasesAdjacentesSudTouteLaLigneJusquAuBord(grille);
				boolean sud = true;
				for (Case caseAdj : casesAdjSud) {
					if (caseAdj.getValeur() >= cell.getValeur()) {
						sud = false;
					}
				}

				if (est || ouest || nord || sud) {
					cell.setEtat(".");
				} else {
					cell.setEtat("X");
				}

			}

		}

	}

	private Grille creationGrille(List<String> res) {
		Integer taille = Integer.valueOf(res.get(0).length());
		Grille grille = new Grille(taille, taille);
		for (int j = 0; j < res.size(); j++) {
			String ligne = res.get(j);
			for (int i = 0; i < ligne.length(); i++) {
				grille.getCase(i, j).setValeur(Integer.valueOf("" + ligne.charAt(i)));
			}
		}
		// grille.affichageValeur();
		return grille;
	}

}