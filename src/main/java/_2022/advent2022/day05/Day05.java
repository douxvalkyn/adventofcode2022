package advent2022.day05;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day05 {

	private static Logger logger = LoggerFactory.getLogger(Day05.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day05 day = new Day05();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day05.txt";
		List<String> res = Outil.importationString(file);

		List<List<String>> listes = creationListesAuto();

		List<String> instructions = new ArrayList<String>();
		instructions = recuperationInstructions(res, instructions);

		listes = applicationInstructions2(listes, instructions);
		System.out.println("run2: " + listes);
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day05.txt";
		List<String> res = Outil.importationString(file);

		// // creation des listes
		List<List<String>> listes = creationListesAuto();

		// recuperation des instructions
		List<String> instructions = new ArrayList<String>();
		instructions = recuperationInstructions(res, instructions);

		// application des instructions aux listes
		listes = applicationInstructions(listes, instructions);
		System.out.println("run1: " + listes);

	}

	private List<List<String>> applicationInstructions2(List<List<String>> listes, List<String> instructions) {

		for (int i = 0; i < instructions.size(); i++) {
			String instruction = instructions.get(i);
			int nbElementsDeplaces = Integer.valueOf(StringUtils.split(instruction, ' ')[1]);
			int from = Integer.valueOf(StringUtils.split(instruction, ' ')[3]);
			int to = Integer.valueOf(StringUtils.split(instruction, ' ')[5]);

			List<String> elementsADeplacer = new ArrayList<String>();
			for (int j = 0; j < nbElementsDeplaces; j++) {
				int indicetoMove = (listes.get(from - 1).size()) - 1;
				// System.out.println(instruction);
				String toMove = listes.get(from - 1).get(indicetoMove);
				elementsADeplacer.add(toMove);
				listes.get(from - 1).remove(indicetoMove);
			}

			// inverser la liste
			Collections.reverse(elementsADeplacer);

			for (int j = 0; j < nbElementsDeplaces; j++) {
				listes.get(to - 1).add(elementsADeplacer.get(j));
			}

		}

		return listes;
	}

	private List<List<String>> applicationInstructions(List<List<String>> listes, List<String> instructions) {

		for (int i = 0; i < instructions.size(); i++) {
			String instruction = instructions.get(i);
			int nbElementsDeplaces = Integer.valueOf(StringUtils.split(instruction, ' ')[1]);
			int from = Integer.valueOf(StringUtils.split(instruction, ' ')[3]);
			int to = Integer.valueOf(StringUtils.split(instruction, ' ')[5]);

			for (int j = 0; j < nbElementsDeplaces; j++) {
				int IndicetoMove = (listes.get(from - 1).size()) - 1;
				String toMove = listes.get(from - 1).get(IndicetoMove);
				listes.get(from - 1).remove(IndicetoMove);
				listes.get(to - 1).add(toMove);

			}

		}

		return listes;
	}

	private List<String> recuperationInstructions(List<String> res, List<String> instructions) {
		for (int i = 0; i < res.size(); i++) {
			instructions.add(res.get(i));
		}

		return instructions;
	}

	private List<List<String>> creationListesAuto() {
		String file = "src/main/resources/advent2022/day05_.txt";
		List<String> resultat = Outil.importationString(file);

		List<List<String>> listes = new ArrayList<List<String>>();
		String[] temp = StringUtils.split(resultat.get(resultat.size() - 1), ' ');
		Integer nbListes = Integer.valueOf(temp[temp.length - 1]);
		resultat.remove(resultat.size() - 1);
		for (int i = 0; i < nbListes; i++) {
			List<String> maListe = new ArrayList<>();

			for (String res : resultat) {
				String lettre = "" + res.charAt(1 + 4 * i);
				if (!lettre.equals(" ")) {
					maListe.add(lettre);
				}
			}
			// inverser la liste
			Collections.reverse(maListe);
			listes.add(maListe);
		}
		return listes;
	}

}
