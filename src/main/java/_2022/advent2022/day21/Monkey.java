package advent2022.day21;

import java.util.Date;

import advent2021.day23.Amphipode;

public class Monkey {

	
	private String name;
	private String childName1;
	private String childName2;
	private Long value=-1L;
	private String operation;
	
	
	public Monkey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Monkey(Monkey monkey) {
		this.name = monkey.name;
		this.childName1 = monkey.childName1;
		this.childName2 = monkey.childName2;
		this.value = monkey.value;
		this.operation = monkey.operation;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getChildName1() {
		return childName1;
	}


	public void setChildName1(String childName1) {
		this.childName1 = childName1;
	}


	public String getChildName2() {
		return childName2;
	}


	public void setChildName2(String childName2) {
		this.childName2 = childName2;
	}


	public Long getValue() {
		return value;
	}


	public void setValue(Long value) {
		this.value = value;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	@Override
	public String toString() {
		return "Monkey [name=" + name + ", childName1=" + childName1 + ", childName2=" + childName2 + ", value=" + value
				+ ", operation=" + operation + "]";
	}
	
	
}
