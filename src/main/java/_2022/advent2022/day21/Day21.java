package advent2022.day21;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2017.outils.Outil;

public class Day21 {

	private static Logger logger = LoggerFactory.getLogger(Day21.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day21.class.getSimpleName() + "]");
		Day21 day = new Day21();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day21.txt";
		List<String> input = Outil.importationString(file);

		long valeurObjectif = determinerValeurObjectifAEgaliser(input);
		List<Monkey> monkeys = creationMonkeys(input);

		// changement, le monkey humn: on doit trouver sa valeur
		Monkey human = getByName(monkeys, "humn");
		human.setValue(null);

		Monkey monkeyRoot = getByName(monkeys, "root");

		// on lance 1 fois le run1 pour connaitre les valeurs qui dependent de humn et les valeurs fixes
		yell(getByName(monkeys, "root"), monkeys);

		// on redescend la liste des enfants de root pour lesquelles les valeurs sont nulles
		Monkey monkeyEnCours;
		if (getByName(monkeys, monkeyRoot.getChildName1()).getValue() == null) {
			monkeyEnCours = getByName(monkeys, monkeyRoot.getChildName1());
		} else {
			monkeyEnCours = getByName(monkeys, monkeyRoot.getChildName2());
		}
		boolean go = true;
		while (go) {
			String operation = monkeyEnCours.getOperation();
			String child1 = monkeyEnCours.getChildName1();
			String child2 = monkeyEnCours.getChildName2();
			Long valeur1 = getByName(monkeys, child1).getValue();
			Long valeur2 = getByName(monkeys, child2).getValue();
			long valeur = 0;
			if (valeur1 == null) {
				valeur = valeur2;
			}
			if (valeur2 == null) {
				valeur = valeur1;
			}

			switch (operation) {
				case "-":
					if (valeur1 != null) {
						valeurObjectif = valeur - valeurObjectif;//
					} else {
						valeurObjectif = valeurObjectif + valeur;//
					}
					break;
				case "+":
					if (valeur1 != null) {
						valeurObjectif = valeurObjectif - valeur;//
					} else {
						valeurObjectif = valeurObjectif - valeur;//
					}
					break;
				case "/":
					if (valeur1 != null) {
						valeurObjectif = valeur / valeurObjectif;//
					} else {
						valeurObjectif = valeur * valeurObjectif;//
					}
					break;
				case "*":
					if (valeur1 != null) {
						valeurObjectif = valeurObjectif / valeur;
					} else {
						valeurObjectif = valeurObjectif / valeur;
					}
					break;
				default:
					break;
			}

			if (monkeyEnCours.getChildName1().equals("humn") || monkeyEnCours.getChildName2().equals("humn")) {
				go = false;
			} else {
				if (getByName(monkeys, monkeyEnCours.getChildName1()).getValue() == null) {
					monkeyEnCours = getByName(monkeys, monkeyEnCours.getChildName1());
				} else {
					monkeyEnCours = getByName(monkeys, monkeyEnCours.getChildName2());
				}
			}

		}
		System.out.println(valeurObjectif);

		// verif
		monkeys = creationMonkeys(input);
		getByName(monkeys, "humn").setValue(valeurObjectif);
		getByName(monkeys, "root").setOperation("=");
		yell(getByName(monkeys, "root"), monkeys);

	}

	private long determinerValeurObjectifAEgaliser(List<String> input) {
		// on lance le run1 2 fois pour connaître le nombre à trouver pour l'égalité du root
		List<Monkey> monkeys = creationMonkeys(input);
		Long valeur1 = yell(getByName(monkeys, getByName(monkeys, "root").getChildName1()), monkeys);
		Long valeur2 = yell(getByName(monkeys, getByName(monkeys, "root").getChildName2()), monkeys);
		monkeys = creationMonkeys(input);
		getByName(monkeys, "humn").setValue(1000L);
		Long valeur1bis = yell(getByName(monkeys, getByName(monkeys, "root").getChildName1()), monkeys);
		Long valeur2bis = yell(getByName(monkeys, getByName(monkeys, "root").getChildName2()), monkeys);
		long valeurObjectif = 0;
		if (valeur1.equals(valeur1bis)) {
			valeurObjectif = valeur1;
		} else {
			valeurObjectif = valeur2;
		}
		return valeurObjectif;
	}

	private List<Monkey> copie(List<Monkey> monkeysSave) {
		List<Monkey> newMonkeys = new ArrayList<Monkey>();
		for (Monkey monkey : monkeysSave) {
			Monkey newMonkey = new Monkey(monkey);
			newMonkeys.add(newMonkey);
		}
		return newMonkeys;
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day21a.txt";
		List<String> input = Outil.importationString(file);
		List<Monkey> monkeys = creationMonkeys(input);

		Long valeur = yell(getByName(monkeys, "root"), monkeys);

		System.out.println(valeur);

	}

	private Long yell(Monkey monkey, List<Monkey> monkeys) {
		if (monkey.getValue() == null) {
			return null;
		}
		if (monkey.getValue() != -1) {
			return monkey.getValue();
		}
		Monkey monkeyChild1 = getByName(monkeys, monkey.getChildName1());
		Monkey monkeyChild2 = getByName(monkeys, monkey.getChildName2());
		Long yell1 = yell(monkeyChild1, monkeys);
		Long yell2 = yell(monkeyChild2, monkeys);
		Long valeur = 0L;
		if (yell1 == null | yell2 == null) {
			monkey.setValue(null);
			return null;
		}
		switch (monkey.getOperation()) {
			case "+":
				valeur = yell1 + yell2;
				break;
			case "-":
				valeur = yell1 - yell2;
				break;
			case "*":
				valeur = yell1 * yell2;
				break;
			case "/":
				valeur = yell1 / yell2;
				break;
			case "=":
				if (yell1.equals(yell2)) {
					System.err.println("ok");
				}
				break;
			default:
				break;
		}
		monkey.setValue(valeur);
		return valeur;
	}

	private Monkey getByName(List<Monkey> monkeys, String name) {
		for (Monkey monkey : monkeys) {
			if (monkey.getName().equals(name)) {
				return monkey;
			}
		}
		return null;
	}

	private List<Monkey> creationMonkeys(List<String> input) {
		List<Monkey> monkeys = new ArrayList();
		for (String ligne : input) {
			String name = StringUtils.split(ligne, ':')[0];
			Monkey monkey = new Monkey();
			monkey.setName(name);
			if (StringUtils.split(ligne, ':')[1].trim().length() <= 6) {
				Long value = Long.valueOf(StringUtils.split(ligne, ':')[1].trim());
				monkey.setValue(value);
			} else {
				String childName1 = StringUtils.split(StringUtils.split(ligne, ':')[1], ' ')[0];
				String operation = StringUtils.split(StringUtils.split(ligne, ':')[1], ' ')[1];
				String childName2 = StringUtils.split(StringUtils.split(ligne, ':')[1], ' ')[2];
				monkey.setChildName1(childName1);
				monkey.setChildName2(childName2);
				monkey.setOperation(operation);
			}
			monkeys.add(monkey);

		}
		return monkeys;
	}

}