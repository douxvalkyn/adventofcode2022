package advent2022.day13;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day13 {

	private static Logger logger = LoggerFactory.getLogger(Day13.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day13 day = new Day13();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	public void run2() {
		String file = "src/main/resources/advent2022/day13.txt";
		List<String> res = Outil.importationString(file);
		
		res=supprimeLignesBlanches(res);
		res=ajouteDividers(res);
		
		List<String>listeOrdonnee= new ArrayList<String>();
		while (res.size()>0) {
			String min=res.get(0);
				for (String paquet:res) {
					if (min != paquet) {
						String petitPaquet = plusPetitDeDeuxListes(min, paquet);
						min=petitPaquet;
						
					}
				}	
			listeOrdonnee.add(min);
			res.remove(min);
			
		}
		System.out.println(listeOrdonnee);
		
		int indice2=rechercheIndiceDivider2(listeOrdonnee);
		int indice6=rechercheIndiceDivider6(listeOrdonnee);
		System.out.println("Run2: " + indice2*indice6);
	}



	
	

	private int rechercheIndiceDivider2(List<String> listeOrdonnee) {
		for (int i=0;i<listeOrdonnee.size();i++) {
			if (listeOrdonnee.get(i).equals("[[2]]")) {
				return i+1;
			}
		}
		return 0;
	}
	private int rechercheIndiceDivider6(List<String> listeOrdonnee) {
		for (int i=0;i<listeOrdonnee.size();i++) {
			if (listeOrdonnee.get(i).equals("[[6]]")) {
				return i+1;
			}
		}
		return 0;
	}




	private List<String> ajouteDividers(List<String> res) {
		res.add("[[2]]");
		res.add("[[6]]");
		return res;
	}



	private List<String> supprimeLignesBlanches(List<String> res) {
		List<String> resultat = new ArrayList<String>();
		for (String ligne: res) {
			if (!ligne.equals("")) {
				resultat.add(ligne);
			}
		}
		return resultat;
	}




	public String plusPetitDeDeuxListes(String listeGauche, String listeDroite) {
		System.out.println("Compare " + listeGauche + " " + listeDroite);
		String resultatComparaison = comparerListes(listeGauche, listeDroite);
		System.out.println(resultatComparaison);
		if (resultatComparaison.equals("right")) {
			return listeGauche;
		}else {
			return listeDroite;
		}
	}
	
	
	public void run1() {
		String file = "src/main/resources/advent2022/day13.txt";
		List<String> res = Outil.importationString(file);
		int sum=0;
		int indice=0;
		for (int pas = 0; pas < res.size(); pas = pas + 3) {
			indice++;
			String listeGauche = res.get(pas);
			String listeDroite = res.get(pas + 1);
			System.out.println("Compare " + listeGauche + " " + listeDroite);
			String resultatComparaison = comparerListes(listeGauche, listeDroite);
			System.out.println(resultatComparaison);
			if (resultatComparaison.equals("right")) {
				sum=sum+indice;
			}
		}
		System.out.println(sum);
		

	}

	public String comparerListes(String listeGauche, String listeDroite) {
		boolean go = true;
		String ordre = "inconnu";
		while (go) {
			if (listeGauche.length() == 2 && listeDroite.length() == 2) { // 2 listes vides
				return "inconnu";
			}
			if (listeGauche.length() == 2 && listeDroite.length() > 2) { // 1 liste vide
				return "right";
			}
			if (listeGauche.length() > 2 && listeDroite.length() == 2) { // 1 liste vides
				return "not right";
			}

			String aComparerGauche = recupereDebutListe(listeGauche);
			String aComparerDroite = recupereDebutListe(listeDroite);
			listeGauche = recupereFinListe(listeGauche);
			listeDroite = recupereFinListe(listeDroite);
			System.out.println("  - Compare " + aComparerGauche + " vs " + aComparerDroite);
			ordre = comparer(aComparerGauche, aComparerDroite);
			if (ordre.equals("right")) {
				return ordre;
			}
			if (ordre.equals("not right")) {
				return ordre;
			}
			if (listeGauche == null && listeDroite == null) {
				return "inconnu";
			}
			if (listeGauche == null && listeDroite != null) {
				return "right";
			}
			if (listeGauche != null && listeDroite == null) {
				return "not right";
			}
		}

		return ordre;
	}

	private String recupereFinListe(String liste) {
		String fin = "";

		// supprimer le [ initial et le ] final
		liste = StringUtils.substring(liste, 1, liste.length() - 1);

		// si 1 seul element, il n'y a pas de fin de liste
//		if (liste.length() == 1) {
//			return null;
//		}
		if (!liste.contains("[") && !liste.contains("]") && !liste.contains(",") ) {
			return null;
		}

		// determiner le caractere qui doit finir le premier element
		char elementFin = ',';
		if (liste.charAt(0) == '[') {
			elementFin = ']';
		}

		int nbCrochets = 0;
		int indexFin = 0;
		for (int i = 0; i < liste.length(); i++) {
			if (liste.charAt(i) == '[') {
				nbCrochets++;
			}
			if (liste.charAt(i) == ']') {
				nbCrochets--;
			}
			if (liste.charAt(i) == elementFin && nbCrochets == 0 & indexFin == 0) {
				indexFin = i;
			}
		}

		if (elementFin == ',') {
			fin = "[" + StringUtils.substring(liste, indexFin + 1) + "]";
		}
		if (elementFin == ']') {
			fin = "[" + StringUtils.substring(liste, indexFin + 2) + "]";
		}

		if (fin.equals("[]")) {
			return null;
		}

		return fin;
	}

	private String comparer(String aComparerGauche, String aComparerDroite) {
		// if (aComparerDroite.charAt(aComparerDroite.length() - 1) == ',') {
		// aComparerDroite = aComparerDroite.substring(0, aComparerDroite.length() - 1);
		// }
		// if (aComparerGauche.charAt(aComparerGauche.length() - 1) == ',') {
		// aComparerGauche = aComparerGauche.substring(0, aComparerGauche.length() - 1);
		// }

		// check si les comparaisons concernent des integers ou des lists:

		// 1. comparaison de lists
		if (aComparerDroite.charAt(0) == '[' && aComparerDroite.charAt(aComparerDroite.length() - 1) == ']' && aComparerGauche.charAt(0) == '['
			&& aComparerGauche.charAt(aComparerGauche.length() - 1) == ']') {
			String ordre = comparerListes(aComparerGauche, aComparerDroite);
			return ordre;
		}

		// 2. mixed type
		if (aComparerDroite.charAt(0) == '[' && aComparerDroite.charAt(aComparerDroite.length() - 1) == ']'
			|| aComparerGauche.charAt(0) == '[' && aComparerGauche.charAt(aComparerGauche.length() - 1) == ']') {

			if (aComparerDroite.charAt(0) == '[' && aComparerDroite.charAt(aComparerDroite.length() - 1) == ']') { // liste à droite vs integer
				// convertir l'integer de gauche en list puis comparer les listes
				aComparerGauche = "[" + aComparerGauche + "]";
				String ordre = comparerListes(aComparerGauche, aComparerDroite);
				return ordre;
			}
			if (aComparerGauche.charAt(0) == '[' && aComparerGauche.charAt(aComparerGauche.length() - 1) == ']') { // liste à gauche vs integer
				// convertir l'integer de droite en list puis comparer les listes
				aComparerDroite = "[" + aComparerDroite + "]";
				String ordre = comparerListes(aComparerGauche, aComparerDroite);
				return ordre;
			}
		}

		// 3. comparaison de integers
		if (Integer.valueOf(aComparerGauche) < Integer.valueOf(aComparerDroite)) {
			return "right";
		}
		if (Integer.valueOf(aComparerGauche) > Integer.valueOf(aComparerDroite)) {
			return "not right";
		}
		if (Integer.valueOf(aComparerGauche) == Integer.valueOf(aComparerDroite)) {
			return "inconnu";
		}

		System.out.println("pb");
		return "inconnu";
	}

	private String recupereDebutListe(String liste) {
		String debut = "";

		// supprimer le [ initial et le ] final
		liste = StringUtils.substring(liste, 1, liste.length() - 1);

		// si la liste ne contient qu'un seul élement, on le renvoie
//		if (liste.length() == 1) {
//			return liste;
//		}
		if (!liste.contains("[") && !liste.contains("]") && !liste.contains(",") ) {
			return liste;
		}

		// recuperer le premier element de la liste
		int nbCrochets = 0;
		int indexFin = 0;

		// determiner le caractere qui doit finir le premier element
		char elementFin = ',';
		if (liste.charAt(0) == '[') {
			elementFin = ']';
		}

		for (int i = 0; i < liste.length(); i++) {
			if (liste.charAt(i) == '[') {
				nbCrochets++;
			}
			if (liste.charAt(i) == ']') {
				nbCrochets--;
			}
			if (liste.charAt(i) == elementFin && nbCrochets == 0 & indexFin == 0) {
				indexFin = i;
			}
		}

		if (elementFin == ',') {
			debut = StringUtils.substring(liste, 0, indexFin);
		}
		if (elementFin == ']') {
			debut = StringUtils.substring(liste, 0, indexFin + 1);
		}

		return debut;
	}

	private Packet analyse(Packet p, String liste) {
		Packet paquet = new Packet();
		if (p != null) {
			paquet = p;
		}
		if (p == null) {
			liste = StringUtils.substring(liste, 1, liste.length() - 1);
		}

		// if (StringUtils.countMatches(liste, '[')==1 && StringUtils.countMatches(liste, ']')==1 ) { //1 packet sans sous-packets
		// liste=StringUtils.substring(liste, 1, liste.length()-1);
		// String[] list = StringUtils.split(liste,',');
		// for (String carac:list) {
		// paquet.getIntegers().add(Integer.valueOf(carac));
		// }
		// return paquet;
		// }

		if (StringUtils.countMatches(liste, '[') == 0 && StringUtils.countMatches(liste, ']') == 0) { // only integers
			String[] aaa = StringUtils.split(liste, ',');
			for (String carac : aaa) {
				paquet.getIntegers().add(Integer.valueOf(carac));
			}
			return paquet;
		}

		if (liste.charAt(0) == '[') { // debut nouveau sous-packet
			String paquetEnString = "[";
			int i = 1;
			while (liste.charAt(i) != ']') {
				paquetEnString = paquetEnString + liste.charAt(i);
				i++;
			}
			paquetEnString = paquetEnString + "]";
			p = analyse(null, paquetEnString);
			paquet.getPackets().add(p);
			String stringRestant = StringUtils.substring(liste, i + 2);
			paquet = analyse(paquet, stringRestant);
			return paquet;
		}
		if (liste.charAt(0) != '[') { // ajout integer
			String integerEnString = "";
			int imax = 0;
			for (int i = 1; i < liste.length(); i++) {
				if (liste.charAt(i) != ',') {
					integerEnString = integerEnString + liste.charAt(i);
				}
				imax = i;
			}
			int integer = Integer.valueOf(integerEnString);
			paquet.getIntegers().add(integer);
			String stringRestant = StringUtils.substring(liste, imax + 1);
			paquet = analyse(paquet, stringRestant);
			return paquet;
		}

		return paquet;
	}

}