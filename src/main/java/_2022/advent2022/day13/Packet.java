package advent2022.day13;

import java.util.ArrayList;
import java.util.List;

public class Packet {

	
	private List<Packet> packets;
	private List<Integer> integers;
	
	
	public Packet() {
		super();
		List<Packet> p = new ArrayList<Packet>();
		packets=p;
		List<Integer> i= new ArrayList<Integer>();
		integers=i;
	}


	public List<Packet> getPackets() {
		return packets;
	}


	public void setPackets(List<Packet> packets) {
		this.packets = packets;
	}


	public List<Integer> getIntegers() {
		return integers;
	}


	public void setIntegers(List<Integer> integers) {
		this.integers = integers;
	}


	@Override
	public String toString() {
		return "Packet [packets=" + packets + ", integers=" + integers + "]";
	}
	
	
	
	
}
