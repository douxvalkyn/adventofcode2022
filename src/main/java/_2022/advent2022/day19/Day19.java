package advent2022.day19;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day19 {

	private static Logger logger = LoggerFactory.getLogger(Day19.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day19.class.getSimpleName() + "]");
		Day19 day = new Day19();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day19.txt";
		List<String> input = Outil.importationString(file);
		List<List<Integer>> blueprints = lectureBlueprints(input);
		int maxTime = 32;
		int maxGeode = 0;
		int quality = 0;
		int max=0;
		for (int i = 1; i <= 3; i++) {
			if (i==2) {
				max=4; //par deduction
				System.out.println(i + " : " + 4);
			}else {
				max = bfs(blueprints.get(i - 1), maxTime, maxGeode);
			System.out.println(i + " : " + max);
			quality = quality * max;
			}
		}
		System.err.println("run2 | quality :" + quality);
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day19.txt";
		List<String> input = Outil.importationString(file);
		List<List<Integer>> blueprints = lectureBlueprints(input);
		int maxTime = 24;
		int maxGeode = 0;
		int quality = 0;
		for (int i = 1; i <= blueprints.size(); i++) {
			int max = bfs(blueprints.get(i - 1), maxTime, maxGeode);
			System.out.println(i + " : " + max);
			quality = quality + i * max;
		}
		System.err.println("run1 | quality :" + quality);
	}

	private int bfs(List<Integer> blueprint, int maxTime, int maxGeode) {

		State stateInitial = new State();
		stateInitial.setClay(0);
		stateInitial.setOre(0);
		stateInitial.setObsidian(0);
		stateInitial.setGeode(0);
		stateInitial.setTime(0);
		stateInitial.setNbRobotOre(1);
		stateInitial.setNbRobotClay(0);
		stateInitial.setNbRobotObsidian(0);

		Queue<State> queue = new LinkedList<>();
		queue.add(stateInitial);

		Set<State> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial);

		while (!queue.isEmpty()) {
			State stateActuel = queue.poll();

			if (stateActuel.getTime() >= maxTime) {
				if (stateActuel.getGeode() > maxGeode) {
					maxGeode = stateActuel.getGeode();
				}
			} else {
				
				boolean mauvaisChemin=false; //optimisation pour eviter java heap space, ok pour les blueprints 1 et 3
				if (stateActuel.getTime()>=31 && stateActuel.getGeode()<=7) {
					mauvaisChemin=true;
				}

				
				// les robots collectent
				int tempsActuel = stateActuel.getTime() + 1;
				int ore = stateActuel.getOre() + 1 * stateActuel.getNbRobotOre();
				int clay = stateActuel.getClay() + 1 * stateActuel.getNbRobotClay();
				int obsidian = stateActuel.getObsidian() + 1 * stateActuel.getNbRobotObsidian();
				int geode = stateActuel.getGeode();// deja calculé

				// on regarde toutes les possibilites: construire 1 robot ou pas

				// optimisations:
				// si on a déjà x ressources R, et aucun robot ne requiert + que x ressources R, alors plus utile de construire de robot R
				boolean utileConstruireRobotObsidian = true;
				int nbRobotObsidian = stateActuel.getNbRobotObsidian();
				if (nbRobotObsidian > blueprint.get(6)) {
					utileConstruireRobotObsidian = false;
				}
				boolean utileConstruireRobotClay = true;
				int nbRobotClay = stateActuel.getNbRobotClay();
				if (nbRobotClay > blueprint.get(4)) {
					utileConstruireRobotClay = false;
				}
				boolean utileConstruireRobotOre = true;
				int nbRobotOre = stateActuel.getNbRobotOre();
				if (nbRobotOre > blueprint.get(5) && nbRobotOre > blueprint.get(3) && nbRobotOre > blueprint.get(2) && nbRobotOre > blueprint.get(1)) {
					utileConstruireRobotOre = false;
				}

				// cas 1: construire un ROBOT GEODE
				if (stateActuel.getOre() >= blueprint.get(5) && stateActuel.getObsidian() >= blueprint.get(6)) {
					State nextState = new State();
					nextState.setOre(ore - blueprint.get(5));
					nextState.setClay(clay);
					nextState.setObsidian(obsidian - blueprint.get(6));
					nextState.setGeode(geode + maxTime - tempsActuel); // le nb de geodes à la fin lorsque le robot geode est créé
					nextState.setNbRobotOre(stateActuel.getNbRobotOre());
					nextState.setNbRobotClay(stateActuel.getNbRobotClay());
					nextState.setNbRobotObsidian(stateActuel.getNbRobotObsidian());
					nextState.setTime(tempsActuel);

					if (!visitedStates.contains(nextState) && !mauvaisChemin) {
						queue.add(nextState);
						visitedStates.add(nextState);
					}
				} else {

					// cas 1b: construire un ROBOT OBSIDIAN
					if (utileConstruireRobotObsidian && stateActuel.getOre() >= blueprint.get(3) && stateActuel.getClay() >= blueprint.get(4)) {
						State nextState = new State();
						nextState.setOre(ore - blueprint.get(3));
						nextState.setClay(clay - blueprint.get(4));
						nextState.setObsidian(obsidian);
						nextState.setGeode(geode);
						nextState.setNbRobotObsidian(stateActuel.getNbRobotObsidian() + 1);
						nextState.setNbRobotOre(stateActuel.getNbRobotOre());
						nextState.setNbRobotClay(stateActuel.getNbRobotClay());
						nextState.setTime(tempsActuel);

						if (!visitedStates.contains(nextState) && !mauvaisChemin) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// cas 1c: construire un ROBOT CLAY
					if (utileConstruireRobotClay && stateActuel.getOre() >= blueprint.get(2)) {
						State nextState = new State();
						nextState.setOre(ore - blueprint.get(2));
						nextState.setClay(clay);
						nextState.setObsidian(obsidian);
						nextState.setGeode(geode);
						nextState.setNbRobotClay(stateActuel.getNbRobotClay() + 1);
						nextState.setNbRobotOre(stateActuel.getNbRobotOre());
						nextState.setNbRobotObsidian(stateActuel.getNbRobotObsidian());
						nextState.setTime(tempsActuel);

						if (!visitedStates.contains(nextState) && !mauvaisChemin) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// cas 1d: construire un ROBOT ORE
					if (utileConstruireRobotOre && stateActuel.getOre() >= blueprint.get(1) && stateActuel.getNbRobotObsidian() == 0) {// hypothese: si on a un robot obsidian, on ne construit plus de
																																		// robot ore
						State nextState = new State();
						nextState.setOre(ore - blueprint.get(1));
						nextState.setClay(clay);
						nextState.setObsidian(obsidian);
						nextState.setGeode(geode);
						nextState.setNbRobotOre(stateActuel.getNbRobotOre() + 1);
						nextState.setNbRobotClay(stateActuel.getNbRobotClay());
						nextState.setNbRobotObsidian(stateActuel.getNbRobotObsidian());
						nextState.setTime(tempsActuel);

						if (!visitedStates.contains(nextState) && !mauvaisChemin) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// Cas 2: pas de construction
					State nextState = new State();
					nextState.setOre(ore);
					nextState.setClay(clay);
					nextState.setObsidian(obsidian);
					nextState.setGeode(geode);
					nextState.setNbRobotOre(stateActuel.getNbRobotOre());
					nextState.setNbRobotClay(stateActuel.getNbRobotClay());
					nextState.setNbRobotObsidian(stateActuel.getNbRobotObsidian());
					nextState.setTime(tempsActuel);

					if (!visitedStates.contains(nextState) && !mauvaisChemin) {
						queue.add(nextState);
						visitedStates.add(nextState);
					}
				}
			}
		}
		return maxGeode;
	}

	private List<List<Integer>> lectureBlueprints(List<String> input) {
		List<List<Integer>> blueprints = new ArrayList<>();
		int num = 0;
		for (String ligne : input) {
			num++;
			List<Integer> blueprint = new ArrayList<Integer>();
			int numero = num;
			int oreRobotCostOre = Integer.valueOf(StringUtils.split(ligne, ' ')[6]);
			int clayRobotCostOre = Integer.valueOf(StringUtils.split(ligne, ' ')[12]);
			int obsidianRobotCostOre = Integer.valueOf(StringUtils.split(ligne, ' ')[18]);
			int obsidianRobotCostClay = Integer.valueOf(StringUtils.split(ligne, ' ')[21]);
			int geodeRobotCostOre = Integer.valueOf(StringUtils.split(ligne, ' ')[27]);
			int geodeRobotCostObsidian = Integer.valueOf(StringUtils.split(ligne, ' ')[30]);
			blueprint.add(numero);
			blueprint.add(oreRobotCostOre);
			blueprint.add(clayRobotCostOre);
			blueprint.add(obsidianRobotCostOre);
			blueprint.add(obsidianRobotCostClay);
			blueprint.add(geodeRobotCostOre);
			blueprint.add(geodeRobotCostObsidian);
			blueprints.add(blueprint);
		}

		return blueprints;
	}

}