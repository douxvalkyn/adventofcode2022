package advent2022.day19;

import java.util.Objects;

public class State {

	private int ore;
	private int clay;
	private int obsidian;
	private int geode;

	private int nbRobotOre;
	private int nbRobotClay;
	private int nbRobotObsidian;

	private int time;

	public State() {
		super();
	}

	public int getGeode() {
		return geode;
	}

	public void setGeode(int geode) {
		this.geode = geode;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getOre() {
		return ore;
	}

	public void setOre(int ore) {
		this.ore = ore;
	}

	public int getClay() {
		return clay;
	}

	public void setClay(int clay) {
		this.clay = clay;
	}

	public int getObsidian() {
		return obsidian;
	}

	public void setObsidian(int obsidian) {
		this.obsidian = obsidian;
	}

	public int getNbRobotOre() {
		return nbRobotOre;
	}

	public void setNbRobotOre(int nbRobotOre) {
		this.nbRobotOre = nbRobotOre;
	}

	public int getNbRobotClay() {
		return nbRobotClay;
	}

	public void setNbRobotClay(int nbRobotClay) {
		this.nbRobotClay = nbRobotClay;
	}

	public int getNbRobotObsidian() {
		return nbRobotObsidian;
	}

	public void setNbRobotObsidian(int nbRobotObsidian) {
		this.nbRobotObsidian = nbRobotObsidian;
	}

	@Override
	public int hashCode() {
		return Objects.hash(clay, geode, nbRobotClay, nbRobotObsidian, nbRobotOre, obsidian, ore, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return clay == other.clay && geode == other.geode && nbRobotClay == other.nbRobotClay && nbRobotObsidian == other.nbRobotObsidian && nbRobotOre == other.nbRobotOre
			&& obsidian == other.obsidian && ore == other.ore && time == other.time;
	}

	@Override
	public String toString() {
		return "State [ore=" + ore + ", clay=" + clay + ", obsidian=" + obsidian + ", geode=" + geode + ", nbRobotOre=" + nbRobotOre + ", nbRobotClay=" + nbRobotClay + ", nbRobotObsidian="
			+ nbRobotObsidian + ", time=" + time + "]";
	}

}
