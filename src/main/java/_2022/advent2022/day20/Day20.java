package advent2022.day20;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2017.outils.Outil;

public class Day20 {

	private static Logger logger = LoggerFactory.getLogger(Day20.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day20.class.getSimpleName() + "]");
		Day20 day = new Day20();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		// day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() throws IOException {

		String file = "src/main/resources/advent2022/day20.txt";
		List<Long> save = Outil.importationLong(file);
		List<Long> res = Outil.importationLong(file);

		// Multiplier chaqaue nombre par 811589153
		List<Long> saveBig = new ArrayList<>();
		List<Long> resBig = new ArrayList<>();
		for (Long val : save) {
			val = val * 811589153;
			saveBig.add(val);
		}
		for (Long val : res) {
			val = val * 811589153;
			resBig.add(val);
		}

		// rendre unique chaque donnée car certains nombres sont dupliqués: je rajoute ';' + un nombre à chaque donnée.
		List<String> res2 = new ArrayList<>();
		List<String> save2 = new ArrayList<>();
		int i = 0;
		for (Long donnee : resBig) {
			i++;
			res2.add("" + donnee + ";" + i);
		}
		i = 0;
		for (Long donnee : saveBig) {
			i++;
			save2.add("" + donnee + ";" + i);
		}

		for (int round = 0; round < 10; round++) {
			for (String string : res2) {
				long donnee = Long.valueOf((StringUtils.split(string, ';')[0]));
				int indice = save2.indexOf(string);
				saveBig.remove(indice);
				save2.remove(indice);
				long diff = donnee % (res.size() - 1);
				int newIndice = (int) (indice + diff) % (res.size() - 1);
				if (newIndice < 0 || newIndice > res.size()) {
					newIndice = (int) (indice + diff + (res.size() - 1));
				}
				saveBig.add(newIndice, donnee);
				save2.add(newIndice, string);
			}
		}

		// recherche des 3 valeurs: 1000e apres 0, 2000e apres 0, 3000e apres 0
		int indiceZero = saveBig.indexOf(0L);
		Long indice1000 = saveBig.get((indiceZero + 1000) % (save.size()));
		Long indice2000 = saveBig.get((indiceZero + 2000) % (save.size()));
		Long indice3000 = saveBig.get((indiceZero + 3000) % (save.size()));
		System.out.println(indice1000 + indice2000 + indice3000);

	}

	public void run1() throws IOException {

		String file = "src/main/resources/advent2022/day20.txt";
		List<Integer> save = Outil.importationInteger(file);
		List<Integer> res = Outil.importationInteger(file);

		// rendre unique chaque donnée car certains nombres sont dupliqués: je rajoute ';' + un nombre à chaque donnée.
		List<String> res2 = new ArrayList<String>();
		List<String> save2 = new ArrayList<String>();
		int i = 0;
		for (Integer donnee : res) {
			i++;
			res2.add("" + donnee + ";" + i);
		}
		i = 0;
		for (Integer donnee : save) {
			i++;
			save2.add("" + donnee + ";" + i);
		}

		for (String string : res2) {
			int donnee = Integer.valueOf((StringUtils.split(string, ';')[0]));
			int indice = save2.indexOf(string);
			save.remove(indice);
			save2.remove(indice);
			int diff = donnee % (res.size() - 1);
			int newIndice = (indice + diff) % (res.size() - 1);
			if (newIndice < 0 || newIndice > res.size()) {
				newIndice = indice + diff + (res.size() - 1);
			}
			save.add(newIndice, donnee);
			save2.add(newIndice, string);
		}

		// recherche des 3 valeurs: 1000e apres 0, 2000e apres 0, 3000e apres 0
		int indiceZero = save.indexOf(0);
		Integer indice1000 = save.get((indiceZero + 1000) % (save.size()));
		Integer indice2000 = save.get((indiceZero + 2000) % (save.size()));
		Integer indice3000 = save.get((indiceZero + 3000) % (save.size()));
		System.out.println(indice1000 + indice2000 + indice3000);

	}

}