package advent2022.day17;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day17 {

	private static Logger logger = LoggerFactory.getLogger(Day17.class);
	private static boolean go=true;

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day17 day = new Day17();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	/** TETRIS */
	
	
	public void run2() {
		String jetPattern=">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>";
		String file = "src/main/resources/advent2022/day17.txt";
		List<String> res = Outil.importationString(file);
		jetPattern=res.get(0);
		Grille grille = new Grille(7,5000);
		List<Case>pieceEnCours=new ArrayList<Case>();
		int hauteur=0;
		List<Integer>hauteurs=new ArrayList<Integer>();
		int i=0;
		for (int nbPieces=0; nbPieces<=1900;nbPieces++) {
			//System.err.println(nbPieces+1);
			switch (nbPieces%5) {
			case 0:grille=envoieRockType1(grille, pieceEnCours);
			break;
			case 1:grille=envoieRockType2(grille, pieceEnCours);
			break;
			case 2:grille=envoieRockType3(grille, pieceEnCours);
			break;
			case 3:grille=envoieRockType4(grille, pieceEnCours);
			break;
			case 4:grille=envoieRockType5(grille, pieceEnCours);
			break;
			default:System.out.println("pb rock");
				break;
			}
			go=true;
			//grille.affichageEtat();
			hauteur=rechercheHauteurTowerRocks(grille);
		//	hauteurs.add(hauteur);
			
			
			//if (nbPieces%5==0 ) {
				hauteurs.add(hauteur);
			//	System.out.println( "nbPieces" + nbPieces + " hauteur " + hauteur);
		//	}
			
			while (go) {
				char jet = jetPattern.charAt(i);
				grille=push(grille,pieceEnCours, jet);
				//grille.affichageEtat();
				grille=down(grille,pieceEnCours);
				//grille.affichageEtat();
				i++;
				if (i==jetPattern.length()) {
					i=0;
				}
			}
			//grille.affichageEtat();
		}
		List<Integer>hauteurs5=new ArrayList<Integer>();
		int h5=0;
		for (int k=1;k<hauteurs.size()-1;k++) {
			int h = hauteurs.get(k+1)-hauteurs.get(k);
			h5=h5+h;
			if (k%5==0) {
			hauteurs5.add(h5);
			h5=0;
			}
			
		}
		System.err.println("hauteur: " + hauteur);
		for (int k=0;k<hauteurs5.size()-3;k++) {
//			if (hauteurs5.get(k)!=hauteurs5.get(k+346)) {
//				System.out.println("recherche debut cycle "+k);
//			}
			
			if (hauteurs5.get(k)==13 && hauteurs5.get(k+1)==9 && hauteurs5.get(k+2)==9 && hauteurs5.get(k+3)==11) {
				System.out.println(k);
			}
		}
		System.err.println(hauteurs5);
	}
	
	






	public void run1() {
		String jetPattern=">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>";
		String file = "src/main/resources/advent2022/day17.txt";
		List<String> res = Outil.importationString(file);
		jetPattern=res.get(0);
		Grille grille = new Grille(7,5000);
		List<Case>pieceEnCours=new ArrayList<Case>();
		int hauteur=0;

		int i=0;
		for (int nbPieces=0; nbPieces<2022;nbPieces++) {
			//System.err.println(nbPieces+1);
			switch (nbPieces%5) {
			case 0:grille=envoieRockType1(grille, pieceEnCours);
			break;
			case 1:grille=envoieRockType2(grille, pieceEnCours);
			break;
			case 2:grille=envoieRockType3(grille, pieceEnCours);
			break;
			case 3:grille=envoieRockType4(grille, pieceEnCours);
			break;
			case 4:grille=envoieRockType5(grille, pieceEnCours);
			break;
			default:System.out.println("pb rock");
				break;
			}
			go=true;
			//grille.affichageEtat();
			hauteur=rechercheHauteurTowerRocks(grille);
			while (go) {
				char jet = jetPattern.charAt(i);
				grille=push(grille,pieceEnCours, jet);
				//grille.affichageEtat();
				grille=down(grille,pieceEnCours);
				//grille.affichageEtat();
				i++;
				if (i==jetPattern.length()) {
					i=0;
				}
			}
			//grille.affichageEtat();
//			System.out.println("   ");
//			System.out.println("   ");

		}
		System.err.println("hauteur: " + hauteur);
	}

	
	
	private int rechercheHauteurTowerRocks(Grille grille) {
		List<Case> cases = grille.getCases();
		int y=grille.getNbLignes();
		for ( Case cell:cases) {
			if (cell.getEtat().equals("#")) {
			int yCell = cell.getY();
			if (yCell<y) {
				y=yCell;
			}
			}
		}
		return((grille.getNbLignes()-y));
	}



	private Grille down(Grille grille, List<Case> pieceEnCours) {
		List<Case>nextPosition=new ArrayList<Case>();
		
		//verif si toutes les cases peuvent bouger
		boolean peutBouger=true;
		for (Case cell:pieceEnCours) {
			Case nextCell = grille.getCase(cell.getX(), cell.getY()+1);
			if (nextCell ==null ||nextCell.getEtat().equals("#")) {
				peutBouger=false;
			}
		}
		if (!peutBouger) {
			//les pieces st stabilisent
			for (Case cell:pieceEnCours) {
				cell.setEtat("#");
			}
			go=false;
			return grille;
			};
		
		for (Case cell:pieceEnCours) {
			cell.setEtat("_");
			Case nextCell = grille.getCase(cell.getX(), cell.getY()+1);
			if (nextCell != null) {
				nextPosition.add(nextCell);
			}
		}
		pieceEnCours.clear();
		for (Case piece:nextPosition) {
			piece.setEtat("@");
			pieceEnCours.add(piece);
		}
		
		return grille;
	}



	private Grille push(Grille grille, List<Case> pieceEnCours, char jet) {
		List<Case>nextPosition=new ArrayList<Case>();
		
		switch (jet) {
		case '>':
			//verif si toutes les cases peuvent bouger
			boolean peutBouger=true;
			for (Case cell:pieceEnCours) {
				Case nextCell = grille.getCase(cell.getX()+1, cell.getY());
				if (nextCell ==null || nextCell.getEtat().equals("#")) {
					peutBouger=false;
				}
			}
			if (!peutBouger) { return grille;};
			
			for (Case cell:pieceEnCours) {
			cell.setEtat("_");
			Case nextCell = grille.getCase(cell.getX()+1, cell.getY());
			if (nextCell != null) {
				nextPosition.add(nextCell);
			}
		}
		pieceEnCours.clear();
		for (Case piece:nextPosition) {
			piece.setEtat("@");
			pieceEnCours.add(piece);
		}
			break;
			
		case '<':
			//verif si toutes les cases peuvent bouger
			peutBouger=true;
			for (Case cell:pieceEnCours) {
				Case nextCell = grille.getCase(cell.getX()-1, cell.getY());
				if (nextCell ==null || nextCell.getEtat().equals("#")) {
					peutBouger=false;
				}
			}
			if (!peutBouger) { return grille;};
			for (Case cell:pieceEnCours) {
			cell.setEtat("_");
			Case nextCell = grille.getCase(cell.getX()-1, cell.getY());
			if (nextCell != null) {
				nextPosition.add(nextCell);
			}
		}
		pieceEnCours.clear();
		for (Case piece:nextPosition) {
			piece.setEtat("@");
			pieceEnCours.add(piece);
		}
			break;
			
		default:System.out.println("pb");
			break;
		}
		
		return grille;
	}


	
	private Grille envoieRockType5(Grille grille, List<Case> pieceEnCours){
		//trouver la position Y du rock le plus haut actuellement
		List<Case> cases = grille.getCases();
		int yMin=grille.getNbLignes();
		for (Case cell:cases) {
			if (cell.getEtat().equals("#")) {
				int y = cell.getY();
				if (y<yMin) {
					yMin=y;
				}
			}
		}

		grille.getCase(2, yMin-4).setEtat("@");
		grille.getCase(3, yMin-5).setEtat("@");
		grille.getCase(2, yMin-5).setEtat("@");
		grille.getCase(3, yMin-4).setEtat("@");
		pieceEnCours.clear();
		pieceEnCours.add(grille.getCase(2, yMin-4));
		pieceEnCours.add(grille.getCase(3, yMin-5));
		pieceEnCours.add(grille.getCase(2, yMin-5));
		pieceEnCours.add(grille.getCase(3, yMin-4));

		return grille;
	}
	
	
	
	private Grille envoieRockType4(Grille grille, List<Case> pieceEnCours){
		//trouver la position Y du rock le plus haut actuellement
		List<Case> cases = grille.getCases();
		int yMin=grille.getNbLignes();
		for (Case cell:cases) {
			if (cell.getEtat().equals("#")) {
				int y = cell.getY();
				if (y<yMin) {
					yMin=y;
				}
			}
		}

		grille.getCase(2, yMin-4).setEtat("@");
		grille.getCase(2, yMin-5).setEtat("@");
		grille.getCase(2, yMin-6).setEtat("@");
		grille.getCase(2, yMin-7).setEtat("@");
		pieceEnCours.clear();
		pieceEnCours.add(grille.getCase(2, yMin-4));
		pieceEnCours.add(grille.getCase(2, yMin-5));
		pieceEnCours.add(grille.getCase(2, yMin-6));
		pieceEnCours.add(grille.getCase(2, yMin-7));

		return grille;
	}
	
	private Grille envoieRockType3(Grille grille, List<Case> pieceEnCours){
		//trouver la position Y du rock le plus haut actuellement
		List<Case> cases = grille.getCases();
		int yMin=grille.getNbLignes();
		for (Case cell:cases) {
			if (cell.getEtat().equals("#")) {
				int y = cell.getY();
				if (y<yMin) {
					yMin=y;
				}
			}
		}

		grille.getCase(2, yMin-4).setEtat("@");
		grille.getCase(3, yMin-4).setEtat("@");
		grille.getCase(4, yMin-4).setEtat("@");
		grille.getCase(4, yMin-5).setEtat("@");
		grille.getCase(4, yMin-6).setEtat("@");
		pieceEnCours.clear();
		pieceEnCours.add(grille.getCase(2, yMin-4));
		pieceEnCours.add(grille.getCase(3, yMin-4));
		pieceEnCours.add(grille.getCase(4, yMin-4));
		pieceEnCours.add(grille.getCase(4, yMin-5));
		pieceEnCours.add(grille.getCase(4, yMin-6));

		return grille;
	}
	
	private Grille envoieRockType2(Grille grille, List<Case> pieceEnCours){
		//trouver la position Y du rock le plus haut actuellement
		List<Case> cases = grille.getCases();
		int yMin=grille.getNbLignes();
		for (Case cell:cases) {
			if (cell.getEtat().equals("#")) {
				int y = cell.getY();
				if (y<yMin) {
					yMin=y;
				}
			}
		}

		grille.getCase(3, yMin-4).setEtat("@");
		grille.getCase(2, yMin-5).setEtat("@");
		grille.getCase(3, yMin-5).setEtat("@");
		grille.getCase(4, yMin-5).setEtat("@");
		grille.getCase(3, yMin-6).setEtat("@");
		pieceEnCours.clear();
		pieceEnCours.add(grille.getCase(3, yMin-4));
		pieceEnCours.add(grille.getCase(2, yMin-5));
		pieceEnCours.add(grille.getCase(3, yMin-5));
		pieceEnCours.add(grille.getCase(4, yMin-5));
		pieceEnCours.add(grille.getCase(3, yMin-6));

		return grille;
	}
	
	private Grille envoieRockType1(Grille grille, List<Case> pieceEnCours){
		//trouver la position Y du rock le plus haut actuellement
		List<Case> cases = grille.getCases();
		int yMin=grille.getNbLignes();
		for (Case cell:cases) {
			if (cell.getEtat().equals("#")) {
				int y = cell.getY();
				if (y<yMin) {
					yMin=y;
				}
			}
		}

		grille.getCase(2, yMin-4).setEtat("@");
		grille.getCase(3, yMin-4).setEtat("@");
		grille.getCase(4, yMin-4).setEtat("@");
		grille.getCase(5, yMin-4).setEtat("@");
		pieceEnCours.clear();
		pieceEnCours.add(grille.getCase(2, yMin-4));
		pieceEnCours.add(grille.getCase(3, yMin-4));
		pieceEnCours.add(grille.getCase(4, yMin-4));
		pieceEnCours.add(grille.getCase(5, yMin-4));

		return grille;
	}
	
	
	
}