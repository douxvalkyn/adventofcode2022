package advent2022.day06;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day06 {

	private static Logger logger = LoggerFactory.getLogger(Day06.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day06 day = new Day06();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day06.txt";
		List<String> res = Outil.importationString(file);
		boolean ok = true;
		int i = 0;
		while (ok && i < res.get(0).length() - 14) {
			String chaineATester = StringUtils.substring(res.get(0), i, i + 14);
			boolean trouve = true;
			for (int j = 0; j < chaineATester.length(); j++) {
				for (int k = 0; k < chaineATester.length(); k++) {
					if (j != k && chaineATester.charAt(j) == chaineATester.charAt(k)) {
						trouve = false;
					}
				}
			}
			if (trouve) {
				System.out.print("run2: " + chaineATester + " ");
				System.out.println(i + 14);
				ok = false;
			}
			i++;
		}
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day06.txt";
		List<String> res = Outil.importationString(file);
		boolean nonTrouve = true;
		int i = 0;
		while (nonTrouve && i < res.get(0).length() - 3) {
			if (res.get(0).charAt(i) != res.get(0).charAt(i + 1) && res.get(0).charAt(i) != res.get(0).charAt(i + 2) && res.get(0).charAt(i) != res.get(0).charAt(i + 3)
				&& res.get(0).charAt(i + 1) != res.get(0).charAt(i + 2) && res.get(0).charAt(i + 1) != res.get(0).charAt(i + 3) && res.get(0).charAt(i + 2) != res.get(0).charAt(i + 3)) {
				System.out.print("run1: " + res.get(0).charAt(i) + res.get(0).charAt(i + 1) + res.get(0).charAt(i + 2) + res.get(0).charAt(i + 3) + " ");
				nonTrouve = false;
				System.out.println(i + 4);
			} else {
				i++;
			}
		}
	}
}
