package advent2022.day23;

import communs.Case;

public class Elfe {

	private Case position;
	private Case positionVisee;
	public Elfe() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Case getPosition() {
		return position;
	}
	public void setPosition(Case position) {
		this.position = position;
	}
	public Case getPositionVisee() {
		return positionVisee;
	}
	public void setPositionVisee(Case positionVisee) {
		this.positionVisee = positionVisee;
	}
	@Override
	public String toString() {
		return "Elfe [position=" + position + ", positionVisee=" + positionVisee + "]";
	}
	
	
	
}
