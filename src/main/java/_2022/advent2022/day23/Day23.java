package advent2022.day23;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2017.outils.Outil;
import communs.Case;
import communs.Grille;

public class Day23 {

	private static Logger logger = LoggerFactory.getLogger(Day23.class);
	private static boolean grilleChanged=true;

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day23.class.getSimpleName() + "]");
		Day23 day = new Day23();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	public void run2() {
		String file = "src/main/resources/advent2022/day23.txt";
		List<String> input = Outil.importationString(file);

		List<Elfe>elfes = new ArrayList<Elfe>();
		Grille grille = creationGrille(input, elfes);
		//grille.affichageEtat();
		String directions="NSWE";
		int round=0;
		while (grilleChanged) {
			round++;
			grilleChanged=false;
			//grille.affichageEtat();
			System.out.println(round);
			elfeProposePosition(elfes, grille, directions);
			elfeBouge(elfes, grille);
			//grille.affichageEtat();
			char dir = directions.charAt(0);
			directions=directions.substring(1,4) + dir;
		}
		System.out.println("run2: "+round);
		
	}
	
	

	public void run1() {
		String file = "src/main/resources/advent2022/day23.txt";
		List<String> input = Outil.importationString(file);

		List<Elfe>elfes = new ArrayList<Elfe>();
		Grille grille = creationGrille(input, elfes);
		//grille.affichageEtat();
		String directions="NSWE";
		for (int round=1;round<=10;round++) {
			elfeProposePosition(elfes, grille, directions);
			elfeBouge(elfes, grille);
			//grille.affichageEtat();
			char dir = directions.charAt(0);
			directions=directions.substring(1,4) + dir;
		}
		calculeRectangle(grille);
		
		
	}

	private void calculeRectangle(Grille grille) {
		List<Case> cells = grille.getCases();
		int xmin=10000;
		int xmax=0;
		int ymin=10000;
		int ymax=0;
		for (Case cell:cells) {
			if (cell.getEtat().equals("#")) {
				int x = cell.getX();
				int y = cell.getY();
				if (x<xmin) {
					xmin=x;
				}
				if (x>xmax) {
					xmax=x;
				}
				if (y<ymin) {
					ymin=y;
				}
				if (y>ymax) {
					ymax=y;
				}
			}
		}
		int cpt=0;
		for (Case cell:cells) {
			if ( cell.getX()>=xmin && cell.getX()<=xmax &&
					cell.getY()>=ymin && cell.getY()<=ymax ){
					if (cell.getEtat().equals("_"))	{
						cpt++;
					}
				}
		}
//		System.out.println(ymin);
//		System.out.println(ymax);
//		System.out.println(xmin);
//		System.out.println(xmax);
		System.out.println("run1: " + cpt);

	}

	private void elfeBouge(List<Elfe> elfes, Grille grille) {
		for (Elfe elfe:elfes) {
			Case positionVisee = elfe.getPositionVisee();
			boolean positionViseeUnique=true;
			for(Elfe elfe2:elfes) {
				if ( elfe!=elfe2 && elfe2.getPositionVisee().equals(positionVisee) ) {
					positionViseeUnique=false;
				}
			}
			if (positionViseeUnique && (elfe.getPosition().getX()!=elfe.getPositionVisee().getX() || elfe.getPosition().getY()!=elfe.getPositionVisee().getY())) {
				elfe.getPosition().setEtat("_");
				elfe.setPosition(positionVisee);
				elfe.getPosition().setEtat("#");
				grilleChanged=true;
			}
		}
		
	}

	private void elfeProposePosition(List<Elfe> elfes, Grille grille, String directions) {
		for (Elfe elfe:elfes) {
			elfe.setPositionVisee(null); //ajout
			Case position = elfe.getPosition();
			List<Case> voisins = position.getCasesAdjacentesQueen(grille);
			int nbElfesVoisins=0;
			for (Case voisin:voisins) {
				if (voisin.getEtat().equals("#")) {
					nbElfesVoisins++;
				}
			}
			if (nbElfesVoisins==0) {
				//this elf do nothing
				elfe.setPositionVisee(null);
			}else {
				//check direction 1
				String direction = ""+directions.charAt(0);
				if (checkDirection(direction,position, grille)==0) {
					elfe.setPositionVisee(position.getCasesAdjacentes(grille).get(numero(direction))); 
				}else {
					//check direction 2
					direction = ""+directions.charAt(1);
					if (checkDirection(direction,position, grille)==0) {
						elfe.setPositionVisee(position.getCasesAdjacentes(grille).get(numero(direction))); 
					}else {
						//check direction 3
						direction = ""+directions.charAt(2);
						if (checkDirection(direction,position, grille)==0) {
							elfe.setPositionVisee(position.getCasesAdjacentes(grille).get(numero(direction))); 
						}else {
							//check direction 4
							direction = ""+directions.charAt(3);
							if (checkDirection(direction,position, grille)==0) {
								elfe.setPositionVisee(position.getCasesAdjacentes(grille).get(numero(direction))); 
							}
						}
					}
				}	
			}
			if (elfe.getPositionVisee()==null) {
				elfe.setPositionVisee(elfe.getPosition());
			}
		}

	}

	private int numero(String direction) {
		switch (direction) {
		case "N":return 0;
		case "S":return 1;
		case "E":return 2;
		case "W":return 3;
		default:
			break;
		}
		return -1;
	}

	private int checkDirection(String direction, Case position, Grille grille) {
		List<Case> voisins = position.getCasesAdjacentesQueen(grille);
		int nbElfes=0;
		switch (direction) {
		case "N":
			if (voisins.get(0).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(4).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(5).getEtat().equals("#")) {
				nbElfes++;
			}
			break;
		case "S":
			if (voisins.get(1).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(6).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(7).getEtat().equals("#")) {
				nbElfes++;
			}
			break;
		case "E":
			if (voisins.get(2).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(5).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(7).getEtat().equals("#")) {
				nbElfes++;
			}
			break;
		case "W":
			if (voisins.get(3).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(4).getEtat().equals("#")) {
				nbElfes++;
			}
			if (voisins.get(6).getEtat().equals("#")) {
				nbElfes++;
			}
			break;

		default:
			break;
		}

		return nbElfes;
	}

	private Grille creationGrille(List<String> input, List<Elfe> elfes) {
		int x=400;
		int y=400;
		Grille grille = new Grille(x,y);
		
		for (int j=0;j<input.size();j++) {
			String ligne = input.get(j);
			for (int i=0;i<ligne.length();i++) {
				char point = ligne.charAt(i);
				if (point=='#') {
					grille.getCase(i+x/2, j+y/2).setEtat("#");
					Elfe elfe = new Elfe();
					elfe.setPosition(grille.getCase(i+x/2, j+y/2));
					elfes.add(elfe);
				}
			}
		}
		return grille;
	}
	}