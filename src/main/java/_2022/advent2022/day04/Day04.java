package advent2022.day04;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day04 {

	private static Logger logger = LoggerFactory.getLogger(Day04.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day04 day = new Day04();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day04.txt";
		List<String> res = Outil.importationString(file);
		// System.out.println(res);

		int sum = 0;
		for (int i = 0; i < res.size(); i++) { // pour chaque paire
			String elfe1 = StringUtils.split(res.get(i), ',')[0];
			String elfe2 = StringUtils.split(res.get(i), ',')[1];

			int doublon = 0;
			if ((Integer.valueOf(StringUtils.split(elfe1, '-')[1]) >= Integer.valueOf(StringUtils.split(elfe2, '-')[0]))
				&& (Integer.valueOf(StringUtils.split(elfe1, '-')[0]) <= Integer.valueOf(StringUtils.split(elfe2, '-')[0]))) {
				// System.out.println("overlap! "+ i);
				sum++;
				doublon++;
			}
			if ((Integer.valueOf(StringUtils.split(elfe2, '-')[1]) >= Integer.valueOf(StringUtils.split(elfe1, '-')[0]))
				&& (Integer.valueOf(StringUtils.split(elfe2, '-')[0]) <= Integer.valueOf(StringUtils.split(elfe1, '-')[0]))) {
				// System.out.println("overlap! "+ i);
				sum++;
				doublon++;
			}
			if (doublon == 2) {
				// System.out.println("-doublon! " + i);
				sum--;
			}

		}
		System.out.println("run2: " + sum);
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day04.txt";
		List<String> res = Outil.importationString(file);
		// System.out.println(res);

		int sum = 0;
		for (int i = 0; i < res.size(); i++) { // pour chaque paire
			String elfe1 = StringUtils.split(res.get(i), ',')[0];
			String elfe2 = StringUtils.split(res.get(i), ',')[1];

			// verif si elfe1 recouvre tout elfe2
			if ((Integer.valueOf(StringUtils.split(elfe1, '-')[0]) <= Integer.valueOf(StringUtils.split(elfe2, '-')[0]))
				&& Integer.valueOf(StringUtils.split(elfe1, '-')[1]) >= Integer.valueOf(StringUtils.split(elfe2, '-')[1])) {
				// System.out.println("elfe1 recouvre elfe 2! "+ i);
				sum++;
			}
			// verif si elfe2 recouvre tout elfe1
			if ((Integer.valueOf(StringUtils.split(elfe2, '-')[0]) <= Integer.valueOf(StringUtils.split(elfe1, '-')[0]))
				&& Integer.valueOf(StringUtils.split(elfe2, '-')[1]) >= Integer.valueOf(StringUtils.split(elfe1, '-')[1])) {
				// System.out.println("elfe2 recouvre elfe 1! "+ i);
				sum++;
			}
			// verif si elfe1=elfe2 > il y a donc un doublon à supprimer
			if ((Integer.valueOf(StringUtils.split(elfe2, '-')[0]) == Integer.valueOf(StringUtils.split(elfe1, '-')[0]))
				&& Integer.valueOf(StringUtils.split(elfe2, '-')[1]) == Integer.valueOf(StringUtils.split(elfe1, '-')[1])) {
				// System.out.println("doublon! "+ i);
				sum--;
			}
		}
		System.out.println("run1: " + sum);
	}

}
