package advent2022.day07;

import java.util.ArrayList;
import java.util.List;

public class Directory {

	Directory dossierParent;
	private List<Directory> directories;
	private List<String> files;
	private String name;
	int poids;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Directory> getDirectories() {
		return directories;
	}

	public void setDirectories(List<Directory> directories) {
		this.directories = directories;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}

	public Directory getDossierParent() {
		return dossierParent;
	}

	public void setDossierParent(Directory dossierParent) {
		this.dossierParent = dossierParent;
	}

	public int getPoids() {
		return poids;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}

	public Directory() {
		super();
		List<Directory> directories = new ArrayList();
		this.setDirectories(directories);
		List<String> files = new ArrayList();
		this.setFiles(files);
		this.poids = 0;
	}

	@Override
	public String toString() {
		List<String> directoriesNames = new ArrayList();
		for (Directory dir : this.getDirectories()) {
			directoriesNames.add(dir.getName());
		}

		return "Dossier [ name=" + name + " directories=" + directoriesNames + ", files=" + files + " poids: " + poids + "]";
	}

}
