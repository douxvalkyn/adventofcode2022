package advent2022.day07;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day07 {

	private static Logger logger = LoggerFactory.getLogger(Day07.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day07 day = new Day07();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		Directory racine = run1();

		// recherche des directories
		List<Directory> liste = new ArrayList<Directory>();
		liste.add(racine);
		for (Directory dir : racine.getDirectories()) {
			liste.add(dir);
			for (Directory aaa : dir.getDirectories()) {
				liste.add(aaa);
				for (Directory bbb : aaa.getDirectories()) {
					liste.add(bbb);
					for (Directory ccc : bbb.getDirectories()) {
						liste.add(ccc);
						for (Directory ddd : ccc.getDirectories()) {
							liste.add(ddd);
							for (Directory eee : ddd.getDirectories()) {
								liste.add(eee);
								for (Directory fff : eee.getDirectories()) {
									liste.add(fff);
									for (Directory ggg : fff.getDirectories()) {
										liste.add(ggg);
										for (Directory hhh : ggg.getDirectories()) {
											liste.add(hhh);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		int unused = 70000000 - racine.getPoids();
		int needed = 30000000 - unused;
		List<Directory> candidats = new ArrayList<Directory>();
		for (Directory dir : liste) {
			if (dir.getPoids() >= needed) {
				candidats.add(dir);
			}
		}

		int min = Integer.MAX_VALUE;
		Directory objectif = null;
		for (Directory cand : candidats) {

			if (cand.getPoids() < min) {
				min = cand.getPoids();
				objectif = cand;
			}
		}
		System.out.println(objectif);
	}

	public Directory run1() {
		String file = "src/main/resources/advent2022/day07.txt";
		List<String> res = Outil.importationString(file);
		Directory directoryCourant = new Directory();
		Directory racine = new Directory();

		for (String ligne : res) {
			if (StringUtils.substring(ligne, 0, 4).equals("$ cd")) { // cd
				String nomDirectory = StringUtils.split(ligne, " ")[2];

				if (nomDirectory.equals("..")) {
					Directory parent = directoryCourant.getDossierParent();
					if (parent != null) {
						directoryCourant = parent;
					}

				} else {
					Directory dir = getDirectory(directoryCourant, nomDirectory);
					directoryCourant = dir;
					if (nomDirectory.equals("/")) {
						dir = racine;
						dir.setName(nomDirectory);
						directoryCourant = dir;
					}
					if (dir == null) {
						System.out.println("pb");
					}
				}
			}
			if (StringUtils.substring(ligne, 0, 3).equals("dir")) { // ajout dir
				Directory directory = new Directory();
				String nomDirectory = StringUtils.trim(StringUtils.split(ligne, ' ')[1]);
				directory.setName(nomDirectory);
				directoryCourant.getDirectories().add(directory);
				directory.setDossierParent(directoryCourant);
			}
			if (!StringUtils.substring(ligne, 0, 3).equals("dir") && !ligne.equals("$ ls") && !StringUtils.substring(ligne, 0, 4).equals("$ cd")) { // ajout fichier
				Integer poids = Integer.valueOf(StringUtils.split(ligne, ' ')[0]);
				String nomFichier = StringUtils.split(ligne, ' ')[1];
				directoryCourant.getFiles().add(nomFichier);
				directoryCourant.setPoids(directoryCourant.getPoids() + poids);
				boolean go = true;
				Directory dir = directoryCourant;
				while (go) {
					Directory parent = dir.getDossierParent();
					if (parent != null) {
						parent.setPoids(parent.getPoids() + poids);
						dir = parent;
					} else {
						go = false;
					}
				}

			}

		}
		System.out.println(racine);

		// recherche des directories de taille <100000
		int sum = 0;
		for (Directory dir : racine.getDirectories()) {
			if (dir.getPoids() < 100000) {
				sum = sum + dir.getPoids();
			}
			for (Directory aaa : dir.getDirectories()) {
				if (aaa.getPoids() < 100000) {
					sum = sum + aaa.getPoids();
				}
				for (Directory bbb : aaa.getDirectories()) {
					if (bbb.getPoids() < 100000) {
						sum = sum + bbb.getPoids();
					}
					for (Directory ccc : bbb.getDirectories()) {
						if (ccc.getPoids() < 100000) {
							sum = sum + ccc.getPoids();
						}
						for (Directory ddd : ccc.getDirectories()) {
							if (ddd.getPoids() < 100000) {
								sum = sum + ddd.getPoids();
							}
							for (Directory eee : ddd.getDirectories()) {
								if (eee.getPoids() < 100000) {
									sum = sum + eee.getPoids();
								}
								for (Directory fff : eee.getDirectories()) {
									if (fff.getPoids() < 100000) {
										sum = sum + fff.getPoids();
									}
									for (Directory ggg : fff.getDirectories()) {
										if (ggg.getPoids() < 100000) {
											sum = sum + ggg.getPoids();
										}
										for (Directory hhh : ggg.getDirectories()) {
											if (hhh.getPoids() < 100000) {
												sum = sum + hhh.getPoids();
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		System.out.println("sum " + sum);
		return racine;

	}

	private void recherchePetitsDossiers(Directory dossier) {
		// TODO Auto-generated method stub

	}

	// recherche dans le dossier courant le dossier dont le nom est nomDirectory
	private Directory getDirectory(Directory directoryCourant, String nomDirectory) {
		for (Directory dir : directoryCourant.getDirectories()) {
			if (dir.getName().equals(nomDirectory)) {
				return dir;
			}
		}
		return null;
	}

	private boolean existeDeja(String nomDirectory, List<Directory> listeOfDirectories) {
		for (Directory dir : listeOfDirectories) {
			if (dir.getName().equals(nomDirectory)) {
				return true;
			}
		}

		return false;
	}

	private Directory rechercheParent(Directory directoryCourant, List<Directory> listeOfDirectories) {
		for (Directory dir : listeOfDirectories) {
			for (Directory enfant : dir.getDirectories()) {
				if (enfant.getName() == null || directoryCourant.getName() == null) {
					System.out.println("pb");
				}
				if (enfant.getName().equals(directoryCourant.getName())) {
					return dir;
				}
			}
		}
		return null;
	}

}