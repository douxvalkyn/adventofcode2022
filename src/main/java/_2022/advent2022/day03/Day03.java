package advent2022.day03;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day03 {

	private static Logger logger = LoggerFactory.getLogger(Day03.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day03 day = new Day03();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}


	public void run2() {
		String file = "src/main/resources/advent2022/day03.txt";
		List<String> res = Outil.importationString(file);
		//System.out.println(res);
		int sum=0;
		for (int t=0;t<res.size();t=t+3) {

			//recherche de la lettre en commun sur le groupe de 3
			boolean trouve=false;
			String elfe1 = res.get(t);
			String elfe2 = res.get(t+1);
			String elfe3 = res.get(t+2);

			for (int i=0;i<elfe1.length();i++) {
				for (int j=0;j<elfe2.length();j++) {
					for (int k=0;k<elfe3.length();k++) {
						if (!trouve &&  elfe1.charAt(i)==(elfe2.charAt(j)) && elfe1.charAt(i)==(elfe3.charAt(k)) ){
							trouve=true;
							sum=sum+calculeValeurLettre(elfe1.charAt(i));
						}
					}
				}
			}
		}
		System.out.println("run1: "+sum);
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day03.txt";
		List<String> res = Outil.importationString(file);
		//System.out.println(res);
		int sum=0;
		for (String rucksack: res) {
			List<String> compartiment1= new ArrayList<String>();
			List<String> compartiment2= new ArrayList<String>();
			for (int i=0;i<rucksack.length()/2;i++){
				compartiment1.add(""+rucksack.charAt(i));
			}
			for (int i=rucksack.length()/2;i<rucksack.length();i++){
				compartiment2.add(""+rucksack.charAt(i));
			}

			//recherche de la lettre en commun
			boolean trouve=false;
			for (int i=0;i<compartiment1.size();i++) {
				for (int j=0;j<compartiment2.size();j++) {
					if (!trouve && compartiment1.get(i).equals(compartiment2.get(j))){
						trouve=true;
						char ch  = compartiment1.get(i).toCharArray()[0];
						sum = sum+ calculeValeurLettre(ch);
					}
				}
			}
		}
		System.out.println("run2: "+sum);
	}


	public int calculeValeurLettre(char ch) {
		int valeur=0;
		int temp = (int)ch;
		if(temp<=122 & temp>=97) { //for lower case
			valeur=+temp-96;
		}
		if (temp<=96) { //for upper case
			valeur=+temp-64+26;
		}
		return valeur;
	}


}
