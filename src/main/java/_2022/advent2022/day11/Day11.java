package advent2022.day11;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day11 {

	private static Logger logger = LoggerFactory.getLogger(Day11.class);

	private static long monkey0;
	private static long monkey1;
	private static long monkey2;
	private static long monkey3;
	private static long monkey4;
	private static long monkey5;
	private static long monkey6;
	private static long monkey7;
	
	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day11 day = new Day11();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	
public void run2() {
		monkey0=0;
		monkey1=0;
		monkey2=0;
		monkey3=0;
		monkey4=0;
		monkey5=0;
		monkey6=0;
		monkey7=0;
		

		List<Monkey> monkeys = new ArrayList<Monkey>();
		
		//ajout des items
		initialisationMonkeys(monkeys);
		
		
		for (int round=1;round<10001;round++) {
			monkeys=monkey0(monkeys,2);
			monkeys=monkey1(monkeys,2);
			monkeys=monkey2(monkeys,2);
			monkeys=monkey3(monkeys,2);
			monkeys=monkey4(monkeys,2);
			monkeys=monkey5(monkeys,2);
			monkeys=monkey6(monkeys,2);
			monkeys=monkey7(monkeys,2);
			//System.out.println("round "+round+ " " +monkeys);
			//System.out.println(round);
			
			
		}
		List<Long> listeMonkeys= new ArrayList<Long>();
		listeMonkeys.add(monkey0);
		listeMonkeys.add(monkey1);
		listeMonkeys.add(monkey2);
		listeMonkeys.add(monkey3);
		listeMonkeys.add(monkey4);
		listeMonkeys.add(monkey5);
		listeMonkeys.add(monkey6);
		listeMonkeys.add(monkey7);
		Long max = Outil.maxLong(listeMonkeys);
		listeMonkeys.remove(max);
		Long max2 = Outil.maxLong(listeMonkeys);
		System.out.println("run2: "+max*max2);
	}



	public void run1() {
		
		List<Monkey> monkeys = new ArrayList<Monkey>();
		
		//ajout des items
		initialisationMonkeys(monkeys);
		
		
		for (int round=1;round<21;round++) {
			monkeys=monkey0(monkeys,1);
			monkeys=monkey1(monkeys,1);
			monkeys=monkey2(monkeys,1);
			monkeys=monkey3(monkeys,1);
			monkeys=monkey4(monkeys,1);
			monkeys=monkey5(monkeys,1);
			monkeys=monkey6(monkeys,1);
			monkeys=monkey7(monkeys,1);
			
			
		}
		
		List<Long> listeMonkeys= new ArrayList<Long>();
		listeMonkeys.add(monkey0);
		listeMonkeys.add(monkey1);
		listeMonkeys.add(monkey2);
		listeMonkeys.add(monkey3);
		listeMonkeys.add(monkey4);
		listeMonkeys.add(monkey5);
		listeMonkeys.add(monkey6);
		listeMonkeys.add(monkey7);
		Long max = Outil.maxLong(listeMonkeys);
		listeMonkeys.remove(max);
		Long max2 = Outil.maxLong(listeMonkeys);
		System.out.println("run1: "+max*max2);
	}

	public void initialisationMonkeys(List<Monkey> monkeys) {
		Monkey monkey0 = new Monkey();
		monkey0.getItems().add(84L);
		monkey0.getItems().add(66L);
		monkey0.getItems().add(62L);
		monkey0.getItems().add(69L);
		monkey0.getItems().add(88L);
		monkey0.getItems().add(91L);
		monkey0.getItems().add(91L);
		monkeys.add(monkey0);
		Monkey monkey1 = new Monkey();
		monkey1.getItems().add(98L);
		monkey1.getItems().add(50L);
		monkey1.getItems().add(76L);
		monkey1.getItems().add(99L);
		monkeys.add(monkey1);
		Monkey monkey2 = new Monkey();
		monkey2.getItems().add(72L);
		monkey2.getItems().add(56L);
		monkey2.getItems().add(94L);
		monkeys.add(monkey2);
		Monkey monkey3 = new Monkey();
		monkey3.getItems().add(55L);
		monkey3.getItems().add(88L);
		monkey3.getItems().add(90L);
		monkey3.getItems().add(77L);
		monkey3.getItems().add(60L);
		monkey3.getItems().add(67L);
		monkeys.add(monkey3);
		Monkey monkey4 = new Monkey();
		monkey4.getItems().add(69L);
		monkey4.getItems().add(72L);
		monkey4.getItems().add(63L);
		monkey4.getItems().add(60L);
		monkey4.getItems().add(72L);
		monkey4.getItems().add(52L);
		monkey4.getItems().add(63L);
		monkey4.getItems().add(78L);
		monkeys.add(monkey4);
		Monkey monkey5 = new Monkey();
		monkey5.getItems().add(89L);
		monkey5.getItems().add(73L);
		monkeys.add(monkey5);
		Monkey monkey6 = new Monkey();
		monkey6.getItems().add(78L);
		monkey6.getItems().add(68L);
		monkey6.getItems().add(98L);
		monkey6.getItems().add(88L);
		monkey6.getItems().add(66L);
		monkeys.add(monkey6);
		Monkey monkey7 = new Monkey();
		monkey7.getItems().add(70L);
		monkeys.add(monkey7);
	}
	
	public void initialisationMonkeys_(List<Monkey> monkeys) {
		Monkey monkey0 = new Monkey();
		monkey0.getItems().add(79L);
		monkey0.getItems().add(98L);
		monkeys.add(monkey0);
		Monkey monkey1 = new Monkey();
		monkey1.getItems().add(54L);
		monkey1.getItems().add(65L);
		monkey1.getItems().add(75L);
		monkey1.getItems().add(74L);
		monkeys.add(monkey1);
		Monkey monkey2 = new Monkey();
		monkey2.getItems().add(79L);
		monkey2.getItems().add(60L);
		monkey2.getItems().add(97L);
		monkeys.add(monkey2);
		Monkey monkey3 = new Monkey();
		monkey3.getItems().add(74L);
		monkeys.add(monkey3);
	}

	
	private List<Monkey> monkey7(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(7);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey7++;
			Long itemSaved = item;
			item=item+7;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%5==0) {
				monkeys.get(1).getItems().add(item);
			}else {
				monkeys.get(3).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	
	private List<Monkey> monkey6(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(6);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey6++;
			Long itemSaved = item;
			item=item+6;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%11==0) {
				monkeys.get(2).getItems().add(item);
			}else {
				monkeys.get(5).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey5(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(5);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey5++;
			Long itemSaved = item;
			item=item+5;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%17==0) {
				monkeys.get(2).getItems().add(item);
			}else {
				monkeys.get(0).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey4(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(4);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey4++;
			Long itemSaved = item;
			item=item*13;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%19==0) {
				monkeys.get(1).getItems().add(item);
			}else {
				monkeys.get(7).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey3(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(3);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey3++;
			Long itemSaved = item;
			item=item+2;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%3==0) {
				monkeys.get(6).getItems().add(item);
			}else {
				monkeys.get(5).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey2(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(2);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey2++;
			Long itemSaved = item;
			item=item+1;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%13==0) {
				monkeys.get(4).getItems().add(item);
			}else {
				monkeys.get(0).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey1(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(1);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey1++;
			Long itemSaved = item;
			item=item*item;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%7==0) {
				monkeys.get(3).getItems().add(item);
			}else {
				monkeys.get(6).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey0(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(0);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey0++;
			Long itemSaved = item;
			item=item*11;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques(item);
			if (item%2==0) {
				monkeys.get(4).getItems().add(item);
			}else {
				monkeys.get(7).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey3_(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(3);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey3++;
			Long itemSaved = item;
			item=item+3;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques_(item);
			if (item%17==0) {
				monkeys.get(0).getItems().add(item);
			}else {
				monkeys.get(1).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey2_(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(2);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey2++;
			Long itemSaved = item;
			item=item*item;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques_(item);
			if (item%13==0) {
				monkeys.get(1).getItems().add(item);
			}else {
				monkeys.get(3).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey1_(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(1);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey1++;
			Long itemSaved = item;
			item=item+6;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques_(item);
			if (item%19==0) {
				monkeys.get(2).getItems().add(item);
			}else {
				monkeys.get(0).getItems().add(item);
			}
			toRemove.add(itemSaved);
		
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}
	
	private List<Monkey> monkey0_(List<Monkey> monkeys, int i) {
		Monkey monkey = monkeys.get(0);
		List<Long> toRemove = new ArrayList<Long>();
		for (Long item:monkey.getItems()) {
			monkey0++;
			Long itemSaved = item;
			item=item*19;
			if (i==1) {
				item=Math.floorDiv(item,3) ;
				}
			item=rechercheItemAvecMemesCaracteristiques_(item);
			if (item%23==0) {
				monkeys.get(2).getItems().add(item);
			}else {
				monkeys.get(3).getItems().add(item);
			}
			toRemove.add(itemSaved);
		}
		for (Long x:toRemove) {
			monkey.getItems().remove(x);
		}
		return monkeys;
	}





	private Long rechercheItemAvecMemesCaracteristiques_(Long item) {
		item=item%(23*19*13*17);
		return item;
	}
	
	private Long rechercheItemAvecMemesCaracteristiques(Long item) {
		if (item>(2*7*13*3*19*17*11*5)) {
			item=item%(2*7*13*3*19*17*11*5);
			return item;
			}
		return item;
	}


}