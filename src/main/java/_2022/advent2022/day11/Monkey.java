package advent2022.day11;

import java.util.ArrayList;
import java.util.List;

public class Monkey {

	private List<Long > items;

	public List<Long > getItems() {
		return items;
	}

	public void setItems(List<Long > items) {
		this.items = items;
	}

	public Monkey() {
		super();
		this.items = new ArrayList<Long >();
	}

	@Override
	public String toString() {
		return "Monkey [items=" + items + "]";
	}
	
	
	
	
}
