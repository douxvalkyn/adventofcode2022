package advent2022.day09;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day09 {

	private static List<Position> positions = new ArrayList<>();

	private static Logger logger = LoggerFactory.getLogger(Day09.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day09 day = new Day09();
		LocalDateTime start = LocalDateTime.now();
		// day.run1bis();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day09.txt";
		List<String> res = Outil.importationString(file);
		System.err.println(res);

		Position n0 = new Position(0, 0);
		Position n1 = new Position(0, 0);
		Position n2 = new Position(0, 0);
		Position n3 = new Position(0, 0);
		Position n4 = new Position(0, 0);
		Position n5 = new Position(0, 0);
		Position n6 = new Position(0, 0);
		Position n7 = new Position(0, 0);
		Position n8 = new Position(0, 0);
		Position n9 = new Position(0, 0);
		List<Position> positions = new ArrayList<>();

		for (int i = 0; i < res.size(); i++) {
			int nbPas = Integer.valueOf(StringUtils.split(res.get(i), ' ')[1]);
			String dir = StringUtils.split(res.get(i), ' ')[0];
			for (int j = 0; j < nbPas; j++) {
				n0 = avancerHead(res.get(i), n0);
				n1 = follow(n0, n1); // n1 suit n0
				n2 = follow(n1, n2); //
				n3 = follow(n2, n3); //
				n4 = follow(n3, n4); //
				n5 = follow(n4, n5); //
				n6 = follow(n5, n6); //
				n7 = follow(n6, n7); //
				n8 = follow(n7, n8); //
				n9 = follow(n8, n9); //

				// noter les positions de n9;
				boolean dejaVisited = false;
				Position positionN9 = new Position(n9.getX(), n9.getY());
				for (Position pos : positions) {
					if (positionN9.getX() == pos.getX() && positionN9.getY() == pos.getY()) {
						dejaVisited = true;
					}
				}
				if (!dejaVisited) {
					positions.add(positionN9);
				}
			}
		}

		System.out.println(positions);
		System.out.println(positions.size());

	}

	public void run1bis() {
		String file = "src/main/resources/advent2022/day09.txt";
		List<String> res = Outil.importationString(file);
		System.err.println(res);

		// coord de depart de Tail et Head
		int hx = 0;
		int hy = 0;
		int tx = 0;
		int ty = 0;
		Position n0 = new Position(hx, hy);
		Position n1 = new Position(tx, ty);
		List<Position> positions = new ArrayList<>();

		for (int i = 0; i < res.size(); i++) {
			int nbPas = Integer.valueOf(StringUtils.split(res.get(i), ' ')[1]);
			String dir = StringUtils.split(res.get(i), ' ')[0];
			for (int j = 0; j < nbPas; j++) {
				n0 = avancerHead(res.get(i), n0);
				n1 = follow(n0, n1); // n1 suit n0

				// noter les positions de n1;
				boolean dejaVisited = false;
				Position positionN1 = new Position(n1.getX(), n1.getY());
				for (Position pos : positions) {
					if (positionN1.getX() == pos.getX() && positionN1.getY() == pos.getY()) {
						dejaVisited = true;
					}
				}
				if (!dejaVisited) {
					positions.add(positionN1);
				}
			}
		}

		System.out.println(positions);
		System.out.println(positions.size());

	}

	private Position follow(Position n0, Position n1) {

		// si n0 et n1 se touchent , rien ne se passe
		if (n0.getX() == n1.getX() && n0.getY() == n1.getY()) { // n0 et n1 confondus
			return n1;
		}
		if (Math.abs(n0.getX() - n1.getX()) <= 1 && Math.abs(n0.getY() - n1.getY()) <= 1) { // n0 et n1 adjacents
			return n1;
		}

		// si n0 et n1 sont sur la même ligne/colonne, à 2 cases d'ecart
		if (n0.getY() == n1.getY()) { // même ligne
			if (n0.getX() == (n1.getX() + 2)) { // n0 est à droite de n1
				n1.setX(n1.getX() + 1);
				return n1;
			}
			if ((n0.getX() + 2) == (n1.getX())) { // n0 est à gauche de n1
				n1.setX(n1.getX() - 1);
				return n1;
			}
		}

		if (n0.getX() == n1.getX()) { // même colonne
			if (n0.getY() == (n1.getY() + 2)) { // n0 est au dessus de n1
				n1.setY(n1.getY() + 1);
				return n1;
			}
			if ((n0.getY() + 2) == (n1.getY())) { // n0 est en dessous de n1
				n1.setY(n1.getY() - 1);
				return n1;
			}
		}

		// sinon si n0 et n1 ne se touchent pas et ne sont pas sur même ligne/colonne
		if ((n0.getX() != n1.getX() && n0.getY() != n1.getY()) && !(n0.getY() == n1.getY() || n0.getX() == n1.getX())) {
			// deplacement en diagonale

			if (n0.getY() > n1.getY() && n0.getX() > n1.getX()) {// n0 en haut à droite de n1
				n1.setX(n1.getX() + 1);
				n1.setY(n1.getY() + 1);
				return n1;
			}
			if (n0.getY() < n1.getY() && n0.getX() > n1.getX()) {// n0 en bas à droite de n1
				n1.setX(n1.getX() + 1);
				n1.setY(n1.getY() - 1);
				return n1;
			}
			if (n0.getY() < n1.getY() && n0.getX() < n1.getX()) {// n0 en bas à gauche de n1
				n1.setX(n1.getX() - 1);
				n1.setY(n1.getY() - 1);
				return n1;
			}
			if (n0.getY() > n1.getY() && n0.getX() < n1.getX()) {// n0 en haut à gauche de n1
				n1.setX(n1.getX() - 1);
				n1.setY(n1.getY() + 1);
				return n1;
			}
		}

		System.err.println("pb");

		// si pas de mouvement
		return n1;
	}

	private Position avancerHead(String instruction, Position n0) {
		String direction = StringUtils.split(instruction, ' ')[0];

		switch (direction) {
			case "U":
				n0.setY(n0.getY() + 1);
				break;
			case "D":
				n0.setY(n0.getY() - 1);
				break;
			case "R":
				n0.setX(n0.getX() + 1);
				break;
			case "L":
				n0.setX(n0.getX() - 1);
				break;

			default:
				break;
		}

		return n0;

	}

	public void run1() {
		String file = "src/main/resources/advent2022/day09.txt";
		List<String> res = Outil.importationString(file);
		System.err.println(res);

		// coord de depart de Tail et Head
		int hx = 0;
		int hy = 0;
		int tx = 0;
		int ty = 0;

		for (int i = 0; i < res.size(); i++) {
			// deplacement de H
			String direction = StringUtils.split(res.get(i), ' ')[0];
			int nbPas = Integer.valueOf(StringUtils.split(res.get(i), ' ')[1]);
			List<Integer> coord = avancer(direction, nbPas, hx, hy, tx, ty);
			hx = coord.get(0);
			hy = coord.get(1);
			tx = coord.get(2);
			ty = coord.get(3);

		}
		System.out.println(positions.size());

	}

	private List<Integer> avancer(String direction, int nbPas, int hx, int hy, int tx, int ty) {
		for (int i = 0; i < nbPas; i++) {
			switch (direction) {
				case "U":
					hy = hy + 1;
					if (hx == tx && hy != (ty + 1) && hy != ty) { // H et T sur la même colonne mais pas collés
						ty = ty + 1;
					} else {
						if (Math.abs(hy - ty) <= 1) {
							// do nothing
						} else {
							tx = hx;
							ty = ty + 1;
						}
					}
					break;
				case "D":
					hy = hy - 1;
					if (hx == tx && hy != (ty - 1) && hy != ty) { // H et T sur la même colonne mais pas collés
						ty = ty - 1;
					} else {
						if (Math.abs(hy - ty) <= 1) {
							// do nothing
						} else {
							tx = hx;
							ty = ty - 1;
						}
					}
					break;
				case "L":
					hx = hx - 1;
					if (hy == ty && hx != (tx - 1) && hx != tx) { // H et T sur la même ligne mais pas collés
						tx = tx - 1;
					} else {
						if (Math.abs(hx - tx) <= 1) {
							// do nothing
						} else {
							ty = hy;
							tx = tx - 1;
						}
					}
					break;
				case "R":
					hx = hx + 1;
					if (hy == ty && hx != (tx + 1) && hx != tx) { // H et T sur la même ligne mais pas collés
						tx = tx + 1;
					} else {
						if (Math.abs(hx - tx) <= 1) {
							// do nothing
						} else {
							ty = hy;
							tx = tx + 1;
						}
					}
					break;

				default:
					break;
			}
			// System.out.println(hx + " " + hy);
			// System.out.println(tx + " " + ty);

			// noter les positions visitees par T
			Position p = new Position(tx, ty);
			boolean dejaVisited = false;
			for (Position pos : positions) {
				if (pos.getX() == tx && pos.getY() == ty) {
					dejaVisited = true;
				}
			}
			if (!dejaVisited) {
				positions.add(p);
			}
		}
		List<Integer> coord = new ArrayList<Integer>();
		coord.add(hx);
		coord.add(hy);
		coord.add(tx);
		coord.add(ty);
		return coord;

	}

}