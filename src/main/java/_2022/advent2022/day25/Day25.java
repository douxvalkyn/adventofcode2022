package advent2022.day25;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day25 {

	private static Logger logger = LoggerFactory.getLogger(Day25.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day25 day = new Day25();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day25.txt";
		List<String> input = Outil.importationString(file);
		long total = 0;
		for (int i = 1; i <= 120; i++) {
			long nombre = decoder(input.get(i - 1));
			total = total + nombre;
		}
		System.out.println(total);
		String res = "0";
		for (int i = 1; i <= 120; i++) {
			res = add(res, input.get(i - 1));
		}
		System.out.println(decoder(res));

	}

	private String add(String snafu1, String snafu2) {
		// ajouter des 0 devant si besoin
		if (snafu1.length() < snafu2.length()) {
			int diff = snafu2.length() - snafu1.length();
			for (int i = 0; i < diff; i++) {
				snafu1 = "0" + snafu1;
			}
		}
		if (snafu1.length() > snafu2.length()) {
			int diff = snafu1.length() - snafu2.length();
			for (int i = 0; i < diff; i++) {
				snafu2 = "0" + snafu2;
			}
		}

		// additionner en colonne
		String sum = "";
		for (int i = 0; i < snafu1.length(); i++) {
			char s1 = snafu1.charAt(i);
			char s2 = snafu2.charAt(i);
			int sn1 = 0;
			int sn2 = 0;

			if (s1 == '2') {
				sn1 = 2;
			}
			if (s1 == '1') {
				sn1 = 1;
			}
			if (s1 == '0') {
				sn1 = 0;
			}
			if (s1 == '-') {
				sn1 = -1;
			}
			if (s1 == '=') {
				sn1 = -2;
			}
			if (s2 == '2') {
				sn2 = 2;
			}
			if (s2 == '1') {
				sn2 = 1;
			}
			if (s2 == '0') {
				sn2 = 0;
			}
			if (s2 == '-') {
				sn2 = -1;
			}
			if (s2 == '=') {
				sn2 = -2;
			}

			int somme = sn1 + sn2;
			String resultat = "";
			if (somme == -4) {
				resultat = "#";
			}
			if (somme == -3) {
				resultat = "*";
			}
			if (somme == -2) {
				resultat = "=";
			}
			if (somme == -1) {
				resultat = "-";
			}
			if (somme == 0) {
				resultat = "0";
			}
			if (somme == 1) {
				resultat = "1";
			}
			if (somme == 2) {
				resultat = "2";
			}
			if (somme == 3) {
				resultat = "3";
			}
			if (somme == 4) {
				resultat = "4";
			}
			sum = sum + resultat;

		}

		// ajouter qq zeros devant
		sum = "0" + sum;

		// regulariser les symboles: =-012 de droite à gauche
		for (int i = sum.length() - 1; i >= 0; i--) {
			char carac = sum.charAt(i);
			if (carac == '4') {
				sum = sum.substring(0, i) + '-' + sum.substring(i + 1);
				// ajouter 1 au caractere precedent
				char caracPrecedent = sum.charAt(i - 1);
				if (caracPrecedent == '4') {
					sum = sum.substring(0, i - 1) + '5' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '3') {
					sum = sum.substring(0, i - 1) + '4' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '2') {
					sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '1') {
					sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '0') {
					sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '-') {
					sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '=') {
					sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
				}
				// if (caracPrecedent=='*') {sum = sum.substring(0, i-1) + '=' + sum.substring(i-1 + 1); }
				// if (caracPrecedent=='#') {sum = sum.substring(0, i-1) + '*' + sum.substring(i-1 + 1); }
			}
			if (carac == '3') {
				sum = sum.substring(0, i) + '=' + sum.substring(i + 1);
				// ajouter 1 au caractere precedent
				char caracPrecedent = sum.charAt(i - 1);
				if (caracPrecedent == '4') {
					sum = sum.substring(0, i - 1) + '5' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '3') {
					sum = sum.substring(0, i - 1) + '4' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '2') {
					sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '1') {
					sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '0') {
					sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '-') {
					sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '=') {
					sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
				}
				// if (caracPrecedent=='*') {sum = sum.substring(0, i-1) + '=' + sum.substring(i-1 + 1); }
				// if (caracPrecedent=='#') {sum = sum.substring(0, i-1) + '*' + sum.substring(i-1 + 1); }
			}
			if (carac == '#') {
				sum = sum.substring(0, i) + '1' + sum.substring(i + 1);
				// retirer 1 au caractere precedent
				char caracPrecedent = sum.charAt(i - 1);
				if (caracPrecedent == '4') {
					sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '3') {
					sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '2') {
					sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '1') {
					sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '0') {
					sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '-') {
					sum = sum.substring(0, i - 1) + '=' + sum.substring(i - 1 + 1);
				}
				// if (caracPrecedent=='=') {sum = sum.substring(0, i-1) + '*' + sum.substring(i-1 + 1); }
				// if (caracPrecedent=='*') {sum = sum.substring(0, i-1) + '#' + sum.substring(i-1 + 1); }
				if (caracPrecedent == '#') {
					sum = sum.substring(0, i - 1) + '@' + sum.substring(i - 1 + 1);
				}
			}
			if (carac == '*') {
				sum = sum.substring(0, i) + '2' + sum.substring(i + 1);
				// retirer 1 au caractere precedent
				char caracPrecedent = sum.charAt(i - 1);
				if (caracPrecedent == '4') {
					sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '3') {
					sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '2') {
					sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '1') {
					sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '0') {
					sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '-') {
					sum = sum.substring(0, i - 1) + '=' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '=') {
					sum = sum.substring(0, i - 1) + '*' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '*') {
					sum = sum.substring(0, i - 1) + '#' + sum.substring(i - 1 + 1);
				}
				if (caracPrecedent == '#') {
					sum = sum.substring(0, i - 1) + '@' + sum.substring(i - 1 + 1);
				}
			}
		}

		// correction si un 5 ou un -5 apparait:
		while (sum.contains("5")) {
			for (int i = 1; i < sum.length(); i++) {
				if (sum.charAt(i) == '5') {
					sum = sum.substring(0, i) + '0' + sum.substring(i + 1);
					char caracPrecedent = sum.charAt(i - 1);
					if (caracPrecedent == '4') {
						sum = sum.substring(0, i - 1) + '5' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '3') {
						sum = sum.substring(0, i - 1) + '4' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '2') {
						sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '1') {
						sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '0') {
						sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '-') {
						sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '=') {
						sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '*') {
						sum = sum.substring(0, i - 1) + '=' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '#') {
						sum = sum.substring(0, i - 1) + '*' + sum.substring(i - 1 + 1);
					}
				}
			}
		}

		while (sum.contains("@")) {
			for (int i = 1; i < sum.length(); i++) {
				if (sum.charAt(i) == '@') {
					sum = sum.substring(0, i) + '0' + sum.substring(i + 1);
					char caracPrecedent = sum.charAt(i - 1);
					if (caracPrecedent == '4') {
						sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '3') {
						sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '2') {
						sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '1') {
						sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '0') {
						sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '-') {
						sum = sum.substring(0, i - 1) + '=' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '=') {
						sum = sum.substring(0, i - 1) + '*' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '*') {
						sum = sum.substring(0, i - 1) + '#' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '#') {
						sum = sum.substring(0, i - 1) + '@' + sum.substring(i - 1 + 1);
					}
				}
			}
		}

		while (sum.contains("3")) {
			for (int i = 1; i < sum.length(); i++) {
				if (sum.charAt(i) == '3') {
					sum = sum.substring(0, i) + '=' + sum.substring(i + 1);
					// ajouter 1 au caractere precedent
					char caracPrecedent = sum.charAt(i - 1);
					if (caracPrecedent == '4') {
						sum = sum.substring(0, i - 1) + '5' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '3') {
						sum = sum.substring(0, i - 1) + '4' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '2') {
						sum = sum.substring(0, i - 1) + '3' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '1') {
						sum = sum.substring(0, i - 1) + '2' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '0') {
						sum = sum.substring(0, i - 1) + '1' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '-') {
						sum = sum.substring(0, i - 1) + '0' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '=') {
						sum = sum.substring(0, i - 1) + '-' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '*') {
						sum = sum.substring(0, i - 1) + '=' + sum.substring(i - 1 + 1);
					}
					if (caracPrecedent == '#') {
						sum = sum.substring(0, i - 1) + '*' + sum.substring(i - 1 + 1);
					}
				}
			}
		}

		return (sum);
	}

	private long decoder(String snafu) {
		long total = 0;
		for (int i = 0; i < snafu.length(); i++) {

			char nb = snafu.charAt(i);
			int nombreBaseCinq = transformeNb(nb);
			double nombre = Math.pow(5, snafu.length() - 1 - i);
			long resultat = (long) ((long) nombreBaseCinq * nombre);
			total = total + resultat;
		}
		return total;
	}

	private int transformeNb(char nb) {
		switch (nb) {
			case '2':
				return 2;
			case '1':
				return 1;
			case '0':
				return 0;
			case '-':
				return -1;
			case '=':
				return -2;
		}
		return 0;
	}

}