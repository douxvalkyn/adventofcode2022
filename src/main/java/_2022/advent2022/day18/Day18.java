package advent2022.day18;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.GraphWeighted;
import communs.NodeWeighted;
import communs.Outil;

public class Day18 {

	private static Logger logger = LoggerFactory.getLogger(Day18.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day18.class.getSimpleName() + "]");
		Day18 day = new Day18();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		// day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	/**
	 * Site à consulter pour visualiser les cubes en 3D: https://www.nctm.org/Classroom-Resources/Illuminations/Interactives/Isometric-Drawing-Tool/
	 */

	public void run2() {
		String file = "src/main/resources/advent2022/day18.txt";
		List<String> res = Outil.importationString(file);

		// recherche des cubes d'air totalement entourés par les cubes d'obsidienne
		List<NodeWeighted> casesLaveOuInterne = new ArrayList<>();

		// creation des nodes=tous les cubes dans l'espace de recherche
		GraphWeighted graphWeighted = new GraphWeighted(true);
		List<NodeWeighted> nodes = new ArrayList<>();
		int identifiant = 0;
		for (int i = 0; i < 22; i++) {
			for (int j = 0; j < 22; j++) {
				for (int k = 0; k < 22; k++) {
					identifiant++;
					String name = "" + i + "," + j + "," + k;
					NodeWeighted node = new NodeWeighted(identifiant, name);
					nodes.add(node);
				}
			}
		}

		// creation des edges
		for (NodeWeighted node : nodes) {
			int x = Integer.valueOf(StringUtils.split(node.getName(), ',')[0]);
			int y = Integer.valueOf(StringUtils.split(node.getName(), ',')[1]);
			int z = Integer.valueOf(StringUtils.split(node.getName(), ',')[2]);

			// le node considéré est il un cube de lave ?
			if (!estUnCubeDeLave(res, x, y, z)) {

				// recherche des voisins
				List<NodeWeighted> voisins = new ArrayList<>();
				NodeWeighted voisin1 = rechercheNodeSelonCoordonnees(nodes, x + 1, y, z);
				NodeWeighted voisin2 = rechercheNodeSelonCoordonnees(nodes, x - 1, y, z);
				NodeWeighted voisin3 = rechercheNodeSelonCoordonnees(nodes, x, y + 1, z);
				NodeWeighted voisin4 = rechercheNodeSelonCoordonnees(nodes, x, y - 1, z);
				NodeWeighted voisin5 = rechercheNodeSelonCoordonnees(nodes, x, y, z + 1);
				NodeWeighted voisin6 = rechercheNodeSelonCoordonnees(nodes, x, y, z - 1);
				voisins.add(voisin1);
				voisins.add(voisin2);
				voisins.add(voisin3);
				voisins.add(voisin4);
				voisins.add(voisin5);
				voisins.add(voisin6);

				// analyse du node et d'un voisin: si au moins un est un cube de lave: pas de creation de lien
				for (NodeWeighted voisin : voisins) {
					if (voisin != null) {
						int vx = Integer.valueOf(StringUtils.split(voisin.getName(), ',')[0]);
						int vy = Integer.valueOf(StringUtils.split(voisin.getName(), ',')[1]);
						int vz = Integer.valueOf(StringUtils.split(voisin.getName(), ',')[2]);

						if (!estUnCubeDeLave(res, vx, vy, vz)) {
							graphWeighted.addEdge(node, voisin, 1);
						}
					}
				}

			} else {
				casesLaveOuInterne.add(node); // le node considéré est une case de lave
			}

		}

		// on teste chaque node pour voir s'il peut être relié à l'exterieur (ie le node 0,0,0)
		// Si oui, c'est une case externe, sinon c'est une case interne
		NodeWeighted nodeExterieur = rechercheNodeSelonCoordonnees(nodes, 0, 0, 0);
		for (int index = 1; index < 10648; index++) {
			if (index % 100 == 0) {
				System.out.println(index);
			}
			graphWeighted.resetNodesVisited();
			int nx = Integer.valueOf(StringUtils.split(nodes.get(index).getName(), ',')[0]);
			int ny = Integer.valueOf(StringUtils.split(nodes.get(index).getName(), ',')[1]);
			int nz = Integer.valueOf(StringUtils.split(nodes.get(index).getName(), ',')[2]);

			if (!nodes.get(index).getName().equals("0,0,0") && !estUnCubeDeLave(res, nx, ny, nz)) {
				String[] resultat = graphWeighted.DijkstraShortestPath(nodeExterieur, nodes.get(index));
				if (resultat != null) {
					// System.out.println(resultat[0]);
					// System.out.println("longueur du chemin: " + resultat[1]);
				} else {
					casesLaveOuInterne.add(nodes.get(index));
					System.out.println("interne: " + nodes.get(index));
				}
			}
		}

	}

	private boolean estUnCubeDeLave(List<String> res, int x, int y, int z) {
		for (String ligne : res) {
			int xLave = Integer.valueOf(StringUtils.split(ligne, ',')[0]);
			int yLave = Integer.valueOf(StringUtils.split(ligne, ',')[1]);
			int zLave = Integer.valueOf(StringUtils.split(ligne, ',')[2]);

			if (xLave == x && yLave == y && zLave == z) {
				return true;
			}

		}

		return false;
	}

	private NodeWeighted rechercheNodeSelonCoordonnees(List<NodeWeighted> nodes, int i, int j, int k) {
		String name = "" + i + "," + j + "," + k;
		for (NodeWeighted node : nodes) {
			if (node.getName().equals(name)) {
				return node;
			}
		}
		return null;
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day18c.txt";
		List<String> res = Outil.importationString(file);
		int nbFacesVisiblesTotal = 0;
		for (String cube1 : res) {
			int nbFacesVisibles = 6;
			for (String cube2 : res) {
				int nbCoordIdentiques = 0;
				int coord1Identique = 0;
				int coord2Identique = 0;
				int coord3Identique = 0;
				if (cube1 != cube2) {
					Integer cube1X = Integer.valueOf(StringUtils.split(cube1, ',')[0]);
					Integer cube1Y = Integer.valueOf(StringUtils.split(cube1, ',')[1]);
					Integer cube1Z = Integer.valueOf(StringUtils.split(cube1, ',')[2]);
					Integer cube2X = Integer.valueOf(StringUtils.split(cube2, ',')[0]);
					Integer cube2Y = Integer.valueOf(StringUtils.split(cube2, ',')[1]);
					Integer cube2Z = Integer.valueOf(StringUtils.split(cube2, ',')[2]);

					if (cube1X == cube2X) {
						coord1Identique = 1;
					}
					if (cube1Y == cube2Y) {
						coord2Identique = 1;
					}
					if (cube1Z == cube2Z) {
						coord3Identique = 1;
					}
					nbCoordIdentiques = coord1Identique + coord2Identique + coord3Identique;

					if (nbCoordIdentiques == 2 && coord1Identique == 0) {
						if (Math.abs(cube1X - cube2X) == 1) {
							nbFacesVisibles--;
						}
					}
					if (nbCoordIdentiques == 2 && coord2Identique == 0) {
						if (Math.abs(cube1Y - cube2Y) == 1) {
							nbFacesVisibles--;
						}
					}
					if (nbCoordIdentiques == 2 && coord3Identique == 0) {
						if (Math.abs(cube1Z - cube2Z) == 1) {
							nbFacesVisibles--;
						}
					}
				}
			}
			nbFacesVisiblesTotal = nbFacesVisiblesTotal + nbFacesVisibles;
		}
		System.out.println("total  : " + nbFacesVisiblesTotal);
	}

}