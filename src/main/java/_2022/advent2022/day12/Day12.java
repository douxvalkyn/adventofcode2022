package advent2022.day12;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.*;

public class Day12 {

	private static Logger logger = LoggerFactory.getLogger(Day12.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day12 day = new Day12();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day12b.txt";
		List<String> res = Outil.importationString(file);
		Grille grille = creationGrille(res);
		Integer sol = creationGraphe2(grille);

		System.out.println("run2: " + sol);

	}

	public void run1() {
		String file = "src/main/resources/advent2022/day12b.txt";
		List<String> res = Outil.importationString(file);
		Grille grille = creationGrille(res);
		String sol = creationGraphe(grille);
		System.out.println("run1");
		System.out.println(StringUtils.split(sol, ' ').length - 1);
		System.out.println("------");

	}

	private Integer creationGraphe2(Grille grille) {

		List<Case> cases = grille.getCases();

		// creation des noeuds
		int rangS = 0;
		int rangE = 0;
		List<Integer> rangsA = new ArrayList<Integer>();
		for (Case cell : cases) {
			if (cell.getEtat().equals("S")) {
				rangS = cell.getRang();
				rangsA.add(cell.getRang());
			}
			if (cell.getEtat().equals("E")) {
				rangE = cell.getRang();
			}
			if (cell.getEtat().equals("a")) {
				rangsA.add(cell.getRang());
			}
		}

		List<Integer> liste = new ArrayList<>();
		for (Integer start : rangsA) {

			// creation des noeuds
			ArrayList<NodeWeighted> nodes = new ArrayList<NodeWeighted>();
			for (Case cell : cases) {
				NodeWeighted node = new NodeWeighted(cell.getRang(), "" + cell.getRang());
				nodes.add(node);
				if (cell.getEtat().equals("S")) {
					rangS = cell.getRang();
				}
				if (cell.getEtat().equals("E")) {
					rangE = cell.getRang();
				}
				if (cell.getEtat().equals("a")) {
				}
			}
			GraphWeighted graphWeighted = new GraphWeighted(true);

			// cration des liens
			for (Case cell : cases) {
				List<Case> voisins = cell.getCasesAdjacentes(grille);
				for (Case voisin : voisins) {
					if ((voisin.getValeur() - cell.getValeur()) <= 1) {
						graphWeighted.addEdge(nodes.get(cell.getRang()), nodes.get(voisin.getRang()), 1);
					}

				}
			}
			String[] res = graphWeighted.DijkstraShortestPath(nodes.get(start), nodes.get(rangE));
			if (res != null) {
				System.out.println(StringUtils.split(res[0], ' ').length - 1);
				liste.add(StringUtils.split(res[0], ' ').length - 1);
			}
		}

		return Outil.min(liste);
	}

	private String creationGraphe(Grille grille) {
		GraphWeighted graphWeighted = new GraphWeighted(true);
		List<Case> cases = grille.getCases();

		// creation des noeuds
		int rangS = 0;
		int rangE = 0;
		List<NodeWeighted> nodes = new ArrayList<NodeWeighted>();
		for (Case cell : cases) {
			NodeWeighted node = new NodeWeighted(cell.getRang(), "" + cell.getRang());
			nodes.add(node);
			if (cell.getEtat().equals("S")) {
				rangS = cell.getRang();
			}
			if (cell.getEtat().equals("E")) {
				rangE = cell.getRang();
			}
		}

		// cration des liens
		for (Case cell : cases) {
			List<Case> voisins = cell.getCasesAdjacentes(grille);
			for (Case voisin : voisins) {
				if ((voisin.getValeur() - cell.getValeur()) <= 1) {
					graphWeighted.addEdge(nodes.get(cell.getRang()), nodes.get(voisin.getRang()), 1);
				}

			}
		}
		String[] res = graphWeighted.DijkstraShortestPath(nodes.get(rangS), nodes.get(rangE));
		return res[0];
	}

	private Grille creationGrille(List<String> res) {
		Grille grille = new Grille(res.get(0).length(), res.size());
		List<Case> cases = new ArrayList<>();
		int k = 0;
		for (int i = 0; i < res.size(); i++) {
			for (int j = 0; j < res.get(0).length(); j++) {
				Case cell = new Case(i, j);
				cell.setEtat("" + res.get(i).charAt(j));
				cell.setValeur(calculeValeurLettre(res.get(i).charAt(j)));
				cell.setRang(k);
				k++;
				cases.add(cell);
			}
		}
		grille.setCases(cases);

		return grille;
	}

	public int calculeValeurLettre(char ch) {
		if (ch == 'S') {
			return 0;
		}
		if (ch == 'E') {
			return 27;
		}

		int valeur = 0;
		int temp = (int) ch;
		if (temp <= 122 & temp >= 97) { // for lower case
			valeur = +temp - 96;
		}
		if (temp <= 96) { // for upper case
			valeur = +temp - 64 + 26;
		}
		return valeur;
	}

}