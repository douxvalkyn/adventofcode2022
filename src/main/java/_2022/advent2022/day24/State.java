package advent2022.day24;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import advent2022.day15.Point;

public class State {

	private int nbDepl;
	private int xPositionActuelle;
	private int yPositionActuelle;
	private List<Point> previousPositions;


	public State() {
		super();
		this.nbDepl = 0;
		this.previousPositions = new ArrayList<Point>();
	}

	public List<Point> getPreviousPositions() {
		return previousPositions;
	}

	public void setPreviousPositions(List<Point> previousPositions) {
		this.previousPositions = previousPositions;
	}

	public int getNbDepl() {
		return nbDepl;
	}

	public void setNbDepl(int nbDepl) {
		this.nbDepl = nbDepl;
	}

	public int getxPositionActuelle() {
		return xPositionActuelle;
	}

	public void setxPositionActuelle(int xPositionActuelle) {
		this.xPositionActuelle = xPositionActuelle;
	}

	public int getyPositionActuelle() {
		return yPositionActuelle;
	}

	public void setyPositionActuelle(int yPositionActuelle) {
		this.yPositionActuelle = yPositionActuelle;
	}

	@Override
	public String toString() {
		return "State [nbDepl=" + nbDepl + ", xPositionActuelle=" + xPositionActuelle + ", yPositionActuelle="
				+ yPositionActuelle + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(nbDepl, xPositionActuelle, yPositionActuelle);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return nbDepl == other.nbDepl && xPositionActuelle == other.xPositionActuelle
				&& yPositionActuelle == other.yPositionActuelle;
	}

}
