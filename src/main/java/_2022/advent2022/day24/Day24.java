package advent2022.day24;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2022.day15.Point;
import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day24 {

	private static Logger logger = LoggerFactory.getLogger(Day24.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day24.class.getSimpleName() + "]");
		Day24 day = new Day24();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);

		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() { 
		String file = "src/main/resources/advent2022/day24.txt";
		List<String> input = Outil.importationString(file);
		Grille grille = new Grille(input.get(0).length(), input.size());
		creationGrille(grille, input);
		List<Blizzard> blizzards = determinerListeBlizzards(grille);
		Map<Integer, List<Blizzard>> map = creerMapBlizzards(grille, blizzards);
		Case caseDepart = determinerPointDepart(grille);
		Case caseArrivee = determinerPointArrivee(grille);
		int timeInit=0;
		int go = bfs(grille, caseDepart, caseArrivee, map, timeInit);
		System.err.println("go: "+go);
		int goBack = bfs(grille, caseArrivee,caseDepart, map, go);
		System.err.println("goBack: "+ goBack);
		int goAgain = bfs(grille, caseDepart, caseArrivee, map, goBack);
		System.err.println("goAgain: " +goAgain);
	}

	public void run1() { 
		String file = "src/main/resources/advent2022/day24b.txt";
		List<String> input = Outil.importationString(file);
		Grille grille = new Grille(input.get(0).length(), input.size());
		creationGrille(grille, input);
	//	grille.affichageEtat();
		List<Blizzard> blizzards = determinerListeBlizzards(grille);
		Map<Integer, List<Blizzard>> map = creerMapBlizzards(grille, blizzards);
		Case caseDepart = determinerPointDepart(grille);
		Case caseArrivee = determinerPointArrivee(grille);
		int timeInit=0;
		int res = bfs(grille, caseDepart, caseArrivee, map, timeInit);
		System.out.println(res);
	}

	private Map<Integer, List<Blizzard>> creerMapBlizzards(Grille grille, List<Blizzard> blizzards) {
		Map<Integer, List<Blizzard>> map = new HashMap<Integer, List<Blizzard>>();
		map.put(0, blizzards);
		for (int minute=1;minute<1000;minute++) {

			List<Blizzard> blizzards3 = deplacementBlizzards(grille, blizzards);
			blizzards = new ArrayList<Blizzard>();
			for (Blizzard bliz : blizzards3) {
				Blizzard bliz2 = new Blizzard();
				bliz2.setFacing(bliz.getFacing());
				bliz2.setX(bliz.getX());
				bliz2.setY(bliz.getY());
				blizzards.add(bliz2);
			}
			map.put(minute, blizzards);
		}
		return map;
	}

	private List<Blizzard> determinerListeBlizzards(Grille grille) {
		List<Blizzard> blizzards = new ArrayList<>();
		List<Case> cells = grille.getCases();
		for (Case cell : cells) {
			if (cell.getEtat().equals("<")) {
				Blizzard bliz = new Blizzard();
				bliz.setX(cell.getX());
				bliz.setY(cell.getY());
				bliz.setFacing(cell.getEtat());
				blizzards.add(bliz);
			}
			if (cell.getEtat().equals(">")) {
				Blizzard bliz = new Blizzard();
				bliz.setX(cell.getX());
				bliz.setY(cell.getY());
				bliz.setFacing(cell.getEtat());
				blizzards.add(bliz);
			}
			if (cell.getEtat().equals("^")) {
				Blizzard bliz = new Blizzard();
				bliz.setX(cell.getX());
				bliz.setY(cell.getY());
				bliz.setFacing(cell.getEtat());
				blizzards.add(bliz);
			}
			if (cell.getEtat().equals("v")) {
				Blizzard bliz = new Blizzard();
				bliz.setX(cell.getX());
				bliz.setY(cell.getY());
				bliz.setFacing(cell.getEtat());
				blizzards.add(bliz);
			}
		}
		return blizzards;
	}

	private int bfs(Grille grille, Case caseDepart, Case caseArrivee, Map<Integer, List<Blizzard>> map, int timeInit) {
		caseDepart.setEtat("E");
		State stateInitial = new State();
		stateInitial.setNbDepl(timeInit);
		stateInitial.setxPositionActuelle(caseDepart.getX());
		stateInitial.setyPositionActuelle(caseDepart.getY());
		Queue<State> queue = new LinkedList<>();
		queue.add(stateInitial);

		Set<State> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial);

		while (!queue.isEmpty()) {
			State stateActuel = queue.poll();
//			if ((stateActuel.getNbDepl()+1)%10==0) {
//				System.out.println(stateActuel.getNbDepl()+1);
//			}
			if (stateActuel.getxPositionActuelle() == caseArrivee.getX() && stateActuel.getyPositionActuelle() == caseArrivee.getY()) {
				return stateActuel.getNbDepl();
			}
			Case caseActuelle = grille.getCase(stateActuel.getxPositionActuelle(), stateActuel.getyPositionActuelle());

			// ajouter les positions des blizzards sur la grille
			// nettoyage de la grille
			List<Case> cells = grille.getCases();
			for (Case cell : cells) {
				if (cell.getEtat().equals("E") || cell.getEtat().equals("<") || cell.getEtat().equals(">") || cell.getEtat().equals("^") || cell.getEtat().equals("v") || cell.getEtat().equals("1")
						|| cell.getEtat().equals("2") || cell.getEtat().equals("3") || cell.getEtat().equals("4") || cell.getEtat().equals("5")) {
					cell.setEtat(".");
				}
			}

			//deplacement des blizzards
			List<Blizzard> blizzards2 = new ArrayList<>();
			blizzards2=map.get(stateActuel.getNbDepl()+1);

			// ajout traces des blizzards
			for (Blizzard bliz : blizzards2) {
				ajouterNumerosAffichageSiPlusieursBlizzards(bliz, grille);
			}

			// ajout trace E pour meilleur affichage
			grille.getCase(caseActuelle.getX(), caseActuelle.getY()).setEtat("E");
			//grille.affichageEtat();

			// Ajout d'un state pour chaque deplacement possible
			List<Case> voisins = caseActuelle.getCasesAdjacentes(grille);
			for (Case voisin : voisins) {
				if (voisin.getEtat().equals(".")) {

					// deplacement de l'elfe
					grille.getCase(caseActuelle.getX(), caseActuelle.getY()).setEtat(".");
					voisin.setEtat("E");

					//System.out.println("move! nb depl: " + (stateActuel.getNbDepl()+1));
					//grille.affichageEtat();
					voisin.setEtat(".");

					// sauvegarde de l'etat de la situation
					State nextState = new State();
					List<Blizzard>BlizzardNew = new ArrayList<Blizzard>();
					for (Blizzard bliz : blizzards2) {
						Blizzard bliz2 = new Blizzard();
						bliz2.setFacing(bliz.getFacing());
						bliz2.setX(bliz.getX());
						bliz2.setY(bliz.getY());
						BlizzardNew.add(bliz2);
					}
					//nextState.setBlizzards(BlizzardNew);
					nextState.setNbDepl(stateActuel.getNbDepl() + 1);
					nextState.setxPositionActuelle(voisin.getX());
					nextState.setyPositionActuelle(voisin.getY());
					List<Point> previousPositions = new ArrayList<Point>();
					List<Point> previous = stateActuel.getPreviousPositions();
					for ( Point pt:previous) {
						previousPositions.add(pt);
					}
					Point pNew=new Point(voisin.getX(), voisin.getY());
					previousPositions.add(pNew);
					nextState.setPreviousPositions(previousPositions);

					// si pas déjà visité, on ajoute à la queue
					if (!visitedStates.contains(nextState)) {
						queue.add(nextState);
						visitedStates.add(nextState);
					}
				}
			} //fin move
				//aucun mouvement possible: possible si la case actuelle reste libre apres deplacements des blizzards
			int x = caseActuelle.getX();
			int y = caseActuelle.getY();
			List<Blizzard> prochainsBlizzards = map.get(stateActuel.getNbDepl()+1);
			boolean noMove=true;
			for (Blizzard bliz:prochainsBlizzards) {
				if (bliz.getX()==x && bliz.getY()==y) {
					noMove=false;
				}
			}
			if (noMove) {
				// sauvegarde de l'etat de la situation
				State nextState = new State();
				//nextState.setBlizzards(blizzards2);
				nextState.setNbDepl(stateActuel.getNbDepl() + 1);
				nextState.setxPositionActuelle(stateActuel.getxPositionActuelle());
				nextState.setyPositionActuelle(stateActuel.getyPositionActuelle());
				List<Point> previousPositions = new ArrayList<Point>();
				List<Point> previous = stateActuel.getPreviousPositions();
				for ( Point pt:previous) {
					previousPositions.add(pt);
				}
				Point pNew=new Point(stateActuel.getxPositionActuelle(), stateActuel.getyPositionActuelle());
				previousPositions.add(pNew);
				nextState.setPreviousPositions(previousPositions);

				// si pas déjà visité, on ajoute à la queue
				if (!visitedStates.contains(nextState)) {
					grille.getCase(stateActuel.getxPositionActuelle(), stateActuel.getyPositionActuelle()).setEtat("E");
					//System.out.println("no move! nb depl: " + (stateActuel.getNbDepl()+1));
					// grille.affichageEtat();
					queue.add(nextState);
					visitedStates.add(nextState);
				}
			}
		}//fin while

		return 0;
	}

	private int ajouterNumerosAffichageSiPlusieursBlizzards(Blizzard bliz, Grille grille) {

		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals(".")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat(bliz.getFacing());
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("<") || grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals(">")
				|| grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("^") || grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("v")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("2");
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("2")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("3");
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("3")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("4");
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("3")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("4");
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("4")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("5");
			return 1;
		}
		if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("5")) {
			grille.getCase(bliz.getX(), bliz.getY()).setEtat("6");
			return 1;
		}
		return 0;
	}


	private List<Blizzard> deplacementBlizzards(Grille grille, List<Blizzard> blizzards) {
		// effacer les (anciens) blizzards de la grille
		for (Case cell : grille.getCases()) {
			if (cell.getEtat().equals("<")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals(">")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("^")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("v")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("1")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("2")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("3")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("4")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("5")) {
				cell.setEtat(".");
			}
			if (cell.getEtat().equals("E")) {
				cell.setEtat(".");
			}
		}

		// deplacer les blizzards
		List<Blizzard> blizzardsNew =new ArrayList<Blizzard>();
		for (Blizzard bliz : blizzards) {
			Blizzard bliz2 = new Blizzard();
			bliz2.setFacing(bliz.getFacing());
			bliz2.setX(bliz.getX());
			bliz2.setY(bliz.getY());
			blizzardsNew.add(bliz2);
		}
		for (Blizzard bliz : blizzardsNew) {
			switch (bliz.getFacing()) {
			case ">":
				bliz.setX(bliz.getX() + 1);
				if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("#")) { // impossible, le blizzard revient de l'autre côté
					bliz.setX(1);
				}
				break;
			case "<":
				bliz.setX(bliz.getX() - 1);
				if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("#")) { // impossible, le blizzard revient de l'autre côté
					bliz.setX(grille.getNbCol() - 2);
				}
				break;
			case "^":
				bliz.setY(bliz.getY() - 1);
				if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("#")) { // impossible, le blizzard revient de l'autre côté
					bliz.setY(grille.getNbLignes() - 2);
				}
				break;
			case "v":
				bliz.setY(bliz.getY() + 1);
				if (grille.getCase(bliz.getX(), bliz.getY()).getEtat().equals("#")) { // impossible, le blizzard revient de l'autre côté
					bliz.setY(1);
				}
				break;
			default:
				break;
			}
		}

		return blizzardsNew;
	}


	private Case determinerPointArrivee(Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			if (grille.getCase(i, grille.getNbLignes() - 1).getEtat().equals(".")) {
				return grille.getCase(i, grille.getNbLignes() - 1);
			}
		}
		return null;
	}

	private Case determinerPointDepart(Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			if (grille.getCase(i, 0).getEtat().equals(".")) {
				return grille.getCase(i, 0);
			}
		}
		return null;
	}

	private void creationGrille(Grille grille, List<String> input) {
		for (int i = 0; i < input.size(); i++) {
			String ligne = input.get(i);
			for (int j = 0; j < ligne.length(); j++) {
				grille.getCase(j, i).setEtat("" + ligne.charAt(j));
			}
		}
	}

}