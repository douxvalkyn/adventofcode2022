package advent2022.day24;

import java.util.Objects;

public class Blizzard {

	@Override
	public int hashCode() {
		return Objects.hash(facing, x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Blizzard other = (Blizzard) obj;
		return Objects.equals(facing, other.facing) && x == other.x && y == other.y;
	}

	private int x;
	private int y;
	private String facing;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getFacing() {
		return facing;
	}

	public void setFacing(String facing) {
		this.facing = facing;
	}

	public Blizzard() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Blizzard [x=" + x + ", y=" + y + ", facing=" + facing + "]";
	}

}
