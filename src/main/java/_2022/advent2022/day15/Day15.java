package advent2022.day15;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day15 {

	private static Logger logger = LoggerFactory.getLogger(Day15.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day15 day = new Day15();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	
	public void run1() {
		String file = "src/main/resources/advent2022/day15.txt";
		List<String> res = Outil.importationString(file);
		//System.err.println(res);
		 List<Long> sensors = creationSensors(res); // 1sensor = 4 nombres
		 System.err.println();
		// int ligne=10;
		 Long ligne=2000000L;
		 int run1 = trouverNbIntersectionSensorsAvecLigne(sensors, ligne);
		
	}
	
	
	
	public void run2() {
		String file = "src/main/resources/advent2022/day15.txt";
		List<String> res = Outil.importationString(file);
		//System.err.println(res);
		 List<Long> sensors = creationSensors(res); // 1sensor = 4 nombres
		 List<Point> points = recherche(sensors);
		 System.out.println(points.size());
		 filtrerPointsExterieurTousDiamants(points, sensors);
		
	}

	





	private List<Point> filtrerPointsInsideZone(List<Point> points) {
		List<Point> pointsFiltre = new ArrayList<>();
		for (Point point:points) {
			if (point.getX()<=4000000 && point.getX()>=0 && point.getY()<=4000000 && point.getY()>=0) {
				pointsFiltre.add(point);
			}
		}
		return pointsFiltre;
	}





	private List<Point> recherche(List<Long> sensors) {
		List<Point> points = new ArrayList<>();
		
		for (int i=0;i<sensors.size();i=i+4) {
			//recherche des points au bord et à l'exterieur
			Long sensorX = sensors.get(i);
			Long sensorY = sensors.get(i+1);
			Long beaconX = sensors.get(i+2);
			Long beaconY = sensors.get(i+3);
			Long distanceSensorBeacon = Math.abs(sensorX-beaconX)+Math.abs(sensorY-beaconY);
			
			//Determiner les 4 points du carré autour du carré initial
			long xA = sensorX;
			long xC = sensorX;
			long yA=sensorY-distanceSensorBeacon-1;
			long yC=sensorY+distanceSensorBeacon+1;
			long xB=sensorX-distanceSensorBeacon-1;
			long xD=sensorX+distanceSensorBeacon+1;
			long yB=sensorY;
			long yD=sensorY;
			
			//ligne AB
			List<Point> pointsAB = new ArrayList<>();
			boolean goAB=true;
			int j=0;
			while (goAB) {
				Point point = new Point(xA-j, yA+j);
				pointsAB.add(point);
				if (yA+j==yB) {
					goAB=false;
				}
				j++;
			}
			points.addAll(pointsAB);
			
			//ligne BC
			List<Point> pointsBC = new ArrayList<>();
			boolean goBC=true;
			j=0;
			while (goBC) {
				Point point = new Point(xB+j, yB+j);
				pointsBC.add(point);
				if (yB+j==yC) {
					goBC=false;
				}
				j++;
			}
			points.addAll(pointsBC);
			
			//ligne CD
			List<Point> pointsCD = new ArrayList<>();
			boolean goCD=true;
			j=0;
			while (goCD) {
				Point point = new Point(xC+j, yC-j);
				pointsCD.add(point);
				if (yC-j==yD) {
					goCD=false;
				}
				j++;
			}
			points.addAll(pointsCD);
			
			//ligne DA
			List<Point> pointsDA = new ArrayList<>();
			boolean goDA=true;
			j=0;
			while (goDA) {
				Point point = new Point(xD-j, yD-j);
				pointsDA.add(point);
				if (yD-j==yA) {
					goDA=false;
				}
				j++;
			}
			points.addAll(pointsDA);
			points=filtrerPointsInsideZone(points);
			
		}
		
		return points;
	}





	private void filtrerPointsExterieurTousDiamants(List<Point> points, List<Long> sensors) {
		int xMax = 4000000;
		int yMin = 0;
		int xMin = 0;
		int yMax = 4000000;
		
		for (Point point:points) {
			boolean outside=true;
			if (point.getX()>=xMin 	&& point.getX()<=xMax && point.getY()>=yMin && point.getY()<= yMax) {
			for (int i=0;i<sensors.size();i=i+4) {
				Long sensorX = sensors.get(i);
				Long sensorY = sensors.get(i+1);
				Long beaconX = sensors.get(i+2);
				Long beaconY = sensors.get(i+3);


				//determiner borne inf et borne sup
				Long distanceSensorBeacon = Math.abs(sensorX-beaconX)+Math.abs(sensorY-beaconY);
				Long inf=-distanceSensorBeacon+Math.abs(point.getY()-sensorY)+sensorX;
				Long sup=distanceSensorBeacon-Math.abs(point.getY()-sensorY)+sensorX;

				if (point.getX()>= inf && point.getX()<=sup ) {
					
					outside=false;
				}
			}
			if (outside) {
				System.out.println(point);
				System.out.println(point.getX()*4000000+point.getY());
			}
		}
		}
	}





	
	private int trouverNbIntersectionSensorsAvecLigne(List<Long> sensors, Long ligne) {
		
		Set<Long>listeXPossibles = new HashSet<Long>();
		List<Long> Xexclusions = new ArrayList<Long>();
		
		for (int i=0;i<sensors.size();i=i+4) {
			Long sensorX = sensors.get(i);
			Long sensorY = sensors.get(i+1);
			Long beaconX = sensors.get(i+2);
			Long beaconY = sensors.get(i+3);
			
			if (beaconY.equals(ligne)) {
				Xexclusions.add(beaconX);
			}
			if (sensorY.equals(ligne)) {
				Xexclusions.add(sensorX);
			}
			
			//determiner borne inf et borne sup
			Long distanceSensorBeacon = Math.abs(sensorX-beaconX)+Math.abs(sensorY-beaconY);
			Long inf=-distanceSensorBeacon+Math.abs(ligne-sensorY)+sensorX;
			Long sup=distanceSensorBeacon-Math.abs(ligne-sensorY)+sensorX;
			if (sup>=inf) {	
				for (Long x=inf;x<=sup;x++) {
					if (!Xexclusions.contains(x)) {
					listeXPossibles.add(x);
					}
				}
				//System.out.println(listeXPossibles);
				
			}
		}
		//System.err.println(listeXPossibles.size());		
				
		
		
		return listeXPossibles.size();
	}

	

	
	private List<Long> creationSensors(List<String> res) {
		List<Long> sensors = new ArrayList<Long>();
		for (String sensor:res) {
			Long sensorX = Long.valueOf( StringUtils.split( StringUtils.split( StringUtils.split(sensor,' ')[2], '=')[1], ',')[0]  );
			Long sensorY = Long.valueOf( StringUtils.split( StringUtils.split( StringUtils.split(sensor,' ')[3], '=')[1], ':')[0]  );
			Long beaconX = Long.valueOf( StringUtils.split( StringUtils.split( StringUtils.split(sensor,' ')[8], '=')[1], ',')[0]  );
			Long beaconY = Long.valueOf( StringUtils.split( StringUtils.split( StringUtils.split(sensor,' ')[9], '=')[1], ':')[0]  );
			sensors.add(sensorX);
			sensors.add(sensorY);
			sensors.add(beaconX);
			sensors.add(beaconY);
		}
		return sensors;
	}

	
}