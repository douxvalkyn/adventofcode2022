package advent2022.day15;

public class Point {

	
	private long x;
	private long y;
	public Point(long xA, long yA) {
		super();
		this.x = xA;
		this.y = yA;
	}
	public long getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
public long getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
	
	
}
