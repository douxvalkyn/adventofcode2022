package advent2022.day10;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day10 {

	private static Logger logger = LoggerFactory.getLogger(Day10.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day10 day = new Day10();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run2() {
		String file = "src/main/resources/advent2022/day10.txt";
		List<String> res = Outil.importationString(file);
		int cycle=0;
		int registerX=1;
		String crt="";
		int sprite1Position=0; 
		int sprite2Position=1; 
		int sprite3Position=2; 
		for (int i=0;i<res.size();i++){
			String instruction =StringUtils.split( res.get(i) )[0] ;
			switch (instruction) {
			case "noop":
				cycle=cycle+1;
				crt=drawsPixel((cycle-1)%40,sprite1Position, sprite2Position, sprite3Position,crt );
				break;
			case "addx":
				cycle=cycle+1;
				crt=drawsPixel((cycle-1)%40,sprite1Position, sprite2Position, sprite3Position,crt );
				cycle=cycle+1;
				crt=drawsPixel((cycle-1)%40,sprite1Position, sprite2Position, sprite3Position,crt );
				int valeur =Integer.valueOf( StringUtils.split( res.get(i) )[1]) ;
				registerX=registerX+valeur;
				sprite1Position=registerX-1;
				sprite2Position=registerX;
				sprite3Position=registerX+1;
				
				break;
			default:
				break;
			}

		}
		affichageEnLignes(crt);
	
	}


	private void affichageEnLignes(String crt) {
		String l1 = StringUtils.substring(crt, 0, 39);
		String l2 = StringUtils.substring(crt, 40, 79);
		String l3 = StringUtils.substring(crt, 80, 119);
		String l4 = StringUtils.substring(crt, 120, 159);
		String l5 = StringUtils.substring(crt, 160, 199);
		String l6 = StringUtils.substring(crt, 200, 239);
		System.out.println(l1);
		System.out.println(l2);
		System.out.println(l3);
		System.out.println(l4);
		System.out.println(l5);
		System.out.println(l6);
		
	}

	private String drawsPixel(int indice, int sprite1Position, int sprite2Position, int sprite3Position, String crt) {
		if (indice==sprite1Position | indice==sprite2Position | indice==sprite3Position) {
			crt=crt+"#";
		}else {
			crt=crt+".";
		}
		
		return crt;
	}


	public void run1() {
		String file = "src/main/resources/advent2022/day10b.txt";
		List<String> res = Outil.importationString(file);
		int cycle=0;
		int registerX=1;
		int signalStrength = 0;
		for (int i=0;i<res.size();i++){
			String instruction =StringUtils.split( res.get(i) )[0] ;
			switch (instruction) {
			case "noop":
				cycle=cycle+1;
				signalStrength = calculeStrength(cycle, registerX, signalStrength);
				break;
			case "addx":
				cycle=cycle+1;
				signalStrength = calculeStrength(cycle, registerX, signalStrength);
				cycle=cycle+1;
				signalStrength = calculeStrength(cycle, registerX, signalStrength);
				int valeur =Integer.valueOf( StringUtils.split( res.get(i) )[1]) ;
				registerX=registerX+valeur;
				
				break;
			default:
				break;
			}

		}
		System.err.println(signalStrength);
	}

	public int calculeStrength(int cycle, int registerX, int signalStrength) {
		if (cycle==20) {
			signalStrength=signalStrength+cycle*registerX;
		}
		if (cycle==60) {
			signalStrength=signalStrength+cycle*registerX;
		}
		if (cycle==100) {
			signalStrength=signalStrength+cycle*registerX;
		}
		if (cycle==140) {
			signalStrength=signalStrength+cycle*registerX;
		}
		if (cycle==180) {
			signalStrength=signalStrength+cycle*registerX;
		}
		if (cycle==220) {
			signalStrength=signalStrength+cycle*registerX;
		}
		return signalStrength;
	}


}