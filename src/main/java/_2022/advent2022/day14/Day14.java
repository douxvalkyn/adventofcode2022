package advent2022.day14;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day14 {

	private static Logger logger = LoggerFactory.getLogger(Day14.class);
	private static boolean go=true;

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day14 day = new Day14();
		LocalDateTime start = LocalDateTime.now();
		//day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	
	
	
	public void run2() throws FileNotFoundException {
		String file = "src/main/resources/advent2022/day14.txt";
		List<String> res = Outil.importationString(file);
		int ymax=determinerYMax(res);
		Grille grille = new Grille(1000,ymax+3);
		creationGrille(grille,res);
		ajoutSol(grille);
		
		int x = 500;
		int y = 0;
		grille.getCase(x, y).setEtat("+");
		//grille.affichageEtat();
		
		int index=1;
		while (go) {
			
			coule(grille, x, y, index);
			index++;
		}
		grille.affichageEtat();
		System.out.println(index-1);
		
	}
	
	
	
	private void ajoutSol(Grille grille) {
		int yMax=grille.getNbLignes();
		for (int x=0;x<1000;x++) {
			grille.getCase(x, yMax-1).setEtat("#");
		}
		
	}




	public void run1() throws FileNotFoundException {
		String file = "src/main/resources/advent2022/day14.txt";
		List<String> res = Outil.importationString(file);
		int ymax=determinerYMax(res);
		Grille grille = new Grille(600,ymax+1);
		
		creationGrille(grille,res);
		//grille.affichageEtat();
		int x = 500;
		int y = 0;
		grille.getCase(x, y).setEtat("+");
		
		int index=1;
		while (go) {
		//for (int index = 1; index <= 100; index++) { //temps qui passe
			
			coule(grille, x, y, index);
			index++;
		}
		grille.affichageEtat();
		
	}

	
	
	private int determinerYMax(List<String> res) {
		int ymax=0;
		for (String ligne:res) {
			String[] points = StringUtils.split(ligne, "->");
			int nbPoints = points.length;
			for (int i=0;i<nbPoints-1;i++) {
				String point = points[i];
				int y1 = Integer.valueOf(StringUtils.trim( StringUtils.split(point,',')[1] ));
				if (y1>ymax){
					ymax=y1;
				}
			}
		}
		return ymax;
		
	}

	private void coule(Grille grille, int x, int y, int index) {
		// l'eau couleur vers le bas si elle n'est pas bloquée
		int ymax = grille.getNbLignes();

		if (y + 1 == ymax) {
			System.out.println("limite max atteinte: "+ (index-1));
			go=false;
			return;
		} // on atteint la limite

		// si la case au sud est libre, le sable y coule
		if (!grille.getCase(x, y+1).getEtat().equals("#") && !grille.getCase(x,y+1).getEtat().equals("o")) {
				coule(grille,x,y+1, index);
			return;
		}
			

		// si la case au sud n'est pas libre  , le sable coule en diagonale gauche si celle ci est libre
		if ( (grille.getCase(x, y+1).getEtat().equals("#") | grille.getCase(x, y+1).getEtat().equals("o")) && (grille.getCase(x-1, y+1).getEtat().equals("_"))  ) {
			
				coule(grille,x-1,y+1, index);
			
			return;
		}
		
		// si la case au sud n'est pas libre  , la case en diagonale bas gauche non plus,  le sable coule en diagonale droite si celle ci est libre
		if ( (grille.getCase(x, y+1).getEtat().equals("#") | grille.getCase(x, y+1).getEtat().equals("o")) && (grille.getCase(x-1, y+1).getEtat().equals("#") | grille.getCase(x-1, y+1).getEtat().equals("o")) && (grille.getCase(x+1, y+1).getEtat().equals("_") )) {
			
				coule(grille,x+1,y+1, index);
			
			return;
		}	

		//si les cases sud, down-left, down-right sont occupées, le sable s'arrête
		if ( 	   (grille.getCase(x, y+1).getEtat().equals("#") | grille.getCase(x, y+1).getEtat().equals("o"))
				&& (grille.getCase(x-1, y+1).getEtat().equals("#") | grille.getCase(x-1, y+1).getEtat().equals("o"))
				&& (grille.getCase(x+1, y+1).getEtat().equals("#") | grille.getCase(x+1, y+1).getEtat().equals("o")) ) {
			grille.getCase(x, y).setEtat("o");
			//System.out.println(y);
			if (index%5000==0) {
				System.out.println("");
				System.out.println("");
				System.out.println("");
			grille.affichageEtat();
			}
			if (x==500 && y==0) {
				System.out.println("fin run2");
				go=false;
				return;
			}
			return;
		}


	}
	
	private Grille creationGrille(Grille grille, List<String> res) {
		for (String ligne:res) {
			String[] points = StringUtils.split(ligne, "->");
			int nbPoints = points.length;
			for (int i=0;i<nbPoints-1;i++) {
				String pointDebut = points[i];
				int x1 = Integer.valueOf(StringUtils.trim( StringUtils.split(pointDebut,',')[0] ));
				int y1 = Integer.valueOf(StringUtils.trim( StringUtils.split(pointDebut,',')[1] ));
				String pointFin =  points[i+1];
				int x2 = Integer.valueOf(StringUtils.trim( StringUtils.split(pointFin,',')[0] ));
				int y2 = Integer.valueOf(StringUtils.trim( StringUtils.split(pointFin,',')[1] ));
				
				//déterminer si la ligne est horzontale ou verticale
				if (x1==x2) { //ligne verticale
					if (y1<y2) {
					for (int j=y1;j<=y2;j++) {
						Case cell = grille.getCase(x1, j);
						cell.setEtat("#");
					}
					}
					if (y1>=y2) {
						for (int j=y2;j<=y1;j++) {
							Case cell = grille.getCase(x1, j);
							cell.setEtat("#");
						}
					}
				}
				if (y1==y2) { //ligne horizontale
					if (x1<x2) {
						for (int j=x1;j<=x2;j++) {
							Case cell = grille.getCase(j, y1);
							cell.setEtat("#");
						}
					}
					if (x1>=x2) {
						for (int j=x2;j<=x1;j++) {
							Case cell = grille.getCase(j, y1);
							cell.setEtat("#");
						}
					}
				}
				
			}
		}
		return grille;
	}


}