package advent2022.day02;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2016.day25.Day25;
import communs.Outil;

public class Day02 {

	private static Logger logger = LoggerFactory.getLogger(Day02.class);

	public static void main(String[] args) throws IOException {
		logger.info("[" + Day25.class.getSimpleName() + "]");
		Day02 day = new Day02();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		logger.info("[Time: " + duree.getSeconds() + " s]");
	}

	public void run1() {
		String file = "src/main/resources/advent2022/day02.txt";
		List<String> res = Outil.importationString(file);
		//System.out.println(res);
		int score=0;
		for (String round: res) {
			String ennemy = StringUtils.split(round, ' ')[0];
			String joueur = StringUtils.split(round, ' ')[1];
			if (joueur.equals("X")) {
				score=score+1;
			}
			if (joueur.equals("Y")) {
				score=score+2;
			}
			if (joueur.equals("Z")) {
				score=score+3;
			}
			
			//outcome
			if( (ennemy.equals("A") && joueur.equals("X")) || (ennemy.equals("B") && joueur.equals("Y")) || (ennemy.equals("C") && joueur.equals("Z")) ){ //draw
				score=score+3;
			}
			if( (ennemy.equals("A") && joueur.equals("Z")) || (ennemy.equals("B") && joueur.equals("X")) || (ennemy.equals("C") && joueur.equals("Y")) ){ //lost
				score=score+0;
			}
			if( (ennemy.equals("A") && joueur.equals("Y")) || (ennemy.equals("B") && joueur.equals("Z")) || (ennemy.equals("C") && joueur.equals("X")) ){ //win
				score=score+6;
			}
			
		}
		System.out.println("run1: "+score);
	}
	
	
	public void run2() {
		String file = "src/main/resources/advent2022/day02.txt";
		List<String> res = Outil.importationString(file);
		//System.out.println(res);
		int score=0;
		for (String round: res) {
			String ennemy = StringUtils.split(round, ' ')[0];
			String result = StringUtils.split(round, ' ')[1];
			String joueur="";
			if (result.equals("X")) { //lose
				if (ennemy.equals("A")) {
					joueur="Z";
				}
				if (ennemy.equals("B")) {
					joueur="X";
				}
				if (ennemy.equals("C")) {
					joueur="Y";
				}
			}
			if (result.equals("Y")) { //draw
				if (ennemy.equals("A")) {
					joueur="X";
				}
				if (ennemy.equals("B")) {
					joueur="Y";
				}
				if (ennemy.equals("C")) {
					joueur="Z";
				}
			}
			if (result.equals("Z")) { //win
				if (ennemy.equals("A")) {
					joueur="Y";
				}
				if (ennemy.equals("B")) {
					joueur="Z";
				}
				if (ennemy.equals("C")) {
					joueur="X";
				}
			}
			
			if (joueur.equals("X")) {
				score=score+1;
			}
			if (joueur.equals("Y")) {
				score=score+2;
			}
			if (joueur.equals("Z")) {
				score=score+3;
			}
		
			
			//outcome
			if( (ennemy.equals("A") && joueur.equals("X")) || (ennemy.equals("B") && joueur.equals("Y")) || (ennemy.equals("C") && joueur.equals("Z")) ){ //draw
				score=score+3;
			}
			if( (ennemy.equals("A") && joueur.equals("Z")) || (ennemy.equals("B") && joueur.equals("X")) || (ennemy.equals("C") && joueur.equals("Y")) ){ //lost
				score=score+0;
			}
			if( (ennemy.equals("A") && joueur.equals("Y")) || (ennemy.equals("B") && joueur.equals("Z")) || (ennemy.equals("C") && joueur.equals("X")) ){ //win
				score=score+6;
			}
			
		}
		System.out.println("run2: "+score);
		
	}

	
	
	
}
