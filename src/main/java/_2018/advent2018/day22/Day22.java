package advent2018.day22;

import java.util.ArrayList;
import java.util.List;

public class Day22 {

	// main ----
	public static void main(String[] args) {
		double startTime = System.currentTimeMillis();
		Day22 d = new Day22();
		d.run2();
		// d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	public void run1() {
		int depth = 4080;
		int xMouth = 0, yMouth = 0, xTarget = 14, yTarget = 785;
		char[][] grille = new char[(yTarget - yMouth + 1)][(xTarget - xMouth + 1)];
		int[][] erosionLevel = new int[(yTarget - yMouth + 1)][(xTarget - xMouth + 1)];
		int[][] geologicIndex = new int[(yTarget - yMouth + 1)][(xTarget - xMouth + 1)];

		// Mouth et Target
		grille[yMouth][xMouth] = 'M';
		geologicIndex[yMouth][xMouth] = 0;
		erosionLevel[yMouth][xMouth] = (geologicIndex[yMouth][xMouth] + depth) % 20183;
		grille[yTarget][xTarget] = 'T';
		geologicIndex[yTarget][xTarget] = 0;
		erosionLevel[yTarget][xTarget] = (geologicIndex[yTarget][xTarget] + depth) % 20183;

		// Cases du bord
		for (int i = 1; i <= yTarget; i++) {
			geologicIndex[i][0] = 48271 * i;
			erosionLevel[i][0] = (geologicIndex[i][0] + depth) % 20183;
			int type = erosionLevel[i][0] % 3;
			if (type == 0) {
				grille[i][0] = '.';
			}
			if (type == 1) {
				grille[i][0] = '=';
			}
			if (type == 2) {
				grille[i][0] = '|';
			}
		}
		for (int j = 1; j <= xTarget; j++) {
			geologicIndex[0][j] = 16807 * j;
			erosionLevel[0][j] = (geologicIndex[0][j] + depth) % 20183;
			int type = erosionLevel[0][j] % 3;
			if (type == 0) {
				grille[0][j] = '.';
			}
			if (type == 1) {
				grille[0][j] = '=';
			}
			if (type == 2) {
				grille[0][j] = '|';
			}
		}

		// Autres cases
		for (int i = 1; i <= yTarget; i++) {
			for (int j = 1; j <= xTarget; j++) {
				if (i != yTarget | j != xTarget) {
					geologicIndex[i][j] = erosionLevel[i][j - 1] * erosionLevel[i - 1][j];
					erosionLevel[i][j] = (geologicIndex[i][j] + depth) % 20183;
					int type = erosionLevel[i][j] % 3;
					if (type == 0) {
						grille[i][j] = '.';
					}
					if (type == 1) {
						grille[i][j] = '=';
					}
					if (type == 2) {
						grille[i][j] = '|';
					}
				}
			}
		}
		visualisation(grille, (yTarget - yMouth + 1), (xTarget - xMouth + 1));
		calculRisque(grille, (yTarget - yMouth + 1), (xTarget - xMouth + 1));

	}

	public void run2() {
		// int depth = 4080;
		// int xMouth=0, yMouth=0, xTarget = 14, yTarget = 785;
		int depth = 4080;
		int xMouth = 0, yMouth = 0, xTarget = 14, yTarget = 14, xMax = 3 * xTarget, yMax = yTarget + 50;
		char[][] grille = new char[(yMax - yMouth + 1)][(xMax - xMouth + 1)];
		int[][] erosionLevel = new int[(yMax - yMouth + 1)][(xMax - xMouth + 1)];
		int[][] geologicIndex = new int[(yMax - yMouth + 1)][(xMax - xMouth + 1)];

		// Mouth et Target
		grille[yMouth][xMouth] = 'M';
		geologicIndex[yMouth][xMouth] = 0;
		erosionLevel[yMouth][xMouth] = (geologicIndex[yMouth][xMouth] + depth) % 20183;
		grille[yTarget][xTarget] = 'T';
		geologicIndex[yTarget][xTarget] = 0;
		erosionLevel[yTarget][xTarget] = (geologicIndex[yTarget][xTarget] + depth) % 20183;

		// Cases du bord
		for (int i = 1; i <= yMax; i++) {
			geologicIndex[i][0] = 48271 * i;
			erosionLevel[i][0] = (geologicIndex[i][0] + depth) % 20183;
			int type = erosionLevel[i][0] % 3;
			if (type == 0) {
				grille[i][0] = '.';
			}
			if (type == 1) {
				grille[i][0] = '=';
			}
			if (type == 2) {
				grille[i][0] = '|';
			}
		}
		for (int j = 1; j <= xMax; j++) {
			geologicIndex[0][j] = 16807 * j;
			erosionLevel[0][j] = (geologicIndex[0][j] + depth) % 20183;
			int type = erosionLevel[0][j] % 3;
			if (type == 0) {
				grille[0][j] = '.';
			}
			if (type == 1) {
				grille[0][j] = '=';
			}
			if (type == 2) {
				grille[0][j] = '|';
			}
		}

		// Autres cases
		for (int i = 1; i <= yMax; i++) {
			for (int j = 1; j <= xMax; j++) {
				if (i != yTarget | j != xTarget) {
					geologicIndex[i][j] = erosionLevel[i][j - 1] * erosionLevel[i - 1][j];
					erosionLevel[i][j] = (geologicIndex[i][j] + depth) % 20183;
					int type = erosionLevel[i][j] % 3;
					if (type == 0) {
						grille[i][j] = '.';
					}
					if (type == 1) {
						grille[i][j] = '=';
					}
					if (type == 2) {
						grille[i][j] = '|';
					}
				}
			}
		}
		visualisation(grille, (yMax - yMouth + 1), (xMax - xMouth + 1));
		List<NodeWeighted> noeuds = new ArrayList<NodeWeighted>();
		GraphWeighted graphWeighted = creationGraphe(grille, (yMax - yMouth + 1), (xMax - xMouth + 1), noeuds, xTarget, yTarget);
		NodeWeighted[] source = findNodeWithName(noeuds, 0, 0, '.', xTarget, yTarget);
		NodeWeighted[] destination = findNodeWithName(noeuds, yTarget, xTarget, '.', xTarget, yTarget);
		String[] res = graphWeighted.DijkstraShortestPath(source[0], destination[0]);
		String test = res[0];
		System.out.println("Chemin: " + res[0]);
		System.out.println("Temps total: " + res[1]);
		String[] splitted = res[0].split(" ");

		int cpt = 0;
		for (int i = 0; i < splitted.length - 1; i++) {
			char lettre_i = splitted[i].charAt(splitted[i].length() - 1);
			char lettre_ip = splitted[i + 1].charAt(splitted[i + 1].length() - 1);
			if (lettre_i != lettre_ip) {
				cpt++;
			}

		}
		System.out.println(7 * cpt + splitted.length - 1);

	}

	private GraphWeighted creationGraphe(char[][] grille, int imax, int jmax, List<NodeWeighted> noeuds, int xTarget, int yTarget) {
		GraphWeighted graphWeighted = new GraphWeighted(false);

		// creation des nodes
		int numCase = 0;
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				String objet1 = null, objet2 = null;
				if (grille[i][j] == '.') {
					objet1 = "T";
					objet2 = "CG";
				}
				if (grille[i][j] == '|') {
					objet1 = "T";
					objet2 = "X";
				}
				if (grille[i][j] == '=') {
					objet1 = "CG";
					objet2 = "X";
				}
				if (i == yTarget & j == xTarget) {
					grille[i][j] = '.';
					objet1 = "T";
					objet2 = "T";
				}
				if (grille[i][j] == 'M') {
					grille[i][j] = '.';
					objet1 = "T";
					objet2 = "T";
				}
				// Gestion de M et T: on commence par une torche et on finit par une torche
				NodeWeighted noeudA = new NodeWeighted(numCase, "(" + String.valueOf(j) + ";" + String.valueOf(i) + ")" + objet1, j, i, objet1);
				NodeWeighted noeudB = new NodeWeighted(numCase, "(" + String.valueOf(j) + ";" + String.valueOf(i) + ")" + objet2, j, i, objet2);
				numCase++;
				noeuds.add(noeudA);
				noeuds.add(noeudB);

			}
		}
		char[] casesAdj = new char[2]; // casesAdj[0]= est , casesAdj[1]=sud, inutile de faire les autres directions
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				char type = grille[i][j];
				if ((j + 1) < jmax) {
					casesAdj[0] = grille[i][j + 1];
				}
				if ((i + 1) < imax) {
					casesAdj[1] = grille[i + 1][j];
				}

				// point de d�part
				if (i == 0 & j == 0) {
					for (int k = 0; k < 2; k++) {
						int i_ = i, j_ = j; // i_ et j_ sont les modifieurs de i et j pour les 2 cases adjacentes est et sud (k=0 EST, k=1 SUD)
						if (k == 0) {
							j_ = j + 1;
						}
						if (k == 1) {
							i_ = i + 1;
						}
						// le point de d�part est forc�ment Rocky .
						if (type == '.' & casesAdj[k] == '.') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
							graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
							graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
							graphWeighted.addEdge(sources[1], destinations[1], 8); // T CG
						}
						if (type == '.' & casesAdj[k] == '=') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
							graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
							graphWeighted.addEdge(sources[1], destinations[0], 8); // T CG
							graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
						}
						if (type == '.' & casesAdj[k] == '|') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
							graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
							graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
							graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
						}
					}
				} // fin pt depart

				// Cases normales (attention si la case adjacente est la destination !)
				if ((i != 0 | j != 0)) {
					for (int k = 0; k < 2; k++) {
						int i_ = i, j_ = j; // i_ et j_ sont les modifieurs de i et j pour les 2 cases adjacentes est et sud (k=0 EST, k=1 SUD)
						if (k == 0) {
							j_ = j + 1;
						}
						if (k == 1) {
							i_ = i + 1;
						}
						// Rocky .
						if (type == '.' & casesAdj[k] == '.') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // CG T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T CG
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[1], 1); // CG CG ok
							}
						}
						if (type == '.' & casesAdj[k] == '=') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // CG T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 1); // CG CG
								graphWeighted.addEdge(sources[1], destinations[1], 8); // CG X ok
							}
						}
						if (type == '.' & casesAdj[k] == '|') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // CG T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // CG X ok
							}
						}
						// Wet =
						if (type == '=' & casesAdj[k] == '.') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T CG
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // CG CG
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X CG ok
							}
						}
						if (type == '=' & casesAdj[k] == '=') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // CG CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // CG X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X CG
								graphWeighted.addEdge(sources[1], destinations[1], 1); // X X ok
							}
						}
						if (type == '=' & casesAdj[k] == '|') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // CG T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // CG T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // CG X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 1); // X X ok
							}
						}
						// Narrow |
						if (type == '|' & casesAdj[k] == '.') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T CG
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X CG ok
							}
						}
						if (type == '|' & casesAdj[k] == '=') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 8); // T CG
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X CG
								graphWeighted.addEdge(sources[1], destinations[1], 1); // X X ok
							}
						}
						if (type == '|' & casesAdj[k] == '|') {
							NodeWeighted[] sources = findNodeWithName(noeuds, i, j, type, xTarget, yTarget);
							NodeWeighted[] destinations = findNodeWithName(noeuds, i_, j_, casesAdj[k], xTarget, yTarget);
							if (i_ == yTarget & j_ == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // X T
							} else if (i == yTarget & j == xTarget) {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[1], destinations[1], 8); // T X
							} else {
								graphWeighted.addEdge(sources[0], destinations[0], 1); // T T
								graphWeighted.addEdge(sources[0], destinations[1], 8); // T X
								graphWeighted.addEdge(sources[1], destinations[0], 8); // X T
								graphWeighted.addEdge(sources[1], destinations[1], 1); // X X
							}
						}

					}
				}
				// reinitialiser les cases adjacentes
				casesAdj[0] = ' ';
				casesAdj[1] = ' ';

			}
		}

		return graphWeighted;
	}

	public NodeWeighted[] findNodeWithName(List<NodeWeighted> noeuds, int i, int j, char type, int xTarget, int yTarget) {
		NodeWeighted[] res = new NodeWeighted[2];
		String objet1 = null, objet2 = null;
		if (type == '.') {
			objet1 = "T";
			objet2 = "CG";
		}
		if (type == '|') {
			objet1 = "T";
			objet2 = "X";
		}
		if (type == '=') {
			objet1 = "CG";
			objet2 = "X";
		}
		if (i == yTarget & j == xTarget) {
			objet1 = "T";
			objet2 = "T";
		}
		if (i == 0 & j == 0) {
			objet1 = "T";
			objet2 = "T";
		}
		for (NodeWeighted noeud : noeuds) {
			if (j == noeud.x & i == noeud.y & objet1.equals(noeud.objet)) {
				res[0] = noeud;
			}
			if (j == noeud.x & i == noeud.y & objet2.equals(noeud.objet)) {
				res[1] = noeud;
			}
		}
		return res;
	}

	private void calculRisque(char[][] grille, int imax, int jmax) {
		int risqueTotal = 0;
		int risk = 0;
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				char type = grille[i][j];
				if (type == '.') {
					risk = 0;
				}
				if (type == '=') {
					risk = 1;
				}
				if (type == '|') {
					risk = 2;
				}
				risqueTotal = risqueTotal + risk;
				risk = 0;
			}
		}
		System.out.println(risqueTotal);

	}

	private void visualisation(char[][] grille, int imax, int jmax) {
		String str = "";
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				System.out.print(grille[i][j]);
				str = str + grille[i][j];
			}
			System.out.println();
			str = str + "\n";
		}
	}

}
