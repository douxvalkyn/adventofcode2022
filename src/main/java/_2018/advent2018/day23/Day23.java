package advent2018.day23;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Day23 {

	// main ------
	public static void main(String[] args) throws ParseException, IOException {
		Day23 d = new Day23();
		double startTime = System.currentTimeMillis();
		d.run1();
		d.run2(); // recherche par zooms successifs en divisant l'input par 1M, 100000, 10000, etc ...
		// d.run4(file, 10458292, 51533488, 45572260); //recherche méthodique autour d'un point
		// d.run3(file); //recherche par zooms successifs
		// d.run2(file);
		// d.run1(file);
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	
	public  void run1() throws ParseException, IOException {
		Day23 d = new Day23();
		String file = "day23";
		d.run1_(file);
	}
	public  void run2() throws ParseException, IOException {
		Day23 d = new Day23();
		String file = "day23";
		d.run5_(file);
	}
	
	
	
	private void run5_(String file) throws FileNotFoundException {

		int tour = 1;
		int cptMax = 0, iInit = 0, jInit = 0, kInit = 0, iMax = 0, jMax = 0, kMax = 0;
		while (tour < 8) {
			List<Nanobot> nanobots = importation(file);
			diviser(nanobots, (int) (10000000 / Math.pow(10, tour)));

			cptMax = 0;
			for (int i = -100 + (iInit); i <= 100 + (iInit); i++) {
				for (int j = -100 + (jInit); j <= 100 + (jInit); j++) {
					for (int k = -100 + (kInit); k <= 100 + (kInit); k++) {
						int res = countInRange2(nanobots, i, j, k);
						if (res > cptMax) {
							cptMax = res;
							iMax = i;
							jMax = j;
							kMax = k;

						}
					}
				}
			}
			System.out.println("tour: " + tour);
			// System.out.println(10000000/Math.pow(10, tour));
			System.out.println(cptMax + " " + iMax + " " + jMax + " " + kMax);
			iInit = iMax * 10;
			jInit = jMax * 10;
			kInit = kMax * 10;
			int dist = Math.abs(iMax) + Math.abs(jMax) + Math.abs(kMax);
			tour++;
		}
	}

	private void diviser(List<Nanobot> nanobots, int diviseur) {
		for (Nanobot nanobot : nanobots) {
			nanobot.setPosX(nanobot.getPosX() / diviseur);
			nanobot.setPosY(nanobot.getPosY() / diviseur);
			nanobot.setPosZ(nanobot.getPosZ() / diviseur);
			nanobot.setRadius(nanobot.getRadius() / diviseur);
		}
	}

	private void run3(String file) throws FileNotFoundException {
		List<Nanobot> nanobots = importation(file);

		// recherche aléatoire
		Random rand = new Random();
		int bestX = 0, bestY = 0, bestZ = 0;
		int min = -500000000, max = 500000000;

		for (int tours = 1; tours < 12; tours++) {
			System.out.println(">Tour: " + tours);

			min = min / 5;
			max = max / 5;
			int cptMax = 0;
			List<Integer> listeX = new ArrayList<Integer>();
			List<Integer> listeY = new ArrayList<Integer>();
			List<Integer> listeZ = new ArrayList<Integer>();
			List<Integer> listeX2 = new ArrayList<Integer>();
			List<Integer> listeY2 = new ArrayList<Integer>();
			List<Integer> listeZ2 = new ArrayList<Integer>();
			List<Integer> listeCptMax = new ArrayList<Integer>();

			for (int i = 0; i < 1000000000; i++) {
				int x = rand.nextInt((bestX + max) - (bestX + min) + 1) + (bestX + min);
				int y = rand.nextInt((bestY + max) - (bestY + min) + 1) + (bestY + min);
				int z = rand.nextInt((bestZ + max) - (bestZ + min) + 1) + (bestZ + min);
				int res = 0;
				if (x + y + z > 87525872 & x + y + z < 107563432) { // je r�duis la recherche en fonction des essais pr�c�dents pour gagner un peu de temps
					res = countInRange2(nanobots, x, y, z);
				}
				if (res >= cptMax) {
					cptMax = res;
					listeX.add(x);
					listeY.add(y);
					listeZ.add(z);
					listeCptMax.add(cptMax);
					// System.out.println(res);
				}
			}
			for (int l = 0; l < listeCptMax.size(); l++) {
				if (listeCptMax.get(l) == cptMax) {
					listeX2.add(listeX.get(l));
					listeY2.add(listeY.get(l));
					listeZ2.add(listeZ.get(l));
				}
			}
			System.out.println("max: " + cptMax);
			bestX = (int) Math.floor(calculateAverage(listeX2));
			bestY = (int) Math.floor(calculateAverage(listeY2));
			bestZ = (int) Math.floor(calculateAverage(listeZ2));
			System.out.println("bestX: " + bestX + ", bestY: " + bestY + ", bestZ: " + bestZ);
			System.out.println("--------");
		}

	}

	private double calculateAverage(List<Integer> marks) {
		return marks.stream().mapToDouble(d -> d).average().orElse(0.0);
	}

	private void run4(String file, int init_x, int init_y, int init_z) throws FileNotFoundException {
		List<Nanobot> nanobots = importation(file);

		int cptMax = 0;
		int max_i = 0;
		int max_j = 0;
		int max_k = 0;
		List<Integer> liste_i = new ArrayList<Integer>();
		List<Integer> liste_j = new ArrayList<Integer>();
		List<Integer> liste_k = new ArrayList<Integer>();
		List<Integer> liste_i2 = new ArrayList<Integer>();
		List<Integer> liste_j2 = new ArrayList<Integer>();
		List<Integer> liste_k2 = new ArrayList<Integer>();
		List<Integer> liste_cptMax = new ArrayList<Integer>();
		int n = 100;
		for (int i = init_x - n; i < init_x + n; i++) {
			for (int j = init_y - n; j < init_y + n; j++) {
				for (int k = init_z - n; k < init_z + n; k++) {
					int res = countInRange2(nanobots, i, j, k);
					if (res >= cptMax) {
						cptMax = res;
						// System.out.println(cptMax);
						// System.out.println("i: "+i+", j: "+j+", k: "+k);
						liste_i.add(i);
						liste_j.add(j);
						liste_k.add(k);
						liste_cptMax.add(cptMax);
					}
				}
			}
		}

		for (int l = 0; l < liste_cptMax.size(); l++) {
			if (liste_cptMax.get(l) == cptMax) {
				liste_i2.add(liste_i.get(l));
				liste_j2.add(liste_j.get(l));
				liste_k2.add(liste_k.get(l));
			}
		}
		System.out.println("max: " + cptMax);
		int score = Integer.MAX_VALUE;
		for (int l = 0; l < liste_i2.size(); l++) {
			if (liste_i2.get(l) + liste_j2.get(l) + liste_k2.get(l) < score) {
				score = liste_i2.get(l) + liste_j2.get(l) + liste_k2.get(l);
				System.out.println(score);
			}
		}

	}

	private void run1_(String file) throws FileNotFoundException {
		List<Nanobot> nanobots = importation(file);
		System.out.println(nanobots);
		Nanobot strongest = findStrongest(nanobots);
		System.out.println(strongest);
		int cpt = countInRange(nanobots, strongest);
		System.out.println(cpt);

	}

	private void run2(String file) throws FileNotFoundException {
		List<Nanobot> nanobots = importation(file);

		// D�terminer pour chaque boule le nb de boule qui l'intersecte
		for (Nanobot nano1 : nanobots) {
			int r1 = nano1.getRadius();
			for (Nanobot nano2 : nanobots) {
				int r2 = nano2.getRadius();
				int dist = Math.abs(nano1.getPosX() - nano2.getPosX()) + Math.abs(nano1.getPosY() - nano2.getPosY()) + Math.abs(nano1.getPosZ() - nano2.getPosZ());
				int cpt = 0;
				if (r1 + r2 > dist & dist != 0 & r1 < dist) {
					nano1.setInter(nano1.getInter() + 1);
				}
			}
		}
		System.out.println(nanobots);

		// conserver seulement les boules qui ont "beaucoup" d'intersections
		List<Nanobot> nanobots2 = new ArrayList<Nanobot>();
		for (Nanobot nano1 : nanobots) {
			if (nano1.getInter() >= 2) {
				nanobots2.add(nano1);
			}
		}
		System.out.println(nanobots2);

		// recherche du barycentre des centres des boules conserv�es, pond�r� par l'inverse du radius
		double x_tot = 0.0, y_tot = 0.0, z_tot = 0.0;
		double rad_tot = 0.0;
		for (Nanobot nano1 : nanobots2) {
			x_tot = x_tot + nano1.getPosX() * (1.0 / nano1.getRadius());
			y_tot = y_tot + nano1.getPosY() * 1.0 / nano1.getRadius();
			z_tot = z_tot + nano1.getPosZ() * 1 / nano1.getRadius();
			rad_tot = rad_tot + 1.0 / nano1.getRadius();
		}
		double x_moy = x_tot / rad_tot;
		double y_moy = y_tot / rad_tot;
		double z_moy = z_tot / rad_tot;
		System.out.println("x init " + x_moy);
		System.out.println("y init " + y_moy);
		System.out.println("z init " + z_moy);

		// recherche autour du centre de gravit� trouv�
		int cptMax = 0;
		int max = 2;
		int start_i = (int) Math.floor(x_moy), start_j = (int) Math.floor(y_moy), start_k = (int) Math.floor(z_moy);

		// recherche du max de cercles qui entourent un point
		int max_i = 0;
		int max_j = 0;
		int max_k = 0;
		for (int i = start_i - max; i < start_i + max; i++) {
			for (int j = start_j - max; j < start_j + max; j++) {
				for (int k = start_k - max; k < start_k + max; k++) {
					int res = countInRange2(nanobots, i, j, k);
					if (res >= cptMax) {
						cptMax = res;
						// TODO !!!
						System.out.println("1: " + (i + j + k));
						System.out.println("2: " + (max_i + max_j + max_k));
						if ((i + j + k) < (max_i + max_j + max_k)) {
							System.out.println("3");
							max_i = i;
							max_j = j;
							max_k = k;
						}
					}
				}
			}
		}
		System.out.println("max: " + cptMax);

		System.out.println("x: " + max_i);
		System.out.println("y: " + max_j);
		System.out.println("z: " + max_k);

	}

	private int countInRange2(List<Nanobot> nanobots, int i, int j, int k) {
		int cpt = 0;
		for (Nanobot nano : nanobots) {
			int dist = Math.abs(nano.getPosX() - i) + Math.abs(nano.getPosY() - j) + Math.abs(nano.getPosZ() - k);
			if (dist <= nano.getRadius()) {
				cpt++;
			}
		}
		return cpt;
	}

	private int countInRange(List<Nanobot> nanobots, Nanobot strongest) {
		int cpt = 0;
		for (Nanobot nano : nanobots) {
			int distance = Math.abs((strongest.getPosX() - nano.getPosX())) + Math.abs((strongest.getPosY() - nano.getPosY())) + Math.abs((strongest.getPosZ() - nano.getPosZ()));
			if (distance <= strongest.getRadius()) {
				cpt++;
			}
		}
		return cpt;
	}

	private Nanobot findStrongest(List<Nanobot> nanobots) {
		int radiusMax = 0;
		Nanobot strongest = null;
		for (Nanobot nano : nanobots) {
			int r = nano.getRadius();
			if (r > radiusMax) {
				radiusMax = r;
				strongest = nano;
			}
		}

		return strongest;
	}

	public List<Nanobot> importation(String file) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day23/day23.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// System.out.println(input);
		List<Nanobot> nanobots = new ArrayList<Nanobot>();
		for (String str : input) {
			String position = str.substring(str.indexOf('<') + 1, str.indexOf('>'));
			int radius = Integer.parseInt(str.substring(str.indexOf('r') + 2));
			String[] positions_splite = position.split(",");
			Nanobot nanobot = new Nanobot(Integer.parseInt(positions_splite[0]), Integer.parseInt(positions_splite[1]), Integer.parseInt(positions_splite[2]), radius);
			nanobots.add(nanobot);

		}
		return nanobots;
	}
}