package advent2018.day23;

public class Nanobot {

	private int posX;
	private int posY;
	private int posZ;
	private int radius;
	private int inter;
	
	
	public int getInter() {
		return inter;
	}


	public void setInter(int inter) {
		this.inter = inter;
	}


	@Override
	public String toString() {
		return "Nanobot [posX=" + posX + ", posY=" + posY + ", posZ=" + posZ + ", radius=" + radius + " inter="+ inter +  "]";
	}


	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
	}


	public int getPosY() {
		return posY;
	}


	public void setPosY(int posY) {
		this.posY = posY;
	}


	public int getPosZ() {
		return posZ;
	}


	public void setPosZ(int posZ) {
		this.posZ = posZ;
	}


	public int getRadius() {
		return radius;
	}


	public void setRadius(int radius) {
		this.radius = radius;
	}


	public Nanobot(int posX, int posY, int posZ, int radius) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.radius = radius;
	}
}
