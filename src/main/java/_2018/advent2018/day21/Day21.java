package advent2018.day21;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Day21 {

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day21 d = new Day21();

		d.run1();
		d.run2();

	}

	
	public  void run1() throws ParseException, IOException {
		Day21 d = new Day21();
		String file = "day21";
		d.run1_(file);
	}
	public  void run2() throws ParseException, IOException {
		Day21 d = new Day21();
		String file = "day21";
		d.run2_(file);
	}
	
	
	private void run2_(String file) {
		// on d�sassemble l'algo en langage elfe pour le traduire en langage java:
		// je mets une boucle for autour pour le faire tourner plein de fois afin de r�cup�rer toutes les valeurs de R5 qui
		// font stopper l'algo
		// ensuite pour la partie2, je cherche parmi la liste de toutes ces solutions le premier qui se r�p�te:
		// celui juste avant est le nombre cherch� !
		// je garde l'algo en elfe dans le run1() car il m'a servi pour v�rifier l'exactitude de la correspondance en java

		int r1 = 0, r4 = 0, r3 = 65536, r5 = 9010242;
		List<Integer> resultats = new ArrayList<Integer>();
		int indice = 0;

		for (int index = 0; index < 10160; index++) {

			r5 = (((r5 + (r3 & 255)) & 16777215) * 65899) & 16777215;
			while (256 <= r3) {

				r1 = 0;
				r4 = r1 + 1;
				r4 = r4 * 256;

				r3 = r3 / 256;
				r5 = (((r5 + (r3 & 255)) & 16777215) * 65899) & 16777215;

			}
			// System.out.println(r5);

			if (resultats.contains(r5)) {
				System.out.println("solution: " + resultats.get(resultats.size() - 1));
			}
			resultats.add(r5);
			r3 = r5 | 65536;
			r5 = 9010242;

		}
		System.out.println("solution: " + resultats.get(indice));

	}

	// M�thodes ----
	public void run1_(String file) throws FileNotFoundException {

		for (int reg0 = 0; reg0 < 1; reg0++) {
			List<List<String>> instructions = importation(file);
			int bound = importationBound(file);

			int IP = 0;
			List<Integer> register = new ArrayList<>();
			register.add(0);
			register.add(0);
			register.add(0);
			register.add(0);
			register.add(0);
			register.add(0);

			register.set(0, 0);
			// pour run1(), j'ai d�roul� l'algo jusque environ la 1850e instruction
			// R5=6619857 � ce moment et en d�compilant l'algo, on observe qu'il y a un test, l'algo s'arr�te quand R0 est �gal � ce nombre
			// System.out.println("reg0: "+reg0);
			long comptage = 0;
			for (long i = 0; i < 10000000; i++) {
				// while (IP<instructions.size()) {
				comptage++;
				// System.out.println("comptage: "+ (comptage-1));
				register.set(bound, IP); // BEFORE: on charge la valeur de IP dans le registre(bound)
				List<String> instruction = instructions.get(IP);// INSTR: on applique l'instruction IP
				List<Integer> instructionInt = new ArrayList<>();
				instructionInt.add(Integer.parseInt(instruction.get(1)));
				instructionInt.add(Integer.parseInt(instruction.get(2)));
				instructionInt.add(Integer.parseInt(instruction.get(3)));
				// System.out.println("instr: "+instructionInt);
				List<Integer> res = new ArrayList<Integer>();
				switch (instruction.get(0)) {

					case "addr":
						res = addr(register, instructionInt);
						break;

					case "addi":
						res = addi(register, instructionInt);
						break;

					case "mulr":
						res = mulr(register, instructionInt);
						break;

					case "muli":
						res = muli(register, instructionInt);
						break;

					case "banr":
						res = banr(register, instructionInt);
						break;

					case "bani":
						res = bani(register, instructionInt);
						break;

					case "borr":
						res = borr(register, instructionInt);
						break;

					case "bori":
						res = bori(register, instructionInt);
						break;

					case "setr":
						res = setr(register, instructionInt);
						break;

					case "seti":
						res = seti(register, instructionInt);
						break;

					case "gtir":
						res = gtir(register, instructionInt);
						break;

					case "gtri":
						res = gtri(register, instructionInt);
						break;

					case "gtrr":
						res = gtrr(register, instructionInt);
						break;

					case "eqri":
						res = eqri(register, instructionInt);
						break;

					case "eqir":
						res = eqir(register, instructionInt);
						break;

					case "eqrr":
						res = eqrr(register, instructionInt);
						break;

					default:
						System.out.println("Choix incorrect");
						break;
				}

				register = res;
				IP = register.get(bound); // AFTER: on remet dans IP la valeur du registre(bound)
				// System.out.println("register"+register);
				IP++; // IP+1
				// System.out.println("IP: "+(IP));
				if (IP == 28) {
					System.out.println("register" + register);
					List<Integer> resultats = new ArrayList<Integer>();
					resultats.add(register.get(5));
					final Set<Integer> setToReturn = new HashSet<>();
					final Set<Integer> set1 = new HashSet<>();

					for (Integer yourInt : resultats) {
						if (!set1.add(yourInt)) {
							setToReturn.add(yourInt);
						}
					}
					if (!setToReturn.isEmpty()) {
						System.out.println(setToReturn);
					}
				}
				// if (comptage%1000000000==0) { System.out.println("top: "+comptage/1000000000 + "milliards"); System.out.println("register"+register);}
			}

		}
	}

	private List<Integer> addr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) + register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// add immediate
	private List<Integer> addi(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) + instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// mul register
	public List<Integer> mulr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) * register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// mul immediate
	public List<Integer> muli(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) * instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise and register
	public List<Integer> banr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) & register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise and immediate
	public List<Integer> bani(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) & instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise or register
	public List<Integer> borr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) | register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// biwise or immediate
	public List<Integer> bori(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) | instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// set register
	public List<Integer> setr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// set immediate
	public List<Integer> seti(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = instruction.get(0);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than immediate/register
	public List<Integer> gtir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (instruction.get(0) > register.get(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than register/immediate
	public List<Integer> gtri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)) > instruction.get(1)) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than register/register
	public List<Integer> gtrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)) > register.get(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal immediate/register
	public List<Integer> eqir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (instruction.get(0).equals(register.get(instruction.get(1)))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal register/immediate
	public List<Integer> eqri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)).equals(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal register/register
	public List<Integer> eqrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)).equals(register.get(instruction.get(1)))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	public List<List<String>> importation(String file) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day21/day21.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// System.out.println(input);

		List<List<String>> instructions = new ArrayList<>();
		input.remove(0);
		for (String str : input) {
			List<String> instruction = new ArrayList<>();
			String[] splited = str.split(" ");
			String instr = splited[0];
			String A = (splited[1]);
			String B = (splited[2]);
			String C = (splited[3]);
			instruction.add(instr);
			instruction.add(A);
			instruction.add(B);
			instruction.add(C);
			instructions.add(instruction);
		}
		return instructions;
	}

	public int importationBound(String file) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day21/day21.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		int bound = Character.getNumericValue(input.get(0).charAt(4));
		return bound;
	}

}