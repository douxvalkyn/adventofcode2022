package advent2018.day15;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class Combattant {
	
	private int num;
	private Case c;
	private int hp;
	private int attaque;
	private String race;
	private boolean played;
	
	
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public boolean hasPlayed() {
		return played;
	}
	public void setPlayed(boolean played) {
		this.played = played;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public int getNom() {
		return num;
	}
	public void setNom(int nom) {
		this.num = nom;
	}
	public Case getC() {
		return c;
	}
	public void setC(Case c) {
		this.c = c;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAttaque() {
		return attaque;
	}
	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}
	@Override
	public String toString() {
		return "[" +race+ num + ", " + c + ", hp=" + hp + "]";
	}
	
	public Combattant(int num, Case c, String race) {
		super();
		this.num = num;
		this.race= race;
		this.c = c;
		this.hp=200;
		this.attaque=3;
		this.played=false;
	}
	
	
	//renvoie la cible adjacente prioritaire (le moins de HP, puis la case la plus petite)
	public Combattant cibleAdjacentePrioritaire(ArrayList<Case> monde, ArrayList<Combattant> gobelins, ArrayList<Combattant> elfes) {
		ArrayList<Combattant>adversairesPotentiels = new ArrayList<>();
		int x1 = this.getC().getCoord_x();
		int y1 = this.getC().getCoord_y();
		String r1 = this.getRace();
		Combattant ennemi = null;
		// s�lection des adversaires adjacents
		if (r1=="G") {
			for (Combattant combattant : elfes) {
				int x2 = combattant.getC().getCoord_x();
				int y2 = combattant.getC().getCoord_y();			
				if(   (Math.abs(x2-x1)==1 && y2==y1) || (Math.abs(y2-y1)==1 && x2==x1) ) {
					//les combattants sont adjacents et de race oppos�e !
					ennemi=combattant;
					adversairesPotentiels.add(ennemi);
				}
			}
		}
		if (r1=="E") {
			for (Combattant combattant : gobelins) {
				int x2 = combattant.getC().getCoord_x();
				int y2 = combattant.getC().getCoord_y();
				if(   (Math.abs(x2-x1)==1 && y2==y1) || (Math.abs(y2-y1)==1 && x2==x1) ) {
					//les combattants sont adjacents et de race oppos�e !
					ennemi=combattant;
					adversairesPotentiels.add(ennemi);	
				}
			}
		}
		//d�termination de la cible prioritaire
		if (adversairesPotentiels.size()>0) {
			orderCombattants( adversairesPotentiels);
			ennemi= adversairesPotentiels.get(0);
		}
		
		return ennemi;		
	}
	
	
	@SuppressWarnings("unchecked")
	public static void orderCombattants(ArrayList<Combattant> combattants) {

		Collections.sort(combattants, new Comparator() {

			public int compare(Object o1, Object o2) {

				Integer y1 = ((Combattant) o1).getHp();
				Integer y2 = ((Combattant) o2).getHp();
				int sComp = y1.compareTo(y2);

				if (sComp != 0) {
					return sComp;
				} 

				Integer x1 = ((Combattant) o1).getC().getNumero();
				Integer x2 = ((Combattant) o2).getC().getNumero();
				return x1.compareTo(x2);
			}});
	}

	
	public void attaque(Combattant cibleAdjacentePrioritaire) {
		cibleAdjacentePrioritaire.setHp(cibleAdjacentePrioritaire.getHp()-this.getAttaque());	
	}
	
	public Case rechercheCasePrioritaire(ArrayList<Case> monde, ArrayList<Combattant> gobelins,
		ArrayList<Combattant> elfes, int longueurCarte, ArrayList<ArrayList<Integer>> adj) {
		ArrayList<Case>casesPotentielles = new ArrayList<>();
		Case casePrioritaire = null;
		int n=this.getC().getNumero();
		int x1 = this.getC().getCoord_x();
		int y1 = this.getC().getCoord_y();
		String r1 = this.getRace();
			if (r1=="G") {
				//recherche des cases "in range" (adjacentes et vides aux ennemis)
			for (Combattant combattant : elfes) {			
				Case caseEnnemie = combattant.getC();
				Case caseEst = monde.get(caseEnnemie.getNumero()+1-1);
				Case caseOuest = monde.get(caseEnnemie.getNumero()-1-1);
				Case caseNord = monde.get(caseEnnemie.getNumero()-longueurCarte-1);
				Case caseSud = monde.get(caseEnnemie.getNumero()+longueurCarte-1);
				if (caseEst.getOccupation()=='.') {casesPotentielles.add(caseEst);}
				if (caseOuest.getOccupation()=='.') {casesPotentielles.add(caseOuest);}
				if (caseNord.getOccupation()=='.') {casesPotentielles.add(caseNord);}
				if (caseSud.getOccupation()=='.') {casesPotentielles.add(caseSud);}
			}
		}
			if (r1=="E") {
				//recherche des cases potentielles "in range" (adjacentes et vides aux ennemis)
			for (Combattant combattant : gobelins) {			
				Case caseEnnemie = combattant.getC();
				Case caseEst = monde.get(caseEnnemie.getNumero()-1+1);
				Case caseOuest = monde.get(caseEnnemie.getNumero()-1-1);
				Case caseNord = monde.get(caseEnnemie.getNumero()-1-longueurCarte);
				Case caseSud = monde.get(caseEnnemie.getNumero()-1+longueurCarte);
				if (caseEst.getOccupation()=='.') {casesPotentielles.add(caseEst);}
				if (caseOuest.getOccupation()=='.') {casesPotentielles.add(caseOuest);}
				if (caseNord.getOccupation()=='.') {casesPotentielles.add(caseNord);}
				if (caseSud.getOccupation()=='.') {casesPotentielles.add(caseSud);}
			}
		}
			//System.out.println("cases potentielles: "+casesPotentielles);
			// on calcule si possible un plus court chemin vers ces cases potentielles ("in range")
			//on ne conserve que les "nearest", celles avec la distance la plus petite

			 PathUnweighted path = new PathUnweighted();
			 ArrayList<Integer>distances = new ArrayList<>();
			 ArrayList<Case>casesNearest = new ArrayList<>();
			 int minDistance = Integer.MAX_VALUE;
			 for (int i=0; i<casesPotentielles.size(); i++) {
				 int source = (n-1), dest = casesPotentielles.get(i).getNumero()-1;
				 int res = path.printShortestDistance(adj, source, dest, adj.size());
				 if (res != Integer.MAX_VALUE) {
				 if (res==minDistance) {
					 casesNearest.add(casesPotentielles.get(i));
				 }
				 if (res<minDistance) {
					 minDistance=res;
					 casesNearest.clear();
					 casesNearest.add(casesPotentielles.get(i));
				 }

			 }
			 }
			 //System.out.println("cases nearest: "+casesNearest);
			 
			 //on conserve la case "chosen": la premiere dans l'ordre de lecture
			 int minNumero = Integer.MAX_VALUE;
			 Case chosen = null;
			 for (Case c: casesNearest) {
				 if (c.getNumero() < minNumero){
					 minNumero=c.getNumero();
					 chosen=c;
				 }
			 }
			// System.out.println("case chosen: "+ chosen);
			 Case step = null;
			 if (chosen != null) {
			// A partir de la case de d�part, on regarde les cases adjacentes (N,S,E,W) et on calcule la distance vers la case chosen
			 ArrayList<Case>steps = new ArrayList<>();
			 int minDistance2 = Integer.MAX_VALUE;
			 for (int i=0; i<adj.get(n-1).size(); i++) {
				 int source = adj.get(n-1).get(i), dest = chosen.getNumero()-1;
				 //System.out.println(source);
				// System.out.println(dest);
				 int res = path.printShortestDistance(adj, source, dest, adj.size());
				// System.out.println(res);
				 if (res==minDistance2) {
					 steps.add( monde.get(adj.get(n-1).get(i)));
				 }
				 if (res<minDistance2) {
					 minDistance2=res;
					 steps.clear();
					 steps.add( monde.get(adj.get(n-1).get(i)));
				 }
				// System.out.println("cases steps_: "+ steps);
			 }
			 //System.out.println("cases steps: "+ steps);

			 //on conserve la case "step": la premiere dans l'ordre de lecture
			 int minNumero2 = Integer.MAX_VALUE;
			 for (Case c: steps) {
				 if (c.getNumero() < minNumero2){
					 minNumero2=c.getNumero();
					 step=c;
				 }
			 }

			 
			 
			 }
			 //System.out.println("case step: "+ step);	 
			// System.out.println("adj: "+ adj); 
		return step;
	}
	
	
	// fonction qui ordonne une liste de cases selon: (1. le num�ro d'ordre) 
	@SuppressWarnings("unchecked")
	public static void orderCase(ArrayList<Case> cases) {
		Collections.sort(cases, new Comparator() {
			public int compare(Object o1, Object o2) {
				Integer y1 = ((Case) o1).getNumero();
				Integer y2 = ((Case) o2).getNumero();
				return y1.compareTo(y2);			
				}});
	}
	
	

}
