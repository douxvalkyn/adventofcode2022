package advent2018.day15;


public class Case {
	
	private int numero;
	private int coord_x;
	private int coord_y;
	private char occupation;
	
	public Case(int numero, int coord_x, int coord_y, char occupation) {
		super();
		this.numero = numero;
		this.coord_x = coord_x;
		this.coord_y = coord_y;
		this.occupation = occupation;
	}
	@Override
	public String toString() {
		return "[Case" + numero + ", x=" + coord_x + ", y=" + coord_y + ", " + occupation
				+ "]";
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getCoord_x() {
		return coord_x;
	}
	public void setCoord_x(int coord_x) {
		this.coord_x = coord_x;
	}
	public int getCoord_y() {
		return coord_y;
	}
	public void setCoord_y(int coord_y) {
		this.coord_y = coord_y;
	}
	public char getOccupation() {
		return occupation;
	}
	public void setOccupation(char occupation) {
		this.occupation = occupation;
	}
	
	

}
