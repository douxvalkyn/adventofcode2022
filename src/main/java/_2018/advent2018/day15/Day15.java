package advent2018.day15;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day15 {
	// constructeur ----
	public Day15() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day15 d = new Day15();
		double startTime = System.currentTimeMillis();
		String file = "day15";
		d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	
	
	public  void run1() throws ParseException, IOException {
		Day15 d = new Day15();
		String file = "day15";
		d.run1_(file);
	}
	
	
	private void run2(String file) throws FileNotFoundException {
		ArrayList<Case> monde = new ArrayList<>();
		ArrayList<Combattant> gobelins = new ArrayList<>();
		ArrayList<Combattant> elfes = new ArrayList<>();
		ArrayList<Integer> dimensionsCarte = new ArrayList<>();
		int nbElfesInitial = 0;

		// flag pour la recherche de l'attack power n�cessaire pour que les elfes ne subissent aucune perte
		boolean go = true;
		int attaque = -1;
		while (go) {
			attaque = attaque + 1;
			creation(monde, gobelins, elfes, dimensionsCarte, file, attaque);
			nbElfesInitial = elfes.size();
			int longueurCarte = dimensionsCarte.get(0);
			int hauteurCarte = dimensionsCarte.get(0);
			System.out.println("------------------------- Attaque Elfe: " + elfes.get(0).getAttaque() + "-------------------------");

			int round = 0;
			while (elfes.size() > 0 && gobelins.size() > 0) { // gestion des rounds
				// while (round<1) { //TODO test
				round++;
				// System.out.println("------------------------- Tour " + (round )+"-------------------------");

				for (int indice_case = 0; indice_case < monde.size(); indice_case++) { // recherche du combattant suivant
					ArrayList<ArrayList<Integer>> adj = creationGraphe(longueurCarte, hauteurCarte, monde);
					char occup = monde.get(indice_case).getOccupation();
					if (occup == 'G' || occup == 'E') { // on est sur une case avec un combattant ...
						Combattant combattant = rechercherCombattantsurCase(monde.get(indice_case), occup, gobelins, elfes);
						if (!combattant.hasPlayed()) { // ... qui n'a pas encore jou� ce tour
							// System.out.println(">>>>>perso suivant: " + combattant);

							if (combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes) != null) {
								// si le combattant a au moins une cible adjacente alors il d�termine sa cible prioritaire ...
								// ... et il l'attaque
								Combattant cible = combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes);
								combattant.attaque(cible);
								// System.out.println(">" + combattant + " attaque " + cible);
								// si la cible meurt, on la supprime de la liste (gobelins/elfes) et de la case (monde)
								if (cible.getHp() <= 0) {
									if (occup == 'G') {
										// on supprime un elfe
										for (int i = elfes.size(); i > 0; i--) {
											if (elfes.get(i - 1).getHp() <= 0) {
												elfes.remove(i - 1);
											}
										}
									}
									if (occup == 'E') {
										// on supprime un gobelin
										for (int i = gobelins.size(); i > 0; i--) {
											if (gobelins.get(i - 1).getHp() <= 0) {
												gobelins.remove(i - 1);
											}
										}
									}
									// on actualise la case ou se trouvait le combattant mort
									monde.get(cible.getC().getNumero() - 1).setOccupation('.');
								}

							} else {
								// sinon il d�termine la case cible vers laquelle se d�placer
								Case step = combattant.rechercheCasePrioritaire(monde, gobelins, elfes, longueurCarte, adj);
								// System.out.println(combattant + " se d�place en case: " +step);

								// mettre � jour le monde, gobelins et elfes
								if (step != null) {
									// maj monde
									monde.get(indice_case).setOccupation('.'); // l'ancienne case devient vide
									monde.get(step.getNumero() - 1).setOccupation(combattant.getRace().charAt(0)); // on modifie la nouvelle case
									if (combattant.getRace() == "E") {
										for (Combattant e : elfes) {
											if (e == combattant) {
												e.setC(step);
												e.setPlayed(true);
											}
										}
									}
									if (combattant.getRace() == "G") {
										for (Combattant g : gobelins) {
											if (g == combattant) {
												g.setC(step);
												g.setPlayed(true);
											}
										}
									}

								}

								// et il attaque s'il est � port�e
								if (combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes) != null) {
									// si le combattant a au moins une cible adjacente alors il d�termine sa cible prioritaire ...
									// ... et il l'attaque
									Combattant cible = combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes);
									combattant.attaque(cible);
									// System.out.println(combattant + " attaque: " +cible);
									// si la cible meurt, on la supprime de la liste (gobelins/elfes) et de la case (monde)
									if (cible.getHp() <= 0) {
										if (occup == 'G') {
											// on supprime un elfe
											for (int i = elfes.size(); i > 0; i--) {
												if (elfes.get(i - 1).getHp() <= 0) {
													elfes.remove(i - 1);
												}
											}
										}
										if (occup == 'E') {
											// on supprime un gobelin
											for (int i = gobelins.size(); i > 0; i--) {
												if (gobelins.get(i - 1).getHp() <= 0) {
													gobelins.remove(i - 1);
												}
											}
										}
										// on actualise la case ou se trouvait le combattant mort
										monde.get(cible.getC().getNumero() - 1).setOccupation('.');
									}
								}

							}

							// System.out.println(">elfes apres: " +elfes);
							// System.out.println(">gobelins apres: " +gobelins);
							// System.out.println(">monde apres: " +monde);
							// System.out.println(">>>>> fin du tour du perso");

						}
					}
				}

				// reinitialiser les persos apres le tour: played= false;
				for (Combattant g : gobelins) {
					g.setPlayed(false);
				}
				for (Combattant e : elfes) {
					e.setPlayed(false);
				}

				// grille de visualisation et export
				// char[][] grille = new char[longueurCarte][hauteurCarte];
				// int k=0;
				// String str="";
				// for (int i=0; i<longueurCarte; i++) {
				// for (int j=0; j<hauteurCarte; j++) {
				// grille[i][j]=monde.get(k).getOccupation();
				// System.out.print(grille[i][j] );
				// k++;
				// str=str+grille[i][j];
				// }
				// System.out.println();
				// str=str + "\n";
				// }
				// try (PrintWriter out = new PrintWriter("filename"+round+".txt")) {
				// out.println(str);
				// }

			}

			// calcul du score en fin de partie
			int pvRestantsGobelins = 0;
			for (Combattant g : gobelins) {
				pvRestantsGobelins = pvRestantsGobelins + g.getHp();
			}
			int pvRestantsElfes = 0;
			for (Combattant e : elfes) {
				pvRestantsElfes = pvRestantsElfes + e.getHp();
			}
			int score = (round) * (pvRestantsGobelins + pvRestantsElfes);
			System.out.println("-----------------------");
			if (pvRestantsGobelins == 0) {
				System.out.println("!Victoire des elfes !");
			}
			if (pvRestantsElfes == 0) {
				System.out.println("!Victoire des gobelins !");
			}
			System.out.println("!Elfes restants: " + elfes.size());
			int nbElfesFinal = elfes.size();
			if (nbElfesInitial == nbElfesFinal) {
				go = false;
			}
			System.out.println("!Score1: " + (round));
			System.out.println("!Score2: " + (pvRestantsGobelins + pvRestantsElfes));
			System.out.println("!Score total: " + score);
			System.out.println("Attention, il faut retirer 1 � score1 pour l'input reel au run2() : 29*1505=46345");
			System.out.println("----------------------------------------------------");

		}

	}

	private void run1_(String file) throws FileNotFoundException {
		ArrayList<Case> monde = new ArrayList<>();
		ArrayList<Combattant> gobelins = new ArrayList<>();
		ArrayList<Combattant> elfes = new ArrayList<>();
		ArrayList<Integer> dimensionsCarte = new ArrayList<>();
		int attaque = 0;
		creation(monde, gobelins, elfes, dimensionsCarte, file, attaque);
		int longueurCarte = dimensionsCarte.get(0);
		int hauteurCarte = dimensionsCarte.get(0);

		int round = 0;
		while (elfes.size() > 0 && gobelins.size() > 0) { // gestion des rounds
			// while (round<1) { //TODO test
			round++;
			System.out.println("------------------------- Tour " + (round) + "-------------------------");

			for (int indice_case = 0; indice_case < monde.size(); indice_case++) { // recherche du combattant suivant
				ArrayList<ArrayList<Integer>> adj = creationGraphe(longueurCarte, hauteurCarte, monde);
				char occup = monde.get(indice_case).getOccupation();
				if (occup == 'G' || occup == 'E') { // on est sur une case avec un combattant ...
					Combattant combattant = rechercherCombattantsurCase(monde.get(indice_case), occup, gobelins, elfes);
					if (!combattant.hasPlayed()) { // ... qui n'a pas encore jou� ce tour
						System.out.println(">>>>>perso suivant: " + combattant);

						if (combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes) != null) {
							// si le combattant a au moins une cible adjacente alors il d�termine sa cible prioritaire ...
							// ... et il l'attaque
							Combattant cible = combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes);
							combattant.attaque(cible);
							// System.out.println(">" + combattant + " attaque " + cible);
							// si la cible meurt, on la supprime de la liste (gobelins/elfes) et de la case (monde)
							if (cible.getHp() <= 0) {
								if (occup == 'G') {
									// on supprime un elfe
									for (int i = elfes.size(); i > 0; i--) {
										if (elfes.get(i - 1).getHp() <= 0) {
											elfes.remove(i - 1);
										}
									}
								}
								if (occup == 'E') {
									// on supprime un gobelin
									for (int i = gobelins.size(); i > 0; i--) {
										if (gobelins.get(i - 1).getHp() <= 0) {
											gobelins.remove(i - 1);
										}
									}
								}
								// on actualise la case ou se trouvait le combattant mort
								monde.get(cible.getC().getNumero() - 1).setOccupation('.');
							}

						} else {
							// sinon il d�termine la case cible vers laquelle se d�placer
							Case step = combattant.rechercheCasePrioritaire(monde, gobelins, elfes, longueurCarte, adj);
							// System.out.println(combattant + " se d�place en case: " +step);

							// mettre � jour le monde, gobelins et elfes
							if (step != null) {
								// maj monde
								monde.get(indice_case).setOccupation('.'); // l'ancienne case devient vide
								monde.get(step.getNumero() - 1).setOccupation(combattant.getRace().charAt(0)); // on modifie la nouvelle case
								if (combattant.getRace() == "E") {
									for (Combattant e : elfes) {
										if (e == combattant) {
											e.setC(step);
											e.setPlayed(true);
										}
									}
								}
								if (combattant.getRace() == "G") {
									for (Combattant g : gobelins) {
										if (g == combattant) {
											g.setC(step);
											g.setPlayed(true);
										}
									}
								}

							}

							// et il attaque s'il est � port�e
							if (combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes) != null) {
								// si le combattant a au moins une cible adjacente alors il d�termine sa cible prioritaire ...
								// ... et il l'attaque
								Combattant cible = combattant.cibleAdjacentePrioritaire(monde, gobelins, elfes);
								combattant.attaque(cible);
								// System.out.println(combattant + " attaque: " +cible);
								// si la cible meurt, on la supprime de la liste (gobelins/elfes) et de la case (monde)
								if (cible.getHp() <= 0) {
									if (occup == 'G') {
										// on supprime un elfe
										for (int i = elfes.size(); i > 0; i--) {
											if (elfes.get(i - 1).getHp() <= 0) {
												elfes.remove(i - 1);
											}
										}
									}
									if (occup == 'E') {
										// on supprime un gobelin
										for (int i = gobelins.size(); i > 0; i--) {
											if (gobelins.get(i - 1).getHp() <= 0) {
												gobelins.remove(i - 1);
											}
										}
									}
									// on actualise la case ou se trouvait le combattant mort
									monde.get(cible.getC().getNumero() - 1).setOccupation('.');
								}
							}

						}

						System.out.println(">elfes apres: " + elfes);
						System.out.println(">gobelins apres: " + gobelins);
						System.out.println(">monde apres: " + monde);
						System.out.println(">>>>> fin du tour du perso");

					}
				}
			}

			// reinitialiser les persos apres le tour: played= false;
			for (Combattant g : gobelins) {
				g.setPlayed(false);
			}
			for (Combattant e : elfes) {
				e.setPlayed(false);
			}

			// grille de visualisation et export
			// char[][] grille = new char[longueurCarte][hauteurCarte];
			// int k=0;
			// String str="";
			// for (int i=0; i<longueurCarte; i++) {
			// for (int j=0; j<hauteurCarte; j++) {
			// grille[i][j]=monde.get(k).getOccupation();
			// System.out.print(grille[i][j] );
			// k++;
			// str=str+grille[i][j];
			// }
			// System.out.println();
			// str=str + "\n";
			// }
			// try (PrintWriter out = new PrintWriter("filename"+round+".txt")) {
			// out.println(str);
			// }

		}

		// calcul du score en fin de partie
		int pvRestantsGobelins = 0;
		for (Combattant g : gobelins) {
			pvRestantsGobelins = pvRestantsGobelins + g.getHp();
		}
		int pvRestantsElfes = 0;
		for (Combattant e : elfes) {
			pvRestantsElfes = pvRestantsElfes + e.getHp();
		}
		int score = (round - 1) * (pvRestantsGobelins + pvRestantsElfes);
		System.out.println("-----------------------");
		if (pvRestantsGobelins == 0) {
			System.out.println("!Victoire des elfes !");
		}
		if (pvRestantsElfes == 0) {
			System.out.println("!Victoire des gobelins !");
		}
		System.out.println("!Score1: " + (round - 1));
		System.out.println("!Score2: " + (pvRestantsGobelins + pvRestantsElfes));
		System.out.println("!Score total: " + score);

	}

	// renvoie le combattant qui est sur cette case
	private Combattant rechercherCombattantsurCase(Case case1, char nextOccupation, ArrayList<Combattant> gobelins, ArrayList<Combattant> elfes) {
		Combattant combattant = null;
		if (nextOccupation == 'G') {
			for (int i = 0; i < gobelins.size(); i++) {
				if (case1 == gobelins.get(i).getC()) {
					combattant = gobelins.get(i);
				}
			}
		}
		if (nextOccupation == 'E') {
			for (int i = 0; i < elfes.size(); i++) {
				if (case1 == elfes.get(i).getC()) {
					combattant = elfes.get(i);
				}
			}
		}
		return combattant;
	}

	private void creation(ArrayList<Case> monde, ArrayList<Combattant> gobelins, ArrayList<Combattant> elfes, ArrayList<Integer> dimensionsCartes, String file, int attaque)
		throws FileNotFoundException {
		monde.clear();
		gobelins.clear();
		elfes.clear();
		dimensionsCartes.clear();

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day15/day15.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		int hauteurCarte = input.size();
		int longueurCarte = input.get(0).length();
		dimensionsCartes.add(longueurCarte);
		dimensionsCartes.add(hauteurCarte);

		// cr�er le monde (cases + combattants)
		int num = 1, num_gobelin = 1, num_elfe = 1;
		for (int j = 0; j < input.size(); j++) {
			String ligne = input.get(j);
			for (int i = 0; i < ligne.length(); i++) {
				char lettre = ligne.charAt(i);
				Case c = new Case(num, i, j, lettre);
				num++;
				monde.add(c);
				if (lettre == 'G') {
					Combattant gob = new Combattant(+num_gobelin, c, "G");
					gobelins.add(gob);
					num_gobelin++;
				}
				if (lettre == 'E') {
					Combattant elfe = new Combattant(+num_elfe, c, "E");
					elfes.add(elfe);
					num_elfe++;
				}
			}
		}
		// System.out.println("!input: "+input);
		// System.out.println("!monde: "+monde);

		// Augmenter l'attaque des elfes (run2)
		for (Combattant comb : elfes) {
			comb.setAttaque(comb.getAttaque() + attaque);
		}

		// System.out.println("!gobelins: "+gobelins);
		// System.out.println("!elfes: "+elfes);
	}

	// creation de la liste des noeuds du graphe de toutes les cases
	private ArrayList<ArrayList<Integer>> creationGraphe(int longueurCarte, int hauteurCarte, ArrayList<Case> monde) {

		// nb de noeuds (vertice) du graphe
		int v = longueurCarte * hauteurCarte;

		// Adjacency list for storing which vertices are connected
		ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>(v);
		for (int i = 0; i < v; i++) {
			adj.add(new ArrayList<Integer>());
		}

		for (int i = 0; i < monde.size(); i++) {

			int num = monde.get(i).getNumero();
			int numOuest = -1, numNord = -1, numEst = -1, numSud = -1;

			if (monde.get(i).getCoord_x() != longueurCarte - 1 && monde.get(i).getOccupation() != '#') {
				numEst = monde.get(i + 1).getNumero();
				if (monde.get(numEst - 1).getOccupation() == '.') {
					addEdge(adj, num - 1, numEst - 1);
					// System.out.println(num + ">"+ numEst);
				}
			}

			if (monde.get(i).getCoord_x() != 0 && monde.get(i).getOccupation() != '#') {
				numOuest = monde.get(i - 1).getNumero();
				if (monde.get(numOuest - 1).getOccupation() == '.') {
					addEdge(adj, num - 1, numOuest - 1);
					// System.out.println(num + ">"+ numOuest);
				}
			}
			if (monde.get(i).getCoord_y() != 0 && monde.get(i).getOccupation() != '#') {
				numNord = monde.get(i - longueurCarte).getNumero();
				if (monde.get(numNord - 1).getOccupation() == '.') {
					addEdge(adj, num - 1, numNord - 1);
					// System.out.println(num + ">"+ numNord);
				}
			}
			if (monde.get(i).getCoord_y() != hauteurCarte - 1 && monde.get(i).getOccupation() != '#') {
				numSud = monde.get(i + longueurCarte).getNumero();
				if (monde.get(numSud - 1).getOccupation() == '.') {
					addEdge(adj, num - 1, numSud - 1);
					// System.out.println(num + ">"+ numSud);
				}
			}
		}
		return adj;
	}

	// function to form edge between two vertices : source and dest
	private static void addEdge(ArrayList<ArrayList<Integer>> adj, int i, int j) {
		adj.get(i).add(j);
	}

}