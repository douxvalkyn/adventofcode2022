package advent2018.day20;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import advent2018.outils.PathUnweighted;


public class Day20 {

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day20 d = new Day20();
		double startTime = System.currentTimeMillis();
		d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// M�thodes ----
	
	
	public  void run1() throws ParseException, IOException {
		Day20 d = new Day20();
		String file = "day20";
		d.run1_(file);
	}
	
	public void run1_(String file) throws FileNotFoundException {
		String input = importation(file);
		int ymax = 1000, xmax = 1000;
		char[][] grille = new char[ymax][xmax];
		int x = xmax / 2, y = ymax / 2;
		grille[ymax / 2][xmax / 2] = 'X';
		lectureRegex(grille, input, x, y, ymax, xmax);
		recherchePorteEloignee(grille);
		// visualisation(grille,ymax,xmax);

	}

	private void recherchePorteEloignee(char[][] grille) {
		// creation d'une grille jumelle avec les num�ros de cases
		int[][] grilleNum = new int[grille.length][grille[0].length];

		// nb de pieces
		int nbPieces = 1;
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				if (grille[i][j] == '.' | grille[i][j] == 'X') {
					grilleNum[i][j] = (nbPieces - 1);
					nbPieces++;
				}
			}
		}

		// Adjacency list: pieces num�rot�es de 0 � nbPieces-1
		ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>(nbPieces);
		for (int i = 0; i < (nbPieces - 1); i++) {
			adj.add(new ArrayList<Integer>());
		}

		// Creating graph given in the above diagram.
		// add_edge function takes adjacency list, source and destination vertex as argument and forms an edge between them.
		// numPiece pour num�roter les pieces : numPiece = 1000000*j+i;
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				if (grille[i][j] == '.' | grille[i][j] == 'X') {
					if (grille[i][j + 1] == '|' | grille[i][j + 1] == '-') { // Est
						addEdge(adj, grilleNum[i][j], grilleNum[i][j + 2]);
					}
					if (grille[i][j - 1] == '|' | grille[i][j - 1] == '-') { // Ouest
						addEdge(adj, grilleNum[i][j], grilleNum[i][j - 2]);
					}
					if (grille[i + 1][j] == '|' | grille[i + 1][j] == '-') { // Sud
						addEdge(adj, grilleNum[i][j], grilleNum[i + 2][j]);
					}
					if (grille[i - 1][j] == '|' | grille[i - 1][j] == '-') { // Nord
						addEdge(adj, grilleNum[i][j], grilleNum[i - 2][j]);
					}
				}
			}
		}
		// Recherche du num�ro de la case X
		int source = 0;
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				if (grille[i][j] == 'X') {
					source = grilleNum[i][j];
				}
			}
		}

		// calcule des distances vers toutes les pieces et sauvegarde de la distanceMax
		int max = 0;
		// pour le run2, recherche du nb de pieces eloignes d'au moins 1000 portes
		int dist_sup_1000 = 0;
		for (int numPiece = 0; numPiece < (nbPieces - 1); numPiece++) {
			int distance = PathUnweighted.printShortestDistance(adj, source, numPiece, nbPieces);
			if (distance > max) {
				max = distance;
			}
			if (distance >= 1000) {
				dist_sup_1000++;
			}
		}
		System.out.println("distance max: " + max);
		System.out.println("distance >= 1000: " + dist_sup_1000);
	}

	public void addEdge(ArrayList<ArrayList<Integer>> adj, int i, int j) {
		adj.get(i).add(j);

	}

	private void lectureRegex(char[][] grille, String input, int x, int y, int ymax, int xmax) {
		int x_init = x;
		int y_init = y;
		for (int i = 1; i < input.length(); i++) { //
			char direction = input.charAt(i);
			// System.out.println(input.charAt(i));
			if (direction == 'W') {
				grille[y][x - 1] = '|';
				grille[y][x - 2] = '.';
				if (grille[y][x + 1] == '\u0000') {
					grille[y][x + 1] = '?';
				}
				if (grille[y - 1][x] == '\u0000') {
					grille[y - 1][x] = '?';
				}
				if (grille[y + 1][x] == '\u0000') {
					grille[y + 1][x] = '?';
				}
				x = x - 2;
			}
			if (direction == 'E') {
				grille[y][x + 1] = '|';
				grille[y][x + 2] = '.';
				if (grille[y][x - 1] == '\u0000') {
					grille[y][x - 1] = '?';
				}
				if (grille[y - 1][x] == '\u0000') {
					grille[y - 1][x] = '?';
				}
				if (grille[y + 1][x] == '\u0000') {
					grille[y + 1][x] = '?';
				}
				x = x + 2;
			}
			if (direction == 'N') {
				grille[y - 1][x] = '-';
				grille[y - 2][x] = '.';
				if (grille[y][x - 1] == '\u0000') {
					grille[y][x - 1] = '?';
				}
				if (grille[y][x + 1] == '\u0000') {
					grille[y][x + 1] = '?';
				}
				if (grille[y + 1][x] == '\u0000') {
					grille[y + 1][x] = '?';
				}
				y = y - 2;
			}
			if (direction == 'S') {
				grille[y + 1][x] = '-';
				grille[y + 2][x] = '.';
				if (grille[y][x - 1] == '\u0000') {
					grille[y][x - 1] = '?';
				}
				if (grille[y][x + 1] == '\u0000') {
					grille[y][x + 1] = '?';
				}
				if (grille[y - 1][x] == '\u0000') {
					grille[y - 1][x] = '?';
				}
				y = y + 2;
			}
			if (direction == '|') {
				x = x_init;
				y = y_init;
			}
			if (direction == '(') { // on entre dans une branche multiple
				// on chercher la parenthese fermante associ�e pour lire ce sous-chemin
				int nb_ouvrantes_intermediaires = 0;
				int indice_fermante = 0;
				for (int l = i + 1; l < input.length(); l++) {
					char lettre = input.charAt(l);
					if (lettre == '(') {
						nb_ouvrantes_intermediaires++;
					}
					if (lettre == ')') {
						if (nb_ouvrantes_intermediaires == 0) {
							indice_fermante = l;
						} else {
							nb_ouvrantes_intermediaires--;
						}
					}
				}

				String inputExtrait = '^' + input.substring(i + 1, indice_fermante) + '$';
				// comme on va lire un extrait avec la ligne ci-dessous, il faut remplacer les lettres de l'input initial pour
				// ne pas les lire une 2e fois !
				String avant = input.substring(0, i);
				String apres = input.substring(indice_fermante, input.length());
				String replacement = "Z";
				for (int t = 1; t < inputExtrait.length(); t++) {
					replacement = replacement + "Z";
				}
				String newInput = avant + replacement + apres;
				input = newInput;
				lectureRegex(grille, inputExtrait, x, y, ymax, xmax);

			}

		}

		for (int i = 0; i < xmax; i++) {
			for (int j = 0; j < ymax; j++) {
				if (grille[i][j] == '?' | grille[i][j] == '\u0000') {
					grille[i][j] = '#';
				}
			}
		}

	}

	public String importation(String file) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day20/day20.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println(input);

		return input.get(0);
	}

	private void visualisation(char[][] grille, int imax, int jmax) {
		String str = "";
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				System.out.print(grille[i][j]);
				str = str + grille[i][j];
			}
			System.out.println();
			str = str + "\n";
		}
	}

}
