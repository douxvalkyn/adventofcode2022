package advent2018.day09;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Day09 {

	// constructeur ----
	public Day09() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day09 d = new Day09();
		// double startTime = System.currentTimeMillis();
		d.run1();
		double endTime = System.currentTimeMillis();
		// System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}
	
	
	public  void run1() throws ParseException, IOException {
		Day09 d = new Day09();
		int nb_joueurs = 430;
		int nb_tours = 71588 * 100;
		d.run1_(nb_joueurs, nb_tours);
	}
	

	// En utilisant une Hashmap pour la liste des billes: plus lent finalement ...
	private void run1b(int nb_joueurs, int nb_tours) throws FileNotFoundException {
		// Scanner infile = new Scanner(new FileReader("src/day10/day10.txt"));
		// ArrayList<Integer> input = new ArrayList<>();
		// while (infile.hasNext()) {
		// input.add(infile.nextInt());
		// }
		// System.out.println(input);
		LinkedHashMap<Integer, Integer> map = new LinkedHashMap<Integer, Integer>(); // liste des billes jou�es

		int[] joueurs = new int[nb_joueurs]; // liste des joueurs avec leur score
		map.put(0, 0);
		int position_actuel = 1;
		double startTime = System.currentTimeMillis();
		for (int i = 1; i < nb_tours; i++) {
			// if (i%100000==0) {System.out.println(i);System.out.println("Time: "+ (System.currentTimeMillis() - startTime)/1000 + " s");}
			if (i % 23 != 0) { // si r�gle classique
				if (position_actuel + 2 > map.size()) {
					my_put(map, 1, i, i);
					position_actuel = 1;
				} else {
					my_put(map, position_actuel + 2, i, i);
					position_actuel = position_actuel + 2;
				}
				// System.out.println(map);
			} else { // sinon r�gle 23
				joueurs[i % nb_joueurs] = joueurs[i % nb_joueurs] + i;
				// attention si on va � gauche de la bille 0
				if (position_actuel - 7 < 0) {
					position_actuel = map.size() + (position_actuel - 7);
				} else {
					position_actuel = position_actuel - 7;
				}
				joueurs[i % nb_joueurs] = joueurs[i % nb_joueurs] + (int) map.values().toArray()[position_actuel];
				map.remove(map.values().toArray()[position_actuel], map.values().toArray()[position_actuel]);
			}

		}

		// recherche du gagnant et de son score
		int score_max = 0;
		for (int i = 0; i < joueurs.length; i++) {
			if (score_max < joueurs[i]) {
				score_max = joueurs[i];
			}
		}
		System.out.println("Score du gagnant: " + score_max);

	}

	private void run1_(int nb_joueurs, int nb_tours) throws FileNotFoundException {
		// Scanner infile = new Scanner(new FileReader("src/day10/day10.txt"));
		// ArrayList<Integer> input = new ArrayList<>();
		// while (infile.hasNext()) {
		// input.add(infile.nextInt());
		// }
		// System.out.println(input);
		ArrayList<Integer> liste = new ArrayList<Integer>(); // liste des billes jou�es
		double[] joueurs = new double[nb_joueurs]; // liste des joueurs avec leur score
		liste.add(0);
		int position_actuel = 1;
		double startTime = System.currentTimeMillis();
		for (int i = 1; i < nb_tours; i++) {
			if (i % 100000 == 0) {
				System.out.println(i);
				System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 60000 + " min");
			}
			if (i % 23 != 0) { // si r�gle classique
				if (position_actuel + 2 > liste.size()) {
					liste.add(1, i);
					position_actuel = 1;
				} else {
					liste.add(position_actuel + 2, i);
					position_actuel = position_actuel + 2;
				}
				// System.out.println(liste);
			} else { // sinon r�gle 23
				// System.out.println("joueur "+i%nb_joueurs+" marque "+ i + " points");
				joueurs[i % nb_joueurs] = joueurs[i % nb_joueurs] + i;

				// attention si on va � gauche de la bille 0
				if (position_actuel - 7 < 0) {
					position_actuel = liste.size() + (position_actuel - 7);
				} else {
					position_actuel = position_actuel - 7;
				}
				// System.out.println("la bille "+ liste.get(position_actuel)+ " est retir�e et ajout�e au score du joueur"+i%nb_joueurs);
				joueurs[i % nb_joueurs] = joueurs[i % nb_joueurs] + liste.get(position_actuel);
				liste.remove(position_actuel);
			}

		}

		// recherche du gagnant et de son score

		double score_max = 0;
		for (int i = 0; i < joueurs.length; i++) {
			System.out.println(joueurs[i]);
			if (score_max < joueurs[i]) {
				score_max = joueurs[i];
			}
		}
		System.out.println("Score du gagnant: " + score_max);
		DecimalFormat decimalPrintFormat = new DecimalFormat("#,##0.####");
		DecimalFormat decimalPrintFormat2 = new DecimalFormat("##0.####");
		System.out.println(decimalPrintFormat.format(score_max));
		System.out.println(decimalPrintFormat2.format(score_max));

	}

	public static void my_put(LinkedHashMap<Integer, Integer> input, int index, int key, int value) {

		if (index >= 0 && index <= input.size()) {
			LinkedHashMap<Integer, Integer> output = new LinkedHashMap<Integer, Integer>();
			int i = 0;
			if (index == 0) {
				output.put(key, value);
				output.putAll(input);
			} else {
				for (Map.Entry<Integer, Integer> entry : input.entrySet()) {
					if (i == index) {
						output.put(key, value);
					}
					output.put(entry.getKey(), entry.getValue());
					i++;
				}
			}
			if (index == input.size()) {
				output.put(key, value);
			}
			input.clear();
			input.putAll(output);
			output.clear();
			output = null;
		} else {
			throw new IndexOutOfBoundsException("index " + index + " must be equal or greater than zero and less than size of the map");
		}
	}

}
