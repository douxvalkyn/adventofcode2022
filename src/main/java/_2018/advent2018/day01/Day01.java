package advent2018.day01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day01 {

	// constructeur
	public Day01() {
	}

	// main
	public static void main(String[] args) throws FileNotFoundException {
		Day01 d = new Day01();
		double startTime = System.currentTimeMillis();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// run1
	public void run1() throws FileNotFoundException {
		int somme = 0;

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day01/day1.txt"));

		while (infile.hasNext()) {
			// System.out.println(somme);
			somme = somme + infile.nextInt();
		}

		System.out.println("Somme = " + somme);
	}

	// run2
	public void run2() throws FileNotFoundException {

		// import
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day01/day1.txt"));
		
		ArrayList<Integer> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextInt());
		}
		// System.out.println("ma liste: " +input);

		// Traitements
		int somme = 0;
		ArrayList<Integer> liste_histo = new ArrayList<>();
		liste_histo.add(0);
		boolean flag = true;
		int i = 0;
		while (flag && i < input.size()) {

			somme = somme + input.get(i);
			// System.out.print(somme);
			i = i + 1;
			if (i == input.size()) {
				i = 0;
			}

			if (liste_histo.contains(somme)) {
				flag = false;
				System.out.println("Le nombre doublé est: " + somme);
			} else {
				liste_histo.add(somme);
			}
		}
	}
}
