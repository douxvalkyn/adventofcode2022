package advent2018.day24;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Day24 {

	// main ------
	public static void main(String[] args) throws ParseException, IOException {
		Day24 d = new Day24();
		d.run1();
		d.run2();
	}

	
	
	public  void run1() throws ParseException, IOException {
		Day24 d = new Day24();
		String file = "day24";
		d.run1_(file);
	}
	public  void run2() throws ParseException, IOException {
		Day24 d = new Day24();
		String file = "day24";
		d.run2_(file);
	}
	
	
	private void run2_(String file) throws FileNotFoundException {
		List<Group> immuneSystem = new ArrayList<Group>();
		List<Group> infection = new ArrayList<Group>();

		int boost = 39; // bonne reponse, les boosts de 1 � 38 g�nerent une impasse: �galit� !
		boolean GO = true;
		// while (GO) {
		// boost++;
		System.out.println("Boost:+" + boost);
		importation2(file, immuneSystem, infection, boost);
		int i = 0;
		while (immuneSystem.size() > 0 & infection.size() > 0) {
			// System.out.println("----------");
			System.out.println("D�but du tour " + (i + 1));
			System.out.println("Immune System: " + immuneSystem);
			System.out.println("Infection: " + infection);
			findTarget(immuneSystem, infection);
			findTarget(infection, immuneSystem);
			attaque(immuneSystem, infection);
			i++;
		}
		System.out.println("----------");
		System.out.println("Fin du combat");
		System.out.println("Immune System: " + immuneSystem);
		System.out.println("Infection: " + infection);
		int resultat = 0;
		// if (immuneSystem.size()==0) {
		// for (Group grp: infection) {
		// resultat=resultat+grp.getNb();
		// }
		// }
		// if (infection.size()==0) {
		// for (Group grp: immuneSystem) {
		// resultat=resultat+grp.getNb();
		// }
		// }
		// System.out.println("resultat: "+ resultat);
		if (infection.size() == 0) {
			GO = false;
			System.out.println("Immune System: " + immuneSystem);
			System.out.println("Infection: " + infection);
			for (Group grp : immuneSystem) {
				resultat = resultat + grp.getNb();
			}
			System.out.println("resultat: " + resultat);
			System.out.println(GO);
		}
		immuneSystem.clear();
		infection.clear();
	}

	// }

	private void importation2(String file, List<Group> immuneSystem, List<Group> infection, int boost) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day24/day24.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		int faction = 0;
		for (String str : input) {
			if (str.equals("Immune System:")) {
				faction = 1;
			}
			if (str.equals("Infection:")) {
				faction = 2;
			}

			if (!str.equals("Immune System:") & !str.equals("Infection:") & !str.equals("")) {
				String[] weaknesses = null;
				String[] immunities = null;

				if (str.contains("(")) {
					String insert = str.substring(str.indexOf('(') + 1, str.indexOf(')'));
					String[] inserts = insert.split(";");

					for (String ins : inserts) {
						ins = ins.trim();

						if (ins.charAt(0) == 'i') { // immune
							ins = ins.substring(10);
							ins = ins.replaceAll("\\s+", "");
							immunities = ins.split(",");
						}
						if (ins.charAt(0) == 'w') { // weaknesses
							ins = ins.substring(8);
							ins = ins.replaceAll("\\s+", "");
							weaknesses = ins.split(",");
						}
					}
				}

				str = str.replaceAll("\\(.*?\\)", ".");
				String[] str_ = str.split(" ");

				Group group = new Group(Integer.valueOf(str_[0]), Integer.valueOf(str_[4]), Integer.valueOf(str_[13]), str_[14], Integer.valueOf(str_[18]), weaknesses, immunities);
				// System.out.println(group);
				if (faction == 1) {
					// on ajoute le boost
					group.setAttackDamage(group.getAttackDamage() + boost);
					immuneSystem.add(group);
				}
				if (faction == 2) {
					infection.add(group);
				}

			}
		}
	}

	private void run1_(String file) throws FileNotFoundException {
		List<Group> immuneSystem = new ArrayList<Group>();
		List<Group> infection = new ArrayList<Group>();
		importation(file, immuneSystem, infection);
		int i = 0;
		while (immuneSystem.size() > 0 & infection.size() > 0) {
			System.out.println("----------");
			System.out.println("D�but du tour " + (i + 1));
			System.out.println("Immune System: " + immuneSystem);
			System.out.println("Infection: " + infection);
			findTarget(immuneSystem, infection);
			findTarget(infection, immuneSystem);
			attaque(immuneSystem, infection);
			i++;
		}
		System.out.println("----------");
		System.out.println("Fin du combat");
		System.out.println("Immune System: " + immuneSystem);
		System.out.println("Infection: " + infection);
		int resultat = 0;
		if (immuneSystem.size() == 0) {
			for (Group grp : infection) {
				resultat = resultat + grp.getNb();
			}
		}
		if (infection.size() == 0) {
			for (Group grp : immuneSystem) {
				resultat = resultat + grp.getNb();
			}
		}
		System.out.println("resultat: " + resultat);

	}

	private void attaque(List<Group> immuneSystem, List<Group> infection) {

		// mettre tous les groupes par ordre d'initiative (immune system et infection m�lang�s
		List<Group> groupes = new ArrayList<Group>();
		for (Group grp : immuneSystem) {
			groupes.add(grp);
		}
		for (Group grp : infection) {
			groupes.add(grp);
		}
		Collections.sort(groupes, Group.ComparatorInit);
		Collections.reverse(groupes);

		// application des d�gats
		for (Group attaquant : groupes) {
			// si un groupe n'a pas de target, il ne peut pas attaquer
			if (attaquant.getTarget() != null) {
				// si un groupe a 0 unit�s, il ne peut pas attaquer
				if (attaquant.getNb() > 0) {
					int effPower = attaquant.getAttackDamage() * attaquant.getNb();
					Group target = attaquant.getTarget();
					// Gestion des weaknesses
					String[] weaknesses = null;
					if (target.getWeaknesses() != null) {
						weaknesses = target.getWeaknesses();
					}
					if (weaknesses != null) {
						for (String weak : weaknesses) {
							if (weak.equals(attaquant.getAttackType())) {
								effPower = 2 * effPower;
							}
						}
					}
					// fin gestion weaknesses
					// recherche de la target dans la liste et application des d�gats
					for (Group grp : groupes) {
						if (grp == target) {
							int killed = effPower / target.getHp();
							if (target.getNb() < killed) {
								killed = target.getNb();
							} // on ne peut pas tuer plus d'unit�s que la cible n'en poss�de
								// System.out.println(attaquant + " attaque " + target + " en tuant " + killed + " unit�s." );
							target.setNb(target.getNb() - killed);
						}
					}

				}
			}
		}
		// Apres les attaques, remettre l'indicateur estCible � false (et target � null) pour tous les groupes
		for (Group grp : groupes) {
			grp.setEstCible(false);
			grp.setTarget(null);
		}

		// reconstituer les listes immune system et infection;
		for (Group grp : immuneSystem) {
			for (Group grp2 : groupes) {
				if (grp.getAttackDamage() == grp2.getAttackDamage() & grp.getHp() == grp2.getHp() & grp.getInitiative() == grp2.getInitiative()) {
					grp = grp2;
				}
			}
		}
		for (Group grp : infection) {
			for (Group grp2 : groupes) {
				if (grp.getAttackDamage() == grp2.getAttackDamage() & grp.getHp() == grp2.getHp() & grp.getInitiative() == grp2.getInitiative()) {
					grp = grp2;
				}
			}
		}

		// si un groupe a 0 unit�s, il disparait
		if (immuneSystem.size() == 1 & immuneSystem.get(0).getNb() == 0) {
			immuneSystem.clear();
		} else {
			Iterator<Group> it = immuneSystem.iterator();
			while (it.hasNext()) {
				Group grp = it.next();
				if (grp.getNb() <= 0) {
					it.remove();
				}
			}
		}

		if (infection.size() == 1 & infection.get(0).getNb() == 0) {
			infection.clear();
		} else {
			Iterator<Group> it = infection.iterator();
			while (it.hasNext()) {
				Group grp = it.next();
				if (grp.getNb() <= 0) {
					it.remove();
				}
			}
		}

		// System.out.println(immuneSystem);
		// System.out.println(infection);

	}

	private void findTarget(List<Group> attaquants, List<Group> defenseurs) {
		// on ordonne les attaquants par ordre d�croissant d'effective power, initiative
		Collections.sort(attaquants, Group.ComparatorEffPowerInit);
		Collections.reverse(attaquants);
		// recherche pour chaque attaquant de sa cible
		for (Group attaquant : attaquants) {
			int effectivePower = attaquant.getAttackDamage() * attaquant.getNb();

			Group defenseurCible = null;
			int degatsTheoriquesMax = 0;

			// boucle sur chaque defenseur non d�j� selectionn�
			for (Group defenseur : defenseurs) {
				if (!defenseur.isEstCible()) {
					int degatsTheoriques = effectivePower;
					// Gestion des weaknesses
					String[] weaknesses = defenseur.getWeaknesses();
					if (weaknesses != null) {
						for (String weak : weaknesses) {
							if (weak.equals(attaquant.getAttackType())) {
								degatsTheoriques = 2 * effectivePower;
							}
						}
					}

					// Gestion des immunities
					String[] immunites = defenseur.getImmunities();
					boolean defenseurImmunise = false;
					// on met les degats theoriques � 0 si le d�fenseurs est immunis�
					if (immunites != null) {
						for (String imm : immunites) {
							if (imm.equals(attaquant.getAttackType())) {
								defenseurImmunise = true;
								degatsTheoriques = 0;
							}
						}
					}

					// selection du defenseur avec max degats theoriques, sinon eff power, sinon initiative

					if (degatsTheoriques > degatsTheoriquesMax) {
						degatsTheoriquesMax = degatsTheoriques;
						defenseurCible = defenseur;
					}
					if (degatsTheoriques == degatsTheoriquesMax & degatsTheoriques > 0) {
						if (defenseur.getAttackDamage() * defenseur.getNb() > defenseurCible.getAttackDamage() * defenseurCible.getNb()) {
							defenseurCible = defenseur;
						}
						if (defenseur.getAttackDamage() * defenseur.getNb() == defenseurCible.getAttackDamage() * defenseurCible.getNb() & degatsTheoriques > 0) {
							if (defenseur.getInitiative() > defenseurCible.getInitiative()) {
								defenseurCible = defenseur;
							}
						}
					}

					// System.out.println(attaquant);
					// System.out.println(degatsTheoriques);
					// System.out.println("cible: "+defenseurCible);
				}
			} // boucle sur chaque defenseur

			// Maj target et estCible (peuvent �tre nuls)
			if (defenseurCible != null) {
				attaquant.setTarget(defenseurCible);
				defenseurCible.setEstCible(true);
			}

		}
	}

	private void importation(String file, List<Group> immuneSystem, List<Group> infection) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day24/day24.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		int faction = 0;
		for (String str : input) {
			if (str.equals("Immune System:")) {
				faction = 1;
			}
			if (str.equals("Infection:")) {
				faction = 2;
			}

			if (!str.equals("Immune System:") & !str.equals("Infection:") & !str.equals("")) {
				String[] weaknesses = null;
				String[] immunities = null;

				if (str.contains("(")) {
					String insert = str.substring(str.indexOf('(') + 1, str.indexOf(')'));
					String[] inserts = insert.split(";");

					for (String ins : inserts) {
						ins = ins.trim();

						if (ins.charAt(0) == 'i') { // immune
							ins = ins.substring(10);
							ins = ins.replaceAll("\\s+", "");
							immunities = ins.split(",");
						}
						if (ins.charAt(0) == 'w') { // weaknesses
							ins = ins.substring(8);
							ins = ins.replaceAll("\\s+", "");
							weaknesses = ins.split(",");
						}
					}
				}

				str = str.replaceAll("\\(.*?\\)", ".");
				String[] str_ = str.split(" ");

				System.out.println(str);
				Group group = new Group(Integer.valueOf(str_[0]), Integer.valueOf(str_[4]), Integer.valueOf(str_[13]), str_[14], Integer.valueOf(str_[18]), weaknesses, immunities);
				// System.out.println(group);
				if (faction == 1) {
					immuneSystem.add(group);
				}
				if (faction == 2) {
					infection.add(group);
				}

			}
		}
	}
}
