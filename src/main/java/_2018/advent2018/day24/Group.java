package advent2018.day24;

import java.util.Arrays;
import java.util.Comparator;

public class Group {
	
	private int nb;
	private int hp;
	public Group getTarget() {
		return target;
	}

	public void setTarget(Group target) {
		this.target = target;
	}

	public boolean isEstCible() {
		return estCible;
	}

	public void setEstCible(boolean estCible) {
		this.estCible = estCible;
	}

	private int attackDamage;
	private String attackType;
	private int initiative ;
	private String[] weaknesses;
	private Group target;
	private boolean estCible;
	
	public String toString() {
		return "Group [nb=" + nb + ", hp=" + hp + ", attackDamage=" + attackDamage + ", attackType=" + attackType
				+ ", initiative=" + initiative + ", weaknesses=" + Arrays.toString(weaknesses) + ", immunities="
				+ Arrays.toString(immunities) + "]";
	}
	public String toString2() {
		return "[nb=" + nb + "]";
	}

	private String[] immunities;
	
	public int getNb() {
		return nb;
	}

	public void setNb(int nb) {
		this.nb = nb;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttackDamage() {
		return attackDamage;
	}

	public void setAttackDamage(int attackDamage) {
		this.attackDamage = attackDamage;
	}

	public String getAttackType() {
		return attackType;
	}

	public void setAttackType(String attackType) {
		this.attackType = attackType;
	}

	public int getInitiative() {
		return initiative;
	}

	public void setInitiative(int initiative) {
		this.initiative = initiative;
	}

	public String[] getWeaknesses() {
		return weaknesses;
	}

	public void setWeaknesses(String[] weaknesses) {
		this.weaknesses = weaknesses;
	}

	public String[] getImmunities() {
		return immunities;
	}

	public void setImmunities(String[] immunities) {
		this.immunities = immunities;
	}

	public Group(int nb, int hp, int attackDamage, String attackType, int initiative, String[] weaknesses,
			String[] immunities) {
		super();
		this.nb = nb;
		this.hp = hp;
		this.attackDamage = attackDamage;
		this.attackType = attackType;
		this.initiative = initiative;
		this.weaknesses = weaknesses;
		this.immunities = immunities;
		this.estCible=false;
	}

	
	public static Comparator<Group> ComparatorEffPowerInit = new Comparator<Group>() {  
   		@Override
		public int compare(Group o1, Group o2) {
   			int res=0;
   			int x=(o1.getAttackDamage()*o1.getNb())-(o2.getAttackDamage()*o2.getNb());
   			if (x!=0) {	
   				res= x;}
   			if (x==0) {
   				int y= (o1.getInitiative()-o2.getInitiative());
   				res= y;
   			}
   			return res;
		}
    };
    
	public static Comparator<Group> ComparatorInit = new Comparator<Group>() {  
   		@Override
		public int compare(Group o1, Group o2) {

   			return (o1.getInitiative()-o2.getInitiative());
		}
    };

}
