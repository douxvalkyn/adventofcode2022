package advent2018.day14;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class Day14 {
	//constructeur ----
	public Day14() {	
	}



	// main ----
	public static void main(String[] args) throws FileNotFoundException {     
		Day14 d = new Day14();
		double startTime = System.currentTimeMillis();
		d.run2();
		//d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}


	//Methodes ----
	private void run2() throws FileNotFoundException {
		ArrayList<Integer> liste = new ArrayList<Integer>();
		liste.add(3);
		liste.add(7);
		//System.out.println(liste);
		int elfe1= 0, elfe2=1;
		int input=846021; //param�re � modifier
		int nb_iter=100000000;

		for (int i=0; i<nb_iter; i++) {
			// calcul du score de la nouvelle recette
			int nouvelleRecette=liste.get(elfe1)+liste.get(elfe2);
			if (nouvelleRecette>9) {
				int a = nouvelleRecette%10;	int b = nouvelleRecette/10;
				liste.add(b);	liste.add(a);
			}else {			liste.add(nouvelleRecette);		}

			// d�placement des elfes
			elfe1=elfe1+liste.get(elfe1)+1;
			elfe2=elfe2+liste.get(elfe2)+1;
			//System.out.println(liste);
			elfe1=elfe1%liste.size();
			elfe2=elfe2%liste.size();
		}



		// Recherche score
		for (int i=0; i<nb_iter; i++) {

			double res = 0;
			int exp=5;
			for (int j=i; j<i+6; j++) {
				res=res+liste.get(j)* Math.pow(10,exp);
				exp=exp-1;
			}

			DecimalFormat decimalPrintFormat = new DecimalFormat("##0.####");

			if (res==input) {System.out.println("Score: "+i+" : " +decimalPrintFormat.format(res));}

		}
	}



	public void run1() throws FileNotFoundException {
		ArrayList<Integer> liste = new ArrayList<Integer>();
		liste.add(3);
		liste.add(7);
		//System.out.println(liste);
		int elfe1= 0, elfe2=1;
		int nb_recettes=846021; //param�re � modifier

		for (int i=0; i<(nb_recettes*2); i++) {
			// calcul du score de la nouvelle recette
			int nouvelleRecette=liste.get(elfe1)+liste.get(elfe2);
			if (nouvelleRecette>9) {
				int a = nouvelleRecette%10;	int b = nouvelleRecette/10;
				liste.add(b);	liste.add(a);
			}else {			liste.add(nouvelleRecette);		}

			// d�placement des elfes
			elfe1=elfe1+liste.get(elfe1)+1;
			elfe2=elfe2+liste.get(elfe2)+1;
			//System.out.println(liste);
			elfe1=elfe1%liste.size();
			elfe2=elfe2%liste.size();
		}

		// lecture du score
		double res = 0;
		int exp=9;
		for (int j=nb_recettes; j<nb_recettes+10; j++) {
			res=res+liste.get(j)* Math.pow(10,exp);
			exp=exp-1;
		}

		DecimalFormat decimalPrintFormat = new DecimalFormat("##0.####");
		System.out.println("Score: " +decimalPrintFormat.format(res));

	}


}