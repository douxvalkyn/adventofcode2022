package advent2018.day16;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Day16 {
	// constructeur ----
	public Day16() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day16 d = new Day16();
		double startTime = System.currentTimeMillis();
		d.run1();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");

	}

	// Methodes ----

	public  void run1() throws ParseException, IOException {
		Day16 d = new Day16();
		String file = "day16", file2 = "day16bis";
		d.run1_(file);
	}
	public  void run2() throws ParseException, IOException {
		Day16 d = new Day16();
		String file = "day16", file2 = "day16bis";
		d.run2_(file, file2);
	}
	
	private void run2_(String file, String file2) throws FileNotFoundException {

		// Assigner le numero opcode au nom de la fonction opcode
		ArrayList<ArrayList<Integer>> liste = new ArrayList<>();
		importation(file, liste);
		Map<Integer, String> instructions = new HashMap<Integer, String>(); // key= numero opcode / value= nom opcode
		recherche(file, liste, instructions);
		System.out.println("liste finale : " + instructions);

		// Appliquer les instructions opcode de l'input au register initial 0 0 0 0:
		ArrayList<ArrayList<Integer>> liste2 = new ArrayList<>();
		importation2(file2, liste2); // liste2 contient la liste des instructions
		List<Integer> reg = new ArrayList<>();
		reg.add(0);
		reg.add(0);
		reg.add(0);
		reg.add(0);

		// les noms des fonctions reli�s aux num�ros sont obtenus en lisant "instructions"
		for (int i = 0; i < liste2.size(); i++) {
			ArrayList<Integer> instr = liste2.get(i);
			Integer opcode = instr.get(0);
			List<Integer> res = null;
			if (opcode == 0) {
				res = eqri(reg, instr);
			}
			if (opcode == 1) {
				res = bori(reg, instr);
			}
			if (opcode == 2) {
				res = addi(reg, instr);
			}
			if (opcode == 3) {
				res = bani(reg, instr);
			}
			if (opcode == 4) {
				res = seti(reg, instr);
			}
			if (opcode == 5) {
				res = eqrr(reg, instr);
			}
			if (opcode == 6) {
				res = addr(reg, instr);
			}
			if (opcode == 7) {
				res = gtri(reg, instr);
			}
			if (opcode == 8) {
				res = borr(reg, instr);
			}
			if (opcode == 9) {
				res = gtir(reg, instr);
			}
			if (opcode == 10) {
				res = setr(reg, instr);
			}
			if (opcode == 11) {
				res = eqir(reg, instr);
			}
			if (opcode == 12) {
				res = mulr(reg, instr);
			}
			if (opcode == 13) {
				res = muli(reg, instr);
			}
			if (opcode == 14) {
				res = gtrr(reg, instr);
			}
			if (opcode == 15) {
				res = banr(reg, instr);
			}
			reg = res;
		}
		System.out.println("resultat: " + reg);
	}

	private void recherche(String file, ArrayList<ArrayList<Integer>> liste, Map<Integer, String> instructions) {
		int j = -1;
		while (instructions.size() < 16) {
			j++;
			ArrayList<String> listeOpcodesSample = new ArrayList<>(); // liste des opcodes valides pour le sample en cours

			ArrayList<Integer> sample = liste.get(j);
			// donnees: R0, R1, R2, R3, opcode, A, B, C, R0_, R1_, R2_, R3_
			List<Integer> before = sample.subList(0, 4);
			List<Integer> after = sample.subList(8, 12);
			List<Integer> instruction = sample.subList(4, 8);

			boolean verif_addi = true, verif_addr = true, verif_mulr = true, verif_muli = true, verif_banr = true, verif_bani = true, verif_borr = true, verif_bori = true, verif_setr = true,
				verif_seti = true, verif_gtir = true, verif_gtri = true, verif_gtrr = true, verif_eqir = true, verif_eqri = true, verif_eqrr = true;
			List<Integer> list_addi = addi(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_addi = verif_addi & (list_addi.get(i) == after.get(i));
			}
			if (verif_addi) {
				listeOpcodesSample.add("addi");
			}

			List<Integer> list_addr = addr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_addr = verif_addr & (list_addr.get(i) == after.get(i));
			}
			if (verif_addr) {
				listeOpcodesSample.add("addr");
			}

			List<Integer> list_mulr = mulr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_mulr = verif_mulr & (list_mulr.get(i) == after.get(i));
			}
			if (verif_mulr) {
				listeOpcodesSample.add("mulr");
			}

			List<Integer> list_muli = muli(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_muli = verif_muli & (list_muli.get(i) == after.get(i));
			}
			if (verif_muli) {
				listeOpcodesSample.add("muli");
			}

			List<Integer> list_banr = banr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_banr = verif_banr & (list_banr.get(i) == after.get(i));
			}
			if (verif_banr) {
				listeOpcodesSample.add("banr");
			}

			List<Integer> list_bani = bani(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_bani = verif_bani & (list_bani.get(i) == after.get(i));
			}
			if (verif_bani) {
				listeOpcodesSample.add("bani");
			}

			List<Integer> list_borr = borr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_borr = verif_borr & (list_borr.get(i) == after.get(i));
			}
			if (verif_borr) {
				listeOpcodesSample.add("borr");
			}

			List<Integer> list_bori = bori(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_bori = verif_bori & (list_bori.get(i) == after.get(i));
			}
			if (verif_bori) {
				listeOpcodesSample.add("bori");
			}

			List<Integer> list_setr = setr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_setr = verif_setr & (list_setr.get(i) == after.get(i));
			}
			if (verif_setr) {
				listeOpcodesSample.add("setr");
			}

			List<Integer> list_seti = seti(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_seti = verif_seti & (list_seti.get(i) == after.get(i));
			}
			if (verif_seti) {
				listeOpcodesSample.add("seti");
			}

			List<Integer> list_gtir = gtir(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtir = verif_gtir & (list_gtir.get(i) == after.get(i));
			}
			if (verif_gtir) {
				listeOpcodesSample.add("gtir");
			}

			List<Integer> list_gtri = gtri(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtri = verif_gtri & (list_gtri.get(i) == after.get(i));
			}
			if (verif_gtri) {
				listeOpcodesSample.add("gtri");
			}

			List<Integer> list_gtrr = gtrr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtrr = verif_gtrr & (list_gtrr.get(i) == after.get(i));
			}
			if (verif_gtrr) {
				listeOpcodesSample.add("gtrr");
			}

			List<Integer> list_eqir = eqir(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqir = verif_eqir & (list_eqir.get(i) == after.get(i));
			}
			if (verif_eqir) {
				listeOpcodesSample.add("eqir");
			}

			List<Integer> list_eqri = eqri(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqri = verif_eqri & (list_eqri.get(i) == after.get(i));
			}
			if (verif_eqri) {
				listeOpcodesSample.add("eqri");
			}

			List<Integer> list_eqrr = eqrr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqrr = verif_eqrr & (list_eqrr.get(i) == after.get(i));
			}
			if (verif_eqrr) {
				listeOpcodesSample.add("eqrr");
			}

			listeOpcodesSample.removeAll(instructions.values());
			if (listeOpcodesSample.size() == 1) {
				instructions.put(instruction.get(0), listeOpcodesSample.get(0));
			}

		}

	}

	private void run1_(String file) throws FileNotFoundException {
		ArrayList<ArrayList<Integer>> liste = new ArrayList<>();
		importation(file, liste);
		// System.out.println(liste);
		int cpt = 0;

		for (int j = 0; j < liste.size(); j++) {

			ArrayList<Integer> sample = liste.get(j);
			// donnees: R0, R1, R2, R3, opcode, A, B, C, R0_, R1_, R2_, R3_
			List<Integer> before = sample.subList(0, 4);
			List<Integer> after = sample.subList(8, 12);
			List<Integer> instruction = sample.subList(4, 8);

			int nb = 0;

			boolean verif_addi = true;
			List<Integer> list_addi = addi(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_addi = verif_addi & (list_addi.get(i) == after.get(i));
			}
			int verif_addi_int = verif_addi ? 1 : 0;// System.out.println(list_addi);

			boolean verif_addr = true;
			List<Integer> list_addr = addr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_addr = verif_addr & (list_addr.get(i) == after.get(i));
			}
			int verif_addr_int = verif_addr ? 1 : 0;// System.out.println(list_addr);

			boolean verif_mulr = true;
			List<Integer> list_mulr = mulr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_mulr = verif_mulr & (list_mulr.get(i) == after.get(i));
			}
			int verif_mulr_int = verif_mulr ? 1 : 0;// System.out.println(list_mulr);

			boolean verif_muli = true;
			List<Integer> list_muli = muli(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_muli = verif_muli & (list_muli.get(i) == after.get(i));
			}
			int verif_muli_int = verif_muli ? 1 : 0;// System.out.println(list_muli);

			boolean verif_banr = true;
			List<Integer> list_banr = banr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_banr = verif_banr & (list_banr.get(i) == after.get(i));
			}
			int verif_banr_int = verif_banr ? 1 : 0;// System.out.println(list_banr);

			boolean verif_bani = true;
			List<Integer> list_bani = bani(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_bani = verif_bani & (list_bani.get(i) == after.get(i));
			}
			int verif_bani_int = verif_bani ? 1 : 0;// System.out.println(list_bani);

			boolean verif_borr = true;
			List<Integer> list_borr = borr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_borr = verif_borr & (list_borr.get(i) == after.get(i));
			}
			int verif_borr_int = verif_borr ? 1 : 0;// System.out.println(list_borr);

			boolean verif_bori = true;
			List<Integer> list_bori = bori(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_bori = verif_bori & (list_bori.get(i) == after.get(i));
			}
			int verif_bori_int = verif_bori ? 1 : 0;// System.out.println(list_bori);

			boolean verif_setr = true;
			List<Integer> list_setr = setr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_setr = verif_setr & (list_setr.get(i) == after.get(i));
			}
			int verif_setr_int = verif_setr ? 1 : 0;// System.out.println(list_setr);

			boolean verif_seti = true;
			List<Integer> list_seti = seti(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_seti = verif_seti & (list_seti.get(i) == after.get(i));
			}
			int verif_seti_int = verif_seti ? 1 : 0;// System.out.println("seti"+list_seti);

			boolean verif_gtir = true;
			List<Integer> list_gtir = gtir(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtir = verif_gtir & (list_gtir.get(i) == after.get(i));
			}
			int verif_gtir_int = verif_gtir ? 1 : 0;// System.out.println(list_gtir);

			boolean verif_gtri = true;
			List<Integer> list_gtri = gtri(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtri = verif_gtri & (list_gtri.get(i) == after.get(i));
			}
			int verif_gtri_int = verif_gtri ? 1 : 0;// System.out.println(list_gtri);

			boolean verif_gtrr = true;
			List<Integer> list_gtrr = gtrr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_gtrr = verif_gtrr & (list_gtrr.get(i) == after.get(i));
			}
			int verif_gtrr_int = verif_gtrr ? 1 : 0;// System.out.println("list_gtrr"+list_gtrr);

			boolean verif_eqir = true;
			List<Integer> list_eqir = eqir(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqir = verif_eqir & (list_eqir.get(i) == after.get(i));
			}
			int verif_eqir_int = verif_eqir ? 1 : 0;// System.out.println(list_eqir);

			boolean verif_eqri = true;
			List<Integer> list_eqri = eqri(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqri = verif_eqri & (list_eqri.get(i) == after.get(i));
			}
			int verif_eqri_int = verif_eqri ? 1 : 0;// System.out.println(list_eqri);

			boolean verif_eqrr = true;
			List<Integer> list_eqrr = eqrr(before, instruction);
			for (int i = 0; i < 4; i++) {
				verif_eqrr = verif_eqrr & (list_eqrr.get(i) == after.get(i));
			}
			int verif_eqrr_int = verif_eqrr ? 1 : 0;// System.out.println(list_eqrr);

			//
			nb = verif_addi_int + verif_addr_int + verif_muli_int + verif_mulr_int + verif_bani_int + verif_banr_int + verif_bori_int + verif_borr_int + +verif_seti_int + verif_setr_int
				+ verif_gtir_int + verif_gtri_int + verif_gtrr_int + verif_eqir_int + verif_eqri_int + verif_eqrr_int;

			if (nb >= 3) {
				cpt++;
			}

			// System.out.println("nb: "+nb);

		}
		System.out.println("cpt: " + cpt);

	}

	// before (R0, R1, R2, R3); renvoie (R0_, R1_, R2_, R3_) apres application de l'instruction (opcode, A, B, C)

	// add register
	private List<Integer> addr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) + register.get(instruction.get(2));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// add immediate
	private List<Integer> addi(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) + instruction.get(2);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// mul register
	public List<Integer> mulr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) * register.get(instruction.get(2));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// mul immediate
	public List<Integer> muli(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) * instruction.get(2);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// bitwise and register
	public List<Integer> banr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) & register.get(instruction.get(2));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// bitwise and immediate
	public List<Integer> bani(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) & instruction.get(2);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// bitwise or register
	public List<Integer> borr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) | register.get(instruction.get(2));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// biwise or immediate
	public List<Integer> bori(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1)) | instruction.get(2);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// set register
	public List<Integer> setr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// set immediate
	public List<Integer> seti(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// greater-than immediate/register
	public List<Integer> gtir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (instruction.get(1) > register.get(instruction.get(2))) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// greater-than register/immediate
	public List<Integer> gtri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (register.get(instruction.get(1)) > instruction.get(2)) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// greater-than register/register
	public List<Integer> gtrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (register.get(instruction.get(1)) > register.get(instruction.get(2))) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// equal immediate/register
	public List<Integer> eqir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (instruction.get(1) == register.get(instruction.get(2))) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// equal register/immediate
	public List<Integer> eqri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (register.get(instruction.get(1)) == instruction.get(2)) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	// equal register/register
	public List<Integer> eqrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		if (register.get(instruction.get(1)) == register.get(instruction.get(2))) {
			res = 1;
		}
		liste.set((instruction.get(3)), res);
		return liste;
	}

	public void importation2(String file2, ArrayList<ArrayList<Integer>> liste2) throws FileNotFoundException {
		// Import input
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day16/day16bis.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// transformer string en liste de int
		for (String str : input) {
			ArrayList<Integer> inputIntLigne = new ArrayList<>();
			String[] splited = str.split(" ");
			inputIntLigne.add(Integer.parseInt(splited[0]));
			inputIntLigne.add(Integer.parseInt(splited[1]));
			inputIntLigne.add(Integer.parseInt(splited[2]));
			inputIntLigne.add(Integer.parseInt(splited[3]));
			liste2.add(inputIntLigne);
		}

	}

	public void importation(String file, ArrayList<ArrayList<Integer>> liste) throws FileNotFoundException {
		// Import input
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day16/day16.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		// premi�re ligne: register
		for (int i = 1; i <= ((input.size() + 1) / 4); i++) {

			// registers before
			ArrayList<Integer> sample = new ArrayList<>();
			sample.add(Character.getNumericValue(input.get(4 * i - 4).charAt(9)));
			sample.add(Character.getNumericValue(input.get(4 * i - 4).charAt(12)));
			sample.add(Character.getNumericValue(input.get(4 * i - 4).charAt(15)));
			sample.add(Character.getNumericValue(input.get(4 * i - 4).charAt(18)));

			// instructions
			String[] instruction = input.get(4 * i - 3).split(" ");
			sample.add(Integer.parseInt(instruction[0]));
			sample.add(Integer.parseInt(instruction[1]));
			sample.add(Integer.parseInt(instruction[2]));
			sample.add(Integer.parseInt(instruction[3]));

			// registers after
			sample.add(Character.getNumericValue(input.get(4 * i - 2).charAt(9)));
			sample.add(Character.getNumericValue(input.get(4 * i - 2).charAt(12)));
			sample.add(Character.getNumericValue(input.get(4 * i - 2).charAt(15)));
			sample.add(Character.getNumericValue(input.get(4 * i - 2).charAt(18)));

			liste.add(sample);
		}

	}

}