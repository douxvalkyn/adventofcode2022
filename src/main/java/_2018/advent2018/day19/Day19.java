package advent2018.day19;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day19 {

	// constructeur ----
	public Day19() {
	}

	// main ----
	public static void main(String[] args) throws FileNotFoundException {
		Day19 d = new Day19();
		double startTime = System.currentTimeMillis();
		String file = "day19";
		// d.run2(file); //trop long !
		// d.run1(file);
		d.run2bis(file);
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----

	private void run2bis(String file) {
		// L'algo correspondant (somme des diviseurs d'un grand nombre: 10 551 354) est le suivant:
		// R0=R1=R3=R4=R5=0;
		// R2=10 551 354;
		// R4=1
		// R5=1
		// Tant que (R4 <= R2) {
		// Tant que (R5<= R2) {
		// Si (R4xR5==R2){
		// RO= R0+R4
		// }
		// R5++
		// }
		// R4++

		// ! Attention � utiliser des long et pas des int
		// sinon les calculs sur les gros int seront faux !
		long R0 = 0;
		long R1 = 0, R2 = 10551354, R3 = 0, R4 = 0, R5 = 0;

		while (R4 <= R2) {
			R5 = 1;
			while (R5 <= R2) {

				if ((R4 * R5) == R2) {
					R0 = R0 + R4;

				}
				R5++;
			}
			R4++;
		}
		System.out.println(R0);

	}

	private void run2(String file) throws FileNotFoundException {

		List<List<String>> instructions = importation(file);
		int bound = importationBound(file);

		int IP = 0;
		List<Integer> register = new ArrayList<>();
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);
		// modif run2: reg0 =1 au lieu de 0
		register.set(0, 1);

		long comptage = 0;
		for (long i = 0; i < 200000000; i++) {
			// while (IP<instructions.size()) {
			comptage++;
			// System.out.println("comptage: "+ (comptage-1));
			register.set(bound, IP); // BEFORE: on charge la valeur de IP dans le registre(bound)
			List<String> instruction = instructions.get(IP);// INSTR: on applique l'instruction IP
			List<Integer> instructionInt = new ArrayList<>();
			instructionInt.add(Integer.parseInt(instruction.get(1)));
			instructionInt.add(Integer.parseInt(instruction.get(2)));
			instructionInt.add(Integer.parseInt(instruction.get(3)));
			// System.out.println("instr: "+instructionInt);
			// System.out.println(instruction.get(0));
			List<Integer> res = new ArrayList<Integer>();
			switch (instruction.get(0)) {

				case "addr":
					res = addr(register, instructionInt);
					break;

				case "addi":
					res = addi(register, instructionInt);
					break;

				case "mulr":
					res = mulr(register, instructionInt);
					break;

				case "muli":
					res = muli(register, instructionInt);
					break;

				case "banr":
					res = banr(register, instructionInt);
					break;

				case "bani":
					res = bani(register, instructionInt);
					break;

				case "borr":
					res = borr(register, instructionInt);
					break;

				case "bori":
					res = bori(register, instructionInt);
					break;

				case "setr":
					res = setr(register, instructionInt);
					break;

				case "seti":
					res = seti(register, instructionInt);
					break;

				case "gtir":
					res = gtir(register, instructionInt);
					break;

				case "gtri":
					res = gtri(register, instructionInt);
					break;

				case "gtrr":
					res = gtrr(register, instructionInt);
					break;

				case "eqri":
					res = eqri(register, instructionInt);
					break;

				case "eqir":
					res = eqir(register, instructionInt);
					break;

				case "eqrr":
					res = eqrr(register, instructionInt);
					break;

				default:
					System.out.println("Choix incorrect");
					break;
			}

			register = res;
			IP = register.get(bound); // AFTER: on remet dans IP la valeur du registre(bound)
			System.out.println("register" + register);
			IP++; // IP+1
			// System.out.println("IP: "+(IP));
			if (comptage % 1000000000 == 0) {
				System.out.println("top: " + comptage / 1000000000 + "milliards");
				System.out.println("register" + register);
			}
		}
		System.out.println("register" + register);
		System.out.println("comptage: " + (comptage - 1));

	}

	private void run1(String file) throws FileNotFoundException {

		List<List<String>> instructions = importation(file);
		int bound = importationBound(file);
		System.out.println("bound" + bound);
		int IP = 0;
		List<Integer> register = new ArrayList<>();
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);
		register.add(0);

		int comptage = 0;
		// for (int i=0;i<7651;i++) {
		while (IP < instructions.size()) {
			comptage++;
			System.out.println("comptage: " + (comptage - 1));
			register.set(bound, IP); // BEFORE: on charge la valeur de IP dans le registre(bound)
			List<String> instruction = instructions.get(IP);// INSTR: on applique l'instruction IP
			List<Integer> instructionInt = new ArrayList<>();
			instructionInt.add(Integer.parseInt(instruction.get(1)));
			instructionInt.add(Integer.parseInt(instruction.get(2)));
			instructionInt.add(Integer.parseInt(instruction.get(3)));
			System.out.println("instr: " + instructionInt);
			List<Integer> res = new ArrayList<Integer>();
			switch (instruction.get(0)) {

				case "addr":
					res = addr(register, instructionInt);
					break;

				case "addi":
					res = addi(register, instructionInt);
					break;

				case "mulr":
					res = mulr(register, instructionInt);
					break;

				case "muli":
					res = muli(register, instructionInt);
					break;

				case "banr":
					res = banr(register, instructionInt);
					break;

				case "bani":
					res = bani(register, instructionInt);
					break;

				case "borr":
					res = borr(register, instructionInt);
					break;

				case "bori":
					res = bori(register, instructionInt);
					break;

				case "setr":
					res = setr(register, instructionInt);
					break;

				case "seti":
					res = seti(register, instructionInt);
					break;

				case "gtir":
					res = gtir(register, instructionInt);
					break;

				case "gtri":
					res = gtri(register, instructionInt);
					break;

				case "gtrr":
					res = gtrr(register, instructionInt);
					break;

				case "eqri":
					res = eqri(register, instructionInt);
					break;

				case "eqir":
					res = eqir(register, instructionInt);
					break;

				case "eqrr":
					res = eqrr(register, instructionInt);
					break;

				default:
					System.out.println("Choix incorrect");
					break;
			}

			register = res;
			IP = register.get(bound); // AFTER: on remet dans IP la valeur du registre(bound)
			System.out.println("register" + register);
			IP++; // IP+1
			System.out.println("IP: " + (IP));
		}
	}

	private List<Integer> addr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) + register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// add immediate
	private List<Integer> addi(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) + instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// mul register
	public List<Integer> mulr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) * register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// mul immediate
	public List<Integer> muli(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) * instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise and register
	public List<Integer> banr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) & register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise and immediate
	public List<Integer> bani(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) & instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// bitwise or register
	public List<Integer> borr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) | register.get(instruction.get(1));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// biwise or immediate
	public List<Integer> bori(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0)) | instruction.get(1);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// set register
	public List<Integer> setr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = register.get(instruction.get(0));
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// set immediate
	public List<Integer> seti(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = instruction.get(0);
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than immediate/register
	public List<Integer> gtir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (instruction.get(0) > register.get(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than register/immediate
	public List<Integer> gtri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)) > instruction.get(1)) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// greater-than register/register
	public List<Integer> gtrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)) > register.get(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal immediate/register
	public List<Integer> eqir(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (instruction.get(0).equals(register.get(instruction.get(1)))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal register/immediate
	public List<Integer> eqri(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)).equals(instruction.get(1))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	// equal register/register
	public List<Integer> eqrr(List<Integer> register, List<Integer> instruction) {
		List<Integer> liste = new ArrayList<>();
		int res = 0;
		liste.add(0, register.get(0));
		liste.add(1, register.get(1));
		liste.add(2, register.get(2));
		liste.add(3, register.get(3));
		liste.add(4, register.get(4));
		liste.add(5, register.get(5));
		if (register.get(instruction.get(0)).equals(register.get(instruction.get(1)))) {
			res = 1;
		}
		liste.set((instruction.get(2)), res);
		return liste;
	}

	public List<List<String>> importation(String file) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day19/day19.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println(input);

		List<List<String>> instructions = new ArrayList<>();
		input.remove(0);
		for (String str : input) {
			List<String> instruction = new ArrayList<>();
			String[] splited = str.split(" ");
			String instr = splited[0];
			String A = (splited[1]);
			String B = (splited[2]);
			String C = (splited[3]);
			instruction.add(instr);
			instruction.add(A);
			instruction.add(B);
			instruction.add(C);
			instructions.add(instruction);
		}
		return instructions;
	}

	public int importationBound(String file) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day19/day19.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		int bound = Character.getNumericValue(input.get(0).charAt(4));
		return bound;
	}

}