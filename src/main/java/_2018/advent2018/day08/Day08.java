package advent2018.day08;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Day08 {

	// constructeur ----
	public Day08() {
	}

	// main ----
	public static void main(String[] args) throws FileNotFoundException {
		Day08 d = new Day08();
		double startTime = System.currentTimeMillis();
		d.run2();
		d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	public void run1() throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day08/day8.txt"));
		ArrayList<Integer> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextInt());
		}
		System.out.println(input);

		// creation d'une liste des des metadonnees
		ArrayList<Integer> metadonnees = new ArrayList<>();

		while (input.size() > 0) {
			System.out.println(input.size());

			// recherche indice du premier header1 nul [on a alors trouv� un noeud final]
			int indice = 0;
			while (input.get(indice) != 0) {
				indice = indice + 1;
			}
			System.out.println("indice header nul: " + indice);
			int nb_metadonnees = input.get(indice + 1);
			// on ajoute les metadonnees dans la liste ad-hoc
			for (int j = nb_metadonnees + indice + 1; j > indice + 1; j--) {
				metadonnees.add(input.get(j));
			}
			// on supprimer ce noeud final de l'input (header+meta) et
			for (int j = nb_metadonnees + indice + 1; j > indice - 1; j--) {
				input.remove(j);
			}
			// on diminue de 1 le nombre de branches du noeud parent (sauf si on est au premier noeud qui n'a pas de parent)
			if (indice != 0) {
				input.set(indice - 2, input.get(indice - 2) - 1);
			}
			System.out.println(input);

		}
		System.out.println(metadonnees);
		int sum = metadonnees.stream().mapToInt(Integer::intValue).sum();

		System.out.println(sum);

	}

	public void run2() throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day08/day8.txt"));
		ArrayList<Integer> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextInt());
		}
		System.out.println(input);

		// creation d'un tableau des valeurs des metadonnees des branches supprim�es
		Map<Integer, List<Integer>> meta = new HashMap<Integer, List<Integer>>();

		while (input.size() > 0) { // boucle principale

			// recherche indice du premier header1 nul
			int indice = 0;
			while (input.get(indice) != 0) {
				indice = indice + 1;
			}

			/// Calcul des metadonnees
			int nb_metadonnees = input.get(indice + 1);
			int somme_meta = 0;
			if (meta.get(indice) == null) { // si cet indice n'existe pas dans la liste des metadonnees

				for (int j = nb_metadonnees + indice + 1; j > indice + 1; j--) {
					somme_meta = somme_meta + input.get(j);
				}
				ArrayList<Integer> l = new ArrayList<>();
				l.add(somme_meta);
				if (meta.containsKey(indice - 2)) {
					meta.get(indice - 2).addAll(l);
				} else {
					meta.put(indice - 2, l);
				}

			} else { // si cet indice existe d�j� dans la liste des metadonnees: calcul diff�rent car il existe au moins 1 branche pour ce noeud
				for (int j = nb_metadonnees + indice + 1; j > indice + 1; j--) {
					int nb_enfants = meta.get(indice).size();
					if (input.get(j) <= nb_enfants) {
						somme_meta = somme_meta + meta.get(indice).get(input.get(j) - 1);
					}
				}
				ArrayList<Integer> l = new ArrayList<>();
				l.add(somme_meta);
				if (indice != 0) {
					if (meta.containsKey(indice - 2)) {
						meta.get(indice - 2).addAll(l);
					} else {
						meta.put(indice - 2, l);
					}

				} else {
					System.out.println("Resultat: " + somme_meta);
				}
				meta.remove(indice);
			}

			// System.out.println(meta);

			// on supprimer ce noeud final de l'input (header+meta)
			for (int j = nb_metadonnees + indice + 1; j > indice - 1; j--) {
				input.remove(j);
			}
			// on diminue de 1 le nombre de branches du noeud parent (sauf si on est au premier noeud qui n'a pas de parent)
			if (indice != 0) {
				input.set(indice - 2, input.get(indice - 2) - 1);
			}
			// System.out.println(input);

		} // fin boucle principale
	}// fin run2()

}
