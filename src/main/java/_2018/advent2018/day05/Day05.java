package advent2018.day05;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day05 {

	// constructeur
	public Day05() {

	}

	// main
	public static void main(String[] args) throws FileNotFoundException {
		Day05 d = new Day05();
		double startTime = System.currentTimeMillis();
		d.run2();
		// d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// M�thodes ----------------------------------------------------------------------------------------------------------
	// run1
	public void run1() throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day05/day5.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		for (String s : input) {
			System.out.println(s);
			boolean GO = true;
			while (GO) {
				System.out.println("longueur avant: " + s.length());
				int longueur_avant = s.length();
				for (int i = 0; i < s.length() - 1; i++) {
					if (Character.toUpperCase(s.charAt(i)) == Character.toUpperCase(s.charAt(i + 1))) {
						if ((Character.isLowerCase(s.charAt(i)) && Character.isUpperCase(s.charAt(i + 1))) || (Character.isUpperCase(s.charAt(i)) && Character.isLowerCase(s.charAt(i + 1)))) {
							s = s.substring(0, i) + s.substring(i + 2);
						}
					}
				}
				System.out.println(s);
				System.out.println("longueur apres: " + s.length());
				int longueur_apres = s.length();
				if (longueur_avant == longueur_apres) {
					GO = false;
				}
			}
		}
	}

	public void run2() throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day05/day5.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		for (String string_initial : input) { // il n'y aura en fait qu'un seul string_initial

			int longueur_apres = 0;
			ArrayList<Integer> resultat = new ArrayList<>();

			for (char char_min = 'a'; char_min <= 'z'; char_min++) {
				char char_maj = Character.toUpperCase(char_min);
				String str_min = char_min + "";// convertir char en String
				String str_maj = char_maj + "";// convertir char en String
				String s = this.removeAllLetters(string_initial, str_min, str_maj);

				boolean GO = true;
				while (GO) {
					int longueur_avant = s.length();
					for (int i = 0; i < s.length() - 1; i++) {

						if (Character.toUpperCase(s.charAt(i)) == Character.toUpperCase(s.charAt(i + 1))) {
							if ((Character.isLowerCase(s.charAt(i)) && Character.isUpperCase(s.charAt(i + 1))) || (Character.isUpperCase(s.charAt(i)) && Character.isLowerCase(s.charAt(i + 1)))) {
								s = s.substring(0, i) + s.substring(i + 2);
							}
						}
					}
					longueur_apres = s.length();
					if (longueur_avant == longueur_apres) {
						GO = false;
					}
				}
				System.out.println(char_min + ":" + longueur_apres);
				resultat.add(longueur_apres);
			} // fin boucle sur les lettres
			System.out.println("valeur minimale:" + Collections.min(resultat));
		}
	}

	public String removeAllLetters(String str, String c1, String c2) {
		str = str.replaceAll(c1, "");
		str = str.replaceAll(c2, "");
		return str;
	}

}
