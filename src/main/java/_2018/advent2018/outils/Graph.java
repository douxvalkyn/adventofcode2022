package advent2018.outils;
//JAVA program to print all paths from a source to destination.
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

//A directed graph using adjacency list representation
public class Graph {

	// No. of vertices in graph
	private int v;

	// adjacency list
	private ArrayList<Integer>[] adjList;

	// Constructor
	public Graph(int vertices)
	{

		// initialise vertex count
		this.v = vertices;

		// initialise adjacency list
		initAdjList();
	}

	// utility method to initialise adjacency list
	@SuppressWarnings("unchecked")
	private void initAdjList()
	{
		adjList = new ArrayList[v];

		for (int i = 0; i < v; i++) {
			adjList[i] = new ArrayList<>();
		}
	}

	// add edge from u to v
	public void addEdge(int u, int v)
	{
		// Add v to u's list.
		adjList[u].add(v);
	}

	// Prints all paths from 's' to 'd'
	public ArrayList<Integer> printAllPaths(int s, int d)
	{
		boolean[] isVisited = new boolean[v];
		ArrayList<Integer> pathList = new ArrayList<>();
		ArrayList<Integer> listTokenFunction = new ArrayList<Integer>(); //liste r�sultat

		// add source to path[]
		pathList.add(s);
	   
		// Call recursive utility
		return(printAllPathsUtil(listTokenFunction,s, d, isVisited, pathList));
	}

	// A recursive function to print all paths from 'u' to 'd'.
	// isVisited[] keeps track of vertices in current path.
	// localPathList<> stores actual vertices in the current path
	private ArrayList<Integer> printAllPathsUtil(ArrayList<Integer> listTokenFunction, Integer u, Integer d, boolean[] isVisited, List<Integer> localPathList)
	{

		   // initialize if null
		   if(listTokenFunction == null) {
		       listTokenFunction = new ArrayList<Integer>();
		   }

		if (u.equals(d)) {
		   	//System.out.println(localPathList);
			listTokenFunction.addAll(localPathList);
			// if match found then no need to traverse more till depth
			return null;
		}

		// Mark the current node
		isVisited[u] = true;

		// Recur for all the vertices adjacent to current vertex
		for (Integer i : adjList[u]) {
			if (!isVisited[i]) {
				// store current node in path[]
				localPathList.add(i);
				printAllPathsUtil(listTokenFunction,i, d, isVisited, localPathList);

				// remove current node in path[]
				localPathList.remove(i);
			}
		}

		// Mark the current node
		isVisited[u] = false;
		
		 return listTokenFunction;
	}

	// Driver program
	public static void main(String[] args)
	{
		// Create a sample graph
		Graph g = new Graph(7);
		g.addEdge(0, 1);
		g.addEdge(1, 3);
		g.addEdge(3, 5);
		g.addEdge(1, 2);
		g.addEdge(2, 4);
		g.addEdge(4, 6);
		g.addEdge(6, 5);

		// arbitrary source
		int s = 1;

		// arbitrary destination
		int d = 5;

		//System.out.println("Following are all different paths from " + s + " to " + d);
		//System.out.println(g.printAllPaths(s, d));
		// s�paration des r�sultats en plusieurs listes
		System.out.println("paths from "+ s + " to "+d +": "+splitBySeparator(g.printAllPaths(s, d), s));
		
		
	}
	
	
    // fonction pour s�parer une liste d'entiers en sous liste selon en s�parateur
	static public List<List<Integer>> splitBySeparator (List<Integer> maListe, int sep){
	List<List<Integer>> result = new ArrayList<>();
	final List<Integer> current = new ArrayList<>();
	maListe.stream().forEach(nb -> {
	      if (nb == sep) {
	        result.add(new ArrayList<>(current));
	        current.clear();
	        current.add(sep);

	      } else {
	        current.add(nb);
	      }
	    }
	);
	result.add(current);
	result.remove(0);

	return result;
	}
	
	
}


