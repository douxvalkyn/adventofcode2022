package advent2018.outils;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;




import advent2018.day01.Day01;
import advent2018.day02.Day02;
import advent2018.day03.Day03;
import advent2018.day04.Day04;
import advent2018.day05.Day05;
import advent2018.day06.Day06;
import advent2018.day07.Day07;
import advent2018.day08.Day08;
import advent2018.day09.Day09;
import advent2018.day10.Day10;
import advent2018.day11.Day11;
import advent2018.day12.Day12;
import advent2018.day13.Day13;
import advent2018.day14.Day14;
import advent2018.day15.Day15;
import advent2018.day16.Day16;
import advent2018.day17.Day17;
import advent2018.day18.Day18;
import advent2018.day19.Day19;
import advent2018.day20.Day20;
import advent2018.day21.Day21;
import advent2018.day22.Day22;
import advent2018.day23.Day23;
import advent2018.day24.Day24;
import advent2018.day25.Day25;




public class Timings {
	
	private static final String ANSI_RESET="\u001B[0m";
	private static final String ANSI_RED="\u001B[31m";
	private static final String ANSI_GREEN="\u001B[32m";
	private static final String ANSI_BRIGHT_GREEN="\u001B[92m";
	private static final String ANSI_YELLOW="\u001B[33m";
	private static final String ANSI_PURPLE="\u001B[35m";
	private static final String ANSI_BLUE="\u001B[34m";
	private static final String ANSI_BRIGHT_MAGENTA="\u001B[95m";
	private static final String  YELLOW_BRIGHT = "\033[0;93m";
	
	public static List<Long> dureesRun1;
	public static List<Long> dureesRun2;
	


	public static void main(String[] args) throws IOException, ParseException {
		List<Long> dureesRun1 = new ArrayList<Long>();
		List<Long> dureesRun2 = new ArrayList<Long>();

		//Day01
		System.out.println("----- Day 01 -----");
		Day01 day01 = new Day01();
		long start = System.currentTimeMillis();
		day01.run1();
		long duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day01.run2();
		long duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day02
		System.out.println("----- Day 02 -----");
		Day02 Day02 = new Day02();
		start = System.currentTimeMillis();
		Day02.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day02.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day03
		System.out.println("----- Day 03 -----");
		Day03 Day03 = new Day03();
		start = System.currentTimeMillis();
		Day03.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day03.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day04
		System.out.println("----- Day 04 -----");
		Day04 Day04 = new Day04();
		start = System.currentTimeMillis();
		Day04.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day04.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day05
		System.out.println("----- Day 05 -----");
		Day05 Day05 = new Day05();
		start = System.currentTimeMillis();
		Day05.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day05.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
//		//Day06
		System.out.println("----- Day 06 -----");
		Day06 Day06 = new Day06();
		start = System.currentTimeMillis();
		Day06.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day06.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day07
		System.out.println("----- Day 07 -----");
		Day07 Day07 = new Day07();
		start = System.currentTimeMillis();
		Day07.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day07.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day08
		System.out.println("----- Day 08 -----");
		Day08 Day08 = new Day08();
		start = System.currentTimeMillis();
		Day08.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day08.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day09
		System.out.println("----- Day 09 -----");
		Day09 Day09 = new Day09();
		start = System.currentTimeMillis();
		Day09.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day09.run2();
		//duree2 = System.currentTimeMillis() - start;
		duree2=999999999;
		dureesRun2.add(duree2);
	
		//Day10
		System.out.println("----- Day 10 -----");
		Day10 Day10 = new Day10();
		start = System.currentTimeMillis();
		Day10.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day10.run2();
		duree2=999999999;
		dureesRun2.add(duree2);
		
		//Day11
		System.out.println("----- Day 11 -----");
		Day11 Day11 = new Day11();
		start = System.currentTimeMillis();
//		Day11.run1();
//		duree = System.currentTimeMillis() - start;		
		duree=7594000;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
//		Day11.run2();
//		duree2 = System.currentTimeMillis() - start;
		duree2=7594000;
		dureesRun2.add(duree2);
		
		//Day12
		System.out.println("----- Day 12 -----");
		Day12 Day12 = new Day12();
		start = System.currentTimeMillis();
		Day12.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day12.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day13
		System.out.println("----- Day 13 -----");
		Day13 Day13 = new Day13();
		start = System.currentTimeMillis();
		Day13.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day13.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day14
		System.out.println("----- Day 14 -----");
		Day14 Day14 = new Day14();
		start = System.currentTimeMillis();
		Day14.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day14.run2();
		duree2 = System.currentTimeMillis() - start;
		duree2=1008714;
		dureesRun2.add(duree2);
		
		//Day15
		System.out.println("----- Day 15 -----");
		Day15 Day15 = new Day15();
		start = System.currentTimeMillis();
		Day15.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day15.run2();
		duree2 = System.currentTimeMillis() - start;
		duree2 = 1005387;
		dureesRun2.add(duree2);
		
		//Day16
		System.out.println("----- Day 16 -----");
		Day16 Day16 = new Day16();
		start = System.currentTimeMillis();
		Day16.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day16.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day17
		System.out.println("----- Day 17 -----");
		Day17 Day17 = new Day17();
		start = System.currentTimeMillis();
		Day17.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		duree2 = 999999999;
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day18
		System.out.println("----- Day 18 -----");
		Day18 Day18 = new Day18();
		start = System.currentTimeMillis();
		Day18.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		duree2 = 999999999;
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day19
		System.out.println("----- Day 19 -----");
		Day19 Day19 = new Day19();
		start = System.currentTimeMillis();
		//Day19.run1();
		//duree = System.currentTimeMillis() - start;	
		duree=32526000;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day19.run2();
		duree2=141096700;
		//duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day20
		System.out.println("----- Day 20 -----");
		Day20 Day20 = new Day20();
		start = System.currentTimeMillis();
		Day20.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		duree2=999999999;
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day21
		System.out.println("----- Day 21 -----");
		Day21 Day21 = new Day21();
		start = System.currentTimeMillis();
		Day21.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day21.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day22
		System.out.println("----- Day 22 -----");
		Day22 Day22 = new Day22();
		start = System.currentTimeMillis();
		Day22.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day22.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day23
		System.out.println("----- Day 23 -----");
		Day23 Day23 = new Day23();
		start = System.currentTimeMillis();
		Day23.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//Day23.run2();
		duree2 = System.currentTimeMillis() - start;
		//duree2=2232000;
		dureesRun2.add(duree2);
		
		//Day24
		System.out.println("----- Day 24 -----");
		Day24 Day24 = new Day24();
		start = System.currentTimeMillis();
		Day24.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		Day24.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day25
		System.out.println("----- Day 25 -----");
		Day25 Day25 = new Day25();
		start = System.currentTimeMillis();
		Day25.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		duree2=0;
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		Timings.dureesRun1=dureesRun1;
		Timings.dureesRun2=dureesRun2;
		
		
		
		List<String> DaysAffichage = new ArrayList<String>();
		DaysAffichage.add("1 ");		DaysAffichage.add("2 ");		DaysAffichage.add("3 ");		DaysAffichage.add("4 ");		DaysAffichage.add("5 ");
		DaysAffichage.add("6 ");		DaysAffichage.add("7 ");		DaysAffichage.add("8 ");		DaysAffichage.add("9 ");		DaysAffichage.add("10 ");
		DaysAffichage.add("11 ");		DaysAffichage.add("12 ");		DaysAffichage.add("13 ");		DaysAffichage.add("14 ");		DaysAffichage.add("15 ");
		DaysAffichage.add("16 ");		DaysAffichage.add("17 ");		DaysAffichage.add("18 ");		DaysAffichage.add("19 ");		DaysAffichage.add("20 ");
		DaysAffichage.add("21 ");		DaysAffichage.add("22 ");		DaysAffichage.add("23 ");		DaysAffichage.add("24 ");		DaysAffichage.add("25 ");

		System.out.println("-----------------------------------------------------------------");
		System.out.println("-----------------------"+ANSI_BLUE+"Advent of Code 2016"+ANSI_RESET+"-----------------------");
		System.out.println("-----------------------------"+ANSI_BLUE+"Timings"+ANSI_RESET+"-----------------------------");
		System.out.println("-----------------------------------------------------------------");
		affichageCouleurDays2(affichageCouleurDays1(dureesRun1, dureesRun2,0), DaysAffichage);
		DaysAffichage.forEach(System.out::print);
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		for (int i=0; i<dureesRun1.size();i++) {
			long calcul1 = dureesRun1.get(i);
			String calculString1 = ""+calcul1;
			long calcul2 = dureesRun2.get(i);
			String calculString2 = ""+calcul2;
			if (i<9) {
			System.out.println("[ Day"+(i+1)+" -  Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
			if (i>=9) {
			System.out.println("[ Day"+(i+1)+" - Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
		}
		long sum = 0;
		for (long dur : dureesRun1) {
			sum=sum+dur;
		}
		for (long dur : dureesRun2) {
			sum=sum+dur;
		}
		long sumSecondes=sum/1000;
		long minutes=sumSecondes/60;
		long heures=minutes/60;
		System.out.println("[ Total:      " + sum + " ms soit "+ sumSecondes +" s soit "+minutes + " min soit "+ heures+" h ]");
	}


	
	private static void affichageCouleurDays2(List<Integer> affichageCouleurDays, List<String> DaysAffichage) {
		for (int i=0;i<DaysAffichage.size();i++) {
			if (affichageCouleurDays.size()>i) {
			if (affichageCouleurDays.get(i)==1) {
				DaysAffichage.set(i, YELLOW_BRIGHT+DaysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==2) {
				DaysAffichage.set(i, ANSI_BRIGHT_GREEN+DaysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==0) {
				DaysAffichage.set(i, ANSI_RED+DaysAffichage.get(i)+ANSI_RESET);
			}
		}
		}
	}



	private static List<Integer> affichageCouleurDays1(List<Long> dureesRun1, List<Long> dureesRun2, int seuil) {
		List<Integer> DaysWinOrLoose = new ArrayList<Integer>();
		for (int i=0; i<dureesRun1.size();i++) {
			long calcul1 = dureesRun1.get(i);
			long calcul2 = dureesRun2.get(i);
			
			if (calcul1 >= seuil && calcul2 < seuil) {
				DaysWinOrLoose.add(1);
			}
			if (calcul1 < seuil && calcul2 >= seuil) {
				DaysWinOrLoose.add(1);
			}
			if (calcul1 >= seuil && calcul2 >= seuil) {
				DaysWinOrLoose.add(2);
			}
			if (calcul1 < seuil && calcul2 < seuil) {
				DaysWinOrLoose.add(0);
			}
			
		}
		return DaysWinOrLoose;
	}


	private static String colored(String calculString) {
	String res = "";
		if (Integer.parseInt(calculString)<1000) {
			res=ANSI_BRIGHT_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000 & Integer.parseInt(calculString)<10000) {
			res=ANSI_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=10000 & Integer.parseInt(calculString)<100000) {
			res=ANSI_YELLOW+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=100000 & Integer.parseInt(calculString)<1000000) {
			res=ANSI_RED+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000000 & Integer.parseInt(calculString)<10000000) {
			res=ANSI_PURPLE+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>1000000) {
			res=ANSI_BRIGHT_MAGENTA+calculString+ANSI_RESET;
		}
		return res;
	}

}
