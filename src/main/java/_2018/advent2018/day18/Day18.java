package advent2018.day18;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day18 {
	// constructeur ----
	public Day18() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day18 d = new Day18();
		d.run1();
	}

	// Methodes ----
	
	
	public  void run1() throws ParseException, IOException {
		Day18 d = new Day18();
		double startTime = System.currentTimeMillis();
		String file = "day18";
		d.run1_(file, startTime);
	}
	
	private void run1_(String file, double startTime) throws FileNotFoundException {
		char[][] grille = importation(file); // grille initiale t=0

		int minute = 1000000000;

		for (int i = 0; i < minute / 2; i++) {
			char[][] grilleTemp = applicationRegles(grille);
			grille = applicationRegles(grilleTemp);
			if (i % 1000000 == 0) {
				System.out.println(2 * i);
				System.out.println("Tps partiel: " + (System.currentTimeMillis() - startTime) / 60000 + " mn");
			}
		}

		// comptage des arbres
		comptage(grille);

	}

	private void comptage(char[][] grille) {
		int a = grille.length;
		int b = grille[0].length;
		int cpt1 = 0, cpt2 = 0;
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				if (grille[i][j] == '|') {
					cpt1++;
				}
				if (grille[i][j] == '#') {
					cpt2++;
				}
			}
		}
		System.out.println("Tree: " + cpt1);
		System.out.println("Lumberyard: " + cpt2);
		System.out.println("Resultat: " + cpt1 * cpt2);
	}

	private char[][] applicationRegles(char[][] grille_depart) {

		int a = grille_depart.length;
		int b = grille_depart[0].length;

		char[][] grille2 = new char[a][b];
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				grille2[i][j] = grille_depart[i][j];
			}
		}

		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				int cpt_tree = 0, cpt_lumberyard = 0;
				if (grille_depart[i][j] == '.') { // on est sur une case OPEN
					if (i + 1 < a) {
						if (grille_depart[i + 1][j] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0) {
						if (grille_depart[i - 1][j] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (j + 1 < b) {
						if (grille_depart[i][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (j > 0) {
						if (grille_depart[i][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i + 1 < a & j + 1 < b) {
						if (grille_depart[i + 1][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0 & j > 0) {
						if (grille_depart[i - 1][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i + 1 < a & j > 0) {
						if (grille_depart[i + 1][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0 & j + 1 < b) {
						if (grille_depart[i - 1][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (cpt_tree >= 3) {
						grille2[i][j] = '|';
					}
				}

				if (grille_depart[i][j] == '|') { // on est sur une case TREE
					if (i + 1 < a) {
						if (grille_depart[i + 1][j] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0) {
						if (grille_depart[i - 1][j] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (j + 1 < b) {
						if (grille_depart[i][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (j > 0) {
						if (grille_depart[i][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i + 1 < a & j + 1 < b) {
						if (grille_depart[i + 1][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0 & j > 0) {
						if (grille_depart[i - 1][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i + 1 < a & j > 0) {
						if (grille_depart[i + 1][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0 & j + 1 < b) {
						if (grille_depart[i - 1][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (cpt_lumberyard >= 3) {
						grille2[i][j] = '#';
					}
				}

				if (grille_depart[i][j] == '#') { // on est sur une case LUMBERYARD
					if (i + 1 < a) {
						if (grille_depart[i + 1][j] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0) {
						if (grille_depart[i - 1][j] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (j + 1 < b) {
						if (grille_depart[i][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (j > 0) {
						if (grille_depart[i][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i + 1 < a & j + 1 < b) {
						if (grille_depart[i + 1][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0 & j > 0) {
						if (grille_depart[i - 1][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i + 1 < a & j > 0) {
						if (grille_depart[i + 1][j - 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i > 0 & j + 1 < b) {
						if (grille_depart[i - 1][j + 1] == '|') {
							cpt_tree = cpt_tree + 1;
						}
					}
					if (i + 1 < a) {
						if (grille_depart[i + 1][j] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0) {
						if (grille_depart[i - 1][j] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (j + 1 < b) {
						if (grille_depart[i][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (j > 0) {
						if (grille_depart[i][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i + 1 < a & j + 1 < b) {
						if (grille_depart[i + 1][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0 & j > 0) {
						if (grille_depart[i - 1][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i + 1 < a & j > 0) {
						if (grille_depart[i + 1][j - 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (i > 0 & j + 1 < b) {
						if (grille_depart[i - 1][j + 1] == '#') {
							cpt_lumberyard = cpt_lumberyard + 1;
						}
					}
					if (cpt_lumberyard >= 1 & cpt_tree >= 1) {
						grille2[i][j] = '#';
					} else {
						grille2[i][j] = '.';
					}
				}

			}
		}

		return grille2;
	}

	private char[][] importation(String file) throws FileNotFoundException {
		// Import input
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day18/day18.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// System.out.println(input);
		int largeur = input.size();
		int hauteur = input.get(0).length();
		char[][] grille = new char[largeur][hauteur];

		for (int j = 0; j < largeur; j++) {
			for (int i = 0; i < largeur; i++) {
				grille[j][i] = input.get(j).charAt(i);
			}
		}
		return grille;
	}

	private void visualisation(char[][] grille, int imax, int jmax) {
		String str = "";
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				System.out.print(grille[i][j]);
				str = str + grille[i][j];
			}
			System.out.println();
			str = str + "\n";
		}
	}

}