package advent2018.day07;

import java.util.ArrayList;

public class Step {

	private char nom;
	ArrayList<Step> precedents = new ArrayList<>();
	private int tpsRequis;
	private boolean enCours;
	
	
	
	public char getNom() {
		return nom;
	}

	public void setNom(char nom) {
		this.nom = nom;
	}

	

	public Step(char c ) {
		nom=c;
		tpsRequis=charToInt(c);
		enCours=false;
	}

	public boolean isEnCours() {
		return enCours;
	}

	public void setEnCours(boolean enCours) {
		this.enCours = enCours;
	}

	public int getTpsRequis() {
		return tpsRequis;
	}

	public void setTpsRequis(int tpsRequis) {
		this.tpsRequis = tpsRequis;
	}

	public ArrayList<Step> getPrecedents() {
		return precedents;
	}

	public void setPrecedents(ArrayList<Step> precedents) {
		this.precedents = precedents;
	}
	
	public void ajoutePrecedents(Step s) {
		precedents.add(s);
	}
	
	public static int charToInt(char valeur) {
	    return (int)valeur - (int)'A' + 61;
	}
}
