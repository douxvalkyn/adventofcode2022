package advent2018.day07;

public class Ouvriers {
	
	
	private int nb_initial;
	private int nb_disponible;
	
	
	public Ouvriers(int nb_initial) {
		super();
		this.nb_initial = nb_initial;
		this.nb_disponible = nb_initial;
	}


	public int getNb_initial() {
		return nb_initial;
	}


	public void setNb_initial(int nb_initial) {
		this.nb_initial = nb_initial;
	}


	public int getNb_disponible() {
		return nb_disponible;
	}


	public void setNb_disponible(int nb_disponible) {
		this.nb_disponible = nb_disponible;
	}
	
	
}
