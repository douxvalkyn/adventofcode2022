package advent2018.day07;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day07 {

	// constructeur ----
	public Day07() {
	}

	// main ----
	public static void main(String[] args) throws FileNotFoundException {
		Day07 d = new Day07();
		double startTime = System.currentTimeMillis();
		d.run2();
		// d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	public void run1() throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day07/day7.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		// Cr�ation de la structure des instructions (arbre)
		ArrayList<Step> listeStep = new ArrayList<>();
		listeStep = this.creation_liste_step(input);

		// Recherche chemin
		String gagnants = new String();

		while (listeStep.size() > 0) {
			ArrayList<Step> listeStepEnLice = new ArrayList<>();
			for (Step st : listeStep) {
				// on conserve les �tapes qui n'ont pas de pr�c�dent dans la liste
				if (possedeAucunPrecedent(st)) {
					listeStepEnLice.add(st);
				}
			}

			// parmi ces �tapes conserv�es, l'�tape gagnante est celle avec la premiere lettre dans alphabet
			char gagnant = '{';
			for (Step s : listeStepEnLice) {
			}
			for (Step st : listeStepEnLice) {
				if (st.getNom() < gagnant) {
					gagnant = st.getNom();
				}
			}
			gagnants = gagnants + gagnant;

			// supprimer le gagnant de la liste des �tapes
			int index_gagnant = -1;
			for (int i = 0; i < listeStep.size(); i++) {
				if (listeStep.get(i).getNom() == gagnant) {
					index_gagnant = i;
				}
			}
			listeStep.remove(index_gagnant);

			// supprimer les liens du gagnant chez les restants
			for (Step s : listeStep) {
				for (int k = 0; k < s.getPrecedents().size(); k++) {
					if (s.getPrecedents().get(k).getNom() == gagnant) {
						s.getPrecedents().remove(k);
					}
				}
			}
		}
		System.out.println("gagnants: " + gagnants);
	}

	public void run2() throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day07/day7.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		ArrayList<Step> listeStep = new ArrayList<>();
		listeStep = this.creation_liste_step(input);
		Ouvriers ouvriers = new Ouvriers(5);

		// on regarde la situation chaque seconde
		for (int seconde = 1; seconde < 2000; seconde++) {
			ArrayList<Step> listeStepEnLice = new ArrayList<>();
			// recherche de la prochaine �tape
			for (Step st : listeStep) {
				// on conserve les �tapes qui n'ont pas de pr�c�dent dans la liste et qui ne sont pas d�ja en cours
				if (possedeAucunPrecedent(st) && !st.isEnCours()) {
					listeStepEnLice.add(st);
				}
			}

			// tant qu'il reste une �tape possible et un ouvrier, on attribue un ouvrier � une �tape (la premiere dans l'ordre alphabetique)
			for (int k = 0; k < ouvriers.getNb_initial(); k++) { // on peut attribuer plusiurs ouvriers chaque seconde !
				if ((!listeStepEnLice.isEmpty()) && ouvriers.getNb_disponible() > 0) {
					String gagnants = new String();
					char gagnant = '{';
					int indice_gagnant = -1;

					for (int l = 0; l < listeStepEnLice.size(); l++) {
						Step st = listeStepEnLice.get(l);
						if (st.getNom() < gagnant) {
							gagnant = st.getNom();
							indice_gagnant = l;
						}
					}
					gagnants = gagnants + gagnant;

					// enlever le gagnant affect� de listeStepEnLice
					listeStepEnLice.remove(indice_gagnant);

					// recherche de l'indice du gagnant dans listeStep
					int index_gagnant = -1;
					for (int i = 0; i < listeStep.size(); i++) {
						if (listeStep.get(i).getNom() == gagnant) {
							index_gagnant = i;
						}
					}

					// on affecte 1 ouvrier au gagnant
					listeStep.get(index_gagnant).setEnCours(true);

					// diminuer le nb d'ouvriers dispo de 1
					ouvriers.setNb_disponible(ouvriers.getNb_disponible() - 1);

				} // fin if affectation
			} // for nb ouvriers

			// diminuer le tps requis des �tapes "en cours" de 1 sec
			// et supprimer une �tape (et ses liens chez les restants) si son tpsRequis tombe � 0
			// et alors: augmenter le nb d'ouvriers dispo de 1
			// (on commence � it�rer de la fin pour �viter les probl�mes de remove en iterant)
			for (int i = listeStep.size() - 1; i >= 0; i--) {
				Step st = listeStep.get(i);
				if (st.isEnCours()) {
					st.setTpsRequis(st.getTpsRequis() - 1);
				}
				if (st.getTpsRequis() == 0) {
					System.out.println(st.getNom() + " - tps: " + seconde);
					listeStep.remove(i);
					ouvriers.setNb_disponible(ouvriers.getNb_disponible() + 1);
					for (Step s : listeStep) {
						for (int k = 0; k < s.getPrecedents().size(); k++) {
							if (s.getPrecedents().get(k).getNom() == st.getNom()) {
								s.getPrecedents().remove(k);
							}
						}
					}
				}
			} // fin for
		} // boucle seconde
	} // fin

	public boolean liste_contient_c(ArrayList<Step> liste, char c) {
		boolean existe = false;
		for (int i = 0; i < liste.size(); i++) {
			if (liste.get(i).getNom() == c) {
				existe = true;
			}
		}
		return existe;
	}

	public boolean possedeAucunPrecedent(Step st) {
		return st.getPrecedents().isEmpty();
	}

	// Cr�ation de la structure des instructions (arbre)
	public ArrayList<Step> creation_liste_step(ArrayList<String> input) {
		ArrayList<Step> listeStep = new ArrayList<>();
		// System.out.println(input);
		for (String s : input) {
			char c1 = s.charAt(5);
			char c2 = s.charAt(36);

			if (!(liste_contient_c(listeStep, c1))) {
				Step s1 = new Step(c1);
				s1.setNom(c1);
				listeStep.add(s1);
			}
			if (!(liste_contient_c(listeStep, c2))) {
				Step s2 = new Step(c2);
				s2.setNom(c2);
				listeStep.add(s2);
			}
			// ajoute un precedent � c2: c'est c1
			int index_s1 = 0, index_s2 = 0;
			for (int i = 0; i < listeStep.size(); i++) {
				if (listeStep.get(i).getNom() == c2) {
					index_s1 = i;
				}
				if (listeStep.get(i).getNom() == c1) {
					index_s2 = i;
				}
			}
			listeStep.get(index_s1).ajoutePrecedents(listeStep.get(index_s2));

		}
		return listeStep;
	}
}
