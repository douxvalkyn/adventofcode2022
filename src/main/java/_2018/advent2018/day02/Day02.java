package advent2018.day02;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day02 {

	// constructeur
	public Day02() {
	}

	// main
	public static void main(String[] args) throws FileNotFoundException {
		Day02 d = new Day02();
		double startTime = System.currentTimeMillis();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// M�thodes ----------------------------------------------------------------------------------------------------------
	// run1
	public void run1() throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day02/day2.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println("ma liste: " + input);

		int compte2 = 0, compte3 = 0;
		for (int j = 0; j < input.size(); j++) {
			if (possedeNLettres(input.get(j), 2)) {
				compte2 = compte2 + 1;
			}
			if (possedeNLettres(input.get(j), 3)) {
				compte3 = compte3 + 1;
			}
		}
		System.out.println("compte2: " + compte2);
		System.out.println("compte3: " + compte3);
		System.out.println("Resultat: " + compte2 * compte3);
		nbLettresCommunes(input.get(0), input.get(2));
	}

	// run2
	public void run2() throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day02/day2.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println("ma liste: " + input);

		for (int j = 0; j < input.size(); j++) {
			String s1 = input.get(j);
			for (int k = j + 1; k < input.size(); k++) {
				String s2 = input.get(k);
				// System.out.println(nbLettresCommunes(s1,s2));
				if (nbLettresCommunes(s1, s2) == s1.length() - 1) {
					System.out.println("id1: " + s1);
					System.out.println("id2: " + s2);
					System.out.println("lettres communes: " + lettresCommunes(s1, s2));
				}
			}
		}
	}

	// renvoie true si le String a exactement n lettres identiques
	public boolean possedeNLettres(String monString, int n) {
		ArrayList<Integer> liste_resultat = new ArrayList<>();

		for (int index = 0; index < monString.length(); index++) {
			char c = monString.charAt(index);
			String first = monString.substring(0, index);
			String second = monString.substring(index + 1);
			String result = first + second;
			int res = 1;
			for (int i = 0; i < result.length(); i++) {
				if (c == result.charAt(i)) {
					res = res + 1;
				}
			}
			// System.out.println(res);
			liste_resultat.add(res);
			// System.out.println(liste_resultat);
		}
		// System.out.println(liste_resultat.contains(n));
		return (liste_resultat.contains(n));
	}

	public int nbLettresCommunes(String s1, String s2) {
		int nbLettresCommunes = 0;
		for (int i = 0; i < s1.length(); i++) { // s1 et s2 ont m�me longueur
			if (s1.charAt(i) == s2.charAt(i)) {
				nbLettresCommunes = nbLettresCommunes + 1;
			}
		}
		return nbLettresCommunes;
	}

	public String lettresCommunes(String s1, String s2) {
		String lettresCommunes = "";
		for (int i = 0; i < s1.length(); i++) { // s1 et s2 ont m�me longueur
			if (s1.charAt(i) == s2.charAt(i)) {
				lettresCommunes = lettresCommunes + s1.charAt(i);
			}
		}
		return lettresCommunes;
	}

}
