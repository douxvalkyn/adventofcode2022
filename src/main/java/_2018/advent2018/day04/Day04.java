package advent2018.day04;

import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Day04 {

	// constructeur
	public Day04() {

	}

	// main
	public static void main(String[] args) throws ParseException, IOException {
		Day04 d = new Day04();
		double startTime = System.currentTimeMillis();
		d.run1();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// M�thodes ----------------------------------------------------------------------------------------------------------
	
	public  void run1() throws ParseException, IOException {
		Day04 d = new Day04();
		Set<Integer> liste_gardes = d.run1_();
	}
	public  void run2() throws ParseException, IOException {
		Day04 d = new Day04();
		Set<Integer> liste_gardes = d.run1_();
		d.run2_(liste_gardes);
	}
	
	// run1
	@SuppressWarnings("deprecation")
	public Set<Integer> run1_() throws ParseException, IOException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day04/day4.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		// Format de Date pour les horaires import�s
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);
		// Cr�ation d'une liste "Map" [cl�=date, valeur=action] avec une TreeMap par Date, qui trie automatiquement
		Map<Date, String> guardList = new TreeMap<Date, String>();
		Map<Integer, Integer> guardList_sum_asleep = new TreeMap<Integer, Integer>();

		for (int index = 0; index < input.size(); index++) {
			Date maDate = format.parse(input.get(index).substring(1, input.get(index).indexOf("]")));
			String monAction = input.get(index).substring(input.get(index).indexOf("]") + 2, input.get(index).length());
			guardList.put(maDate, monAction);

		}

		// System.out.println(guardList);
		int id_guard = 0;
		int start_asleep = 0;
		int stop_asleep = 0;

		// It�rer sur les �l�ments ordonn�s de la liste
		Iterator iterator = guardList.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			Date key = (Date) entry.getKey();
			String value = (String) entry.getValue();
			// wr.write(key.toString());
			// wr.write(" : ");
			// wr.write(value);
			// wr.write("\n");
			if (value.contains("#")) {
				id_guard = Integer.parseInt(value.substring(7, value.indexOf("b")).trim());
			}
			if (value.contains("falls")) {
				start_asleep = key.getMinutes();
			}
			if (value.contains("wakes")) {
				stop_asleep = key.getMinutes();

				// si l'id_guard est d�j� sorti, il faut sommer le temps de sommeil avec ce qui est d�j� stock�
				guardList_sum_asleep.put(id_guard, guardList_sum_asleep.getOrDefault(id_guard, 0) + stop_asleep - start_asleep);

			}

		}
		// wr.flush();
		// wr.close();

		// R�cup�rer la valeur max
		System.out.println(guardList_sum_asleep);
		Set<Integer> listeGardes = guardList_sum_asleep.keySet();
		Map.Entry<Integer, Integer> maxEntry = null;

		for (Map.Entry<Integer, Integer> entry : guardList_sum_asleep.entrySet()) {
			if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
				maxEntry = entry;
			}
		}
		System.out.println(maxEntry);

		// Reparcourir la liste initiale en filtrant sur la cl� trouv�e et en recherchant la minute o� on a le plus de sommeil
		int[] minutage = new int[60];
		// It�rer sur les �l�ments ordonn�s de la liste
		Iterator iterator2 = guardList.entrySet().iterator();
		while (iterator2.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator2.next();
			Date key = (Date) entry.getKey();
			String value = (String) entry.getValue();

			if (value.contains("#")) {
				id_guard = Integer.parseInt(value.substring(7, value.indexOf("b")).trim());
			}
			if (value.contains("falls")) {
				start_asleep = key.getMinutes();
			}
			if (value.contains("wakes") && id_guard == maxEntry.getKey()) {
				stop_asleep = key.getMinutes();
				for (int i = start_asleep; i < stop_asleep; i++) {
					minutage[i] = minutage[i] + 1;
				}
			}
		}

		// for (int i=0;i<60;i++) {System.out.println(minutage[i]);}
		// Afficher la minute avec le max de sommeil
		int max = Arrays.stream(minutage).max().getAsInt();
		int minute_max = 0;
		for (int i = 0; i < 60; i++) {
			if (minutage[i] == max) {
				minute_max = i;
			}
		}
		System.out.println("minute max: " + minute_max);
		System.out.println("Resultat: " + minute_max * maxEntry.getKey());
		return listeGardes;
	}

	// run2
	public void run2_(Set<Integer> liste_gardes) throws ParseException, IOException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day04/day4.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// Format de Date pour les horaires import�s
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);
		// Cr�ation d'une liste "Map" [cl�=date, valeur=action] avec une TreeMap par Date, qui trie automatiquement
		Map<Date, String> guardList = new TreeMap<Date, String>();
		for (int index = 0; index < input.size(); index++) {
			Date maDate = format.parse(input.get(index).substring(1, input.get(index).indexOf("]")));
			String monAction = input.get(index).substring(input.get(index).indexOf("]") + 2, input.get(index).length());
			guardList.put(maDate, monAction);

		}

		// Boucle � faire pour chaque garde (c'est long mais �a va marcher)
		int minute_max_tous_gardes = 0;
		int max_tous_gardes = 0;
		int id_gard_max = 0;
		for (Integer s : liste_gardes) {

			int id_guard = 0;
			int start_asleep = 0;
			int stop_asleep = 0;

			int[] minutage = new int[60];
			// It�rer sur les �l�ments ordonn�s de la liste
			Iterator iterator2 = guardList.entrySet().iterator();
			while (iterator2.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator2.next();
				Date key = (Date) entry.getKey();
				String value = (String) entry.getValue();

				if (value.contains("#")) {
					id_guard = Integer.parseInt(value.substring(7, value.indexOf("b")).trim());
				}
				if (value.contains("falls")) {
					start_asleep = key.getMinutes();
				}
				if (value.contains("wakes") && id_guard == s) {
					stop_asleep = key.getMinutes();
					for (int i = start_asleep; i < stop_asleep; i++) {
						minutage[i] = minutage[i] + 1;
					}
				}
			}

			// Afficher la minute avec le max de sommeil
			int max = Arrays.stream(minutage).max().getAsInt();
			int minute_max = 0;
			for (int i = 0; i < 60; i++) {
				if (minutage[i] == max) {
					minute_max = i;
				}
			}

			// on conserve la minute max de tous les gardes
			if (max > max_tous_gardes) {
				minute_max_tous_gardes = minute_max;
				max_tous_gardes = max;
				id_gard_max = s;
			}

		} // fin boucle

		System.out.println("minute max: " + minute_max_tous_gardes);
		System.out.println("maximum: " + max_tous_gardes);
		System.out.println("ID Guard: " + id_gard_max);
		System.out.println("Resultat: " + id_gard_max * minute_max_tous_gardes);

	}
}
