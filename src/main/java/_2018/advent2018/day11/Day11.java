package advent2018.day11;
import java.io.FileNotFoundException;
import java.util.ArrayList;




public class Day11 {

	//constructeur ----
	public Day11() {	
	}



	// main ----
	public static void main(String[] args) throws FileNotFoundException {     
		Day11 d = new Day11();
		double startTime = System.currentTimeMillis();
		d.run2();
		//d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}


	//Methodes ----
	private void run2() throws FileNotFoundException {
		int gridSerialNumber = 6042;
		ArrayList<Point> listePoints = new ArrayList<>();
		int[][] grille = new int[301][301];
		importation(listePoints);
		creation_grille_visible(grille);
		calculPowerCell(listePoints, gridSerialNumber);
		saisiePoints(grille, listePoints);
		rechercheLargestTotal2(listePoints, grille);
		//affichage(grille);
	}


	private void rechercheLargestTotal2(ArrayList<Point> listePoints, int[][] grille) {
		double cote=Math.sqrt(listePoints.size()); //nb de points qui forment un c�t� de la grille (la grille fait donc cote x cote)
		int max=-1000000, max_i=0, max_j=0, max_taille=0;
		for (int taille=1; taille <cote+1; taille++) { // boucle sur la taille du carr� cherch�
			System.out.println(taille);
			// calcul des indices pour i (indice du point en haut � gauche dans le carr� cherch�)
			ArrayList<Integer> liste_i = new ArrayList<>();
			for (int l=0; l<(cote-taille+1); l++) {
				for (int m=0; m<(cote-taille+1); m++) {
					liste_i.add((int) (1+l*cote+m));				
				}			
			}
			// calcul de la somme des power pour les carr�s recherch�s (selon la taille)
			// et recherche du power max			
			for (int i:liste_i) {
				int power=0;
				for (int k=0; k<taille; k++) {					
					for (int j=0; j<taille; j++) {
						power=power+listePoints.get((int) (i+k*cote+j-1)).getPowerLevel();					
					}				
				}
				//System.out.println("i: "+i+" power: "+ power);
				if (power>max) {max=power; max_i=listePoints.get(i-1).getPosition_x(); max_taille=taille; max_j=listePoints.get(i-1).getPosition_y();}
			}
		}
		//Affichage des r�sultats
		System.out.println("max: "+ max);
		System.out.println("max i: "+ (max_i));
		System.out.println("max j: "+ (max_j));
		System.out.println("max taille: "+ max_taille);		
	}



	private void run1() throws FileNotFoundException {
		int gridSerialNumber = 6042;
		ArrayList<Point> listePoints = new ArrayList<>();
		int[][] grille = new int[301][301];
		importation(listePoints);
		creation_grille_visible(grille);
		calculPowerCell(listePoints, gridSerialNumber);
		saisiePoints(grille, listePoints);
		rechercheLargestTotal(grille);	
		affichage(grille);
	}

	private void rechercheLargestTotal(int[][]   grille) {
		int max=-1000000, max_i=0, max_j=0;
		for (int i=2; i<300; i++) {
			for (int j=2; j<300; j++) {
				int total=grille[i-1][j-1] + grille[i-1][j] + grille[i-1][j+1]+grille[i][j-1] + grille[i][j] + grille[i][j+1]+grille[i+1][j-1] + grille[i+1][j] + grille[i+1][j+1] ;
				if (total>max) {max=total;max_i=i-1; max_j=j-1;}
			}		
		}
		System.out.println("max: "+max);
		System.out.println("cellule: " + max_j +","+ max_i);
	}


	private void calculPowerCell(ArrayList<Point> listePoints, int gridSerialNumber) {
		for (Point str : listePoints) {
			int rackId= str.getPosition_x()+10;
			int powerLevel= (rackId*str.getPosition_y()+gridSerialNumber)*rackId;
			powerLevel= (powerLevel/100)%10; //garder le chiffre des centaines
			powerLevel=powerLevel-5;
			str.setPowerLevel(powerLevel);				
		}				
	}



	private void saisiePoints (int[][] grille, ArrayList<Point> listePoints) {
		for (Point str : listePoints) {
			grille[str.getPosition_y()][str.getPosition_x()]=str.getPowerLevel();	
		}
	}


	private void affichage(int[][] grille) {
		// affichage de la grille avec les points
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				System.out.print(grille[i][j] + "\t");
			}
			System.out.println();
		}
	}


	private void creation_grille_visible(int[][] grille) {	
		//affichage neutre de la grille visible
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				grille[i][j] =0;
			}
		}
	}


	public void importation(ArrayList<Point>listePoints) throws FileNotFoundException {
		for(int j=1; j<301; j++) {
			for(int i=1; i<301; i++) {
				int position_x = i;
				int position_y = j;					
				Point point = new Point(position_x, position_y);
				listePoints.add(point);
			}
		}	
	}		


}

