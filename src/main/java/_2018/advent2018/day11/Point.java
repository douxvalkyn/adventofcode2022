package advent2018.day11;

public class Point {
	
	private int position_x;
	private int position_y;
	private int powerLevel;
	
	
	
	public int getPosition_x() {
		return position_x;
	}
	public void setPosition_x(int position_x) {
		this.position_x = position_x;
	}
	public int getPosition_y() {
		return position_y;
	}
	public void setPosition_y(int position_y) {
		this.position_y = position_y;
	}


	public int getPowerLevel() {
		return powerLevel;
	}
	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}
	public Point(int position_x, int position_y) {
		super();
		this.position_x = position_x;
		this.position_y = position_y;

	}
	
	public String toString() {
		return ("pos: "+ position_x + ","+ position_y + "; vel: ");
		
	}
}
