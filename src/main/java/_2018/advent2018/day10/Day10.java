package advent2018.day10;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day10 {

	// constructeur ----
	public Day10() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day10 d = new Day10();
		double startTime = System.currentTimeMillis();
		d.run1();
		
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	
	
	public  void run1() throws ParseException, IOException {
		Day10 d = new Day10();
		d.run1_();
	}
	
	
	public void run1_() throws FileNotFoundException {
		ArrayList<Point> listePoints = new ArrayList<>();
		importation(listePoints);
		String[][] grille = new String[400][400];
		creation_grille_visible(grille);
		saisiePoints(grille, listePoints);

		int nbPtsVisibles = 0;
		ArrayList<Integer> nbPts = new ArrayList<>();
		// actualiser les points � chaque seconde en fonction de velocity
		for (int i = 0; i < 10619; i++) {
			deplacement(grille, listePoints);
			creation_grille_visible(grille);
			nbPtsVisibles = saisiePoints(grille, listePoints);

			nbPts.add(nbPtsVisibles);
		}

		// System.out.println(nbPts);
		int max = 0;
		int maxPos = 0;
		for (int i = 0; i < nbPts.size(); i++) {
			int value = nbPts.get(i);
			if (value > max) {
				max = value;
				maxPos = i;
			}
		}
		System.out.println("maxPos: " + maxPos);
		System.out.println("cpt: " + nbPts.get(maxPos));
		affichage(grille);
	}

	private void deplacement(String[][] grille, ArrayList<Point> listePoints) {
		for (Point str : listePoints) {
			str.setPosition_x(str.getPosition_x() + str.getVelocity_x());
			str.setPosition_y(str.getPosition_y() + str.getVelocity_y());
		}
	}

	// saisie des coords des points dans la grille si ils sont � port�e et calcul du points dans la fen�tre visible
	private int saisiePoints(String[][] grille, ArrayList<Point> listePoints) {
		int nbPointsVisibles = 0;
		for (Point str : listePoints) {
			if (str.getPosition_y() > -150 && str.getPosition_y() < 210 && str.getPosition_x() > -100 && str.getPosition_x() < 200) {
				grille[str.getPosition_y() + 170][str.getPosition_x() + 140] = "X";
				nbPointsVisibles++;
			}
		}
		return nbPointsVisibles;
	}

	private void creation_grille_visible(String[][] grille) {
		// affichage neutre de la grille visible
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				grille[i][j] = ".";
			}
		}
	}

	private void affichage(String[][] grille) {
		// affichage de la grille avec les points
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				System.out.print(grille[i][j] + "\t");
			}
			System.out.println();
		}
	}

	// Lecture input -----
	public void importation(ArrayList<Point> listePoints) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day10/day10.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		int cpt = 0;
		for (String str : input) {

			String position = str.substring(str.indexOf("n") + 3, str.indexOf(">"));
			String velocity = str.substring(str.indexOf("y") + 3, str.lastIndexOf(">"));

			int position_x = Integer.parseInt(position.substring(0, position.indexOf(",")).trim());
			int position_y = Integer.parseInt(position.substring(position.indexOf(",") + 1).trim());

			int velocity_x = Integer.parseInt(velocity.substring(0, velocity.indexOf(",")).trim());
			int velocity_y = Integer.parseInt(velocity.substring(velocity.indexOf(",") + 1).trim());

			Point point = new Point(position_x, position_y, velocity_x, velocity_y);
			listePoints.add(point);
			cpt++;
		}
	}
}