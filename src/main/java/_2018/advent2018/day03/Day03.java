package advent2018.day03;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day03 {

	// constructeur
	public Day03() {
	}

	// main
	public static void main(String[] args) throws FileNotFoundException {
		Day03 d = new Day03();
		double startTime = System.currentTimeMillis();
		d.run1();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// M�thodes ----------------------------------------------------------------------------------------------------------
	public  void run1() throws FileNotFoundException {
		Day03 d = new Day03();
		int[][] grille = new int[1000][1000];
		d.run1_(grille);
	}
	public  void run2() throws FileNotFoundException {
		Day03 d = new Day03();
		int[][] grille = new int[1000][1000];
		d.run2_(d.run1_(grille));
	}
	
	
	
	// run1
	public int[][] run1_(int[][] grille) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day03/day3.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// Cr�ation de la grille
		for (int index = 0; index < input.size(); index++) {
			// R�cup�ration des informations du claim
			int id = Integer.parseInt(input.get(index).substring(1, input.get(index).indexOf("@") - 1).trim());
			int x = Integer.parseInt(input.get(index).substring(input.get(index).indexOf("@") + 1, input.get(index).indexOf(",")).trim());
			int y = Integer.parseInt(input.get(index).substring(input.get(index).indexOf(",") + 1, input.get(index).indexOf(":")).trim());
			int l = Integer.parseInt(input.get(index).substring(input.get(index).indexOf(":") + 1, input.get(index).indexOf("x")).trim());
			int L = Integer.parseInt(input.get(index).substring(input.get(index).indexOf("x") + 1, input.get(index).length()).trim());

			// Modifier l'�tat des points
			// x et y sont invers�s !
			for (int colonne = 0; colonne < L; colonne++) {
				for (int ligne = 0; ligne < l; ligne++) {
					grille[y + colonne][x + ligne] = grille[y + colonne][x + ligne] + 1;
				}
			}
		}

		// affichage de la grille
		// for (int i = 0; i < grille.length; i++) {
		// for (int j = 0; j < grille[0].length; j++) {
		// System.out.print(grille[i][j] + "\t");
		// }
		// System.out.println();
		// }

		// Comptage des points qui sont tenus par plusieurs
		int somme = 0;
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				if (grille[i][j] > 1) {
					somme = somme + 1;
				}
			}
		}
		System.out.println("R�sultat:" + somme);
		return grille;
	}

	// run2

	public void run2_(int[][] grille) throws FileNotFoundException {

		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day03/day3.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		for (int index = 0; index < input.size(); index++) {
			// R�cup�ration des informations du claim
			int id = Integer.parseInt(input.get(index).substring(1, input.get(index).indexOf("@") - 1).trim());
			int x = Integer.parseInt(input.get(index).substring(input.get(index).indexOf("@") + 1, input.get(index).indexOf(",")).trim());
			int y = Integer.parseInt(input.get(index).substring(input.get(index).indexOf(",") + 1, input.get(index).indexOf(":")).trim());
			int l = Integer.parseInt(input.get(index).substring(input.get(index).indexOf(":") + 1, input.get(index).indexOf("x")).trim());
			int L = Integer.parseInt(input.get(index).substring(input.get(index).indexOf("x") + 1, input.get(index).length()).trim());

			// Modifier l'�tat des points
			// x et y sont invers�s !
			int produit = 1;
			for (int colonne = 0; colonne < L; colonne++) {
				for (int ligne = 0; ligne < l; ligne++) {
					produit = produit * grille[y + colonne][x + ligne];
				}
			}
			if (produit == 1) {
				System.out.println("id seul: " + id);
			}
		}

		// affichage de la grille
		// for (int i = 0; i < grille.length; i++) {
		// for (int j = 0; j < grille[0].length; j++) {
		// System.out.print(grille[i][j] + "\t");
		// }
		// System.out.println();
		// }

		// Comptage des points qui sont tenus par plusieurs
		int somme = 0;
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille[0].length; j++) {
				if (grille[i][j] > 1) {
					somme = somme + 1;
				}
			}
		}

	}

}
