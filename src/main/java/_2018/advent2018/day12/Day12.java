package advent2018.day12;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Day12 {

	// constructeur ----
	public Day12() {
	}

	// main ----
	public static void main(String[] args) throws FileNotFoundException {
		Day12 d = new Day12();
		double startTime = System.currentTimeMillis();
		d.run2();
		// d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	public void run2() throws FileNotFoundException {
		String initial_state = "";
		ArrayList<Regle> regles = new ArrayList<>();
		int nb_generations = 120;
		initial_state = importation(initial_state, regles);
		initial_state = elargissement_initial_state(initial_state);

		ArrayList<String> states = new ArrayList<>();
		states.add(initial_state);
		states = initialisation_states(states, nb_generations); // on met � vide les 20 autres �tats

		for (int k = 1; k < nb_generations + 2; k++) {
			String etat = states.get(k);
			for (int i = 10; i < etat.length() - 2; i++) {
				char a = states.get(k - 1).charAt(i - 2);
				char b = states.get(k - 1).charAt(i - 1);
				char c = states.get(k - 1).charAt(i);
				char d = states.get(k - 1).charAt(i + 1);
				char e = states.get(k - 1).charAt(i + 2);
				for (int j = 0; j < regles.size(); j++) {
					Regle regle = regles.get(j);
					if (regle.getCondition1() == a && regle.getCondition2() == b && regle.getCondition3() == c && regle.getCondition4() == d && regle.getCondition5() == e) {
						etat = etat.substring(0, i) + '#' + etat.substring(i + 1);
					}
				}
				states.set(k, etat);
			}
			System.out.println("Etape" + (k - 1) + ":" + states.get(k - 1));
		}

		// Calcul des r�sultats apres x g�n�rations
		String etat_final = states.get(nb_generations);
		int somme = 0;
		for (int i = 0; i < etat_final.length(); i++) {
			if (etat_final.charAt(i) == '#') {
				somme = somme + (i - 20);
			}
		}
		System.out.println("score: " + somme);

		// on remarque une stabilisation � partir de la 100e generation
		// G100: 5983
		// G101: +51=6034
		// G102: +51=6085
		// On a donc une suite arithm�tique de raison 51 (= le nb de plantes), de premier terme 5983
		// Un= 5983+51(n-100)
		// on cherche le terme U(50 000 000 000 - 100)=5983+51*(50000000000-100)
		System.out.println("R�sultat final: " + (5983 + 51 * (50000000000L - 100)));

	}

	public void run1() throws FileNotFoundException {
		String initial_state = "";
		ArrayList<Regle> regles = new ArrayList<>();
		initial_state = importation(initial_state, regles);
		initial_state = elargissement_initial_state(initial_state);

		ArrayList<String> states = new ArrayList<>();
		states.add(initial_state);
		states = initialisation_states(states, 20); // on met � vide les 20 autres �tats

		for (int k = 1; k < 22; k++) {
			String etat = states.get(k);
			for (int i = 10; i < etat.length() - 2; i++) {
				char a = states.get(k - 1).charAt(i - 2);
				char b = states.get(k - 1).charAt(i - 1);
				char c = states.get(k - 1).charAt(i);
				char d = states.get(k - 1).charAt(i + 1);
				char e = states.get(k - 1).charAt(i + 2);
				for (int j = 0; j < regles.size(); j++) {
					Regle regle = regles.get(j);
					if (regle.getCondition1() == a && regle.getCondition2() == b && regle.getCondition3() == c && regle.getCondition4() == d && regle.getCondition5() == e) {
						etat = etat.substring(0, i) + '#' + etat.substring(i + 1);
					}
				}
				states.set(k, etat);
			}
			System.out.println("Etape" + (k - 1) + ":" + states.get(k - 1));
		}

		// Calcul des r�sultats apres x g�n�rations
		String etat_final = states.get(20);
		int somme = 0;
		for (int i = 0; i < etat_final.length(); i++) {
			if (etat_final.charAt(i) == '#') {
				somme = somme + (i - 20);
			}
		}
		System.out.println(somme);

	}

	private ArrayList<String> initialisation_states(ArrayList<String> states, int nb_generations) {
		for (int i = 0; i < nb_generations + 1; i++) {
			// String state="..........................................................................."; // 20+30+25=75 carac pour run1a
			String state = ".............................................................................................................................................................................................................................................................................."; // 20+150+100=270
																																																																												// carac
																																																																												// pour
																																																																												// run1
			states.add(state);
		}
		return states;
	}

	private String elargissement_initial_state(String initial_state) {
		// on ajoute 20 points � gauche et 150 points � droite
		initial_state = ("....................").concat(initial_state);
		initial_state = initial_state.concat("......................................................................................................................................................");
		return initial_state;
	}

	private String importation(String initial_state, ArrayList<Regle> regles) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day12/day12.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println("input: " + input);

		// premiere ligne= initial state
		initial_state = input.get(0).substring(15, input.get(0).length());

		// � partir de la ligne 2: r�gles

		for (int i = 2; i < input.size(); i++) {
			String str = input.get(i);
			// System.out.println(str);
			Regle rule = new Regle(str.substring(0, 1).charAt(0), str.substring(1, 2).charAt(0), str.substring(2, 3).charAt(0), str.substring(3, 4).charAt(0), str.substring(4, 5).charAt(0),
				str.substring(9, 10).charAt(0));
			regles.add(rule);
		}
		// System.out.println(regles);
		// supprimer les r�gles qui ne g�n�rent pas de plante
		for (int i = regles.size() - 1; i >= 0; i--) {
			Regle rule = regles.get(i);
			if (rule.getEtat() == '.')
				regles.remove(rule);
		}
		System.out.println("r" + regles);
		return initial_state;
	}

}