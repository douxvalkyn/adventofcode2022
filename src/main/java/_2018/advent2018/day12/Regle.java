package advent2018.day12;

public class Regle {

	private char condition1;
	private char condition2;
	private char condition3;
	private char condition4;
	private char condition5;
	private char etat;
	
	public Regle(char condition1, char condition2, char condition3, char condition4, char condition5,
			char etat) {
		super();
		this.condition1 = condition1;
		this.condition2 = condition2;
		this.condition3 = condition3;
		this.condition4 = condition4;
		this.condition5 = condition5;
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "["+condition1 + condition2 +  condition3 + condition4 +  condition5 + " > " + etat +"]";
	}

	public char getCondition1() {
		return condition1;
	}

	public void setCondition1(char condition1) {
		this.condition1 = condition1;
	}

	public char getCondition2() {
		return condition2;
	}

	public void setCondition2(char condition2) {
		this.condition2 = condition2;
	}

	public char getCondition3() {
		return condition3;
	}

	public void setCondition3(char condition3) {
		this.condition3 = condition3;
	}

	public char getCondition4() {
		return condition4;
	}

	public void setCondition4(char condition4) {
		this.condition4 = condition4;
	}

	public char getCondition5() {
		return condition5;
	}

	public void setCondition5(char condition5) {
		this.condition5 = condition5;
	}

	public char getEtat() {
		return etat;
	}

	public void setEtat(char etat) {
		this.etat = etat;
	}
}
