package advent2018.day13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Train{

	private int coord_x;
	private int coord_y;
	private String sens;
	private String movePrecedent;
	private String etat;
	
	public Train(int coord_x, int coord_y, String sens) {
		super();
		this.coord_x = coord_x;
		this.coord_y = coord_y;
		this.sens = sens;
		this.etat="OK";
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public int getCoord_x() {
		return coord_x;
	}

	public void setCoord_x(int coord_x) {
		this.coord_x = coord_x;
	}

	public int getCoord_y() {
		return coord_y;
	}

	public void setCoord_y(int coord_y) {
		this.coord_y = coord_y;
	}

	public String getSens() {
		return sens;
	}

	public void setSens(String sens) {
		this.sens = sens;
	}

	public String getMovePrecedent() {
		return movePrecedent;
	}

	public void setMovePrecedent(String movePrecedent) {
		this.movePrecedent = movePrecedent;
	}

	@Override
	public String toString() {
		return "[x=" + coord_x + ", y=" + coord_y + ", " + sens + "]";
	}



	
	
}
