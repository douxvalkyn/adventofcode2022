package advent2018.day13;

public class Rail {
	
	private int coord_x;
	private int coord_y;
	private Orientation orientation;

	
	public Rail(int coord_x, int coord_y, Orientation or) {
		super();
		this.coord_x = coord_x;
		this.coord_y = coord_y;
		this.orientation = or;
	}

	public int getCoord_x() {
		return coord_x;
	}

	public void setCoord_x(int coord_x) {
		this.coord_x = coord_x;
	}

	public int getCoord_y() {
		return coord_y;
	}

	public void setCoord_y(int coord_y) {
		this.coord_y = coord_y;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	@Override
	public String toString() {
		return "[x=" + coord_x + ", y=" + coord_y + "," + orientation + "]";
	}




	
	
}
