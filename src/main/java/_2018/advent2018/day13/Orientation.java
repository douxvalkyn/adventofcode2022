package advent2018.day13;

public enum Orientation {
		  NS,
		  EO,
		  SLASH,
		  ANTISLASH,
		  INTERSECTION
}
