package advent2018.day13;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Day13 {

	// constructeur ----
	public Day13() {
	}

	// main ----
	public static void main(String[] args) throws FileNotFoundException {
		Day13 d = new Day13();
		double startTime = System.currentTimeMillis();
		d.run2();
		// d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	public void run2() throws FileNotFoundException {
		ArrayList<Rail> rails = new ArrayList<>();
		ArrayList<Train> trains = new ArrayList<>();
		importation(rails, trains);
		System.out.println("Rails: " + rails);
		System.out.println("Trains au d�part: " + trains);
		int tick = 0;
		boolean collision = false;
		while (trains.size() > 1) {
			// for ( tick=1; tick<20; tick++) {
			tick = tick + 1;
			collision = deplacement(rails, trains);
			// si une collision a eu lieu, je supprime les trains crash�s
			if (collision) {
				for (Train monPetitTrain : new ArrayList<Train>(trains)) {
					if (monPetitTrain.getEtat() == "KO") {
						trains.remove(monPetitTrain);
					}
				}
			}
			System.out.println("Tick " + tick + ": Trains: " + trains);
		}
	}

	public void run1() throws FileNotFoundException {
		ArrayList<Rail> rails = new ArrayList<>();
		ArrayList<Train> trains = new ArrayList<>();
		importation(rails, trains);
		System.out.println("Rails: " + rails);
		System.out.println("Trains au d�part: " + trains);
		int tick = 0;
		boolean collision = false;
		while (!collision) {
			// for ( tick=1; tick<9; tick++) {
			tick = tick + 1;
			collision = deplacement(rails, trains);
			System.out.println("Tick " + tick + ": Trains: " + trains);
		}
	}

	private boolean rechercheCollision(ArrayList<Train> trains) {
		// 2 trains sont en collision quand ils partagent les m�mes coordonn�es ou qd ils ont 1 case d'�cart et des sens oppos�s (ils viennent de se croiser)
		boolean collision = false;
		for (int i = 0; i < trains.size(); i++) {
			Train train1 = trains.get(i);

			for (int j = 0; j < trains.size(); j++) {
				Train train2 = trains.get(j);
				if (train1.getCoord_x() == train2.getCoord_x() && train1.getCoord_y() == train2.getCoord_y() && i != j) {
					System.out.println("Collision! [x: " + train1.getCoord_x() + ", y: " + train1.getCoord_y() + "]");
					collision = true;
					trains.get(i).setEtat("KO"); // on rep�re les trains crash�s
					trains.get(j).setEtat("KO"); // on rep�re les trains crash�s
				}

			}
		}
		return collision;
	}

	private boolean deplacement(ArrayList<Rail> rails, ArrayList<Train> trains) {
		Day13.order(trains);
		// System.out.println("Trains_order: "+trains);
		boolean collision = false;
		for (Train train : trains) {
			int x_depart = train.getCoord_x();
			int y_depart = train.getCoord_x();
			String sens_depart = train.getSens();

			// d�placement de 1
			if (train.getSens() == "E") {
				train.setCoord_x(train.getCoord_x() + 1);
			}
			if (train.getSens() == "O") {
				train.setCoord_x(train.getCoord_x() - 1);
			}
			if (train.getSens() == "N") {
				train.setCoord_y(train.getCoord_y() - 1);
			}
			if (train.getSens() == "S") {
				train.setCoord_y(train.getCoord_y() + 1);
			}
			// ajustement du sens selon l'orientation du rail d'arriv�e
			Orientation or = rechercheOrientationRail(train.getCoord_x(), train.getCoord_y(), rails);

			// si or=EO, NS: ok
			// si or= INTERSECTION: on fait tourner le train
			// si or= SLASH, ANTISLASH: on fait tourner le train

			if (or == Orientation.INTERSECTION) {
				boolean GO = true; // switch case maison
				if (train.getMovePrecedent() == null && sens_depart == "E" && GO) {
					train.setSens("N");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == null && sens_depart == "O" && GO) {
					train.setSens("S");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == null && sens_depart == "N" && GO) {
					train.setSens("O");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == null && sens_depart == "S" && GO) {
					train.setSens("E");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == "LEFT" && sens_depart == "E" && GO) {
					train.setSens("E");
					train.setMovePrecedent("STRAIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "LEFT" && sens_depart == "O" && GO) {
					train.setSens("O");
					train.setMovePrecedent("STRAIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "LEFT" && sens_depart == "N" && GO) {
					train.setSens("N");
					train.setMovePrecedent("STRAIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "LEFT" && sens_depart == "S" && GO) {
					train.setSens("S");
					train.setMovePrecedent("STRAIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "STRAIGHT" && sens_depart == "E" && GO) {
					train.setSens("S");
					train.setMovePrecedent("RIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "STRAIGHT" && sens_depart == "O" && GO) {
					train.setSens("N");
					train.setMovePrecedent("RIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "STRAIGHT" && sens_depart == "N" && GO) {
					train.setSens("E");
					train.setMovePrecedent("RIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "STRAIGHT" && sens_depart == "S" && GO) {
					train.setSens("O");
					train.setMovePrecedent("RIGHT");
					GO = false;
				}
				if (train.getMovePrecedent() == "RIGHT" && sens_depart == "E" && GO) {
					train.setSens("N");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == "RIGHT" && sens_depart == "O" && GO) {
					train.setSens("S");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == "RIGHT" && sens_depart == "N" && GO) {
					train.setSens("O");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
				if (train.getMovePrecedent() == "RIGHT" && sens_depart == "S" && GO) {
					train.setSens("E");
					train.setMovePrecedent("LEFT");
					GO = false;
				}
			}
			if (or == Orientation.SLASH) {
				if (sens_depart == "E") {
					train.setSens("N");
				}
				if (sens_depart == "O") {
					train.setSens("S");
				}
				if (sens_depart == "N") {
					train.setSens("E");
				}
				if (sens_depart == "S") {
					train.setSens("O");
				}
			}
			if (or == Orientation.ANTISLASH) {
				if (sens_depart == "E") {
					train.setSens("S");
				}
				if (sens_depart == "O") {
					train.setSens("N");
				}
				if (sens_depart == "N") {
					train.setSens("O");
				}
				if (sens_depart == "S") {
					train.setSens("E");
				}
			}
			if (rechercheCollision(trains)) {
				collision = true;
			}
			;
		}

		return collision;
	}

	private Orientation rechercheOrientationRail(int coord_x, int coord_y, ArrayList<Rail> rails) {
		Orientation or = null;
		for (Rail rail : rails) {
			if (rail.getCoord_x() == coord_x && rail.getCoord_y() == coord_y) {
				or = rail.getOrientation();
			}
		}
		return or;
	}

	private void importation(ArrayList<Rail> rails, ArrayList<Train> trains) throws FileNotFoundException {
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day13/day13.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		// System.out.println("input: "+input);

		// cr�ation des rails et trains

		for (int i = 0; i < input.size(); i++) {
			String ligne = input.get(i);
			for (int j = 0; j < ligne.length(); j++) {
				if (ligne.charAt(j) == '-') {
					Rail rail = new Rail(j, i, Orientation.EO);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '|') {
					Rail rail = new Rail(j, i, Orientation.NS);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '/') {
					Rail rail = new Rail(j, i, Orientation.SLASH);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '\\') {
					Rail rail = new Rail(j, i, Orientation.ANTISLASH);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '+') {
					Rail rail = new Rail(j, i, Orientation.INTERSECTION);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '>') { // On doit cr�er un rail quand on cr�e un train.
					Train train = new Train(j, i, "E");
					trains.add(train);
					Rail rail = new Rail(j, i, Orientation.EO);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '<') { // On doit cr�er un rail quand on cr�e un train.
					Train train = new Train(j, i, "O");
					trains.add(train);
					Rail rail = new Rail(j, i, Orientation.EO);
					rails.add(rail);
				}
				if (ligne.charAt(j) == '^') { // On doit cr�er un rail quand on cr�e un train.
					Train train = new Train(j, i, "N");
					trains.add(train);
					Rail rail = new Rail(j, i, Orientation.NS);
					rails.add(rail);
				}
				if (ligne.charAt(j) == 'v') { // On doit cr�er un rail quand on cr�e un train.
					Train train = new Train(j, i, "S");
					trains.add(train);
					Rail rail = new Rail(j, i, Orientation.NS);
					rails.add(rail);
				}
			}
		}
	}

	// fonction qui ordonne une liste de train selon: (1. la coordonn�e x, 2. la coordonn�e y) de chaque train
	@SuppressWarnings("unchecked")
	public static void order(ArrayList<Train> trains) {

		Collections.sort(trains, new Comparator() {

			public int compare(Object o1, Object o2) {

				Integer y1 = ((Train) o1).getCoord_y();
				Integer y2 = ((Train) o2).getCoord_y();
				int sComp = y1.compareTo(y2);

				if (sComp != 0) {
					return sComp;
				}

				Integer x1 = ((Train) o1).getCoord_x();
				Integer x2 = ((Train) o2).getCoord_x();
				return x1.compareTo(x2);
			}
		});
	}
}