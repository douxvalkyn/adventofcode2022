package advent2018.day17;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day17 {
	// constructeur ----
	public Day17() {
	}

	// main ----
	public static void main(String[] args) throws ParseException, IOException {
		Day17 d = new Day17();
		double startTime = System.currentTimeMillis();
		d.run1();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: " + (endTime - startTime) / 1000 + " s");
	}

	// Methodes ----
	
	public  void run1() throws ParseException, IOException {
		Day17 d = new Day17();
		String file = "day17";
		d.run1_(file);
	}

	
	
	private void run1_(String file) throws FileNotFoundException {
		char[][] grille2 = new char[1800][600]; // grille jumelle
		char[][] grille = importation(file);
		// int xmax=586, ymax=1739; // selon l'input reel
		int x = 500;
		int y = 0;

		// nettoyage de la grille2
		for (int index = 0; index <= 1000; index++) {
			for (int i = 0; i < 599; i++) {
				for (int j = 0; j < 1800; j++) {
					grille2[j][i] = ' ';
				}
			}

			coule(grille, x, y, grille2);
			stabilisation(grille);
		}
		visualisation(grille, 1740, 600);
		System.out.println("compteur Eau: " + compteurEau(grille));
		System.out.println("compteur Eau Stagnante: " + compteurEauStagnante(grille));

	}

	private int compteurEauStagnante(char[][] grille) {
		int cpt2 = 0;
		for (int i = 0; i < 600; i++) {
			for (int j = 7; j <= 1739; j++) {
				if (grille[j][i] == '~') {
					cpt2++;
				}
			}
		}
		return cpt2;
	}

	private int compteurEau(char[][] grille) {
		int cpt = 0;
		for (int i = 0; i < 600; i++) {
			for (int j = 7; j <= 1739; j++) {
				if (grille[j][i] == '|' | grille[j][i] == '!' | grille[j][i] == '~') {
					cpt++;
				}
			}
		}
		return cpt;
	}

	private void stabilisation(char[][] grille) {
		for (int i = 0; i < 599; i++) {
			for (int j = 0; j < 1800; j++) {
				if (grille[j][i] == '#' & grille[j][i + 1] == '!') {
					// on recherche un autre mur
					boolean go = true;
					int k = 0;
					int l = 0;
					while (go) {
						if (i + 1 + k == 188) {
							go = false;
						}
						if (grille[j][i + 1 + k] != '!' & grille[j][i + 1 + k] != '#' & grille[j][i + 1 + k] != '~') {
							go = false;
						}
						if (grille[j][i + 1 + k] == '!') {
							k = k + 1;
						}
						if (grille[j][i + 1 + k] == '#') {
							go = false;
							while (l < k) {
								l = l + 1;
								grille[j][i + l] = '~';
							}
						}
					}
				}
			}
		}
		for (int i = 0; i < 600; i++) {
			for (int j = 0; j < 1800; j++) {
				if (grille[j][i] == '!') {
					grille[j][i] = '|';
				}
			}
		}

	}

	private void coule(char[][] grille, int x, int y, char[][] grille2) {
		// l'eau couleur vers le bas si elle n'est pas bloqu�e
		int ymax = grille.length;
		int xmax = grille[0].length;

		if (y + 1 == ymax) {
			return;
		} // on atteint la limite

		// si la case au sud est libre, l'eau y coule
		if (grille[y + 1][x] != '#' & grille[y + 1][x] != '~') {
			// System.out.println("1");System.out.println(x);System.out.println(y);System.out.println("---");
			if (grille2[y + 1][x] != 'X') {
				grille[y + 1][x] = '!';
				grille2[y + 1][x] = 'X';
				coule(grille, x, y + 1, grille2);
			}
			return;

		}

		// si la case au sud n'est pas libre et la case � l'est n'est pas libre, l'eau coule vers l'ouest
		if ((grille[y + 1][x] == '#' | grille[y + 1][x] == '!' | grille[y + 1][x] == '~') & (grille[y][x + 1] == '#' | grille[y][x + 1] == '!') & (grille[y][x - 1] != '#' & grille[y][x - 1] != '!')) {
			// System.out.println("2");System.out.println(x);System.out.println(y);System.out.println("---");
			if (grille2[y][x - 1] != 'X') {
				if (grille[y + 1][x - 1] != '#' & grille[y + 1][x - 1] != '!') {
					grille[y][x - 1] = '!';
					grille2[y][x - 1] = 'X';
				} else {
					grille[y][x - 1] = '!';
					grille2[y][x - 1] = 'X';
				}
				// visualisation(grille,30,530);
				coule(grille, x - 1, y, grille2);
			}
			return;
		}

		// si la case au sud n'est pas libre et la case � l'ouest n'est pas libre, l'eau coule vers l'est
		if ((grille[y + 1][x] == '#' | grille[y + 1][x] == '!' | grille[y + 1][x] == '~') & (grille[y][x - 1] == '#' | grille[y][x - 1] == '!') & (grille[y][x + 1] != '#' & grille[y][x + 1] != '!')) {
			// System.out.println("3");System.out.println(x);System.out.println(y);System.out.println("---");
			if (grille2[y][x + 1] != 'X') {
				if (grille[y + 1][x + 1] != '#' & grille[y + 1][x + 1] != '!') {
					grille[y][x + 1] = '!';
					grille2[y][x + 1] = 'X';
				} else {
					grille[y][x + 1] = '!';
					grille2[y][x + 1] = 'X';
				}
				// visualisation(grille,30,530);
				coule(grille, x + 1, y, grille2);
			}
			return;
		}

		// si la case au sud n'est pas libre l'eau coule vers l'est et l'ouest
		if ((grille[y + 1][x] == '#' | grille[y + 1][x] == '!' | grille[y + 1][x] == '~') & (grille[y][x - 1] != '#' | grille[y][x - 1] != '!') & (grille[y][x + 1] != '#' & grille[y][x + 1] != '!')) {
			// System.out.println("4");System.out.println(x);System.out.println(y);System.out.println("---");
			if (grille2[y][x - 1] != 'X' & grille2[y][x + 1] != 'X') {
				// si au sud est ou nord ouest la case est vide, on fait un ! car l'eau ne stagnera pas
				if (grille[y + 1][x + 1] != '#' & grille[y + 1][x + 1] != '!') {
					grille[y][x + 1] = '!';
					grille2[y][x + 1] = 'X';
				} else {
					grille[y][x + 1] = '!';
					grille2[y][x + 1] = 'X';
				}
				if (grille[y + 1][x - 1] != '#' & grille[y + 1][x - 1] != '!') {
					grille[y][x - 1] = '!';
					grille2[y][x - 1] = 'X';
				} else {
					grille[y][x - 1] = '!';
					grille2[y][x - 1] = 'X';
				}
				// visualisation(grille,30,530);

				coule(grille, x + 1, y, grille2);
				coule(grille, x - 1, y, grille2);
			}
			return;
		}

		// si la case au sud n'est pas libre , de l'eau qui coule � l'est et l'ouest, l'eau coule encore vers l'est et l'ouest
		if ((grille[y + 1][x] == '#' | grille[y + 1][x] == '~') & (grille[y][x - 1] == '!') & (grille[y][x + 1] == '!') & grille2[y][x - 1] != 'X' & grille2[y][x + 1] != 'X') {
			// System.out.println("5");System.out.println(x);System.out.println(y);System.out.println("---");
			// visualisation(grille,100,600);
			grille2[y][x + 1] = 'X';
			grille2[y][x - 1] = 'X';
			coule(grille, x + 1, y, grille2);
			coule(grille, x - 1, y, grille2);
			return;
		}

	}

	private void visualisation(char[][] grille, int imax, int jmax) {
		String str = "";
		for (int i = 0; i < imax; i++) {
			for (int j = 0; j < jmax; j++) {
				System.out.print(grille[i][j]);
				str = str + grille[i][j];
			}
			System.out.println();
			str = str + "\n";
		}
	}

	private char[][] importation(String file) throws FileNotFoundException {
		// Import input
		Scanner infile = new Scanner(new FileReader("src/main/java/_2018/advent2018/day17/day17.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}
		System.out.println(input);
		char[][] grille = new char[1800][600];
		int xmax = 0;
		int ymax = 0;
		grille[0][500] = '+'; // water source
		for (String ligne : input) {
			char xy = ligne.charAt(0);
			String[] splited = ligne.split(",");
			int requiredString0 = Integer.parseInt(splited[0].substring(splited[0].indexOf("=") + 1));
			int requiredString1 = Integer.parseInt(splited[1].substring(splited[1].indexOf("=") + 1, splited[1].indexOf(".")));
			int requiredString2 = Integer.parseInt(splited[1].substring(splited[1].indexOf(".") + 2));
			if (xy == 'x') {
				if (requiredString1 > ymax) {
					ymax = requiredString1;
				}
				if (requiredString2 > ymax) {
					ymax = requiredString2;
				}
				for (int j = requiredString1; j <= requiredString2; j++) {
					grille[j][requiredString0] = '#';
				}
			}
			if (xy == 'y') {
				if (requiredString1 > xmax) {
					xmax = requiredString1;
				}
				if (requiredString2 > xmax) {
					xmax = requiredString2;
				}
				for (int i = requiredString1; i <= requiredString2; i++) {
					grille[requiredString0][i] = '#';
				}
			}
		}
		System.out.println("xmax: " + xmax);
		System.out.println("ymax: " + ymax);
		return grille;
	}

}
