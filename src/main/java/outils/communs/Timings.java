package communs;

import java.io.IOException;

public class Timings {
	
	
	public static void main(String[] args) throws IOException {
		int annee = 2022;
		
		run(annee);
	}

	private static void run(int annee) throws IOException {
		if (annee==2016) {
			advent2016.outils.Timings t =new advent2016.outils.Timings();
			t.main(null);
		}
		if (annee==2017) {
			advent2017.outils.Timings t =new advent2017.outils.Timings();
			t.main(null);
		}
		if (annee==2021) {
			advent2021.outils.Timings t =new advent2021.outils.Timings();
			t.main(null);
		}
		if (annee==2022) {
			advent2022.outils.Timings t =new advent2022.outils.Timings();
			t.main(null);
		}
	}
}