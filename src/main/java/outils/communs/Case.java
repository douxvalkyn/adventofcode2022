package communs;

import java.util.ArrayList;
import java.util.List;

public class Case {

	private int x;
	private int y;
	private int rang = 0;
	private int valeur;
	private String etat;

	public Case(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.valeur = 0;
		this.etat = "_";

	}

	/* constructeur copie > pour créer un clone d'une case */
	public Case(Case cell) {
		this.x = cell.x;
		this.y = cell.y;
		this.valeur = cell.valeur;
		this.etat = cell.etat;
		this.rang = cell.rang;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtatPlusUn() {
		this.valeur = this.valeur + 1;
	}

	@Override
	public String toString() {
		return "Case" + rang + "[x=" + x + ", y=" + y + "] (" + valeur + ")" + "(" + etat + ")";
	}

	/**
	 *
	 * @param grille
	 * @return cases adjacentes ROOK si existe: N S E W
	 */
	// adjacence Rook
	public List<Case> getCasesAdjacentes(Grille grille) {
		x = this.getX();
		y = this.getY();
		Case nord = grille.getCase(x, y - 1);
		Case sud = grille.getCase(x, y + 1);
		Case est = grille.getCase(x + 1, y);
		Case ouest = grille.getCase(x - 1, y);
		List<Case> casesAdj = new ArrayList<Case>();
		if (nord != null) {
			casesAdj.add(nord);
		}
		if (sud != null) {
			casesAdj.add(sud);
		}
		if (est != null) {
			casesAdj.add(est);
		}
		if (ouest != null) {
			casesAdj.add(ouest);
		}
		return casesAdj;
	}

	public List<Case> getCasesAdjacentesNordTouteLaLigneJusquAuBord(Grille grille) {

		int x = this.getX();
		int y = this.getY();

		Case nord = null;
		List<Case> casesNord = new ArrayList<Case>();
		nord = grille.getCase(x, y - 1);
		while (nord != null) {
			nord = grille.getCase(x, y - 1);
			if (nord != null) {
				casesNord.add(nord);
			}
			y = y - 1;
		}
		// supprime le bord N
		// if (casesNord.size() - 1 >= 0)
		// casesNord.remove(casesNord.size() - 1);

		return casesNord;
	}

	public List<Case> getCasesAdjacentesSudTouteLaLigneJusquAuBord(Grille grille) {

		int x = this.getX();
		int y = this.getY();

		Case sud = null;
		List<Case> casesSud = new ArrayList<Case>();
		sud = grille.getCase(x, y + 1);
		while (sud != null) {
			sud = grille.getCase(x, y + 1);
			if (sud != null) {
				casesSud.add(sud);
			}
			y = y + 1;
		}
		// supprime le bord
		// if (casesSud.size() - 1 >= 0)
		// casesSud.remove(casesSud.size() - 1);

		return casesSud;
	}

	public List<Case> getCasesAdjacentesEstTouteLaLigneJusquAuBord(Grille grille) {

		int x = this.getX();
		int y = this.getY();

		Case est = null;
		List<Case> casesEst = new ArrayList<Case>();
		est = grille.getCase(x + 1, y);
		while (est != null) {
			est = grille.getCase(x + 1, y);
			if (est != null) {
				casesEst.add(est);
			}
			x = x + 1;
		}
		// supprime le bord
		// if (casesEst.size() - 1 >= 0)
		// casesEst.remove(casesEst.size() - 1);

		return casesEst;
	}

	public List<Case> getCasesAdjacentesOuestTouteLaLigneJusquAuBord(Grille grille) {

		int x = this.getX();
		int y = this.getY();

		Case ouest = null;
		List<Case> casesOuest = new ArrayList<Case>();
		ouest = grille.getCase(x - 1, y);
		while (ouest != null) {
			ouest = grille.getCase(x - 1, y);
			if (ouest != null) {
				casesOuest.add(ouest);
			}
			x = x - 1;
		}
		// // supprime le bord
		// if (casesOuest.size() - 1 >= 0)
		// casesOuest.remove(casesOuest.size() - 1);

		return casesOuest;
	}

	/**
	 *
	 * @param grille
	 * @return cases adjacentes ROOK : N S E W si non existence, renvoie un element null dans la liste
	 */
	// adjacence Rook
	public List<Case> getCasesAdjacentes2(Grille grille) {
		x = this.getX();
		y = this.getY();
		Case nord = grille.getCase(x, y - 1);
		Case sud = grille.getCase(x, y + 1);
		Case est = grille.getCase(x + 1, y);
		Case ouest = grille.getCase(x - 1, y);
		List<Case> casesAdj = new ArrayList<Case>();
		if (nord != null) {
			casesAdj.add(nord);
		} else {
			casesAdj.add(null);
		}
		if (sud != null) {
			casesAdj.add(sud);
		} else {
			casesAdj.add(null);
		}
		if (est != null) {
			casesAdj.add(est);
		} else {
			casesAdj.add(null);
		}
		if (ouest != null) {
			casesAdj.add(ouest);
		} else {
			casesAdj.add(null);
		}
		return casesAdj;
	}

	// adjacence Queen
	/**
	 * 
	 * @param grille
	 * @return NN SS EE OO NO NE SO SE
	 */
	public List<Case> getCasesAdjacentesQueen(Grille grille) {
		x = this.getX();
		y = this.getY();
		Case nn = grille.getCase(x, y - 1);
		Case ss = grille.getCase(x, y + 1);
		Case ee = grille.getCase(x + 1, y);
		Case oo = grille.getCase(x - 1, y);
		Case no = grille.getCase(x - 1, y - 1);
		Case ne = grille.getCase(x + 1, y - 1);
		Case so = grille.getCase(x - 1, y + 1);
		Case se = grille.getCase(x + 1, y + 1);
		List<Case> casesAdj = new ArrayList<Case>();
		if (nn != null) {
			casesAdj.add(nn);
		}
		if (ss != null) {
			casesAdj.add(ss);
		}
		if (ee != null) {
			casesAdj.add(ee);
		}
		if (oo != null) {
			casesAdj.add(oo);
		}
		if (no != null) {
			casesAdj.add(no);
		}
		if (ne != null) {
			casesAdj.add(ne);
		}
		if (so != null) {
			casesAdj.add(so);
		}
		if (se != null) {
			casesAdj.add(se);
		}
		return casesAdj;
	}

}
