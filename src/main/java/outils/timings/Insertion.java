package timings;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Insertion {

	public static void main(String[] args) throws IOException, ParseException {
		
		// ----------------------PARAMETRE ANNEE A MODIFIER --------------------------------------------
		 int annee=2018;
		// ----------------------PARAMETRE ANNEE A MODIFIER --------------------------------------------

		// insertion donnees
		List<List<Long>> durees = run(annee);
		List<Long> dureesRun1 = durees.get(0);
		List<Long> dureesRun2 = durees.get(1);

		List<Advent> advents = new ArrayList<>();
		for (int i = 1; i <= dureesRun1.size(); i++) {
			Advent adventRun1 = new Advent();
			adventRun1.setAnnee(annee);
			adventRun1.setDay(i);
			adventRun1.setRun(1);
			adventRun1.setTemps(dureesRun1.get(i - 1));
			Advent adventRun2 = new Advent();
			adventRun2.setAnnee(annee);
			adventRun2.setDay(i);
			adventRun2.setRun(2);
			adventRun2.setTemps(dureesRun2.get(i - 1));
			if (adventRun1.getTemps()>0) {
				adventRun1.setStatus("OK");
			}
			if (adventRun2.getTemps()>0) {
				adventRun2.setStatus("OK");
			}

			advents.add(adventRun1);
			advents.add(adventRun2);
		}

		insertions(advents);
		ManagerAdvent mgr = new ManagerAdvent();
		List<Advent> resultats = mgr.getAdvent(annee);
		System.out.println(resultats);
	}

	private static void insertion(Advent advent) {
		ManagerAdvent manager = new ManagerAdvent();
		manager.insertAdvent(advent);
	}

	//TODO
	private static void insertionAdventSiMeilleureDonnee (Advent advent) {
		ManagerAdvent manager = new ManagerAdvent();
		List<Advent> advents = manager.getAdvent(advent.getAnnee());
		for (Advent adv:advents) {
			if (adv.getDay()==advent.getDay() &&  adv.getRun()== advent.getRun() && adv.getTemps() > advent.getTemps()) {
					adv.setTemps(advent.getTemps());
					manager.insertAdvent(adv);
			}
		}
		
		//delete la ligne de l'année avant d'inserer
		manager.deleteAnnee(advents.get(0).getAnnee());
		manager.insertAdvents(advents);
	}
	
	
	
	private static void insertions(List<Advent> advents) {
		ManagerAdvent manager = new ManagerAdvent();
		//delete les evenements de l'année avant d'inserer
		manager.deleteAnnee(advents.get(0).getAnnee());
		manager.insertAdvents(advents);
	}

	private static List<List<Long>> run(int annee) throws IOException, ParseException {
		if (annee == 2016) {
			advent2016.outils.Timings t = new advent2016.outils.Timings();
			t.main(null);
			List<List<Long>> durees = new ArrayList<List<Long>>();
			durees.add(t.dureesRun1);
			durees.add(t.dureesRun2);
			return durees;
		}
		if (annee == 2017) {
			advent2017.outils.Timings t = new advent2017.outils.Timings();
			t.main(null);
			List<List<Long>> durees = new ArrayList<List<Long>>();
			durees.add(t.dureesRun1);
			durees.add(t.dureesRun2);
			return durees;
		}
		if (annee == 2018) {
			advent2018.outils.Timings t = new advent2018.outils.Timings();
			t.main(null);
			List<List<Long>> durees = new ArrayList<List<Long>>();
			durees.add(t.dureesRun1);
			durees.add(t.dureesRun2);
			return durees;
		}
		if (annee == 2021) {
			advent2021.outils.Timings t = new advent2021.outils.Timings();
			t.main(null);
			List<List<Long>> durees = new ArrayList<List<Long>>();
			durees.add(t.dureesRun1);
			durees.add(t.dureesRun2);
			return durees;
		}
		if (annee == 2022) {
			advent2022.outils.Timings t = new advent2022.outils.Timings();
			t.main(null);
			List<List<Long>> durees = new ArrayList<List<Long>>();
			durees.add(t.dureesRun1);
			durees.add(t.dureesRun2);
			return durees;
		}
		return null;
	}

}
