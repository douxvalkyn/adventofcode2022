package timings;

import java.util.List;

import org.hibernate.query.Query;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class ManagerAdvent {


	public ManagerAdvent() {
		super();
	}

	public List<Advent> getAdvent(int valeur) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPADemo");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
	    entityManager.getTransaction().begin();
		List<Advent> resultats = null;
		try {
			org.hibernate.query.Query<Advent> query = (Query<Advent>) entityManager.createNamedQuery("Advent_findByAnnee", Advent.class);
			query.setParameter("annee", valeur);
			resultats = query.getResultList();
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.close();
		    entityManagerFactory.close();
		}

		return resultats;

	}

	public void insertAdvent(Advent advent) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPADemo");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
	    entityManager.getTransaction().begin();
		try {
			entityManager.persist(advent);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.close();
		    entityManagerFactory.close();
		}
	}

	public void insertAdvents(List<Advent> advents) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPADemo");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
	    entityManager.getTransaction().begin();
		try {
			for (int i = 0; i < advents.size(); i++) {
				entityManager.persist(advents.get(i));
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.close();
		    entityManagerFactory.close();
		}
	}

	public void deleteAnnee(int annee) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPADemo");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
	    entityManager.getTransaction().begin();
		try {
			org.hibernate.query.Query<Advent> query = (Query<Advent>) entityManager.createNamedQuery("Advent_findByAnnee", Advent.class);
			query.setParameter("annee", annee);
			List<Advent> resultats = query.getResultList();
			for (Advent advent:resultats) {
			entityManager.remove(advent);
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.close();
		    entityManagerFactory.close();
		}
		
	}

}
