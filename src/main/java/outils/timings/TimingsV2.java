package timings;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class TimingsV2 {
	
	private static final String ANSI_RESET="\u001B[0m";
	private static final String ANSI_RED="\u001B[31m";
	private static final String ANSI_GREEN="\u001B[32m";
	private static final String ANSI_BRIGHT_GREEN="\u001B[92m";
	private static final String ANSI_YELLOW="\u001B[33m";
	private static final String ANSI_PURPLE="\u001B[35m";
	private static final String ANSI_BLUE="\u001B[34m";
	private static final String ANSI_BRIGHT_MAGENTA="\u001B[95m";
	private static final String  YELLOW_BRIGHT = "\033[0;93m";
	

	public static void main(String[] args) {

		// ------------------------- Parametre annee à modifier ------------------------
		int annee=2019;
		// ------------------------- Parametre annee à modifier ------------------------
		
		
		//Affichage des valeurs de l'année considérée
		List<Advent> advents = getAdvents(annee);
		affichageCouleur(advents);
		
	}

	
	private static  List<Advent> getAdvents(int annee) {
		ManagerAdvent manager= new ManagerAdvent();
		return manager.getAdvent(annee);
	}

	private static void affichageCouleur(List<Advent> advents) {
		int annee=advents.get(0).getAnnee();
		List<Long> dureesRun1 = new ArrayList<Long>();
		List<Long> dureesRun2 = new ArrayList<Long>();
		
		for (int i=1;i<=25;i++) { //on passe en revue les 25 jours potentiels
			boolean existe1=false;
			boolean existe2=false;
			for (Advent adv:advents) {
				if (adv.getDay()==i && adv.getRun()==1 && adv.getStatus().equals("OK")) {
					dureesRun1.add(adv.getTemps());
					existe1=true;
				}
				if (adv.getDay()==i && adv.getRun()==2 && adv.getStatus().equals("OK")) {
					dureesRun2.add(adv.getTemps());
					existe2=true;
			}
		}
			if (!existe1) {
				dureesRun1.add(null);
			}
			if (!existe2) {
				dureesRun2.add(null);
			}
	}
		
		
		
		List<String> DaysAffichage = new ArrayList<String>();
		DaysAffichage.add("1 ");		DaysAffichage.add("2 ");		DaysAffichage.add("3 ");		DaysAffichage.add("4 ");		DaysAffichage.add("5 ");
		DaysAffichage.add("6 ");		DaysAffichage.add("7 ");		DaysAffichage.add("8 ");		DaysAffichage.add("9 ");		DaysAffichage.add("10 ");
		DaysAffichage.add("11 ");		DaysAffichage.add("12 ");		DaysAffichage.add("13 ");		DaysAffichage.add("14 ");		DaysAffichage.add("15 ");
		DaysAffichage.add("16 ");		DaysAffichage.add("17 ");		DaysAffichage.add("18 ");		DaysAffichage.add("19 ");		DaysAffichage.add("20 ");
		DaysAffichage.add("21 ");		DaysAffichage.add("22 ");		DaysAffichage.add("23 ");		DaysAffichage.add("24 ");		DaysAffichage.add("25 ");

		System.out.println("-----------------------------------------------------------------");
		System.out.println("-----------------------"+ANSI_BLUE+"Advent of Code "+annee+ANSI_RESET+"-----------------------");
		System.out.println("-----------------------------"+ANSI_BLUE+"Timings"+ANSI_RESET+"-----------------------------");
		System.out.println("-----------------------------------------------------------------");
		affichageCouleurDays2(affichageCouleurDays1(advents), DaysAffichage);
		DaysAffichage.forEach(System.out::print);
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		for (int i=0; i<dureesRun1.size();i++) {
			String calculString1="";
			if (dureesRun1.get(i)==null) {
				calculString1="X";
			}else {
			long calcul1 = dureesRun1.get(i);
			calculString1 = ""+calcul1;
			}
			String calculString2="";
			if (dureesRun2.get(i)==null) {
				calculString2="X";
			}else {
			long calcul2 = dureesRun2.get(i);
			calculString2 = ""+calcul2;
			}
			if (i<9) {
			System.out.println("[ Day"+(i+1)+" -  Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
			if (i>=9) {
			System.out.println("[ Day"+(i+1)+" - Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
		}
		long sum = 0;
		for (Long dur : dureesRun1) {
			if (dur!=null) {
			sum=sum+dur;
			}
			
		}
		for (Long dur : dureesRun2) {
			if (dur!=null) {
			sum=sum+dur;
			}
		}
		long sumSecondes=sum/1000;
		long minutes=sumSecondes/60;
		long heures=minutes/60;
		System.out.println("[ Total:      " + sum + " ms soit "+ sumSecondes +" s soit "+minutes + " min soit "+ heures+" h ]");
	}
	
	
	private static void affichageCouleurDays2(List<Integer> affichageCouleurDays, List<String> DaysAffichage) {
		for (int i=0;i<DaysAffichage.size();i++) {
			if (affichageCouleurDays.size()>i) {
			if (affichageCouleurDays.get(i)==1) {
				DaysAffichage.set(i, YELLOW_BRIGHT+DaysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==2) {
				DaysAffichage.set(i, ANSI_BRIGHT_GREEN+DaysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==0) {
				DaysAffichage.set(i, ANSI_RED+DaysAffichage.get(i)+ANSI_RESET);
			}
		}
		}
	}



	private static List<Integer> affichageCouleurDays1(List<Advent>advents) {
		List<Integer> daysWinOrLoose = new ArrayList<Integer>(); //0 1 2
	for (int i=1;i<=25;i++) {
		int winOrLoose=0;
		for (Advent advent:advents) {
			if (advent.getDay()==i) {
				if (advent.getStatus().equals("OK")) {
					winOrLoose++;
				}
			}
		}
		daysWinOrLoose.add(winOrLoose);
	}
			
		
		return daysWinOrLoose;
	}


	private static String colored(String calculString) {
	String res = "";
	if (calculString.equals("X")) {
		return ANSI_BRIGHT_MAGENTA+calculString+ANSI_RESET;
	}
		if (Integer.parseInt(calculString)<1000) {
			res=ANSI_BRIGHT_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000 & Integer.parseInt(calculString)<10000) {
			res=ANSI_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=10000 & Integer.parseInt(calculString)<100000) {
			res=ANSI_YELLOW+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=100000 & Integer.parseInt(calculString)<1000000) {
			res=ANSI_RED+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000000 & Integer.parseInt(calculString)<10000000) {
			res=ANSI_PURPLE+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>1000000) {
			res=ANSI_BRIGHT_MAGENTA+calculString+ANSI_RESET;
		}
		return res;
	}
	
	
}
