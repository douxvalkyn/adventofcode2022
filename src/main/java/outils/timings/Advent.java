package timings;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@org.hibernate.annotations.NamedQuery(name = "Advent_findByAnnee", query = "from Advent where annee = :annee")
@Entity
@Table(name = "advent")
public class Advent {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
    @Column(name = "id")
    private int id;

    @Column(name="annee")
    private int annee;

    @Column(name="day")
    private int day;

    @Column(name="run")
    private int run;
    
    @Column(name="temps")
    private long temps;
    


	@Column(name="status")
    private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getRun() {
		return run;
	}

	public void setRun(int run) {
		this.run = run;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
    public long getTemps() {
		return temps;
	}

	public void setTemps(Long dureeRun1) {
		this.temps = dureeRun1;
	}

	@Override
	public String toString() {
		return "Advent [id=" + id + ", annee=" + annee + ", day=" + day + ", run=" + run + ", temps=" + temps
				+ ", status=" + status + "]";
	}



	public Advent(int annee, int day, int run, int temps, String status) {
		super();
		this.annee = annee;
		this.day = day;
		this.run = run;
		this.temps = temps;
		this.status = status;
	}

	public Advent() {
		super();
	}
    
 
    
    
}