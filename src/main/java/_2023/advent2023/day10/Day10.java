package advent2023.day10;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day10 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day10.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day10.class.getSimpleName());
		Day10 day = new Day10();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day10.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());

		initGrille(fichier, grille);
		List<Case> parcours = reperageParcours(grille);
		analyseGrille(grille, parcours);

	}

	private void analyseGrille(Grille grille, List<Case> parcours) {
		int sum = 0;
		for (int i = 0; i < grille.getNbLignes(); i++) {
			for (int j = 0; j < grille.getNbCol(); j++) {
				// on parcourt chaque case
				// si on est hors du parcours, on compte � gauche le nb de |
				// si nb impair et pas de - on est � l'int�rieur
				if (!parcours.contains(grille.getCase(j, i))) {
					List<Case> casesGauche = grille.getCase(j, i).getCasesAdjacentesOuestTouteLaLigneJusquAuBord(grille);
					int nbLignesHorizontales = 0;
					int nbLignesVerticales = 0;
					if (grille.getCase(j, i).getX() == 7 && grille.getCase(j, i).getY() == 4) {
						System.err.println();
					}
					for (Case cell : casesGauche) {

						// compter nb |
						if (cell.getEtat().equals("|") && parcours.contains(cell)) {
							nbLignesVerticales++;
						}
						// compter toutes possibilites de L-----7
						if (cell.getEtat().equals("L") && analyseL7(cell, grille, j) && parcours.contains(cell)) {
							nbLignesVerticales++;
						}
						// compter toutes possibilites de F---J
						if (cell.getEtat().equals("F") && analyseFJ(cell, grille, j) && parcours.contains(cell)) {
							nbLignesVerticales++;
						}
					}
					if (nbLignesVerticales > 0 && nbLignesVerticales % 2 == 1) {
						System.err.println(grille.getCase(j, i));
						sum++;
					}

				}

			}
		}
		System.err.println(sum);
	}

	private boolean analyseFJ(Case cell, Grille grille, int xMax) {
		// on recherche un �ventuel J apres un/des - en allant vers l'est
		int xMin = cell.getX();
		boolean analyse = true;
		for (int k = xMin; k < xMax; k++) {
			if (cell.getCasesAdjacentes2(grille).get(2) != null && cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("J")) {
				analyse = analyse && true;
				return analyse;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null && cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("-")) {
				analyse = analyse && true;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null && !cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("-")) {
				analyse = false;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null) {
				cell = cell.getCasesAdjacentes2(grille).get(2);
			}

		}
		return analyse;
	}

	private boolean analyseL7(Case cell, Grille grille, int xMax) {
		// on recherche un �ventuel 7 apres un/des - en allant vers l'est
		int xMin = cell.getX();
		boolean analyse = true;
		for (int k = xMin; k < xMax; k++) {
			if (cell.getCasesAdjacentes2(grille).get(2) != null && cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("7")) {
				analyse = analyse && true;
				return analyse;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null && cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("-")) {
				analyse = analyse && true;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null && !cell.getCasesAdjacentes2(grille).get(2).getEtat().equals("-")) {
				analyse = false;
			}
			if (cell.getCasesAdjacentes2(grille).get(2) != null) {
				cell = cell.getCasesAdjacentes2(grille).get(2);
			}

		}
		return analyse;
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day10a.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());

		initGrille(fichier, grille);
		reperageParcours(grille);
	}

	private List<Case> reperageParcours(Grille grille) {

		// reperage Start
		int xStart = 0;
		int yStart = 0;
		Case cellStart = null;
		for (Case cell : grille.getCases()) {
			if (cell.getEtat().equals("S")) {
				xStart = cell.getX();
				yStart = cell.getY();
				cellStart = cell;
			}
		}

		// recherche pipes connectees
		Case cellEnCours = cellStart;
		List<Case> casesAdjacentes = cellEnCours.getCasesAdjacentes2(grille);

		// sauvegarde parcours de la boucle
		List<Case> parcours = new ArrayList<Case>();
		parcours.add(cellEnCours);

		boolean boucleTerminee = false;

		while (!boucleTerminee) {
			Case nextCell = rechercheCaseSuivante(grille, cellEnCours, parcours);
			if (nextCell != null) {
				parcours.add(nextCell);
				cellEnCours = nextCell;
				boucleTerminee = boucleTerminee(parcours);
			} else {
				boucleTerminee = true;
			}

		}
		// System.out.println("Run1: " + (parcours.size() - 1) / 2);

		return parcours;

	}

	private boolean boucleTerminee(List<Case> parcours) {
		int sum = 0;
		for (Case cell : parcours) {
			if ("S".equals(cell.getEtat())) {
				sum++;
			}
		}
		if (sum == 2) {
			return true;
		}
		return false;
	}

	private Case rechercheCaseSuivante(Grille grille, Case cellEnCours, List<Case> parcours) {
		List<Case> casesAdjacentes = cellEnCours.getCasesAdjacentes2(grille);

		// | is a vertical pipe connecting north and south.
		// - is a horizontal pipe connecting east and west.
		// L is a 90-degree bend connecting north and east.
		// J is a 90-degree bend connecting north and west.
		// 7 is a 90-degree bend connecting south and west.
		// F is a 90-degree bend connecting south and east.
		// . is ground; there is no pipe in this tile.
		// S is the starting position of the animal; there is a pipe on this

		/** premiere case: S */
		if (cellEnCours.getEtat().equals("S")) {
			if (casesAdjacentes.get(0) != null && (casesAdjacentes.get(0).getEtat().equals("|") || casesAdjacentes.get(0).getEtat().equals("7") || casesAdjacentes.get(0).getEtat().equals("F"))) {
				return casesAdjacentes.get(0);
			}
			if (casesAdjacentes.get(1) != null && (casesAdjacentes.get(1).getEtat().equals("|") || casesAdjacentes.get(1).getEtat().equals("J") || casesAdjacentes.get(1).getEtat().equals("L"))) {
				return casesAdjacentes.get(1);
			}
			if (casesAdjacentes.get(2) != null && (casesAdjacentes.get(2).getEtat().equals("-") || casesAdjacentes.get(2).getEtat().equals("J") || casesAdjacentes.get(2).getEtat().equals("7"))) {
				return casesAdjacentes.get(2);
			}
			if (casesAdjacentes.get(3) != null && (casesAdjacentes.get(3).getEtat().equals("-") || casesAdjacentes.get(3).getEtat().equals("L") || casesAdjacentes.get(3).getEtat().equals("F"))) {
				return casesAdjacentes.get(3);
			}
		}

		/** premiere case: | */
		// recherche Nord
		Case caseAdjacenteNord = casesAdjacentes.get(0);
		if (caseAdjacenteNord != null) {
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteNord.getEtat().equals("|") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteNord.getEtat().equals("7") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteNord.getEtat().equals("F") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteNord.getEtat().equals("S") && (!parcours.contains(caseAdjacenteNord) || parcours.size() > 2)) {
				return caseAdjacenteNord;
			}
		}
		// recherche Sud
		Case caseAdjacenteSud = casesAdjacentes.get(1);
		if (caseAdjacenteSud != null) {
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteSud.getEtat().equals("|") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteSud.getEtat().equals("L") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteSud.getEtat().equals("J") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("|") && caseAdjacenteSud.getEtat().equals("S") && (!parcours.contains(caseAdjacenteSud) || parcours.size() > 2)) {
				return caseAdjacenteSud;
			}
		}

		/** premiere case: - */
		// recherche Est
		Case caseAdjacenteEst = casesAdjacentes.get(2);
		if (caseAdjacenteEst != null) {
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteEst.getEtat().equals("-") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteEst.getEtat().equals("J") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteEst.getEtat().equals("7") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteEst.getEtat().equals("S") && (!parcours.contains(caseAdjacenteEst) || parcours.size() > 2)) {
				return caseAdjacenteEst;
			}
		}
		// recherche West
		Case caseAdjacenteWest = casesAdjacentes.get(3);
		if (caseAdjacenteWest != null) {
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteWest.getEtat().equals("-") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteWest.getEtat().equals("L") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteWest.getEtat().equals("F") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("-") && caseAdjacenteWest.getEtat().equals("S") && (!parcours.contains(caseAdjacenteWest) || parcours.size() > 2)) {
				return caseAdjacenteWest;
			}
		}

		/** premiere case: L */
		// recherche Nord
		caseAdjacenteNord = casesAdjacentes.get(0);
		if (caseAdjacenteNord != null) {
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteNord.getEtat().equals("|") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteNord.getEtat().equals("7") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteNord.getEtat().equals("F") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteNord.getEtat().equals("S") && (!parcours.contains(caseAdjacenteNord) || parcours.size() > 2)) {
				return caseAdjacenteNord;
			}
		}
		// recherche Est
		caseAdjacenteEst = casesAdjacentes.get(2);
		if (caseAdjacenteEst != null) {
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteEst.getEtat().equals("-") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteEst.getEtat().equals("7") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteEst.getEtat().equals("J") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("L") && caseAdjacenteEst.getEtat().equals("S") && (!parcours.contains(caseAdjacenteEst) || parcours.size() > 2)) {
				return caseAdjacenteEst;
			}
		}

		/** premiere case: J */
		// recherche Nord
		caseAdjacenteNord = casesAdjacentes.get(0);
		if (caseAdjacenteNord != null) {
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteNord.getEtat().equals("|") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteNord.getEtat().equals("7") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteNord.getEtat().equals("F") && !parcours.contains(caseAdjacenteNord)) {
				return caseAdjacenteNord;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteNord.getEtat().equals("S") && (!parcours.contains(caseAdjacenteNord) || parcours.size() > 2)) {
				return caseAdjacenteNord;
			}
		}

		// recherche West
		caseAdjacenteWest = casesAdjacentes.get(3);
		if (caseAdjacenteWest != null) {
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteWest.getEtat().equals("-") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteWest.getEtat().equals("L") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteWest.getEtat().equals("F") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("J") && caseAdjacenteWest.getEtat().equals("S") && (!parcours.contains(caseAdjacenteWest) || parcours.size() > 2)) {
				return caseAdjacenteWest;
			}
		}

		/** premiere case: 7 */
		// recherche Sud
		caseAdjacenteSud = casesAdjacentes.get(1);
		if (caseAdjacenteSud != null) {
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteSud.getEtat().equals("|") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteSud.getEtat().equals("L") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteSud.getEtat().equals("J") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteSud.getEtat().equals("S") && (!parcours.contains(caseAdjacenteSud) || parcours.size() > 2)) {
				return caseAdjacenteSud;
			}
		}
		// recherche West
		caseAdjacenteWest = casesAdjacentes.get(3);
		if (caseAdjacenteWest != null) {
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteWest.getEtat().equals("-") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteWest.getEtat().equals("L") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteWest.getEtat().equals("F") && !parcours.contains(caseAdjacenteWest)) {
				return caseAdjacenteWest;
			}
			if (cellEnCours.getEtat().equals("7") && caseAdjacenteWest.getEtat().equals("S") && (!parcours.contains(caseAdjacenteWest) || parcours.size() > 2)) {
				return caseAdjacenteWest;
			}
		}

		/** premiere case: F */
		// recherche Sud
		caseAdjacenteSud = casesAdjacentes.get(1);
		if (caseAdjacenteSud != null) {
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteSud.getEtat().equals("|") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteSud.getEtat().equals("L") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteSud.getEtat().equals("J") && !parcours.contains(caseAdjacenteSud)) {
				return caseAdjacenteSud;
			}
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteSud.getEtat().equals("S") && (!parcours.contains(caseAdjacenteSud) || parcours.size() > 2)) {
				return caseAdjacenteSud;
			}
		}
		// recherche Est
		caseAdjacenteEst = casesAdjacentes.get(2);
		if (caseAdjacenteEst != null) {
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteEst.getEtat().equals("-") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteEst.getEtat().equals("7") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("F") && caseAdjacenteEst.getEtat().equals("J") && !parcours.contains(caseAdjacenteEst)) {
				return caseAdjacenteEst;
			}
			if (cellEnCours.getEtat().equals("FL") && caseAdjacenteEst.getEtat().equals("S") && (!parcours.contains(caseAdjacenteEst) || parcours.size() > 2)) {
				return caseAdjacenteEst;
			}
		}

		return null;
	}

	private boolean connexionPossible(Case case1, Case case2) {
		String etat1 = case1.getEtat();
		String etat2 = case2.getEtat();

		if (etat1.equals(".") || etat2.equals(".")) {
			return false;
		}
		if (etat1.equals("S") || etat2.equals("S")) {
			return true;
		}

		if (etat1.equals("|") && etat2.equals("-")) {
			return false;
		}
		if (etat1.equals("-") && etat2.equals("|")) {
			return false;
		}

		return false;
	}

	public void initGrille(List<String> fichier, Grille grille) {
		for (int j = 0; j < fichier.size(); j++) {
			String ligne = fichier.get(j);

			for (int i = 0; i < ligne.length(); i++) {
				grille.getCase(i, j).setEtat("" + ligne.charAt(+i));
			}

		}
		// grille.affichageEtat();
	}

}