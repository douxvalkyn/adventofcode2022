package advent2023.day03;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day03 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day03.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day03.class.getSimpleName());
		Day03 day = new Day03();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day03.txt";
		List<String> res = Outil.importationString(file);
		int sum = 0;
		for (int numLigne = 0; numLigne < res.size(); numLigne++) {
			// on cherche pour chaque ligne les nombres
			// pour chaque nombre on memorise son numColonne de depart et ses numColonne suivants
			// pour chaque numColonne: on cherche autour un symbole
			String[] nombres = StringUtils.split(res.get(numLigne), '.');

			nombres = supprimerSymboles(nombres);
			int dernierIndex = 0;
			for (String nombre : nombres) {
				boolean nombreValide = false;
				if (nombre != null && !nombre.equals("")) {
					// recherche numColonnes pour chaque nombre
					int numColonneDebut = res.get(numLigne).indexOf(nombre, dernierIndex);
					List<Integer> numColonnes = new ArrayList<>();
					for (int i = numColonneDebut - 1; i <= numColonneDebut + nombre.length(); i++) {
						numColonnes.add(i);
					}
					dernierIndex = numColonneDebut + nombre.length();
					// System.out.println(nombre);
					// recherche � gauche du nombre (m�me ligne)
					if (numColonneDebut - 1 >= 0) {
						if (StringUtils.indexOfAny("" + res.get(numLigne).charAt(numColonneDebut - 1),
							new String[] { "*", "&", "#", "{", "(", "[", "-", "|", "_", "^", "@", ")", "=", "+", "/", "%", "!", "�", ":", ";", "?", "$" }) != -1) {
							nombreValide = true;
						}
					}
					// recherche � droite du nombre (m�me ligne)
					try {
						if (StringUtils.indexOfAny("" + res.get(numLigne).charAt(numColonneDebut + nombre.length()),
							new String[] { "*", "&", "#", "{", "(", "[", "-", "|", "_", "^", "@", ")", "=", "+", "/", "%", "!", "�", ":", ";", "?", "$" }) != -1) {
							nombreValide = true;
						}
					} catch (Exception e) {
					}
					// recherche au dessus du nombre (ligne au dessus)
					if (numLigne - 1 > 0) {
						for (Integer num : numColonnes) {
							if (num >= 0 && num < res.get(0).length()) {

								if (StringUtils.indexOfAny("" + res.get(numLigne - 1).charAt(num),
									new String[] { "*", "&", "#", "{", "(", "[", "-", "|", "_", "^", "@", ")", "=", "+", "/", "%", "!", "�", ":", ";", "?", "$" }) != -1) {
									nombreValide = true;
								}
							}
						}
					}

					// recherche en dessous du nombre (ligne en dessous)
					if (numLigne + 1 < res.size()) {
						for (Integer num : numColonnes) {
							if (num >= 0 && num < res.get(0).length()) {

								if (StringUtils.indexOfAny("" + res.get(numLigne + 1).charAt(num),
									new String[] { "*", "&", "#", "{", "(", "[", "-", "|", "_", "^", "@", ")", "=", "+", "/", "%", "!", "�", ":", ";", "?", "$" }) != -1) {
									nombreValide = true;
								}
							}
						}
					}
					if (nombreValide) {
						sum = sum + Integer.parseInt(nombre);
					}
					// System.out.println(nombreValide);
				}
			}
		}
		System.out.println("Run1: " + sum);

	}

	public void run2() {
		String file = "src/main/resources/advent2023/day03.txt";
		List<String> res = Outil.importationString(file);
		long sum = 0L;
		List<Gear> gears = new ArrayList<>();
		for (int numLigne = 0; numLigne < res.size(); numLigne++) {
			String[] nombres = StringUtils.split(res.get(numLigne), '.');
			nombres = supprimerSymboles(nombres);
			int dernierIndex = 0;

			for (String nombre : nombres) {
				boolean nombreValide = false;
				if (nombre != null && !nombre.equals("")) {

					// recherche numColonnes pour chaque nombre
					int numColonneDebut = res.get(numLigne).indexOf(nombre, dernierIndex);
					List<Integer> numColonnes = new ArrayList<>();
					for (int i = numColonneDebut - 1; i <= numColonneDebut + nombre.length(); i++) {
						numColonnes.add(i);
					}
					dernierIndex = numColonneDebut + nombre.length();

					int numColonneEtoile = -1;
					int numLigneEtoile = -1;

					// recherche � gauche du nombre (m�me ligne)
					if (numColonneDebut - 1 >= 0) {
						if (StringUtils.indexOfAny("" + res.get(numLigne).charAt(numColonneDebut - 1), new String[] { "*" }) != -1) {
							nombreValide = true;
							numColonneEtoile = numColonneDebut - 1;
							numLigneEtoile = numLigne;
						}
					}
					// recherche � droite du nombre (m�me ligne)
					try {
						if (StringUtils.indexOfAny("" + res.get(numLigne).charAt(numColonneDebut + nombre.length()), new String[] { "*" }) != -1) {
							nombreValide = true;
							numColonneEtoile = numColonneDebut + nombre.length();
							numLigneEtoile = numLigne;
						}
					} catch (Exception e) {
					}
					// recherche au dessus du nombre (ligne au dessus)
					if (numLigne - 1 > 0) {
						for (Integer num : numColonnes) {
							if (num >= 0 && num < res.get(0).length()) {
								if (StringUtils.indexOfAny("" + res.get(numLigne - 1).charAt(num), new String[] { "*" }) != -1) {
									nombreValide = true;
									numColonneEtoile = num;
									numLigneEtoile = numLigne - 1;
								}
							}
						}
					}

					// recherche en dessous du nombre (ligne en dessous)
					if (numLigne + 1 < res.size()) {
						for (Integer num : numColonnes) {
							if (num >= 0 && num < res.get(0).length()) {
								if (StringUtils.indexOfAny("" + res.get(numLigne + 1).charAt(num), new String[] { "*" }) != -1) {
									nombreValide = true;
									numColonneEtoile = num;
									numLigneEtoile = numLigne + 1;
								}
							}
						}
					}
					if (nombreValide) {
						for (Gear gear : gears) {
							if (gear.getNumColonne() == numColonneEtoile && gear.getNumLigne() == numLigneEtoile) {
								gear.setPart2(Integer.parseInt(nombre));
							}
						}
						Gear newGear = new Gear(numLigneEtoile, numColonneEtoile);
						newGear.setPart1(Integer.parseInt(nombre));
						gears.add(newGear);
					}

				}
			}
		}
		// calcul final
		for (Gear gear : gears) {
			long gearRatio = gear.getPart1() * gear.getPart2();
			sum = sum + gearRatio;
		}
		System.out.println("Run2: " + sum);
	}

	private static String[] supprimerSymboles(String[] nombres) {
		List<String> nombresValides = new ArrayList<String>();

		for (int i = 0; i < nombres.length; i++) {

			String nombre = nombres[i];

			boolean dejaFait = false;
			if ((nombre != null && nombre.contains("*") && !nombre.equals("*") && nombre.charAt(0) != '*' && nombre.charAt(nombre.length() - 1) != '*')) {
				String[] nbs = StringUtils.split(nombre, "*");
				nombres = Arrays.copyOf(nombres, nombres.length + nbs.length); // create new array from old array and allocate one more element
				for (int j = 0; j < nbs.length; j++) {
					nombresValides.add(StringUtils.split(nombre, "*")[j]);
				}
				dejaFait = true;
			}

			if (!dejaFait && nombre != null) {
				nombre = nombre.replace("*", "");
				nombre = nombre.replace("&", "");
				nombre = nombre.replace("#", "");
				nombre = nombre.replace("{", "");
				nombre = nombre.replace("(", "");
				nombre = nombre.replace("[", "");
				nombre = nombre.replace("-", "");
				nombre = nombre.replace("|", "");
				nombre = nombre.replace("_", "");
				nombre = nombre.replace("^", "");
				nombre = nombre.replace("@", "");
				nombre = nombre.replace(")", "");
				nombre = nombre.replace("=", "");
				nombre = nombre.replace("+", "");
				nombre = nombre.replace("/", "");
				nombre = nombre.replace("%", "");
				nombre = nombre.replace("!", "");
				nombre = nombre.replace("�", "");
				nombre = nombre.replace(":", "");
				nombre = nombre.replace(";", "");
				nombre = nombre.replace("?", "");
				nombre = nombre.replace("$", "");
				nombresValides.add(nombre);
			}
		}
		return nombresValides.toArray(new String[0]);
	}

}
