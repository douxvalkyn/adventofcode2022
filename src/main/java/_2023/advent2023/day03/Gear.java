package advent2023.day03;

public class Gear {

	private int numLigne;
	private int numColonne;
	private int part1;
	private int part2;

	public Gear(int numLigne, int numColonne) {
		super();
		this.numLigne = numLigne;
		this.numColonne = numColonne;
	}

	public int getNumLigne() {
		return numLigne;
	}

	public void setNumLigne(int numLigne) {
		this.numLigne = numLigne;
	}

	public int getNumColonne() {
		return numColonne;
	}

	public void setNumColonne(int numColonne) {
		this.numColonne = numColonne;
	}

	public long getPart1() {
		return part1;
	}

	public void setPart1(int part1) {
		this.part1 = part1;
	}

	public long getPart2() {
		return part2;
	}

	public void setPart2(int part2) {
		this.part2 = part2;
	}

	@Override
	public String toString() {
		return "Gear [numLigne=" + numLigne + ", numColonne=" + numColonne + ", part1=" + part1 + ", part2=" + part2 + "]";
	}

}
