package advent2023.day20;

import java.util.ArrayList;
import java.util.List;

public class Module {

	private String name;
	private String type;
	private List<String> envoieVers;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<String> getEnvoieVers() {
		return envoieVers;
	}
	public void setEnvoieVers(List<String> envoieVers) {
		this.envoieVers = envoieVers;
	}
	@Override
	public String toString() {
		return "Module [name=" + name + ", type=" + type + ", envoieVers=" + envoieVers + "]";
	}
	public Module() {
		super();
		List<String>envoieVers=new ArrayList();
		this.envoieVers=envoieVers;
	}
	public Module(String name, String type, List<String> envoieVers) {
		super();
		this.name = name;
		this.type = type;
		this.envoieVers = envoieVers;
	}
	
	
}
