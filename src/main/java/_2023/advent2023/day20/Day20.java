package advent2023.day20;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day20 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day20.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day20.class.getSimpleName());
		Day20 day = new Day20();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day20a.txt";
		List<String> fichier = Outil.importationString(file);
		List<Module>modules= new ArrayList<Module>();
		init(fichier, modules);
		flux("0", modules); //0=LOW 1=HIGH
		System.out.println();
	}

	private void flux(String string, List<Module> modules) {
		//BUTTON ON -> Boradcaster
		Module module = getModuleByName(modules, "broadcaster");
		
		
	}

	private Module getModuleByName(List<Module> modules, String name) {
		Module mod = modules
		.stream()
		.filter(module -> module.getName().equals(name))
        .findFirst()
        .orElse(null);
		return mod;
	}

	private void init(List<String> fichier, List<Module> modules) {
	for (String ligne:fichier) {
		String gauche = StringUtils.split(ligne, "->")[0];
		String droite = StringUtils.split(ligne, "->")[1];
		List<String> listeDroite = Arrays.asList(StringUtils.split(droite, ","));
		listeDroite = listeDroite.stream().map(String::trim).collect(Collectors.toList());
		if (gauche.charAt(0)=='%') { //flip-flop
			Module module = new Module(StringUtils.substring(gauche, 1),"flip-flop",listeDroite );
			modules.add(module);
		}else if (gauche.charAt(0)=='&') { //conjunction
			Module module = new Module(StringUtils.substring(gauche, 1),"conjunction",listeDroite );
			modules.add(module);
		}else { //broadcaster
			Module module = new Module("broadcaster","broadcaster",listeDroite );
			modules.add(module);
		}
	}
		
	}

	
	
	
}