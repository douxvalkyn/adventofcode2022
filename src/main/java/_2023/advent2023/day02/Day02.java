package advent2023.day02;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day02 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day02.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day02.class.getSimpleName());
		Day02 day = new Day02();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day02.txt";
		List<String> res = Outil.importationString(file);
		int sum = 0;
		for (String ligne : res) {
			String[] splittedLine = StringUtils.split(ligne, ':');
			int nbMiniCubeBlue = 0;
			int nbMiniCubeRed = 0;
			int nbMiniCubeGreen = 0;
			String[] indices = StringUtils.split(splittedLine[1], ';');
			for (String indice : indices) {
				String[] tirages = StringUtils.split(indice, ',');
				for (String tirage : tirages) {
					if (tirage.contains("blue")) {
						int nbCubesBlue = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesBlue > nbMiniCubeBlue) {
							nbMiniCubeBlue = nbCubesBlue;
						}
					}
					if (tirage.contains("green")) {
						int nbCubesGreen = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesGreen > nbMiniCubeGreen) {
							nbMiniCubeGreen = nbCubesGreen;
						}
					}
					if (tirage.contains("red")) {
						int nbCubesRed = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesRed > nbMiniCubeRed) {
							nbMiniCubeRed = nbCubesRed;
						}
					}

				}

			}
			sum = sum + nbMiniCubeBlue * nbMiniCubeGreen * nbMiniCubeRed;
		}
		LOGGER.info(">Run1: " + sum);
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day02.txt";
		List<String> res = Outil.importationString(file);
		int nbMaxCubesBlue = 14;
		int nbMaxCubesRed = 12;
		int nbMaxCubesGreen = 13;
		int sum = 0;
		for (String ligne : res) {
			String[] splittedLine = StringUtils.split(ligne, ':');
			int numeroGame = Integer.parseInt(StringUtils.split(splittedLine[0], ' ')[1]);
			boolean ligneValide = true;
			String[] indices = StringUtils.split(splittedLine[1], ';');
			for (String indice : indices) {
				String[] tirages = StringUtils.split(indice, ',');
				for (String tirage : tirages) {
					if (tirage.contains("blue")) {
						int nbCubesBlue = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesBlue <= nbMaxCubesBlue) {
						} else {
							ligneValide = false;
						}
					}
					if (tirage.contains("red")) {
						int nbCubesRed = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesRed <= nbMaxCubesRed) {
						} else {
							ligneValide = false;
						}
					}
					if (tirage.contains("green")) {
						int nbCubesGreen = Integer.parseInt(StringUtils.split(tirage, ' ')[0]);
						if (nbCubesGreen <= nbMaxCubesGreen) {
						} else {
							ligneValide = false;
						}
					}
				}

			}
			if (ligneValide) {
				sum = sum + numeroGame;
			}

		}
		LOGGER.info(">Run2: " + sum);

	}

}
