package advent2023.day18;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import advent2023.day17.State;
import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day18 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day18.class);
	private static int sumParcours = 0;
	private static int sumInterieur = 0;

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day18.class.getSimpleName());
		Day18 day = new Day18();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day18.txt";
		List<String> fichier = Outil.importationString(file);
		int sr = sommeDirection(fichier, 'R');
		int sd = sommeDirection(fichier, 'D');
		int su = sommeDirection(fichier, 'U');
		int sl = sommeDirection(fichier, 'L');
		Grille grille = new Grille(sr + sl - 500, sd + su - 500);
		 //Grille grille = new Grille(sr + sl, sd + su);

		initGrille(fichier, grille, grille.getCase(500, 500));
		// initGrille(fichier, grille, grille.getCase(0, 0));
		// grille.affichageEtat();

		ajoutInterieur(grille, grille.getCase(510, 500));
		// ajoutInterieur(grille, grille.getCase(1, 1));

	}

	private void rechercheCentre(Grille grille) {
		grille.getCase(500, 500).setEtat("X");
		grille.affichageEtat();

	}

	private static void ajoutInterieur(Grille grille, Case caseDepart) {
		Case cellEnCours = caseDepart;

		rechercheVoisin(grille, cellEnCours);

		System.err.println(sumInterieur);
		System.err.println(sumParcours);
		System.err.println(sumParcours + sumInterieur);
	}

	private static void rechercheVoisin(Grille grille, Case cellEnCours) {
		Queue<Case> queue = new LinkedList<>();
		queue.add(cellEnCours);
		
		while (!queue.isEmpty()) {
			if (sumInterieur%1000==0) {
				System.out.println(sumInterieur);
				System.out.println(queue.size());
			}
			cellEnCours = queue.poll();
		if (cellEnCours != null && !cellEnCours.getEtat().equals("#")) {
			cellEnCours.setEtat("#");
			sumInterieur++;
		}
		List<Case> casesAdjacentes = cellEnCours.getCasesAdjacentes2(grille);
		for (Case c:casesAdjacentes) {
			if (c!=null &&  !queue.contains(c) && !c.getEtat().equals("#")) {
				queue.add(c);
			}
		}
		}
		
		
		
			

	}

	private int sommeDirection(List<String> fichier, char direction) {
		int sum = 0;
		for (String ligne : fichier) {
			if (ligne.charAt(0) == direction) {
				sum = sum + Integer.parseInt(StringUtils.split(StringUtils.split(ligne, '(')[0], ' ')[1]);
			}

		}
		return sum;
	}

	private void initGrille(List<String> fichier, Grille grille, Case caseEnCours) {
		caseEnCours.setEtat("#");
		for (String ligne : fichier) {
			int nombreDePas = Integer.parseInt(StringUtils.split(StringUtils.split(ligne, '(')[0], ' ')[1]);
			String direction = "" + ligne.charAt(0);
			int x = caseEnCours.getX();
			int y = caseEnCours.getY();
			for (int i = 1; i <= nombreDePas; i++) {
				switch (direction) {
					case "R":
						grille.getCase(x + i, y).setEtat("#");
						caseEnCours = grille.getCase(x + i, y);
						sumParcours++;
						break;
					case "L":
						grille.getCase(x - i, y).setEtat("#");
						caseEnCours = grille.getCase(x - i, y);
						sumParcours++;
						break;
					case "U":
						grille.getCase(x, y - i).setEtat("#");
						caseEnCours = grille.getCase(x, y - i);
						sumParcours++;
						break;
					case "D":
						grille.getCase(x, y + i).setEtat("#");
						caseEnCours = grille.getCase(x, y + i);
						sumParcours++;
						break;

					default:
						break;
				}

			}

		}

	}
}