package advent2023.day11;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day11 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day11.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day11.class.getSimpleName());
		Day11 day = new Day11();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}


	
	
	public void run2() {
		String file = "src/main/resources/advent2023/day11.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(),fichier.size());
		List<Galaxy> galaxies = new ArrayList<>();
		initGrille(grille, fichier, galaxies);
		List<Integer> listeLignesSansGalaxie = rechercheLignesVides(grille, galaxies);
		List<Integer> listeColonnesSansGalaxie = rechercheColonnesVides(grille, galaxies);
		expansion2(galaxies, listeColonnesSansGalaxie, listeLignesSansGalaxie);
		recherchePlusCourtChemin(galaxies);
		
		
		
		
		System.err.println();
		
	}
	
	public void run1() {
		String file = "src/main/resources/advent2023/day11.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(),fichier.size());
		List<Galaxy> galaxies = new ArrayList<>();
		initGrille(grille, fichier, galaxies);
		List<Integer> listeLignesSansGalaxie = rechercheLignesVides(grille, galaxies);
		List<Integer> listeColonnesSansGalaxie = rechercheColonnesVides(grille, galaxies);
		expansion(galaxies, listeColonnesSansGalaxie, listeLignesSansGalaxie);
		recherchePlusCourtChemin(galaxies);
		
		
		
		
		System.err.println();
		
	}

	
	private void recherchePlusCourtChemin(List<Galaxy> galaxies) {
		long sum=0;
for (Galaxy galaxy1:galaxies) {
	for (Galaxy galaxy2:galaxies) {
		if (galaxy1!=galaxy2) {
			long dist=Math.abs( galaxy2.getX()-galaxy1.getX()) + Math.abs( galaxy2.getY()-galaxy1.getY()) ;
				sum=sum+dist;
			
			
		}
	}

}
System.err.println(sum/2);
		
		
	}

	
	private void expansion2(List<Galaxy> galaxies, List<Integer> listeColonnesSansGalaxie,
			List<Integer> listeLignesSansGalaxie) {
int constanteExpansion=1000000;
		
		for (int i=0;i<galaxies.size();i++) {
			
			//expansion sur les colonnes ->
			int nbAjoutX = 0;
			for (int j=0;j<listeColonnesSansGalaxie.size();j++) {
				if (galaxies.get(i).getX()>=listeColonnesSansGalaxie.get(j)) {
					nbAjoutX++;
				}
			}
			galaxies.get(i).setX(galaxies.get(i).getX()+nbAjoutX*(constanteExpansion-1));
			
			//expansion sur les lignes v
			int nbAjoutY = 0;
			for (int j=0;j<listeLignesSansGalaxie.size();j++) {
				if (galaxies.get(i).getY()>=listeLignesSansGalaxie.get(j)) {
					nbAjoutY++;
				}
			}
			galaxies.get(i).setY(galaxies.get(i).getY()+nbAjoutY*(constanteExpansion-1));
			
			
		}
	
		
		
		
	}
	
	private void expansion(List<Galaxy> galaxies, List<Integer> listeColonnesSansGalaxie,
			List<Integer> listeLignesSansGalaxie) {

		
		for (int i=0;i<galaxies.size();i++) {
			
			//expansion sur les colonnes ->
			int nbAjoutX = 0;
			for (int j=0;j<listeColonnesSansGalaxie.size();j++) {
				if (galaxies.get(i).getX()>=listeColonnesSansGalaxie.get(j)) {
					nbAjoutX++;
				}
			}
			galaxies.get(i).setX(galaxies.get(i).getX()+nbAjoutX);
			
			//expansion sur les lignes v
			int nbAjoutY = 0;
			for (int j=0;j<listeLignesSansGalaxie.size();j++) {
				if (galaxies.get(i).getY()>=listeLignesSansGalaxie.get(j)) {
					nbAjoutY++;
				}
			}
			galaxies.get(i).setY(galaxies.get(i).getY()+nbAjoutY);
			
			
		}
	
		
		
		
	}

	private List<Integer> rechercheColonnesVides(Grille grille, List<Galaxy> galaxies) {
		List<Integer> listeColonnesSansGalaxie = new ArrayList<>();
		for (int i=0;i<grille.getNbCol();i++) {
			boolean aucuneGalaxie=true;
			for (int j=0;j< grille.getNbLignes();j++) {
				if (grille.getCase(i, j).getEtat().equals("#")) {
					aucuneGalaxie=false;
				}
			}
			if (aucuneGalaxie) {
				listeColonnesSansGalaxie.add(i);
			}
		}
		
		return listeColonnesSansGalaxie;
		
	}
	
	private List<Integer> rechercheLignesVides(Grille grille, List<Galaxy> galaxies) {
		List<Integer> listeLignesSansGalaxie = new ArrayList<>();
		for (int i=0;i<grille.getNbLignes();i++) {
			boolean aucuneGalaxie=true;
			for (int j=0;j< grille.getNbCol();j++) {
				if (grille.getCase(j, i).getEtat().equals("#")) {
					aucuneGalaxie=false;
				}
			}
			if (aucuneGalaxie) {
				listeLignesSansGalaxie.add(i);
				}
		}
		
		return listeLignesSansGalaxie;
		
	}

	private void initGrille(Grille grille, List<String> fichier, List<Galaxy> galaxies) {
		
		for (int i=0;i<fichier.size();i++) {
			for (int j=0;j<fichier.get(0).length();j++) {
				String element = ""+fichier.get(i).charAt(j);
				grille.getCase(j, i).setEtat(element);
				
				if (element.equals("#")) {
					Galaxy galaxy = new Galaxy();
					galaxy.setX(j);
					galaxy.setY(i);
					galaxies.add(galaxy);
				}
		}
		
	}
	//	grille.affichageEtat();
	}
}