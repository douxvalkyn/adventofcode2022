package advent2023.day24;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Grille;
import communs.Outil;

public class Day24 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day24.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day24.class.getSimpleName());
		Day24 day = new Day24();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day24.txt";
		List<String> fichier = Outil.importationString(file);
		List<Hail> hails= new ArrayList<Hail>();
		hails=init(fichier, hails);
		//rechercheIntersectionXY(hails, 7, 27);
		rechercheIntersectionXY(hails, 200000000000000.0, 400000000000000.0);
		

	}

	private void rechercheIntersectionXY(List<Hail> hails, double min, double max) {
		int sum=0;
for (int i=0;i<hails.size();i++) {
	Hail hail1 = hails.get(i);
	double a1 = (hail1.getVy())/(hail1.getVx());
	double b1 = hail1.getY() - a1*hail1.getX();
	for (int j=i+1;j<hails.size();j++) {
		Hail hail2 = hails.get(j);
			double a2 = (hail2.getVy())/(hail2.getVx());
			double b2 = hail2.getY() - a2*hail2.getX();
			if (a1==a2) {
				//System.out.println("//");
			}else {
				double x = (b2-b1)/(a1-a2);
				double y = a1*x+b1;
//				System.out.println(x);
//				System.out.println(y);
				if (x >= min && x <= max && y >= min && y <= max && futur(hail1.getVx(),hail1.getX(),x) && futur(hail2.getVx(),hail2.getX(),x)) {
					sum++;
				}
			
		}
	}
}
	System.err.println(sum);	
		
		
	}

	private boolean futur(double vx, double x1, double x2) {

		if( (vx>0 && x2>x1)||(vx<0 && x2<x1)){
			return true;
		}
		return false;
	}

	private List<Hail> init(List<String> fichier, List<Hail> hails) {
		for (String ligne:fichier) {
			String xyz = StringUtils.split(ligne, "@")[0];
			String vxvyvz = StringUtils.split(ligne, "@")[1];
			long x = Long.parseLong(StringUtils.trim( StringUtils.split(xyz, ",")[0]));
			long y = Long.parseLong(StringUtils.trim( StringUtils.split(xyz, ",")[1]));
			long z = Long.parseLong(StringUtils.trim( StringUtils.split(xyz, ",")[2]));
			long vx = Long.parseLong(StringUtils.trim( StringUtils.split(vxvyvz, ",")[0]));
			long vy = Long.parseLong(StringUtils.trim( StringUtils.split(vxvyvz, ",")[1]));
			long vz = Long.parseLong(StringUtils.trim( StringUtils.split(vxvyvz, ",")[2]));
			Hail hail= new Hail(x,y,z,vx,vy,vz);
			hails.add(hail);
		}
		return hails;
	}

}