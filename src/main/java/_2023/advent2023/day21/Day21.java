package advent2023.day21;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day21 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day21.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day21.class.getSimpleName());
		Day21 day = new Day21();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day21.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		Case start = rechercheStart(grille);
		int nbSteps=64;
		List<Case>cases=new ArrayList();
		cases.add(start);
		for (int step=1;step<=nbSteps;step++) {
			System.out.println(step);
			List<Case>nextCases=new ArrayList();
			for (Case c:cases) {
				List<Case> casesAdjacentes = c.getCasesAdjacentes2(grille);
				for (Case caseAdjacente:casesAdjacentes) {
					if (caseAdjacente.getEtat().equals(".")) {
						caseAdjacente.setEtat("O");
						nextCases.add(caseAdjacente);
					}
				}
				c.setEtat(".");
			}
			cases=nextCases;
			//grille.affichageEtat();
			
		}
		
		int nb = calculNombreO(grille);
		System.err.println(nb);

	}

	
	
	
	private int calculNombreO(Grille grille) {
		int nb=0;
		List<Case> cases = grille.getCases();
		for (Case c:cases) {
			if (c.getEtat().equals("O")) {
				nb++;
			}
		}
		return nb;
	}

	private Case rechercheStart(Grille grille) {
		List<Case> cases = grille.getCases();
		for (Case c:cases) {
			if (c.getEtat().equals("S")) {
				c.setEtat(".");
				return c;
			}
		}
		return null;
	}

	private static void initGrille(List<String> fichier, Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			for (int j = 0; j < grille.getNbLignes(); j++) {
				grille.getCase(i, j).setEtat("" + fichier.get(j).charAt(i));
			}
		}
		grille.affichageEtat();
	}
}