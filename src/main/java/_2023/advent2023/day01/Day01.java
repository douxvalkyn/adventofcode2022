package advent2023.day01;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day01 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day01.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day01.class.getSimpleName());
		Day01 day = new Day01();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day01.txt";
		List<String> res = Outil.importationString(file);
		int sum = 0;
		for (String ligne : res) {
			int nombre = 100;
			int index = 100;
			if (renvoieIndex(ligne, "1") != -1) {
				nombre = 1;
				index = renvoieIndex(ligne, "1");
			}
			if (renvoieIndex(ligne, "2") != -1 && renvoieIndex(ligne, "2") < index) {
				nombre = 2;
				index = renvoieIndex(ligne, "2");
			}
			if (renvoieIndex(ligne, "3") != -1 && renvoieIndex(ligne, "3") < index) {
				nombre = 3;
				index = renvoieIndex(ligne, "3");
			}
			if (renvoieIndex(ligne, "4") != -1 && renvoieIndex(ligne, "4") < index) {
				nombre = 4;
				index = renvoieIndex(ligne, "4");
			}
			if (renvoieIndex(ligne, "5") != -1 && renvoieIndex(ligne, "5") < index) {
				nombre = 5;
				index = renvoieIndex(ligne, "5");
			}
			if (renvoieIndex(ligne, "6") != -1 && renvoieIndex(ligne, "6") < index) {
				nombre = 6;
				index = renvoieIndex(ligne, "6");
			}
			if (renvoieIndex(ligne, "7") != -1 && renvoieIndex(ligne, "7") < index) {
				nombre = 7;
				index = renvoieIndex(ligne, "7");
			}
			if (renvoieIndex(ligne, "8") != -1 && renvoieIndex(ligne, "8") < index) {
				nombre = 8;
				index = renvoieIndex(ligne, "8");
			}
			if (renvoieIndex(ligne, "9") != -1 && renvoieIndex(ligne, "9") < index) {
				nombre = 9;
				index = renvoieIndex(ligne, "9");
			}
			if (renvoieIndex(ligne, "one") != -1 && renvoieIndex(ligne, "one") < index) {
				nombre = 1;
				index = renvoieIndex(ligne, "one");
			}
			if (renvoieIndex(ligne, "two") != -1 && renvoieIndex(ligne, "two") < index) {
				nombre = 2;
				index = renvoieIndex(ligne, "two");
			}
			if (renvoieIndex(ligne, "three") != -1 && renvoieIndex(ligne, "three") < index) {
				nombre = 3;
				index = renvoieIndex(ligne, "three");
			}
			if (renvoieIndex(ligne, "four") != -1 && renvoieIndex(ligne, "four") < index) {
				nombre = 4;
				index = renvoieIndex(ligne, "four");
			}
			if (renvoieIndex(ligne, "five") != -1 && renvoieIndex(ligne, "five") < index) {
				nombre = 5;
				index = renvoieIndex(ligne, "five");
			}
			if (renvoieIndex(ligne, "six") != -1 && renvoieIndex(ligne, "six") < index) {
				nombre = 6;
				index = renvoieIndex(ligne, "six");
			}
			if (renvoieIndex(ligne, "seven") != -1 && renvoieIndex(ligne, "seven") < index) {
				nombre = 7;
				index = renvoieIndex(ligne, "seven");
			}
			if (renvoieIndex(ligne, "eight") != -1 && renvoieIndex(ligne, "eight") < index) {
				nombre = 8;
				index = renvoieIndex(ligne, "eight");
			}
			if (renvoieIndex(ligne, "nine") != -1 && renvoieIndex(ligne, "nine") < index) {
				nombre = 9;
			}

			int nombre2 = 100;
			int index2 = 0;
			if (renvoieLastIndex(ligne, "1") != -1) {
				nombre2 = 1;
				index2 = renvoieLastIndex(ligne, "1");
			}
			if (renvoieLastIndex(ligne, "2") != -1 && renvoieLastIndex(ligne, "2") > index2) {
				nombre2 = 2;
				index2 = renvoieLastIndex(ligne, "2");
			}
			if (renvoieLastIndex(ligne, "3") != -1 && renvoieLastIndex(ligne, "3") > index2) {
				nombre2 = 3;
				index2 = renvoieLastIndex(ligne, "3");
			}
			if (renvoieLastIndex(ligne, "4") != -1 && renvoieLastIndex(ligne, "4") > index2) {
				nombre2 = 4;
				index2 = renvoieLastIndex(ligne, "4");
			}
			if (renvoieLastIndex(ligne, "5") != -1 && renvoieLastIndex(ligne, "5") > index2) {
				nombre2 = 5;
				index2 = renvoieLastIndex(ligne, "5");
			}
			if (renvoieLastIndex(ligne, "6") != -1 && renvoieLastIndex(ligne, "6") > index2) {
				nombre2 = 6;
				index2 = renvoieLastIndex(ligne, "6");
			}
			if (renvoieLastIndex(ligne, "7") != -1 && renvoieLastIndex(ligne, "7") > index2) {
				nombre2 = 7;
				index2 = renvoieLastIndex(ligne, "7");
			}
			if (renvoieLastIndex(ligne, "8") != -1 && renvoieLastIndex(ligne, "8") > index2) {
				nombre2 = 8;
				index2 = renvoieLastIndex(ligne, "8");
			}
			if (renvoieLastIndex(ligne, "9") != -1 && renvoieLastIndex(ligne, "9") > index2) {
				nombre2 = 9;
				index2 = renvoieLastIndex(ligne, "9");
			}
			if (renvoieLastIndex(ligne, "one") != -1 && renvoieLastIndex(ligne, "one") > index2) {
				nombre2 = 1;
				index2 = renvoieLastIndex(ligne, "one");
			}
			if (renvoieLastIndex(ligne, "two") != -1 && renvoieLastIndex(ligne, "two") > index2) {
				nombre2 = 2;
				index2 = renvoieLastIndex(ligne, "two");
			}
			if (renvoieLastIndex(ligne, "three") != -1 && renvoieLastIndex(ligne, "three") > index2) {
				nombre2 = 3;
				index2 = renvoieLastIndex(ligne, "three");
			}
			if (renvoieLastIndex(ligne, "four") != -1 && renvoieLastIndex(ligne, "four") > index2) {
				nombre2 = 4;
				index2 = renvoieLastIndex(ligne, "four");
			}
			if (renvoieLastIndex(ligne, "five") != -1 && renvoieLastIndex(ligne, "five") > index2) {
				nombre2 = 5;
				index2 = renvoieLastIndex(ligne, "five");
			}
			if (renvoieLastIndex(ligne, "six") != -1 && renvoieLastIndex(ligne, "six") > index2) {
				nombre2 = 6;
				index2 = renvoieLastIndex(ligne, "six");
			}
			if (renvoieLastIndex(ligne, "seven") != -1 && renvoieLastIndex(ligne, "seven") > index2) {
				nombre2 = 7;
				index2 = renvoieLastIndex(ligne, "seven");
			}
			if (renvoieLastIndex(ligne, "eight") != -1 && renvoieLastIndex(ligne, "eight") > index2) {
				nombre2 = 8;
				index2 = renvoieLastIndex(ligne, "eight");
			}
			if (renvoieLastIndex(ligne, "nine") != -1 && renvoieLastIndex(ligne, "nine") > index2) {
				nombre2 = 9;
			}
			if (nombre2 == 100) {
				nombre2 = nombre;
			}
			String nb = "" + nombre + nombre2;
			sum = sum + Integer.parseInt(nb);
		}
		System.out.println(sum);
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day01.txt";
		List<String> res = Outil.importationString(file);
		int somme = 0;
		for (String ligne : res) {
			String nb1 = null;
			String nb2 = null;
			String nb = null;
			int nombre = 0;
			for (int i = 0; i < ligne.length(); i++) {
				if ((ligne.charAt(i) == '0') || (ligne.charAt(i) == '1') || (ligne.charAt(i) == '2') || (ligne.charAt(i) == '3') || (ligne.charAt(i) == '4') || (ligne.charAt(i) == '5')
					|| (ligne.charAt(i) == '6') || (ligne.charAt(i) == '7') || (ligne.charAt(i) == '8') || (ligne.charAt(i) == '9')) {
					if (nb1 != null) {
						nb2 = "" + ligne.charAt(i);
					} else {
						nb1 = "" + ligne.charAt(i);
					}

				}

			}

			if (nb2 == null) {
				nb2 = nb1;
			}
			nb = nb1 + nb2;
			nombre = Integer.parseInt(nb);
			somme = somme + nombre;
		}
		System.out.println(somme);
	}

	private static int renvoieIndex(String chaine, String recherche) {
		return chaine.indexOf(recherche);
	}

	private static int renvoieLastIndex(String chaine, String recherche) {
		return chaine.lastIndexOf(recherche);
	}
}
