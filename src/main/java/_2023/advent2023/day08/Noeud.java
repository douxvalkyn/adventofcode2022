package advent2023.day08;

public class Noeud {

	private String nom;
	private String nomLeft;
	private String nomRight;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNomLeft() {
		return nomLeft;
	}

	public void setNomLeft(String nomLeft) {
		this.nomLeft = nomLeft;
	}

	public String getNomRight() {
		return nomRight;
	}

	public void setNomRight(String nomRight) {
		this.nomRight = nomRight;
	}

	public Noeud() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Noeud [nom=" + nom + ", nomLeft=" + nomLeft + ", nomRight=" + nomRight + "]";
	}

}
