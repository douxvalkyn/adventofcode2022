package advent2023.day08;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day08 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day08.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day08.class.getSimpleName());
		Day08 day = new Day08();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day08.txt";
		List<String> res = Outil.importationString(file);
		String directions = res.get(0);
		List<Noeud> noeuds = new ArrayList<>();
		initNoeuds(res, noeuds);

		// Liste des noeuds de depart (qui finissent par A)
		List<Noeud> noeudsDepart = new ArrayList<>();
		List<String> nomsNoeudsEnCours = new ArrayList<>();
		for (Noeud noeud : noeuds) {
			if (noeud.getNom().charAt(2) == 'A') {
				noeudsDepart.add(noeud);
				nomsNoeudsEnCours.add(noeud.getNom());
			}
		}

		// calculer pour chaque noeud de depart le nb d'etapes avant une fin possible
		for (Noeud noeudDepart : noeudsDepart) {

			int nbEtapes = 0;
			String nomNoeudEnCours = noeudDepart.getNom();

			while (nbEtapes < 100000) {
				Noeud noeudEnCours = findNoeudByName(noeuds, nomNoeudEnCours);
				// System.out.println(noeudEnCours.getNom());
				if (nbEtapes == directions.length()) {
					directions = directions + directions;
				}
				char direction = directions.charAt(nbEtapes);
				if (direction == 'L') {
					nomNoeudEnCours = noeudEnCours.getNomLeft();
				}
				if (direction == 'R') {
					nomNoeudEnCours = noeudEnCours.getNomRight();
				}

				if (nomNoeudEnCours.charAt(2) == 'Z') {
					System.err.println(noeudDepart.getNom() + " " + nbEtapes);
				}
				nbEtapes++;

			}

		}

		List<Long> dureesCycle = new ArrayList<>();
		dureesCycle.add(13207L);
		dureesCycle.add(22199L);
		dureesCycle.add(14893L);
		dureesCycle.add(16579L);
		dureesCycle.add(20513L);
		dureesCycle.add(12083L);
		dureesCycle.add(20513L);

		long ppmc = Outil.ppmc(dureesCycle);

		System.out.println(ppmc);
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day08.txt";
		List<String> res = Outil.importationString(file);
		String directions = res.get(0);
		List<Noeud> noeuds = new ArrayList<>();
		initNoeuds(res, noeuds);

		int nbEtapes = 0;
		String nomNoeudEnCours = "AAA";

		while (!"ZZZ".equals(nomNoeudEnCours)) {
			Noeud noeudEnCours = findNoeudByName(noeuds, nomNoeudEnCours);
			System.out.println(noeudEnCours.getNom());
			if (nbEtapes == directions.length()) {
				directions = directions + directions;
			}
			char direction = directions.charAt(nbEtapes);
			if (direction == 'L') {
				nomNoeudEnCours = noeudEnCours.getNomLeft();
			}
			if (direction == 'R') {
				nomNoeudEnCours = noeudEnCours.getNomRight();
			}
			nbEtapes++;

		}

		System.out.println(nbEtapes);
	}

	private Noeud findNoeudByName(List<Noeud> noeuds, String nom) {
		for (Noeud noeud : noeuds) {
			if (noeud.getNom().equals(nom)) {
				return noeud;
			}
		}
		return null;
	}

	private void initNoeuds(List<String> res, List<Noeud> noeuds) {
		for (int i = 2; i < res.size(); i++) {
			String nom = StringUtils.trim(StringUtils.split(res.get(i), '=')[0]);
			String nomLeft = StringUtils.trim(StringUtils.split(StringUtils.split(StringUtils.split(res.get(i), '=')[1], ',')[0], '(')[1]);
			String nomRight = StringUtils.trim(StringUtils.split(StringUtils.split(StringUtils.split(res.get(i), '=')[1], ',')[1], ')')[0]);
			Noeud noeud = new Noeud();
			noeud.setNom(nom);
			noeud.setNomLeft(nomLeft);
			noeud.setNomRight(nomRight);
			noeuds.add(noeud);
		}
	}

}
