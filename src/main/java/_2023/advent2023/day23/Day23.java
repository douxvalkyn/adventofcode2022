package advent2023.day23;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day23 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day23.class);
	private static int max=0;

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day23.class.getSimpleName());
		Day23 day = new Day23();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {// 2574
		String file = "src/main/resources/advent2023/day23.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		Case start = findStart(grille);
		Case stop = findStop(grille);
		rechercheChemins2(grille, start, stop);

	}

	public void run1() {
		String file = "src/main/resources/advent2023/day23.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		Case start = findStart(grille);
		Case stop = findStop(grille);
		rechercheChemins(grille, start, stop);
	}

	private int rechercheChemins2(Grille grille, Case start, Case stop) {

		var stateInitial = new State();
		stateInitial.setCaseActuelle(start);
		stateInitial.setLongueurChemin(0);

		Queue<State> queue = new LinkedList<>();
		queue.add(stateInitial);

		Set<Case> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial.getCaseActuelle());

		long i = 0;

		queue.add(stateInitial);
		while (!queue.isEmpty()) {
			i++;
			if (i % 1_000_000 == 0) {
				System.out.println("qs: "+queue.size() +" "+ i);
			}
			State stateActuel = queue.poll();

			if (stateActuel.getCaseActuelle() == stop && stateActuel.getLongueurChemin()>max) {
				max=stateActuel.getLongueurChemin();
				System.err.println(stateActuel.getLongueurChemin() +" (qs: "+ queue.size()+")");
			} else {
				List<Case> casesAdjacentes = stateActuel.getCaseActuelle().getCasesAdjacentes2(grille);

				for (Case caseAdjacente : casesAdjacentes) {
					if (caseAdjacente != null && !caseAdjacente.getEtat().equals("#") && !stateActuel.getChemin().contains(caseAdjacente.getX() + ";" + caseAdjacente.getY() + " ")) {
						State nextState = new State();
						nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
						nextState.setCaseActuelle(caseAdjacente);
						nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY() + " ");
						queue.add(nextState);
						visitedStates.add(nextState.getCaseActuelle());
					}
				}

			}
		}
		return 0;
	}

	private int rechercheChemins(Grille grille, Case start, Case stop) {

		State stateInitial = new State();
		stateInitial.setCaseActuelle(start);
		stateInitial.setLongueurChemin(0);

		Queue<State> queue = new LinkedList<State>();
		queue.add(stateInitial);

		while (!queue.isEmpty()) {

			State stateActuel = queue.poll();

			if (stateActuel.getCaseActuelle() == stop) {
				System.out.println(stateActuel.getLongueurChemin());
			} else {

				List<Case> casesAdjacentes = stateActuel.getCaseActuelle().getCasesAdjacentes2(grille);

				// si on est sur une case > on doit aller dans cette direction
				boolean slope = false;
				if (stateActuel.getCaseActuelle().getEtat().equals(">")) {
					State nextState = new State();
					Case caseAdjacente = casesAdjacentes.get(2);
					nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
					nextState.setCaseActuelle(caseAdjacente);
					nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY());
					queue.add(nextState);
					slope = true;
				}
				if (stateActuel.getCaseActuelle().getEtat().equals("v")) {
					State nextState = new State();
					Case caseAdjacente = casesAdjacentes.get(1);
					nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
					nextState.setCaseActuelle(caseAdjacente);
					nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY());
					queue.add(nextState);
					slope = true;
				}
				if (stateActuel.getCaseActuelle().getEtat().equals("<")) {
					State nextState = new State();
					Case caseAdjacente = casesAdjacentes.get(3);
					nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
					nextState.setCaseActuelle(caseAdjacente);
					nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY());
					queue.add(nextState);
					slope = true;
				}
				if (stateActuel.getCaseActuelle().getEtat().equals("^")) {
					State nextState = new State();
					Case caseAdjacente = casesAdjacentes.get(0);
					nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
					nextState.setCaseActuelle(caseAdjacente);
					nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY());
					queue.add(nextState);
					slope = true;
				}

				if (!slope) {
					for (Case caseAdjacente : casesAdjacentes) {
						if (caseAdjacente != null && !caseAdjacente.getEtat().equals("#") && !stateActuel.getChemin().contains("(" + caseAdjacente.getX() + ";" + caseAdjacente.getY() + ")")
								&& neRencontrePasDeSlopeBloquante(stateActuel, caseAdjacente, casesAdjacentes)) {
							State nextState = new State();
							nextState.setLongueurChemin(stateActuel.getLongueurChemin() + 1);
							nextState.setCaseActuelle(caseAdjacente);
							nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getX() + ";" + stateActuel.getCaseActuelle().getY());
							queue.add(nextState);
						}
					}
				}

			}
		}

		return 0;
	}

	private boolean neRencontrePasDeSlopeBloquante(State stateActuel, Case caseAdjacente, List<Case> casesAdjacentes) {
		// i= 0N 1S 2E 3W
		int direction = -1;
		for (int i = 0; i < casesAdjacentes.size(); i++) {
			if (caseAdjacente == casesAdjacentes.get(i)) {
				direction = i;
			}
		}

		if (caseAdjacente.getEtat().equals("^") && direction == 1) {
			return false;
		}
		if (caseAdjacente.getEtat().equals("v") && direction == 0) {
			return false;
		}
		if (caseAdjacente.getEtat().equals("<") && direction == 2) {
			return false;
		}
		if (caseAdjacente.getEtat().equals(">") && direction == 3) {
			return false;
		}
		return true;
	}

	private Case findStart(Grille grille) {
		List<Case> cases = grille.getCases();
		for (Case c : cases) {
			if (c.getY() == 0 && c.getEtat().equals(".")) {
				return c;
			}
		}
		return null;
	}

	private Case findStop(Grille grille) {
		List<Case> cases = grille.getCases();
		for (Case c : cases) {
			if (c.getY() == (grille.getNbLignes() - 1) && c.getEtat().equals(".")) {
				return c;
			}
		}
		return null;
	}

	private static void initGrille(List<String> fichier, Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			for (int j = 0; j < grille.getNbLignes(); j++) {
				grille.getCase(i, j).setEtat("" + fichier.get(j).charAt(i));
			}
		}
		grille.affichageEtat();
	}

}