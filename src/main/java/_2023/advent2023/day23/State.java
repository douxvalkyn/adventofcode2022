package advent2023.day23;

import java.util.Objects;

import communs.Case;

public class State {

	private int longueurChemin;
	private Case caseActuelle;
	String chemin= "";
	
	
	

	public String getChemin() {
		return chemin;
	}
	public void setChemin(String chemin) {
		this.chemin = chemin;
	}
	public int getLongueurChemin() {
		return longueurChemin;
	}
	public void setLongueurChemin(int longueurChemin) {
		this.longueurChemin = longueurChemin;
	}
	public Case getCaseActuelle() {
		return caseActuelle;
	}
	public void setCaseActuelle(Case caseActuelle) {
		this.caseActuelle = caseActuelle;
	}
	@Override
	public String toString() {
		return "State [longueurChemin=" + longueurChemin + ", caseActuelle=" + caseActuelle + "]";
	}
	public State() {
		super();
	}
	@Override
	public int hashCode() {
		return Objects.hash(caseActuelle, chemin, longueurChemin);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return Objects.equals(caseActuelle, other.caseActuelle) && Objects.equals(chemin, other.chemin)
				&& longueurChemin == other.longueurChemin;
	}
	
	
	
}
