package advent2023.day07;

public class CamelHand {

	private int rank;
	private int bid;
	private String type;
	private String carte1;
	private String carte2;
	private String carte3;
	private String carte4;
	private String carte5;

	public int getRank() {
		return rank;
	}

	public void setRank(int value) {
		this.rank = value;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getCarte1() {
		return carte1;
	}

	public void setCarte1(String carte1) {
		this.carte1 = carte1;
	}

	public String getCarte2() {
		return carte2;
	}

	public void setCarte2(String carte2) {
		this.carte2 = carte2;
	}

	public String getCarte3() {
		return carte3;
	}

	public void setCarte3(String carte3) {
		this.carte3 = carte3;
	}

	public String getCarte4() {
		return carte4;
	}

	public void setCarte4(String carte4) {
		this.carte4 = carte4;
	}

	public String getCarte5() {
		return carte5;
	}

	public void setCarte5(String carte5) {
		this.carte5 = carte5;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "CamelCard [rank=" + rank + ", bid=" + bid + ", type=" + type + ", cartes=" + carte1 + carte2 + carte3 + carte4 + carte5 + "]";
	}

	public CamelHand() {
		// TODO Auto-generated constructor stub
	}

}
