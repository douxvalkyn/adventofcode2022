package advent2023.day07;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day07 {

	private static final String HIGH_CARD = "highCard";
	private static final String ONE_PAIR = "onePair";
	private static final String TWO_PAIR = "twoPair";
	private static final String THREE_OF_A_KIND = "threeOfAKind";
	private static final String FULL_HOUSE = "fullHouse";
	private static final String FOUR_OF_A_KIND = "fourOfAKind";
	private static final String FIVE_OF_A_KIND = "fiveOfAKind";
	private static final Logger LOGGER = LoggerFactory.getLogger(Day07.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day07.class.getSimpleName());
		Day07 day = new Day07();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		List<CamelHand> camelCards = new ArrayList<>();
		String file = "src/main/resources/advent2023/day07.txt";
		init(camelCards, file);
		initJoker(camelCards);
		int ranks = camelCards.size();

		// quinte
		List<CamelHand> camelCardsFiveOfAKind = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FIVE_OF_A_KIND)) {
				camelCardsFiveOfAKind.add(camelCard);
			}
		}

		while (!camelCardsFiveOfAKind.isEmpty()) {
			CamelHand handMax = camelCardsFiveOfAKind.get(0);
			for (int j = 1; j < camelCardsFiveOfAKind.size(); j++) {
				CamelHand hand2 = camelCardsFiveOfAKind.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsFiveOfAKind.remove(handMax);
		}

		// carre
		List<CamelHand> camelCardsFourOfAKind = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FOUR_OF_A_KIND)) {
				camelCardsFourOfAKind.add(camelCard);
			}
		}

		while (!camelCardsFourOfAKind.isEmpty()) {
			CamelHand handMax = camelCardsFourOfAKind.get(0);
			for (int j = 1; j < camelCardsFourOfAKind.size(); j++) {
				CamelHand hand2 = camelCardsFourOfAKind.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsFourOfAKind.remove(handMax);
		}

		// full
		List<CamelHand> camelCardsFull = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FULL_HOUSE)) {
				camelCardsFull.add(camelCard);
			}
		}

		while (!camelCardsFull.isEmpty()) {
			CamelHand handMax = camelCardsFull.get(0);
			for (int j = 1; j < camelCardsFull.size(); j++) {
				CamelHand hand2 = camelCardsFull.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsFull.remove(handMax);
		}

		// brelan
		List<CamelHand> camelCardsBrelan = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(THREE_OF_A_KIND)) {
				camelCardsBrelan.add(camelCard);
			}
		}

		while (!camelCardsBrelan.isEmpty()) {
			CamelHand handMax = camelCardsBrelan.get(0);
			for (int j = 1; j < camelCardsBrelan.size(); j++) {
				CamelHand hand2 = camelCardsBrelan.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsBrelan.remove(handMax);
		}

		// 2 paires
		List<CamelHand> camelCardsTwoPair = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(TWO_PAIR)) {
				camelCardsTwoPair.add(camelCard);
			}
		}

		while (!camelCardsTwoPair.isEmpty()) {
			CamelHand handMax = camelCardsTwoPair.get(0);
			for (int j = 1; j < camelCardsTwoPair.size(); j++) {
				CamelHand hand2 = camelCardsTwoPair.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsTwoPair.remove(handMax);
		}

		// 1 paire
		List<CamelHand> camelCardsOnePair = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(ONE_PAIR)) {
				camelCardsOnePair.add(camelCard);
			}
		}

		while (!camelCardsOnePair.isEmpty()) {
			CamelHand handMax = camelCardsOnePair.get(0);
			for (int j = 1; j < camelCardsOnePair.size(); j++) {
				CamelHand hand2 = camelCardsOnePair.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsOnePair.remove(handMax);
		}

		// high card
		List<CamelHand> camelCardsHighCard = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(HIGH_CARD)) {
				camelCardsHighCard.add(camelCard);
			}
		}

		while (!camelCardsHighCard.isEmpty()) {
			CamelHand handMax = camelCardsHighCard.get(0);
			for (int j = 1; j < camelCardsHighCard.size(); j++) {
				CamelHand hand2 = camelCardsHighCard.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher2(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsHighCard.remove(handMax);
		}

		// fin
		int sum = 0;
		for (CamelHand camelCard : camelCards) {
			sum = sum + camelCard.getBid() * camelCard.getRank();
		}

		// trier les mains
		int rank = 1000;
		List<CamelHand> camelCardsTriees = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			for (CamelHand camelCard : camelCards) {
				if (camelCard.getRank() == rank) {
					camelCardsTriees.add(camelCard);
					rank--;
				}
			}
		}
		System.out.println("Run2: " + sum);

	}

	private void initJoker(List<CamelHand> camelCards) {
		for (CamelHand camelCard : camelCards) {

			if (camelCard.getType().equals(FOUR_OF_A_KIND)) { // carr�
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getValue() == 1 && mapElement.getKey() == 'J') {
						camelCard.setType(FIVE_OF_A_KIND);
					}
					if (mapElement.getValue() == 4 && mapElement.getKey() == 'J') {
						camelCard.setType(FIVE_OF_A_KIND);
					}
				}
			} // fin carr�

			if (camelCard.getType().equals(FULL_HOUSE)) { // full
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getKey() == 'J') {
						camelCard.setType(FIVE_OF_A_KIND);
					}
				}
			} // fin full

			if (camelCard.getType().equals(THREE_OF_A_KIND)) { // brelan
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getKey() == 'J' && mapElement.getValue() == 3) {
						camelCard.setType(FOUR_OF_A_KIND);
					}
					if (mapElement.getKey() == 'J' && mapElement.getValue() == 1) {
						camelCard.setType(FOUR_OF_A_KIND);
					}
				}

			} // fin brelan

			if (camelCard.getType().equals(TWO_PAIR)) { // 2 paires
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getValue() == 1 && mapElement.getKey() == 'J') {
						camelCard.setType(FULL_HOUSE);
					}
					if (mapElement.getValue() == 2 && mapElement.getKey() == 'J') {
						camelCard.setType(FOUR_OF_A_KIND);
					}
				}
			} // fin 2 paires

			if (camelCard.getType().equals(ONE_PAIR)) { // 1 paire
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getKey() == 'J' && mapElement.getValue() == 2) {
						camelCard.setType(THREE_OF_A_KIND);
					}
					if (mapElement.getKey() == 'J' && mapElement.getValue() == 1) {
						camelCard.setType(THREE_OF_A_KIND);
					}
				}
			} // fin 1 paire

			if (camelCard.getType().equals(HIGH_CARD)) { // high card
				Map<Character, Integer> count = findDuplicates(camelCard.getCarte1() + camelCard.getCarte2() + camelCard.getCarte3() + camelCard.getCarte4() + camelCard.getCarte5());
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getKey() == 'J') {
						camelCard.setType(ONE_PAIR);
					}
				}
			} // fin full
		}

	}

	public void run1() {

		List<CamelHand> camelCards = new ArrayList<>();
		String file = "src/main/resources/advent2023/day07.txt";
		init(camelCards, file);
		int ranks = camelCards.size();

		// quinte
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FIVE_OF_A_KIND)) {
				camelCard.setRank(ranks);
				ranks--;
			}
		}

		// carre
		List<CamelHand> camelCardsFourOfAKind = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FOUR_OF_A_KIND)) {
				camelCardsFourOfAKind.add(camelCard);
			}
		}

		while (!camelCardsFourOfAKind.isEmpty()) {
			CamelHand handMax = camelCardsFourOfAKind.get(0);
			for (int j = 1; j < camelCardsFourOfAKind.size(); j++) {
				CamelHand hand2 = camelCardsFourOfAKind.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsFourOfAKind.remove(handMax);
		}

		// full
		List<CamelHand> camelCardsFull = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(FULL_HOUSE)) {
				camelCardsFull.add(camelCard);
			}
		}

		while (!camelCardsFull.isEmpty()) {
			CamelHand handMax = camelCardsFull.get(0);
			for (int j = 1; j < camelCardsFull.size(); j++) {
				CamelHand hand2 = camelCardsFull.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsFull.remove(handMax);
		}

		// brelan
		List<CamelHand> camelCardsBrelan = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(THREE_OF_A_KIND)) {
				camelCardsBrelan.add(camelCard);
			}
		}

		while (!camelCardsBrelan.isEmpty()) {
			CamelHand handMax = camelCardsBrelan.get(0);
			for (int j = 1; j < camelCardsBrelan.size(); j++) {
				CamelHand hand2 = camelCardsBrelan.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsBrelan.remove(handMax);
		}

		// 2 paires
		List<CamelHand> camelCardsTwoPair = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(TWO_PAIR)) {
				camelCardsTwoPair.add(camelCard);
			}
		}

		while (!camelCardsTwoPair.isEmpty()) {
			CamelHand handMax = camelCardsTwoPair.get(0);
			for (int j = 1; j < camelCardsTwoPair.size(); j++) {
				CamelHand hand2 = camelCardsTwoPair.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsTwoPair.remove(handMax);
		}

		// 1 paire
		List<CamelHand> camelCardsOnePair = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(ONE_PAIR)) {
				camelCardsOnePair.add(camelCard);
			}
		}

		while (!camelCardsOnePair.isEmpty()) {
			CamelHand handMax = camelCardsOnePair.get(0);
			for (int j = 1; j < camelCardsOnePair.size(); j++) {
				CamelHand hand2 = camelCardsOnePair.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsOnePair.remove(handMax);
		}

		// high card
		List<CamelHand> camelCardsHighCard = new ArrayList<>();
		for (CamelHand camelCard : camelCards) {
			if (camelCard.getType().equals(HIGH_CARD)) {
				camelCardsHighCard.add(camelCard);
			}
		}

		while (!camelCardsHighCard.isEmpty()) {
			CamelHand handMax = camelCardsHighCard.get(0);
			for (int j = 1; j < camelCardsHighCard.size(); j++) {
				CamelHand hand2 = camelCardsHighCard.get(j);
				handMax = compareTowHandsOfSameTypeAndReturnHigher(handMax, hand2);
			}
			handMax.setRank(ranks);
			ranks--;
			camelCardsHighCard.remove(handMax);
		}

		// fin
		int sum = 0;
		for (CamelHand camelCard : camelCards) {
			sum = sum + camelCard.getBid() * camelCard.getRank();
		}
		System.out.println("Run1: " + sum);

	}

	private CamelHand compareTowHandsOfSameTypeAndReturnHigher2(CamelHand h1, CamelHand h2) {
		// carte1
		int valeur1 = 0;
		valeur1 = calculeValeurCarte2(h1.getCarte1());
		int valeur2 = 0;
		valeur2 = calculeValeurCarte2(h2.getCarte1());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte2
		valeur1 = 0;
		valeur1 = calculeValeurCarte2(h1.getCarte2());
		valeur2 = 0;
		valeur2 = calculeValeurCarte2(h2.getCarte2());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte3
		valeur1 = calculeValeurCarte2(h1.getCarte3());
		valeur2 = calculeValeurCarte2(h2.getCarte3());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte4
		valeur1 = calculeValeurCarte2(h1.getCarte4());
		valeur2 = calculeValeurCarte2(h2.getCarte4());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte5
		valeur1 = calculeValeurCarte2(h1.getCarte5());
		valeur2 = calculeValeurCarte2(h2.getCarte5());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		System.out.println("Erreur comparaison");
		return null;
	}

	private CamelHand compareTowHandsOfSameTypeAndReturnHigher(CamelHand h1, CamelHand h2) {
		// carte1
		int valeur1 = 0;
		valeur1 = calculeValeurCarte(h1.getCarte1());
		int valeur2 = 0;
		valeur2 = calculeValeurCarte(h2.getCarte1());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte2
		valeur1 = calculeValeurCarte(h1.getCarte2());
		valeur2 = calculeValeurCarte(h2.getCarte2());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte3
		valeur1 = calculeValeurCarte(h1.getCarte3());
		valeur2 = calculeValeurCarte(h2.getCarte3());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte4
		valeur1 = calculeValeurCarte(h1.getCarte4());
		valeur2 = calculeValeurCarte(h2.getCarte4());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		// carte5
		valeur1 = calculeValeurCarte(h1.getCarte5());
		valeur2 = calculeValeurCarte(h2.getCarte5());

		if (valeur1 > valeur2) {
			return h1;
		}
		if (valeur1 < valeur2) {
			return h2;
		}

		System.out.println("Erreur comparaison");
		return null;
	}

	private int calculeValeurCarte(String c) {
		int valeur = 0;
		switch (c) {
			case "A":
				valeur = 14;
				break;
			case "K":
				valeur = 13;
				break;
			case "Q":
				valeur = 12;
				break;
			case "J":
				valeur = 11;
				break;
			case "T":
				valeur = 10;
				break;
			case "9":
				valeur = 9;
				break;
			case "8":
				valeur = 8;
				break;
			case "7":
				valeur = 7;
				break;
			case "6":
				valeur = 6;
				break;
			case "5":
				valeur = 5;
				break;
			case "4":
				valeur = 4;
				break;
			case "3":
				valeur = 3;
				break;
			case "2":
				valeur = 2;
				break;
			default:
				System.out.println("erreur");
				break;
		}
		return valeur;
	}

	private int calculeValeurCarte2(String c) {
		int valeur = 0;
		switch (c) {
			case "A":
				valeur = 13;
				break;
			case "K":
				valeur = 12;
				break;
			case "Q":
				valeur = 11;
				break;
			case "T":
				valeur = 10;
				break;
			case "9":
				valeur = 9;
				break;
			case "8":
				valeur = 8;
				break;
			case "7":
				valeur = 7;
				break;
			case "6":
				valeur = 6;
				break;
			case "5":
				valeur = 5;
				break;
			case "4":
				valeur = 4;
				break;
			case "3":
				valeur = 3;
				break;
			case "2":
				valeur = 2;
				break;
			case "J":
				valeur = 1;
				break;
			default:
				System.out.println("erreur");
				break;
		}
		return valeur;
	}

	private void init(List<CamelHand> camelCards, String file) {
		List<String> res = Outil.importationString(file);
		for (String ligne : res) {
			CamelHand camelCard = new CamelHand();
			String cards = StringUtils.split(ligne, " ")[0];
			int bid = Integer.parseInt(StringUtils.split(ligne, " ")[1]);
			camelCard.setBid(bid);
			camelCard.setCarte1("" + cards.charAt(0));
			camelCard.setCarte2("" + cards.charAt(1));
			camelCard.setCarte3("" + cards.charAt(2));
			camelCard.setCarte4("" + cards.charAt(3));
			camelCard.setCarte5("" + cards.charAt(4));

			// calcul du type
			Map<Character, Integer> count = findDuplicates(cards);
			for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
				if (mapElement.getValue() == 5) {
					camelCard.setType(FIVE_OF_A_KIND);
				}
				if (mapElement.getValue() == 4) {
					camelCard.setType(FOUR_OF_A_KIND);

				}

			}
			if (count.size() == 2) {
				int nbOccurrences1 = 0;
				int nbOccurrences2 = 0;
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (nbOccurrences1 == 0) {
						nbOccurrences1 = mapElement.getValue();
					} else {
						nbOccurrences2 = mapElement.getValue();
					}
				}
				if ((nbOccurrences1 == 3 && nbOccurrences2 == 2) || (nbOccurrences2 == 3 && nbOccurrences1 == 2)) {
					camelCard.setType(FULL_HOUSE);
				}

			}
			if (count.size() == 3) { // brelan ou 2 paires
				for (Map.Entry<Character, Integer> mapElement : count.entrySet()) {
					if (mapElement.getValue() == 3) {
						camelCard.setType(THREE_OF_A_KIND);
					}
				}
				if (camelCard.getType() == null) {
					camelCard.setType(TWO_PAIR);
				}

			}
			if (count.size() == 4) {
				camelCard.setType(ONE_PAIR);
			}
			if (count.size() == 5) {
				camelCard.setType(HIGH_CARD);
			}

			camelCards.add(camelCard);
		}

	}

	public static Map<Character, Integer> findDuplicates(String str) {
		Map<Character, Integer> count = new HashMap<>();
		for (int i = 0; i < str.length(); i++) {
			if (count.containsKey(str.charAt(i)))
				count.put(str.charAt(i), count.get(str.charAt(i)) + 1);
			else
				count.put(str.charAt(i), 1);
		}

		return count;

	}

}
