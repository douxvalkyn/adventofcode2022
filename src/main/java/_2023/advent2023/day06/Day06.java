package advent2023.day06;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Day06 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day06.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day06.class.getSimpleName());
		Day06 day = new Day06();
		LocalDateTime start = LocalDateTime.now();
		// day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		List<Long> times = new ArrayList<>();
		List<Long> distances = new ArrayList<>();
		// initExemple2(times, distances);
		init2(times, distances);
		int score = 1;
		for (int i = 0; i < times.size(); i++) {
			int win = 0;
			Long time = times.get(i);
			Long distance = distances.get(i);
			for (long hold = 0; hold <= time; hold++) {
				long remainingTime = time - hold;
				long dist = hold * remainingTime;
				if (dist > distance) {
					win++;
				}
			}
			score = score * win;
		}
		System.out.println(score);
	}

	public void run1() {
		List<Integer> times = new ArrayList<Integer>();
		List<Integer> distances = new ArrayList<Integer>();
		// initExemple(times, distances);
		init(times, distances);
		int score = 1;
		for (int i = 0; i < times.size(); i++) {
			int win = 0;
			Integer time = times.get(i);
			Integer distance = distances.get(i);
			for (int hold = 0; hold <= time; hold++) {
				int remainingTime = time - hold;
				int dist = hold * remainingTime;
				if (dist > distance) {
					win++;
				}
			}
			System.out.println(win);
			score = score * win;
		}
		System.out.println(score);
	}

	private void init(List<Integer> times, List<Integer> distances) {
		times.add(48);
		times.add(87);
		times.add(69);
		times.add(81);

		distances.add(255);
		distances.add(1288);
		distances.add(1117);
		distances.add(1623);
	}

	private void initExemple(List<Integer> times, List<Integer> distances) {
		times.add(7);
		times.add(15);
		times.add(30);

		distances.add(9);
		distances.add(40);
		distances.add(200);
	}

	private void init2(List<Long> times, List<Long> distances) {
		times.add(48876981L);
		distances.add(255128811171623L);
	}

	private void initExemple2(List<Long> times, List<Long> distances) {
		times.add(71530L);
		distances.add(940200L);
	}

}
