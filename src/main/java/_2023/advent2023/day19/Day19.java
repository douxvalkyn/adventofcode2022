package advent2023.day19;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day19 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day19.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day19.class.getSimpleName());
		Day19 day = new Day19();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day19.txt";
		List<String> fichier = Outil.importationString(file);
		List<Rule> rules = new ArrayList<>();
		List<String> ratings = new ArrayList<String>();
		init(fichier, rules, ratings);
		for (int i = 0; i < 100_000; i++) {
			workflows(ratings, rules);
		}

	}

	private void workflows(List<String> ratings, List<Rule> rules) {
		int totalRules = 0;
		for (String rating : ratings) {
			String[] xmas = StringUtils.split(rating, ",");
			int x = Integer.parseInt(StringUtils.split(xmas[0], "=")[1]);
			int m = Integer.parseInt(StringUtils.split(xmas[1], "=")[1]);
			int a = Integer.parseInt(StringUtils.split(xmas[2], "=")[1]);
			int s = Integer.parseInt(StringUtils.split(StringUtils.split(xmas[3], "=")[1], "}")[0]);

			String actualRuleName = "in";
			while (!actualRuleName.equals("A") && !actualRuleName.equals("R")) {
				actualRuleName = applyRule(x, m, a, s, rules, actualRuleName);
			}

			if (actualRuleName.equals("A")) {
				int totalRule = x + m + a + s;
				totalRules = totalRules + totalRule;
			}

		}
		// System.err.println(totalRules);
	}

	private String applyRule(int x, int m, int a, int s, List<Rule> rules, String actualRuleName) {
		Rule actualRule = findRule(rules, actualRuleName);
		for (int i = 0; i < actualRule.getNbRules(); i++) {
			String[] xxx = null;
			if (i == 0) { // Rule 1
				xxx = StringUtils.split(actualRule.getRule1(), ":");
			}
			if (i == 1) { // Rule 2
				xxx = StringUtils.split(actualRule.getRule2(), ":");
			}
			if (i == 2) { // Rule 3
				xxx = StringUtils.split(actualRule.getRule3(), ":");
			}
			if (i == 3) { // Rule 4
				xxx = StringUtils.split(actualRule.getRule4(), ":");
			}
			if (i == 4) { // Rule 5
				xxx = StringUtils.split(actualRule.getRule5(), ":");
			}

			if (xxx.length == 1 && xxx[0].equals("R")) {
				return "R";
			}
			if (xxx.length == 1 && xxx[0].equals("A")) {
				return "A";
			}
			if (xxx.length == 1 && xxx[0].length() > 1) {
				return xxx[0];
			}
			String nextRuleName = xxx[1];
			if (xxx[0].contains(">")) {
				String xmas = StringUtils.split(xxx[0], ">")[0];
				int nbAVerifier = Integer.parseInt(StringUtils.split(xxx[0], ">")[1]);
				if (xmas.equals("x")) {
					if (x > nbAVerifier) {
						return nextRuleName;
					}
				}
				if (xmas.equals("m")) {
					if (m > nbAVerifier) {
						return nextRuleName;
					}
				}
				if (xmas.equals("a")) {
					if (a > nbAVerifier) {
						return nextRuleName;
					}
				}
				if (xmas.equals("s")) {
					if (s > nbAVerifier) {
						return nextRuleName;
					}
				}
			}
			if (xxx[0].contains("<")) {
				String xmas = StringUtils.split(xxx[0], "<")[0];
				int nbAVerifier = Integer.parseInt(StringUtils.split(xxx[0], "<")[1]);
				if (xmas.equals("x")) {
					if (x < nbAVerifier) {
						return nextRuleName;
					}
				}
				if (xmas.equals("m")) {
					if (m < nbAVerifier) {
						return nextRuleName;
					}
				}
				if (xmas.equals("a")) {
					if (a < nbAVerifier) {
						return nextRuleName;

					}
				}
				if (xmas.equals("s")) {
					if (s < nbAVerifier) {
						return nextRuleName;
					}
				}
			}
		}
		return null;

	}

	private Rule findRule(List<Rule> rules, String actualRuleName) {
		for (Rule rule : rules) {
			if (rule.getName().equals(actualRuleName)) {
				return rule;
			}
		}
		return null;
	}

	private void init(List<String> fichier, List<Rule> rules, List<String> ratings) {
		for (String ligne : fichier) {
			if (!ligne.startsWith("{") && ligne.length() > 0) { // rules
				Rule rule = new Rule();
				rule.setName(StringUtils.split(ligne, "{")[0]);

				String[] regles = StringUtils.split(StringUtils.split(StringUtils.split(ligne, "{")[1], "}")[0], ",");
				for (int i = 0; i < regles.length; i++) {
					if (i == 0) {
						rule.setRule1(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
					if (i == 1) {
						rule.setRule2(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
					if (i == 2) {
						rule.setRule3(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
					if (i == 3) {
						rule.setRule4(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
					if (i == 4) {
						rule.setRule5(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
					if (i == 5) {
						rule.setRule6(regles[i]);
						rule.setNbRules(rule.getNbRules() + 1);
					}
				}
				rules.add(rule);
			}

			if (ligne.startsWith("{")) { // part ratings
				ratings.add(ligne);
			}

		}
	}

}