package advent2023.day19;

public class Rule {

	private String name;
	private String rule1;
	private String rule2;
	private String rule3;
	private String rule4;
	private String rule5;
	private String rule6;
	private int nbRules = 0;

	public int getNbRules() {
		return nbRules;
	}

	public void setNbRules(int nbRules) {
		this.nbRules = nbRules;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRule1() {
		return rule1;
	}

	public void setRule1(String rule1) {
		this.rule1 = rule1;
	}

	public String getRule2() {
		return rule2;
	}

	public void setRule2(String rule2) {
		this.rule2 = rule2;
	}

	public String getRule3() {
		return rule3;
	}

	public void setRule3(String rule3) {
		this.rule3 = rule3;
	}

	public String getRule4() {
		return rule4;
	}

	public void setRule4(String rule4) {
		this.rule4 = rule4;
	}

	public String getRule5() {
		return rule5;
	}

	public void setRule5(String rule5) {
		this.rule5 = rule5;
	}

	public String getRule6() {
		return rule6;
	}

	public void setRule6(String rule6) {
		this.rule6 = rule6;
	}

	@Override
	public String toString() {
		return "Rule [name=" + name + ", rule1=" + rule1 + ", rule2=" + rule2 + ", rule3=" + rule3 + ", rule4=" + rule4 + ", rule5=" + rule5 + ", rule6=" + rule6 + "]";
	}

	public Rule() {
		super();
		// TODO Auto-generated constructor stub
	}

}
