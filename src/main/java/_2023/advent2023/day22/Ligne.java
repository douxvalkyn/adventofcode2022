package advent2023.day22;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Ligne implements Comparable<Ligne> {

	private List<Cube> cubes;
	private boolean desintegrable;

	public boolean isDesintegrable() {
		return desintegrable;
	}

	public void setDesintegrable(boolean desintegrable) {
		this.desintegrable = desintegrable;
	}

	public List<Cube> getCubes() {
		return cubes;
	}

	public void setCubes(List<Cube> cubes) {
		this.cubes = cubes;
	}

	public Ligne() {
		super();
		this.cubes = new ArrayList<Cube>();
	}

	public Ligne(List<Cube> cubes) {
		this.cubes = new ArrayList<>();
		for (Cube cube : cubes) {
			// Cr�er de nouveaux objets Cube pour la copie profonde
			this.cubes.add(new Cube(cube.getX(), cube.getY(), cube.getZ()));
		}
	}

	@Override
	public String toString() {
		return "[" + cubes + ", desintegrable=" + desintegrable + "]";
	}

	@Override
	public int compareTo(Ligne autreLigne) {
		// Comparaison bas�e sur la coordonn�e z du cube avec la plus petite valeur de z
		if (!this.cubes.isEmpty() && !autreLigne.cubes.isEmpty()) {
			Cube cubeActuel = this.cubes.stream().min(Comparator.comparingInt(Cube::getZ)).orElseThrow(NoSuchElementException::new);
			Cube autreCube = autreLigne.cubes.stream().min(Comparator.comparingInt(Cube::getZ)).orElseThrow(NoSuchElementException::new);
			// Comparaison des coordonn�es z
			return Integer.compare(cubeActuel.getZ(), autreCube.getZ());
		} else {
			// Gestion du cas o� l'une des lignes n'a pas de cubes
			return Integer.compare(this.cubes.size(), autreLigne.cubes.size());
		}
	}

	public Cube getCubeWithMinZ() {
		return cubes.stream().min(Comparator.comparingInt(Cube::getZ)).orElse(null);
	}

	public List<Cube> getCubesWithMinZ() {
		int minZValue = cubes.stream().mapToInt(Cube::getZ).min().orElseThrow(); // Vous pouvez remplacer cela par une valeur par d�faut ou un traitement d'erreur

		return cubes.stream().filter(cube -> cube.getZ() == minZValue).collect(Collectors.toList());
	}
}
