package advent2023.day22;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day22 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day22.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day22.class.getSimpleName());
		Day22 day = new Day22();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day22.txt";
		List<String> fichier = Outil.importationString(file);
		List<Ligne> lignes3D = new ArrayList<>();
		initCubes(fichier, lignes3D);
		Collections.sort(lignes3D);
		down(lignes3D);
		analyseSupport(lignes3D);
		analyseNombreLignesQuiTombentEnSupprimantUneLigne(lignes3D);

	}

	private void analyseNombreLignesQuiTombentEnSupprimantUneLigne(List<Ligne> lignes3d) {
		int cpt = 0;
		for (Ligne ligne : lignes3d) {
			if (!ligne.isDesintegrable()) { // on analyse les lignes non desintegrables
				// faire une copie de lignes3d down, enlever la ligne en cours et compter combien de lignes tombent
				List<Ligne> copieLignes = new ArrayList<>();
				for (Ligne ligne2 : lignes3d) {
					if (ligne2 != ligne) {
						// Cr�er de nouveaux objets Ligne pour la copie profonde
						copieLignes.add(new Ligne(ligne2.getCubes()));
					}
				}
				cpt = cpt + down2(copieLignes);

			}
		}
		System.out.println("Run2: " + cpt);
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day22.txt";
		List<String> fichier = Outil.importationString(file);
		List<Ligne> lignes3D = new ArrayList<>();
		initCubes(fichier, lignes3D);
		Collections.sort(lignes3D);
		down(lignes3D);
		analyseSupport(lignes3D);
		int cpt = comptageDesintegrable(lignes3D);
		System.out.println("Run1: " + cpt);
	}

	private int comptageDesintegrable(List<Ligne> lignes3d) {
		int cpt = 0;
		for (Ligne ligne : lignes3d) {
			if (ligne.isDesintegrable()) {
				cpt++;
			}
		}
		return cpt;
	}

	private void analyseSupport(List<Ligne> lignes3d) {
		for (Ligne ligne : lignes3d) {
			List<Ligne> lignesAuDessus = trouveLignesAuDessus(ligne, lignes3d);
			Set<Ligne> ligneSet = new HashSet<>(lignesAuDessus);
			List<Ligne> lignesAuDessusSansDoublon = new ArrayList<>(ligneSet);

			// ces lignes sont elles toutes support�es par plusieurs lignes ?
			boolean desintegrable = true;
			if (lignesAuDessusSansDoublon.isEmpty()) {
				ligne.setDesintegrable(desintegrable);
			} else {
				for (Ligne ligne2 : lignesAuDessusSansDoublon) {
					List<Ligne> lignesAuDessous = trouveLignesAuDessous(ligne2, lignes3d);
					Set<Ligne> ligneSet2 = new HashSet<>(lignesAuDessous);
					List<Ligne> lignesAuDessousSansDoublon = new ArrayList<>(ligneSet2);
					if (lignesAuDessousSansDoublon.size() > 1) {
						desintegrable = desintegrable && true;
					} else {
						desintegrable = false;
					}

					ligne.setDesintegrable(desintegrable);

				}
			}
		}
	}

	private List<Ligne> trouveLignesAuDessous(Ligne ligne, List<Ligne> lignes3d) {
		List<Ligne> lignesAuDessus = new ArrayList<Ligne>();
		for (Cube cube : ligne.getCubes()) {
			for (Ligne ligne2 : lignes3d) {
				if (ligne != ligne2) {
					for (Cube cube2 : ligne2.getCubes()) {
						if (cube2.getX() == cube.getX() && cube2.getY() == cube.getY() && (cube2.getZ() + 1) == cube.getZ()) {
							lignesAuDessus.add(ligne2);
						}
					}
				}
			}
		}
		return lignesAuDessus;
	}

	private List<Ligne> trouveLignesAuDessus(Ligne ligne, List<Ligne> lignes3d) {
		List<Ligne> lignesAuDessus = new ArrayList<Ligne>();
		for (Cube cube : ligne.getCubes()) {
			for (Ligne ligne2 : lignes3d) {
				if (ligne != ligne2) {
					for (Cube cube2 : ligne2.getCubes()) {
						if (cube2.getX() == cube.getX() && cube2.getY() == cube.getY() && cube2.getZ() == (cube.getZ() + 1)) {
							lignesAuDessus.add(ligne2);
						}
					}
				}
			}
		}
		return lignesAuDessus;
	}

	private int down2(List<Ligne> lignes3d) {
		int cpt = 0;
		for (Ligne ligne : lignes3d) {

			boolean descend = true;
			boolean descendu = false;
			while (descend) {
				descend = descendSiPossible(lignes3d, ligne);
				if (descend && !descendu) {
					cpt++;
					descendu = true;
				}
			}
		}
		return cpt;
	}

	private void down(List<Ligne> lignes3d) {
		for (Ligne ligne : lignes3d) {
			boolean descend = true;
			while (descend) {
				descend = descendSiPossible(lignes3d, ligne);
			}
		}
	}

	private boolean descendSiPossible(List<Ligne> lignes3d, Ligne ligne) {
		// on regarde les cases avec la coordonn�e Z minimale
		List<Cube> cubes = ligne.getCubesWithMinZ();

		// on regarde si toutes les cases en-dessous sont vides
		var trouveCubeEnDessous = false;
		for (Cube cube : cubes) {
			if (trouveCubeEnDessous || trouveCubeEnDessous(cube, lignes3d)) {
				trouveCubeEnDessous = true;
			}
		}
		boolean descend = false;
		if (!trouveCubeEnDessous && ligne.getCubeWithMinZ().getZ() > 1) {// on descend d'une case toute la ligne
			descend = true;
			for (Cube cube : ligne.getCubes()) {
				cube.setZ(cube.getZ() - 1);
			}
		}
		return descend;
	}

	private boolean trouveCubeEnDessous(Cube cube1, List<Ligne> lignes3d) {
		for (Ligne ligne : lignes3d) {
			for (Cube cube : ligne.getCubes()) {
				if (cube.getX() == cube1.getX() && cube.getY() == cube1.getY() && cube.getZ() == (cube1.getZ() - 1)) {
					return true;
				}
			}
		}
		return false;
	}

	private void initCubes(List<String> fichier, List<Ligne> lignes3d) {
		for (String ligne : fichier) {
			Ligne ligne3d = new Ligne();
			String[] deb = StringUtils.split(StringUtils.split(ligne, "~")[0], ",");
			String[] fin = StringUtils.split(StringUtils.split(ligne, "~")[1], ",");
			Cube cubeDeb = new Cube();
			cubeDeb.setX(Integer.parseInt(StringUtils.trim(deb[0])));
			cubeDeb.setY(Integer.parseInt(StringUtils.trim(deb[1])));
			cubeDeb.setZ(Integer.parseInt(StringUtils.trim(deb[2])));
			Cube cubeFin = new Cube();
			cubeFin.setX(Integer.parseInt(StringUtils.trim(fin[0])));
			cubeFin.setY(Integer.parseInt(StringUtils.trim(fin[1])));
			cubeFin.setZ(Integer.parseInt(StringUtils.trim(fin[2])));
			if (cubeFin.getX() == cubeDeb.getX() && cubeFin.getY() == cubeDeb.getY() && cubeFin.getZ() == cubeDeb.getZ()) {
				ligne3d.getCubes().add(cubeDeb);
				lignes3d.add(ligne3d);
			} else {
				ajoutCubesEntreDebutEtFin(cubeDeb, cubeFin, ligne3d);
				lignes3d.add(ligne3d);
			}
		}

	}

	private void ajoutCubesEntreDebutEtFin(Cube cubeDeb, Cube cubeFin, Ligne ligne3d) {
		if (cubeDeb.getX() != cubeFin.getX()) {
			for (int i = cubeDeb.getX(); i <= cubeFin.getX(); i++) {
				Cube cube = new Cube();
				cube.setX(i);
				cube.setY(cubeDeb.getY());
				cube.setZ(cubeDeb.getZ());
				ligne3d.getCubes().add(cube);
			}
		}
		if (cubeDeb.getY() != cubeFin.getY()) {
			for (int i = cubeDeb.getY(); i <= cubeFin.getY(); i++) {
				Cube cube = new Cube();
				cube.setY(i);
				cube.setX(cubeDeb.getX());
				cube.setZ(cubeDeb.getZ());
				ligne3d.getCubes().add(cube);
			}
		}
		if (cubeDeb.getZ() != cubeFin.getZ()) {
			for (int i = cubeDeb.getZ(); i <= cubeFin.getZ(); i++) {
				Cube cube = new Cube();
				cube.setZ(i);
				cube.setY(cubeDeb.getY());
				cube.setX(cubeDeb.getX());
				ligne3d.getCubes().add(cube);
			}
		}

	}

}