package advent2023.day15;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day15 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day15.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day15.class.getSimpleName());
		Day15 day = new Day15();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day15.txt";
		List<String> fichier = Outil.importationString(file);
		String[] fichierSepare = StringUtils.split(fichier.get(0), ',');
		List<String> sequence = Arrays.asList(fichierSepare);

		int sum = 0;
		for (String seq : sequence) {
			int currentValue = hash(seq);
			sum = sum + currentValue;
		}
		System.err.println(sum);
	}

	public int hash(String seq) {
		int currentValue = 0;
		for (int i = 0; i < seq.length(); i++) {
			int ascii = (int) seq.charAt(i);
			currentValue = currentValue + ascii;
			currentValue = currentValue * 17;
			currentValue = currentValue % 256;
		}
		return currentValue;
	}
	
	
	
	
	public void run2() {
		String file = "src/main/resources/advent2023/day15.txt";
		List<String> fichier = Outil.importationString(file);
		List<String> sequence = Arrays.asList(StringUtils.split(fichier.get(0), ','));
		
		List<Box> boxes = new ArrayList<Box>();
		for (int i=0;i<256;i++) {
			Box box = new Box();
			box.setNumeroBox(i);
			boxes.add(box);
		}
		
		for (String seq : sequence) {
			if (seq.contains("-")) { //dash
				String debut = StringUtils.split(seq,'-')[0];
				int numeroBox = hash(debut);
				List<String> lensDeLaBox = boxes.get(numeroBox).getLens();
				removeLens(debut, lensDeLaBox);
			}
			if (seq.contains("=")) { //equals
				String debut = StringUtils.split(seq,'=')[0];
				int focalLength = Integer.parseInt( StringUtils.split(seq,'=')[1]);
				int numeroBox = hash(debut);
				List<String> lensDeLaBox = boxes.get(numeroBox).getLens();
				
				int toReplace=-1;
				for (int i=0;i<lensDeLaBox.size();i++) {
					String l = lensDeLaBox.get(i);
					if (l.contains(debut)) {
						toReplace=i;
					}
				}
				if (toReplace!=-1) {
					lensDeLaBox.set(toReplace, debut+" "+focalLength);
				}else {
					lensDeLaBox.add( debut+" "+focalLength);
				}
				
			}
			
		}
		
		//comptages de fin
		int sum=0;
		for (Box box:boxes) {
			for (int j=0;j<box.getLens().size();j++) {
				String l = box.getLens().get(j);
				String aaa = StringUtils.split(l, ' ')[0];
				int focal =Integer.parseInt( StringUtils.split(l, ' ')[1]);
			
				int score = (box.getNumeroBox()+1)*focal*(j+1);
			sum=sum+score;
			}
		}
		System.err.println(sum);
		
		
		
	}

	public void removeLens(String debut, List<String> lensDeLaBox) {
		int toRemove=-1;
		for (int i=0;i<lensDeLaBox.size();i++) {
			String l = lensDeLaBox.get(i);
			if (l.contains(debut)) {
				toRemove = i;
			}
		}
		if (toRemove!=-1) {
			lensDeLaBox.remove(toRemove);
		}
	}

}