package advent2023.day15;

import java.util.ArrayList;
import java.util.List;

public class Box {

	
	private int numeroBox;
	private List<String>lens;
	public int getNumeroBox() {
		return numeroBox;
	}
	public void setNumeroBox(int numeroBox) {
		this.numeroBox = numeroBox;
	}
	public List<String> getLens() {
		return lens;
	}
	public void setLens(List<String> lens) {
		this.lens = lens;
	}
	@Override
	public String toString() {
		return "Box [numeroBox=" + numeroBox + ", lens=" + lens + "]";
	}
	public Box() {
		super();
		List<String> lens = new ArrayList<>();
		this.setLens(lens);
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
