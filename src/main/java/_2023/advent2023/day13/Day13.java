package advent2023.day13;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day13 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day13.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day13.class.getSimpleName());
		Day13 day = new Day13();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}



	public void run2() { 
		String file = "src/main/resources/advent2023/day13.txt";
		List<String> fichier = Outil.importationString(file);
		List<Grille> grilles = new ArrayList<>();

		List<String> pattern = new ArrayList<String>();
		initGrilles(fichier, grilles, pattern);

		List<String> axes = new ArrayList(); 
		//recherche premier axe pour chaque grille
		for (Grille grille : grilles) {
			int numColonneSymetrieVerticale = rechercheAxeSymetrieVertical(grille);
			if (numColonneSymetrieVerticale != -1) {
				axes.add("V"+numColonneSymetrieVerticale);
			}
			int numLigneSymetrieHorizontale = rechercheAxeSymetrieHorizontal(grille);
			if (numLigneSymetrieHorizontale != -1) {
				axes.add("H"+numLigneSymetrieHorizontale);
			}
		}


		//recherche 2e axe de symetrie
		int v = 0;
		int h = 0;
		int  i=0;
		for (int t=0;t<grilles.size();t++) {
			String axePrecedent = axes.get(i);
			Grille grille = grilles.get(t);

			List<Case> cases = grille.getCases();
			int j=0;
			boolean fini=false;
			while (!fini) {
				Case c = cases.get(j);
				if (c.getEtat().equals(".")) {
					c.setEtat("#");
				}else {
					c.setEtat(".");
				}
				List<Integer> numColonneSymetrieVerticale = rechercheAxesSymetrieVertical(grille);
				int numeroAxePrecedent = Integer.parseInt( StringUtils.substring(axePrecedent, 1));
				if (numColonneSymetrieVerticale!=null && numColonneSymetrieVerticale.size()>1) {
					numColonneSymetrieVerticale=nettoie(numColonneSymetrieVerticale,numeroAxePrecedent);
				
				}
				if (numColonneSymetrieVerticale!=null && numColonneSymetrieVerticale.get(0) != -1 && !axePrecedent.equals("V"+numColonneSymetrieVerticale.get(0)) ) {
					System.out.println("vertical: " + (numColonneSymetrieVerticale.get(0) + 1));
					v = v + numColonneSymetrieVerticale.get(0) + 1;
					fini=true;
				}
				List<Integer> numLigneSymetrieHorizontale = rechercheAxesSymetrieHorizontal(grille);
				numeroAxePrecedent = Integer.parseInt( StringUtils.substring(axePrecedent, 1));
				if (numLigneSymetrieHorizontale!=null && numLigneSymetrieHorizontale.size()>1) {
					numLigneSymetrieHorizontale=nettoie(numLigneSymetrieHorizontale,numeroAxePrecedent);
					}
				if (numLigneSymetrieHorizontale!=null &&numLigneSymetrieHorizontale.get(0) != -1 && !axePrecedent.equals("H"+numLigneSymetrieHorizontale.get(0))) {
					System.out.println("horizontal: " + (numLigneSymetrieHorizontale.get(0) + 1));
					h = h + numLigneSymetrieHorizontale.get(0) + 1;
					fini=true;
				}
				j++;
				if (c.getEtat().equals(".")) {
					c.setEtat("#");
				}else {
					c.setEtat(".");
				}
			}
			i++;
		}
		System.out.print("Run2: ");
		System.out.println(v + 100 * h);

	}


	private List<Integer> nettoie(List<Integer> liste, int numeroAxePrecedent) {
		for (int i=0;i<liste.size();i++) {
			if (liste.get(i)==numeroAxePrecedent) {
				liste.remove(i);
				return liste;
			}
		}
		return liste;
	}



	public void run1() {
		String file = "src/main/resources/advent2023/day13.txt";
		List<String> fichier = Outil.importationString(file);
		List<Grille> grilles = new ArrayList<>();

		List<String> pattern = new ArrayList<String>();
		initGrilles(fichier, grilles, pattern);

		int v = 0;
		int h = 0;

		for (Grille grille : grilles) {
			int numColonneSymetrieVerticale = rechercheAxeSymetrieVertical(grille);
			if (numColonneSymetrieVerticale != -1) {
				System.out.println("vertical: " + (numColonneSymetrieVerticale + 1));
				v = v + numColonneSymetrieVerticale + 1;
			}
			int numLigneSymetrieHorizontale = rechercheAxeSymetrieHorizontal(grille);
			if (numLigneSymetrieHorizontale != -1) {
				System.out.println("horizontal: " + (numLigneSymetrieHorizontale + 1));
				h = h + numLigneSymetrieHorizontale + 1;
			}
		}
		System.out.print("Run1: ");
		System.out.println(v + 100 * h);

	}

	private int rechercheAxeSymetrieHorizontal(Grille grille) {
		for (int i = 0; i < grille.getNbLignes() - 1; i++) {
			if (rechercheAxeSymetrieHorizontalSelonLigneEntreIetIplus1(grille, i) == i) {
				return i;
			}
		}
		return -1;
	}
	
	private List<Integer> rechercheAxesSymetrieHorizontal(Grille grille) {
		List<Integer>axes=new ArrayList();
		
		for (int i = 0; i < grille.getNbLignes() - 1; i++) {
			if (rechercheAxeSymetrieHorizontalSelonLigneEntreIetIplus1(grille, i) == i) {
				axes.add(i);
			}
		}
		if (!axes.isEmpty()) {
			return axes;
		}
		return null;
	}

	private List<Integer> rechercheAxesSymetrieVertical(Grille grille) {
		List<Integer>axes=new ArrayList();
		
		for (int i = 0; i < grille.getNbCol() - 1; i++) {
			if (rechercheAxeSymetrieVerticalSelonColonneEntreIetIplus1(grille, i) == i) {
				axes.add(i);
			}
		}
		if (!axes.isEmpty()) {
			return axes;
		}
		return null;
	}
	
	private int rechercheAxeSymetrieVertical(Grille grille) {
		for (int i = 0; i < grille.getNbCol() - 1; i++) {
			if (rechercheAxeSymetrieVerticalSelonColonneEntreIetIplus1(grille, i) == i) {
				return i;
			}
		}
		return -1;
	}

	private int rechercheAxeSymetrieHorizontalSelonLigneEntreIetIplus1(Grille grille, int i) {

		boolean test = true;
		for (int j = 0; j < grille.getNbLignes(); j++) {
			if (grille.getCase(0, i - j) != null && grille.getCase(0, i + j + 1) != null) {
				test = test && comparaisonLignes(grille, i - j, i + j + 1);
			}

		}
		if (test) {
			return i;
		}
		return -1;
	}

	private int rechercheAxeSymetrieVerticalSelonColonneEntreIetIplus1(Grille grille, int i) {

		boolean test = true;
		for (int j = 0; j < grille.getNbCol(); j++) {
			if (grille.getCase(i - j, 0) != null && grille.getCase(i + j + 1, 0) != null) {
				test = test && comparaisonColonnes(grille, i - j, i + j + 1);
			}

		}
		if (test) {
			return i;
		}
		return -1;
	}

	private boolean comparaisonLignes(Grille grille, int i, int j) {
		boolean lignesSontIdentiques = true;
		for (int k = 0; k < grille.getNbCol(); k++) {
			lignesSontIdentiques = lignesSontIdentiques && grille.getCase(k, i).getEtat().equals(grille.getCase(k, j).getEtat());
		}

		return lignesSontIdentiques;
	}

	private boolean comparaisonColonnes(Grille grille, int i, int j) {
		boolean colonnesSontIdentiques = true;
		for (int k = 0; k < grille.getNbLignes(); k++) {
			colonnesSontIdentiques = colonnesSontIdentiques && grille.getCase(i, k).getEtat().equals(grille.getCase(j, k).getEtat());
		}

		return colonnesSontIdentiques;
	}

	public void initGrilles(List<String> fichier, List<Grille> grilles, List<String> pattern) {
		for (String ligne : fichier) {

			if (!StringUtils.isBlank(ligne)) {
				pattern.add(ligne);
			} else {
				Grille grille = new Grille(pattern.get(0).length(), pattern.size());
				for (int i = 0; i < pattern.size(); i++) {
					for (int j = 0; j < pattern.get(i).length(); j++) {
						grille.getCase(j, i).setEtat("" + pattern.get(i).charAt(j));
					}
				}
				// grille.affichageEtat();
				grilles.add(grille);
				pattern.clear();
				// System.out.println(" ");
			}

		}
	}

}