package advent2023.day17;

import communs.Case;

public record Node(Case caseActuelle, int nbPasDansDirection, String direction) {

}