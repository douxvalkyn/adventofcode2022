package advent2023.day17;

public class Element2 implements Comparable<Element2> {

	private int score;
	private String direction;
	private String chemin = "";

	private Node node;

	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	public Element2(Node node, int score) {
		this.node = node;
		this.score = score;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	@Override
	public int compareTo(Element2 o) {
		if (this.score != o.getScore()) {
			return Integer.compare(this.score, o.getScore());
		} else if (this.node.direction().equals(o.getNode().direction()) && this.node.nbPasDansDirection() != o.getNode().nbPasDansDirection()) {
			return Integer.compare(this.node.nbPasDansDirection(), o.getNode().nbPasDansDirection());
		} else if (this.node.caseActuelle().getY() != o.getNode().caseActuelle().getY()) {
			return Integer.compare(this.node.caseActuelle().getY(), o.getNode().caseActuelle().getY());
		} else {
			return Integer.compare(this.node.caseActuelle().getX(), o.getNode().caseActuelle().getX());
		}
	}

	public Node getNode() {
		return this.node;
	}

	public int getScore() {
		return this.score;
	}
}