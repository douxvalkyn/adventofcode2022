package advent2023.day17;

import java.util.Objects;

import communs.Case;

public class State  implements Comparable<State>{

	private int nbPasDansDirection;
	private int score;
	private Case caseActuelle;
	private String direction;
//	 private String chemin;
	
//	 public String getChemin() {
//	 return chemin;
//	 }
//	
//	 public void setChemin(String chemin) {
//	 this.chemin = chemin;
//	 }

	public Case getCaseActuelle() {
		return caseActuelle;
	}

	public void setCaseActuelle(Case caseActuelle) {
		this.caseActuelle = caseActuelle;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getNbPasDansDirection() {
		return nbPasDansDirection;
	}

	public void setNbPasDansDirection(int nbPasDansDirection) {
		this.nbPasDansDirection = nbPasDansDirection;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public State() {
		super();
	}

	@Override
	public String toString() {
		return "State [nbPasDansDirection=" + nbPasDansDirection + ", score=" + score + ", caseActuelle=" + caseActuelle + ", direction=" + direction + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(caseActuelle, direction, nbPasDansDirection, score);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return Objects.equals(caseActuelle, other.caseActuelle) && Objects.equals(direction, other.direction) && nbPasDansDirection == other.nbPasDansDirection ;
	}

	@Override
	public int compareTo(State o) {
		if (this.getDirection().equals(o.getDirection()) && this.getNbPasDansDirection()!= o.getNbPasDansDirection()) {
	            return Integer.compare(this.getNbPasDansDirection(), o.getNbPasDansDirection());
	        } else if (this.getCaseActuelle().getY() != o.getCaseActuelle().getY()) {
	            return Integer.compare(this.getCaseActuelle().getY(), o.getCaseActuelle().getY());
	        } else {
	            return Integer.compare(this.getCaseActuelle().getX(), o.getCaseActuelle().getX());
	        }
	}

}
