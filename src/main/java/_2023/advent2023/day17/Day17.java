package advent2023.day17;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day17 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day17.class);
	private static int min = 1000;
	private static boolean show1 = true;
	private static boolean show2 = true;
	private static boolean show3 = true;
	private static boolean show4 = true;
	private static boolean show5 = true;
	private static boolean show6 = true;
	private static boolean show7 = true;
	private static boolean show8 = true;
	private static boolean show9 = true;
	private static boolean show10 = true;
	private static boolean show11 = true;
	private static boolean show12 = true;
	private static boolean show13 = true;
	private static boolean show14 = true;
	private static List<Integer> liste = new ArrayList<Integer>();
	private int tailleVisited = 0;

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day17.class.getSimpleName());
		Day17 day = new Day17();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day17.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		bfs3(grille.getCase(0, 0), grille);
		System.err.println(Outil.min(liste));

	}

	private int bfs3(Case caseActuelle, Grille grille) {

		Queue<Element2> queue = new PriorityQueue<>();
		Set<Node> visited = new HashSet<>();

		Node node1 = new Node(caseActuelle, 1, "S");
		Node node2 = new Node(caseActuelle, 1, "E");
		Element2 stateInitial1 = new Element2(node1, 0);
		Element2 stateInitial2 = new Element2(node2, 0);

		// La queue contiendra tous les noeuds(=les states) à visiter
		queue.add(stateInitial1);
		queue.add(stateInitial2);

		while (!queue.isEmpty()) { // on continue tant que la queue contient encore des noeuds à visiter (mais on peut spécifier un arrêt apres si on a trouvé un noeud cherché)

			Element2 stateActuel = queue.poll();
			stateActuel.setDirection(stateActuel.getNode().direction());
			if (visited.contains(stateActuel.getNode())) {
				continue;
			}
			visited.add(stateActuel.getNode());

			if (stateActuel.getNode().nbPasDansDirection() >= 4 && stateActuel.getNode().caseActuelle().getX() == (grille.getNbCol() - 1)
				&& stateActuel.getNode().caseActuelle().getY() == (grille.getNbLignes() - 1)) { // condition
				System.out.println(stateActuel.getScore());
				if (stateActuel.getScore() < min) {
					min = stateActuel.getScore();
					liste.add(min);
				}
			} else {
				List<Case> casesAdjacentes = stateActuel.getNode().caseActuelle().getCasesAdjacentes2(grille);

				if (stateActuel.getNode().nbPasDansDirection() < 10) { // on peut aller dans la même direction
					Case caseMemeDirection = renvoieCaseAdjacenteSelonDirection(stateActuel.getDirection(), casesAdjacentes);
					if (caseMemeDirection != null) {
						Node node = new Node(caseMemeDirection, stateActuel.getNode().nbPasDansDirection() + 1, stateActuel.getDirection());
						Element2 nextState = new Element2(node, stateActuel.getScore() + caseMemeDirection.getValeur());

						if (!visited.contains(nextState)) {
							nextState.setChemin(stateActuel.getChemin() + nextState.getNode().caseActuelle().getValeur());
							queue.add(nextState);
						}
					}
				}
				// si 4 blocs ou plus, on peut aller tout droit, à gauche, à droite
				if (stateActuel.getNode().nbPasDansDirection() >= 4) {
					if (stateActuel.getDirection().equals("N") || stateActuel.getDirection().equals("S")) {// on peut donc aller E, W

						// E
						if (casesAdjacentes.get(2) != null) {
							Node node = new Node(casesAdjacentes.get(2), 1, "E");
							Element2 nextState = new Element2(node, stateActuel.getScore() + casesAdjacentes.get(2).getValeur());
							if (!visited.contains(nextState)) {
								nextState.setChemin(stateActuel.getChemin() + nextState.getNode().caseActuelle().getValeur());
								queue.add(nextState);
							}
						}
						// W
						if (casesAdjacentes.get(3) != null) {
							Node node = new Node(casesAdjacentes.get(3), 1, "W");
							Element2 nextState = new Element2(node, stateActuel.getScore() + casesAdjacentes.get(3).getValeur());
							if (!visited.contains(nextState)) {
								nextState.setChemin(stateActuel.getChemin() + nextState.getNode().caseActuelle().getValeur());
								queue.add(nextState);
							}
						}
					}
					if (stateActuel.getDirection().equals("E") || stateActuel.getDirection().equals("W")) {// on peut donc aller N, S

						// N
						if (casesAdjacentes.get(0) != null) {
							Node node = new Node(casesAdjacentes.get(0), 1, "N");
							Element2 nextState = new Element2(node, stateActuel.getScore() + casesAdjacentes.get(0).getValeur());
							if (!visited.contains(nextState)) {
								nextState.setChemin(stateActuel.getChemin() + nextState.getNode().caseActuelle().getValeur());
								queue.add(nextState);
							}
						}
						// S
						if (casesAdjacentes.get(1) != null) {
							Node node = new Node(casesAdjacentes.get(1), 1, "S");
							Element2 nextState = new Element2(node, stateActuel.getScore() + casesAdjacentes.get(1).getValeur());
							if (!visited.contains(nextState)) {
								nextState.setChemin(stateActuel.getChemin() + nextState.getNode().caseActuelle().getValeur());
								queue.add(nextState);
							}
						}
					}

				}

			}
		}
		return 0;

	}

	private int bfs2(Case caseActuelle, Grille grille) {
		// on crée 2 etats initiaux (direction S et E)
		State stateInitial1 = new State();
//		stateInitial1.setNbPasDansDirection(1);
//		stateInitial1.setScore(0);
//		stateInitial1.setCaseActuelle(caseActuelle);
//		stateInitial1.setDirection("S");
		State stateInitial2 = new State();
		stateInitial2.setNbPasDansDirection(1);
		stateInitial2.setScore(0);
		stateInitial2.setCaseActuelle(caseActuelle);
		stateInitial2.setDirection("E");

		// La queue contiendra tous les noeuds(=les states) à visiter
		Queue<State> queue = new PriorityQueue<State>();
		//queue.add(stateInitial1);
		queue.add(stateInitial2);

		// on ne visitera que les noeuds nouveaux afin de ne pas tourner en rond, donc on a besoin de sauvegarder l'ensemble des noeuds deja visités
		Set<State> visitedStates = new HashSet<>();
		//visitedStates.add(stateInitial1);
		visitedStates.add(stateInitial2);

		while (!queue.isEmpty()) { // on continue tant que la queue contient encore des noeuds à visiter (mais on peut spécifier un arrêt apres si on a trouvé un noeud cherché)

			State stateActuel = queue.poll();// on recupere le noeud à étudier
			tailleVisited = visitedStates.size();
			show(stateActuel);

			if (stateActuel.getCaseActuelle().getX() == (grille.getNbCol() - 1) && stateActuel.getCaseActuelle().getY() == (grille.getNbLignes() - 1) && stateActuel.getNbPasDansDirection() >= 4) { // condition
																																																		// d'arrêt
																																																		// si
																																																		// on
																																																		// cherche
																																																		// un
																																																		// noeud
				System.out.println(stateActuel.getScore());
				if (stateActuel.getScore() < min) {
					min = stateActuel.getScore();
					liste.add(min);
				}
			} else {
				List<Case> casesAdjacentes = stateActuel.getCaseActuelle().getCasesAdjacentes2(grille);

				if (stateActuel.getNbPasDansDirection() < 10) { // on peut aller dans la même direction
					Case caseMemeDirection = renvoieCaseAdjacenteSelonDirection(stateActuel.getDirection(), casesAdjacentes);
					if (caseMemeDirection != null) {
						State nextState = new State();
						nextState.setCaseActuelle(caseMemeDirection);
						nextState.setDirection(stateActuel.getDirection());
						nextState.setNbPasDansDirection(stateActuel.getNbPasDansDirection() + 1);
						nextState.setScore(stateActuel.getScore() + caseMemeDirection.getValeur());

						if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}
				}
				// si 4 blocs ou plus, on peut aller tout droit, à gauche, à droite
				if (stateActuel.getNbPasDansDirection() >= 4) {
					if (stateActuel.getDirection().equals("N") || stateActuel.getDirection().equals("S")) {// on peut donc aller E, W

						// E
						if (casesAdjacentes.get(2) != null) {
							State nextState = new State();
							nextState.setCaseActuelle(casesAdjacentes.get(2));
							nextState.setDirection("E");
							nextState.setNbPasDansDirection(1);
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(2).getValeur());
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
						// W
						if (casesAdjacentes.get(3) != null) {
							State nextState = new State();
							nextState.setCaseActuelle(casesAdjacentes.get(3));
							nextState.setDirection("W");
							nextState.setNbPasDansDirection(1);
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(3).getValeur());
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}
					if (stateActuel.getDirection().equals("E") || stateActuel.getDirection().equals("W")) {// on peut donc aller N, S

						// N
						if (casesAdjacentes.get(0) != null) {
							State nextState = new State();
							nextState.setCaseActuelle(casesAdjacentes.get(0));
							nextState.setDirection("N");
							nextState.setNbPasDansDirection(1);
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(0).getValeur());
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
						// S
						if (casesAdjacentes.get(1) != null) {
							State nextState = new State();
							nextState.setCaseActuelle(casesAdjacentes.get(1));
							nextState.setDirection("S");
							nextState.setNbPasDansDirection(1);
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(1).getValeur());
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}

				}

			}
		}
		return 0;

	}

	private Case renvoieCaseAdjacenteSelonDirection(String direction, List<Case> casesAdjacentes) {
		Case caseAdjacente = null;
		switch (direction) {
			case "N":
				caseAdjacente = casesAdjacentes.get(0);
				break;
			case "S":
				caseAdjacente = casesAdjacentes.get(1);
				break;
			case "E":
				caseAdjacente = casesAdjacentes.get(2);
				break;
			case "W":
				caseAdjacente = casesAdjacentes.get(3);
				break;
			default:
				break;
		}
		return caseAdjacente;
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day17.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		bfs(grille.getCase(0, 0), grille, "S");
		System.err.println(Outil.min(liste));

	}

	private static void initGrille(List<String> fichier, Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			for (int j = 0; j < grille.getNbLignes(); j++) {
				grille.getCase(i, j).setValeur(Integer.parseInt("" + fichier.get(j).charAt(i)));
			}
		}
		grille.affichageValeur();
	}

	private int bfs(Case caseActuelle, Grille grille, String direction) {
		// on crée un etat initial
		State stateInitial = new State();
		stateInitial.setNbPasDansDirection(0);
		stateInitial.setScore(0);
		stateInitial.setCaseActuelle(caseActuelle);
		stateInitial.setDirection(direction);
		// stateInitial.setChemin("");

		// La queue contiendra tous les noeuds(=les states) à visiter
		Queue<State> queue = new PriorityQueue<State>();

		queue.add(stateInitial);

		// on ne visitera que les noeuds nouveaux afin de ne pas tourner en rond, donc on a besoin de sauvegarder l'ensemble des noeuds deja visités
		Set<State> visitedStates = new HashSet<>();
		visitedStates.add(stateInitial);

		while (!queue.isEmpty()) { // on continue tant que la queue contient encore des noeuds à visiter (mais on peut spécifier un arrêt apres si on a trouvé un noeud cherché)

			State stateActuel = queue.poll();// on recupere le noeud à étudier

			show(stateActuel);

			if (stateActuel.getCaseActuelle().getX() == (grille.getNbCol() - 1) && stateActuel.getCaseActuelle().getY() == (grille.getNbLignes() - 1)) { // condition d'arrêt si on cherche un noeud

				System.out.println(stateActuel.getScore());
				if (stateActuel.getScore() < min) {
					min = stateActuel.getScore();
					liste.add(min);
				}
			} else {

				List<Case> casesAdjacentes = stateActuel.getCaseActuelle().getCasesAdjacentes2(grille);
				if (stateActuel.getNbPasDansDirection() < 3) {
					State nextState = new State();
					// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());

					// N ok
					if (casesAdjacentes.get(0) != null && !stateActuel.getDirection().equals("S")) {
						nextState = new State();
						// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
						nextState.setCaseActuelle(casesAdjacentes.get(0));
						nextState.setDirection("N");
						nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(0).getValeur());
						if (stateActuel.getDirection().equals("N")) {
							nextState.setNbPasDansDirection(stateActuel.getNbPasDansDirection() + 1);
						} else {
							nextState.setNbPasDansDirection(1);
						}
						// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
						if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// S ok
					if (casesAdjacentes.get(1) != null && !stateActuel.getDirection().equals("N")) {
						nextState = new State();
						// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
						nextState.setCaseActuelle(casesAdjacentes.get(1));
						nextState.setDirection("S");
						nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(1).getValeur());
						if (stateActuel.getDirection().equals("S")) {
							nextState.setNbPasDansDirection(stateActuel.getNbPasDansDirection() + 1);
						} else {
							nextState.setNbPasDansDirection(1);
						}
						// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
						if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// E ok
					if (casesAdjacentes.get(2) != null && !stateActuel.getDirection().equals("W")) {
						nextState = new State();
						// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
						nextState.setCaseActuelle(casesAdjacentes.get(2));
						nextState.setDirection("E");
						nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(2).getValeur());
						if (stateActuel.getDirection().equals("E")) {
							nextState.setNbPasDansDirection(stateActuel.getNbPasDansDirection() + 1);
						} else {
							nextState.setNbPasDansDirection(1);
						}
						// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
						if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

					// W ok
					if (casesAdjacentes.get(3) != null && !stateActuel.getDirection().equals("E")) {
						nextState = new State();
						// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
						nextState.setCaseActuelle(casesAdjacentes.get(3));
						nextState.setDirection("W");
						nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(3).getValeur());
						if (stateActuel.getDirection().equals("W")) {
							nextState.setNbPasDansDirection(stateActuel.getNbPasDansDirection() + 1);
						} else {
							nextState.setNbPasDansDirection(1);
						}
						// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
						if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
							queue.add(nextState);
							visitedStates.add(nextState);
						}
					}

				} else {
					// impossible d'aller dans la direction en cours, impossible de retourner en arriere, les autres directions sont possibles
					State nextState = new State();
					// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
					if (stateActuel.getDirection().equals("N")) {

						// E ok
						if (casesAdjacentes.get(2) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(2));
							nextState.setDirection("E");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(2).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}

						// W ok
						if (casesAdjacentes.get(3) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(3));
							nextState.setDirection("W");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(3).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}
					if (stateActuel.getDirection().equals("S")) {
						// E ok
						if (casesAdjacentes.get(2) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(2));
							nextState.setDirection("E");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(2).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}

						// W ok
						if (casesAdjacentes.get(3) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(3));
							nextState.setDirection("W");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(3).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}
					if (stateActuel.getDirection().equals("E")) {
						// N ok
						if (casesAdjacentes.get(0) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(0));
							nextState.setDirection("N");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(0).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}

						// S ok
						if (casesAdjacentes.get(1) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(1));
							nextState.setDirection("S");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(1).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}
					if (stateActuel.getDirection().equals("W")) {
						// N ok
						if (casesAdjacentes.get(0) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(0));
							nextState.setDirection("N");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(0).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}

						// S ok
						if (casesAdjacentes.get(1) != null) {
							nextState = new State();
							// nextState.setChemin(stateActuel.getChemin() + stateActuel.getCaseActuelle().getValeur());
							nextState.setCaseActuelle(casesAdjacentes.get(1));
							nextState.setDirection("S");
							nextState.setScore(stateActuel.getScore() + casesAdjacentes.get(1).getValeur());
							nextState.setNbPasDansDirection(1);
							// pour un cas donné, on l'ajoute à la queue si c'est un nouveau noeud
							if (!visitedStates.contains(nextState) && nextState.getScore() < min && estPlausible(nextState)) {
								queue.add(nextState);
								visitedStates.add(nextState);
							}
						}
					}

				}

			}
		}
		return 0;

	}

	public boolean estPlausible(State state) {

		if (state.getCaseActuelle().getX() <= 10 && state.getScore() < 300) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 10 && state.getCaseActuelle().getX() < 20 && state.getScore() < 350) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 20 && state.getCaseActuelle().getX() < 30 && state.getScore() < 400) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 30 && state.getCaseActuelle().getX() < 40 && state.getScore() < 450) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 40 && state.getCaseActuelle().getX() < 50 && state.getScore() < 500) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 50 && state.getCaseActuelle().getX() < 60 && state.getScore() < 550) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 60 && state.getCaseActuelle().getX() < 70 && state.getScore() < 600) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 70 && state.getCaseActuelle().getX() < 80 && state.getScore() < 650) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 80 && state.getCaseActuelle().getX() < 90 && state.getScore() < 700) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 90 && state.getCaseActuelle().getX() < 100 && state.getScore() < 750) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 100 && state.getCaseActuelle().getX() < 110 && state.getScore() < 800) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 110 && state.getCaseActuelle().getX() < 120 && state.getScore() < 850) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 120 && state.getCaseActuelle().getX() < 130 && state.getScore() < 900) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 130 && state.getCaseActuelle().getX() < 140 && state.getScore() < 950) {
			return true;
		}
		if (state.getCaseActuelle().getX() >= 140 && state.getCaseActuelle().getX() < 150 && state.getScore() < 1000) {
			return true;
		}

		return true;

	}

	public void show(State stateActuel) {
		if (stateActuel.getCaseActuelle().getX() > 10 && show1) {
			System.err.println(">10 " + tailleVisited);
			show1 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 20 && show2) {
			System.err.println(">20 " + tailleVisited);
			show2 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 30 && show3) {
			System.err.println(">30 " + tailleVisited);
			show3 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 40 && show4) {
			System.err.println(">40 " + tailleVisited);
			show4 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 50 && show5) {
			System.err.println(">50 " + tailleVisited);
			show5 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 60 && show6) {
			System.err.println(">60 " + tailleVisited);
			show6 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 70 && show7) {
			System.err.println(">70 " + tailleVisited);
			show7 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 80 && show8) {
			System.err.println(">80 " + tailleVisited);
			show8 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 90 && show9) {
			System.err.println(">90 " + tailleVisited);
			show9 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 100 && show10) {
			System.err.println(">100 " + tailleVisited);
			show10 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 110 && show11) {
			System.err.println(">110 " + tailleVisited);
			show11 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 120 && show12) {
			System.err.println(">120 " + tailleVisited);
			show12 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 130 && show13) {
			System.err.println(">130 " + tailleVisited);
			show13 = false;
		}
		if (stateActuel.getCaseActuelle().getX() > 140 && show14) {
			System.err.println(">140 " + tailleVisited);
			show14 = false;
		}

	}
}