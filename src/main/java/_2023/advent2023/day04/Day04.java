package advent2023.day04;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day04 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day04.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day04.class.getSimpleName());
		Day04 day = new Day04();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day04.txt";
		List<String> res = Outil.importationString(file);
		Map<Integer, Integer> jeux = new HashMap<>();
		for (int i = 1; i <= res.size(); i++) {
			jeux.put(i, 1);
		}

		for (String ligne : res) {
			int numGame = Integer.parseInt(StringUtils.split(StringUtils.split(ligne, ':')[0], ' ')[1]);
			String cartes = StringUtils.split(ligne, ':')[1];
			String[] winningCards = StringUtils.split(StringUtils.split(cartes, '|')[0], ' ');
			String[] myCards = StringUtils.split(StringUtils.split(cartes, '|')[1], ' ');

			List<Integer> maListeWinningCards = new ArrayList<>();
			for (int index = 0; index < winningCards.length; index++) {
				maListeWinningCards.add(Integer.parseInt(winningCards[index]));
			}
			List<Integer> maListeMyCards = new ArrayList<>();
			for (int index = 0; index < myCards.length; index++) {
				maListeMyCards.add(Integer.parseInt(myCards[index]));
			}

			int nbCartesGagnantes = 0;
			for (int i = 0; i < maListeMyCards.size(); i++) {
				int myCard = maListeMyCards.get(i);
				if (maListeWinningCards.contains(myCard)) {
					nbCartesGagnantes++;
				}
			}
			for (int j = 1; j <= nbCartesGagnantes; j++) {
				jeux.put(numGame + j, jeux.get(numGame + j) + 1 * jeux.get(numGame));
			}
		}
		// calcul nb cartes au total
		int sum = jeux.values().stream().reduce(0, Integer::sum);
		System.out.println("Run2: " + sum);

	}

	public void run1() {
		String file = "src/main/resources/advent2023/day04.txt";
		List<String> res = Outil.importationString(file);
		int points = 0;
		for (String ligne : res) {
			String cartes = StringUtils.split(ligne, ':')[1];
			String[] winningCards = StringUtils.split(StringUtils.split(cartes, '|')[0], ' ');
			String[] myCards = StringUtils.split(StringUtils.split(cartes, '|')[1], ' ');

			List<Integer> maListeWinningCards = new ArrayList<>();
			for (int index = 0; index < winningCards.length; index++) {
				maListeWinningCards.add(Integer.parseInt(winningCards[index]));
			}
			List<Integer> maListeMyCards = new ArrayList<>();
			for (int index = 0; index < myCards.length; index++) {
				maListeMyCards.add(Integer.parseInt(myCards[index]));
			}

			int pointsCarte = 0;
			for (int i = 0; i < maListeMyCards.size(); i++) {
				int myCard = maListeMyCards.get(i);
				if (maListeWinningCards.contains(myCard)) {
					if (pointsCarte == 0) {
						pointsCarte = 1;
					} else {
						pointsCarte = pointsCarte * 2;
					}

				}
			}
			points = points + pointsCarte;
		}
		System.out.println("Run1: " + points);

	}

}
