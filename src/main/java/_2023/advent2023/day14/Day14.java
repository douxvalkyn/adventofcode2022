package advent2023.day14;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day14 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day14.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day14.class.getSimpleName());
		Day14 day = new Day14();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run1() {
		String file = "src/main/resources/advent2023/day14.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		tilt(grille, "N");
		calculLoad(grille);
	}

	private void calculLoad(Grille grille) {
		List<Case> cases = grille.getCases();
		int load = 0;
		for (Case cell : cases) {
			if ("O".equals(cell.getEtat())) {
				int scoreCell = grille.getNbLignes() - cell.getY();
				load = load + scoreCell;
			}
		}

		System.err.println(load);

	}

	private void tilt(Grille grille, String tilt) {
		if (("N").equals(tilt)) {
			for (int i = 0; i < grille.getNbCol(); i++) {
				for (int j = 0; j < grille.getNbLignes(); j++) {
					Case cell = grille.getCase(i, j);
					if (cell.getEtat().equals("O")) {
						// on la d�place au max vers la direction N
						Case cellLaPlusAuNord = cell;
						boolean stop = false;
						while (!stop) {
							if (cellLaPlusAuNord.getCasesAdjacentes2(grille).get(0) != null && cellLaPlusAuNord.getCasesAdjacentes2(grille).get(0).getEtat().equals(".")) {
								// deplacement vers le N possible
								cellLaPlusAuNord = cellLaPlusAuNord.getCasesAdjacentes2(grille).get(0);
							} else {
								stop = true;
							}
						}
						if (cell != cellLaPlusAuNord) {
							cell.setEtat(".");
							cellLaPlusAuNord.setEtat("O");
							// grille.affichageEtat();
						}

					}

				}
			}

		}
	}

	private static void initGrille(List<String> fichier, Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			for (int j = 0; j < grille.getNbLignes(); j++) {
				grille.getCase(i, j).setEtat("" + fichier.get(j).charAt(i));
			}
		}
		grille.affichageEtat();
	}

}