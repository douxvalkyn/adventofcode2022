package advent2023.day05;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;

import communs.Outil;

public class Day05 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day05.class);

	public static void main(String[] args) {
		LOGGER.info("[{}]", Day05.class.getSimpleName());
		Day05 day = new Day05();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day05.txt";
		List<List<Long>> seeds = initSeeds(file);
		List<List<List<Long>>> transformations = init(file);

		List<Range<Long>> numberList = Arrays.asList(Range.closed(seeds.get(0).get(0), seeds.get(0).get(1)));
		RangeSet<Long> numberRangeSet = TreeRangeSet.create(numberList);
		for (int j = 1; j < seeds.size(); j++) {
			// calcul de l'union des intervalles
			List<Long> seed = seeds.get(j);
			List<Range<Long>> numberList2 = Arrays.asList(Range.closed(seed.get(0), seed.get(1)));
			RangeSet<Long> numberRangeSet2 = TreeRangeSet.create(numberList2);
			numberRangeSet.addAll(numberRangeSet2);
		}

		long minimum = 100000000000000L;
		Set<Range<Long>> ranges = numberRangeSet.asRanges();
		for (Range<Long> range : ranges) {
			System.out.println("-----");
			System.out.println(LocalDateTime.now());
			System.out.println(range);
			Long min = range.lowerEndpoint();
			Long max = range.upperEndpoint();
			System.out.print("range: ");
			System.out.println(max - min);

			for (long i = min; i < max; i++) {
				Long res = transfo2(transformations.get(0), i);
				res = transfo2(transformations.get(1), res);
				res = transfo2(transformations.get(2), res);
				res = transfo2(transformations.get(3), res);
				res = transfo2(transformations.get(4), res);
				res = transfo2(transformations.get(5), res);
				res = transfo2(transformations.get(6), res);
				if (res < minimum) {
					minimum = res;
					System.out.println(minimum);
				}
			}
		}

	}

	public void run1() {
		String file = "src/main/resources/advent2023/day05a.txt";
		String[] seeds = StringUtils.split(StringUtils.split(Outil.importationString(file).get(0), ':')[1], ' ');
		List<List<List<Long>>> transformations = init(file);

		seeds = transfo(transformations.get(0), seeds);
		seeds = transfo(transformations.get(1), seeds);
		seeds = transfo(transformations.get(2), seeds);
		seeds = transfo(transformations.get(3), seeds);
		seeds = transfo(transformations.get(4), seeds);
		seeds = transfo(transformations.get(5), seeds);
		seeds = transfo(transformations.get(6), seeds);

		Long min = recherchePlusPetit(seeds);
		System.out.println("Run1: " + min);
	}

	private List<List<Long>> initSeeds(String file) {
		String[] seeds = StringUtils.split(StringUtils.split(Outil.importationString(file).get(0), ':')[1], ' ');
		// on memorise le min et max de chaque seed
		List<List<Long>> listes = new ArrayList<>();
		for (int i = 0; i < seeds.length; i++) {
			List<Long> liste = new ArrayList<>();
			if (i % 2 == 0) {
				long a = Long.parseLong(seeds[i]);
				long length = Long.parseLong(seeds[i + 1]);
				long b = a + length - 1;
				liste.add(a);
				liste.add(b);
				listes.add(liste);
			}

		}

		return listes;
	}

	private Long recherchePlusPetit(String[] seeds) {
		Long min = 1000000000000L;

		for (String seed : seeds) {
			long seedInt = Long.parseLong(seed);
			if (seedInt < min) {
				min = seedInt;
			}
		}
		return min;
	}

	private static Long transfo2(List<List<Long>> list, long nombre) {
		Long seedsOut = 0L;
		for (int j = 0; j < list.size(); j++) {
			long in = nombre;
			long out = in;
			for (List<Long> subList : list) {
				if (subList.get(1) <= in && in < subList.get(1) + subList.get(2)) {
					out = in + (subList.get(0) - subList.get(1));
				}
			}
			seedsOut = out;
		}
		return seedsOut;
	}

	private String[] transfo(List<List<Long>> list, String[] seeds) {
		String[] seedsOut = seeds;
		int i = 0;
		for (String seed : seeds) {

			long in = Long.parseLong(seed);
			long out = in;
			for (List<Long> subList : list) {
				Long destination = subList.get(0);
				Long source = subList.get(1);
				Long length = subList.get(2);
				if (source <= in && in < source + length) {
					out = in + (destination - source);
				}
			}
			seedsOut[i] = "" + out;
			i++;
		}
		return seedsOut;
	}

	private List<List<List<Long>>> init(String file) {
		List<String> res = Outil.importationString(file);
		System.out.println(res);

		// autres lignes: transformations
		List<List<List<Long>>> transformations = new ArrayList<>();
		for (int i = 1; i < res.size(); i++) {
			if ("seed-to-soil map:".equals(res.get(i)) || "soil-to-fertilizer map:".equals(res.get(i)) || "fertilizer-to-water map:".equals(res.get(i)) || "water-to-light map:".equals(res.get(i))
				|| "light-to-temperature map:".equals(res.get(i)) || "temperature-to-humidity map:".equals(res.get(i)) || "humidity-to-location map:".equals(res.get(i))) {
				List<List<Long>> listeUneTransfo = fabricationListeUneTransfo(res, i);
				transformations.add(listeUneTransfo);
			}
		}

		return transformations;
	}

	private static List<List<Long>> fabricationListeUneTransfo(List<String> res, int i) {
		List<List<Long>> transformation1 = new ArrayList<>();

		int j = i + 1;
		while (j < res.size() && !"".equals(res.get(j))) {
			List<Long> liste1 = new ArrayList<>();
			String[] nombres = StringUtils.split(res.get(j), ' ');
			for (String nombre : nombres) {
				liste1.add(Long.parseLong(nombre));
			}
			transformation1.add(liste1);
			j++;
		}
		return transformation1;
	}

}
