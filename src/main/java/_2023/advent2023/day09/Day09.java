package advent2023.day09;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day09 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day09.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day09.class.getSimpleName());
		Day09 day = new Day09();
		LocalDateTime start = LocalDateTime.now();
		day.run1();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	
	
	public void run2() throws IOException {
		String file = "src/main/resources/advent2023/day09.txt";
		List<String> fichier = Outil.importationString(file);
		int sommeDesEstimations=0;
		for (String ligne:fichier) {
			String[] res = StringUtils.split(ligne, " ");
			List<String> history = Arrays.asList(res);
			List<Integer> historyNombres = new ArrayList<>();
			for (String string:history) {
				historyNombres.add(Integer.parseInt(string));
			}
			
			int sommeDesPremiersNombres=historyNombres.get(0);
			//calcul des différences
			List<Integer> diff = historyNombres;
			int nbEtapes=0;
			while ( !verifyAllEqual(diff)) {
				diff = differences(diff);
				if (nbEtapes%2==0) {
				sommeDesPremiersNombres=sommeDesPremiersNombres-diff.get(0);
				}else {
				sommeDesPremiersNombres=sommeDesPremiersNombres+diff.get(0);
				}
				nbEtapes++;
			}
			sommeDesEstimations=sommeDesEstimations+sommeDesPremiersNombres;
		}
	
		System.out.println("Run2: "+sommeDesEstimations);
	}
	
	
	public void run1() throws IOException {
		String file = "src/main/resources/advent2023/day09.txt";
		List<String> fichier = Outil.importationString(file);
		int sommeDesEstimations=0;
		for (String ligne:fichier) {
			String[] res = StringUtils.split(ligne, " ");
			List<String> history = Arrays.asList(res);
			List<Integer> historyNombres = new ArrayList<>();
			for (String string:history) {
				historyNombres.add(Integer.parseInt(string));
			}
			
			int sommeDesDerniersNombres=historyNombres.get(historyNombres.size()-1);
			//calcul des différences
			List<Integer> diff = historyNombres;
			while ( !verifyAllEqual(diff)) {
				diff = differences(diff);
				sommeDesDerniersNombres=sommeDesDerniersNombres+diff.get(diff.size()-1);
			}
			//System.out.println(sommeDesDerniersNombres);
			sommeDesEstimations=sommeDesEstimations+sommeDesDerniersNombres;
		}
	
		System.out.println("Run1: "+sommeDesEstimations);
	}

	
	public boolean verifyAllEqual(List<Integer> list) {
	    return list.stream()
	      .distinct()
	      .count() <= 1;
	}
	
	private List<Integer> differences(List<Integer> history) {
		List<Integer> differences = new ArrayList<>();
		for (int i=0;i<history.size()-1;i++) {
			int diff = history.get(i+1) -history.get(i);
			differences.add(diff);
		}
		
		
		return differences;
	}

}
