package advent2023.day12;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Outil;

public class Day12 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day12.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day12.class.getSimpleName());
		Day12 day = new Day12();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}

	public void run2() {
		String file = "src/main/resources/advent2023/day12a.txt";
		List<String> fichier = Outil.importationString(file);

		int somme = 0;
		int cpt = 0;
		for (String ligne : fichier) {

			// init
			cpt++;
			System.out.println("___ " + ligne + " _ligne numero:_ " + cpt);
			String springs = StringUtils.split(ligne, " ")[0];
			String[] sizes2 = StringUtils.split(StringUtils.split(ligne, " ")[1], ",");

			// run2: X5
			springs = springs + "?" + springs + "?" + springs + "?" + springs + "?" + springs;

			List<String> listOfSizes2 = Arrays.asList(sizes2);
			List<String> listOfSizes = new ArrayList<>();

			listOfSizes.addAll(listOfSizes2);
			listOfSizes.addAll(listOfSizes2);
			listOfSizes.addAll(listOfSizes2);
			listOfSizes.addAll(listOfSizes2);
			listOfSizes.addAll(listOfSizes2);

			// exec
			int sum = rechercheNombreArrangements(springs, listOfSizes);

			// fin
			somme = somme + sum;
			System.out.println("nb arrangements: " + sum);
		}
		System.out.println(somme);

	}

	public void run1() {
		String file = "src/main/resources/advent2023/day12.txt";
		List<String> fichier = Outil.importationString(file);
		int somme = 0;
		int cpt = 0;
		for (String ligne : fichier) {

			// System.out.println(ligne);
			String springs = StringUtils.split(ligne, " ")[0];
			String[] sizes2 = StringUtils.split(StringUtils.split(ligne, " ")[1], ",");
			List<String> listOfSizes = Arrays.asList(sizes2);
			int sum = rechercheNombreArrangements(springs, listOfSizes);
			somme = somme + sum;
			// System.out.println(sum);
		}
		System.out.print(cpt + " : ");
		System.out.println(somme);

	}

	private int rechercheNombreArrangements(String springs, List<String> listOfSizes) {

		List<String> possibilites = new ArrayList<>();
		String regex = "\\?";
		Pattern pattern = Pattern.compile(regex);
		possibilites.add(springs);

		// liste de toutes les possibilites
		while (StringUtils.contains(possibilites.get(0), "?")) {

			Matcher matcher = pattern.matcher(possibilites.get(0));

			String aaa = matcher.replaceFirst(".");
			String bbb = matcher.replaceFirst("#");

			possibilites.remove(0);

			if (plausible(aaa, listOfSizes)) {
				possibilites.add(aaa);
			}
			if (plausible(bbb, listOfSizes)) {
				possibilites.add(bbb);
			}

		}

		System.out.println("nb possibilites: " + possibilites.size());
		// supprimer doublons
		Set<String> elements = new HashSet<String>();
		possibilites = possibilites.stream().filter(n -> elements.add(n)).collect(Collectors.toList());

		// listes autorisees
		int sum = 0;
		for (String possibilite : possibilites) {
			String[] sequences = StringUtils.split(possibilite, '.');

			boolean test = true;
			if (sequences.length == listOfSizes.size()) {
				for (int i = 0; i < sequences.length; i++) {
					test = test && sequences[i].length() == Integer.parseInt(listOfSizes.get(i));
				}
				if (test) {
					sum++;
					// System.out.println(possibilite);
				}
			}
		}

		return sum;

	}

	private boolean plausible(String possibilite, List<String> listOfSizes) {
		boolean plausible = true;
		String[] sequences = StringUtils.split(possibilite, '.');
		// on observe les premieres sequences termin�es, ie sans ?
		List<String> sequences2 = Arrays.asList(sequences);
		int index = 0;
		while (plausible && index < sequences2.size()) {
			String seq = sequences2.get(index);
			if (!StringUtils.contains(seq, "?") && listOfSizes.size() > index) {
				plausible = plausible && sequences[index].length() == Integer.parseInt(listOfSizes.get(index));
			} else {
				return plausible;
			}
			index++;
		}
		return plausible;
	}

	private List<Integer> reperageDesPositionsAtester(String springs) {
		List<Integer> positions = new ArrayList<>();
		int index = springs.indexOf("?");
		while (index >= 0) {
			positions.add(index);
			index = springs.indexOf("?", index + 1);
		}
		return positions;
	}

}