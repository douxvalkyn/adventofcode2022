package advent2023.day16;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import communs.Case;
import communs.Grille;
import communs.Outil;

public class Day16 {

	private static final Logger LOGGER = LoggerFactory.getLogger(Day16.class);

	public static void main(String[] args) throws IOException {
		LOGGER.info("[{}]", Day16.class.getSimpleName());
		Day16 day = new Day16();
		LocalDateTime start = LocalDateTime.now();
		day.run2();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		LOGGER.info("[Time:{} s]", duree.getSeconds());
	}



	public void run2() {
		String file = "src/main/resources/advent2023/day16.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		//0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest

List<Integer>sums = new ArrayList<>();

		//on teste toutes les cases des bords du haut
		for (int i=0;i<grille.getNbCol();i++) {
			//System.out.println(i);
			grille = new Grille(fichier.get(0).length(), fichier.size());
			initGrille(fichier, grille);
			Case cellActuelle = grille.getCase(i, 0);
			cellActuelle.setValeur(1);

			//on sauvegarde la liste de toutes les cellules x directions pour ne pas continuer à l'infini
			Map<Case, Integer> map = new HashMap<>();


			//gestion premiere direction ... //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
			int direction=1;
			if (cellActuelle.getEtat().equals("\\")) {
				direction=2;
			}
			if (cellActuelle.getEtat().equals(".")) {
				direction=1;
			}
			if (cellActuelle.getEtat().equals("/")) {
				direction=3;
			}
			if (cellActuelle.getEtat().equals("-")) {
				direction=2;
				avance(grille, cellActuelle, direction, map);
				direction=3;
			}
			if (cellActuelle.getEtat().equals("|")) {
				direction=1;
			}

			avance(grille, cellActuelle, direction, map);
			//grille.affichageValeur();

			//calcule des cases energisées
			int sum = 0;
			for (Case cell:grille.getCases()) {
				if (cell.getValeur() != 0) {
					sum++;
				}
			}
		//	System.err.println("Run2: "+sum);
			sums.add(sum);
		
		}
		
		
				//on teste toutes les cases des bords du bas
				for (int i=0;i<grille.getNbCol();i++) {
				//	System.out.println(i);
					grille = new Grille(fichier.get(0).length(), fichier.size());
					initGrille(fichier, grille);
					Case cellActuelle = grille.getCase(i, grille.getNbLignes()-1);
					cellActuelle.setValeur(1);

					//on sauvegarde la liste de toutes les cellules x directions pour ne pas continuer à l'infini
					Map<Case, Integer> map = new HashMap<>();


					//gestion premiere direction ... //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
					int direction=0;
					if (cellActuelle.getEtat().equals("\\")) {
						direction=3;
					}
					if (cellActuelle.getEtat().equals(".")) {
						direction=0;
					}
					if (cellActuelle.getEtat().equals("/")) {
						direction=2;
					}
					if (cellActuelle.getEtat().equals("-")) {
						direction=2;
						avance(grille, cellActuelle, direction, map);
						direction=3;
					}
					if (cellActuelle.getEtat().equals("|")) {
						direction=0;
					}

					avance(grille, cellActuelle, direction, map);
					//grille.affichageValeur();

					//calcule des cases energisées
					int sum = 0;
					for (Case cell:grille.getCases()) {
						if (cell.getValeur() != 0) {
							sum++;
						}
					}
				//	System.err.println("Run2: "+sum);
					sums.add(sum);
				}
				
		
				//on teste toutes les cases des bords de gauche
				for (int i=0;i<grille.getNbLignes();i++) {
				//	System.out.println(i);
					grille = new Grille(fichier.get(0).length(), fichier.size());
					initGrille(fichier, grille);
					Case cellActuelle = grille.getCase(0, i);
					cellActuelle.setValeur(1);

					//on sauvegarde la liste de toutes les cellules x directions pour ne pas continuer à l'infini
					Map<Case, Integer> map = new HashMap<>();


					//gestion premiere direction ... //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
					int direction=2;
					if (cellActuelle.getEtat().equals("\\")) {
						direction=1;
					}
					if (cellActuelle.getEtat().equals(".")) {
						direction=2;
					}
					if (cellActuelle.getEtat().equals("/")) {
						direction=0;
					}
					if (cellActuelle.getEtat().equals("-")) {
						direction=2;
					}
					if (cellActuelle.getEtat().equals("|")) {
						direction=0;
						avance(grille, cellActuelle, direction, map);
						direction=1;
					}

					avance(grille, cellActuelle, direction, map);
					//grille.affichageValeur();

					//calcule des cases energisées
					int sum = 0;
					for (Case cell:grille.getCases()) {
						if (cell.getValeur() != 0) {
							sum++;
						}
					}
				//	System.err.println("Run2: "+sum);
					sums.add(sum);
				}
		
				
				//on teste toutes les cases des bords de droite
				for (int i=0;i<grille.getNbLignes();i++) {
				//	System.out.println(i);
					grille = new Grille(fichier.get(0).length(), fichier.size());
					initGrille(fichier, grille);
					Case cellActuelle = grille.getCase(grille.getNbLignes()-1, i);
					cellActuelle.setValeur(1);

					//on sauvegarde la liste de toutes les cellules x directions pour ne pas continuer à l'infini
					Map<Case, Integer> map = new HashMap<>();


					//gestion premiere direction ... //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
					int direction=3;
					if (cellActuelle.getEtat().equals("\\")) {
						direction=0;
					}
					if (cellActuelle.getEtat().equals(".")) {
						direction=3;
					}
					if (cellActuelle.getEtat().equals("/")) {
						direction=1;
					}
					if (cellActuelle.getEtat().equals("-")) {
						direction=2;
					}
					if (cellActuelle.getEtat().equals("|")) {
						direction=0;
						avance(grille, cellActuelle, direction, map);
						direction=1;
					}

					avance(grille, cellActuelle, direction, map);
				//	grille.affichageValeur();

					//calcule des cases energisées
					int sum = 0;
					for (Case cell:grille.getCases()) {
						if (cell.getValeur() != 0) {
							sum++;
						}
					}
			//		System.err.println("Run2: "+sum);
					sums.add(sum);
				}
		
		System.err.println("Run2: "+ Outil.max(sums));
	}




	public void run1() {
		String file = "src/main/resources/advent2023/day16.txt";
		List<String> fichier = Outil.importationString(file);
		Grille grille = new Grille(fichier.get(0).length(), fichier.size());
		initGrille(fichier, grille);
		int direction = 2; //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
		Case cellActuelle = grille.getCase(0, 0);
		cellActuelle.setValeur(1);

		//on sauvegarde la liste de toutes les cellules x directions pour ne pas continuer à l'infini
		Map<Case, Integer> map = new HashMap<>();

		//gestion premiere direction ...
		if (cellActuelle.getEtat().equals("\\")) {
			direction=1;
		}
		if (cellActuelle.getEtat().equals(".")) {
			direction=2;
		}
		if (cellActuelle.getEtat().equals("/")) {
			direction=0;
		}
		if (cellActuelle.getEtat().equals("-")) {
			direction=2;
		}
		if (cellActuelle.getEtat().equals("|")) {
			direction=1;
		}








		avance(grille, cellActuelle, direction, map);
		grille.affichageValeur();

		//calcule des cases energisées
		int sum = 0;
		for (Case cell:grille.getCases()) {
			if (cell.getValeur() != 0) {
				sum++;
			}
		}
		System.err.println("Run1: "+sum);
	}



	private static void initGrille(List<String> fichier, Grille grille) {
		for (int i = 0; i < grille.getNbCol(); i++) {
			for (int j = 0; j < grille.getNbLignes(); j++) {
				grille.getCase(i, j).setEtat("" + fichier.get(j).charAt(i));
			}
		}
		//grille.affichageEtat();
	}

	private static boolean contientCaseAvecDirection(Map<Case, Integer> map, Case cell, Integer direction) {
		for (Entry<Case, Integer> entry : map.entrySet()) {
			if(entry.getKey()== cell && entry.getValue()==direction) {
				return true;
			}
		}
		return false;
	}


	private static void avance(Grille grille, Case cellActuelle, int direction, Map<Case, Integer> map) {
		//grille.affichageValeur();
		//System.out.println(" ");

		cellActuelle = cellActuelle.getCasesAdjacentes2(grille).get(direction);



		if (cellActuelle != null && !contientCaseAvecDirection(map, cellActuelle, direction )) {

			cellActuelle.setValeur(1);
			map.put(cellActuelle, direction);

			switch (cellActuelle.getEtat()) { 
			case ".":
				avance(grille, cellActuelle, direction, map);
				break;
			case "/": //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
				if (direction==2) {
					avance(grille, cellActuelle, 0, map);
				}
				if (direction==3) {
					avance(grille, cellActuelle, 1, map);
				}
				if (direction==0) {
					avance(grille, cellActuelle, 2, map);
				}
				if (direction==1) {
					avance(grille, cellActuelle, 3, map);
				}
				break;
			case "\\": //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
				if (direction==2) {
					avance(grille, cellActuelle, 1, map);
				}
				if (direction==3) {
					avance(grille, cellActuelle, 0, map);
				}
				if (direction==0) {
					avance(grille, cellActuelle, 3, map);
				}
				if (direction==1) {
					avance(grille, cellActuelle, 2, map);
				}
				break;
			case "-": //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
				if (direction==2 || direction==3) {
					avance(grille, cellActuelle, direction, map);
				}
				if (direction==0 || direction==1) {
					avance(grille, cellActuelle, 2, map);
					avance(grille, cellActuelle, 3, map);
				}
				break;
			case "|": //0: vers le Nord   1: vers le Sud   2: vers l'Est   3: vers l'Ouest
				if (direction==0 || direction==1) {
					avance(grille, cellActuelle, direction, map);
				}
				if (direction==2 || direction==3) {
					avance(grille, cellActuelle, 0, map);
					avance(grille, cellActuelle, 1, map);
				}
				break;
			default:
				break;
			}
		}
	}



}